<?php


return [

'LIVE'=>[
    
'CREDENTIALS'=>[
    
    'CLIENT_ID'=>'tboprod',
    'USER_NAME'=>'DELJ203',
    'PASSWORD'=>'tbolive-203@@',
    'IP'=>'103.211.216.225',
    
    ],

'AUTHENTICATE' => 'https://api.travelboutiqueonline.com/SharedAPI/SharedData.svc/rest/Authenticate',
	
	'FLIGHT'=>[
	
				'SEARCH' =>'https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Search/',
				'FARE_QUOTE' => 'https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareQuote/',
				'FARE_RULE' => 'https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/FareRule/',
				'SSR' => 'https://tboapi.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/SSR/',
				'BOOK'=>'https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/rest/Book/',
				'TICKET'=>'https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/Ticket/',
				'DETAILS'=>'https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/GetBookingDetails/',
				'RELEASE_PNR'=>'https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/ReleasePNRRequest/',
				'CHANGE_REQUEST'=>'https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/SendChangeRequest/',
				'CHANGE_STATUS'=>'https://booking.travelboutiqueonline.com/AirAPI_V10/AirService.svc/GetChangeRequestStatus/'
				
			],
			
	'HOTELS'=>[
				'SEARCH'=> 'https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelResult/',
				'HOTEL_INFO'=> 'https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelInfo/',
				'BLOCK_ROOM'=>'https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/BlockRoom/',
				'ROOM_INFO'=>'https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelRoom/',
				'BOOK'=>'https://booking.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/Book/',
				'BOOKING_DETAILS'=>'https://booking.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetBookingDetail/',		

	],

],


'TEST'=>[
	'CREDENTIALS'=>[
    
		'CLIENT_ID'=>'ApiIntegrationNew',
		'USER_NAME'=>'Thailandwale',
		'PASSWORD'=>'Thail@123',
		'IP'=>'103.211.216.225',
		
		],
'AUTHENTICATE' => 'http://api.tektravels.com/SharedServices/SharedData.svc/rest/Authenticate',
	
	'FLIGHT'=>[
	
				'SEARCH' =>'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Search/',
				'FARE_QUOTE' => 'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/FareQuote/',
				'FARE_RULE' => 'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/FareRule/',
				'SSR' => 'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/SSR/',
				'BOOK'=>'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Book/',
				'TICKET'=>'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Ticket/',
				'DETAILS'=>'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/GetBookingDetails/',
				'RELEASE_PNR'=>'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/ReleasePNRRequest/',
				'CHANGE_REQUEST'=>'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/SendChangeRequest/',
				'CHANGE_STATUS'=>'http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/GetChangeRequestStatus/'
				
			],
			
	'HOTELS'=>[
				'SEARCH'=> 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelResult/',
				'HOTEL_INFO'=> 'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelInfo/',
				'BLOCK_ROOM'=>'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/BlockRoom/',
				'ROOM_INFO'=>'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelRoom/',
				'BOOK'=>'http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/Book/',
				'BOOKING_DETAILS'=>'http://api.tektravels.com/BookingEngineService_Hotel/HotelService.svc/rest/GetBookingDetail/',		

	],

],
	
	
	
	
];