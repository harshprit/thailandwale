<?php

namespace App\Notifications\Frontend\ReferandEarn;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Jobs\SendReferenceEmail;


class ReferenceSend extends Notification 
{
    

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($sender)
    {
        $this->sender=$sender;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("$this->sender Signed Up to ".app_name())
                    ->line("Your friend $this->sender has invited you to join thailandwale.com, You will be rewarded INR 1000 worth of thailandwale cash as a referral bonus on successful registration.")
                    ->action('Click here to Signup', route('frontend.refer.register',['token'=>access()->user()->id]))
                    ->line('Best Regards!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
