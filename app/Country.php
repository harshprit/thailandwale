<?php

namespace App;

use App\Models\BaseModel;
use App\Models\City;
use App\Models\State;
use Illuminate\Database\Eloquent\SoftDeletes;
use Exception,
    Session;

class Country extends BaseModel
{
    protected $fillable = [
        'sortname',
        'name',
        'phonecode',
        ];
    protected $table;

    public function cities()
    {
        return $this->hasManyThrough(City::class,State::class);
    }

}
