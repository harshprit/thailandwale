<?php

Breadcrumbs::register('admin.destinations.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.destinations.management'), route('admin.destinations.index'));
});

Breadcrumbs::register('admin.destinations.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.destinations.index');
    $breadcrumbs->push(trans('menus.backend.destinations.create'), route('admin.destinations.create'));
});

Breadcrumbs::register('admin.destinations.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.destinations.index');
    $breadcrumbs->push(trans('menus.backend.destinations.edit'), route('admin.destinations.edit', $id));
});
