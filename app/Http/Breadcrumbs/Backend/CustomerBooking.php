<?php

Breadcrumbs::register('admin.customer_bookings.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.customer_bookings.management'), route('admin.customer_bookings.index'));
});

Breadcrumbs::register('admin.customer_bookings.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.customer_bookings.index');
    $breadcrumbs->push(trans('menus.backend.customer_bookings.create'), route('admin.customer_bookings.create'));
});

Breadcrumbs::register('admin.customer_bookings.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.customer_bookings.index');
    $breadcrumbs->push(trans('menus.backend.destinations.edit'), route('admin.customer_bookings.edit', $id));
});
