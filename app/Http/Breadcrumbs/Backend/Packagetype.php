<?php

Breadcrumbs::register('admin.packagetypes.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.packagetypes.management'), route('admin.packagetypes.index'));
});

Breadcrumbs::register('admin.packagetypes.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.packagetypes.index');
    $breadcrumbs->push(trans('menus.backend.packagetypes.create'), route('admin.packagetypes.create'));
});

Breadcrumbs::register('admin.packagetypes.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.packagetypes.index');
    $breadcrumbs->push(trans('menus.backend.packagetypes.edit'), route('admin.packagetypes.edit', $id));
});
