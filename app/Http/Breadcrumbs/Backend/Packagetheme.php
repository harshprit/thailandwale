<?php

Breadcrumbs::register('admin.packagethemes.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.packagethemes.management'), route('admin.packagethemes.index'));
});

Breadcrumbs::register('admin.packagethemes.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.packagethemes.index');
    $breadcrumbs->push(trans('menus.backend.packagethemes.create'), route('admin.packagethemes.create'));
});

Breadcrumbs::register('admin.packagethemes.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.packagethemes.index');
    $breadcrumbs->push(trans('menus.backend.packagethemes.edit'), route('admin.packagethemes.edit', $id));
});
