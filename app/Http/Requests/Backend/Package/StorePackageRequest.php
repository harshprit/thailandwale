<?php

namespace App\Http\Requests\Backend\Package;

use Illuminate\Foundation\Http\FormRequest;

class StorePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('create-package');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'           => 'required|max:191',
            'destination'       => 'required',
            'featured_image' => 'required',
            'content'        => 'required',                   
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please insert Package Title',
            'title.max'      => 'Package Title may not be greater than 191 characters.',
        ];
    }
}
