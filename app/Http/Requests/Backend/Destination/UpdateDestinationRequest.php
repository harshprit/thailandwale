<?php

namespace App\Http\Requests\Backend\Destination;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDestinationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-destination');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'           => 'required|max:191',
           
            'description'     => 'required',
           
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please insert Blog Title',
            'title.max'      => 'Blog Title may not be greater than 191 characters.',
        ];
    }
}
