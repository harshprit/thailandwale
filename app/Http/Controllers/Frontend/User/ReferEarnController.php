<?php

namespace App\Http\Controllers\Frontend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Notifications\Frontend\ReferandEarn\ReferenceSend;
use App\Models\Access\User\User;
class ReferEarnController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user)
    {
        $this->user=$user;
    }
    public function sendReference(Request $request)
    {
        $data=$request->input('email');
        $data=explode(',',$data);
        foreach($data as $email)
        {
            (new User)->forceFill([
                'name'=>'Friend',
                'email'=>$email,
            ])->notify(new ReferenceSend(access()->user()->name));
        }
        return "Invitation sent successfully";

    }
    
    
}
