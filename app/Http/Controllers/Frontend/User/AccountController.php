<?php

namespace App\Http\Controllers\Frontend\User;
use App\Repositories\Frontend\CustomerBooking\CustomerBookingRepository;
use App\Models\Access\User\User;
use App\Models\CustomerBooking\CustomerBooking;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\Models\Access\User\UserDetail;
use Illuminate\Support\Facades\Storage;
use App\Wallet;



/**
 * Class AccountController.
 *
 */


class AccountController extends Controller
{
    protected $customerbooking;

    protected $upload_path;

public function __construct(CustomerBookingRepository $customerbooking)
{
  $this->customerbooking = $customerbooking;

  $this->upload_path = 'img'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR.'profile'.DIRECTORY_SEPARATOR;
  $this->storage = Storage::disk('public');
}
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $passbook=User::find(access()->user()->id)->wallet()->get();
        $wallet=User::find(access()->user()->id)->wallet()->latest()->first();
        $bookings=User::find(access()->user()->id)->bookings()->get();

        if(isset($wallet))
        {
            $walletBalance=$wallet->amountBalance;
        }
        else
        {
            $walletBalance=0;
        }
        //dd($bookings);
        return view('frontend.user.account')->with(['wallet'=>$walletBalance,'passbook'=>$passbook,'bookings'=>$bookings]);
    }
    public function getBookings(Request $request)
    {
        return Datatables::of($this->customerbooking->getForDataTable())
            ->escapeColumns(['id'])
             ->addColumn('booking_id', function ($customerbooking) {
                  return $customerbooking->booking_id;
              })
            ->addColumn('reference_no', function ($customerbooking) {
                return $customerbooking->reference_no;
            })
            ->addColumn('confirmation_no', function ($customerbooking) {
                return $customerbooking->confirmation_no;
            })
            ->addColumn('lead_passenger', function ($customerbooking) {
                  return $customerbooking->lead_passenger;
              })
              ->addColumn('booking_type', function ($customerbooking) {
                  return $customerbooking->booking_type;
              })
            ->addColumn('checkin', function ($customerbooking) {
                return $customerbooking->checkin;
            })
            ->addColumn('checkout', function ($customerbooking) {
                  return $customerbooking->checkout;
              })
            ->addColumn('actions', function ($customerbooking) {
                return $customerbooking->action_buttons;
            })
            ->make(true);

    }
    public function bookingDetail(Request $request,$type,$booking_id)
    {
        $booking=CustomerBooking::where('booking_id',$booking_id)->first();
        if($type==='hotel')
        {
            return view('frontend.user.manage-booking')->with('booking',$booking);
        }
        else
        {
            return view('frontend.user.manage-booking-flight')->with('booking',$booking);
        }

    }
    public function printTicket(Request $request,$type,$booking_id)
    {
        $booking=json_decode(CustomerBooking::where('booking_id',$booking_id)->first()->booking_detail);
        if($type==='hotel')
        {

            return view('frontend.home.hotel_voucher')->with(['bookingInfo'=>$booking]);
        }
        else
        {
            return view('frontend.user.account.flights.ticket')->with(['booking'=>$booking]);
        }
    }

    public function invoice(Request $request, $booking_id)
    {
        return view('frontend.user.account.invoice');
    }

    public function storeUserDetails(Request $request){
      $data = $request->except('_token');
      $user = User::find(access()->user()->id);
      $user->first_name = $data['first_name'];
      $user->last_name = $data['last_name'];
      $user->email = $data['your_email'];
      $user->save();
      $userDetail = UserDetail::where('user_id',access()->user()->id)->first();
      if(!$userDetail){
        UserDetail::create([
          'user_id'=>access()->user()->id,
          'mobile'=>$data['your_mobile'],
          'dob'=>$data['your_date'],
          'address'=>$data['your_address'],
          'city'=>$data['your_city'],
          'state'=>$data['your_state'],
          'image'=>$this->uploadImage($data['image'])
        ]);
      }
      else {
          $userDetail->user_id = access()->user()->id;
          $userDetail->mobile = $data['your_mobile'];
          $userDetail->dob = $data['your_date'];
          $userDetail->address = $data['your_address'];
          $userDetail->city = $data['your_city'];
          $userDetail->state = $data['your_state'];
          if(isset($data['image'])){
          $userDetail->image = $this->uploadImage($data['image']);
          }

          $userDetail->save();
      }
      return redirect(route('frontend.user.account'));
    }

    public function uploadImage($input)
    {
        $avatar = $input;

        if (isset($input) && !empty($input)) {
            $fileName = time().$avatar->getClientOriginalName();

            $this->storage->put($this->upload_path.$fileName, file_get_contents($avatar->getRealPath()));

            return $fileName;
        }
    }
}
