<?php

namespace App\Http\Controllers\Frontend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coupon\Coupon;
use App\CouponUsed;
use DateTime;

class CouponController extends Controller
{
    public function verifyCoupon(Request $request)
    {
        $promoCode=$request->input('code');
        $amount=$request->input('amount');
        $discount=0;
        $promo=Coupon::where(['code'=>$promoCode])->latest()->first();
        $today=new DateTime();
        
        if(isset($promo))
        {
        $expiry=new DateTime($promo->expiry_date);
        $start=new DateTime($promo->start_date);
        $valid_from=$start->diff($today)->format('%R%a');
        $valid_to=$today->diff($expiry)->format('%R%a');
                if($valid_from>0 && $valid_to>0)
                {
                    if(empty(CouponUsed::where('couponId',$promo->couponId)->first()))
                    {
                        if($promo->type==='Percentage' && $amount>=$promo->minimum_amount)
                        {
                            $discount=($amount*$promo->amount)/100;
                            if($discount>$promo->max_discount)
                            {
                                $discount=$promo->max_discount;
                            }
                        }
                        else if($amount>=$promo->minimum_amount) 
                        {
                            $discount=$promo->amount;
                        }

                        $response=array(
                            'discount'=>round($discount,0),
                        );
                        
                    }
                    else
                    {
                        $response=array(
                            'error'=>"You have already used this Coupon, Try with Different Coupon",
                        );
                    }
                    
                    

                }
                else
                {
                    $response=array(
                        'error'=>'Promo Code Expired, Please Enter another Promo Code',
                    );

                }
        }
        else
        {
            $response=array(
                'error'=>'Invalid Promocode, Please Enter a Valid Promo Code',
            );
        }

        return json_encode($response);

    }
}
