<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Settings\Setting;
use App\Repositories\Frontend\Pages\PagesRepository;
use Illuminate\Http\Request;
use Appnings\Payment\Facades\Payment;
use App\Models\Package\Package;
use App\Models\Testimonial\Testimonial;
use App\Models\Destination\Destination;
use App\Models\Packagetype\Packagetype;
use App\Models\Packagetheme\Packagetheme;
use App\Models\Blogs\Blog;
use App\Models\CustomerBooking\CustomerBooking;
use App\Models\Transaction\Transaction;
use App\Models\Activity\Activity;
use App\Country;
use App\Token;
use DateInterval;
use DateTime;
use Exception,
    Session,
    DB;
use App\Mail\SendMail;
if (version_compare(phpversion(), '7.1', '>=')) {
    ini_set( 'serialize_precision', -1 );
}
/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
     
     
     protected $flightCabinClass="";
     public function __construct(){
         $this->userIp = gethostbyname(trim(`hostname`));
    $this->token = $token_id=Token::orderBy('id', 'desc')->first()->token;
            $city_list = $this->getHotelDestinations();
            
         $this->flightCabinClass = array(
             '1'=>'All',
             '2'=>'Economy',
             '3'=>'Premium Economy',
             '4'=>'Business',
             '5'=>'Premium Business',
             '6'=>'First',
             );
             $this->cities=$city_list;
             //dd($this->cities);
     }

     public function apiCalls($data,$url)
     {
        $search_json=json_encode($data);    
        $curl = curl_init($url);
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $search_json);
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_TIMEOUT,1000);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);  
        return $decoded;
     }
     
    public function index(Package $package,Destination $destination,PackageType $packageType,Packagetheme $packageTheme)
    {   
        
        $airport = $this->getAirportDetails();
        $package = $package->getAllPackage();
        $destination = $destination->getAllDestination();
        $packageType = $packageType->getAllPackageType();
        $packageTheme = $packageTheme->getAllPackageTheme();
        $testimonials = Testimonial::all();
        
        return view('frontend.index')
        ->with(
            [
                'airport'=>$this->getAirportDetails(),
                'package'=>$package,
                'destination'=>$destination,
                'packageType'=>$packageType,
                'packageTheme'=>$packageTheme,
                'testimonials' =>$testimonials
                ]
            );
    }
   
    /**
     * @return \Illuminate\View\View
     */
     
    public function macros()
    {
        return view('frontend.macros');
    }


public function packageDetail()
    {
        return view('frontend.home.package_detail');
    }



    /**
     * show page by $page_slug.
     */
    public function showPage($slug, PagesRepository $pages)
    {
        $result = $pages->findBySlug($slug);
        if(!isset($result))
        {
            return redirect()->back()->with('error','Error: Invalid request');
        }
        return view('frontend.pages.index')
            ->withpage($result);
    }
    public function blog()
    {
        return view('frontend.home.blog');
    }
    public function hotel(Package $package,Destination $destination,PackageType $packageType,Packagetheme $packageTheme)
    {   
        if($this->cities==false)
        {
            return redirect('/')->withErrors(['msg'=>'Error: Cities not found']);
        }
        $package = $package->getAllPackage();
        $destination = $destination->getAllDestination();
        $packageType = $packageType->getAllPackageType();
        $packageTheme = $packageTheme->getAllPackageTheme();
        // $city_list=$this->getCities();
        // $cities=simplexml_load_string($city_list->DestinationCityList);
        return view('frontend.home.hotels')->with(['cities'=>$this->cities,'package'=>$package,'destination'=>$destination,'packageType'=>$packageType,'packageTheme'=>$packageTheme]);
    }
    public function hotelCities()
    {
        $cities=$this->getHotelDestinations();
        
        // $cities=simplexml_load_string($city_list->DestinationCityList);
        $hotelCity=array();
        //dd($cities);
        if($cities!=false)
        {
                foreach ($cities as $city)
        {
            $hotelCity[$city['CityName']]=$city['CityId'];
            
        }
        }
        // dd($hotelCity);
        return $hotelCity;
        
    }
    public function services()
    {
        return view('frontend.home.services');
    }
    public function tours()
    {
        return view('frontend.home.tours');
    }
    public function room()
    {
        return view('frontend.home.room');
    }
    // public function flights()
    // {
    //     return view('frontend.home.flights')->with(['airport'=>$this->getAirportDetails()]);
    // }
    public function about()
    {
        return view('frontend.home.about');
    }
    public function contact()
    {
        return view('frontend.home.contact');
    }
    
        public function searchHotels(Request $request)
    {
        if($request->isMethod('post'))
        {
            $check_in=date('d/m/Y',strtotime($request->input('check_in')));
            $token_id=$this->token;
            $userIp=$this->userIp;
            $datetime1 = new DateTime(date_format(date_create($request->input('check_in')),"Y-m-d H:i:s"));
            $datetime2 = new DateTime(date_format(date_create($request->input('check_out')),"Y-m-d H:i:s"));
            $interval = $datetime1->diff($datetime2);
    
            $no_of_nights=$interval->format('%a');
            $city=explode(',',$request->input('dest'));
            $city_code=$city[1];
            $country_code='TH';
            $currency='INR';
            $nationality='IN';
            $no_of_rooms=$request->input('rooms');
            $adults=$request->input('adults');
            $children=$request->input('children');
            $c_ages=$request->input('ages');
            $x=array();
            $age_index=0;
            for($i=0;$i<$no_of_rooms;$i++){
                    $x[$i]['NoOfAdults']=$adults[$i];
                    $x[$i]['NoOfChild']=$children[$i];
    
                    for($j=0;$j<$children[$i];$j++){
                        $x[$i]['ChildAge'][$j]=$c_ages[$age_index];
                        $age_index++;
                    }
                }
            $room_guests= array();
            $room_guests=$x;
            $search_data=array(
                    'CheckInDate'=>$check_in,
                    'NoOfNights'=>$no_of_nights,
                    'CountryCode'=>$country_code,
                    'CityId'=>$city_code,
                    'ResultCount'=>500,
                    'PreferredCurrency'=>$currency,
                    'GuestNationality'=>$nationality,
                    'NoOfRooms'=>$no_of_rooms,
                    'RoomGuests'=>$room_guests,
                    'EndUserIp'=>$userIp,
                    'TokenId'=>$token_id,
                    'MaxRating'=>5,
                    'MinRating'=>0,
                        );
            $request->session()->put('hotel_search_data',$search_data);
            $search_url=config('constants.TEST.HOTELS.SEARCH');
            $decoded=$this->apiCalls($search_data,$search_url);
            $request->session()->put('hotel_listing_data',$decoded);
        }
        else{
            $decoded=$request->session()->get('hotel_listing_data');
        }
     

return view('frontend.home.hotel-listings')->with(['hotels'=>$decoded->HotelSearchResult->HotelResults,'trace_id'=>$decoded->HotelSearchResult->TraceId,'cities'=>$this->cities]);
}
    
//     public function searchHotels(Request $request)
//     {
        
//         $check_in=date('d/m/Y',strtotime($request->input('check_in')));

//         $token_id=$this->token;
//         $userIp=gethostbyname(trim(`hostname`));
//         $datetime1 = new DateTime(date_format(date_create($request->input('check_in')),"Y-m-d H:i:s"));
//         $datetime2 = new DateTime(date_format(date_create($request->input('check_out')),"Y-m-d H:i:s"));
//         $interval = $datetime1->diff($datetime2);
        
//         $no_of_nights=$interval->format('%a');
//         $city=explode(',',$request->input('dest'));
//         if(!array_key_exists('1',$city))
//         {
//             // if(!array_filter($city))
//             return redirect('/hotels')->withErrors(['City not defined!']);
//             // else 
//             // dd($city);
//         }
//         $city_code=$city[1];
//         $country_code='TH';
//         $currency='INR';
//         $nationality='IN';
//         $no_of_rooms=$request->input('rooms');
//         $adults=$request->input('adults');
//         $children=$request->input('children');
//         $c_ages=$request->input('ages');
//         $x=array();
//         $age_index=0;
//         for($i=0;$i<$no_of_rooms;$i++){
//                 $x[$i]['NoOfAdults']=$adults[$i];
//                 $x[$i]['NoOfChild']=$children[$i];
                
//                 for($j=0;$j<$children[$i];$j++){
//                     $x[$i]['ChildAge'][$j]=$c_ages[$age_index];
//                     $age_index++;
//                 }
//             }
//         $room_guests= array();
//         $room_guests=$x;
//         $search_data=array(
//                 'CheckInDate'=>$check_in,
//                 'NoOfNights'=>$no_of_nights,
//                 'CountryCode'=>$country_code,
//                 'CityId'=>$city_code,
//                 'ResultCount'=>null,
//                 'PreferredCurrency'=>$currency,
//                 'GuestNationality'=>$nationality,
//                 'NoOfRooms'=>$no_of_rooms,
//                 'RoomGuests'=>$room_guests,
//                 'EndUserIp'=>$userIp,
//                 'TokenId'=>$token_id,
//                 'MaxRating'=>5,
//                 'MinRating'=>0,
//                     );
//         $request->session()->put('hotel_search_data',$search_data);
//         $search_url="http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelResult/";
//         $decoded=$this->apiCalls($search_data,$search_url);
//         $request->session()->put('hotel_listing_data',$decoded);
//         if(!isset($this->cities))
//         {
//             return redirect()->back()->with('error','Error: Cities not found');
//         }
//          if(!isset($decoded))
//         {
//             return redirect()->back()->with('error','Error:Hotels not found');
//         }
// return view('frontend.home.hotel-listings')->with(['hotels'=>$decoded,'cities'=>$this->cities]);
// }
public function ajaxHotelInclusion(Request $request)
{
    $resultIndex=$request->resultIndex;
    $hotelCode=$request->hotelCode;
    $idSelector=$request->idSelector;
 
    $hotels=$request->session()->get('hote_search_result');
    
  //dd($hotels[$idSelector]);
  return view('frontend.home.partials.flights.ajax-hotel-inclusion')->with(['package_hotels'=>$hotels[$idSelector],'idSelector'=>$idSelector,'resultIndex'=>$resultIndex,'hotelCode'=>$hotelCode]);
}
public function ajaxFlightInclusion(Request $request)
{
    $resultIndex=$request->resultIndex;
    $flights=$request->session()->get('flight_listing_data');
   return view('frontend.home.partials.flights.ajax-flight-inclusion')->with(['flights'=>$flights,'resultIndex'=>$resultIndex]);
}
    
   public function searchFlights(Request $request)
    {
      
        $inputData=$request->all();
        if($request->isMethod('post'))
        {
        $userIp=$this->userIp;
        $token_id=$this->token;
        $journey_date=$request->input('journey_date');
        $country_code='TH';
        $currency='INR';
        $nationality='IN';
        $adult=$request->input('adult_one');
        $child=$request->input('child_one');
        $infant=$request->input('infants_one');
        $class=$request->input('travel_class');
        $origin=$request->input('from');
        $destination=$request->input('to');


        if($class==""||$class==null){
            $class=2;
        }
        $journey_type=$request->input('journey_type');
        $is_domestic=true;

        if($journey_type==1||$journey_type==2)
        {
        $org=explode(',',$origin[0]);
        $det=explode(',',$destination[0]);
        // echo '<pre>';print_r($org);die();
        if($org[0]==$det[0])
        {
            $is_domestic=true;
        }
        else
        {
            $is_domestic=false;
        }

        for($i=0;$i<1;$i++)
        {
            $segments[$i]['Origin']=$org[0];
            $segments[$i]['Destination']=$det[0];
            $segments[$i]['FlightCabinClass']=$class;
            $segments[$i]['PreferredDepartureTime']=date('Y-m-d',strtotime($journey_date[$i])).'T00:00:00';
            $segments[$i]['PreferredArrivalTime']=date('Y-m-d',strtotime($journey_date[$i])).'T00:00:00';
                if(count($journey_date)>1)
                {
                $segments[$i+1]['Origin']=$det[0];
                $segments[$i+1]['Destination']=$org[0];
                $segments[$i+1]['FlightCabinClass']=$class;
                $segments[$i+1]['PreferredDepartureTime']=date('Y-m-d',strtotime($journey_date[$i+1])).'T00:00:00';
                $segments[$i+1]['PreferredArrivalTime']=date('Y-m-d',strtotime($journey_date[$i+1])).'T00:00:00';
                }
        }
    }
    else if($journey_type==3)
    {
        for($i=0;$i<count($origin);$i++)
        {
        $org=explode(',',$origin[$i]);
        $det=explode(',',$destination[$i]);
        $segments[$i]['Origin']=$org[0];
        $segments[$i]['Destination']=$det[0];
        $segments[$i]['FlightCabinClass']=$class;
        $segments[$i]['PreferredDepartureTime']=date('Y-m-d',strtotime($journey_date[$i])).'T00:00:00';
        $segments[$i]['PreferredArrivalTime']=date('Y-m-d',strtotime($journey_date[$i])).'T00:00:00';
        }
    }
        $search_data=array(
                    'EndUserIp'=>$userIp,
                    'TokenId'=>$token_id,
                    'AdultCount'=>$adult,
                    'ChildCount'=>$child,
                    'InfantCount'=>$infant,
                    'DirectFlight'=>false,
                    'JourneyType'=>$journey_type,
                    'Segments'=>$segments,
                );
//        dd($search_data);
   $request->session()->put('flight_search_data',$search_data);
   $request->session()->put('journey_type',$journey_type);
   $request->session()->put('is_domestic',$is_domestic);
   $search_url = config('constants.TEST.FLIGHT.SEARCH');
   $decoded=$this->apiCalls($search_data,$search_url);
     
            }
            else
            {
                $journey_type=$request->session()->get('journey_type');
                $decoded=$request->session()->get('flight_listing_data');
                $is_domestic=$request->session()->get('is_domestic');
            }

          
            // dd($decoded);

   $flight_search = $request->session()->get('flight_search_data');

if(isset($decoded->Response->ResponseStatus) && $decoded->Response->ResponseStatus==1)
{
$request->session()->put('flight_listing_data',$decoded);

/*Don't Remove this Comment*/
$sources=array();
$stops=array();
$source_count=0;
//dd($decoded->Response->Results[0][0]);
$airlineDetails=array();
 foreach($decoded->Response->Results[0] as $result)
 {
     $seg_count=0;
     foreach($result->Segments as $segment)
     {
         
         $airlineDetails[$seg_count][$source_count]=$segment[0]->Airline->AirlineName;
         $stops[$seg_count][$source_count]=count($segment)-1;
         
         $seg_count++;
         
     }
     
     $source_count++;
     
     
     
 }
 
 $filters=array();
 $stopCount=array();
 for($i=0;$i<count($stops);$i++)
 {
     $filters[$i]['stops']=array_count_values($stops[$i]);
     $filters[$i]['airlines']=array_count_values($airlineDetails[$i]);
     
 }
 //dd($filters);
 
 
/*Don't Remove Comment End */
 //dd(array_count_values($stops));
//dd($decoded->Response->Results[0][0]);
//dd($this->flightCabinClass);
return view('frontend.home.flights-listing')->with(['airport'=>$this->getAirportDetails(),'journey_type'=>$journey_type,'is_domestic'=>$is_domestic,'flights'=>$decoded,'cabinClass'=>$this->flightCabinClass,'search_info'=>$flight_search,'filters'=>$filters]);;
}
else{
    $msg = $decoded->Response->Error->ErrorMessage;
    $request->session()->flash('flash_error',$msg);
     return redirect()->back()->with('error',$msg);
}
}




//     public function searchFlights(Request $request)
//     {
//         $inputData=$request->all();
//         if($request->isMethod('post'))
//         {
//         $userIp=gethostbyname(trim(`hostname`));
//         $token_id=Token::orderBy('id', 'desc')->first()->token;
//         $journey_date=$request->input('journey_date');
//         $country_code='TH';
//         $currency='INR';
//         $nationality='IN';
//         $adult=$request->input('adult_one');
//         $child=$request->input('child_one');
//         $infant=$request->input('infants_one');
//         $class=$request->input('travel_class');
//         $origin=$request->input('from');
//         $destination=$request->input('to');
        
        
//         if($class==""||$class==null){
//             $class=2;
//         }
//         $journey_type=$request->input('journey_type');
//         $is_domestic=true;
        
//         if($journey_type==1||$journey_type==2)
//         {
//         $org=explode(',',$origin[0]);
//         $det=explode(',',$destination[0]);

//         if($org[1]==$det[1])
//         {
//             $is_domestic=true;
//         }
//         else
//         {
//             $is_domestic=false;
//         }
            
//         for($i=0;$i<1;$i++)
//         { 
//             $segments[$i]['Origin']=$org[0];
//             $segments[$i]['Destination']=$det[0];
//             $segments[$i]['FlightCabinClass']=$class;
//             $segments[$i]['PreferredDepartureTime']=date('Y-m-d',strtotime($journey_date[$i])).'T00:00:00';
//             $segments[$i]['PreferredArrivalTime']=date('Y-m-d',strtotime($journey_date[$i])).'T00:00:00';
//                 if(count($journey_date)>1)
//                 {
//                 $segments[$i+1]['Origin']=$det[0];
//                 $segments[$i+1]['Destination']=$org[0];
//                 $segments[$i+1]['FlightCabinClass']=$class;
//                 $segments[$i+1]['PreferredDepartureTime']=date('Y-m-d',strtotime($journey_date[$i+1])).'T00:00:00';
//                 $segments[$i+1]['PreferredArrivalTime']=date('Y-m-d',strtotime($journey_date[$i+1])).'T00:00:00';
//                 }
//         }
//     }
//     else if($journey_type==3)
//     {
//         for($i=0;$i<count($origin);$i++)
//         {
//         $org=explode(',',$origin[$i]);
//         $det=explode(',',$destination[$i]);
//         $segments[$i]['Origin']=$org[0];
//         $segments[$i]['Destination']=$det[0];
//         $segments[$i]['FlightCabinClass']=$class;
//         $segments[$i]['PreferredDepartureTime']=date('Y-m-d',strtotime($journey_date[$i])).'T00:00:00';
//         $segments[$i]['PreferredArrivalTime']=date('Y-m-d',strtotime($journey_date[$i])).'T00:00:00';
//         }
//     }
//         $search_data=array(
//                     'EndUserIp'=>$userIp,
//                     'TokenId'=>$token_id,
//                     'AdultCount'=>$adult,
//                     'ChildCount'=>$child,
//                     'InfantCount'=>$infant,
//                     'DirectFlight'=>false,
//                     'JourneyType'=>$journey_type,
//                     'Segments'=>$segments,
//                 );
//   $request->session()->put('flight_search_data',$search_data);
//   $request->session()->put('journey_type',$journey_type);
//   $request->session()->put('is_domestic',$is_domestic);
//   $search_url="http://api.tektravels.com/BookingEngineService_Air/AirService.svc/rest/Search/";
//   $decoded=$this->apiCalls($search_data,$search_url);
//             }
//             else
//             {
//                 $journey_type=$request->session()->get('journey_type');
//                 $decoded=$request->session()->get('flight_listing_data');
//                 $is_domestic=$request->session()->get('is_domestic');
//             }
            
//             //dd($search_data);

   
//   $flight_search = $request->session()->get('flight_search_data');

// if($decoded->Response->ResponseStatus==1)
// {
// $request->session()->put('flight_listing_data',$decoded);
// //echo $curl_response;
// //dd($decoded);
// //dd($decoded->Response->Results[0][2]);
// if($journey_type==3)
// {
//   //dd($decoded->Response->Results[0][0]); 
//     return view('frontend.home.flights-listing-multi-trip')->with(['flights'=>$decoded,'cabinClass'=>$this->flightCabinClass,'search_info'=>$flight_search]);

// }
// if($journey_type==1)
// {
//     return view('frontend.home.flights-one-way-listing')->with(['flights'=>$decoded,'cabinClass'=>$this->flightCabinClass,'search_info'=>$flight_search]);
// }
// else if($is_domestic)
// {
//   //dd($decoded->Response->Results[1][0]);
    
//     return view('frontend.home.flights-listing-domestic')->with(['flights'=>$decoded,'cabinClass'=>$this->flightCabinClass,'search_info'=>$flight_search]);
// }
// else
// {
//   // dd($decoded->Response->Results[0][0]);
//     return view('frontend.home.flights-listing-intl')->with(['flights'=>$decoded,'cabinClass'=>$this->flightCabinClass,'search_info'=>$flight_search]);
// }
// }
// else{
//     $msg = $decoded->Response->Error->ErrorMessage;
//         $request->session()->flash('flash_error',$msg);
//      return redirect()->back()->with('error',$msg);   
// }
// }

public function getPassangerDetailsReturn(Request $request)
{
    $trace_id=$request->input('TraceId');
    $result_index=$request->input('ob_ResultIndex');
    $result_index_return=$request->input('ib_ResultIndex');
    $flight_listing = $request->session()->get('flight_listing_data'); 
    $fare_rule_return = $this->getFlightFareRule($result_index_return,$trace_id);
    $fare_quote_return = $this->getFlightFareQuote($result_index_return,$trace_id);
     $fare_rule = $this->getFlightFareRule($result_index,$trace_id);
     $fare_quote = $this->getFlightFareQuote($result_index,$trace_id);
     $ssr = $this->getFlightSSR($result_index,$trace_id);
     $ssr_return = $this->getFlightSSR($result_index_return,$trace_id);
     $country=Country::get();
     $adult_price="";
     $child_price="";
     $affent_price="";
 
     $adult_price_return="";
     $child_price_return="";
     $affent_price_return="";
 
   //var_dump($country);
   //dd($ssr);
   if(isset($fare_quote->Response->Results->FareBreakdown) && count($fare_quote->Response->Results->FareBreakdown)>0)
   {
     if(isset($fare_quote->Response->Results->FareBreakdown[0])){  
       $adults_base=$fare_quote->Response->Results->FareBreakdown[0]->BaseFare;
       $adults_tax=$fare_quote->Response->Results->FareBreakdown[0]->Tax;
       $adults_yqtax=$fare_quote->Response->Results->FareBreakdown[0]->YQTax;
       $adults_adl_txn_fee_ofd=$fare_quote->Response->Results->FareBreakdown[0]->AdditionalTxnFeeOfrd;
       $adults_adl_txn_fee_pbd=$fare_quote->Response->Results->FareBreakdown[0]->AdditionalTxnFeePub;
       
        $adults=$fare_quote->Response->Results->FareBreakdown[0]->PassengerCount;
        
         $adult_price=array(
         'base'=>$adults_base/$adults,
         'tax'=>$adults_tax/$adults,
         'yq_tax'=>$adults_yqtax/$adults,
         'adl_txn_fee_ofd'=>$adults_adl_txn_fee_ofd/$adults,
         'adl_txn_fee_pbd'=>$adults_adl_txn_fee_pbd/$adults,
     );
     }
     if(isset($fare_quote->Response->Results->FareBreakdown[1])){
       $child_base=$fare_quote->Response->Results->FareBreakdown[1]->BaseFare;
       $child_tax=$fare_quote->Response->Results->FareBreakdown[1]->Tax;
       $child_yqtax=$fare_quote->Response->Results->FareBreakdown[1]->YQTax;
       $child_adl_txn_fee_ofd=$fare_quote->Response->Results->FareBreakdown[1]->AdditionalTxnFeeOfrd;
       $child_adl_txn_fee_pbd=$fare_quote->Response->Results->FareBreakdown[1]->AdditionalTxnFeePub;
       
        $child=$fare_quote->Response->Results->FareBreakdown[1]->PassengerCount;
        
         $child_price=array(
         'base'=>$child_base/$child,
         'tax'=>$child_tax/$child,
         'yq_tax'=>$child_yqtax/$child,
         'adl_txn_fee_ofd'=>$child_adl_txn_fee_ofd/$child,
         'adl_txn_fee_pbd'=>$child_adl_txn_fee_pbd/$child,
     );
     }
     if(isset($fare_quote->Response->Results->FareBreakdown[2])){
       $affent_base=$fare_quote->Response->Results->FareBreakdown[2]->BaseFare;
       $affent_tax=$fare_quote->Response->Results->FareBreakdown[2]->Tax;
       $affent_yqtax=$fare_quote->Response->Results->FareBreakdown[2]->YQTax;
       $affent_adl_txn_fee_ofd=$fare_quote->Response->Results->FareBreakdown[2]->AdditionalTxnFeeOfrd;
       $affent_adl_txn_fee_pbd=$fare_quote->Response->Results->FareBreakdown[2]->AdditionalTxnFeePub;
       
       $affent=$fare_quote->Response->Results->FareBreakdown[2]->PassengerCount; 
       
        $affent_price=array(
         'base'=>$affent_base/$affent,
         'tax'=>$affent_tax/$affent,
         'yq_tax'=>$affent_yqtax/$affent,
         'adl_txn_fee_ofd'=>$affent_adl_txn_fee_ofd/$affent,
         'adl_txn_fee_pbd'=>$affent_adl_txn_fee_pbd/$affent,
         );
     }
 
 
     if(isset($fare_quote_return->Response->Results->FareBreakdown) && count($fare_quote_return->Response->Results->FareBreakdown)>0)
     {
       if(isset($fare_quote_return->Response->Results->FareBreakdown[0])){  
         $adults_base=$fare_quote_return->Response->Results->FareBreakdown[0]->BaseFare;
         $adults_tax=$fare_quote_return->Response->Results->FareBreakdown[0]->Tax;
         $adults_yqtax=$fare_quote_return->Response->Results->FareBreakdown[0]->YQTax;
         $adults_adl_txn_fee_ofd=$fare_quote_return->Response->Results->FareBreakdown[0]->AdditionalTxnFeeOfrd;
         $adults_adl_txn_fee_pbd=$fare_quote_return->Response->Results->FareBreakdown[0]->AdditionalTxnFeePub;
         
          $adults=$fare_quote_return->Response->Results->FareBreakdown[0]->PassengerCount;
          
           $adult_price_return=array(
           'base'=>$adults_base/$adults,
           'tax'=>$adults_tax/$adults,
           'yq_tax'=>$adults_yqtax/$adults,
           'adl_txn_fee_ofd'=>$adults_adl_txn_fee_ofd/$adults,
           'adl_txn_fee_pbd'=>$adults_adl_txn_fee_pbd/$adults,
       );
       }
       if(isset($fare_quote_return->Response->Results->FareBreakdown[1])){
         $child_base=$fare_quote_return->Response->Results->FareBreakdown[1]->BaseFare;
         $child_tax=$fare_quote_return->Response->Results->FareBreakdown[1]->Tax;
         $child_yqtax=$fare_quote_return->Response->Results->FareBreakdown[1]->YQTax;
         $child_adl_txn_fee_ofd=$fare_quote_return->Response->Results->FareBreakdown[1]->AdditionalTxnFeeOfrd;
         $child_adl_txn_fee_pbd=$fare_quote_return->Response->Results->FareBreakdown[1]->AdditionalTxnFeePub;
         
          $child=$fare_quote_return->Response->Results->FareBreakdown[1]->PassengerCount;
          
           $child_price_return=array(
           'base'=>$child_base/$child,
           'tax'=>$child_tax/$child,
           'yq_tax'=>$child_yqtax/$child,
           'adl_txn_fee_ofd'=>$child_adl_txn_fee_ofd/$child,
           'adl_txn_fee_pbd'=>$child_adl_txn_fee_pbd/$child,
       );
       }
       if(isset($fare_quote_return->Response->Results->FareBreakdown[2])){
         $affent_base=$fare_quote_return->Response->Results->FareBreakdown[2]->BaseFare;
         $affent_tax=$fare_quote_return->Response->Results->FareBreakdown[2]->Tax;
         $affent_yqtax=$fare_quote_return->Response->Results->FareBreakdown[2]->YQTax;
         $affent_adl_txn_fee_ofd=$fare_quote_return->Response->Results->FareBreakdown[2]->AdditionalTxnFeeOfrd;
         $affent_adl_txn_fee_pbd=$fare_quote_return->Response->Results->FareBreakdown[2]->AdditionalTxnFeePub;
         
         $affent=$fare_quote_return->Response->Results->FareBreakdown[2]->PassengerCount; 
         
          $affent_price_return=array(
           'base'=>$affent_base/$affent,
           'tax'=>$affent_tax/$affent,
           'yq_tax'=>$affent_yqtax/$affent,
           'adl_txn_fee_ofd'=>$affent_adl_txn_fee_ofd/$affent,
           'adl_txn_fee_pbd'=>$affent_adl_txn_fee_pbd/$affent,
           );
 
           
       }
     }
       
 for($i=0;$i<count($flight_listing->Response->Results[0]);$i++){
  if($flight_listing->Response->Results[0][$i]->ResultIndex==$result_index){
    $ob_selected_flight = $flight_listing->Response->Results[0][$i];
    }
 }
 for($i=0;$i<count($flight_listing->Response->Results[1]);$i++){
  if($flight_listing->Response->Results[1][$i]->ResultIndex==$result_index_return){
    $ib_selected_flight = $flight_listing->Response->Results[1][$i];
    }
 }
if(null!==($request->session()->get("ib_selected_flight")))
{
 $request->session()->forget("ib_selected_flight");
}
if(null!==($request->session()->get("selected_flight")))
{
 $request->session()->forget("selected_flight");
}
$request->session()->put("ib_selected_flight",$ib_selected_flight);
$request->session()->put("selected_flight",$ob_selected_flight);

 
 

       if(!isset($fare_quote))
       {
        return redirect()->back()->with(['error', 'Fare quote not found!!!']);   
       }
       //dd($flights);
     return view('frontend.home.flightPassengerDetailsForm')
     ->with([
         'flights'=>$ob_selected_flight,
         'ibFlights'=>$ib_selected_flight,
         'fareRule'=>$fare_rule,
         'fareRule_return'=>$fare_rule_return,
         'fareQuote'=>$fare_quote,
         'fare_quote_return'=>$fare_quote_return,
         'ssr'=>$ssr,
         'ssr_return'=>$ssr_return,
         'countries'=>$country,
         'trace_id'=>$trace_id,
         'result_index'=>$result_index,
         'affent_price'=>$affent_price,
         'adult_price'=>$adult_price,
         'child_price'=>$child_price,
         'affent_price_return'=>$affent_price_return,
         'adult_price_return'=>$adult_price_return,
         'child_price_return'=>$child_price_return,
         'is_lcc'=>$fare_quote->Response->Results->IsLCC,   
         'is_lcc_return'=>$fare_quote_return->Response->Results->IsLCC,
         'result_index_return'=>$result_index_return
         ]);
     }
     else
     {
         return redirect()->back()->withErrors(['msg', 'The Message']);
     }
     //dd($request->all());
    //$this->getPassengerDetails($request,$trace_id,$result_index,$result_index_return);
}

/*-------------------------------------------------------------------------------------------------------------|
|-------------------------Redirect to Passenger Details form for oneway and international----------------------|
|-------------------------------------------------------------------------------------------------------------*/ 
public function getPassengerDetails(Request $request,$trace_id,$result_index,$result_index_return=null){
    
    $data=$this->getFlightPassangerDetails($request,$trace_id,$result_index);
    // echo '<pre>';print_r($data);die();
         if(!isset($data) && count($data)<1)
         {
             return redirect()->back()->withErrors(['Session Expired!']);
         }
         if(!is_array($data))
         {
             return redirect('/')->withErrors(['fpderror'=>'Session Expired!']);
         }
         return view('frontend.home.flightPassengerDetailsForm')
    ->with([
        'flights'=>$data['selected_flight'],
        'fareRule'=>$data['fare_rule'],
        'fareQuote'=>$data['fare_quote'],
        'ssr'=>$data['ssr'],
        'countries'=>$data['country'],
        'trace_id'=>$data['trace_id'],
        'result_index'=>$data['result_index'],
        'affent_price'=>$data['affent_price'],
        'adult_price'=>$data['adult_price'],
        'child_price'=>$data['child_price'],
        'is_lcc'=>$data['is_lcc'],
        ]);
    

}


public function getFlightPassangerDetails($request,$trace_id,$result_index)
{
    
    $flight_listing = $request->session()->get('flight_listing_data');
    $fare_rule = $this->getFlightFareRule($result_index,$trace_id);
    $fare_quote = $this->getFlightFareQuote($result_index,$trace_id);
    $ssr = $this->getFlightSSR($result_index,$trace_id);
    $country=Country::get();
    $adult_price="";
    $child_price="";
    $affent_price="";
  if(isset($fare_quote->Response->Results->FareBreakdown) && count($fare_quote->Response->Results->FareBreakdown)>0)
  {
    if(isset($fare_quote->Response->Results->FareBreakdown[0])){  
      $adults_base=$fare_quote->Response->Results->FareBreakdown[0]->BaseFare;
      $adults_tax=$fare_quote->Response->Results->FareBreakdown[0]->Tax;
      $adults_yqtax=$fare_quote->Response->Results->FareBreakdown[0]->YQTax;
      $adults_adl_txn_fee_ofd=$fare_quote->Response->Results->FareBreakdown[0]->AdditionalTxnFeeOfrd;
      $adults_adl_txn_fee_pbd=$fare_quote->Response->Results->FareBreakdown[0]->AdditionalTxnFeePub;
      
       $adults=$fare_quote->Response->Results->FareBreakdown[0]->PassengerCount;
       
        $adult_price=array(
        'base'=>$adults_base/$adults,
        'tax'=>$adults_tax/$adults,
        'yq_tax'=>$adults_yqtax/$adults,
        'adl_txn_fee_ofd'=>$adults_adl_txn_fee_ofd/$adults,
        'adl_txn_fee_pbd'=>$adults_adl_txn_fee_pbd/$adults,
    );
    }
    if(isset($fare_quote->Response->Results->FareBreakdown[1])){
      $child_base=$fare_quote->Response->Results->FareBreakdown[1]->BaseFare;
      $child_tax=$fare_quote->Response->Results->FareBreakdown[1]->Tax;
      $child_yqtax=$fare_quote->Response->Results->FareBreakdown[1]->YQTax;
      $child_adl_txn_fee_ofd=$fare_quote->Response->Results->FareBreakdown[1]->AdditionalTxnFeeOfrd;
      $child_adl_txn_fee_pbd=$fare_quote->Response->Results->FareBreakdown[1]->AdditionalTxnFeePub;
      
       $child=$fare_quote->Response->Results->FareBreakdown[1]->PassengerCount;
       
        $child_price=array(
        'base'=>$child_base/$child,
        'tax'=>$child_tax/$child,
        'yq_tax'=>$child_yqtax/$child,
        'adl_txn_fee_ofd'=>$child_adl_txn_fee_ofd/$child,
        'adl_txn_fee_pbd'=>$child_adl_txn_fee_pbd/$child,
    );
    }
    if(isset($fare_quote->Response->Results->FareBreakdown[2])){
      $affent_base=$fare_quote->Response->Results->FareBreakdown[2]->BaseFare;
      $affent_tax=$fare_quote->Response->Results->FareBreakdown[2]->Tax;
      $affent_yqtax=$fare_quote->Response->Results->FareBreakdown[2]->YQTax;
      $affent_adl_txn_fee_ofd=$fare_quote->Response->Results->FareBreakdown[2]->AdditionalTxnFeeOfrd;
      $affent_adl_txn_fee_pbd=$fare_quote->Response->Results->FareBreakdown[2]->AdditionalTxnFeePub;
      
      $affent=$fare_quote->Response->Results->FareBreakdown[2]->PassengerCount; 
      
       $affent_price=array(
        'base'=>$affent_base/$affent,
        'tax'=>$affent_tax/$affent,
        'yq_tax'=>$affent_yqtax/$affent,
        'adl_txn_fee_ofd'=>$affent_adl_txn_fee_ofd/$affent,
        'adl_txn_fee_pbd'=>$affent_adl_txn_fee_pbd/$affent,
        );
    }
for($i=0;$i<count($flight_listing->Response->Results[0]);$i++){
  if($flight_listing->Response->Results[0][$i]->ResultIndex==$result_index){
    $selected_flight = $flight_listing->Response->Results[0][$i];
    
  }
}
if(null!==($request->session()->get("selected_flight"))){
 $request->session()->forget("selected_flight");
}
$request->session()->put("selected_flight",$selected_flight);
  
     if(!isset($fare_quote))
       {
        return redirect()->back()->withErrors(['Fare quote not find.']);   
       }
     
        $data['is_lcc']=$fare_quote->Response->Results->IsLCC;
        $data['selected_flight']=$selected_flight;
        $data['fare_rule']=$fare_rule;
        $data['fare_quote']=$fare_quote;
        $data['ssr']=$ssr;
        $data['country']=$country;
        $data['trace_id']=$trace_id;
        $data['result_index']=$result_index;
        $data['affent_price']=$affent_price;
        $data['adult_price']=$adult_price;
        $data['child_price']=$child_price;
        return $data;
        
     
    
    }
    else
    {
        return redirect()->back()->withErrors(['Session Expired']);
    }
    
}

/*--------------------------------------------------------------------------------|
|-------------------------Redirect to Booking Review------------------------------|
|--------------------------------------------------------------------------------*/ 
public function showBookingReview(Request $request){
   //dd($request->all());
/*-------------------------------Passenger form validation--------------------------------*/
    $input = $request->validate([
            'Title.*' => 'required|string',
            'FirstName.*' => 'required|distinct',
            'LastName.*' => 'required|string|different:FirstName',
            'Gender.*' => 'required|numeric|digits_between:0,3', 
            'DobDay.*'=> 'required|numeric',
            'DobMonth.*'=> 'required|numeric',
            'DobYear.*' => 'required|numeric',
            'addressLine1' => 'required',
            'mobileNo' => 'required|numeric|digits:10',
            'email' => 'required|email',
            
            ], [
            // 'Title.*.required' => 'Title is mandatory',
            // 'Title.*.string' => 'Title is mandatory',
            // 'FirstName.*.required' => 'First Name is required',
            // 'FirstName.*.distinct' => 'First Name must be distinct',
            // 'LastName.*.required' => 'Last Name is required',
            // 'LastName.*.distinct' => 'Last Name must be distinct',
            // 'Gender.*.required' => 'Gender is mandatory',
            // 'Gender.*.numeric' => 'Gender is mandatory',
            // 'Gender.*.digits_between:min,max' => 'Please select Gender',
            // 'DobDay.*.required'=> 'Day',
            // 'DobMonth.*.required'=> 'Month',
            // 'DobYear.*.required'=> 'Year',
            // 'addressLine1.required'=>'Address is required',
        ]);
/*-------------------------------Passenger form validation end--------------------------------*/
    
    $userIp=gethostbyname(trim(`hostname`));
    
    $token_id=$this->token;
    $trace_id=$request->input('trace_id');
    $result_index=$request->input('result_index');
    $result_index_return=$request->input('result_index_return');
    $is_lcc=$request->input('is_lcc');
    $is_lcc_return =$request->input('is_lcc_return');
    $paxType = $request->input('PaxType');
    $title=$request->input('Title');
    $first_name=$request->input('FirstName');
    $last_name=$request->input('LastName');
    $gender=$request->input('Gender');
    $dob_day=$request->input('DobDay');
    $dob_month=$request->input('DobMonth');
    $dob_year=$request->input('DobYear');
    $addressLine1=$request->input('addressLine1');
    $addressLine2=$request->input('addressLine2');
    $City=$request->input('City');
    $mobileNo=$request->input('mobileNo');
    $email=$request->input('email');
    $Country=$request->input('Country');
    $Nationality=$request->input('Nationality');
    $PassportNo=$request->input('PassportNo');
    $PassExpDay=$request->input('PassExpDay');
    $PassExpMonth=$request->input('PassExpMonth');
    $PassExpYear=$request->input('PassExpYear');
    $baggage=$request->input('baggage');
    $baggage_return = $request->input('baggage_ret');
    $meal = $request->input('meal');
    $meal_ret = $request->input('meal_ret');
    $meal_0_indi_1_seg_0=$request->input('meal_0_indi_1_seg_0');
    $meal_0_indi_1_seg_1=$request->input('meal_0_indi_1_seg_1');
    $Seat_0_indi_1_seg_0=$request->input('Seat_0_indi_1_seg_0');
    $Seat_0_indi_1_seg_1=$request->input('Seat_0_indi_1_seg_1');
    $baggage_selected = ($request->obbagSelected)+($request->ibbagSelected);
    $meal_platter = ($request->obmealPlatter)+($request->ibmealPlatter);
    $baggage_cost = ($request->obbagCharges)+($request->ibbagCharges);
    $meal_cost = ($request->obmealCharge)+($request->ibmealCharge);
    $bookingPrice = $request->bookingCharge;
    $userAction_0=$request->input('userAction_0');
    $CustomerID_0=$request->input('CustomerID_0');
    $gstNumber=$request->input('gstNumber');
    $gstContact=$request->input('gstContact');
    $gstEmail=$request->input('gstEmail');
    $gstName=$request->input('gstName');
    $gstaddress=$request->input('gstaddress');
    $roamerCountryCode=$request->input('roamerCountryCode');
    $roamerContact=$request->input('roamerContact');
    $PromoCode=$request->input('PromoCode');
    $baseFare = $request->input('BaseFare');
    $tax = $request->input('Tax');
    $transactionFee = $request->input('TransactionFee');
    $yqTax = $request->input('YQTax');
    $additionalTxnFeeOfrd = $request->input('AdditionalTxnFeeOfrd');
    $additionalTxnFeePub = $request->input('AdditionalTxnFeePub');
    $airTransFee = $request->input('AirTransFee');
    //Return fare details

    $baseFare_return = $request->input('BaseFareReturn');
    $tax_return = $request->input('TaxReturn');
    $transactionFee_return = $request->input('TransactionFeeReturn');
    $yqTax_return = $request->input('YQTaxReturn');
    $additionalTxnFeeOfrd_return = $request->input('AdditionalTxnFeeOfrdReturn');
    $additionalTxnFeePub_return = $request->input('AdditionalTxnFeePubReturn');
    $airTransFee_return = $request->input('AirTransFeeReturn');

    ///////////////////////////////////////


    $passanger=array();
    $passanger_return=array();
    for($i=0;$i<count($first_name);$i++)
    
    {
        $passanger[$i]['Title']=$title[$i];
        $passanger[$i]['FirstName']=$first_name[$i];
        $passanger[$i]['LastName']=$last_name[$i];
        $passanger[$i]['PaxType']=$paxType[$i];
        //$passanger[$i]['DateOfBirth']=$dob_day[$i].'/'.$dob_month[$i].'/'.$dob_year[$i];
        $passanger[$i]['Gender']=$gender[$i];
        $passanger[$i]['PassportNo']=$PassportNo[$i];
        $passanger[$i]['DateOfBirth']=$dob_year[$i].'-'.$dob_month[$i].'-'.$dob_day[$i].'T00:00:00';
        if(isset($PassExpYear[$i])&&isset($PassExpMonth[$i])&&isset($PassExpDay[$i]))
        {
        $passanger[$i]['PassportExpiry']=$PassExpYear[$i].'-'.$PassExpMonth[$i].'-'.$PassExpDay[$i].'T00:00:00';
        }
        else
        {
            $passanger[$i]['PassportExpiry']=null;
        }
        $passanger[$i]['AddressLine1']=$addressLine1;
        $passanger[$i]['AddressLine2']=$addressLine2;
        $passanger[$i]['City']=$City;
        $passanger[$i]['CountryCode']=$Country[0];
        $passanger[$i]['CountryName']=$Country[0];
        $passanger[$i]['ContactNo']=$mobileNo;
        $passanger[$i]['Email']=$email;
        if($i==0)
        $passanger[$i]['IsLeadPax']=true;
        else
        $passanger[$i]['IsLeadPax']=false;
        
        $passanger[$i]['Fare']['BaseFare']=$baseFare[$i];
        $passanger[$i]['Fare']['Tax']=$tax[$i];
        $passanger[$i]['Fare']['TransactionFee']=$transactionFee[$i];
        $passanger[$i]['Fare']['YQTax']=$yqTax[$i];
        $passanger[$i]['Fare']['AdditionalTxnFeeOfrd']=$additionalTxnFeeOfrd[$i];
        $passanger[$i]['Fare']['AdditionalTxnFeePub']=$additionalTxnFeePub[$i];
        $passanger[$i]['Fare']['AirTransFee']=$airTransFee[$i];
    }
    for($i=0;$i<count($first_name);$i++)
    
    {
        $passanger_return[$i]['Title']=$title[$i];
        $passanger_return[$i]['FirstName']=$first_name[$i];
        $passanger_return[$i]['LastName']=$last_name[$i];
        $passanger_return[$i]['PaxType']=$paxType[$i];
        //$passanger[$i]['DateOfBirth']=$dob_day[$i].'/'.$dob_month[$i].'/'.$dob_year[$i];
        $passanger_return[$i]['Gender']=$gender[$i];
        $passanger_return[$i]['PassportNo']=$PassportNo[$i];
        $passapassanger_returnnger[$i]['DateOfBirth']=$dob_year[$i].'-'.$dob_month[$i].'-'.$dob_day[$i].'T00:00:00';
        if(isset($PassExpYear[$i])&&isset($PassExpMonth[$i])&&isset($PassExpDay[$i]))
        {
        $passanger_return[$i]['PassportExpiry']=$PassExpYear[$i].'-'.$PassExpMonth[$i].'-'.$PassExpDay[$i].'T00:00:00';
        }
        else
        {
            $passanger_return[$i]['PassportExpiry']=null;
        }
        $passanger_return[$i]['AddressLine1']=$addressLine1;
        $passanger_return[$i]['AddressLine2']=$addressLine2;
        $passanger_return[$i]['City']=$City;
        $passanger_return[$i]['CountryCode']=$Country[0];
        $passanger_return[$i]['CountryName']=$Country[0];
        $passanger_return[$i]['ContactNo']=$mobileNo;
        $passanger_return[$i]['Email']=$email;
        if($i==0)
        $passanger_return[$i]['IsLeadPax']=true;
        else
        $passanger_return[$i]['IsLeadPax']=false;
        $passanger_return[$i]['Fare']['BaseFare']=$baseFare_return[$i];
        $passanger_return[$i]['Fare']['Tax']=$tax_return[$i];
        $passanger_return[$i]['Fare']['TransactionFee']=$transactionFee_return[$i];
        $passanger_return[$i]['Fare']['YQTax']=$yqTax[$i];
        $passanger_return[$i]['Fare']['AdditionalTxnFeeOfrd']=$additionalTxnFeeOfrd_return[$i];
        $passanger_return[$i]['Fare']['AdditionalTxnFeePub']=$additionalTxnFeePub_return[$i];
        $passanger_return[$i]['Fare']['AirTransFee']=$airTransFee[$i];
    }


     $fare_rule = $this->getFlightFareRule($result_index,$trace_id);
     $fare_rule_return = $this->getFlightFareRule($result_index_return,$trace_id);
    $flight_listing = $request->session()->get('flight_listing_data');
    
    for($i=0;$i<count($flight_listing->Response->Results[0]);$i++){
      if($flight_listing->Response->Results[0][$i]->ResultIndex==$result_index){
        $ob_selected_flight = $flight_listing->Response->Results[0][$i];
        }
     }
    if(isset($flight_listing->Response->Results[1])){
        for($i=0;$i<count($flight_listing->Response->Results[1]);$i++){
          if($flight_listing->Response->Results[1][$i]->ResultIndex==$result_index_return){
            $ib_selected_flight = $flight_listing->Response->Results[1][$i];
            }
         }
     }
    if(null!==($request->session()->get("ib_selected_flight")))
    {
     $request->session()->forget("ib_selected_flight");
    }
    if(null!==($request->session()->get("selected_flight")))
    {
     $request->session()->forget("selected_flight");
    }
    if(isset($ib_selected_flight)){
    $request->session()->put("ib_selected_flight",$ib_selected_flight);
    }
    $request->session()->put("selected_flight",$ob_selected_flight);

    $ib_selectedFlight = $request->session()->get("ib_selected_flight");
    $selectedFlight = $request->session()->get('selected_flight');
    $request->session()->put('flight_passenger_data',$passanger);
    $request->session()->put('booking_price',$bookingPrice);
    if(isset($passanger_return))
    {
    $request->session()->put('flight_passenger_data_return',$passanger_return);    
    }
  if(!isset($passanger))
  {
      return redirect()->back()->withErrors(['Passenger Details not found']);
  }
  
    //dd($selectedFlight);
   //dd($ib_selectedFlight);
  // dd($meal_cost);
  
  return view('frontend.home.flightBookingReview')
  ->with([
      'passenger'=>$passanger,
      'passengerReturn'=>$passanger_return,
      'flights'=>$selectedFlight,
      'ibFlights'=>$ib_selectedFlight,
      'fareRule'=>$fare_rule,
      'fareRule_return'=>$fare_rule_return,
      'baggage_selected'=>$baggage_selected,
      'meal_platter'=>$meal_platter,
      'baggage_cost'=>$baggage_cost,
      'meal_cost'=>$meal_cost,
      'trace_id'=>$trace_id,
      'result_index'=>$result_index,
      'result_index_return'=>$result_index_return,
      'is_lcc'=>$is_lcc,
      'is_lcc_return'=>$is_lcc_return
      ]);  
}


public function getAirportDetails()
{
$err_upTmpName = 'AIRPORT-CITY-COUNTRY DETAILS.csv';
$row = 0;
$list=array();
$count=0;
if (($handle = fopen($err_upTmpName, "r")) !== FALSE) {
    
	while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {

		if($row == 0){
			$row++;
		} else {

			// $data[0] = first name; $data[1] = last name; $data[2] = email; $data[3] = phone
			/*********************************************************************************************************************/
			if(!empty($data[0]) && !empty($data[1])&& ($data[5]=='TH'||$data[5]=='IN'))
      {
        $list[$count]['AirportName']=$data[0];
        $list[$count]['AirportCode']=$data[1];
        $list[$count]['CityName']=$data[2];
        $list[$count]['CityCode']=$data[3];
        $list[$count]['CountryName']=$data[4];
        $list[$count]['CountryCode']=$data[5];
        $list[$count]['Nationality']=$data[6];
        $list[$count]['Currency']=$data[7];
      }
      $count++;
		}

	}

} else {

	echo 'File could not be opened.';
}

fclose($handle);
return $list;
}



public function getHotelDestinations()
{
    
$err_upTmpName = 'TBO_Hotel_City_List-Sheet1.csv';
$row = 0;
$list=array();
$count=0;
if (($handle = fopen($err_upTmpName, "r")) !== FALSE) {
    
	while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {

		if($row == 0){
			$row++;
		} else {

			// $data[0] = first name; $data[1] = last name; $data[2] = email; $data[3] = phone
			/*********************************************************************************************************************/
			if(!empty($data[0]) && !empty($data[1])&& ($data[5]=='TH'||$data[5]=='IN'))
      {
        $list[$count]['CityId']=$data[0];
        $list[$count]['CityName']=$data[1];
        // $list[$count]['CityName']=$data[2];
        // $list[$count]['CityCode']=$data[3];
        // $list[$count]['CountryName']=$data[4];
        // $list[$count]['CountryCode']=$data[5];
        // $list[$count]['Nationality']=$data[6];
        // $list[$count]['Currency']=$data[7];
      }
      $count++;
		}

	}

} else {

	echo 'File could not be opened.';
}

fclose($handle);
//dd($list);
return $list;
    
}




public function getFlightCities()
{
    $cities=$this->getAirportDetails();
    $flightCities=array();
    foreach ($cities as $city)
    {
        $flightCities[$city['CityName']]=$city['CityCode'];
    }
    return $flightCities;
}

public function getAuthentication()
{
    $userIp=gethostbyname(trim(`hostname`));
    $clientId = config('constants.TEST.CREDENTIALS.CLIENT_ID');
    $userName = config('constants.TEST.CREDENTIALS.USER_NAME');
    $password = config('constants.TEST.CREDENTIALS.PASSWORD');
    $userIp = gethostbyname(trim(`hostname`));
    $service_url = config('constants.TEST.AUTHENTICATE');
    $data=array('ClientId'=>$clientId,'UserName'=>$userName,'Password'=>$password,'EndUserIp'=>$userIp);
    
    $decoded = $this->apiCalls($data,$service_url);
    // echo'<pre>';print_r($decoded);die();
    $token_id=$decoded->TokenId;
    $token_data=new Token();
    $token_data->token=$token_id;
    $token_data->save();
   // dd($decoded);
    //return $decoded;
}
public function getCities()
{
    $userIp=gethostbyname(trim(`hostname`));
    
        $clientId="ApiIntegrationNew";       
        $token_id=$this->token;
        $country_code='TH';
        $city_data=array('ClientId'=>$clientId,'EndUserIp'=>$userIp,'TokenId'=>$token_id,'CountryCode'=>$country_code);
        
        $city_url="http://api.tektravels.com/SharedServices/SharedData.svc/rest/DestinationCityList";
        $decoded = $this->apiCalls($city_data,$city_url);
        
        return $decoded;
}
public function getFlightCalenderFare()
{

}
public function getFlightFareRule($result_index,$trace_id)
{
    $userIp=gethostbyname(trim(`hostname`));
    $token_id=$this->token;
    
    $data=array('EndUserIp'=>$userIp,'TokenId'=>$token_id,'TraceId'=>$trace_id,'ResultIndex'=>$result_index);
        $url = config('constants.LIVE.FLIGHT.FARE_RULE');
        
        $decoded = $this->apiCalls($data,$url);
       
        return $decoded;

}
public function getFlightFareQuote($result_index,$trace_id)
{
    $userIp=gethostbyname(trim(`hostname`));
    
    $token_id=$this->token;
    
    $data=array('EndUserIp'=>$userIp,'TokenId'=>$token_id,'ResultIndex'=>$result_index,'TraceId'=>$trace_id);
        $url=config('constants.LIVE.FLIGHT.FARE_QUOTE');
        
        $decoded = $this->apiCalls($data,$url);
        return $decoded;

}
public function getFlightSSR($result_index,$trace_id){
    $userIp = gethostbyname(trim(`hostname`));
    
    $token_id=$this->token;
    
    $data=array('EndUserIp'=>$userIp,'TokenId'=>$token_id,'ResultIndex'=>$result_index,'TraceId'=>$trace_id);
        $url = config('constants.LIVE.FLIGHT.SSR');
        
        $decoded = $this->apiCalls($data,$url);
       // dd($decoded);
        return $decoded;
}
//Hold the Non-LCC flights
public function holdFlightBooking(Request $request,$trace_id,$result_index,$passenger_data,$result_index_return=null)
{
        
    $userIp = $this->userIp;
    
    $token_id = $this->token;
    //dd($passenger_data);
    $data=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'ResultIndex'=>$result_index,
        'TraceId'=>$trace_id,
        'Passengers'=>$passenger_data,
    );
        $url = config('constants.LIVE.FLIGHT.BOOK');
        $decoded = $this->apiCalls($data,$url);
        return $decoded;
}
// Ticket the Non-LCC Flights
public function getFlightTicketNonLcc(Request $request,$trace_id,$pnr,$booking_id)
{
    $userIp=gethostbyname(trim(`hostname`));
    
    $token_id=$this->token;
    $data=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'TraceId'=>$trace_id,
        'PNR'=>$pnr,
        'BookingId'=>$booking_id,
    );
        $url = config('constants.LIVE.FLIGHT.TICKET');
      
        $decoded = $this->apiCalls($data,$url);
        return $decoded;
}

//direct ticket Non-LCC
public function ticketFlightBooking(Request $request,$trace_id,$result_index,$result_index_return=null)
{
$passenger_data = $request->session()->get('flight_passenger_data');
$response=$this->holdFlightBooking($request,$trace_id,$result_index,$passenger_data);
dd($response);
$return_passanger_data=$request->session()->get('flight_passenger_data_return');
$pnr=$response->Response->Response->PNR;
$booking_id=$response->Response->Response->BookingId;
$session_data=array(
        array(
            'trace_id'=>$trace_id,
            'pnr'=>$pnr,
            'booking_id'=>$booking_id
            )
            
    );
if(isset($return_passanger_data))
{
$response_return=$this->holdFlightBooking($request,$trace_id,$result_index_return,$return_passanger_data);
    if(isset($response_return->Response->Response->PNR))
    {
        $pnr_return=$response_return->Response->Response->PNR;
        $booking_id_return=$response_return->Response->Response->BookingId;
        $session_data=array(
            array(
                'trace_id'=>$trace_id,
                'pnr'=>$pnr,
                'booking_id'=>$booking_id
            ),
                array(
                    'trace_id'=>$trace_id,
                    'pnr'=>$pnr_return,
                    'booking_id'=>$booking_id_return
                    )
        );
    }
    

}




    
$request->session()->put('non_lcc_flight_booking_data',$session_data);
//$request->session()->put('booking_data',$data);
$amount = $request->session()->get('booking_price');
$tid=rand(100000,9999999);
$parameters = [
      
    'tid' => $tid,
    'order_id' =>$result_index,
     'amount' => $amount,
    //'amount' => '1',  
  ];
  
  // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker
  
  $order = Payment::gateway('CCAvenue')->prepare($parameters);
  return Payment::process($order);
//$booking_info=$this->getFlightTicketNonLcc($request,$trace_id,$pnr,$booking_id);
/*--------------------------------------Proceed without payment------------------------------------*/
 $non_lcc_data=$request->session()->get('non_lcc_flight_booking_data');
  
            if(isset($non_lcc_data))
            {
                $data=$request->session()->get('non_lcc_flight_booking_data');

                $data_return=$request->session()->get('non_lcc_flight_booking_data_return');
                
                $booking_info=$this->getFlightTicketNonLcc($request,$data['trace_id'],$data['pnr'],$data['booking_id']);
                
                

                //dd(booking_info);
                $flight_booking_details = $this->getFlightBookingDetail($data['booking_id'],$data['pnr'],$data['trace_id']);
               // dd($flight_booking_details);
               if(!isset($flight_booking_details))
              {
                  return redirect()->back()->withErrors(['Flight booking details not found']);
              }
               
                    return view('frontend.home.flightConfirmation')
                    ->with([
                    'fb_cnf'=>$flight_booking_details
                    ]);
                
            }
}

public function doneHoldBooking(Request $request,$trace_id,$result_index,$result_index_return=null)
{
    $response=$this->holdFlightBooking($request,$trace_id,$result_index);
    $pnr=$response->Response->Response->PNR;
    $booking_id=$response->Response->Response->BookingId;
    if($result_index_return!=null)
    {
    $response_return=$this->holdFlightBooking($request,$trace_id,$result_index_return);
    $pnr=$response->Response->Response->PNR;
    $booking_id=$response->Response->Response->BookingId;
    }
    $flight_booking_details = $this->getFlightBookingDetail($booking_id,$pnr,$trace_id);
               //var_dump($flight_booking_details);
               //die;
               if(empty($flight_booking_details) && count($flight_booking_details)<1)
              {
                  return redirect()->back()->withErrors(['Flight booking details not found']);
              }
                    return view('frontend.home.flightConfirmation')
                    ->with([
                    'flight_bookingCnf'=>$flight_booking_details
                    ]);
}
//Booking for LCC FLights
public function ticketLccFlightBooking(Request $request,$trace_id,$result_index,$result_index_return=null)
{
    $passenger_data = $request->session()->get('flight_passenger_data');    
   
    $userIp = gethostbyname(trim(`hostname`));
    
    $token_id=$this->token;
    $data[0]=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'ResultIndex'=>$result_index,
        'TraceId'=>$trace_id,
        'Passengers'=>$passenger_data,
    );
    $return_passanger_data=$request->session()->get('flight_passenger_data_return');
    if(isset($return_passanger_data))
    {
    $data[1]=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'ResultIndex'=>$result_index_return,
        'TraceId'=>$trace_id,
        'Passengers'=>$return_passanger_data,

    );
}
//echo $data_return;
    $request->session()->put('lcc_flight_booking_data',$data);
    $amount = $request->session()->get('booking_price');
    $tid=rand(100000,9999999);
    $parameters = [
          
        'tid' => $tid,
        'order_id' =>$result_index,
        'amount' => $amount,
        //'amount' => '1',  
      ];
      
      // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker
      
      $order = Payment::gateway('CCAvenue')->prepare($parameters); 
      return Payment::process($order);
    /*-------------------------Proceed without payment--------------------------*/
    $lcc_data=$request->session()->get('lcc_flight_booking_data');
   if(isset($lcc_data))
            {
                    $data=$request->session()->get('lcc_flight_booking_data');
                    $response=$this->doneLccFlightBooking($data);  
                    //echo json_encode($data);
                   // dd($response);
                    if(isset($data_return))
                    {
                        //echo json_encode($data_return);
                    $response_return=$this->doneLccFlightBooking($data_return);  
                    }
                    //var_dump($response);
                    //dd($response);
                    //echo $response->Response->Response->BookingId;
                    $flight_booking_details = $this->getFlightBookingDetail($response->Response->Response->BookingId,$response->Response->Response->PNR,$response->Response->TraceId);
                   // dd($flight_booking_details);
                   if(empty($flight_booking_details) && count($flight_booking_details)<1)
              {
                  return redirect()->back()->withErrors(['Flight booking details not found']);
              }
                    return view('frontend.home.flightConfirmation')
                    ->with([
                    'fb_cnf'=>$flight_booking_details
                    ]);
            }

}

public function doneLccFlightBooking($data)

{
        $url = config('constants.LIVE.FLIGHT.TICKET');
        $decoded = $this->apiCalls($data,$url);
        //dd($decoded);
        return $decoded;
 
}

public function getFlightBookingDetail($booking_id,$pnr,$trace_id)
{
    $userIp=gethostbyname(trim(`hostname`));
    
    $token_id=$this->token;
    
    $data=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'BookingId'=>$booking_id,
        'PNR'=>$pnr,
        'TraceId'=>$trace_id,
         );
        $url = config('constants.LIVE.FLIGHT.DETAILS');
        $curl_response=$this->apiCalls($data,$url);
        return $curl_response;
}
public function releasePnrRequest()
{
    $userIp=gethostbyname(trim(`hostname`));
    
    $token_id=$this->token;
    
    $data=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'BookingId'=>$booking_id,
        'Source'=>$source,    
    );
        $url = config('constants.LIVE.FLIGHT.RELEASE_PNR');
      
        $decoded = $this->apiCalls($data,$url);
        return $decoded;
}


public function hotelInfo(Request $request,$trace_id,$result_index,$hotel_code)
{
    $userIp=gethostbyname(trim(`hostname`));
    $token_id=$this->token;
    //Get Hotel Info
    $data=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'TraceId'=>$trace_id,
        'ResultIndex'=>$result_index,    
        'HotelCode'=>$hotel_code,    
    );
    $url=config('constants.LIVE.HOTELS.HOTEL_INFO');
    $hotel_info = $this->apiCalls($data,$url);
    return $hotel_info;
}

public function getHotelInfo(Request $request,$trace_id,$result_index,$hotel_code)
{
            $token_id=$this->token;
            $hotel_info = $this->hotelInfo($request,$trace_id,$result_index,$hotel_code);
            $comb=array();
            $room_info=array();
            //dd($hotel_info);
             if(empty($hotel_info) && (count($hotel_info)<1))
              {
                  return redirect()->back()->withErrors(['Hotel details not found']);
              }
            if($hotel_info->HotelInfoResult->ResponseStatus===1)
            {
                $room_info=$this->getHotelRoomInfo($token_id,$trace_id,$result_index,$hotel_code);
                
                $hotel_search_data=$request->session()->get('hotel_search_data');
               
                $comb=$this->roomCombination($room_info,$hotel_search_data['NoOfRooms']);
                
            }
            
        
        //dd($room_info);
           if(empty($room_info) && (count($room_info)<1))
              {
                  return redirect()->back()->withErrors(['Room details not found. Session may be expired']);
              }  
        return view('frontend.home.hotel-detail')->with([
        'comb'=>$comb,
        'hotel_info'=>$hotel_info,
        'room_info'=>$room_info,
        'trace_id'=>$trace_id,
        'result_index'=>$result_index,
        'no_of_rooms'=>$hotel_search_data['NoOfRooms']
        ]);
}
public function blockHotelRoom(Request $request)
{   $hotel_search_data=$request->session()->get('hotel_search_data');
//dd($request->all());
    $userIp=gethostbyname(trim(`hostname`));
    $token_id=$this->token;
    if($request->isMethod('post'))
    {
    $combination=explode('-',$request->input('combination'));
    $trace_id=$request->input('trace_id');
    $result_index=$request->input('result_index');
    $hotel_name=$request->input('HotelName');
    $hotel_code=$request->input('HotelCode');    
    $room_info=$this->getHotelRoomInfo($token_id,$trace_id,$result_index,$hotel_code);
    $block_room_info = $this->getBlockHotelRoom($room_info,$hotel_search_data,$token_id,$combination,$trace_id,$result_index,$hotel_name,$hotel_code);
    $request->session()->put('room_info',$room_info);
    $request->session()->put('block_room_info',$block_room_info);
    $request->session()->put('trace_id',$trace_id);
    $request->session()->put('hotel_name',$hotel_name);
    $request->session()->put('hotel_code',$hotel_code);
    $request->session()->put('result_index',$result_index);

    }
    else
    {
        $block_room_info=$request->session()->get('block_room_info');
        $room_info=$request->session()->get('room_info');
        $trace_id=$request->session()->get('trace_id');
        $hotel_name=$request->session()->get('hotel_name');
        $hotel_code=$request->session()->get('hotel_code');
        $result_index=$request->session()->get('result_index');

        
    }
    //  dd($block_room_info);
    //dd($block_room_info);
    //echo $request->input('combination');
    if($block_room_info->BlockRoomResult->ResponseStatus==1 && $block_room_info->BlockRoomResult->AvailabilityType==="Confirm")
    {
      //dd($block_room_info);  
    return view('frontend.home.hotel_booking')
    ->with([
        'block_room_info'=>$block_room_info,    
        'search_info'=>$hotel_search_data,
        'hotel_code'=>$hotel_code,
        'trace_id'=>$trace_id,
        'result_index'=>$result_index,
        ]);
    }
    else if($block_room_info->BlockRoomResult->ResponseStatus==1 && $block_room_info->BlockRoomResult->AvailabilityType==="Available")
    {
        $msg = "Room Availability is not confirmed";
        $request->session()->flash('flash_error',$msg);
     return redirect()->back()->with('error',$msg);   
    }

    else 
    {
        $msg = $block_room_info->BlockRoomResult->Error->ErrorMessage;
        $request->session()->flash('flash_error',$msg);
     return redirect()->back()->with('error',$block_room_info->BlockRoomResult->Error->ErrorMessage);   
    }
}

public function getBlockHotelRoom($room_info,$hotel_search_data,$token_id,$combination,$trace_id,$result_index,$hotel_name,$hotel_code)
{
    //dd($combination);
    $userIp=gethostbyname(trim(`hostname`));
    
    $price=array();
    $hotel_room=array();
    
    $i=0;
    foreach($room_info->GetHotelRoomResult->HotelRoomsDetails as $rm)
    {
        if(in_array($rm->RoomIndex,$combination))
        {
        $price[$i]['CurrencyCode']=$rm->Price->CurrencyCode;
        $price[$i]['RoomPrice']=$rm->Price->RoomPrice;
        $price[$i]['Tax']=$rm->Price->Tax;
        $price[$i]['ExtraGuestCharge']=$rm->Price->ExtraGuestCharge;
        $price[$i]['ChildCharge']=$rm->Price->ChildCharge;
        $price[$i]['OtherCharges']=$rm->Price->OtherCharges;
        $price[$i]['Discount']=$rm->Price->Discount;
        $price[$i]['PublishedPrice']=$rm->Price->PublishedPrice;
        $price[$i]['PublishedPriceRoundedOff']=$rm->Price->PublishedPriceRoundedOff;
        $price[$i]['OfferedPrice']=$rm->Price->OfferedPrice;
        $price[$i]['OfferedPriceRoundedOff']=$rm->Price->OfferedPriceRoundedOff;
        $price[$i]['AgentCommission']=$rm->Price->AgentCommission;
        $price[$i]['AgentMarkUp']=$rm->Price->AgentMarkUp;
        $price[$i]['ServiceTax']=$rm->Price->ServiceTax;
        $price[$i]['TDS']=$rm->Price->TDS;
       
        $hotel_room[$i]['RoomIndex']=$rm->RoomIndex;
        $hotel_room[$i]['RatePlanCode']=$rm->RatePlanCode;
        $hotel_room[$i]['RoomTypeCode']=$rm->RoomTypeCode;
        $hotel_room[$i]['RoomTypeName']=$rm->RoomTypeName;
        $hotel_room[$i]['BedTypeCode']=null;
        $hotel_room[$i]['SmokingPreference']=0;
        $hotel_room[$i]['Supplements']=null;
        $hotel_room[$i]['Price']=$price[$i];                      
        $i++;
        }
    }




        //dd($hotel_room);
    $data=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'TraceId'=>$trace_id,
        'ResultIndex'=>$result_index,    
        'HotelCode'=>$hotel_code,
        'HotelName'=>$hotel_name,
        'GuestNationality'=>'IN',
        'NoOfRooms'=>$hotel_search_data['NoOfRooms'],
        'ClientReferenceNo'=>'0',
        'IsVoucherBooking'=>true,
        'HotelRoomsDetails'=>$hotel_room,
    );
    //dd($data);
    
      $url=config('constants.LIVE.HOTELS.BLOCK_ROOM');

    
    $block_room_info = $this->apiCalls($data,$url);

    return $block_room_info;

}

public function bookHotelRoom(Request $request)
{
    $search_info=$request->session()->get('hotel_search_data');
    $userIp=gethostbyname(trim(`hostname`));
    $token_id=Token::orderBy('id', 'desc')->first()->token;
    $CurrencyCode=$request->input('CurrencyCode');
    $RoomPrice=$request->input('RoomPrice');
    $Tax=$request->input('Tax');
    $ExtraGuestCharge=$request->input('ExtraGuestCharge');
    $ChildCharge=$request->input('ChildCharge');
    $OtherCharges=$request->input('OtherCharges');
    $Discount=$request->input('Discount');
    $PublishedPrice=$request->input('PublishedPrice');
    $PublishedPriceRoundedOff=$request->input('PublishedPriceRoundedOff');
    $OfferedPrice=$request->input('OfferedPrice');
    $OfferedPriceRoundedOff=$request->input('OfferedPriceRoundedOff');
    $AgentCommission=$request->input('AgentCommission');    
    $AgentMarkUp=$request->input('AgentMarkUp');
    $ServiceTax=$request->input('ServiceTax');
    $TDS=$request->input('TDS');
    $trace_id=$request->input('trace_id');
    $result_index=$request->input('result_index');
    $hotel_name=$request->input('HotelName');
    $hotel_code=$request->input('HotelCode');
    $RoomIndex=$request->input('RoomIndex');
    $RatePlanCode=$request->input('RatePlanCode');
    $RoomTypeCode=$request->input('RoomTypeCode');
    $RoomTypeName=$request->input('RoomTypeName');
    $BedTypes=$request->input('BedTypes');
    $SmokingPreference=$request->input('SmokingPreference');
    $title_adult=$request->input("titleAdult");
    $first_name_adult=$request->input("firstNameAdult");
    $last_name_adult=$request->input("lastNameAdult");
    $title_child=$request->input("titleChild");
    $first_name_child=$request->input("firstNameChild");
    $last_name_child=$request->input("lastNameChild");
    $age_child=$request->input("ageChild");
    $hotelBookingPrice=$request->bookingCharge;
    $guest_email=$request->guest_email;
    $user_email=$request->user_email;
    
    $request->session()->put('booking_price',$hotelBookingPrice);
    
    $price=array();
    for($i=0;$i<count($RoomPrice);$i++)
    {
        $price[$i]['CurrencyCode']=$CurrencyCode[$i];
        $price[$i]['RoomPrice']=$RoomPrice[$i];
        $price[$i]['Tax']=$Tax[$i];
        $price[$i]['ExtraGuestCharge']=$ExtraGuestCharge[$i];
        $price[$i]['ChildCharge']=$ChildCharge[$i];
        $price[$i]['OtherCharges']=$OtherCharges[$i];
        $price[$i]['Discount']=$Discount[$i];
        $price[$i]['PublishedPrice']=$PublishedPrice[$i];
        $price[$i]['PublishedPriceRoundedOff']=$PublishedPriceRoundedOff[$i];
        $price[$i]['OfferedPrice']=$OfferedPrice[$i];
        $price[$i]['OfferedPriceRoundedOff']=$OfferedPriceRoundedOff[$i];
        $price[$i]['AgentCommission']=$AgentCommission[$i];
        $price[$i]['AgentMarkUp']=$AgentMarkUp[$i];
        $price[$i]['ServiceTax']=$ServiceTax[$i];
        $price[$i]['TDS']=$TDS[$i];
    }
    $passangers=array();
    $adults=array();
    $child=array();
    for($i=0;$i<count($first_name_adult);$i++)
    {
                    $adults[$i]['Title']=$title_adult[$i];
                    $adults[$i]['FirstName']=$first_name_adult[$i];
                    $adults[$i]['LastName']=$last_name_adult[$i];
                    $adults[$i]['PaxType']=1;
                    if($i==0)
                    {
                    $adults[$i]['LeadPassenger']=true;
                    }
                    else
                    {
                        $adults[$i]['LeadPassenger']=true;
                    }
    }
    if(isset($first_name_child))
    {
        for($i=0;$i<count($first_name_child);$i++)
        {
            $child[$i]['Title']=$title_child[$i];
            $child[$i]['FirstName']=$first_name_child[$i];
            $child[$i]['LastName']=$last_name_child[$i];
            $child[$i]['PaxType']=2;
            $child[$i]['LeadPassenger']=false;
            $child[$i]['Age']=$age_child[$i];


        }
    }

    $i=0;
    $k=0;
    $a=0;
    $c=0;
    foreach($search_info['RoomGuests'] as $guests)
    {
	for($j=0;$j<$guests['NoOfAdults'];$j++)    
        {
        $passangers[$i][$k]=$adults[$a];
        $a++;
        $k++;
        }

        for($j=0;$j<$guests['NoOfChild'];$j++)    
        {
        $passangers[$i][$k]=$child[$c];
        $c++;
        $k++;
        }
        $i++;
        $k=0;
    }
 
    //dd($passangers);
    $hotel_room=array();
        for($i=0;$i<count($RoomIndex);$i++)
        {
        $hotel_room[$i]['RoomIndex']=$RoomIndex[$i];
        $hotel_room[$i]['RatePlanCode']=$RatePlanCode[$i];
        $hotel_room[$i]['RoomTypeCode']=$RoomTypeCode[$i];
        $hotel_room[$i]['RoomTypeName']=$RoomTypeName[$i];
        $hotel_room[$i]['BedTypeCode']=null;
        $hotel_room[$i]['SmokingPreference']=0;
        $hotel_room[$i]['Supplements']=null;
        $hotel_room[$i]['Price']=$price[$i];                      
        $hotel_room[$i]['HotelPassenger']=$passangers[$i];                      
        }
        

    $data=array(
    'EndUserIp'=>$userIp,
    'TokenId'=>$token_id,
    'TraceId'=>$trace_id,
    'ResultIndex'=>$result_index,
    'HotelCode'=>$hotel_code,
    'HotelName'=>$hotel_name,
    'GuestNationality'=>'IN',
    'NoOfRooms'=>$search_info['NoOfRooms'],
    'IsVoucherBooking'=>true,
    'HotelRoomsDetails'=>$hotel_room,
);

$request->session()->put('booking_data',$data);

$tid=rand(100000,9999999);
$parameters = [
      
    'tid' => $tid,
    'order_id' =>$result_index,
     'amount' => $hotelBookingPrice,
    //'amount' => '501', 
    //can also get by $request->session()->get('booking_price')  
  ];
  
  // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker
  
  $order = Payment::gateway('CCAvenue')->prepare($parameters);
  return Payment::process($order);

}

public function getHotelRoomInfo($token_id,$trace_id,$result_index,$hotel_code)
{
    $userIp=gethostbyname(trim(`hostname`));
    $data=array(
        'EndUserIp'=>$userIp,
        'TokenId'=>$token_id,
        'TraceId'=>$trace_id,
        'ResultIndex'=>$result_index,    
        'HotelCode'=>$hotel_code,    
    );
    $url=config('constants.LIVE.HOTELS.ROOM_INFO');
    
    $room_info = $this->apiCalls($data,$url);
    //dd($room_info);
    return $room_info;
}
public function bookingResponse(Request $request)
{
            $package_booking_data=$request->session()->get('package_booking_data');
            $booking_data=$request->session()->get('booking_data');
            $lcc_data=$request->session()->get('lcc_flight_booking_data');
            $non_lcc_data=$request->session()->get('non_lcc_flight_booking_data');
            $ob_non_lcc=$request->session()->get('ob_non_lcc');
            $ib_non_lcc=$request->session()->get('ib_non_lcc');
            
			$response = Payment::gateway('CCAvenue')->response($request);
			//dd($response);
			$response_order_id=$response['order_id'];
			$response_category=array();
			if(strpos($response_order_id,"ACT")!==false)
			{
			$response_category= explode('-',$response_order_id);
			}
			if(access()->user())
			    {
			        $userid = access()->user()->id;
			    }
			    else
			    {
			        $userid = '';
			    }
	    	/*--------------------------------------------------------------------------------|
            |-----------------------------Store Trasaction Details----------------------------|
            |--------------------------------------------------------------------------------*/
        if($response['order_status'])
        // if($response['order_status']=='Success')
        {
			$customer_transaction = array(
                    'txn_id'=>$response['bank_ref_no'], 
                    'status'=>$response['order_status'],
                    'amount'=>$response['amount'],
                    );
			$transaction_id = DB::table('transaction')->insertGetId($customer_transaction); 
			if(isset($response_category) && count($response_category)>0)
			{
			    if($response_category[0]=="ACT"){
			    
			     $customer_booking=array(
			          'user_id'=>$userid,
                      'booking_id'=>$response_category[1],
                      'booking_type'=>"Activity",
                      'transaction_id'=>$transaction_id, 
				);
				  $insert_booking = DB::table('customer_bookings')->insert($customer_booking); 
			   }
			}
			/*--------------------------------------------------------------------------------|
            |-----------------------------Package Booking-------------------------------------|
            |--------------------------------------------------------------------------------*/		
           if(isset($package_booking_data))
            {
				               
                $url=config('constants.LIVE.HOTELS.BOOK');
                $data=$package_booking_data['hotels'];
                $book_room_info=array();
                $hotels=0;
                foreach($data as $booking_data)
                {
                    $book_room_info = $this->apiCalls($booking_data,$url);          
					$bookingInfo =$this->getBookingDetails($book_room_info,$booking_data['TokenId'],$booking_data['TraceId']);
                  $customer_booking=array(
                      'booking_id'=>$book_room_info->BookResult->BookingId,
                      'booking_type'=>"Package-Hotels",
					  'booking_detail'=>json_encode($bookingInfo),
					  'transaction_id'=>$transaction_id, 
                  );
                
                $insert_booking = DB::table('customer_bookings')->insert($customer_booking);    
                }
				$bookinInfo=[];
				$booking_id="";
				$flightData=$package_booking_data['flights'];
				if(!$package_booking_data['is_lcc'])
				{
                    $booking_info=$this->getFlightTicketNonLcc($request,$flightData['trace_id'],$flightData['pnr'],$flightData['booking_id']);
                    
                    $bookingInfo = $this->getFlightBookingDetail($booking_info->Response->Response->BookingId,$booking_info->Response->Response->PNR,$booking_info->Response->TraceId);
                    $booking_id=$booking_info->Response->Response->BookingId;
					
				}
				else
				{
					
                    $lcc_response=$this->doneLccFlightBooking($flightData);
                    
                    if(isset($lcc_response) && $lcc_response->Response->ResponseStatus==1)
                    {
                    $bookingInfo = $this->getFlightBookingDetail($lcc_response->Response->Response->BookingId,$lcc_response->Response->Response->PNR,$lcc_response->Response->TraceId);
					$booking_id=$lcc_response->Response->Response->BookingId;    
                    }
                    else
                    {
                        return redirect('/');
                    }
                    
                
                
					
				}
				$customer_booking=array(
                      'booking_id'=>$booking_id,
                      'booking_type'=>"Package-Flights",
					  'booking_detail'=>json_encode($bookingInfo),
					  'transaction_id'=>$transaction_id, 
                  );
				  $insert_booking = DB::table('customer_bookings')->insert($customer_booking);
				
				
                
            }
            /*--------------------------------------------------------------------------------|
            |-----------------------------Hotels Booking--------------------------------------|
            |--------------------------------------------------------------------------------*/
            if(isset($booking_data))
            {
                $data=$search_info=$request->session()->get('booking_data');  
                
                $url=config('constants.LIVE.HOTELS.BOOK');
                
                $book_room_info = $this->apiCalls($data,$url);
                
                  /*-----------------------------------------------------------------|
                  |----------------------Get Booking details-------------------------|
                  |-----------------------------------------------------------------*/
                
                $booking_curl_response =$this->getBookingDetails($book_room_info,$data['TokenId'],$data['TraceId']);
                $bookingInfo = $booking_curl_response;
                //dd($bookingInfo);
                  $customer_booking=array(
                      'booking_id'=>$book_room_info->BookResult->BookingId,
                      'booking_type'=>"Hotel",
                      'booking_detail'=>json_encode($booking_curl_response),
                      'transaction_id'=>$transaction_id, 
                  );
                if(empty($bookingInfo) && count($bookingInfo)<1)
              {
                  return redirect()->back()->withErrors(['Booking details not found']);
              }
                $insert_booking = DB::table('customer_bookings')->insert($customer_booking);
                 return view('frontend.home.hotel_voucher')
                 ->with([
                     'bookingInfo'=>$bookingInfo,
                    ]);
            }
            /*--------------------------------------------------------------------------------|
            |-----------------------------Flight Booking LCC----------------------------------|
            |--------------------------------------------------------------------------------*/
            if(isset($lcc_data))
            {       
                    $data=$request->session()->get('lcc_flight_booking_data');
                    $lcc_response=array();
                    $bookingInfo=array();
                    $i=0;
                    $booking_id="";
                    foreach($data as $flightData)
                    {
                    $lcc_response[$i]=$this->doneLccFlightBooking($flightData);
                    if(isset($lcc_response[$i]) && $lcc_response[$i]->Response->ResponseStatus==1)
                    {
                    $bookingInfo[$i]  = $this->getFlightBookingDetail($lcc_response[$i]->Response->Response->BookingId,$lcc_response[$i]->Response->Response->PNR,$lcc_response[$i]->Response->TraceId);    
                        if($i>0)
                    {
                        $booking_id.='-'.$lcc_response[$i]->Response->Response->BookingId;
                    }
                    else
                    {
                        $booking_id=$lcc_response[$i]->Response->Response->BookingId;
                    }
                    $i++;
                    }
                    }
                    
             
                  $customer_booking=array(
                      'booking_id'=>$booking_id,
                      'booking_type'=>"LCC Flight",
                      'booking_detail'=>json_encode($bookingInfo),
                      'transaction_id'=>$transaction_id, 
                  );
                if(empty($bookingInfo) && count($bookingInfo)<1)
              {
                  return redirect()->back()->withErrors(['Booking details not found']);
              }
                $insert_booking = DB::table('customer_bookings')->insert($customer_booking);
                //dd($bookingInfo);
                    return view('frontend.home.flightConfirmation')
                    ->with([
                    'fb_cnf'=>$bookingInfo
                    ]);
            }
            /*--------------------------------------------------------------------------------|
            |--------------------------Flight Booking Non-LCC---------------------------------|
            |--------------------------------------------------------------------------------*/
            if(isset($non_lcc_data))    
            {
               
                $data=$request->session()->get('non_lcc_flight_booking_data');
                $bookingInfo = array();
                $i=0;
                $booking_id="";
                
                foreach($data as $flightData)
                {
                    $booking_info[$i]=$this->getFlightTicketNonLcc($request,$flightData['trace_id'],$flightData['pnr'],$flightData['booking_id']);
                    if(isset($booking_info[$i]) && $booking_info[$i]->Response->ResponseStatus==1)
                    {
                        $bookingInfo[$i] = $this->getFlightBookingDetail($booking_info[$i]->Response->Response->BookingId,$booking_info[$i]->Response->Response->PNR,$booking_info[$i]->Response->TraceId);
                    if($i>0)
                    {
                        $booking_id.='-'.$flightData['booking_id'];
                    }
                    else
                    {
                        $booking_id=$flightData['booking_id'];
                    }
                    $i++;
                        
                    }
                    
                }

             
                      $customer_booking=array(
                          'booking_id'=>$booking_id,
                          'booking_type'=>"NonLCC Flight",
                          'booking_detail'=>json_encode($bookingInfo),
                          'transaction_id'=>$transaction_id, 
                      );
                    if(empty($bookingInfo) && count($bookingInfo)<1)
              {
                  return redirect()->back()->withErrors(['Booking details not found']);
              }
                        $insert_booking = DB::table('customer_bookings')->insert($customer_booking);
                        return view('frontend.home.flightConfirmation')
                        ->with([
                        'fb_cnf'=>$bookingInfo
                        ]);
            }
            
            if(isset($ib_non_lcc))
            {

                    $bookingInfo = array();
                    $booking_info=$this->getFlightTicketNonLcc($request,$ib_non_lcc['ib_data']['trace_id'],$ib_non_lcc['ib_data']['pnr'],$ib_non_lcc['ib_data']['booking_id']);
                    $bookingInfo[0] = $this->getFlightBookingDetail($booking_info->Response->Response->BookingId,$booking_info->Response->Response->PNR,$booking_info->Response->TraceId);
                    $lcc_response=$this->doneLccFlightBooking($ib_non_lcc['ob_data']);
                    $bookingInfo[1]  = $this->getFlightBookingDetail($lcc_response->Response->Response->BookingId,$lcc_response->Response->Response->PNR,$lcc_response->Response->TraceId);
                    $booking_id=$booking_info->Response->Response->BookingId.'-'.$lcc_response->Response->Response->BookingId;

             /*---------------save to payment details to transaction table--------------*/
                    // $customer_transaction = array(
                    // 'txn_id'=>$response['bank_ref_no'],
                    // 'status'=>$response['order_status'],
                    // 'amount'=>$response['amount'],
                    // 'booking_id'=>$booking_id,
                    // );
                    // $insert_transaction = DB::table('transaction')->insert($customer_transaction);


              // dd($curl_flight_booking_details);
                /*-------------------------save customer booking details------------------------*/


                  //  dd($bookingInfo);
                      $customer_booking=array(
                          'booking_id'=>$booking_id,
                          'booking_type'=>"IB_NON",
                          'booking_detail'=>json_encode($bookingInfo),
                          'transaction_id'=>$transaction_id, 
                      );
                        if(empty($bookingInfo) && count($bookingInfo)<1)
              {
                  return redirect()->back()->withErrors(['Booking details not found']);
              }
                    $insert_booking = DB::table('customer_bookings')->insert($customer_booking);
                    //dd($book_room_info);
                        return view('frontend.home.flightConfirmation')
                        ->with([
                        'fb_cnf'=>$bookingInfo
                        ]);

            }
            if(isset($ob_non_lcc))
            {

                $bookingInfo = array();
                $booking_info=$this->getFlightTicketNonLcc($request,$ob_non_lcc['ob_data']['trace_id'],$ob_non_lcc['ob_data']['pnr'],$ob_non_lcc['ob_data']['booking_id']);
                $bookingInfo[0] = $this->getFlightBookingDetail($booking_info->Response->Response->BookingId,$booking_info->Response->Response->PNR,$booking_info->Response->TraceId);
                $lcc_response=$this->doneLccFlightBooking($ob_non_lcc['ib_data']);
                $bookingInfo[1]  = $this->getFlightBookingDetail($lcc_response->Response->Response->BookingId,$lcc_response->Response->Response->PNR,$lcc_response->Response->TraceId);
                $booking_id=$booking_info->Response->Response->BookingId.'-'.$lcc_response->Response->Response->BookingId;

         /*---------------save to payment details to transaction table--------------*/
                // $customer_transaction = array(
                // 'txn_id'=>$response['bank_ref_no'],
                // 'status'=>$response['order_status'],
                // 'amount'=>$response['amount'],
                // 'booking_id'=>$booking_id,
                // );
                // $insert_transaction = DB::table('transaction')->insert($customer_transaction);


          // dd($curl_flight_booking_details);
            /*-------------------------save customer booking details------------------------*/


              //  dd($bookingInfo);
                  $customer_booking=array(
                      'booking_id'=>$booking_id,
                      'booking_type'=>"OB_NON",
                      'booking_detail'=>json_encode($bookingInfo),
                      'transaction_id'=>$transaction_id, 
                  );
                    if(empty($bookingInfo) && count($bookingInfo)<1)
              {
                  return redirect()->back()->withErrors(['Booking details not found']);
              }
                $insert_booking = DB::table('customer_bookings')->insert($customer_booking);
               // dd($book_room_info);
                    return view('frontend.home.flightConfirmation')
                    ->with([
                    'fb_cnf'=>$bookingInfo
                    ]);

            }
        }
    else{
        return redirect()->route('frontend.index');
    }

}

public function bookingCancel(){
    return redirect()->route('frontend.hotels');
}

public function getBookingDetails($book_room_info,$token_id,$trace_id)
{
    $userIp=gethostbyname(trim(`hostname`));
    $data = array(
        'EndUserIp' =>$userIp,
        'TokenId' =>$token_id,
        'BookingId' =>$book_room_info->BookResult->BookingId,
     );
     
     $url=config('constants.LIVE.HOTELS.BOOKING_DETAILS');
     $curl_response=$this->apiCalls($data,$url);
     
     return $curl_response;

}



public function referAndEarn(){
    try{
        return view('frontend.home.refer-and-earn');
    }
    catch(Exception $e){
        dd($e);
    }
}

public function customizePackage(Request $request)
{

    
    if($request->isMethod('post'))
    {
    $userIp=gethostbyname(trim(`hostname`));
    $token_id=$this->token;
    $check_in=$request->input('depart_date');
    $country_code='TH';
    $currency='INR';
    $nationality='IN';
    $adults=$request->input('adults');
    $children=$request->input('children');
    $infant=$request->input('infants');
    $destination=$request->input('destination');
    $c_ages=$request->input('ages');
    $no_of_rooms=$request->input('no_of_rooms');
    $no_of_nights=$request->input('no_of_nights');
    
    
    $passanger_count=array(
        'adults'=>0,
        'child'=>0,
        'infant'=>0,
    );
    
    foreach ($adults as $adult)
    {
        $passanger_count['adults']+=$adult;
    }
    
    foreach ($children as $adult)
    {
        $passanger_count['child']+=$adult;
    }
    foreach ($infant as $adult)
    {
        $passanger_count['infant']+=$adult;
    }
    


    $x=array();
    $age_index=0;
    for($i=0;$i<$no_of_rooms;$i++){
        $x[$i]['NoOfAdults']=$adults[$i];
        $x[$i]['NoOfChild']=$children[$i];

        for($j=0;$j<$children[$i];$j++){
            $x[$i]['ChildAge'][$j]=$c_ages[$age_index];
            $age_index++;
        }
    }
    $room_guests= array();
    $room_guests=$x;
    $search_data=array();
    $hotel_city=$this->hotelCities();
    if(empty($hotel_city) && count($hotel_city)<1)
        {
            return redirect('/')->withErrors(['msg'=>'Error: Cities not found']);
        }
    for($i=0;$i<count($no_of_nights);$i++)
    {
        $dest=explode(',',$destination[$i+1]);
        if($dest[2]==='India')
        {
            $country_code='IN';
        }
        else
        {
            $country_code='TH';
        }
    $search_data[$i]['CheckInDate']=date('d/m/Y',strtotime($check_in[$i+1]));
    $search_data[$i]['NoOfNights']=$no_of_nights[$i];
    $search_data[$i]['CountryCode']=$country_code;
    $search_data[$i]['CityId']=$hotel_city[$dest[0]];
    $search_data[$i]['ResultCount']=null;
    $search_data[$i]['PreferredCurrency']=$currency;
    $search_data[$i]['GuestNationality']=$nationality;
    $search_data[$i]['NoOfRooms']=$no_of_rooms;
    $search_data[$i]['RoomGuests']=$room_guests;
    $search_data[$i]['EndUserIp']=$userIp;
    $search_data[$i]['TokenId']=$token_id;
    $search_data[$i]['MaxRating']=5;
    $search_data[$i]['MinRating']=3;
    }
    //dd($search_data);
    $hotel_search_data=$search_data;
    $hote_search_result=array();
    $hotel=0;
    $hotel_url=config('constants.LIVE.HOTELS.SEARCH');
    foreach ($search_data as $search){
        $hote_search_result[$hotel]=$this->apiCalls($search,$hotel_url);
        $hotel++;

    }
    
    $request->session()->put('hote_search_result',$hote_search_result);
//     //dd($hote_search_result);
    /////////////end Hotel Search////////////////////

    /////////Flight Search Start////////////////////
$flightCities=$this->getFlightCities();

    for($i=0;$i<count($destination);$i++)
{
$org=explode(',',$destination[$i]);

if($i===count($destination)-1)
{
    $det=explode(',',$destination[0]);
    $journeyDate=$check_in[0];
}
else
{
    $det=explode(',',$destination[$i+1]);
    $journeyDate=$check_in[$i+1];
    
}

$segments[$i]['Origin']=$flightCities[$org[0]];
$segments[$i]['Destination']=$flightCities[$det[0]];
$segments[$i]['FlightCabinClass']=1;
$segments[$i]['PreferredDepartureTime']=date('Y-m-d',strtotime($journeyDate)).'T00:00:00';
$segments[$i]['PreferredArrivalTime']=date('Y-m-d',strtotime($journeyDate)).'T00:00:00';
}

$search_data=array(
    'EndUserIp'=>$userIp,
    'TokenId'=>$token_id,
    'AdultCount'=>$passanger_count['adults'],
    'ChildCount'=>$passanger_count['child'],
    'InfantCount'=>$passanger_count['infant'],
    'DirectFlight'=>false,
    'JourneyType'=>3,
    'Segments'=>$segments,
);
$package_data=array('flights'=>$search_data,'hotels'=>$hotel_search_data);
$request->session()->put('package_search',$package_data);
$flight_url = config('constants.LIVE.FLIGHT.SEARCH');
$flightSearchResult=$this->apiCalls($search_data,$flight_url);
$flight_search = $request->session()->put('flight_listing_data',$flightSearchResult);
$request->session()->put('destination',$destination);
$request->session()->put('no_of_nights',$no_of_nights);
$request->session()->put('passanger_count',$passanger_count);
    
    }
    else
    {
        $hote_search_result=$request->session()->get('hote_search_result');
        $flightSearchResult=$request->session()->get('flight_listing_data');
        $no_of_nights=$request->session()->get('no_of_nights');
        $destination=$request->session()->get('destination');
        $passanger_count=$request->session()->get('passanger_count');
        
        
    }
    



return view('frontend.home.package-detail')->with([
    'package_hotels'=>$hote_search_result,
    'flights'=>$flightSearchResult,
    'passanger_count'=>$passanger_count,
    'no_of_nights'=>$no_of_nights,
    'destination'=>$destination,
    ]);
}

public function packagePassangerDetails(Request $request)
{
$token_id=$this->token;
$package=$request->session()->get('package_search');
$hotel_search_data=$package['hotels'];
//dd($hotel_search_data);
$flight_search_data=$package['flights'];
$data=$request->all();
$hotel_info=array();
$room_info=array();
$block_room_info=array();
$i=0;
$combination=array();

foreach($data['HotelTraceId'] as $trace_id)
{
$hotel_info[$i]=$this->hotelInfo($request,$trace_id,$data['HotelResultIndex'][$i],$data['HotelCode'][$i]);
if($hotel_info[$i]->HotelInfoResult->ResponseStatus==1)
{
$room_info[$i]=$this->getHotelRoomInfo($token_id,$trace_id,$data['HotelResultIndex'][$i],$data['HotelCode'][$i]);
$hotel_name=$hotel_info[$i]->HotelInfoResult->HotelDetails->HotelName;
$hotel_code=$hotel_info[$i]->HotelInfoResult->HotelDetails->HotelCode;
$combination=$this->roomCombination($room_info[$i],$hotel_search_data[$i]['NoOfRooms']);
//dd($combination);
$block_room_info[$i]=$this->getBlockHotelRoom($room_info[$i],$hotel_search_data[$i],$token_id,$combination[0],$trace_id,$data['HotelResultIndex'][$i],$hotel_name,$hotel_code);
if($block_room_info[$i]->BlockRoomResult->ResponseStatus!=1)
{
    return redirect()->back()->with('error',$block_room_info[$i]->BlockRoomResult->Error->ErrorMessage);
}
else if($block_room_info[$i]->BlockRoomResult->ResponseStatus==1 && $block_room_info[$i]->BlockRoomResult->AvailabilityType!=='Confirm')
{
    return redirect()->back()->with('error',"Room Availability Not Confirmed for the Hotel");
}
}
else
{
    return redirect()->back()->with('error',$hotel_info[$i]->HotelInfoResult->Error->ErrorMessage);
}
$block_room_info[$i]->BlockRoomResult->ResultIndex=$data['HotelResultIndex'][$i];
$block_room_info[$i]->BlockRoomResult->HotelCode=$data['HotelCode'][$i];

$i++;
}
$data=$this->getFlightPassangerDetails($request,$data['FlightTraceId'],$data['FlightResultIndex']);
$request->session()->put('block_room_info',$block_room_info);
//dd($flight_passanger_details);
//dd($block_room_info);

//return $flight_passanger_details;
return view('frontend.home.flightPassengerDetailsForm')->with([
    'flights'=>$data['selected_flight'],
        'fareRule'=>$data['fare_rule'],
        'fareQuote'=>$data['fare_quote'],
        'ssr'=>$data['ssr'],
        'countries'=>$data['country'],
        'trace_id'=>$data['trace_id'],
        'result_index'=>$data['result_index'],
        'affent_price'=>$data['affent_price'],
        'adult_price'=>$data['adult_price'],
        'child_price'=>$data['child_price'],
        'is_lcc'=>$data['is_lcc'],
        'booking_type'=>'package',
        'packageHotels'=>$block_room_info,
        'search_info'=>$hotel_search_data,
    ]);





}

public function makeCombination($in,$minLength = 1) { 
    $count = count($in); 
    $members = pow(2,$count); 
    $return = array(); 
    for ($i = 0; $i < $members; $i++) { 
       $b = sprintf("%0".$count."b",$i); 
       $out = array(); 
       for ($j = 0; $j < $count; $j++) { 
          if ($b{$j} == '1') $out[] = $in[$j]; 
       } 
       if (count($out) == $minLength) { 
          $return[] = $out; 
       } 
    }
    
    return $return; 
 }

public function roomCombination($room_info,$no_of_rooms)
{

    $comb=array();
    if($room_info->GetHotelRoomResult->RoomCombinations->InfoSource==='OpenCombination')
    {
    foreach($room_info->GetHotelRoomResult->RoomCombinations->RoomCombination as $cm)
    {
    $comb=array_merge($comb,$cm->RoomIndex);
    }
    
    $comb=$this->makeCombination($comb,$no_of_rooms);
    //dd($comb);
    
}
    else
    { 
    $c=0;
    
    foreach($room_info->GetHotelRoomResult->RoomCombinations->RoomCombination as $cm)
    {
    
    $comb[$c]=$cm->RoomIndex;
    $c++;
    }

    
}
return $comb;   
}
/*----------------------------------------------------------------------------------------|
|---------------------------frontend management from backend------------------------------|
|----------------------------------------------------------------------------------------*/
public function handleRegistration(Request $request,$id)
    {
        $request->session()->put('referer',$id);
        return redirect()->route('frontend.auth.register');
    }

public function allPackage($sourceid = null, $slug = null, Request $request,Package $package,Destination $destination,PackageType $packageType,Packagetheme $packageTheme){
        try{
            if($sourceid!=null)
            {
                
                if($sourceid=="destination")
                {
                
                    $package = $package->getAllPackagesByDestination();
                    
                    $id = Destination::where('title', from_slug($slug))->first()->id;
                    
                    
                
                }
                if($sourceid=="theme")
                {
                    
                    $package = $package->getAllPackagesByTheme();
                    $id = Packagetheme::where('name', from_slug($slug))->first()->id;
                }
                
                $sourceid = $sourceid.'-'.$id; 
                
                
                
            }
            else{
                
                if(!empty($request->input('source')))
                    {
                    $sourceid=$request->input('source');
                    }
                $package = $package->getAllPackage();
            }
            $destination = $destination->getAllDestination();
            $packageType = $packageType->getAllPackageType();
            $packageTheme = $packageTheme->getAllPackageTheme();
            return view('frontend.home.all-package')
            ->with(['package'=>$package,
                    'destination'=>$destination,
                    'packagetype'=>$packageType,
                    'packagetheme'=>$packageTheme,
                    'source_id'=>$sourceid,
                ]);
        }
        catch(Exception $e){
            dd($e);
        }
    }
    
    
    public function holidays(Blog $blog,Package $package,Destination $destination,PackageType $packageType,Packagetheme $packageTheme){
        try{
            $destination = $destination->getAllDestination();
            $package_theme = $packageTheme->getAllPackageTheme();
            $blog = $blog->getRecentBlog();
            //$destination_package = $destination->getAllPackagesByDestination();
            //$theme_package = $packageTheme->getAllPackagesByTheme();
            //dd($theme_package);
            return view('frontend.home.holidays')
            ->with([
                'destination'=>$destination,
                'packageTheme'=>$package_theme,
                'blog'=>$blog,
            ]);
        }
        catch(Exception $e){
            dd($e);
        }
    }
    public function packageReview(Request $request)
    {
        
        $input = $request->validate([
            'Title.*' => 'required|string',
            'FirstName.*' => 'required|distinct',
            'LastName.*' => 'required|string|different:FirstName',
            'Gender.*' => 'required|numeric|digits_between:0,3', 
            'DobDay.*'=> 'required|numeric',
            'DobMonth.*'=> 'required|numeric',
            'DobYear.*' => 'required|numeric',
            'addressLine1' => 'required',
           // 'addressLine2' => 'sometimes|required',
            'mobileNo' => 'required|numeric|digits:10',
            'email' => 'required|email',
            
            ], [
            'Title.*.required' => 'Title is mandatory',
            'Title.*.string' => 'Title is mandatory',
            'FirstName.*.required' => 'First Name is required',
            'FirstName.*.distinct' => 'First Name must be distinct',
            'LastName.*.required' => 'Last Name is required',
            'LastName.*.distinct' => 'Last Name must be distinct',
            'Gender.*.required' => 'Gender is mandatory',
            'Gender.*.numeric' => 'Gender is mandatory',
            'Gender.*.digits_between:min,max' => 'Please select Gender',
            'DobDay.*.required'=> 'Day',
            'DobMonth.*.required'=> 'Month',
            'DobYear.*.required'=> 'Year',
            'addressLine1.required'=>'Address is required',
        ]);
/*-------------------------------Passenger form validation end--------------------------------*/
    
    $userIp=gethostbyname(trim(`hostname`));
    
    $token_id=$this->token;
    $trace_id=$request->input('trace_id');
    $result_index=$request->input('result_index');
    $result_index_return=$request->input('result_index_return');
    $is_lcc=$request->input('is_lcc');
    $paxType = $request->input('PaxType');
    $title=$request->input('Title');
    $first_name=$request->input('FirstName');
    $last_name=$request->input('LastName');
    $gender=$request->input('Gender');
    $dob_day=$request->input('DobDay');
    $dob_month=$request->input('DobMonth');
    $dob_year=$request->input('DobYear');
    $addressLine1=$request->input('addressLine1');
    $addressLine2=$request->input('addressLine2');
    $City=$request->input('City');
    $mobileNo=$request->input('mobileNo');
    $email=$request->input('email');
    $Country=$request->input('Country');
    $Nationality=$request->input('Nationality');
    $PassportNo=$request->input('PassportNo');
    $PassExpDay=$request->input('PassExpDay');
    $PassExpMonth=$request->input('PassExpMonth');
    $PassExpYear=$request->input('PassExpYear');
    $baggage=$request->input('baggage');
    $baggage_selected = ($request->obbagSelected)+($request->ibbagSelected);
    $meal_platter = ($request->obmealPlatter)+($request->ibmealPlatter);
    $baggage_cost = ($request->obbagCharges)+($request->ibbagCharges);
    $meal_cost = ($request->obmealCharge)+($request->ibmealCharge);
    $meal_0_indi_1_seg_0=$request->input('meal_0_indi_1_seg_0');
    $meal_0_indi_1_seg_1=$request->input('meal_0_indi_1_seg_1');
    $Seat_0_indi_1_seg_0=$request->input('Seat_0_indi_1_seg_0');
    $Seat_0_indi_1_seg_1=$request->input('Seat_0_indi_1_seg_1');
    $userAction_0=$request->input('userAction_0');
    $CustomerID_0=$request->input('CustomerID_0');
    $gstNumber=$request->input('gstNumber');
    $gstContact=$request->input('gstContact');
    $gstEmail=$request->input('gstEmail');
    $gstName=$request->input('gstName');
    $gstaddress=$request->input('gstaddress');
    $roamerCountryCode=$request->input('roamerCountryCode');
    $roamerContact=$request->input('roamerContact');
    $PromoCode=$request->input('PromoCode');
    $baseFare = $request->input('BaseFare');
    $tax = $request->input('Tax');
    $transactionFee = $request->input('TransactionFee');
    $yqTax = $request->input('YQTax');
    $additionalTxnFeeOfrd = $request->input('AdditionalTxnFeeOfrd');
    $additionalTxnFeePub = $request->input('AdditionalTxnFeePub');
    $airTransFee = $request->input('AirTransFee');
    //Return fare details

    $baseFare_return = $request->input('BaseFareReturn');
    $tax_return = $request->input('TaxReturn');
    $transactionFee_return = $request->input('TransactionFeeReturn');
    $yqTax_return = $request->input('YQTaxReturn');
    $additionalTxnFeeOfrd_return = $request->input('AdditionalTxnFeeOfrdReturn');
    $additionalTxnFeePub_return = $request->input('AdditionalTxnFeePubReturn');
    $airTransFee_return = $request->input('AirTransFeeReturn');

    ///////////////////////////////////////


    $passanger=array();
    
    for($i=0;$i<count($first_name);$i++)
    
    {
        $passanger[$i]['Title']=$title[$i];
        $passanger[$i]['FirstName']=$first_name[$i];
        $passanger[$i]['LastName']=$last_name[$i];
        $passanger[$i]['PaxType']=$paxType[$i];
        //$passanger[$i]['DateOfBirth']=$dob_day[$i].'/'.$dob_month[$i].'/'.$dob_year[$i];
        $passanger[$i]['Gender']=$gender[$i];
        $passanger[$i]['PassportNo']=$PassportNo[$i];
        $passanger[$i]['DateOfBirth']=$dob_year[$i].'-'.$dob_month[$i].'-'.$dob_day[$i].'T00:00:00';
        if(isset($PassExpYear[$i])&&isset($PassExpMonth[$i])&&isset($PassExpDay[$i]))
        {
        $passanger[$i]['PassportExpiry']=$PassExpYear[$i].'-'.$PassExpMonth[$i].'-'.$PassExpDay[$i].'T00:00:00';
        }
        else
        {
            $passanger[$i]['PassportExpiry']=null;
        }
        $passanger[$i]['AddressLine1']=$addressLine1;
        $passanger[$i]['AddressLine2']=$addressLine2;
        $passanger[$i]['City']=$City;
        $passanger[$i]['CountryCode']=$Country[$i];
        $passanger[$i]['CountryName']=$Country[$i];
        $passanger[$i]['ContactNo']=$mobileNo;
        $passanger[$i]['Email']=$email;
        if($i==0)
        $passanger[$i]['IsLeadPax']=true;
        else
        $passanger[$i]['IsLeadPax']=false;
        
        $passanger[$i]['Fare']['BaseFare']=$baseFare[$i];
        $passanger[$i]['Fare']['Tax']=$tax[$i];
        $passanger[$i]['Fare']['TransactionFee']=$transactionFee[$i];
        $passanger[$i]['Fare']['YQTax']=$yqTax[$i];
        $passanger[$i]['Fare']['AdditionalTxnFeeOfrd']=$additionalTxnFeeOfrd[$i];
        $passanger[$i]['Fare']['AdditionalTxnFeePub']=$additionalTxnFeePub[$i];
        $passanger[$i]['Fare']['AirTransFee']=$airTransFee[$i];
    }


     $fare_rule = $this->getFlightFareRule($result_index,$trace_id);
     
    $flight_listing = $request->session()->get('flight_listing_data');
    $selectedFlight = $request->session()->get('selected_flight');
    $request->session()->put('flight_passenger_data',$passanger);
    $package=$request->session()->get('package_search');
    $hotel_search_data=$package['hotels'];
    $block_room_info=$request->session()->get('block_room_info');
    //dd($block_room_info);
    
    
    ///////////////////HotelBooking//////////////////////////////////
    
    
    $passangers=array();
    $adults=array();
    $child=array();
    $a=0;
    $c=0;
    
    for($p=0;$p<count($first_name);$p++)
    {
        if($paxType[$p]==1)
        {
            
        
        
                    $adults[$a]['Title']=$title[$p];
                    $adults[$a]['FirstName']=$first_name[$p];
                    $adults[$a]['LastName']=$last_name[$p];
                    $adults[$a]['PaxType']=1;
                    if($a==0)
                    {
                    $adults[$a]['LeadPassenger']=true;
                    }
                    else
                    {
                        $adults[$a]['LeadPassenger']=true;
                    }
                    $a++;
        }
        else if($paxType==2)
        {
            $child[$c]['Title']=$title[$p];
            $child[$c]['FirstName']=$first_name[$p];
            $child[$c]['LastName']=$last_name_child[$p];
            $child[$c]['PaxType']=2;
            $child[$c]['LeadPassenger']=false;
            $child[$c]['Age']=2;
            $c++;
        }
                
    }
   
    $i=0;
    $k=0;
    $a=0;
    $c=0;
    foreach($hotel_search_data[0]['RoomGuests'] as $guests)
    {
	for($j=0;$j<$guests['NoOfAdults'];$j++)    
        {
        $passangers[$i][$k]=$adults[$a];
        $a++;
        $k++;
        }

        for($j=0;$j<$guests['NoOfChild'];$j++)    
        {
        $passangers[$i][$k]=$child[$c];
        $c++;
        $k++;
        }
        $i++;
        $k=0;
    }
    
        $bookingData=array();
        $prices=array();
        $hotelRooms=array();
        for($hotels=0;$hotels<count($block_room_info);$hotels++)
        {
            foreach($block_room_info[$hotels] as $hotelInfo)    
            {
                $room_no=0;
                foreach($hotelInfo->HotelRoomsDetails as $roomDetails)
                {
                    
                    $price['CurrencyCode']=$roomDetails->Price->CurrencyCode;
                    $price['RoomPrice']=$roomDetails->Price->RoomPrice;
                    $price['Tax']=$roomDetails->Price->Tax;
                    $price['ExtraGuestCharge']=$roomDetails->Price->ExtraGuestCharge;
                    $price['ChildCharge']=$roomDetails->Price->ChildCharge;
                    $price['OtherCharges']=$roomDetails->Price->OtherCharges;
                    $price['Discount']=$roomDetails->Price->Discount;
                    $price['PublishedPrice']=$roomDetails->Price->PublishedPrice;
                    $price['PublishedPriceRoundedOff']=$roomDetails->Price->PublishedPriceRoundedOff;
                    $price['OfferedPrice']=$roomDetails->Price->OfferedPrice;
                    $price['OfferedPriceRoundedOff']=$roomDetails->Price->OfferedPriceRoundedOff;
                    $price['AgentCommission']=$roomDetails->Price->AgentCommission;
                    $price['AgentMarkUp']=$roomDetails->Price->AgentMarkUp;
                    $price['ServiceTax']=$roomDetails->Price->ServiceTax;
                    $price['TDS']=$roomDetails->Price->TDS;            
                    $hotel_room['RoomIndex']=$roomDetails->RoomIndex;
                    $hotel_room['RatePlanCode']=$roomDetails->RatePlanCode;
                    $hotel_room['RoomTypeCode']=$roomDetails->RoomTypeCode;
                    $hotel_room['RoomTypeName']=$roomDetails->RoomTypeName;
                    $hotel_room['BedTypeCode']=null;
                    $hotel_room['SmokingPreference']=0;
                    $hotel_room['Supplements']=null;
                    $hotel_room['Price']=$price;                      
                    $hotel_room['HotelPassenger']=$passangers[$room_no];                      
                }
                $hotel_rooms[$room_no]=$hotel_room;
                $room_no++;
                
            }
            $bookingData[$hotels]=array(
                                'EndUserIp'=>$userIp,
                                'TokenId'=>$token_id,
                                'TraceId'=>$hotelInfo->TraceId,
                                'ResultIndex'=>$hotelInfo->ResultIndex,
                                'HotelCode'=>$hotelInfo->HotelCode,
                                'HotelName'=>$hotelInfo->HotelName,
                                'GuestNationality'=>'IN',
                                'NoOfRooms'=>$hotel_search_data[$i]['NoOfRooms'],
                                'IsVoucherBooking'=>true,
                                'HotelRoomsDetails'=>$hotel_rooms,
);
            
        }
   
$request->session()->put('hotel_booking_data',$bookingData);
    
    /////////////////End Hotel Booking//////////////////////////////
    
    
    
  return view('frontend.home.flightBookingReview')
  ->with([
      'passenger'=>$passanger,
      'flights'=>$selectedFlight,
      'fareRule'=>$fare_rule,
      'trace_id'=>$trace_id,
      'result_index'=>$result_index,
      'is_lcc'=>$is_lcc,
      'booking_type'=>'package',
      'packageHotels'=>$block_room_info,
      'search_info'=>$hotel_search_data,
      'baggage_selected'=>$baggage_selected,
      'baggage_cost'=>$baggage_cost,
      'meal_platter'=>$meal_platter,
      'meal_cost'=>$meal_cost,
      ]);
        
    }
    public function packageBooking(Request $request)
    {
        //dd($request->all());
        $data=$request->all();
        $token_id=$this->token;
        $userIp = $this->userIp;
        $passenger_data = $request->session()->get('flight_passenger_data');
        if(!$data['is_lcc'])
        {
        $response=$this->holdFlightBooking($request,$data['TraceId'],$data['ResultIndex'],$passenger_data);
        if(isset($response) && $response->Response->ResponseStatus==1 && isset($response->Response->Response->PNR) )
        {
        $pnr=$response->Response->Response->PNR;
        $booking_id=$response->Response->Response->BookingId;
      $flight_data=array(
        'trace_id'=>$data['TraceId'],
        'pnr'=>$pnr,
        'booking_id'=>$booking_id
        );        
        }
        else
        {
            return redirect()->back()->with('error',$response->Response->Error->ErrorMessage);
        }
        
     }
     else
     {
    
    $flight_data=array(
        'EndUserIp'=>$this->userIp,
        'TokenId'=>$this->token,
        'ResultIndex'=>$data['ResultIndex'],
        'TraceId'=>$data['TraceId'],
        'Passengers'=>$passenger_data,
    );
         
     }
     $hotel_data=$request->session()->get('hotel_booking_data');
     $packageData=[
         'hotels'=>$hotel_data,
         'flights'=>$flight_data,
         'is_lcc'=>$data['is_lcc']
         ];
         
         $request->session()->put('package_booking_data',$packageData);
         $amount = $request->session()->get('booking_price');
         $tid=rand(100000,9999999);
         $parameters = [
                        'tid' => $tid,
                        'order_id' =>$data['ResultIndex'],
                        'amount' => $amount,  
                        //'amount' => '1',  
                        ];
  
  // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker
  
  $order = Payment::gateway('CCAvenue')->prepare($parameters);
  return Payment::process($order);
     
     
     
    }
    
    public function ticketFlightBookingIBNonLcc(Request $request,$trace_id,$result_index,$result_index_return)
{
    $passenger_data = $request->session()->get('flight_passenger_data');
    $return_passanger_data=$request->session()->get('flight_passenger_data_return');
    $response=$this->holdFlightBooking($request,$trace_id,$result_index_return,$return_passanger_data);
    $pnr=$response->Response->Response->PNR;
    $booking_id=$response->Response->Response->BookingId;
    $ib_data=array(
            'trace_id'=>$trace_id,
            'pnr'=>$pnr,
            'booking_id'=>$booking_id,
    );
    $ob_data=array(
            'EndUserIp'=>$this->userIp,
            'TokenId'=>$this->token,
            'ResultIndex'=>$result_index_return,
            'TraceId'=>$trace_id,
            'Passengers'=>$passenger_data,
        );


    $session_data=array(
        'ob_data'=>$ob_data,
        'ib_data'=>$ib_data
    );





    $request->session()->put('ib_non_lcc',$session_data);
    //$request->session()->put('booking_data',$data);
    $amount = $request->session()->get('booking_price');
    $tid=rand(100000,9999999);
    $parameters = [

        'tid' => $tid,
        'order_id' =>$result_index,
        'amount' => $amount,
        //'amount' => '1',
      ];

      // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker

      $order = Payment::gateway('CCAvenue')->prepare($parameters);
      return Payment::process($order);
}

public function ticketFlightBookingOBNonLcc(Request $request,$trace_id,$result_index,$result_index_return)
{
$passenger_data = $request->session()->get('flight_passenger_data');
$return_passanger_data=$request->session()->get('flight_passenger_data_return');
$response=$this->holdFlightBooking($request,$trace_id,$result_index,$passenger_data);
$pnr=$response->Response->Response->PNR;
$booking_id=$response->Response->Response->BookingId;
$ob_data=array(
        'trace_id'=>$trace_id,
        'pnr'=>$pnr,
        'booking_id'=>$booking_id,
);
$ib_data=array(
        'EndUserIp'=>$this->userIp,
        'TokenId'=>$this->token,
        'ResultIndex'=>$result_index_return,
        'TraceId'=>$trace_id,
        'Passengers'=>$return_passanger_data,
    );


$session_data=array(
    'ob_data'=>$ob_data,
    'ib_data'=>$ib_data
);





$request->session()->put('ob_non_lcc',$session_data);
//$request->session()->put('booking_data',$data);
$amount = $request->session()->get('booking_price');
$tid=rand(100000,9999999);
$parameters = [

    'tid' => $tid,
    'order_id' =>$result_index,
    'amount' => $amount,
    //'amount' => '1',
  ];

  // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker

  $order = Payment::gateway('CCAvenue')->prepare($parameters);
  return Payment::process($order);

}

public function singlePackageDetail(Request $request,Package $package,Activity $activity,Destination $destination)
{
    $slug = $request->input('package');
    //dd($package_id);
    $package_detail = $package->getPackageById($slug);
    
    $activity_detail = $activity->getAllActivity();
    $destination_list = $destination->getAllDestination();
    $country_code='TH';
    $currency='INR';
    $nationality='IN';
    $package_price=$request->input('package_price');
    $package_price_off=$request->input('package_price_off');
 
        $adults=array(2);
        $children=array(0);
        $infant=array(0);
        $c_ages=array(0);
        $no_of_rooms=1;
        $check_in=$request->input('depart_date');
        $no_of_nights=$request->input('no_of_nights');
        $destination=array();
        $destination_hnf_code=$request->input('destination');
        $passanger_count=array(
            'adults'=>2,
            'child'=>0,
            'infant'=>0,
        );
    for($i=0;$i<count($destination_hnf_code);$i++)
    {
        $e_val=explode(',',$destination_hnf_code[$i]);
        if($i==0)
            $destination[$i]=$e_val[0].','.$e_val[1].','.$e_val[2];
        else
            $destination[$i]=$e_val[0].','.$e_val[1].',Thailand';
    }
        
return view('frontend.home.single-package.detail')->with([
'passanger_count'=>$passanger_count,
'no_of_nights'=>$no_of_nights,
'destination'=>$destination,
'package_detail'=>$package_detail,
'activity_detail'=>$activity_detail,
'package_cost'=>$package_price,
'package_cost_off'=>$package_price_off,
]);
}

public function activities($source,Activity $activity,Destination $destination)
{
    try{
       $activity_list = $activity->getAllActivityBySource($source);
       $destination_list = $destination->getAllDestination();
       $activities_count = $activity->getCountByDestination($source);
       //dd($activities_count);
       //dd($activity_list);
        return view('frontend.home.activity')
        ->with([
           'activity'=>$activity_list,
           'destination'=>$destination_list,
           'activity_count'=>$activities_count,
           'source'=>$source,
        ]);
    }
    catch(Exception $e){
        dd($e);
    }
}

public function allActivities(Activity $activity,Destination $destination)
{
    try{
       $activity_list = $activity->getAllActivity();
       $destination_list = $destination->getAllDestination();
       $activities_count = $activity->getCountByDestination();
       //dd($activities_count);
       //dd($activity_list);
        return view('frontend.home.all-activity')
        ->with([
           'activity'=>$activity_list,
           'destination'=>$destination_list,
           'activity_count'=>$activities_count,
           
        ]);
    }
    catch(Exception $e){
        dd($e);
    }
}
public function activitiesByRegion($region,Activity $activity,Destination $destination)
{
    try{
        $source = explode('-',$region);
        //dd($source);
        $activity_list = $activity->getAllActivityBySourceAndRegion($source[0],str_replace('_',' ',$source[1]));
        $destination_list = $destination->getAllDestination();
        $activities_count = $activity->getCountByDestination($source[0]);
        //dd($activities_count);
        //dd($activity_list);
         return view('frontend.home.activity')
         ->with([
            'activity'=>$activity_list,
            'destination'=>$destination_list,
            'activity_count'=>$activities_count,
            'source'=>$source[0],
         ]);
     }
     catch(Exception $e){
         dd($e);
     }
}
public function bookingActivity(Request $request, $source,Activity $activity)
{
  //dd($source);
  $amount = $request->session()->get('booking_price');
    $tid=rand(100000,9999999);
    $parameters = [
      
    'tid' => $tid,
    'order_id' =>'ACT-'.$source,
    'amount' => $amount,
    //'amount' => '1',  
    
  ];
  
  // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker
  
    $order = Payment::gateway('CCAvenue')->prepare($parameters);
    return Payment::process($order);
  
}



public function filterFlights(Request $request)
    {
                //return json_encode($request->all());
                $journey_type=$request->session()->get('journey_type');
                $flights=$request->session()->get('flight_listing_data');
                $is_domestic=$request->session()->get('is_domestic');
                $search_info = $request->session()->get('flight_search_data');
                $results=$flights->Response->Results[0];
                $stops=$request->stops;
                $depTime=$request->depTime;
                $arrTime=$request->arrTime;
                $segment=$request->segment;
                $airlines=$request->airlines;
                $prices=explode('-',$request->prices);
                $min=$prices[0];
                $flights_count=0;
                if(!empty($prices[1]))
                    {
                        $max=$prices[1];
        
                    }
                else
                    {
                    $max=0;
                    }
                $newResult=array();
                $flight_result=array();
                
                            foreach($results as $fd)
                            {
                            
                            $basefare = $fd->FareBreakdown[0]->BaseFare;
                            $tax = $fd->FareBreakdown[0]->Tax;
                            $other_charges = $fd->Fare->OtherCharges;
                            $adult_count = $fd->FareBreakdown[0]->PassengerCount;
                            if(isset($fd->FareBreakdown[1]))
                            $child_count = $fd->FareBreakdown[1]->PassengerCount;
                            else
                            $child_count=0;
                            $other_charges_per_adult =$other_charges/($adult_count+$child_count); 
                             $pubfare_per_adult = ($basefare+$tax)/($adult_count)+$other_charges_per_adult;
                            //$pubfare_per_adult = ($basefare+$tax)/($adult_count+$child_count);
                            $offer_on_pubfare = $fd->Fare->CommissionEarned+$fd->Fare->PLBEarned+$fd->Fare->IncentiveEarned;
                            $offer_on_pubfare_per_adult = $offer_on_pubfare/($adult_count+$child_count);
                            $offeredfare_per_adult = $pubfare_per_adult-$offer_on_pubfare_per_adult;
                            
                            $newResult=$fd;
                            if(isset($request->prices)&& !empty($request->prices))
                            {
                            if($max>0 && ($offeredfare_per_adult>$min && $offeredfare_per_adult<$max))
                            {
                                
                                $newResult=$fd;
                                
                                
                                
                                }
                                else if($max==0 && $offeredfare_per_adult > $min )
                                {
                                        $newResult=$fd;
                                        

                                    }
                                    else
                                    {
                                        
                                        $newResult=array();
                                       
                                    }
                                
                            }
                            
                                    
                                if(isset($airlines) && count($airlines)>0)
                                {
                                    if(!empty($newResult) && in_array($newResult->Segments[$segment][0]->Airline->AirlineName,$airlines))
                                    {
                                        $newResult=$newResult;
                                        
                                    }
                                    else
                                    {
                                        $newResult=array();
                                    }
                                }
                                
                                if(isset($stops) && count($stops)>0)
                                {
                                    if(!empty($newResult) && in_array((count($newResult->Segments[$segment])-1),$stops))
                                    {
                                        $newResult=$newResult;
                                    }
                                    else
                                    {
                                        $newResult=array();
                                    }
                                }
                                
                                
                                
                                if(!empty($newResult) && isset($depTime) && count($depTime)>0)
                                {
                                    $flag=0;
                                    for($k=0;$k<count($depTime);$k++)
                                    {
                                        $departure=explode('-',$depTime[$k]);
                                        $flight_time = $newResult->Segments[$segment][0]->Origin->DepTime;
                                        $from = $departure[0];
                                        $to = $departure[1];
                                        
                                        $date1 = date('g:i A', strtotime($flight_time));
                                        $date2 = date('g:i A', strtotime($from));
                                        $date3 = date('g:i A', strtotime($to));
                                        
                                        // echo $date1.$date2.$date3;
                                        // die;
                                    
                                        if (!empty($newResult) && $date1 > $date2 && $date1 < $date3)
                                        {
                                            $newResult=$newResult;
                                            $flag=1;
                                            break;
                                        }
                                            }
                                            if($flag==0)
                                            {
                                                $newResult=array();
                                            }
                                    
                                }
                                
                                if(!empty($newResult) && isset($arrTime) && count($arrTime)>0)
                                {
                                    
                                    $flag=0;
                                    for($k=0;$k<count($arrTime);$k++)
                                    {
                                        $departure=explode('-',$arrTime[$k]);
                                        $flight_time = $newResult->Segments[$segment][count($newResult->Segments[$segment])-1]->Destination->ArrTime;
                                        $from = $departure[0];
                                        $to = $departure[1];
                                        
                                        $date1 = date('g:i A', strtotime($flight_time));
                                        $date2 = date('g:i A', strtotime($from));
                                        $date3 = date('g:i A', strtotime($to));
                                        
                                        // echo $date1.$date2.$date3;
                                        // die;
                                    
                                        if ($date1 > $date2 && $date1 < $date3)
                                        {
                                            $newResult=$newResult;
                                            $flag=1;
                                            break;
                                        }
                                            }
                                            if($flag==0)
                                            {
                                                $newResult=array();
                                            }
                                }
                                
                                
                                
                                    
                            if(!empty($newResult))        
                            {
                            $flight_result[$flights_count]=$newResult;    
                            $flights_count++;
                            }
                            }
                            $cabinClass=$this->flightCabinClass;
                            
                            if($journey_type==3)
	                            return view('frontend.home.partials.flights.multi-trip',compact('flight_result','flights','cabinClass','search_info'));
                            elseif($journey_type==1)
	                            return view('frontend.home.partials.flights.one-way',compact('flight_result','flights','cabinClass','search_info'));
                            elseif($is_domestic && $journey_type==2)
	                            return view('frontend.home.partials.flights.round-dom',compact('flight_result','flights','cabinClass','search_info'));
                            else
	                            return view('frontend.home.partials.flights.round-intl',compact('flight_result','flights','cabinClass','search_info'));

                            
                            
                            
    //   return json_encode($filteredResult);
    }
    
    public function filterHotels(Request $request)
{
    $ratings =$request->ratings;
    $prices=explode('-',$request->prices);
    $facility=$request->facility;
    $locations=$request->locations;
    $category=$request->category;
    $min=$prices[0];
    
    if(!empty($prices[1]))
    {
        $max=$prices[1];
        
    }
    else
    {
        $max=0;
    }
    
    
    $hotelData=$request->session()->get('hotel_listing_data');    
    $filteredHotel=array();
    
     $j=0;
    for($i=0;$i<count($hotelData->HotelSearchResult->HotelResults);$i++)
    {
        if(!empty($ratings)>0 && !empty($prices)>0)
        {
            if(in_array($hotelData->HotelSearchResult->HotelResults[$i]->StarRating,$ratings))
            {
                if($max>0 && ($hotelData->HotelSearchResult->HotelResults[$i]->Price->OfferedPriceRoundedOff>$min && $hotelData->HotelSearchResult->HotelResults[$i]->Price->OfferedPriceRoundedOff<$max))
                {
                    
                    $filteredHotel[$j]=$hotelData->HotelSearchResult->HotelResults[$i];
                    $j++;
                }
                else if($max==0 && $hotelData->HotelSearchResult->HotelResults[$i]->Price->OfferedPriceRoundedOff > $min ){
    
                    $filteredHotel[$j]=$hotelData->HotelSearchResult->HotelResults[$i];
                    $j++;

                }
            
            }

        }
        else if(isset($ratings) && !isset($prices))
        {
            if(in_array($hotelData->HotelSearchResult->HotelResults[$i]->StarRating,$ratings))
            {
            $filteredHotel[$j]=$hotelData->HotelSearchResult->HotelResults[$i];
            $j++;
            }
        }
        else if(!isset($ratings) && isset($prices))
        {

            if($max>0 && ($hotelData->HotelSearchResult->HotelResults[$i]->Price->OfferedPriceRoundedOff>$min && $hotelData->HotelSearchResult->HotelResults[$i]->Price->OfferedPriceRoundedOff<$max))
                {
                    
                    $filteredHotel[$j]=$hotelData->HotelSearchResult->HotelResults[$i];
                    $j++;
                }
                else if($max==0 && $hotelData->HotelSearchResult->HotelResults[$i]->Price->OfferedPriceRoundedOff > $min ){
    
                    $filteredHotel[$j]=$hotelData->HotelSearchResult->HotelResults[$i];
                    $j++;

                }

        }
        else
        {
            $filteredHotel[$j]=$hotelData->HotelSearchResult->HotelResults[$i];
            $j++;
        }
        
        

    }

    
    return view('frontend.home.partials.hotel-detail')->with(['hotels'=>$filteredHotel,'trace_id'=>$hotelData->HotelSearchResult->TraceId]);
    //return json_encode($filteredHotel);

    
}


public function storeFeedback(Request $request){
    
    $data = $request->except('_token');
    
    if(Feedback::create($data)){
        
        Mail::send('emails.feedback.user', ['name' => $data['name'], 'email' => $data['email']], function ($m) use ($data) {
        $m->from('noreply@thailandwale.com', 'Thailandwale');
        $m->to($data['email'])->subject('Your Feedback to Thailandwale Submitted');
        });
        
        Mail::send('emails.feedback.thailandwale', ['name' => $data['name'], 'email' => $data['email']], function ($m) use ($data) {
        $m->from($data['email'], 'Thailandwale');
        $m->to('hello@thailandwale.com')->subject('Feedback Received from '.$data['name']);
        });
    }
    
    return redirect('/feedback-page');
        
}

/*public function singlePackageBooking(Request $request)
{

    $data = $request->except('_token');
    $amount = $request->session()->get('booking_price');
    $tid=rand(100000,9999999);
    $parameters = [
    'tid' => $tid,
    'order_id' =>'PKG-'.$data['package_id'],
    'amount' => $data['package_basefare'],
  ];
  
  // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker
  
    $order = Payment::gateway('CCAvenue')->prepare($parameters);
    return Payment::process($order);
    
}*/

function singlePackagePaxDetails(Request $request, Package $package){
    
    $data = $request->except('_token');
    // dd($data);
    
    $package_detail = $package->getPackageById($data['package_id']);
    
    
    return view('frontend.home.single-package.booking-form')
                ->with([
                    'adult_count' => $data['adults'],
                    'child_count' => $data['children'],
                    'package' => $package_detail,
                    'price' => $data['package_basefare'],
                    'discount' => $data['package_basefare_off'],
                    ]);
}

function singlePackageBooking(Request $request, Package $package){
    
    
    
    $data = $request->except('_token');
    
    
    $tid=rand(100000,9999999);
    $parameters = [
    'tid' => $tid,
    'order_id' =>'PKG-'.$data['package_id'],
    'amount' => $data['payable_amount'],
  ];
  
  // gateway = CCAvenue / PayUMoney / EBS / Citrus / InstaMojo / ZapakPay / Mocker
  
    $order = Payment::gateway('CCAvenue')->prepare($parameters);
    return Payment::process($order);
}
function submitform(Request $request){
    
        
        Mail::send('emails.feedback.thailandwale', ['name' => $request->name, 'email' =>$request->email,'experience'=>$request->experience,'comments'=>$request->comments], function ($m) use ($data) {
        $m->from($data['email'], 'Thailandwale');
        $m->to('hello@thailandwale.com')->subject('Feedback Received from '.$data['name']);
        });
        return json_encode(array('msg'=>'Successfully Send your Feedback','code'=>'200'));
}




}

