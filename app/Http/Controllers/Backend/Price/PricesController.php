<?php

namespace App\Http\Controllers\Backend\Price;

use App\Models\Price\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Price\PriceRepository;
use App\Http\Requests\Backend\Price\ManagePriceRequest;
use App\Http\Requests\Backend\Price\CreatePriceRequest;
use App\Http\Requests\Backend\Price\StorePriceRequest;
use App\Http\Requests\Backend\Price\EditPriceRequest;
use App\Http\Requests\Backend\Price\UpdatePriceRequest;
use App\Http\Requests\Backend\Price\DeletePriceRequest;

/**
 * PricesController
 */
class PricesController extends Controller
{
    /**
     * variable to store the repository object
     * @var PriceRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param PriceRepository $repository;
     */

     protected $services=[
        'Flight'=>'Flight',
        'Hotel'=>'Hotel',
        'Package'=>'Package',
     ];
     protected $type=[
        'Increase'=>'Increase',
        'Decrease'=>'Decrease',
     ];
     protected $amountType=[
        'Fixed'=>'Fixed',
        'Percentage'=>'Percentage',
     ];
     protected $status=[
        'Expired' => 'Expired',
        'Active'  => 'Active',
     ];

    public function __construct(PriceRepository $repository)
    {
        $this->repository = $repository;
        
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Price\ManagePriceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(ManagePriceRequest $request)
    {
        return view('backend.prices.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreatePriceRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreatePriceRequest $request)
    {
        return view('backend.prices.create')
                ->with([
                    'services'=>$this->services,
                    'type'=>$this->type,
                    'amountType'=>$this->amountType,
                    'status'=>$this->status,
                    ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePriceRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePriceRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return redirect()->route('admin.prices.index')->withFlashSuccess(trans('alerts.backend.prices.created'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Price\Price  $price
     * @param  EditPriceRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Price $price, EditPriceRequest $request)
    {
        $status=$this->status;
        $services=$this->services;
        $type=$this->type;
        $amountType=$this->amountType;
        
        return view('backend.prices.edit', compact('price','status','type','amountType','services'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePriceRequestNamespace  $request
     * @param  App\Models\Price\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePriceRequest $request, Price $price)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $price, $input );
        //return with successfull message
        return redirect()->route('admin.prices.index')->withFlashSuccess(trans('alerts.backend.prices.updated'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeletePriceRequestNamespace  $request
     * @param  App\Models\Price\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function destroy(Price $price, DeletePriceRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($price);
        //returning with successfull message
        return redirect()->route('admin.prices.index')->withFlashSuccess(trans('alerts.backend.prices.deleted'));
    }
    
}
