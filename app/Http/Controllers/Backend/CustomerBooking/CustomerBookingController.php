<?php

namespace App\Http\Controllers\Backend\CustomerBooking;

use App\Http\Requests\Backend\CustomerBooking\ManageCustomerBookingRequest;
use App\Models\CustomerBooking\CustomerBooking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\CustomerBooking\CustomerBookingRepository;


use Exception,Session;

/**
 * PackagesController
 */
class CustomerBookingController extends Controller
{

    /**
     * @var PackageRepository
     */
    protected $customerbooking;

    /**
     * @param \App\Repositories\Backend\Package\PackageRepository $package
     */
    public function __construct(CustomerBookingRepository $customerbookings)
    {
        $this->customerbookings = $customerbookings;
    }

    /**
     * @param \App\Http\Requests\Backend\Package\ManagePackageRequest $request
     *
     * @return mixed
     **/
    public function index(Request $request,CustomerBooking $customerbooking)
    {
        $customer_booking = $customerbooking->getAllBookingDetails();
        //dd($customer_booking);
        return view('backend.customer_bookings.index')->with(['booking_data'=>$customer_booking]);
        //return view('backend.customer_bookings.index')->with(['booking_data'=>$customer_booking]);
    }
    public function destroy(CustomerBooking $customerbooking, ManageCustomerBookingRequest $request)
    {
        echo"hello"; die;
        //Calling the delete method on repository
        $this->customerbooking->delete($customerbooking);
        //returning with successfull message
        return redirect()->route('admin.customer_bookings.index')->withFlashSuccess(trans('alerts.backend.customer_bookings.deleted'));
    }
    public function getCustomerBooking($booking_id,CustomerBooking $customerbooking)
    {
    $booking_json_data = $customerbooking->getSingleBookingDetails($booking_id);
        foreach($booking_json_data as $bd){
            $json_booking_details = $bd->booking_detail;
        }
        // dd($json_booking_details);
               $bookingInfo = json_decode($json_booking_details);
              //  dd($bookingInfo);
          return view('backend.customer_bookings.hotel_voucher')
             ->with([
                 'bookingInfo'=>$bookingInfo,
                ]);

    }
}
