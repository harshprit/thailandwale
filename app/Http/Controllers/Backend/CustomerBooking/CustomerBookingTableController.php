<?php

namespace App\Http\Controllers\Backend\CustomerBooking;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\CustomerBooking\CustomerBookingRepository;
use App\Http\Requests\Backend\CustomerBooking\ManageCustomerBookingRequest;


class CustomerBookingTableController extends Controller
{
      protected $customerbooking;

      public function __construct(CustomerBookingRepository $customerbooking)
    {
        $this->customerbooking = $customerbooking;
    }
    public function __invoke(ManageCustomerBookingRequest $request)
    {

       // dd($this->customerbooking);
        return Datatables::of($this->customerbooking->getForDataTable())
            ->escapeColumns(['id'])
             ->addColumn('booking_id', function ($customerbooking) {
                  return $customerbooking->booking_id;
              })
            // ->addColumn('transaction_id', function ($customerbooking) {
            //     return $customerbooking->transaction()->txn_id;
            // })
            // ->addColumn('confirmation_no', function ($customerbooking) {
            //     return $customerbooking->confirmation_no;
            // })
            // ->addColumn('lead_passenger', function ($customerbooking) {
            //       return $customerbooking->lead_passenger;
            //   })
              ->addColumn('booking_type', function ($customerbooking) {
                  return $customerbooking->booking_type;
              })
            ->addColumn('checkin', function ($customerbooking) {
                return $customerbooking->checkin;
            })
            // ->addColumn('checkout', function ($customerbooking) {
            //       return $customerbooking->checkout;
            //   })
            ->addColumn('actions', function ($customerbooking) {
                return $customerbooking->action_buttons;
            })
            ->make(true);
    }
}
