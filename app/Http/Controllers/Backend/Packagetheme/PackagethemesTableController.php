<?php

namespace App\Http\Controllers\Backend\Packagetheme;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Packagetheme\PackagethemeRepository;
use App\Http\Requests\Backend\Packagetheme\ManagePackagethemeRequest;

/**
 * Class PackagethemesTableController.
 */
class PackagethemesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var PackagethemeRepository
     */
    protected $packagetheme;

    /**
     * contructor to initialize repository object
     * @param PackagethemeRepository $packagetheme;
     */
    public function __construct(PackagethemeRepository $packagetheme)
    {
        $this->packagetheme = $packagetheme;
    }

    /**
     * This method return the data of the model
     * @param ManagePackagethemeRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManagePackagethemeRequest $request)
    {
        return Datatables::of($this->packagetheme->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('name',function ($packagetheme){
                return $packagetheme->name;
            })
            ->addColumn('created_at', function ($packagetheme) {
                return Carbon::parse($packagetheme->created_at)->toDateString();
            })
            ->addColumn('actions', function ($packagetheme) {
                return $packagetheme->action_buttons;
            })
            ->make(true);
    }
}
