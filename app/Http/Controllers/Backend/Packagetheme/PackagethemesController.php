<?php

namespace App\Http\Controllers\Backend\Packagetheme;

use App\Models\Packagetheme\Packagetheme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Packagetheme\PackagethemeRepository;
use App\Http\Requests\Backend\Packagetheme\ManagePackagethemeRequest;
use App\Http\Requests\Backend\Packagetheme\CreatePackagethemeRequest;
use App\Http\Requests\Backend\Packagetheme\StorePackagethemeRequest;
use App\Http\Requests\Backend\Packagetheme\EditPackagethemeRequest;
use App\Http\Requests\Backend\Packagetheme\UpdatePackagethemeRequest;
use App\Http\Requests\Backend\Packagetheme\DeletePackagethemeRequest;

/**
 * PackagethemesController
 */
class PackagethemesController extends Controller
{
    /**
     * variable to store the repository object
     * @var PackagethemeRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param PackagethemeRepository $repository;
     */
    public function __construct(PackagethemeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Packagetheme\ManagePackagethemeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(ManagePackagethemeRequest $request)
    {
        return view('backend.packagethemes.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreatePackagethemeRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreatePackagethemeRequest $request)
    {
        return view('backend.packagethemes.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePackagethemeRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackagethemeRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method

        $this->repository->create($input);
        //return with successfull message
        return redirect()->route('admin.packagethemes.index')->withFlashSuccess(trans('alerts.backend.packagethemes.created'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Packagetheme\Packagetheme  $packagetheme
     * @param  EditPackagethemeRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Packagetheme $packagetheme, EditPackagethemeRequest $request)
    {
        return view('backend.packagethemes.edit', compact('packagetheme'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePackagethemeRequestNamespace  $request
     * @param  App\Models\Packagetheme\Packagetheme  $packagetheme
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePackagethemeRequest $request, Packagetheme $packagetheme)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $packagetheme, $input );
        //return with successfull message
        return redirect()->route('admin.packagethemes.index')->withFlashSuccess(trans('alerts.backend.packagethemes.updated'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeletePackagethemeRequestNamespace  $request
     * @param  App\Models\Packagetheme\Packagetheme  $packagetheme
     * @return \Illuminate\Http\Response
     */
    public function destroy(Packagetheme $packagetheme, DeletePackagethemeRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($packagetheme);
        //returning with successfull message
        return redirect()->route('admin.packagethemes.index')->withFlashSuccess(trans('alerts.backend.packagethemes.deleted'));
    }
    
}
