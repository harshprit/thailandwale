<?php

namespace App\Http\Controllers\Backend\Activity;

use App\Models\Activity\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Activity\ActivityRepository;
use App\Http\Requests\Backend\Activity\ManageActivityRequest;
use App\Http\Requests\Backend\Activity\CreateActivityRequest;
use App\Http\Requests\Backend\Activity\StoreActivityRequest;
use App\Http\Requests\Backend\Activity\EditActivityRequest;
use App\Http\Requests\Backend\Activity\UpdateActivityRequest;
use App\Http\Requests\Backend\Activity\DeleteActivityRequest;
use App\Models\Destination\Destination;

/**
 * ActivitiesController
 */
class ActivitiesController extends Controller
{
    protected $activity_categories=[
        'Entertainment'=>'Entertainment',
        'Transfers'=>'Transfers',
    ];
    /**
     * variable to store the repository object
     * @var ActivityRepository
     */
    protected $activity;

   

    /**
     * contructor to initialize repository object
     * @param ActivityRepository $repository;
     */
    public function __construct(ActivityRepository $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Activity\ManageActivityRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(ManageActivityRequest $request)
    {
        return view('backend.activities.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateActivityRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreateActivityRequest $request)
    {
        $activityCategory=$this->activity_categories;
        $destination_list = Destination::pluck('title','id')->toArray();
        
        return view('backend.activities.create',compact('activityCategory','destination_list'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreActivityRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreActivityRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->activity->create($input);
        //return with successfull message
        return redirect()->route('admin.activities.index')->withFlashSuccess(trans('alerts.backend.activities.created'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Activity\Activity  $activity
     * @param  EditActivityRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity, EditActivityRequest $request)
    {
        $activityCategory=$this->activity_categories;
        $selectedCategory=$activity->category;
        $selectedDestination = $activity->destination;
        $destination_list = Destination::pluck('title','id')->toArray();
        return view('backend.activities.edit', compact('activity','activityCategory','selectedCategory','destination_list','selectedDestination'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateActivityRequestNamespace  $request
     * @param  App\Models\Activity\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateActivityRequest $request, Activity $activity)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->activity->update( $activity, $input );
        //return with successfull message
        return redirect()->route('admin.activities.index')->withFlashSuccess(trans('alerts.backend.activities.updated'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteActivityRequestNamespace  $request
     * @param  App\Models\Activity\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity, DeleteActivityRequest $request)
    {
        //Calling the delete method on repository
        $this->activity->delete($activity);
        //returning with successfull message
        return redirect()->route('admin.activities.index')->withFlashSuccess(trans('alerts.backend.activities.deleted'));
    }
    
}
