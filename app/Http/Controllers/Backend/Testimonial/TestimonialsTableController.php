<?php

namespace App\Http\Controllers\Backend\Testimonial;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Testimonial\TestimonialRepository;
use App\Http\Requests\Backend\Testimonial\ManageTestimonialRequest;

/**
 * Class TestimonialsTableController.
 */
class TestimonialsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var TestimonialRepository
     */
    protected $testimonial;

    /**
     * contructor to initialize repository object
     * @param TestimonialRepository $testimonial;
     */
    public function __construct(TestimonialRepository $testimonial)
    {
        $this->testimonial = $testimonial;
    }

    /**
     * This method return the data of the model
     * @param ManageTestimonialRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageTestimonialRequest $request)
    {
        return Datatables::of($this->testimonial->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('name', function ($testimonial) {
                return $testimonial->name;
            })
            ->addColumn('title', function ($testimonial) {
                return $testimonial->title;
            })
            ->addColumn('description', function ($testimonial) {
                return $testimonial->description;
            })
            ->addColumn('created_at', function ($testimonial) {
                return Carbon::parse($testimonial->created_at)->toDateString();
            })
            ->addColumn('actions', function ($testimonial) {
                return $testimonial->action_buttons;
            })
            ->make(true);
    }
}
