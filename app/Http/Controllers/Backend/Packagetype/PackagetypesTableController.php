<?php

namespace App\Http\Controllers\Backend\Packagetype;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Packagetype\PackagetypeRepository;
use App\Http\Requests\Backend\Packagetype\ManagePackagetypeRequest;

/**
 * Class PackagetypesTableController.
 */
class PackagetypesTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var PackagetypeRepository
     */
    protected $packagetype;

    /**
     * contructor to initialize repository object
     * @param PackagetypeRepository $packagetype;
     */
    public function __construct(PackagetypeRepository $packagetype)
    {
        $this->packagetype = $packagetype;
    }

    /**
     * This method return the data of the model
     * @param ManagePackagetypeRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManagePackagetypeRequest $request)
    {
        return Datatables::of($this->packagetype->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('name',function ($packagetype){
                return $packagetype->name;
            })
            ->addColumn('created_at', function ($packagetype) {
                return Carbon::parse($packagetype->created_at)->toDateString();
            })
            ->addColumn('actions', function ($packagetype) {
                return $packagetype->action_buttons;
            })
            ->make(true);
    }
}
