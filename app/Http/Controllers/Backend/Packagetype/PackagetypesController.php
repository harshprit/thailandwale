<?php

namespace App\Http\Controllers\Backend\Packagetype;

use App\Models\Packagetype\Packagetype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Packagetype\PackagetypeRepository;
use App\Http\Requests\Backend\Packagetype\ManagePackagetypeRequest;
use App\Http\Requests\Backend\Packagetype\CreatePackagetypeRequest;
use App\Http\Requests\Backend\Packagetype\StorePackagetypeRequest;
use App\Http\Requests\Backend\Packagetype\EditPackagetypeRequest;
use App\Http\Requests\Backend\Packagetype\UpdatePackagetypeRequest;
use App\Http\Requests\Backend\Packagetype\DeletePackagetypeRequest;

/**
 * PackagetypesController
 */
class PackagetypesController extends Controller
{
    /**
     * variable to store the repository object
     * @var PackagetypeRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param PackagetypeRepository $repository;
     */
    public function __construct(PackagetypeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Packagetype\ManagePackagetypeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(ManagePackagetypeRequest $request)
    {
        return view('backend.packagetypes.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreatePackagetypeRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function create(CreatePackagetypeRequest $request)
    {
        return view('backend.packagetypes.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePackagetypeRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackagetypeRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return redirect()->route('admin.packagetypes.index')->withFlashSuccess(trans('alerts.backend.packagetypes.created'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Packagetype\Packagetype  $packagetype
     * @param  EditPackagetypeRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Packagetype $packagetype, EditPackagetypeRequest $request)
    {
        return view('backend.packagetypes.edit', compact('packagetype'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePackagetypeRequestNamespace  $request
     * @param  App\Models\Packagetype\Packagetype  $packagetype
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePackagetypeRequest $request, Packagetype $packagetype)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $packagetype, $input );
        //return with successfull message
        return redirect()->route('admin.packagetypes.index')->withFlashSuccess(trans('alerts.backend.packagetypes.updated'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeletePackagetypeRequestNamespace  $request
     * @param  App\Models\Packagetype\Packagetype  $packagetype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Packagetype $packagetype, DeletePackagetypeRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($packagetype);
        //returning with successfull message
        return redirect()->route('admin.packagetypes.index')->withFlashSuccess(trans('alerts.backend.packagetypes.deleted'));
    }
    
}
