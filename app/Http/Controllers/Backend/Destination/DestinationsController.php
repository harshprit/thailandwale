<?php

namespace App\Http\Controllers\Backend\Destination;

use App\Models\Destination\Destination;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Destination\DestinationRepository;
use App\Http\Requests\Backend\Destination\ManageDestinationRequest;
use App\Http\Requests\Backend\Destination\CreateDestinationRequest;
use App\Http\Requests\Backend\Destination\StoreDestinationRequest;
use App\Http\Requests\Backend\Destination\EditDestinationRequest;
use App\Http\Requests\Backend\Destination\UpdateDestinationRequest;
use App\Http\Requests\Backend\Destination\DeleteDestinationRequest;
use App\Http\Controllers\Frontend\FrontendController;
use App\Token;


/**
 * DestinationsController
 */
class DestinationsController extends Controller
{
    /**
     * Destination Status.
     */
    protected $status = [
        'Published' => 'Published',
        'Draft'     => 'Draft',
        'InActive'  => 'InActive',
        'Scheduled' => 'Scheduled',
    ];

    /**
     * variable to store the repository object
     * @var DestinationRepository
     */
    protected $destination;
    
    protected $frontend;

    /**
     * @param \App\Repositories\Backend\Destination\DestinationRepository $destination
     */
    public function __construct(DestinationRepository $destination, FrontendController $frontend)
    {
        $this->destination = $destination;
        $this->frontend = $frontend;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Destination\ManageDestinationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(ManageDestinationRequest $request)
    {
        return view('backend.destinations.index')->with([
            'status'=> $this->status,
            ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateDestinationRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function create(ManageDestinationRequest $request)
    {   
        $destination_list = array();
        $destination_city_list = array();
        $hotel_city_list=$this->hotelCities();
        $flight_city_list=$this->getFlightCities();
       
        foreach($hotel_city_list as $hkey=>$hcity)
        {                
         $destination_list[$hkey]=$hkey.','.$hcity;
        }
        foreach($flight_city_list as $fkey=>$fcity)
        {                
         $destination_list[$fkey]=$fkey.','.$fcity;
        }
        foreach($destination_list as $dkey=>$dcity)
        {
            if(array_key_exists($dkey,$hotel_city_list) && array_key_exists($dkey,$flight_city_list))
            {
                $destination_city_list[$dkey.','.$flight_city_list[$dkey].','.$hotel_city_list[$dkey]]=$dkey;
            }
            else
            {
                $destination_city_list[$dkey.','.$dcity]=$dkey; 
            }
        }

        //dd($destination_list);
        return view('backend.destinations.create')->with([
              'status'         => $this->status,
              'destination_city'=>$destination_city_list,
        ]);;
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreDestinationRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDestinationRequest $request)
    {
       
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->destination->create($input);
        //return with successfull message
        return redirect()->route('admin.destinations.index')->withFlashSuccess(trans('alerts.backend.destinations.created'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Destination\Destination  $destination
     * @param  EditDestinationRequestNamespace  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Destination $destination, ManageDestinationRequest $request)
    {
        $destination_list = array();
        $destination_city_list = array();
        $hotel_city_list=$this->hotelCities();
        $flight_city_list=$this->getFlightCities();
       
        foreach($hotel_city_list as $hkey=>$hcity)
        {                
         $destination_list[$hkey]=$hkey.','.$hcity;
        }
        foreach($flight_city_list as $fkey=>$fcity)
        {                
         $destination_list[$fkey]=$fkey.','.$fcity;
        }
        foreach($destination_list as $dkey=>$dcity)
        {
            if(array_key_exists($dkey,$hotel_city_list) && array_key_exists($dkey,$flight_city_list))
            {
                $destination_city_list[$dkey.','.$flight_city_list[$dkey].','.$hotel_city_list[$dkey]]=$dkey;
            }
            else
            {
                $destination_city_list[$dkey.','.$dcity]=$dkey; 
            }
        }
        if($destination->flight_citycode!=null||$destination->flight_citycode!="")
        {
            $selectedDestination=$destination->title.','.$destination->flight_citycode.','.$destination->hotel_citycode;
        }
        else
        {
            $selectedDestination=$destination->title.','.$destination->hotel_citycode;
        }
        //dd($selectedDestination);
        return view('backend.destinations.edit')->with([
            'destination_city'=>$destination_city_list,
            'selectedDestination'=>$selectedDestination,
            'destination'     =>$destination,            
            'status'             => $this->status,
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateDestinationRequestNamespace  $request
     * @param  App\Models\Destination\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDestinationRequest $request, Destination $destination)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->destination->update( $destination, $input );
        //return with successfull message
        return redirect()->route('admin.destinations.index')->withFlashSuccess(trans('alerts.backend.destinations.updated'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteDestinationRequestNamespace  $request
     * @param  App\Models\Destination\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function destroy(Destination $destination, ManageDestinationRequest $request)
    {
        //Calling the delete method on repository
        $this->destination->delete($destination);
        //returning with successfull message
        return redirect()->route('admin.destinations.index')->withFlashSuccess(trans('alerts.backend.destinations.deleted'));
    }


    public function getFlightCities()
    {
        $cities=$this->getAirportDetails();
        $flightCities=array();
        foreach ($cities as $city)
        {
            $flightCities[$city['CityName']]=$city['CityCode'];
        }
        return $flightCities;
    }
    public function getAirportDetails()
{
$err_upTmpName = 'AIRPORT-CITY-COUNTRY DETAILS.csv';
$row = 0;
$list=array();
$count=0;
if (($handle = fopen($err_upTmpName, "r")) !== FALSE) {
    
	while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {

		if($row == 0){
			$row++;
		} else {

			// $data[0] = first name; $data[1] = last name; $data[2] = email; $data[3] = phone
			/*********************************************************************************************************************/
			if(!empty($data[0]) && !empty($data[1])&& ($data[5]=='TH'||$data[5]=='IN'))
      {
        $list[$count]['AirportName']=$data[0];
        $list[$count]['AirportCode']=$data[1];
        $list[$count]['CityName']=$data[2];
        $list[$count]['CityCode']=$data[3];
        $list[$count]['CountryName']=$data[4];
        $list[$count]['CountryCode']=$data[5];
        $list[$count]['Nationality']=$data[6];
        $list[$count]['Currency']=$data[7];
      }
      $count++;
		}

	}

} else {

	echo 'File could not be opened.';
}

fclose($handle);
return $list;
}
public function hotelCities()
{
    // $city_list=$this->getCities();
    $cities = $this->frontend->getHotelDestinations();
    $hotelCity=array();
    foreach ($cities as $city)
    {
        $hotelCity[$city['CityName']]=$city['CityId'];
        
    }
    
    return $hotelCity;
    
}
public function getCities()
{
    $userIp=gethostbyname(trim(`hostname`));
    
        $clientId="ApiIntegrationNew";       
        $token_id=Token::orderBy('id', 'desc')->first()->token;
        $country_code='TH';
        $city_data=array('ClientId'=>$clientId,'EndUserIp'=>$userIp,'TokenId'=>$token_id,'CountryCode'=>$country_code);
        $city_url="http://api.tektravels.com/SharedServices/SharedData.svc/rest/DestinationCityList";
        $decoded = $this->apiCalls($city_data,$city_url);
        //dd($decoded);
        return $decoded;
}
public function apiCalls($data,$url)
     {
        $search_json=json_encode($data);    
        $curl = curl_init($url);
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $search_json);
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_TIMEOUT,1000);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);  
        return $decoded;
     }
    
}
