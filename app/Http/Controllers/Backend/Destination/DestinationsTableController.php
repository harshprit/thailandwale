<?php

namespace App\Http\Controllers\Backend\Destination;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Destination\DestinationRepository;
use App\Http\Requests\Backend\Destination\ManageDestinationRequest;

/**
 * Class DestinationsTableController.
 */
class DestinationsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var DestinationRepository
     */
    protected $destination;

    /**
     * contructor to initialize repository object
     * @param DestinationRepository $destination;
     */
    public function __construct(DestinationRepository $destination)
    {
        $this->destination = $destination;
    }

    /**
     * This method return the data of the model
     * @param ManageDestinationRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageDestinationRequest $request)
    {
        return Datatables::of($this->destination->getForDataTable())
            ->escapeColumns(['title'])
            ->addColumn('status', function ($destination) {
                return $destination->status;
            })
            ->addColumn('created_by', function ($destination) {
                return $destination->user_name;
            })
            ->addColumn('created_at', function ($destination) {
                return Carbon::parse($destination->created_at)->toDateString();
            })
            ->addColumn('actions', function ($destination) {
                return $destination->action_buttons;
            })
            ->make(true);
    }
}
