<?php

namespace App\Http\Controllers\Backend\Package;

use App\Models\Package\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Package\PackageRepository;
use App\Http\Requests\Backend\Package\ManagePackageRequest;
use App\Http\Requests\Backend\Package\CreatePackageRequest;
use App\Http\Requests\Backend\Package\StorePackageRequest;
use App\Http\Requests\Backend\Package\EditPackageRequest;
use App\Http\Requests\Backend\Package\UpdatePackageRequest;
use App\Http\Requests\Backend\Package\DeletePackageRequest;
use App\Models\Destination\Destination;
use App\Models\Packagetheme\Packagetheme;
use App\Models\Packagetype\Packagetype;
use App\Models\Activity\Activity;
use App\Country;

/**
 * PackagesController
 */
class PackagesController extends Controller
{
    protected $status = [
        'Published' => 'Published',
        'Draft'     => 'Draft',
        'InActive'  => 'InActive',
        'Scheduled' => 'Scheduled',
    ];
    
    protected $inclusions=[
        'Flights'=>'Flights',
        'Hotels'=>'Hotels',
        'Meals'=>'Meals',
        'Activities'=>'Activities',
        'Transfers'=>'Transfers',
    ];

    
    /**
     * @var PackageRepository
     */
    protected $package;
    
    protected $startingCities;

    /**
     * @param \App\Repositories\Backend\Package\PackageRepository $package
     */
    public function __construct(PackageRepository $package)
    {
        $this->package = $package;
        $this->startingCities=Country::find(101)->cities()->get()->pluck('name','id')->toArray();
    }

    /**
     * @param \App\Http\Requests\Backend\Package\ManagePackageRequest $request
     *
     * @return mixed
     */
    public function index(ManagePackageRequest $request)
    {
        return view('backend.packages.index')->with([
            'status'=> $this->status,
        ]);
    }

    /**
     * @param \App\Http\Requests\Backend\Package\ManagePackageRequest $request
     *
     * @return mixed
     */
    public function create(ManagePackageRequest $request)
    {
        
        $packageDestination = Destination::pluck('title','id')->toArray(); //can also use Destination::pluck('title','id')->toArray();
        $packageTheme = ['' => 'Please Select Package Theme'] + Packagetheme::pluck('name','id')->toArray();  
        $packageType = Packagetype::pluck('name','id')->toArray();  
        $activities = Activity::pluck('title','id')->toArray();
       // dd($activities);
        return view('backend.packages.create')->with([
                'activities' => $activities,
                'packageDestination' => $packageDestination,
                'packageTheme' => $packageTheme,
                'packageType' => $packageType,
                'status'      => $this->status,
                'inclusions'=>$this->inclusions,
                'startingCities'=>$this->startingCities,
             ]);
    }

    /**
     * @param \App\Http\Requests\Backend\Package\StorePackageRequest $request
     *
     * @return mixed
     */
    public function store(StorePackageRequest $request)
    {
        $this->package->create($request->except('_token'));

        return redirect()
            ->route('admin.packages.index')
            ->with('flash_success', trans('alerts.backend.packages.created'));
    }

    /**
     * @param \App\Models\Package\Package                              $package
     * @param \App\Http\Requests\Backend\Package\ManagePackageRequest $request
     *
     * @return mixed
     */
    public function edit(Package $package, ManagePackageRequest $request)
    {
    
        $selectedDestination = [];
        $selectedDestNights = [];
        $selDestination_string = $package->destination_id;
        $selectedDestination_ar = explode("-",$selDestination_string);
        for($i=0;$i<count($selectedDestination_ar);$i++)
        {
            $selDest = explode(',',$selectedDestination_ar[$i]);

            $selectedDestination[$i]= $selDest[0];

            $selectedDestNights[$i]= $selDest[1]; 
        }
        
       $selectedTheme = $package->package_theme;
       $selectedInclusions=explode('-',$package->inclusion);
       $selectedActivities=explode('-',$package->activity);
       $selectedType_str = $package->package_type;
       $selectedType = explode(',',$selectedType_str);
        $packageDestination = Destination::getSelectData('title','id'); //can also use Destination::pluck('title','id')->toArray();
        //dd($packageDestination);
        $packageTheme = ['' => 'Please Select Package Theme'] + Packagetheme::pluck('name','id')->toArray();  
        $packageType =  Packagetype::pluck('name','id')->toArray();   
        $activities = Activity::pluck('title','id')->toArray();
        return view('backend.packages.edit')->with([
            'activities' => $activities,
            'selectedActivities'=> $selectedActivities,
            'packageDestination' => $packageDestination,
            'packageTheme' => $packageTheme,
            'packageType' => $packageType,
            'package'     => $package,
            'status'      => $this->status,
            'selectedDestination'=> $selectedDestination,
            'selectedDestinationNights'=>$selectedDestNights,
            'selectedTheme' =>$selectedTheme,
            'selectedType' =>$selectedType,
            'publishDate'   => \Carbon\Carbon::parse($package->publish_datetime)->format('m/d/Y h:i a'),
            'selectedInclusions'=>$selectedInclusions,
            'inclusions'=>$this->inclusions,
            'startingCities'=>$this->startingCities,
            'selectedCity'=>'Delhi'
        ]);
    }

    /**
     * @param \App\Models\Package\Package                              $package
     * @param \App\Http\Requests\Backend\Package\UpdatePackageRequest $request
     *
     * @return mixed
     */
    public function update(Package $package, UpdatePackageRequest $request)
    {
        $input = $request->all();

        $this->package->update($package, $request->except(['_token', '_method']));

        return redirect()
            ->route('admin.packages.index')
            ->with('flash_success', trans('alerts.backend.packages.updated'));
    }

    /**
     * @param \App\Models\Package\Package                              $package
     * @param \App\Http\Requests\Backend\Package\ManagePackageRequest $request
     *
     * @return mixed
     */
    public function destroy(Package $package, ManagePackageRequest $request)
    {
        $this->package->delete($package);

        return redirect()
            ->route('admin.packages.index')
            ->with('flash_success', trans('alerts.backend.packages.deleted'));
    }
    
}
