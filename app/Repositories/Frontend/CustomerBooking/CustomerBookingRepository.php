<?php

namespace App\Repositories\Frontend\CustomerBooking;

use App\Exceptions\GeneralException;
use App\Models\CustomerBooking\CustomerBooking;
use App\Repositories\BaseRepository;


/**
 * Class PagesRepository.
 */
class CustomerBookingRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = CustomerBooking::class;

    /*
    * Find page by page_slug
    */
   

    public function getForDataTable()
    {
        return $this->query()
       
            ->select([
                config('module.customerbookings.table').'.booking_id',
                config('module.customerbookings.table').'.reference_no',
                config('module.customerbookings.table').'.confirmation_no',
                config('module.customerbookings.table').'.lead_passenger',
                config('module.customerbookings.table').'.booking_type',
                config('module.customerbookings.table').'.checkin',
                config('module.customerbookings.table').'.checkout',
               
            ]);
    }
}
