<?php

namespace App\Repositories\Backend\Packagetype;

use App\Events\Backend\PackageType\PackageTypeCreated;
use App\Events\Backend\PackageType\PackageTypeDeleted;
use App\Events\Backend\PackageType\PackageTypeUpdated;
use DB;
use Carbon\Carbon;
use App\Models\Packagetype\Packagetype;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PackagetypeRepository.
 */
class PackagetypeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Packagetype::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.packagetypes.table').'.id',
                config('module.packagetypes.table').'.name',
                config('module.packagetypes.table').'.created_at',
                config('module.packagetypes.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        DB::transaction(function () use ($input) {
           
           
            //  $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
           // $input['created_by'] = access()->user()->id;
            
            if ($packagetype = Packagetype::create($input)) {
                // Inserting associated category's id in mapper table
            
                event(new PackageTypeCreated($packagetype));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.packagetypes.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Packagetype $packagetype
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Packagetype $packagetype, array $input)
    {
    	if ($packagetype->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.packagetypes.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Packagetype $packagetype
     * @throws GeneralException
     * @return bool
     */
    public function delete(Packagetype $packagetype)
    {
        if ($packagetype->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.packagetypes.delete_error'));
    }
}
