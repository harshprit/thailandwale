<?php

namespace App\Repositories\Backend\Package;

use App\Events\Backend\Package\PackageCreated;
use App\Events\Backend\Package\PackageDeleted;
use App\Events\Backend\Package\PackageUpdated;
use App\Exceptions\GeneralException;
use App\Models\Package\Package;

use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class PackageRepository.
 */
class PackageRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Package::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct()
    {
        $this->upload_path = 'img'.DIRECTORY_SEPARATOR.'package'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }
    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->leftjoin(config('access.users_table'), config('access.users_table').'.id', '=', config('module.packages.table').'.created_by')
            ->select([
                config('module.packages.table').'.id',
                config('module.packages.table').'.title',
                config('module.packages.table').'.publish_datetime',
                config('module.packages.table').'.status',
                config('module.packages.table').'.created_by',
                config('module.packages.table').'.created_at',
                config('access.users_table').'.first_name as user_name',
            ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input)
    {
    
        //$tagsArray = $this->createTags($input['tags']);
       // $categoriesArray = $this->createCategories($input['categories']);
        //unset($input['tags'], $input['categories']);

        DB::transaction(function () use ($input) {
            $input = $this->dayTitle($input);
            $input=$this->inclusionDetails($input);
            
             $input = $this->dayDestination($input);
            $input = $this->dayDescription($input);
           // $input = $this->dayImage(null,$input);
            $input = $this->packageType($input);
            // $input = $this->dayImage($input);
            $input['inclusion']=implode('-',$input['inclusion']);
            if(isset($input['activity'])){
                $input['activity']=implode('-',$input['activity']);
            }
            else
            {
                $input['activity']=""; 
            }
            $destination_nights_array = $input['destinationNights'];
            $destinationTags_array = $this->destinationTags($input['destination']);
            for($i=0;$i<count($destinationTags_array);$i++){
                $destinationTags_array[$i]=$destinationTags_array[$i].','.$destination_nights_array[$i];
            }
            $dtags_string = implode("-",$destinationTags_array);
            $input['destination_id']=$dtags_string;
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
            $input = $this->uploadImage($input);
            $input['created_by'] = access()->user()->id;
            if($input['standard_price']==null){
            $input['standard_price']=0;
            $input['standard_off']=0;            
            }
            elseif($input['standard_off']==null){
                $input['standard_off']=0;
            }
            if($input['delux_price']==null){
            $input['delux_price']=0;
            $input['delux_off']=0;
            }
            elseif($input['delux_off']==null){
                $input['delux_off']=0;
            }
            if($input['premium_price']==null){
            $input['premium_price']=0;
            $input['premium_off']=0;
            }
            elseif($input['premium_off']==null){
                $input['premium_off']=0;
            }
                                 
            if ($package = Package::create($input)) {
                // Inserting associated category's id in mapper table
            
                event(new PackageCreated($package));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.packages.create_error'));
        });
    }
/**
     * Update Package.
     *
     * @param \App\Models\Package\Package $package
     * @param array                  $input
     */
    public function update(Package $package, array $input)
    {
            $input = $this->dayTitle($input);
            //dd($input);
            $input=$this->inclusionDetails($input);
            $input = $this->dayDestination($input);
            $input = $this->dayDescription($input);
            // $input = $this->dayImage($package,$input);
            // $input = $this->dayImage($input);
            $input = $this->packageType($input);
            $input['inclusion']=implode('-',$input['inclusion']);
            if(isset($input['activity'])){
                $input['activity']=implode('-',$input['activity']);
            }
            else
            {
                $input['activity']=""; 
            }
            $destination_nights_array = $input['destinationNights'];

            $destinationTags_array = $this->destinationTags($input['destination']);
            for($i=0;$i<count($destinationTags_array);$i++){
                $destinationTags_array[$i]=$destinationTags_array[$i].','.$destination_nights_array[$i];
            }
            $dtags_string = implode("-",$destinationTags_array);
            $input['destination_id']=$dtags_string;
            $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
           
                if($input['standard_price']==null){
                $input['standard_price']=0;
                $input['standard_off']=0;            
                }
                elseif($input['standard_off']==null){
                    $input['standard_off']=0;
                }
                if($input['delux_price']==null){
                $input['delux_price']=0;
                $input['delux_off']=0;
                }
                elseif($input['delux_off']==null){
                    $input['delux_off']=0;
                }
                if($input['premium_price']==null){
                $input['premium_price']=0;
                $input['premium_off']=0;
                }
                elseif($input['premium_off']==null){
                    $input['premium_off']=0;
                }

            if (array_key_exists('featured_image', $input)) {
                $this->deleteOldFile($package);
                //dd($input);
                $input = $this->uploadImage($input);
            }
            
            $input['updated_by'] = access()->user()->id;
            
        //dd($input);
        //dd($package);
       // $day_details = Package::getSelectData('day_title','day_destination','day_description'); //'day_image
        //dd($day_details);
        DB::transaction(function () use ($input,$package) {
            
                               
            if ($package->update($input)) {
                // Inserting associated category's id in mapper table
            
                event(new PackageUpdated($package));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.packages.update_error'));
        });
}

public function destinationTags($dtags)
    {
        //Creating a new array for tags (newly created)
        $dtags_array = [];

        foreach ($dtags as $dtag) {
            if (is_numeric($dtag)) {
                $dtags_array[] = $dtag;
            } 
        }

        return $dtags_array;
    }

    public function packageType($input){
        $pack_type_array = $input['package_type'];
//dd($pack_type_array);
        $pack_type_val = implode(",",$pack_type_array);
        $input = array_merge($input, ['package_type'=>$pack_type_val]);
        return $input;
    }
    public function dayTitle($input)
    {
        $title_array = $input['dayTitle'];
        $title_val = implode(",",$title_array);
        $input = array_merge($input, ['day_title' => $title_val]);
        return $input;
    }
    public function inclusionDetails($input)
    {
        $data=array();
        if(isset($input['flight_details']))
        {
            $data['flights']=$input['flight_details'];
        }
        if(isset($input['hotel_details']))
        {
            $data['hotels']=$input['hotel_details'];
        }
        $input['inclusion_details']=json_encode($data);
        return $input;
        
    }
    public function dayDestination($input)
    {
        $destination_array = $input['dayDestination'];
        $destination_val = implode(",",$destination_array);
        $input = array_merge($input, ['day_destination' => $destination_val]);
        return $input;
    }
    public function dayDescription($input)
    {
        $description_array = $input['dayDescription'];
        $description_val = implode("---",$description_array);
        $input = array_merge($input, ['day_description' => $description_val]);
        return $input;
    }

    public function dayImage($package,$input)
    {
        $dimage = $input['dayImage'];
        if (isset($input['dayImage']) && !empty($input['dayImage'])) {
            $comma = '';
            $dayimg_array = '';
            for($i=0;$i<count($input['dayImage']);$i++){
                // if (array_key_exists($i, $dimage)) {
                //     $this->deleteOldFile($package);
                //     $input = $this->uploadImage($input);
                // }
                $fileName = time().$dimage[$i]->getClientOriginalName();
                $this->storage->put($this->upload_path.$fileName, file_get_contents($dimage[$i]->getRealPath()));
               
                $dayimg_array = $dayimg_array.$comma.''.$fileName;
                $comma = ',';
            }
            $input = array_merge($input, ['day_image' => $dayimg_array]);
           
            return $input;
        }
    }
   /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input)
    {
        $avatar = $input['featured_image'];

        if (isset($input['featured_image']) && !empty($input['featured_image'])) {
            
            $fileName = time().$avatar->getClientOriginalName();
            //$fileName = time().$avatar;
            $this->storage->put($this->upload_path.$fileName, file_get_contents($avatar->getRealPath()));

            $input = array_merge($input, ['featured_image' => $fileName]);

            return $input;
        }
    }
 

    /**
     * For deleting the respective model from storage
     *
     * @param Package $package
     * @throws GeneralException
     * @return bool
     */
    public function delete(Package $package)
    {
        if ($package->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.packages.delete_error'));
    }
    public function deleteOldFile($model)
    {
        $fileName = $model->featured_image;
        return $this->storage->delete($this->upload_path.$fileName);
    }
   
}
