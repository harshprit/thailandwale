<?php

namespace App\Repositories\Backend\Destination;

use App\Events\Backend\Destination\DestinationCreated;
use App\Events\Backend\Destination\DestinationDeleted;
use App\Events\Backend\Destination\DestinationUpdated;
use DB;
use Carbon\Carbon;
use App\Models\Destination\Destination;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


/**
 * Class DestinationRepository.
 */
class DestinationRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Destination::class;
    protected $upload_path;

     /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct()
    {
        $this->upload_path = 'img'.DIRECTORY_SEPARATOR.'destination'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
        ->leftjoin(config('access.users_table'), config('access.users_table').'.id', '=', config('module.destinations.table').'.created_by')
            ->select([
                config('module.destinations.table').'.id',
                config('module.destinations.table').'.title',
                config('module.destinations.table').'.status',
                config('module.destinations.table').'.created_at',
                config('module.destinations.table').'.created_by',
                config('access.users_table').'.first_name as user_name',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {   
        $title_val=$input['title'];
        $title_val_explode = explode(',',$title_val);
        if ($this->query()->where('title', $title_val_explode[0])->first()) {
            throw new GeneralException(trans('exceptions.backend.destinations.already_exists'));
        }
          DB::transaction(function () use ($input) {
            $title_val=$input['title'];
            $title_val_explode = explode(',',$title_val);
            $input['title']=$title_val_explode[0];
            if(is_numeric($title_val_explode[1]))
            {
                $input['hotel_citycode']=$title_val_explode[1];
            }
            else
            {
                $input['flight_citycode']=$title_val_explode[1];
            }
            if($title_val_explode[2]!=null||$title_val_explode[2]=="")
            {
                $input['hotel_citycode']=$title_val_explode[2];
            }
           // dd($title_val_explode[2]);
            $input = $this->uploadImage($input);
            $input['status'] = isset($input['status']) ? 1 : 0;
            $input['created_by'] = access()->user()->id;

            if ($destination = Destination::create($input)) {
                // Inserting associated category's id in mapper table
               
                event(new DestinationCreated($destination));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.destinations.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Destination $destination
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Destination $destination, array $input)
    {
        $title_val=$input['title'];
        $title_val_explode = explode(',',$title_val);
        $input['title']=$title_val_explode[0];
            if(is_numeric($title_val_explode[1]))
            {
                $input['hotel_citycode']=$title_val_explode[1];
            }
            else
            {
                $input['flight_citycode']=$title_val_explode[1];
            }
        if($title_val_explode[2]!=null||$title_val_explode[2]=="")
        {
            $input['hotel_citycode']=$title_val_explode[2];
        }
        $input['status'] = isset($input['status']) ? 1 : 0;
    	$input['updated_by'] = access()->user()->id;

        // Uploading Image
        if (array_key_exists('image', $input)) {
            $this->deleteOldFile($destination);
            $input = $this->uploadImage($input);
        }

        DB::transaction(function () use ($destination, $input) {
            if ($destination->update($input)) {

                event(new DestinationUpdated($destination));

                return true;
            }

            throw new GeneralException(
                trans('exceptions.backend.destinations.update_error')
            );
        });
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Destination $destination
     * @throws GeneralException
     * @return bool
     */
    public function delete(Destination $destination)
    {
        if ($destination->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.destinations.delete_error'));
    }
    public function uploadImage($input)
    {
        $avatar = $input['image'];

        if (isset($input['image']) && !empty($input['image'])) {
            $fileName = time().$avatar->getClientOriginalName();

            $this->storage->put($this->upload_path.$fileName, file_get_contents($avatar->getRealPath()));

            $input = array_merge($input, ['image' => $fileName]);

            return $input;
        }
    }

    /**
     * Destroy Old Image.
     *
     * @param int $id
     */
    public function deleteOldFile($model)
    {
        $fileName = $model->image;

        return $this->storage->delete($this->upload_path.$fileName);
    }
}
