<?php

namespace App\Repositories\Backend\CustomerBooking;

use App\Events\Backend\Destination\CustomerBookingDeleted;
use DB;
use Carbon\Carbon;
use App\Models\CustomerBooking\CustomerBooking;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


/**
 * Class DestinationRepository.
 */
class CustomerBookingRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = CustomerBooking::class;
   // protected $upload_path;

     /**
     * Storage Class Object.
     *
     *  @var \Illuminate\Support\Facades\Storage
     */
    //protected $storage;

    public function __construct()
    {
        // $this->upload_path = 'img'.DIRECTORY_SEPARATOR.'destination'.DIRECTORY_SEPARATOR;
        // $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->leftjoin('transaction','customer_bookings.transaction_id', 'transaction.id')
            ->select([
                config('module.customerbookings.table').'.booking_id',
                'transaction.txn_id',
                config('module.customerbookings.table').'.booking_type',
                config('module.customerbookings.table').'.checkin',

            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {

    }

    /**
     * For updating the respective Model in storage
     *
     * @param Destination $destination
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Destination $destination, array $input)
    {

    }

    /**
     * For deleting the respective model from storage
     *
     * @param Destination $destination
     * @throws GeneralException
     * @return bool
     */
    public function delete(CustomerBooking $customerbooking)
    {
        DB::transaction(function () use ($customerbooking) {
            if ($customerbooking->delete()) {


                event(new CustomerBookingDeleted($customerbooking));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.customer_bookings.delete_error'));
        });
    }


}
