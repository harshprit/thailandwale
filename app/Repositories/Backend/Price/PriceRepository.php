<?php

namespace App\Repositories\Backend\Price;

use DB;
use Carbon\Carbon;
use App\Models\Price\Price;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PriceRepository.
 */
class PriceRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Price::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.prices.table').'.id',
                config('module.prices.table').'.service',
                config('module.prices.table').'.type',
                config('module.prices.table').'.amount_type',
                config('module.prices.table').'.amount',
                config('module.prices.table').'.created_at',
                config('module.prices.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        $price = self::MODEL;
        $price = new $price();
        if ($price->save($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.prices.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Price $price
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Price $price, array $input)
    {
    	if ($price->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.prices.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Price $price
     * @throws GeneralException
     * @return bool
     */
    public function delete(Price $price)
    {
        if ($price->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.prices.delete_error'));
    }
}
