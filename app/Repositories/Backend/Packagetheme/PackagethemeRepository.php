<?php

namespace App\Repositories\Backend\Packagetheme;

use App\Events\Backend\PackageTheme\PackageThemeCreated;
use App\Events\Backend\PackageTheme\PackageThemeDeleted;
use App\Events\Backend\PackageTheme\PackageThemeUpdated;
use DB;
use Carbon\Carbon;
use App\Models\Packagetheme\Packagetheme;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Storage;
/**
 * Class PackagethemeRepository.
 */
class PackagethemeRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Packagetheme::class;
    protected $upload_path;
    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    protected $storage;
    public function __construct()
    {
        $this->upload_path = 'img'.DIRECTORY_SEPARATOR.'packagetheme'.DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }
    public function getForDataTable()
    {
        return $this->query()       
            ->select([
                config('module.packagethemes.table').'.id',
                config('module.packagethemes.table').'.name',
                config('module.packagethemes.table').'.created_at',
                config('module.packagethemes.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.packagethemes.already_exists'));
        }
        DB::transaction(function () use ($input) {
           
           
            //  $input['publish_datetime'] = Carbon::parse($input['publish_datetime']);
           // $input['created_by'] = access()->user()->id;
           //dd($input);
           $input = $this->uploadImage($input);

            if ($packagetheme = Packagetheme::create($input)) {
                // Inserting associated category's id in mapper table
            
                event(new PackageThemeCreated($packagetheme));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.packagethemes.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Packagetheme $packagetheme
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Packagetheme $packagetheme, array $input)
    {
        if (array_key_exists('image', $input)) {
            $this->deleteOldFile($packagetheme);
            $input = $this->uploadImage($input);
        }
        DB::transaction(function () use ($packagetheme, $input) {
            if ($packagetheme->update($input)){

                event(new PackageThemeUpdated($packagetheme));

                return true;
            }

        throw new GeneralException(trans('exceptions.backend.packagethemes.update_error'));
        });
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Packagetheme $packagetheme
     * @throws GeneralException
     * @return bool
     */

    public function delete(Packagetheme $packagetheme)
    {
        if ($packagetheme->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.packagethemes.delete_error'));
    }
    
    public function uploadImage($input)
    {
       // dd($input);
        $avatar = $input['image'];
        //dd($avatar);
        if (isset($input['image']) && !empty($input['image'])) {
            $fileName = time().$avatar->getClientOriginalName();

            $this->storage->put($this->upload_path.$fileName, file_get_contents($avatar->getRealPath()));

            $input = array_merge($input, ['image' => $fileName]);

            return $input;
        }
    }
    public function deleteOldFile($model)
    {
        $fileName = $model->image;
        if($fileName!="")
        return $this->storage->delete($this->upload_path.$fileName);
    }
}
