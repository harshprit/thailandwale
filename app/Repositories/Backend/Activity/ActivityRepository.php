<?php

namespace App\Repositories\Backend\Activity;

use App\Events\Backend\Activity\ActivityCreated;
use App\Events\Backend\Activity\ActivityDeleted;
use App\Events\Backend\Activity\ActivityUpdated;
use DB;
use Carbon\Carbon;
use App\Models\Activity\Activity;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ActivityRepository.
 */
class ActivityRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Activity::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.activities.table').'.id',
                config('module.activities.table').'.title',                
                config('module.activities.table').'.created_at',
                config('module.activities.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        DB::transaction(function () use ($input) {
                                            
            if ($activity = Activity::create($input)) {
                // Inserting associated category's id in mapper table
            
                event(new ActivityCreated($activity));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.packages.create_error'));
        });
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Activity $activity
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Activity $activity, array $input)
    {
    	if ($activity->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.activities.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Activity $activity
     * @throws GeneralException
     * @return bool
     */
    public function delete(Activity $activity)
    {
        if ($activity->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.activities.delete_error'));
    }
}
