<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable = [
        'userId',
        'description',
        'amountAdded',
        'amountUsed',
        'amountBalance',
        
    ];
}
