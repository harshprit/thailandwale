<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponUsed extends Model
{
    protected $fillable=[
        'userId',
        'couponId',
        'discount',
        'service',
    ];
}
