<?php

namespace App\Listeners\Backend\Testimonial;

use App\Events\Backend\Testimonial\Testimonials;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestimonialsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Testimonials  $event
     * @return void
     */
    public function handle(Testimonials $event)
    {
        //
    }
}
