<?php

namespace App\Listeners\Backend\Coupon;

use App\Events\Backend\Coupon\Coupon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CouponListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Coupon  $event
     * @return void
     */
    public function handle(Coupon $event)
    {
        //
    }
}
