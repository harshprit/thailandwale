<?php

namespace App\Listeners\Backend\Destination;

use App\Events\Backend\Destination\DestinationDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DestinationDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DestinationDeleted  $event
     * @return void
     */
    public function handle(DestinationDeleted $event)
    {
        //
    }
}
