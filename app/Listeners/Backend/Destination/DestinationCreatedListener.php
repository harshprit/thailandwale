<?php

namespace App\Listeners\Backend\Destination;

use App\Events\Backend\Destination\DestinationCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DestinationCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DestinationCreated  $event
     * @return void
     */
    public function handle(DestinationCreated $event)
    {
        //
    }
}
