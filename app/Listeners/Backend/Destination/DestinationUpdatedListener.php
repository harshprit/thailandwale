<?php

namespace App\Listeners\Backend\Destination;

use App\Events\Backend\Destination\DestinationUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DestinationUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DestinationUpdated  $event
     * @return void
     */
    public function handle(DestinationUpdated $event)
    {
        //
    }
}
