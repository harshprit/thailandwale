<?php

namespace App\Listeners\Backend\Package;

use App\Events\Backend\Package\PackageCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PackageCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PackageCreated  $event
     * @return void
     */
    public function handle(PackageCreated $event)
    {
        //
    }
}
