<?php

namespace App\Listeners\Backend\Package;

use App\Events\Backend\Package\PackageUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PackageUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PackageUpdated  $event
     * @return void
     */
    public function handle(PackageUpdated $event)
    {
        //
    }
}
