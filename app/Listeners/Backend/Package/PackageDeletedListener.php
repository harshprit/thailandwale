<?php

namespace App\Listeners\Backend\Package;

use App\Events\Backend\Package\PackageDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PackageDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PackageDeleted  $event
     * @return void
     */
    public function handle(PackageDeleted $event)
    {
        //
    }
}
