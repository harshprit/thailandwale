<?php

namespace App\Listeners\Backend\Package;

/**
 * Class PackageEventListener.
 */
class PackageEventListener
{
    /**
     * @var string
     */
    private $history_slug = 'Package';

    /**
     * @param $event
     */
    public function onCreated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->package->id)
            ->withText('trans("history.backend.package.created") <strong>'.$event->package->name.'</strong>')
            ->withIcon('plus')
            ->withClass('bg-green')
            ->log();
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->package->id)
            ->withText('trans("history.backend.package.updated") <strong>'.$event->package->name.'</strong>')
            ->withIcon('save')
            ->withClass('bg-aqua')
            ->log();
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        history()->withType($this->history_slug)
            ->withEntity($event->package->id)
            ->withText('trans("history.backend.package.deleted") <strong>'.$event->package->name.'</strong>')
            ->withIcon('trash')
            ->withClass('bg-maroon')
            ->log();
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Package\PackageCreated::class,
            'App\Listeners\Backend\Package\PackageEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Package\PackageUpdated::class,
            'App\Listeners\Backend\Package\PackageEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Package\PackageDeleted::class,
            'App\Listeners\Backend\Package\PackageEventListener@onDeleted'
        );
    }
}
