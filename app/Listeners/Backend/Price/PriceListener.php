<?php

namespace App\Listeners\Backend\Price;

use App\Events\Backend\Price\Price;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PriceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Price  $event
     * @return void
     */
    public function handle(Price $event)
    {
        //
    }
}
