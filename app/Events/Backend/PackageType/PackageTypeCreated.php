<?php

namespace App\Events\Backend\PackageType;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PackageTypeCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var
     */ 
    public $packagetypes;

    /**
     * @param $package
     */
    public function __construct($packagetypes)
    {
        $this->packagetypes = $packagetypes;
    }
}
