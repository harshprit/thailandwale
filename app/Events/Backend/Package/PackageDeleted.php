<?php

namespace App\Events\Backend\Package;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PackageDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

     /**
     * @var
     */ 
    public $packages;

    /**
     * @param $package
     */
    public function __construct($packages)
    {
        $this->packages = $packages;
    }
}
