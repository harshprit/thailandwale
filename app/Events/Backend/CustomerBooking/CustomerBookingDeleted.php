<?php

namespace App\Events\Backend\CustomerBooking;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CustomerBookingDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   
    /**
     * @var
     */
    public $customerbooking;

    /**
     * @param $destinations
     */
    public function __construct($customerbooking)
    {
        $this->customerbooking = $customerbooking;
    }
}
