<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Exception,
    Session;

class City extends BaseModel
{
    protected $fillable = [
        'name',
        'state_id',
        ];
    protected $table='cities';
    

}
