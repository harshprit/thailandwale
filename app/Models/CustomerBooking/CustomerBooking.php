<?php

namespace App\Models\CustomerBooking;

use App\Models\BaseModel;
use App\Models\CustomerBooking\Traits\Attribute\CustomerBookingAttribute;
use App\Models\CustomerBooking\Traits\Relationship\CustomerBookingRelationship;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Exception,Session;
class CustomerBooking extends BaseModel
{
    use ModelTrait,
        SoftDeletes,
         CustomerBookingAttribute,
         CustomerBookingRelationship
        {
            // BlogAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    protected $fillable = [
        'user_id',
        'booking_id',
        'transaction_id',
        'booking_type',
        'booking_detail',
        'checkin',
    ];

    protected $dates = [
        'date_created',

    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('module.customerbookings.table');
    }
    public function getAllBookingDetails(){
         try{
           $query = $this
                    ->leftjoin('transaction','customer_bookings.transaction_id','=','transaction.id')
                    ->select('transaction.*','customer_bookings.*')
                    ->get();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }
     public function getSingleBookingDetails($bid){
         try{
           $query = $this
                    ->select('customer_bookings.booking_detail')
                    ->where('booking_id','=',$bid)
                    ->get();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }
}
