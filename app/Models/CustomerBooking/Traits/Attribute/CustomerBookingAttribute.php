<?php

namespace App\Models\CustomerBooking\Traits\Attribute;

/**
 * Class BlogAttribute.
 */
trait CustomerBookingAttribute
{
     // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='label label-success'>".trans('labels.general.active').'</label>';
        }

        return "<label class='label label-danger'>".trans('labels.general.inactive').'</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }

    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                   '.$this->getDeleteButtonAttribute("delete-customer_bookings", "admin.customer_bookings.destroy").'
                </div>';
    }
}
