<?php
namespace App\Models\Transaction;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Exception,
    Session;
    
class Transaction extends BaseModel{
    protected $fillable = [
        'txn_id',
        'status',
        'amount',
        'date_created',
        ];
    protected $table;
    
  public function  __construct(array $attributes=[]){
        parent::__construct($attributes);
        $this->table = config('module.transaction.table');
    }
}
