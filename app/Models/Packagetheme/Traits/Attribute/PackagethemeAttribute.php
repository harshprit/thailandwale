<?php

namespace App\Models\Packagetheme\Traits\Attribute;

/**
 * Class PackagethemeAttribute.
 */
trait PackagethemeAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                '.$this->getEditButtonAttribute("edit-packagetheme", "admin.packagethemes.edit").'
                '.$this->getDeleteButtonAttribute("delete-packagetheme", "admin.packagethemes.destroy").'
                </div>';
    }
}
