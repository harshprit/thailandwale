<?php

namespace App\Models\Packagetheme;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Packagetheme\Traits\Attribute\PackagethemeAttribute;
use App\Models\Packagetheme\Traits\Relationship\PackagethemeRelationship;

class Packagetheme extends Model
{
    use ModelTrait,
        PackagethemeAttribute,
    	PackagethemeRelationship {
            // PackagethemeAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/5.4/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table;

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
         ];

    /**
     * Default values for model fields
     * @var array
     */
    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('module.packagethemes.table');
    }
    public function getAllPackageTheme(){
        try{
           $query = $this
                    ->get();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }
   
}
