<?php

namespace App\Models\Feedback;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends BaseModel
{
    use ModelTrait,
        SoftDeletes;
     

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table='feedback';

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The default values for attributes.
     *
     * @var array
     */



}
