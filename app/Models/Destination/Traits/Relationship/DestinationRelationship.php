<?php

namespace App\Models\Destination\Traits\Relationship;

/**
 * Class DestinationRelationship
 */
trait DestinationRelationship
{
   
        /**
         * Destination belongs to relationship with state.
         */
        public function creator()
        {
            return $this->belongsTo(User::class, 'created_by');
        }
  
    
}
