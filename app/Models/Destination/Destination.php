<?php

namespace App\Models\Destination;

use App\Models\BaseModel;
use App\Models\ModelTrait;
//use Illuminate\Database\Eloquent\Model;
use App\Models\Destination\Traits\Attribute\DestinationAttribute;
use App\Models\Destination\Traits\Relationship\DestinationRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class Destination extends BaseModel
{
    use ModelTrait,
        SoftDeletes,
        DestinationAttribute,
    	DestinationRelationship {
            // DestinationAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

        protected $fillable = [
            'title',
            'hotel_citycode',
            'flight_citycode',
            'description',
            'image',
            'status',
            'created_by',
        ];
    
        protected $dates = [
            'created_at',
            'updated_at',
        ];
    

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/5.4/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table;

    
    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('module.destinations.table');
    }

    public function getAllDestination(){
        try{
           $query = $this
                    ->where('status','=','1')
                    ->get();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }
}
