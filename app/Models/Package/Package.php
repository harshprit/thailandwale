<?php

namespace App\Models\Package;

//use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Package\Traits\Attribute\PackageAttribute;
use App\Models\Package\Traits\Relationship\PackageRelationship;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Exception,
    Session;


class Package extends BaseModel
{
    use ModelTrait,
        SoftDeletes,
        PackageAttribute,
        PackageRelationship {
            
        }
       
    protected $fillable = [
        'title',
        'featured_image',
        'destination_id',
        'day_title',
        'day_destination',
        'day_description',
        'day_image',
        'no_days',
        'offer',
        'cost',
        'content',
        'inclusion',
        'inclusion_details',
        'terms_and_conditions',
        'exclusion',
        'package_theme',
        'package_type',
        'activity',
        'standard_price',
        'standard_off',
        'delux_price',
        'delux_off',
        'premium_price',
        'premium_off',        
        'meta_title',
        'cannonical_link',
        'slug',
        'meta_keywords',
        'meta_description',
        'status',
        'publish_datetime',
        'created_by',
        'updated_by',
    ];

    protected $dates = [
        'publish_datetime',
        'created_at',
        'updated_at',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('module.packages.table');
    }
    /*-----------------------------------------------------------------------------------
    -------------------------------Frontend Process--------------------------------------
    ----------------------------------------------------------------------------------- */
    public function getAllPackage(){
        try{
           $query = $this
                    ->where('packages.status','=','Published')
                    ->get();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }
    public function getAllPackagesByDestination(){
        try{
            
           $query = $this
                    ->where('status','=','Published')
                    ->get();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }
    public function getAllPackagesByTheme(){
        try{
           $query = $this
                    ->where('status','=','Published')
                    ->get();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }
    public function getPackageById($pack_id)
    {
        try{
            $query = $this
                    ->where('slug','=',$pack_id)
                    ->first();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }

}
