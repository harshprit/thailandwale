<?php

namespace App\Models\Package\Traits\Relationship;

/**
 * Class PackageRelationship
 */
trait PackageRelationship
{
    /*
    * put you model relationships here
    * Take below example for reference
    */
    /*
    public function users() {
        //Note that the below will only work if user is represented as user_id in your table
        //otherwise you have to provide the column name as a parameter
        //see the documentation here : https://laravel.com/docs/5.4/eloquent-relationships
        $this->belongsTo(User::class);
    }
     */
     /**
     * Package has many relationship with categories.
     */
    public function destination()
    {
        return $this->belongsToMany(Destination::class, 'package_map_destinations', 'package_id', 'destination_id');
    }
}
