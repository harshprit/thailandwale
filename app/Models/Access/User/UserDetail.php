<?php

namespace App\Models\Access\User;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable=[
        'user_id',
        'mobile',
        'dob',
        'address',
        'city',
        'state',
        'pincode',
        'image',
    ];    
}
