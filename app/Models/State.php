<?php

namespace App\Models;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Exception,
    Session;

class State extends BaseModel
{
    protected $fillable = [
        'name',
        'country_id',
        ];
    protected $table='states';
    

}
