<?php

namespace  App\Models\PackageMapDestination;

use Illuminate\Database\Eloquent\Model;

class PackageMapDestination extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'package_map_destinations';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
