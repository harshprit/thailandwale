<?php

namespace App\Models\Packagetype\Traits\Attribute;

/**
 * Class PackagetypeAttribute.
 */
trait PackagetypeAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                '.$this->getEditButtonAttribute("edit-packagetype", "admin.packagetypes.edit").'
                '.$this->getDeleteButtonAttribute("delete-packagetype", "admin.packagetypes.destroy").'
                </div>';
    }
}
