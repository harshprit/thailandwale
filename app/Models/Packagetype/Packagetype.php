<?php

namespace App\Models\Packagetype;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Packagetype\Traits\Attribute\PackagetypeAttribute;
use App\Models\Packagetype\Traits\Relationship\PackagetypeRelationship;

class Packagetype extends Model
{
    use ModelTrait,
        PackagetypeAttribute,
    	PackagetypeRelationship {
            // PackagetypeAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/5.4/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table;

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('module.packagetypes.table');
    }
    public function getAllPackageType(){
        try{
           $query = $this
                    ->get();
                return $query;
        }
        catch(Exception $e){
            throw $e;
        }
    }
}
