<?php

namespace App\Models\Activity;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Activity\Traits\ActivityAttribute;
use App\Models\Activity\Traits\ActivityRelationship;
use DB;
class Activity extends Model
{
    use ModelTrait,
        ActivityAttribute,
    	ActivityRelationship {
            // ActivityAttribute::getEditButtonAttribute insteadof ModelTrait;
        }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/5.4/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = [
        'title',
        'destination',
        'category',
        'description',
        'price',
        'inclusions',
        'exclusions',
        'suitable_for',

    ];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [

    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    protected $table;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('module.activities.table');
    }
    public function getAllActivity()
    {
        $query=$this
        ->leftjoin('destinations as dest','dest.id','=','activities.destination')
        ->select('dest.title as d_title','activities.*')
        ->get();
        return $query;
    }
    public function getAllActivityBySource($source)
    {
        $query=$this
                ->leftjoin('destinations as dest','dest.id','=','activities.destination')
                ->select('dest.title as d_title','activities.*')
                ->where('activities.category','=',$source)
                ->get();
            return $query;
    }
    public function getAllActivityBySourceAndRegion($source,$region)
    {
        $query=$this
        ->leftjoin('destinations as dest','dest.id','=','activities.destination')
        ->where('activities.category','=',$source)
        ->where('dest.title','=',$region)
        ->select('dest.title as d_title','activities.*')
        ->get();
        return $query;
    }
    public function getCountByDestination($source=null){
        if(!empty($source))
        {
        $query =    $this
                    ->leftjoin('destinations','destinations.id','=','activities.destination')
                    ->where('activities.category','=',$source)
                    ->select( DB::raw('count(activities.destination) as total'),'destinations.title as d_title')                    
                    ->groupBy('activities.destination')
                    ->get();    
        }
        else
        {
            $query =    $this
                    ->leftjoin('destinations','destinations.id','=','activities.destination')
                    ->select( DB::raw('count(activities.destination) as total'),'destinations.title as d_title')                    
                    ->groupBy('activities.destination')
                    ->get();
            
        }
        
        return $query;
    }
    

}
