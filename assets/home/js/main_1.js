$(function () {
    var date = new Date();

    $("#datepicker").datepicker({
   startDate: date,
   format: "dd MM yy"
   //// set the minDate to the today's date
    // you can add other options here
    });
     $("#datepicker2").datepicker({
   startDate: date,
   format: "dd MM yy"
   //// set the minDate to the today's date
    // you can add other options here
    });
     $("#datepicker3").datepicker({
   startDate: date,
   format: "dd MM yy"
   //// set the minDate to the today's date
    // you can add other options here
    });
});

function travelClassInfo() {
    var travel_class = $("#travel_class").val();
    if (travel_class == "" || travel_class == null)
    {
        travel_class = "Economy";
    } else if (travel_class == 2) {
        travel_class = "Economy";
    } else if (travel_class == 3) {
        travel_class = "Premium Economy";
    } else if (travel_class == 4) {
        travel_class = "Business";
    } else {
        travel_class = "Economy";
    }
    return travel_class;
}
$(document).ready(function () {

//increment the value of passenger
    $("#adult_up").on("click", function () {
        var ad_count = $("#adult_one").val();
        ad_count++;
        $("#adult_one").val(ad_count);

        var t_class = travelClassInfo();

        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count + "|" + t_class);
    });
    $("#child_up").on("click", function () {

        var ch_count = $("#child_one").val();
        ch_count++;
        $("#child_one").val(ch_count);

        var t_class = travelClassInfo();

        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count + "|" + t_class);
    });
    $("#inf_up").on("click", function () {
        var inf_count = $("#infants_one").val();
        inf_count++;
        $("#infants_one").val(inf_count);

        var t_class = travelClassInfo();

        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count + "|" + t_class);
    });
//decrement the value of passenger
    $("#adult_dn").on("click", function () {
        var ad_count = $("#adult_one").val();
        ad_count--;

        if (ad_count >= 1) {
            $("#adult_one").val(ad_count);
            var passengers_count = $("#passenger_count").val();
            passengers_count--;
        } else {
            var passengers_count = $("#passenger_count").val();
        }

        var t_class = travelClassInfo();

        $("#passenger_count").val(passengers_count);
        // alert(passengers_count);
        $("#passengers_one").val(passengers_count + "|" + t_class);
    });
    $("#child_dn").on("click", function () {
        var ch_count = $("#child_one").val();
        ch_count--;

        if (ch_count >= 0) {
            $("#child_one").val(ch_count);
            var passengers_count = $("#passenger_count").val();
            passengers_count--;
        } else {
            var passengers_count = $("#passenger_count").val();
        }

        var t_class = travelClassInfo();

        $("#passenger_count").val(passengers_count);
        //alert(passengers_count);
        $("#passengers_one").val(passengers_count + "|" + t_class);
    });
    $("#inf_dn").on("click", function () {
        var inf_count = $("#infants_one").val();
        inf_count--;

        if (inf_count >= 0) {
            $("#infants_one").val(inf_count);
            var passengers_count = $("#passenger_count").val();
            passengers_count--;
        } else {
            var passengers_count = $("#passenger_count").val();
        }

        var t_class = travelClassInfo();

        $("#passenger_count").val(passengers_count);
        //alert(passengers_count);
        $("#passengers_one").val(passengers_count + "|" + t_class);
    });

    $(".flight-class").click(function () {

        $(".flight-class").removeClass("active");
        // $(".tab").addClass("active"); // instead of this do the below
        $('#travel_class').val($(this).val());
        $(this).addClass("active");
        // alert($(this).val());
        var passengers_count = $("#passenger_count").val();
        //alert(passengers_count);
        var t_class = travelClassInfo();

        $("#passengers_one").val(passengers_count + "|" + t_class);


    });

    $("#input3").on("change", function () {
        // alert("hello");
        var source = $("#input2").val();
        var destination = $("#input3").val();
        // alert(source+" "+destination);
        if (source == "" && destination == "") {
            $("#input2").focus();
        } else if (source == destination)
        {
            alert("Source and Destination can not be same.");
            $('#input3 option[value=""]').prop('selected', true);
            $("#input3").text("Destination");
            $("#input3").focus();
//            $("#input3").trigger("chosen:updated");
        }


    });
      $("#change").on('click', function() {
    var pickup = $('#input2').val();
    console.log(pickup);
    $('#input2').val($('#input3').val());
    $('#input2').val(pickup);
    $("#input2").trigger("chosen:updated");
    $("#input3").trigger("chosen:updated");

  });
    $("#flightSearch_form").submit(function () {
        //alert($("#input2").val());
        var from = $("#input2").val();
        var to = $("#input3").val();
        var start_date = $("#datepicker").val();
        var end_date = $("#datepicker1").val();
        //alert(start_date);
        preloader_on();
        if (from == "" || from == null)
        {
            alert("Source can not be blank");
            $("#select2-input2-container").css({'background-color': 'rgba(251, 0, 0, 0.4)'});
            preloader_off();
            return false;
        } else if (to == "" || to == null)
        {
            alert("Destination can not be blank");
            $("#select2-input3-container").css({'background-color': 'rgba(251, 0, 0, 0.4)'});
            return false;
        } else if (start_date == "" || start_date == null) {
            $("#datepicker").focus();
            return false;
        } else if (end_date == "" || end_date == null) {
            if ($("input[name='journey_type'][value='1']").prop("checked") == true)
            {
                //   alert($("input[name='journey_type'][value=1]").prop("checked"));
            } else {
                $("#datepicker1").focus();
                return false;
            }
        } else
            return true;
    });
});
$('#passengers_one').on('click', function () {
    $('#passengerCount').show();
});
$('#done').on('click', function () {
    $('#passengerCount').hide();
});

  function preloader_on()
{
   $("body").prepend('<div id="preloader"><img src="./assets/home/images/preloadernew.gif" alt=""/>  </div>');
}
function preloader_off()
{
 $("#preloader").remove();
}
    
      particlesJS.load('particles-js-footer', './assets/home/js/partilejsFooter.json', function() {
    console.log('callback - particles.js config loaded');
  });
  
  preloader_on();
  $( document ).ready(function() {
      preloader_off();
  });
 $("#idForm").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');

    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
            window.location.href = data.html; // show response from the php script.
           }
         });
 $(function() {
            var availableTutorials = [
               "ActionScript",
               "Bootstrap",
               "C",
               "C++",
            ];
            $( "#automplete-2" ).autocomplete({
               source: availableTutorials,
               autoFocus:true
            });
         });

});