$(document).ready( () => {
  $('.minus-button').click( (e) => {

    const currentInput = $(e.currentTarget).parent().prev()[0];

    let minusInputValue = $(currentInput).html();

    minusInputValue --;
    $(currentInput).html(minusInputValue);
  });

  $('.plus-button').click( (e) => {

    const currentInput = $(e.currentTarget).parent().prev()[0];

    let plusInputValue = $(currentInput).html();

    plusInputValue ++;
    $(currentInput).html(plusInputValue);
  });
});

$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        $("div.bhoechie-tab.1>div.bhoechie-tab-content.1.bhangi").addClass("active");
        $("div.bhoechie-tab.2>div.bhoechie-tab-content.2.bhangi").addClass("active");
    });
});
$(document).ready(function() {
    $("div.bhoechie-tab-menu.1>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab.1>div.bhoechie-tab-content.1").removeClass("active");
        $("div.bhoechie-tab.1>div.bhoechie-tab-content.1").eq(index).addClass("active");
    });
});
$(document).ready(function() {
    $("div.bhoechie-tab-menu.2>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab.2>div.bhoechie-tab-content.2").removeClass("active");
        $("div.bhoechie-tab.2>div.bhoechie-tab-content.2").eq(index).addClass("active");
    });
});

$(document).ready(function() {
    var $btnSets = $('#responsive'),
    $btnLinks = $btnSets.find('a');
 
    $btnLinks.click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.user-menu>div.user-menu-content").removeClass("active");
        $("div.user-menu>div.user-menu-content").eq(index).addClass("active");
    });
});

$( document ).ready(function() {
    $("[rel='tooltip']").tooltip();    
 
    $('.view').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 
});

$.fn.modal.Constructor.prototype.enforceFocus = function() {};

// Owl Carousel
// Testimonial Slider

jQuery('.testimonials-section .three-items-carousel').owlCarousel({
			items:3,
			autoplay: true,
			autoplayTimeout: 3500,
			animateOut: 'slideOutLeft',
			animateIn: 'slideInLeft',
			loop: true,
			responsiveClass:true,
    responsive:{
        300:{
            items:1,
            nav:true
        },
        800:{
            items:3,
            nav:true
        }
    },
			navText:[ '', '' ]
		})
