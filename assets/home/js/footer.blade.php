 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script src="{{asset('assets/home/chosen_select/chosen.jquery.min.js')}}"></script>
        <script src="{{asset('assets/home/js/main_1.js')}}"></script>
         <script src="{{asset('assets/home/js/owl-carousel.js')}}"></script>
    <script src="{{asset('assets/home/js/particles.js')}}"></script>
        <script>
$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
  $(document).ready(function () {
        $('.landingCardSlider').slick({
          dots: false,

          slidesToShow: 4,
          slidesToScroll: 1
        });
   
           $('#owl-carousel').owlCarousel({
              loop: true,
              margin: 50,
              responsiveClass: true,
              responsive: {
                  0: {
                      items: 1,
                      nav: true
                  },
                  600: {
                      items: 2,
                      nav: false
                  },
                  768: {
                      items: 3,
                      nav: false
                  },
                  1000: {
                      items: 3,
                      nav: true,
                      loop: true
                  }
              }
          });
      });
        </script>