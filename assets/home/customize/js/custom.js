'use strict';

var mobileViewport = 992;
var isSafari = navigator.userAgent.indexOf("Safari") > -1;
var isChrome   = navigator.userAgent.indexOf('Chrome') > -1;
if((isSafari) && (isChrome)) {
    isSafari = false;
}

let months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
let days=["Sunday","Monday","Tuesday","Wednessday","Thursday","Friday","Saturday"];

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

$(document).ready(function(){
    OwlCarousel();

    $('.blur-area').blurArea();
    $('.ws-action').windowScrollAction();
    checkboxes();
    afternavHeight();
    activeBookmark();
    magnificLightbox();
    priceSlider();
    BSTabsActions();
    datePickers();
    heroSearchSections();
    $('.quantity-selector').quantitySelector();
    autocomplete();
    searchResultsCollapse();
    comingSoonCountdown();
    smoothScroll();

    if($(window).width() > mobileViewport) {
       YouTubeVideo();
       stickySidebars();
    }

    if($(window).width() < mobileViewport) {
        mobileFilters();
    }
});

googleMaps();


function smoothScroll() {
    var scroll = new SmoothScroll('a[data-scroll]');
}


function YouTubeVideo() {
    var $video = $('#youtube-video');
    if($video.length) {

        $video.YTPlayer({
            fitToBackground: true,
            videoId: $video.data('video-id'),
            events: {
                onReady: function() {
                    $video.data('ytPlayer').player.mute();
                }
            }
        });

        $(document).on('scroll', function(){
            videoScroll();
        });

        function videoScroll() {
            var fraction = 0.75,
                player = $video.data('ytPlayer').player,
                videoHeight = $video.height(),
                videoOffsetTop = $video.offset().top,
                windowScrollY = window.scrollY;

            if(windowScrollY > (videoHeight + videoOffsetTop) *fraction) {
                player.pauseVideo();
            } else {
                player.playVideo();
            }
        }
    }
}

function OwlCarousel() {
    $('.owl-carousel').each( function() {
        var $carousel = $(this);
        $carousel.owlCarousel({
            // dots : false,
            // items : $carousel.data("items"),
            slideBy : $carousel.data("slideby"),
            center : $carousel.data("center"),
            loop : $carousel.data("loop"),
            margin : $carousel.data("margin"),

            autoplay : $carousel.data("autoplay"),
            autoplayTimeout : $carousel.data("autoplay-timeout"),
            navText : [ '<span class="fa fa-angle-left"><span>', '<span class="fa fa-angle-right"></span>' ],
            responsive: {
                0 : {
                    items: 1,
                    dots: true,
                    nav: false
                },
                992 : {
                    items: $carousel.data("items"),
                    dots: false,
                    nav : $carousel.data("nav")
                }
            }
        });
    });
}

function stickySidebars() {
    $('.sticky-col').stick_in_parent({
        parent: $('#sticky-parent')
    });

    $('.sticky-cols').stick_in_parent({
        parent: $('.sticky-parent')
    });
}


function mobileFilters() {
    if($('#mobileFilters').length) {
        $(document).on('scroll', function(){
            filtersScroll();
        });
    }

    function filtersScroll() {

        var filters = $('#mobileFilters');
        var footer =  $('#mainFooter');

        if(filters.offset().top + filters.height() > footer.offset().top - 10 || !$(document).scrollTop()) {
            filters.removeClass('active');
        } else {
            filters.addClass('active');
        }

        if($(document).scrollTop + window.innerHeight > footer.offset().top) {
            filters.addClass('active');
        }
    }
}

function checkboxes() {
    $('.icheck, .iradio').iCheck({
        checkboxClass: 'icheck',
        radioClass: 'iradio'
    });
}


function googleMaps() {
    if($('.google-map').length) {
        window.initMap = function() {
            $('.google-map').gmap();
        }
    } else {
        window.initMap = function() {
            return;
        }
    }
}



function afternavHeight() {
    $('.afternav-height').each(function(){
        var $mainNav = $('#main-nav'),
            mainNavHeight = $mainNav.height(),
            height = $(window).height() - mainNavHeight;

        $(this).css('height', height);
    });
}


function activeBookmark() {
    $('.theme-search-results-item-bookmark').each(function(index, el){
        $(el).on('click', function(e){
            e.preventDefault();
            $(this).toggleClass('active');
        });
    });
}



function magnificLightbox() {
    $('.magnific-gallery').each(function(index, el){
        $(el).magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1]
            }
        })
    });

    $('.magnific-gallery-link').each(function(index , value){
      var gallery = $(this);
      var galleryImages = $(this).data('items').split(',');
        var items = [];
        for(var i=0;i<galleryImages.length; i++){
          items.push({
            src:galleryImages[i],
            title:''
          });
        }
        gallery.magnificPopup({
          mainClass: 'mfp-fade',
          items:items,
          gallery:{
            enabled:true,
            tPrev: $(this).data('prev-text'),
            tNext: $(this).data('next-text')
          },
          type: 'image'
        });
    });

    $('.magnific-inline').magnificPopup({
        type: 'inline',
        fixedContentPos: true
    });

    $('.magnific-iframe').magnificPopup({
        type: 'iframe'
    });
}


function priceSlider() {
    $("#price-slider").ionRangeSlider({
        type: "double",
        prefix: "$"
    });

    $("#price-slider-mob").ionRangeSlider({
        type: "double",
        prefix: "$"
    });
}



function BSTabsActions() {
    $('#sticky-tab').on('shown.bs.tab', function (e) {
      $('.sticky-tab-col').stick_in_parent({
            parent: $('#sticky-tab-parent')
        });
    });


    $('#slideTabs a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
      var control = $(this).attr('aria-controls')
      var active = $('#slideTabsSlides').find('.active')[0];
      var target = $('#slideTabsSlides').find("[data-tab='" + control + "']")[0];
      if(active !== target) {
        $(active).removeClass('active');
        $(target).addClass('active');
      }
    });
}


function datePickers() {

    $('.datePickerSingle').datetimepicker({
        format: 'ddd, MMM D',
        minDate:new Date()
    });


    $('.datePickerStart').datetimepicker({
        format: 'ddd M/D'
    }).on('dp.change', function(e){
        
        var parent = $($(this).parents('.row')[0]),
            endDate = parent.find('.datePickerEnd');
        endDate.data("DateTimePicker").minDate(e.date).show();
    });

    $('.datePickerEnd').datetimepicker({
        minDate:new Date(),
        format: 'ddd M/D/Y',
        useCurrent: false
    }).on('dp.change', function(e){
        var parent = $($(this).parents('.row')[0]),
            startDate = parent.find('.datePickerStart');
        //startDate.data("DateTimePicker").maxDate(e.date);
    });

}


function heroSearchSections() {
    $('.theme-hero-search-section').each(function(){
        var label,
            input;

        label = $(this).find('.theme-hero-search-section-label');
        input = $(this).find('.theme-hero-search-section-input');

        if(input.val()) {
            label.addClass('active');
        }

        input.focus(function(){
            label.addClass('active');
        });

        input.blur(function(){
            if(!input.val()) {
                label.removeClass('active');
            }
        });
    });
}

function autocomplete() {
    $('.typeahead').typeahead({
        minLength: 3,
        showHintOnFocus: true,
        source: function(q, cb) {
            return $.ajax({
                dataType: 'json',
                type: 'get',
                url: 'https://atoz.digital/world_destinations/destinations.php?q=' + q,
                chache: false,
                success: function(data) {
                    var res = [];
                    $.each(data, function(index, val){
                        if(val !== "%s") {
                            res.push({
                                id: index,
                                name: val
                            })
                        }
                    })
                    cb(res);
                }
            })
        }
    })
}


function searchResultsCollapse() {

    $('.theme-search-results-item-collapse').on('shown.bs.collapse', function(){
        $(this).parents('.theme-search-results-item').addClass('active');
    });

    $('.theme-search-results-item-collapse').on('hidden.bs.collapse', function(){
        $(this).parents('.theme-search-results-item').removeClass('active');
    });

}


function comingSoonCountdown() {

    $('#commingSoonCountdown').countdown('2018/10/10', function(e){
        $(this).html(e.strftime(''
            + '<div><p>%D</p><span>days</span></div>'
            + '<div><p>%H</p><span>hours</span></div>'
            + '<div><p>%M</p><span>minutes</span></div>'
            + '<div><p>%S</p><span>seconds</span></div>'
        ));
    });
}

$('.mobile-picker').each(function(i, item){
    if(!isSafari) {
        $(item).attr('type', 'text');
        $(item).val($(item).attr('value'));
        $(item).on('focus', function(){
            $(item).attr('type', 'date');
        });
        $(item).on('blur', function(){
            $(item).attr('type', 'text');
        });
    }
});
let num=2;
$(document).on('click','.addnew',function(){
    
$('#kingpas1').clone().prop('id','kingpas'+num).append('<div class="col-md-1 col-sm-1"><a href="#" class="closeall add_rooms">X</a></div>').find('input:text').val('').end().find('span').html('Room '+num).end().insertAfter('div.kingpas:last').children('.age-sel-txt').remove().end().children('.child-age-dd').remove().end().find('.quantity-selector-current').html('0').end();
    $('#no_of_rooms').val(num);
num++;
    $('.quantity-selector').quantitySelector();
});

$(document).on('click','.child_age',function(){
let row='<div class="col-md-3  col-sm-3 age-sel-txt"><span class="fonw">Select Age</span></div><div class="col-md-3  col-sm-3 child-age-dd"><div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light quantity-selector" data-increment="Rooms"><label class="theme-search-area-section-label">Child: (2-11 Yrs)</label><div class="theme-search-area-section-inner"><i class="theme-search-area-section-icon lin lin-tag"></i><input class="theme-search-area-section-input" value="1" type="text" name="ages[]"><div class="quantity-selector-box" id="HotelSearchRooms"><div class="quantity-selector-inner"><p class="quantity-selector-title">Rooms</p><ul class="quantity-selector-controls"><li class="quantity-selector-decrement"><a href="#">-</a></li><li class="quantity-selector-current">2</li><li class="quantity-selector-increment"><a href="#">+</a></li></ul></div></div></div></div>';
let row1='<div class="col-md-3  col-sm-3 child-age-dd"><div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light quantity-selector" data-increment="Rooms"><label class="theme-search-area-section-label">Child: (2-11 Yrs)</label><div class="theme-search-area-section-inner"><i class="theme-search-area-section-icon lin lin-tag"></i><input class="theme-search-area-section-input" value="1" type="text" name="ages[]"><div class="quantity-selector-box" id="HotelSearchRooms"><div class="quantity-selector-inner"><p class="quantity-selector-title">Rooms</p><ul class="quantity-selector-controls"><li class="quantity-selector-decrement"><a href="#">-</a></li><li class="quantity-selector-current">2</li><li class="quantity-selector-increment"><a href="#">+</a></li></ul></div></div></div></div>';
if($(this).closest('.kingpas').children('.child-age-dd').length>0)
{
    $(this).closest('.kingpas').append(row1);
}
else{
    $(this).closest('.kingpas').append(row);
}

    $('.quantity-selector').quantitySelector();

});

$(document).on('click','.closeall',function(){
    if($(this).hasClass('add_rooms'))
    {
        num--;
        $('#no_of_rooms').val(num);
    }
    $(this).closest('.row').remove();
});
let dest_count=0;
let start_date="";
$(document).on('click','.dropdown-item',function(){

    if(dest_count===0)
    {
        start_date=new Date($('#date').val());
    }
    else
    {
        let day_diff=parseInt($('#test').prev().children().children().children().next().next().children().children().next().val());

        let old_st_dt=new Date($('#test').prev().children().children().children().next().next().children().children().next().next().val());
        start_date=old_st_dt.addDays(day_diff);

    }
    let end_date=start_date.addDays(3);
    let in_date=start_date.getDate();
    let in_month=months[start_date.getMonth()];
    let out_date=end_date.getDate();
    let out_month=months[end_date.getMonth()];
    let depart_date=(start_date.getMonth()+1)+"/"+in_date+"/"+start_date.getFullYear();
    let depart_date_end=(end_date.getMonth()+1)+"/"+out_date+"/"+end_date.getFullYear();




    let id=$(this).parent().parent().parent().children().next().attr('id');
    if(id==='destination')
    {

        let destination=$('#'+id).val();
        let row='<div class="row itianary"><div class="col-md-12"><div class="whiteloop"><div class="col-md-5"><p class="holD_city_name append_bottom5 ng-binding">'+destination+'</p><ul class="theme-search-results-item-room-feature-list"><li><i class="fa fa-heart theme-search-results-item-room-feature-list-icon"></i><span class="theme-search-results-item-room-feature-list-title">Romantic Leisure</span></li><li><i class="fa fa-refresh theme-search-results-item-room-feature-list-icon"></i><span class="theme-search-results-item-room-feature-list-title">Shoping</span></li></ul></div><div class="col-md-3"><span class="colorback"><span class="twodiv"><span class="infont">In</span><span class="inbigfont">'+in_date+" "+in_month+'</span></span><span class="twodiv bnon"><span class="infont">Out</span><span class="inbigfont">'+out_date+" "+out_month+'</span></span></span></div><div class="col-md-3"><div class="selectingform"><span>Night</span><select class="themform" name="no_of_nights[]"><option>1</option><option>2</option><option selected>3</option><option>4</option><option>5</option></select><input type="hidden" name="depart_date[]" class="depart_date" value="'+depart_date+'"><input type="hidden" name="destination[]" class="destination" value="'+destination+'"></div></div><div class="col-md-1"><a href="#" class="closeall">X</a></div></div></div></div>';
        $('#test').before(row);
        $('#'+id).val('');
        dest_count++;

        $('#depart_date').val(depart_date_end);
    }
    
    

});
$(document).on('change','.themform',function(){

let no_days=$(this).val();
let old_date=new Date($(this).next().val());
let old=old_date.addDays(parseInt(no_days));
 let new_date=old.getDate();
 let new_month=months[old.getMonth()];
 let new_depart_date=(old.getMonth()+1)+"/"+new_date+"/"+old.getFullYear();
 //    $(this).next().val(new_depart_date);
//let in_date = $(this).parent().parent().prev().children().children().children().next().html();
$(this).parent().parent().prev().children().children().next().children().next().html(new_date+" "+new_month);

    let itianary=$(this).closest('.itianary').nextAll('.itianary');
    if(itianary.length>0)
    {
        for(let i=0;i<itianary.length;i++)
        {

            // let new_out_date = (new_date.getMonth()+1)+"/"+new_date.getDate()+"/"+new_date.getFullYear();


            //alert($(itianary[i]).attr('class'));
            let dest=$(itianary[i]).children().children().children().next().next().children().children().next().next().next().val();
            let opt=$(itianary[i]).children().children().children().next().next().children().children().next().html();
            let opt_val=$(itianary[i]).children().children().children().next().next().children().children().next().val();
                alert(opt_val);
           $(itianary[i]).children().children().children().next().next().children().children().next().next().val(new_depart_date);
            $(itianary[i]).children().children().children().next().next().children().children().next().next().next().val(dest);

                no_days=$(itianary[i]).children().children().children().next().next().children().children().next().val();
                old_date=new Date(new_depart_date);
                let old_dt=old_date.getDate();
                let old_month=months[old_date.getMonth()];
                old=old_date.addDays(parseInt(no_days));
                new_date=old.getDate();
                new_month=months[old.getMonth()];
                new_depart_date=(old.getMonth()+1)+"/"+new_date+"/"+old.getFullYear();
             $(itianary[i]).children().children().children().next().children().children().children().next().html(old_dt+" "+old_month);
             $(itianary[i]).children().children().children().next().children().children().next().children().next().html(new_date+" "+new_month);
            $(itianary[i]).children().children().children().next().next().children().children().next().html(opt);







        }
    }
    $('#depart_date').val(new_depart_date);


//alert("In "+in_date+" and out "+out_date);
});

$(document).on('click','.theme-search-area-submit',function () {

    
    let depart_date=$('.depart_date');
    let row1="";
    let dest="";
    let destinations=$('.destination');
    let org_date="";
    let in_date="";

    if(destinations.length>1)
    {
        for(let i=0;i<destinations.length;i++)
        {
            let org=$(destinations[i]).val();



            if(i==(destinations.length-1))
            {
                dest=$(destinations[0]).val();
                org_date=new Date($(depart_date[0]).val());
                in_date=months[org_date.getMonth()]+" "+org_date.getDate()+", "+org_date.getFullYear();
            }
            else
            {
                
                dest=$(destinations[i+1]).val();

                org_date=new Date($(depart_date[i+1]).val());
                in_date=months[org_date.getMonth()]+" "+org_date.getDate()+", "+org_date.getFullYear();
            }
            
            row1+='<div class="theme-search-results-item-preview"><div class="row" data-gutter="20"><div class="col-md-10 "><div class="theme-search-results-item-flight-sections"><div class="theme-search-results-item-flight-section"><div class="row row-no-gutter row-eq-height"><div class="col-md-2 "><div class="theme-search-results-item-flight-section-airline-logo-wrap"></div></div><div class="col-md-10 "><div class="theme-search-results-item-flight-section-item"><div class="row"><div class="col-md-3 "><div class="theme-search-results-item-flight-section-meta"><p class="theme-search-results-item-flight-section-meta-city">'+org+'</p><p class="theme-search-results-item-flight-section-meta-date">'+in_date+'</p></div></div><div class="col-md-6 "><div class="theme-search-results-item-flight-section-path"><div class="theme-search-results-item-flight-section-path-line"></div><div class="theme-search-results-item-flight-section-path-line-start"><i class="fa fa-plane theme-search-results-item-flight-section-path-icon"></i><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">CLY</div></div><div class="theme-search-results-item-flight-section-path-line-end"><i class="fa fa-plane theme-search-results-item-flight-section-path-icon"></i><div class="theme-search-results-item-flight-section-path-line-dot"></div><div class="theme-search-results-item-flight-section-path-line-title">LGA</div></div></div></div><div class="col-md-3 "><div class="theme-search-results-item-flight-section-meta"><p class="theme-search-results-item-flight-section-meta-city">'+dest+'</p><p class="theme-search-results-item-flight-section-meta-date">'+in_date+'</p></div></div></div></div></div></div></div></div></div><div class="col-md-2 "><div class="theme-search-results-item-book"><div class="theme-search-results-item-price"><p class="theme-search-results-item-price-sign">(Uncheck to remove)</p><p class="theme-search-results-item-price-tag"><label class="fullcheck">&nbsp;<input type="checkbox" checked="checked"><span class="checkmark"></span></label></p></div></div></div></div></div>';
            
            // alert($(destinations[i]).val());
            // alert($(depart_date[i]).val());
        }
        $('.theme-search-results-item').html(row1);
    }
    



});

$(document).on('click','.rullybutton',function(){
    $('#package_form')[0].submit();
})




