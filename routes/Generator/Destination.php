<?php
/**
 * Destination Management
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Destination'], function () {
        Route::resource('destinations', 'DestinationsController');
        //For Datatable
        Route::post('destinations/get', 'DestinationsTableController')->name('destinations.get');
    });
    
});