<?php
/**
 * Package Theme Management
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Packagetheme'], function () {
        Route::resource('packagethemes', 'PackagethemesController');
        //For Datatable
        Route::post('packagethemes/get', 'PackagethemesTableController')->name('packagethemes.get');
    });
    
});