<?php
/**
 * Package Type Management
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Packagetype'], function () {
        Route::resource('packagetypes', 'PackagetypesController');
        //For Datatable
        Route::post('packagetypes/get', 'PackagetypesTableController')->name('packagetypes.get');
    });
    
});