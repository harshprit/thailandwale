<?php
/**
 * Activity
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Activity'], function () {
        Route::resource('activities', 'ActivitiesController');
        //For Datatable
        Route::post('activities/get', 'ActivitiesTableController')->name('activities.get');
    });
    
});