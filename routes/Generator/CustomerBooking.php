<?php
/* Customer Booking Route Management */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'CustomerBooking'], function () {
        Route::resource('customer_bookings', 'CustomerBookingController');
        //For Datatable
        Route::post('customer_bookings/get', 'CustomerBookingTableController')->name('customerbookings.get');

        Route::get('/booking_info/{bookingId}','CustomerBookingController@getCustomerBooking')->name('booking_info');
    });
    
});