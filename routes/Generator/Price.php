<?php
/**
 * Manage Prices
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    
    Route::group( ['namespace' => 'Price'], function () {
        Route::resource('prices', 'PricesController');
        //For Datatable
        Route::post('prices/get', 'PricesTableController')->name('prices.get');
    });
    
});