<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::post('/menu-content/render/','FrontendController@menuContent')->name('menu_content');
Route::get('/', 'FrontendController@index')->name('index');
Route::get('macros', 'FrontendController@macros')->name('macros');
Route::post('/get/states', 'FrontendController@getStates')->name('get.states');
Route::post('/submitform', 'FrontendController@submitform');
Route::post('/get/cities', 'FrontendController@getCities')->name('get.cities');
Route::get('/blog','FrontendController@blog')->name('blog');
Route::get('/token','FrontendController@getAuthentication')->name('token');

Route::get('/holidays','FrontendController@holidays')->name('holidays');


Route::get('/customer-support',function(){
    return view('frontend.home.support');
})->name('support');

Route::get('/gift-a-vacation',function(){
    return view('frontend.home.gift-a-vacation');
})->name('giftavacation');

Route::get('/elite-travellers',function(){
    return view('frontend.home.elite-travellers');
})->name('elitetravellers');

Route::get('/destination-wedding',function(){
    return view('frontend.home.destination-wedding');
})->name('destinationwedding');

Route::get('/feedback-page','FrontendController@feedback')->name('feedbackpage');

Route::post('/feedback-page','FrontendController@storeFeedback')->name('store.feedback');


Route::get('/visa',function(){
    return view('frontend.home.visa');
})->name('visa');

Route::get('/cabs',function(){
    return view('frontend.home.cabs');
})->name('cabs');

Route::get('/entertainment',function(){
    return view('frontend.home.entertainment');
})->name('entertainment');

Route::get('/all-activity','FrontendController@allActivities')->name('allactivity');

Route::get('/services','FrontendController@services')->name('services');
Route::get('/tours','FrontendController@tours')->name('tours');
Route::get('/room','FrontendController@room')->name('room');
Route::get('/about','FrontendController@about')->name('about');
Route::get('/contact','FrontendController@contact')->name('contact');
Route::get('/package-detail','FrontendController@packageDetail')->name('package_detail');
/*-----------------------------------------------------------------------------------------------------|
|---------------------------------Flight Redirection---------------------------------------------------|
|-----------------------------------------------------------------------------------------------------*/
// Route::get('/flights','FrontendController@flights')->name('flights');
Route::any('/filter-flight','FrontendController@filterFlights')->name('filter_flight');
Route::any('/search-flight','FrontendController@searchFlights')->name('search_flight');

Route::get('/flight/passenger-details/{TraceId}/{ResultIndex}','FrontendController@getPassengerDetails')->name('flight_passenger_details');
Route::post('/flight/passenger-details','FrontendController@getPassangerDetailsReturn')->name('flight_passenger_details_return');

Route::post('/flight/booking-review','FrontendController@showBookingReview')->name('booking_review');
Route::get('/flight/hold/{TraceId}/{ResultIndex}','FrontendController@doneHoldBooking')->name('flight_hold');
Route::get('/flight/hold/{TraceId}/{ResultIndex}/{ResultIndexReturn}','FrontendController@doneHoldBooking')->name('flight_hold_return');
Route::get('/flight/ticket/{TraceId}/{ResultIndex}','FrontendController@ticketFlightBooking')->name('flight_ticket_non_lcc_direct');
Route::get('/flight/ticket/{TraceId}/{ResultIndex}/{ResultIndexReturn}','FrontendController@ticketFlightBooking')->name('flight_ticket_non_lcc_direct_return');

Route::get('/flight/ticket-book/{TraceId}/{ResultIndex}','FrontendController@getFlightTicketNonLcc')->name('flight_ticket_non_lcc');
Route::get('/flight/ticket-lcc/{TraceId}/{ResultIndex}/{ResultIndexReturn}','FrontendController@ticketLccFlightBooking')->name('flight_ticket_lcc');
Route::get('/flight/ticket-lcc/{TraceId}/{ResultIndex}','FrontendController@ticketLccFlightBooking')->name('flight_ticket_lcc_intl');
Route::post('/flight/ticket','FrontendController@getFlightTicket')->name('flight_ticket');

Route::get('/flight/ticket/ib-non-lcc/{TraceId}/{ResultIndex}/{ResultIndexReturn}','FrontendController@ticketFlightBookingIBNonLcc')->name('ib_non_lcc');
Route::get('/flight/ticket/ob-non-lcc/{TraceId}/{ResultIndex}/{ResultIndexReturn}','FrontendController@ticketFlightBookingOBNonLcc')->name('ob_non_lcc');
Route::post('/filter-flights','FrontendController@filterFlights')->name('fliter.flights');

/*-----------------------------------------------------------------------------------------------------|
|----------------------------------Hotel Redirection---------------------------------------------------|
|-----------------------------------------------------------------------------------------------------*/
Route::post('/filter-hotels','FrontendController@filterHotels')->name('fliter.hotels');
Route::get('/hotels','FrontendController@hotel')->name('hotels');
Route::any('/search-hotel','FrontendController@searchHotels')->name('search_hotel');
Route::get('/hotel-info/{TraceId}/{ResultIndex}/{HotelCode}','FrontendController@getHotelInfo')->name('hotel_info');
Route::any('/block-hotel','FrontendController@blockHotelRoom')->name('block_hotel');
Route::post('/book-hotel','FrontendController@bookHotelRoom')->name('book_hotel');
Route::any('/payment/response','FrontendController@bookingResponse')->name('response');
Route::any('/payment/cancel','FrontendController@bookingCancel')->name('booking_cancel');

Route::get('/holidays/{sourceid?}/{slug?}','FrontendController@allPackage')->name('all_package');
Route::any('/single-package','FrontendController@singlePackageDetail')->name('single_package');
Route::get('/package-details/{slug}', 'FrontendController@singlePackageDetail')->name('package.get_details');
Route::post('/single-package/pax-details','FrontendController@singlePackagePaxDetails')->name('package.single.pax.details');
Route::post('/single-package/booking','FrontendController@singlePackageBooking')->name('package.single.booking');


/*-----------------------------------------------------------------------------------------------------|
|----------------------------------Refer & Earn--------------------------------------------------------|
|-----------------------------------------------------------------------------------------------------*/
Route::get('/refer-and-earn','FrontendController@referAndEarn')->name('refer_and_earn');
Route::get('refer/earn/{token}', 'FrontendController@handleRegistration')->name('refer.register');

/*-----------------------------------------------------------------------------------------------------|
|----------------------------------Customize Package---------------------------------------------------|
|-----------------------------------------------------------------------------------------------------*/
Route::get('/customize/package',function() {
return view('frontend.home.customize-package');
}
)->name('customize_package');

Route::any('/package-details','FrontendController@customizePackage')->name('package_details');

Route::post('/package/passanger-details','FrontendController@packagePassangerDetails')->name('process_package');

Route::any('/package-review','FrontendController@packageReview')->name('package_review');
Route::any('/package-booking','FrontendController@packageBooking')->name('package.booking');

Route::get('/activity/{source}','FrontendController@activities')->name('activity');
Route::get('/activity-by-region/{region}','FrontendController@activitiesByRegion')->name('region_activity');
Route::get('/booking-activity/{source}','FrontendController@bookingActivity')->name('booking_activity');

// Route::any('/payment/cancel', function () {
//     return redirect()->route('frontend.hotels');
// });




/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');

        /*
         * User Profile Picture
         */
        Route::patch('profile-picture/update', 'ProfileController@updateProfilePicture')->name('profile-picture.update');

        /*
         * Refer and Earn Specific
         */
        Route::post('refer-earn', 'ReferEarnController@sendReference')->name('send_refer_email');

        /*
         * Verify Promo Code
         */
        Route::post('verify/coupon','CouponController@verifyCoupon')->name('verify_coupon');

        Route::any('get/bookings','AccountController@getBookings')->name('get_bookings');

        Route::get('booking-detail/{type}/{booking_id}','AccountController@bookingDetail')->name('booking_details');

        Route::get('print-booking/{type}/{booking_id}','AccountController@printTicket')->name('print_booking');
        
        Route::get('invoice/{booking_id}','AccountController@invoice')->name('invoice');
        
        Route::post('user-profile','AccountController@storeUserDetails')->name('update.profile');

        
        

    });
});

/*
* Show pages
*/
Route::get('pages/{slug}', 'FrontendController@showPage')->name('pages.show');
