<?php
return [
	"backend" => [
	"access" => [
	"title" => "Access Management",
	"roles" => [
	"all" => "All Roles",
	"create" => "Create Role",
	"edit" => "Edit Role",
	"management" => "Role Management",
	"main" => "Roles",
	],
	"permissions" => [
	"all" => "All Permissions",
	"create" => "Create Permission",
	"edit" => "Edit Permission",
	"management" => "Permission Management",
	"main" => "Permissions",
	],
	"users" => [
	"all" => "All Users",
	"change-password" => "Change Password",
	"create" => "Create User",
	"deactivated" => "Deactivated Users",
	"deleted" => "Deleted Users",
	"edit" => "Edit User",
	"main" => "Users",
	"view" => "View User",
	],
	],
	"log-viewer" => [
	"main" => "Log Viewer",
	"dashboard" => "Dashboard",
	"logs" => "Logs",
	],
	"sidebar" => [
	"dashboard" => "Dashboard",
	"general" => "General",
	"system" => "System",
	],
	"pages" => [
	"all" => "All Pages",
	"create" => "Create Page",
	"edit" => "Edit Page",
	"management" => "Page Management",
	"main" => "Pages",
	],
	"blogs" => [
	"all" => "All Blog",
	"create" => "Create Blog",
	"edit" => "Edit Blog",
	"management" => "Blog Management",
	"main" => "Blogs",
	],
	"blogcategories" => [
	"all" => "All Blog Categories",
	"create" => "Create Blog Category",
	"edit" => "Edit Blog Category",
	"management" => "Blog Category Management",
	"main" => "CMS Pages",
	],
	"blogtags" => [
	"all" => "All Blog Tag",
	"create" => "Create Blog Tag",
	"edit" => "Edit Blog Tag",
	"management" => "Blog Tag Management",
	"main" => "Blog Tags",
	],
	"blog" => [
	"all" => "All Blog Page",
	"create" => "Create Blog Page",
	"edit" => "Edit Blog Page",
	"management" => "Blog Management",
	"main" => "Blog Pages",
	],
	"faqs" => [
	"all" => "All Faq Page",
	"create" => "Create Faq Page",
	"edit" => "Edit Faq Page",
	"management" => "Faq Management",
	"main" => "Faq Pages",
	],
	"emailtemplates" => [
	"all" => "All Email Template",
	"create" => "Create Email Template",
	"edit" => "Edit Email Template",
	"management" => "Email Template Management",
	"main" => "Email Template",
	],
	"settings" => [
	"all" => "All Settings",
	"create" => "Create Settings",
	"edit" => "Edit Settings",
	"management" => "Settings Management",
	"main" => "Settings",
	],
	"menus" => [
	"all" => "All Menu",
	"create" => "Create Menu",
	"edit" => "Edit Menu",
	"management" => "Menu Management",
	"main" => "Menus",
	],
	"modules" => [
	"all" => "All Modules Page",
	"create" => "Create Module Page",
	"management" => "Module Management",
	"main" => "Module Pages",
	],
	"destinations" => [
	"all" => "All Destinations",
	"create" => "Create Destination",
	"edit" => "Edit Destination",
	"management" => "Destination Management",
	"main" => "Destinations",
	],
	"packages" => [
	"all" => "All Packages",
	"create" => "Create Package",
	"edit" => "Edit Package",
	"management" => "Package Management",
	"main" => "Packages",
	],
	"packagetypes" => [
	"all" => "All Packagetypes",
	"create" => "Create Packagetype",
	"edit" => "Edit Packagetype",
	"management" => "Packagetype Management",
	"main" => "Packagetypes",
	],
	"packagethemes" => [
	"all" => "All Packagethemes",
	"create" => "Create Packagetheme",
	"edit" => "Edit Packagetheme",
	"management" => "Packagetheme Management",
	"main" => "Packagethemes",
	],
	"coupons" => [
	"all" => "All Coupons",
	"create" => "Create Coupon",
	"edit" => "Edit Coupon",
	"management" => "Coupon Management",
	"main" => "Coupons",
	],
	"prices" => [
	"all" => "All Prices",
	"create" => "Create Price",
	"edit" => "Edit Price",
	"management" => "Price Management",
	"main" => "Prices",
	],
	"activities"=>[
		"all"=>"All Activities",
		"create"=>"Create Activity",
	],
		"testimonials" => [
	"all" => "All Testimonials",
	"create" => "Create Testimonial",
	"edit" => "Edit Testimonial",
	"management" => "Testimonial Management",
	"main" => "Testimonials",
	],
	],
	"language-picker" => [
	"language" => "Language",
	"langs" => [
	"ar" => "Arabic",
	"da" => "Danish",
	"de" => "German",
	"el" => "Greek",
	"en" => "English",
	"es" => "Spanish",
	"fr" => "French",
	"id" => "Indonesian",
	"it" => "Italian",
	"nl" => "Dutch",
	"pt_BR" => "Brazilian Portuguese",
	"ru" => "Russian",
	"sv" => "Swedish",
	"th" => "Thai",
	],
	],
];