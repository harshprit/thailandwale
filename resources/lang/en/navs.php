<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => 'Home',
        'logout' => 'Logout',
    ],

    'frontend' => [
        'dashboard' => 'Dashboard',
        'login'     => 'Login',
        'macros'    => 'Macros',
        'register'  => 'Signup',
        'tours'     =>  'Tours',
        'hotels'    =>  'Hotels',
        'flights'    =>  'Flights',
        'blog'      =>  'Blog',
        'about'     =>  'About',
        'contact'   =>  'Contact',
        'services'   =>  'Services',
        'all_package'   => 'Holidays',
        'refer_and_earn' => 'Refer & Earn',
        'support' => 'Support',
        'holidays' => 'Holidays',
        'cabs' => 'Cabs',
        'entertainment' => 'Entertainment',
        'visa' => 'Visa',
        'allactivity' => 'All Activities',
        'destinationwedding' => 'Destination Wedding',
        
        

        'user' => [
            'account'         => 'My Account',
            'administration'  => 'Administration',
            'change_password' => 'Change Password',
            'my_information'  => 'My Information',
            'profile'         => 'Profile',
            'booking_history' => 'Booking History',
            'manage_booking' => 'Manage Booking',
        ],
    ],
];
