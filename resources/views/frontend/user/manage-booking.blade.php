@extends('frontend.layouts.app')
@include('frontend.includes.header')
@include('frontend.includes.nav1')
@php
$booking_detail=json_decode($booking->booking_detail);
//dd($booking_detail);
$paxDetails = $booking_detail->GetBookingDetailResult->HotelRoomsDetails;
$hotelPolicy = $booking_detail->GetBookingDetailResult->HotelPolicyDetail;
$hotel_name=$booking_detail->GetBookingDetailResult->HotelName;
$address=$booking_detail->GetBookingDetailResult->AddressLine1.', '.$booking_detail->GetBookingDetailResult->AddressLine1;
$check_in=$booking_detail->GetBookingDetailResult->CheckInDate;
$check_out=$booking_detail->GetBookingDetailResult->CheckOutDate;
$room_type=$booking_detail->GetBookingDetailResult->HotelRoomsDetails[0]->RoomTypeName;
$amount=$booking_detail->GetBookingDetailResult->InvoiceAmount
@endphp
<div class="container">
        <div class="col-md-7">
            <section class="tripTicketInner">
<div class="width100 fleft padTB10">
<div class="width100 fleft">
<span class="col-md-4 col-sm-4 col-xs-4 pad0">
<i class="fa fa-star yellow fleft ng-scope"></i><i class="fa fa-star yellow fleft ng-scope"></i><i class="fa fa-star yellow fleft ng-scope"></i>
</span>
<span class="col-md-8 col-sm-8 col-xs-8 pad0 txtRight">
<span class="dib greyDr fmed ico14">Booking ID</span>
<span class="dib padL3 fmed ico14 ng-binding">{{$booking->booking_id}}</span>
</span>
</div>
<div class="col-md-9 col-sm-9 col-xs-9 pad0">
<span class="width100 fleft ico18 greyDr fmed padT5 ng-binding">{{$hotel_name}}</span>
<span class="width100 fleft greyLt ico14 lh1-5 padT5 ng-binding">{{$address}}</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="tcktImg">
<img ng-src="https://cdn1.goibibo.com/ibis-styles-krabi-ao-nang-krabi-111016683656-jpeg-th.jpg" src="https://cdn1.goibibo.com/ibis-styles-krabi-ao-nang-krabi-111016683656-jpeg-th.jpg">
</span>
</div>
</div>
<div class="pageBrk">
<span class="breakDash"></span>

</div>
<div class="width100 fleft padTB10">
<div class="width100 fleft padTB10 borderBtm">
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft ico13 grey">Check In</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">{{date("d M 'y, H:i",strtotime($check_in))}}</span>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft ico13 grey">Check Out</span>
<span class="width100 fleft padT5 checkText greyDr ng-binding">{{date("d M 'y, H:i",strtotime($check_out))}}</span>
</div>
</div>
<div class="width100 fleft padTB10 borderBtm">
<span class="width100 fleft greyLt ico13">Room Type</span>
<span class="width100 fleft padT10 ico17 greyDr ng-binding">{{$room_type}}</span>
</div>
<div class="width100 fleft padTB10 borderBtm">
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft greyLt ico13">Guests</span>
<span class="width100 fleft padT10 ico17 greyDr ng-binding">Mr Amarnath Mehrotra</span>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft greyLt ico13">Rooms</span>
<span class="width100 fleft padT10 ico17 greyDr ng-binding">1</span>
</div>
</div>
<div class="width100 fleft padTB10 borderBtm">
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft greyLt ico13">
Transaction Amount
<div class="fmtTooltip">
<i class="fa fa-info fmed"></i>
<div class="tip tip_top white">
<table class="table table-bordered margin0">
<tbody>
<tr><th>Total Fare</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>13614</td></tr>
<tr><th>Promo Discount</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>1490 (GOINTH)</td></tr>
<tr><th>Travel Amount</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>12124</td></tr>
<tr><th>Gocash Used</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>0</td></tr>
<tr><th>Gocash+ Used</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>0</td></tr>
<tr><th>Amount Paid</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i></td></tr>
<tr><th>Cashback (Gocash)</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>0</td></tr>
</tbody>
</table>
</div>
</div>
</span>
<span class="width100 fleft padT10 ico20 greyDr">
<i class="icon-rupee ico14 fleft padT3"></i>
<span class="fleft ng-binding">{{$amount}}</span>
</span>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 pad0 marginT15 fr txtRight">
</div>
</div>
<div class="width100 fleft padTB10 borderBtm">
<div class="col-md-6 col-sm-6 col-xs-5 pad0">
<span class="width100 fleft greyLt ico13">Payment Method</span>
<span class="width100 fleft padT5 ico17 greyDr ng-binding">Saved Card</span>
</div>
</div>
</div>
</section>
        </div>
        <div class="col-md-5">
            <section class="tripActionOuter">
<section class="tripActionInner">
<span class="width100 fl txtCenter ico20 greyLt">Manage Booking</span>
<div class="actionItem">
<ul>
<!-- ngRepeat: action_code in bookingDetailData.actions --><li>

<a data-toggle="modal" data-target="#myModal" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-edit stylish-icon itemIco"></i>
<span class="itemText ng-binding">Modify Booking</span>

</a>


</li><li class="ng-scope">


  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
         <div id="ChangeRequestPopup" class="cancel_bkng_popup" style="">
                <div class="dtl_left">
                    <div class="modelpophead"><span>You are about to cancel the booking. Please review the details carefully before cancelling.</span><a id="ChangeRequestClose" class="fl_close_norm fl_close_ipad cursor"></a></div>

                </div>
                <div class="cancel_bkng">
                    <div class="can_bkng_row">
                        <span class="bold head">Hotel Booking</span> <span>
                            <label class="bold">
                                Pax Name:</label>
                            <code id="_paxNamepopup">Anupam Dixit</code></span> <span>
                                <label class="bold">
                                    Hotel Name:</label>
                                <code id="_hotelNamepopup">{{$hotel_name}}</code></span>
                        <span class="red">Note : For amendments, please contact TBO Operations.</span>
                    </div>
                    <div class="can_bkng_row">
                        <span class="bold">Cancel Booking</span> <span>
                            <select>
                                <option>Cancel Hotel Booking</option>
                            </select></span>
                    </div>
                    <div class="can_bkng_row">
                        <span class="bold">Please enter remarks</span>
                        <textarea rows="3" cols="56" id="_remarkpopup"></textarea>
                    </div>
                </div>
                <div class="can_bkng_row_btn">
                    <span>Do you want to proceed?</span><br>
                    <input type="button" id="_btnSendChangeRequest" class="btn_main_s mr mt5" value="Send Request">
                    <input type="button" id="CloseButtonPopup" class="btn_main_s  mt5" value="Close">
                </div>
                <input type="hidden" id="_BookingID" name="_BookingID" value="1424687">
            </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>




</li><li class="ng-scope">



<a data-toggle="modal" data-target="#policy" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-clipboard stylish-icon itemIco"></i>
<span class="itemText ng-binding">Cancellation &amp; Hotel Policy</span>

</a>

</li>

<div class="modal fade" id="policy">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Cancellation and Hotel Policy</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{$hotelPolicy}}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>




<li class="ng-scope">
<a target="_blank" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-search stylish-icon itemIco"></i>
<span class="itemText ng-binding">Review Cancellation Charges</span>

</a>




</li><li class="ng-scope">
<a target="_blank" href="{{route('frontend.user.print_booking',['type'=>'hotel','booking_id'=>$booking->booking_id])}}"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-print stylish-icon itemIco"></i>
<span class="itemText ng-binding">Print Voucher</span>

</a>




</li><li class="ng-scope">
<a target="_blank" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-print stylish-icon itemIco"></i>
<span class="itemText ng-binding">Payment Receipt</span>

</a>




</li><li>


<a target="_blank" href="#"><i class="itemArrow fa fa-chevron-right"></i>

<i class="fa fa-envelope stylish-icon itemIco"></i>
<span class="itemText ng-binding">Email Voucher</span>
</a>


</li><li class="ng-scope">




<a data-toggle="modal" data-target="#contact" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-phone stylish-icon itemIco"></i>
<span class="itemText fmtTooltip ng-binding">Contact Hotel<div class="tip tip_left black ng-binding" style="font-size: 15px;" ng-bind-html="vertical_actions[bookingDetailData.vertical_key][action_code].popover_content.format(bookingDetailData)">M:  <br> P: </div></span>

</a>
</li>
<div class="modal fade" id="contact">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Hotel Contact Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="can_bkng_row">
            <span class="bold">Hotel Name:</span> <span>
              {{$hotel_name}}
        </div>
        <div class="can_bkng_row">
            <span class="bold">Address Line1:</span> <span>
              {{$booking_detail->GetBookingDetailResult->AddressLine1}}
        </div>
        <div class="can_bkng_row">
            <span class="bold">Address Line2:</span> <span>
{{$booking_detail->GetBookingDetailResult->AddressLine2}}
        </div>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<li class="ng-scope">

</li></ul>
</div>
</section>
</section>
        </div>
    </div>
@include('frontend.includes.footer')
