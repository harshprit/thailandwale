
@include('frontend.includes.header')
@include('frontend.includes.nav1')
@php
$paxType=array(
'1'=>'Adult',
'2'=>'Child',
'3'=>'Infant'
);
$booking=json_decode($booking->booking_detail);
$itinerary=$booking->Response->FlightItinerary;
$segements=$itinerary->Segments;
$passangers=$itinerary->Passenger;

@endphp
<div class="container">
        <div class="col-md-7">
            @foreach($segements as $segment)
            <section class="tripTicketInner">
<div class="width100 fleft padTB10">
<div class="width100 fleft">
<span class="col-md-4 col-sm-4 col-xs-4 pad0">
{{$segment->Airline->AirlineName." ".$segment->Airline->AirlineCode." ".$segment->Airline->FlightNumber}}
</span>
<span class="col-md-8 col-sm-8 col-xs-8 pad0 txtRight">
<span class="dib greyDr fmed ico14">PNR</span>
<span class="dib padL3 fmed ico14 ng-binding">{{$itinerary->PNR}}</span>
</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico18 greyDr fmed padT5 ng-binding">{{date('h:i',strtotime($segment->Origin->DepTime))}}</span>
<span class="width100 fleft ico14 lh1-5 ng-binding biggie">{{$segment->Origin->Airport->AirportCode}}</span>
<span class="width100 fleft greyLt ico14 lh1-5 ng-binding">{{$segment->Origin->Airport->CityName}}</span>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft ico18 greyDr padT5 fmed ng-binding"><?php $datetimed = new DateTime($segment->Origin->DepTime);
                           $datetimea = new DateTime($segment->Destination->ArrTime);
                           $interval = $datetimed->diff($datetimea);
                           echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?></span>
<span class="width100 fleft greyLt ico14 lh1-5 ng-binding">--------------------------</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico18 greyDr fmed padT5 ng-binding">{{date('h:i',strtotime($segment->Destination->ArrTime))}}</span>
<span class="width100 fleft ico14 lh1-5  ng-binding biggie">{{$segment->Destination->Airport->AirportCode}}</span>
<span class="width100 fleft greyLt ico14 lh1-5  ng-binding">{{$segment->Destination->Airport->CityName}}</span>
</div>

</div>
<div class="pageBrk">
<span class="breakDash"></span>

</div>
<div class="width100 fleft padTB10">
<div class="width100 fleft padTB10 borderBtm">
<div class="departure-secty">
    <h5 class="departure-heady">Depart: {{$segment->Origin->Airport->AirportName}}</h5>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico13 grey">Travel Date</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">{{date('D, M d Y',strtotime($segment->Origin->DepTime))}}</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico13 grey">Depart</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">{{date('h:i',strtotime($segment->Origin->DepTime))}}</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico13 grey">Gate</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">--</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico13 grey">Terminal</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">-</span>
</div>
</div>

</div>
<div class="width100 fleft padTB10 borderBtm">
<div class="arrival-secty">
    <h5 class="departure-heady">Arrive: {{$segment->Destination->Airport->AirportName}}</h5>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico13 grey">Travel Date</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">{{date('D, M d Y',strtotime($segment->Destination->ArrTime))}}</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico13 grey">Arrive</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">{{date('h:i',strtotime($segment->Destination->ArrTime))}}</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico13 grey">Gate</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">--</span>
</div>
<div class="col-md-3 col-sm-3 col-xs-3 pad0">
<span class="width100 fleft ico13 grey">Terminal</span>
<span class="checkText width100 fleft padT5 greyDr ng-binding">3</span>
</div>
</div>

</div>

<div class="width100 fleft padTB10 borderBtm">
<div class="col-md-12 col-sm-12 col-xs-12 pad0">
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft greyLt ico13">Passanger</span>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft greyLt ico13">Type</span>
</div>
</div>
@foreach($passangers as $passanger)
<div class="col-md-12 col-sm-12 col-xs-12 pad0">
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft padT10 ico17 greyDr ng-binding">{{$passanger->Title.". ".$passanger->FirstName." ".$passanger->LastName}}</span>

</div>
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft padT10 ico17 greyDr ng-binding">{{$paxType[$passanger->PaxType]}}</span>

</div>
</div>
@endforeach
</div>

<div class="width100 fleft padTB10 borderBtm">
<div class="col-md-6 col-sm-6 col-xs-6 pad0">
<span class="width100 fleft greyLt ico13">
Transaction Amount
<div class="fmtTooltip">
<i class="fa fa-info fmed"></i>
<div class="tip tip_top white">
<table class="table table-bordered margin0">
<tbody>
<tr><th>Total Fare</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>13614</td></tr>
<tr><th>Promo Discount</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>1490 (GOINTH)</td></tr>
<tr><th>Travel Amount</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>12124</td></tr>
<tr><th>Gocash Used</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>0</td></tr>
<tr><th>Gocash+ Used</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>0</td></tr>
<tr><th>Amount Paid</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>12124</td></tr>
<tr><th>Cashback (Gocash)</th><td class="ng-binding"><i class="icon-rupee ico10 fleft padT3"></i>0</td></tr>
</tbody>
</table>
</div>
</div>
</span>
<span class="width100 fleft padT10 ico20 greyDr">
<i class="icon-rupee ico14 fleft padT3"></i>
<span class="fleft ng-binding">12124</span>
</span>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 pad0 marginT15 fr txtRight">
</div>
</div>
<div class="width100 fleft padTB10 borderBtm">
<div class="col-md-6 col-sm-6 col-xs-5 pad0">
<span class="width100 fleft greyLt ico13">Payment Method</span>
<span class="width100 fleft padT5 ico17 greyDr ng-binding">Saved Card</span>
</div>
</div>
</div>
</section>
@endforeach
        </div>
        <div class="col-md-5">
            <section class="tripActionOuter">
<section class="tripActionInner">
<span class="width100 fl txtCenter ico20 greyLt">Manage Booking</span>
<div class="actionItem">
<ul>
<!-- ngRepeat: action_code in bookingDetailData.actions --><li>

<a data-toggle="modal" data-target="#myModal" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-edit stylish-icon itemIco"></i>
<span class="itemText ng-binding">Modify Booking</span>

</a>




</li><li class="ng-scope">

<a data-toggle="modal" data-target="#myModal" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-ban stylish-icon itemIco"></i>
<span class="itemText ng-binding">Cancel Booking</span>

</a>
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
              <div class="bookingque_popup" id="btnSendChangeReqPopup" style="">
        <div class="dtl_left">
            <div class="modelpophead">
                <span>Change Request ( <tt id="selectedPNR" class="bold">
                            <b>PNR :</b>
                            SG - <tt id="PNR_0">YYYJTM</tt>
                        </tt>)</span> <a class="fl_close_ie7 cursor fl_close_for_mobile" id="btnSendChangeReqClose"></a>
            </div>
        </div>
        <div class="request_change">
            <input type="hidden" id="BEStatus" value="True">
            <div class="req_row">
                <div id="errMsgRequestType" class="red font10"></div>
                <select id="requestType"><option value="Select">-Select-</option><option value="FullCancellation">Refund</option><option value="Reissuance">Change Itinerary / Reissue</option><option value="PartialCancellation">Partial Refund</option><option value="FlightCancelled">Flight Cancelled</option></select>

            </div>
            
            <div class="req_row" id="SectorInfo"><span class="bold">Please select Refund Sectors</span><span><input id="sector_0" type="checkbox" value="All" name="sectorsSelectors">All</span><span><input type="checkbox" id="sector_1" name="sectorsSelectors" value="DEL-BKK">DEL-BKK (<code>27 Jan 2019</code>)</span><span><input type="checkbox" id="sector_2" name="sectorsSelectors" value="BKK-DEL">BKK-DEL (<code>30 Jan 2019</code>)</span></div>
            <div id="secErrmsg" class="red font10">
            </div>
            
            <div class="req_row">
                <div class="width_100 fleft" id="PassengerInfo"><span class="bold">Please select Passenger</span><span><input type="checkbox" name="PaxDetails" id="PaxDetails_0" value="1692500">1.  Arun Kumar </span></div>
                <div id="errMsg" class="red font10">
                </div>
            </div>
            <div class=" req_row">
                <span class="bold">Please enter remarks <sup class="red">*</sup></span>
                <textarea rows="3" cols="56" id="txtRemarks"></textarea>
                <div id="errMsgRemarks" class="red font10"></div>
            </div>
            <div class="req_row" id="sendReqMsgs" style="border-bottom: none;">
                <span class="bold">Note.</span>
                <ol class="note_req">
                    <li>Partial Refund will be processed offline.</li>
                    <li>In case of Infant booking, cancellation will be processed offline.</li>
                    <li>In case of One sector to be cancel, please send the offline request.</li>
                    <li>In case of Flight cancelation/ flight reschedule, please select flight cancelled.</li>
                    <li>Cancellation Charges cannot be retrieved for Partial Cancelled Booking</li>
                </ol>
            </div>

        </div>
        <div class="req_row"></div>
        <span class="bold align_center fleft width_100 mt" id="CancellationChargesBlock" style="display: none;">Total Cancellation Charges: <tt id="CancellationCharges"></tt>
            <br>
            Total Refund Amount : <tt id="RefundedAmount"></tt></span>
        <span class="bold red font10 font10 align_center fleft width_100 mt" id="errCancellationCharges" style="display: none;"></span>

        <span class="req_row_btn" id="btnPanel">

            <div id="loaderCanCharges" class="ml align_center fleft width_100 mt mb" style="display: none">
                <img src="images/loaderNew.gif" alt="loading" style="vertical-align: middle;">
                <b class="ml mt">Loading Cancellation Charges...</b>
                <div class="clr"></div>
            </div>

            <input type="button" class="btn_main_s mr" value="View Cancel. Charges" id="CancellationChargesbtn" style="display: none;">
            <input type="button" class="btn_main_s mr" value="Send Request" id="btnSendChangeReq">
            <input type="button" class="btn_main_s" value="Cancel" id="btnSendChangeReqCancel">
        </span>

        <div class="req_row red bold align_center" id="processingMsg" style="display: none;">
            Your Request is Processing...
        </div>
    </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>



</li><li class="ng-scope">
<a target="_blank" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-search stylish-icon itemIco"></i>
<span class="itemText ng-binding">Review Cancellation Charges</span>

</a>




</li><li class="ng-scope">
<a target="_blank" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-print stylish-icon itemIco"></i>
<span class="itemText ng-binding">Print Ticket</span>

</a>




</li><li class="ng-scope">
<a target="_blank" href="#"><i class="itemArrow fa fa-chevron-right"></i>
<i class="fa fa-print stylish-icon itemIco"></i>
<span class="itemText ng-binding">Payment Receipt</span>

</a>




</li><li>


<a target="_blank" href="#"><i class="itemArrow fa fa-chevron-right"></i>

<i class="fa fa-envelope stylish-icon itemIco"></i>
<span class="itemText ng-binding">Email Ticket</span>
</a>


</li><li class="ng-scope">

</li></ul>
</div>
</section>
</section>
        </div>
    </div>
@include('frontend.includes.footer')
