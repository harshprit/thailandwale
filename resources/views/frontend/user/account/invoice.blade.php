<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 14px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table tr td:first-child {
    min-width: 307px;
}

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoicy td:nth-child(2) {
    min-width: 100px;
    width: 130px !important;
}

.invoicy td:nth-child(4) {
    min-width: 100px;
    width: 130px;
}

.invoicy td:nth-child(3) {
    min-width: 100px;
    width: 115px;
}
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
        font-size: 12px;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
       
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="http://thailandwale.com/thailandwale/assets/home/images/logo-2.png" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
                                Invoice #: 123<br>
                                Booking Date: January 1, 2015<br>
                                Thailand Wale Booking ID: February 1, 2015
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Jaunt Makers Pvt. Ltd.<br>
                                A-73, 2nd Floor, Sector 2, Noida, <br>
                                Uttar Pradesh 201301
                                GSTIN: 07AADCJ9011E1ZV
                            </td>
                            
                            <td>
                                Acme Corp.<br>
                                John Doe<br>
                                john@example.com<br>
                                Party GSTIN: Mr.Sumit Agarwal
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0">
            <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    Check #
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    Check
                </td>
                
                <td>
                    1000
                </td>
            </tr>
        </table>
        <table class="invoicy" cellpadding="0" cellspacing="0">   
            <tr class="heading">
                <td>
                    Flight Details
                </td>
                
                <td>
                    Base Fare
                </td>

                <td>
                    Service Fee/Taxes
                </td>

                <td>
                    Amount
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    <strong>Spice Jet SG-592 </strong> Delhi (DEL) To Bangkok (BKK)<br>
                    Travel Date - Thu, 23 Sept 2018, 3:50 hrs
                </td>
                
                <td>
                    ₹ 3000.00
                </td>

                <td>
                    ₹ 300.00
                </td>

                <td>
                    ₹ 3300.00
                </td>
            </tr>
            
            
            <tr class="item last">
                <td>
                    <strong>Spice Jet SG-592 </strong> Bangkok (BKK) To Delhi (DEL)<br>
                    Travel Date - Sat, 26 Sept 2018, 3:50 hrs
                </td>
                
                <td>
                    ₹ 3000.00
                </td>

                <td>
                    ₹ 300.00
                </td>

                <td>
                    ₹ 3300.00
                </td>
            </tr>
            
            <tr class="total">
                <td></td>
                <td></td>
                <td>Flight Total:</td>
                <td>
                    ₹ 3300.00
                </td>
            </tr>
        </table>
        <table class="invoicy" cellpadding="0" cellspacing="0">   
            <tr class="heading">
                <td>
                    Hotel Details
                </td>
                
                <td>
                    Base Fare
                </td>

                <td>
                    Service Fee/Taxes
                </td>

                <td>
                    Amount
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    <strong>Hotel Silver Oak </strong> Bangkok <br>
                    Stay Date - Thu, 23 Sept 2018,<br>
                    Stay Duration - 3 Nights
                </td>
                
                <td>
                    ₹ 3000.00
                </td>

                <td>
                    ₹ 300.00
                </td>

                <td>
                    ₹ 3300.00
                </td>
            </tr>
            
            
            
            <tr class="total">
                <td></td>
                <td></td>
                <td>Hotel Total:</td>
                <td>
                    ₹ 3300.00
                </td>
            </tr>
        </table>
        <table class="invoicy" cellpadding="0" cellspacing="0">   
            <tr class="heading">
                <td>
                    Tour Details
                </td>
                
                <td>
                    Base Fare
                </td>

                <td>
                    Service Fee/Taxes
                </td>

                <td>
                    Amount
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    <strong>The Amazing Thailand</strong><br>
                    Travel Date - Thu, 23 Sept 2018,<br>
                    Duration - 3 Nights/4 Days
                </td>
                
                <td>
                    ₹ 3000.00
                </td>

                <td>
                    ₹ 300.00
                </td>

                <td>
                    ₹ 3300.00
                </td>
            </tr>
            
            
            
            <tr class="total">
                <td></td>
                <td></td>
                <td>Tour Total:</td>
                <td>
                    ₹ 3300.00
                </td>
            </tr>
        </table>
        <table class="invoicy" cellpadding="0" cellspacing="0">   
            
            
           
            
            
            
            <tr class="total">
                <td></td>
                <td></td>
                <td>Super Total:</td>
                <td>
                    ₹ 9900.00
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
