<!doctype html>
<html lang="en">
    

<head>
        <title>thailandwale</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" href="assets/home/images/favicon.png" type="image/x-icon">
        
        <!-- Google Fonts -->	
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        
        <!-- Bootstrap Stylesheet -->	
        <link rel="stylesheet" href="{{asset('assets/home/css/bootstrap.min.css')}}">
        
         <!-- Datatable Stylesheet -->	
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
        
        
        <!-- Font Awesome Stylesheet -->
        <link rel="stylesheet" href="{{asset('assets/home/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/home/css/select2.css')}}">
            
        <!-- Custom Stylesheets -->	
        <link rel="stylesheet" href="{{asset('assets/home/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/home/css/zubair.css')}}">
        <link rel="stylesheet" href="{{asset('assets/home/css/ali.css')}}">
        <link rel="stylesheet" id="cpswitch" href="{{asset('assets/home/css/orange.css')}}">
        <link rel="stylesheet" href="{{asset('assets/home/css/responsive.css')}}">
        
        <!-- Flex Slider Stylesheet -->
        <link rel="stylesheet" href="{{asset('assets/home/css/flexslider.css')}}" type="text/css" />
        
        <!--Date-Picker Stylesheet-->
        
        <link rel="stylesheet" type="text/css" href="{{asset('assets/home/css/daterangepicker.css')}}" />
        
        <!-- Magnific Gallery -->
        <link rel="stylesheet" href="{{asset('assets/home/css/magnific-popup.css')}}">
        
        <!-- Color Panel -->
        <link rel="stylesheet" href="{{asset('assets/home/css/jquery.colorpanel.css')}}">
        <link rel='stylesheet' type='text/css' href="{{asset('assets/home/css/immybox.css')}}">
        <link rel="stylesheet" href="{{asset('assets/home/css/jquery-ui.css')}}" />
        <script src='https://code.jquery.com/jquery-1.10.2.min.js'></script>
        <script src='https://cdn.jsdelivr.net/npm/sweetalert2@7.28.1/dist/sweetalert2.all.min.js'></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111277165-12"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-111277165-12');
        </script>

        <script>
        window.oncroll = function() {myFunction()};
            
            function myFunction() {
                
                
                if (window.pageYOffset > 50||document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    document.getElementById("sidebar-hotel").className = "test";
                } else {
                    document.getElementById("sidebar-hotel").className = "";
                }
            }
</script>
        
    </head>
    
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bdad40d33c6fc47d2d4c9e1/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    <body id="flight-homepage">
   
    
    
        
        
        <!--============= TOP-BAR ===========-->
        <div id="top-bar" class="tb-text-white">
            <div class="container">
                <div class="row">          
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <nav class="navbar navbar-default main-navbar navbar-custom navbar-white" id="mynavbar">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" id="menu-button" type="button" data-toggle="collapse" data-target="#myNavbar1" aria-controls="myNavbar1" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>                        
                    </button>
                  
                    <a href="{{route('frontend.index')}}" class="navbar-brand"><img src="{{asset('assets/home/images/logo-2.png')}}"></a>
                </div><!-- end navbar-header -->
                
                <div class="collapse navbar-collapse" id="myNavbar1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown active" menu-name="flights">{{ link_to_route('frontend.index', trans('navs.frontend.flights')) }}
                          			
                        </li>
                        
                        <li class="dropdown" menu-name="hotels">{{ link_to_route('frontend.hotels', trans('navs.frontend.hotels')) }}
                         			
                        </li>
                        <li class="dropdown" menu-name="holidays">{{ link_to_route('frontend.holidays', trans('navs.frontend.holidays')) }}
                           		
                        </li>
                        <li class="dropdown" menu-name="tohfa"><a href="{{route('frontend.giftavacation')}}">Tohfa</a> 
                           <!--<span class="le-bc-badge"><img src="{{asset('assets/home/images/new-png-logo-1.png')}}"></span>		-->
                        </li>
                        <li class="dropdown" menu-name="elite"><a href="#">Elite Travellers</a> 
                           <!--<span class="le-bc-elite-badge"><img src="{{asset('assets/home/images/new-png-logo-1.png')}}"></span>		-->
                        </li>
                        <li class="dropdown" menu-name="more">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        More
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu more-links">
                        
                        <li>
                            <a class="btn btn-block" href="{{route('frontend.visa')}}">{{trans('navs.frontend.visa') }}</a>
                                            </li>
                        <li>
                            <a class="btn btn-block" href="{{route('frontend.activity','Transfers')}}">{{trans('navs.frontend.cabs') }}</a>
                        </li>                        <li>
                            <a class="btn btn-block" href="{{route('frontend.activity','Entertainment')}}">{{trans('navs.frontend.entertainment') }}</a>
                        </li>                </ul>
                </li>
                           			
                        
                     
                       
                    </ul>
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </nav><!-- end navbar -->
                        </div>
                    
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        
                        <div id="links">
                            <ul class="list-unstyled list-inline">
						 <li class="dropdown">
						     <a href="{{route('frontend.refer_and_earn')}}"><span class="fa fa-rupee"></span>{{trans('navs.frontend.refer_and_earn') }}</a>
                           		
                        </li>
                        <li class="dropdown">
                            <a href="{{route('frontend.support')}}"><span class="fa fa-headphones"></span>{{trans('navs.frontend.support') }}</a>
                           		
                        </li>
                                
								@if(!access()->user())
                                <li><a href="#" data-toggle="modal" data-target="#signin"><span><i class="fa fa-lock"></i></span>Login</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#signup"><span><i class="fa fa-user-plus"></i></span>Sign Up</a></li>
                                @else
                                
                                
                                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="fa fa-user"></span>
                        <strong>Hey, {{access()->user()->first_name}}</strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu menu-ka-drop">
                        
                        <li>
                                            <a href="{!! route('frontend.user.account') !!}" class="btn btn-block mcount-bt">My Account</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">My Trips</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">Cancellation</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">My Bookings</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">Wallet</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">Refer & Earn</a>
                        </li>
                        <li>
                                            <a href="{!! route('frontend.auth.logout') !!}" class="btn btn-block mcount-bt">Logout</a>
                        </li>
                    </ul>
                </li>
                
                                
                                @endif
            
                                
                                
                            </ul>
                            
                        </div><!-- end links -->
                    </div><!-- end columns -->				
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end top-bar -->
		
		
		
		<div class="modal fade" id="signin" role="dialog">
		
		    <ul class="signup-bhi login-ki-class dropdown-menu"  style="padding: 15px;min-width: 250px;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			
                      <li>
					  
                         <div class="row">
                            <div class="col-md-12">
                               <form method="POST" class="register-form" id="login-form" action="{{route('frontend.auth.login')}}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <div class="form-group">
                                     <label class="sr-only" for="emailInput">Email address</label>
                                     <input type="email" name="email" class="form-control" id="emailInput" placeholder="Email address" required="">
                                  </div>
                                  <div class="form-group">
                                     <label class="sr-only" for="passwordInput">Password</label>
                                     <input type="password" name="password" class="form-control" id="passwordInput" placeholder="Password" required="">
                                  </div>
                                  <div class="checkbox">
                                     <label>
                                     <input type="checkbox" name="remember"> Remember me
                                     </label>
                                  </div>
                                  <div class="form-group">
                                     <button type="submit" class="btn btn-success btn-block">Sign in</button>
                                  </div>
                               </form>
                            </div>
                         </div>
                      </li>
                      <li class="divider"></li>
                      <li>
                         <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','google')}}" id="sign-in-google">Sign Up with Google</a>
                         <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','facebook')}}" id="sign-in-facebook" >Sign Up with Facebook</a>
                      </li>
                   </ul>
				   </div>
					
					<div class="modal fade" id="signup" role="dialog">
					
				   <ul class="login-ki-class dropdown-menu"  style="padding: 15px;min-width: 250px;">
				   <button type="button" class="close" data-dismiss="modal">&times;</button>
				   
                      <li>
                         <div class="row">
                            <div class="col-md-12">
                               <form method="POST" class="register-form" id="register-form" action="{{route('frontend.auth.register')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
				<fieldset>
					<input class="form-control" placeholder="First Name" name="first_name" type="text">
					<input class="form-control" placeholder="Last Name" name="last_name" type="text">
			    	<input class="form-control middle" placeholder="E-mail" name="email" type="email">
			    	<input class="form-control middle" placeholder="Password" name="password" type="password" value="">
			    	<input class="form-control bottom" placeholder="Confirm Password" name="password_confirmation" type="password" value="">
					 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" name="is_term_accept"> I agree with all statements in  <a href="#" class="term-service">Terms of service</a>
                                     </label>
                                  </div>
				    
				    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign Up">
				    <p class="text-center">OR</p>
				    
				  	
				  	<p class="text-center"><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#signin">Already have an account?</a></p>
			  	</fieldset>
		  	</form>
                            </div>
                         </div>
                      </li>
                      <li class="divider"></li>
                      <li>
                         <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','google')}}" id="sign-in-google">Sign Up with Google</a>
                         <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','facebook')}}" id="sign-in-facebook" >Sign Up with Facebook</a>
                      </li>
                   </ul>
				   </div>
				   
@yield('content')

@include('frontend.includes.footer')


<script type="text/javascript">
$(document).ready(function(){
    $(".dropdown").on("click",function(){
    let menu_active = $(this).attr('menu-name');
    //alert(menu_active);
        $.session.remove('menu-name');
       $.session.set("menu-name", menu_active);
    });
    //alert($.session.get("menu-name"));
    if($.session.get("menu-name")!=null)
    {
        //alert("hello");
        let cur_menu = $.session.get("menu-name");
          let thiselement=  $("li[menu-name="+cur_menu+"]");
          $(thiselement).siblings().removeClass('active');
         $(thiselement).addClass('active');
         $.session.remove('menu-name');
    }
    //  alert($.session.get("menu-name"));
});

</script>

