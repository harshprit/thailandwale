@extends('frontend.layouts.app')
@include('frontend.includes.header')
@include('frontend.includes.nav1')

<div class="container">
<div class="abtak-ki-bookings fleft">
<h5 class="bdc-header">Upcoming Trips</h5>
@foreach($bookings as $booking)
<div class="booking-detail-card">

<span class="iconHolder"><i class="icon-reverse-hotels blueLt ico26"></i></span>
<div class="bcd-info row">
<div class="col-md-2 col-sm-2 col-xs-12">

<span class="mobdn ye-date">
<span class="bcd-d-ico fleft">01</span>
<span class="bcd-rd fleft grey">
<span class="bcd-wday">Tue</span>
<span class="bcd-m-date">Jan'19</span>
</span>
</span>
</div>
<div class="col-md-5 col-sm-5 col-xs-12 padB5">
<span class="fleft width100 ico17 greyDr ng-binding">IBIS STYLES KRABI AO NANG, Krabi</span>
<span class="fl greyLt lh1-2 padT3 ng-binding ng-scope" ng-if="trip_obj.trip_type=='Upcoming'">Your trip starts in 2 months</span>
</div>
<div class="col-md-4 col-sm-4 col-xs-12 greyLt">
<div class="fleft width100">
<span class="statusTag fleft marginR10 marginB5 ng-binding" style="background: #a46161;color: #fff;">Booking Confirmed</span>
</div>
<div class="fleft pnrWrap">
<span class="fleft width100 ng-binding">Booking Id: HTLA4XKQ3R</span>
<span class="fleft lh1-2 ng-binding">Booked on Thu, Nov 01, 2018</span>
</div>
</div><div class="col-md-1 jiski-bhi">
    <span class="fa fa-plane"></span>
</div>
</div>
<div class="buttonWrap">
<div class="fl width100">
<a href="#" target="_blank" class="button outlineBlue fleft marginR10 marginB10">
<i class="fl icon-settings2 ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2">Manage Booking</span>
</a>
<a href="#&quot;" target="_blank" class="button outlineBlue fleft marginR10 marginB10">
<i class="fleft icon-quick-help ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2">Quick Help</span>
</a>
<a href="#" target="_blank" class="button outlineBlue fleft marginR10 marginB10 ng-hide" ng-show="flavour=='mobile'">
<i class="fl icon-bubbles ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2">Contact Us</span>
</a>
<div class="ng-scope">
<div class="ng-scope">
<a target="_blank" class="button outlineBlue fleft marginR10 marginB10" href="#">
<i class="icon-print fl ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2 ng-binding">Print Voucher</span>
</a>
</div>



</div><div class="ng-scope">
<div class="ng-scope">
<a target="_blank" class="button outlineBlue fleft marginR10 marginB10" href="#">
<i class="icon-print fl ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2 ng-binding">Payment Receipt</span>
</a>
</div>



</div>
</div>
</div>
</div>
@endforeach
<div class="booking-detail-card">
<span class="iconHolder"><i class="icon-reverse-hotels blueLt ico26"></i></span>
<div class="bcd-info row">
<div class="col-md-2 col-sm-2 col-xs-12">

<span class="mobdn ye-date">
<span class="bcd-d-ico fleft">01</span>
<span class="bcd-rd fleft grey">
<span class="bcd-wday">Tue</span>
<span class="bcd-m-date">Jan'19</span>
</span>
</span>
</div>
<div class="col-md-5 col-sm-5 col-xs-12 padB5">
<span class="fleft width100 ico17 greyDr ng-binding">IBIS STYLES KRABI AO NANG, Krabi</span>
<span class="fl greyLt lh1-2 padT3 ng-binding ng-scope" ng-if="trip_obj.trip_type=='Upcoming'">Your trip starts in 2 months</span>
</div>
<div class="col-md-4 col-sm-4 col-xs-12 greyLt">
<div class="fleft width100">
<span class="statusTag fleft marginR10 marginB5 ng-binding" style="background: #a46161;color: #fff;">Booking Confirmed</span>
</div>
<div class="fleft pnrWrap">
<span class="fleft width100 ng-binding">Booking Id: HTLA4XKQ3R</span>
<span class="fleft lh1-2 ng-binding">Booked on Thu, Nov 01, 2018</span>
</div>
</div><div class="col-md-1 jiski-bhi">
    <span class="fa fa-hotel"></span>
</div>
</div>
<div class="buttonWrap">
<div class="fl width100">
<a href="#" target="_blank" class="button outlineBlue fleft marginR10 marginB10">
<i class="fl icon-settings2 ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2">Manage Booking</span>
</a>
<a href="#&quot;" target="_blank" class="button outlineBlue fleft marginR10 marginB10">
<i class="fleft icon-quick-help ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2">Quick Help</span>
</a>
<a href="#" target="_blank" class="button outlineBlue fleft marginR10 marginB10 ng-hide" ng-show="flavour=='mobile'">
<i class="fl icon-bubbles ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2">Contact Us</span>
</a>
<div class="ng-scope">
<div class="ng-scope">
<a target="_blank" class="button outlineBlue fleft marginR10 marginB10" href="#">
<i class="icon-print fl ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2 ng-binding">Print Voucher</span>
</a>
</div>



</div><div class="ng-scope">
<div class="ng-scope">
<a target="_blank" class="button outlineBlue fleft marginR10 marginB10" href="#">
<i class="icon-print fl ico14 padR5"></i>
<span class="txtTransUpper fl ico11 fn padT2 ng-binding">Payment Receipt</span>
</a>
</div>



</div>
</div>
</div>
</div>
</div>        
        
    </div>

@include('frontend.includes.footer')
