<head>

</head>
@include('frontend.includes.header')
@if(session()->get('flash_error'))
<div class=' alert alert-danger'>{{session()->get('flash_error')}}</div>
@endif
<form method="post"  action="{{route('frontend.search_flight')}}" >
<section class=" form-flights">
      <div class="container">
    <div class="row">
         <div class="tabbable-panel">
     <div class="tabbable-line">
         <div class="col-md-6 col-xs-12">
                <div class="flights-headline">
                  <h3>Book Flghts to Thailand</h3>
                </div>
            </div>
     <div class="col-md-6 col-xs-12">
   
		 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="travel_class" id="travel_class">
            <div id="tab" class="btn-group btn-group-justified" data-toggle="buttons">
        <a href="#one_way" class="btn btn-default active" data-toggle="tab">
          <input type="radio" name="journey_type" value="1" checked="checked"/>ONE WAY
        </a>
        <a href="#round_trip" class="btn btn-default" data-toggle="tab">
          <input type="radio" name="journey_type" value="2" />ROUND TRIP
        </a>
        <a href="#multi_trip" class="btn btn-default" data-toggle="tab">
          <input type="radio" name="journey_type" value="3"/>MULTI-CITY
        </a>
    
      </div>
     </div>

      <div class="tab-content">
        <div class="tab-pane active" id="">	
        
                          <div class="row" style="float: left;">
                        <div class="col-xs-6 col-md-3">
                              <h3>From</h3>
                        <select class="select2" id='input2' name="from[]" required="required">
                              <option value="">From</option>  
                                <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?> 
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php } }?>

                        </select>
                        <i class="fa fa-exchange" style=" position: absolute; color: #fff; font-size: 15px; bottom: 10px; right: -8px; "></i>
                            </div>
                        <div class="col-xs-6 col-md-3">
                              <h3>To</h3>
                        <select class="select2" id='input3' name="to[]" onchange="setOrigin(this);"  required="required">
                            <option value="">To</option>
                              <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?>
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php }
                                 }?>
                        </select>
                        
                            </div>
                            


                        <div class="col-xs-6 col-md-2">
                            <div class="date">
                                <div class="depart">
                                  <h3>Depart</h3>
                                  <input id="datepicker" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy"   required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>
                        <div class="col-xs-6 col-md-2 desktop-dhappa" id="multi-trip-hide">
                              <div class="return">
                            <h3>Return</h3>
                          <input  id="datepicker1" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" onfocus="this.value = '';"  required>
                          </div>
                          <i class="fa fa-close" style=" position: absolute; color: #fff; font-size: 15px; bottom: 10px; right: 18px; "></i>
                            </div>

					  <div class="col-xs-6 col-md-2">
                        <div class="popover-markup"> 
                            <div class="trigger form-group form-group-lg form-group-icon-left"><i class="fa fa-users input-icon input-icon-highlight"></i>
                                  <label>Passenger</label>
                                  <input type="hidden"  name="passenger_count" id="passenger_count" value="1">
                                  <input type="text" name="passengers_one" id="passengers_one" value="1|Economy" class="form-control">
                            </div>
                            <div id="contactForm" style="display:none">
                                  <div class="triangle"></div>
                            <div class="content">
                                <!-- adult row -->
							<div class="row">
                                  <div class="form-group">
                                      <div class="col-md-3">
                                <label class="control-label"><strong>Adults</strong><br>
                                      <i> (+12 yrs)</i></label></div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn btn-danger" data-dir="dwn" id="adult_dn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="adult_one" id="adult_one" class="form-control text-center" value="1">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="adult_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class active" type="button" value="2"> Economy </button></div>
                              </div>
							  </div>
							  <!-- adult row end -->
							  <!-- child row -->
							  	<div class="row">
                                  <div class="form-group">
                                      <div class="col-md-3">
                                <label class="control-label"><strong>Children</strong><br>
                                      <i> (+12 yrs)</i></label>
                                      </div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn btn-danger" data-dir="dwn" id="child_dn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="child_one" id="child_one" class="form-control text-center" value="0">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="child_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="3">Premium Economy </button></div>
                              </div>
							  </div>
							 <!-- child row end -->
							 <!-- infant row -->
							  <div class="row">
                                <div class="form-group">
                                    <div class="col-md-3">
                                <label class="control-label"><strong>Infants</strong><br>
                                      <i> (0-2 yrs)</i></label>
                                      </div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner1"> <span class="input-group-btn"> <a class="btn btn-danger" id="inf_dn" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="infants_one" id="infants_one" class="form-control text-center" value="0">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="inf_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="4"> Business </button></div>
                              </div>
							  </div>
							  <!-- infant row end -->
							  <div class="row">
							      <div class="form-group">
                                  <button class="btn btn-default btn-block demise" type="button" id="done">Done</button>
                                  </div>
                              </div>
								  </div>
								  </div>
                          </div>
                            </div>                    
                       <input type="submit" class="btn btn-warning form-search-btn mobile-dhappa" id="test1212" value="Search"> 

                             <!--- Multi City Starts from Here --->
          <div class="row multi-city-extra" id="multi_city" style="display:none">
                        <div class="row">
                        <div class="col-xs-6 col-md-2">
                              <h3>From</h3>
                        <select class="select2 input-select"  name="from[]" required="required">
                              <option value="">From</option>  
                                <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?> 
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php } }?>

                        </select>
                            </div>
                        <div class="col-xs-6 col-md-2">
                              <h3>To</h3>
                        <select class="select2 input-select"  name="to[]" required="required" onchange="setOrigin(this.value);">
                            <option value="">To</option>
                              <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?>
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php }
                                 }?>
                        </select>
                            </div>
                            


                        <div class="col-xs-6 col-md-2">
                            <div class="date">
                                <div class="depart">
                                  <h3>Depart</h3>
                                  <input  name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" class="datepicker"  required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>		
						</div>
                        <button id="addCityMulti" class="btn btn-warning form-search-btn" type="button">Add City</button>
                        
                      </div>
                      <!---- Multi City Ends Here--->							

							
						
					
                            
                   
                      </div>
                      <input type="submit" class="btn btn-warning form-search-btn desktop-dhappa" id="test1212" value="Search">
					  </div>
        
      </div>
    </div>
  </div>
</div>
</div>
    </section>
    </form>
                    @include('frontend.includes.flightoffers')
                    @include('frontend.includes.footer')
                    <script>
                    $(document).ready(function(){
                        $('#multi_city').find(':input').prop('disabled', true);
                    });
                    
                    </script>
                    <script>
                        $('.carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }

    next.children(':first-child').clone().appendTo($(this));
  }
});
                    </script>
<script>

$(document).ready(function(){

    
     
      $("#input3").on("change",function(){
         // alert("hello");
            var source = $("#input2").val();
            var destination = $("#input3").val();
           // alert(source+" "+destination);
           if(source=="" && destination==""){
                $("#input2").focus();
            }
             else if(source==destination)
            {
                  alert("Source and Destination can not be same.");
                    $('#input3 option[value=""]').prop('selected', true);
                    $("#select2-input3-container").text("Destination");
                  $("#input3").focus();
            }
           
            
      });
});
// $(function() {
//       $( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
//       });
$(document).ready(function() {
$(".flight-class").click(function () {
    
    $(".flight-class").removeClass("active");
    // $(".tab").addClass("active"); // instead of this do the below
    $('#travel_class').val($(this).val());
    $(this).addClass("active");
    // alert($(this).val());
    var passengers_count = $("#passenger_count").val();
     //alert(passengers_count);
    var t_class = travelClassInfo();
    
    $("#passengers_one").val(passengers_count+"|"+t_class);
    
    
});
});
</script> 
<script type="text/javascript">
  
    var dates = $("#datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {
    $("#datepicker1").datepicker('option', 'minDate', date);
  }
});
$("#datepicker1").datepicker({ numberOfMonths: 2,changeMonth: true,});
</script>

<script>
    var dates = $(".datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {
        $(".datepicker").closest('div.body').next().find('div.datepicker').datepicker('option', 'minDate', date);
  }
});
$(".datepicker").closest('div.body').next().find('div.datepicker').datepicker({ numberOfMonths: 2,changeMonth: true,});
</script>





<script>
 function travelClassInfo(){
         var travel_class= $("#travel_class").val();
        if(travel_class==""||travel_class==null)
        { travel_class="Economy"; }
        else if(travel_class==2){
            travel_class="Economy";
        }
        else if(travel_class==3){
            travel_class="Premium Economy";
        }
        else if(travel_class==4){
            travel_class="Business";
        }
        else{
            travel_class = "Economy";
        }
        return travel_class;
    }

$(document).ready(function(){
   
//increment the value of passenger
    $("#adult_up").on("click",function(){
        var ad_count = $("#adult_one").val();
        ad_count++;
        $("#adult_one").val(ad_count);
       
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
     $("#child_up").on("click",function(){
        
        var ch_count = $("#child_one").val();
        ch_count++;
        $("#child_one").val(ch_count);
        
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
    $("#inf_up").on("click",function(){
        var inf_count = $("#infants_one").val();
        inf_count++;
        $("#infants_one").val(inf_count);
        
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
//decrement the value of passenger
     $("#adult_dn").on("click",function(){
        var ad_count = $("#adult_one").val();
        ad_count--;
        
        if(ad_count>=1){
        $("#adult_one").val(ad_count);
         var passengers_count = $("#passenger_count").val();
        passengers_count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
       // alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
     $("#child_dn").on("click",function(){
        var ch_count = $("#child_one").val();
        ch_count--;
        
       if(ch_count>=0){
        $("#child_one").val(ch_count);
         var passengers_count = $("#passenger_count").val();
        passengers_count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
        //alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
    $("#inf_dn").on("click",function(){
        var inf_count = $("#infants_one").val();
        inf_count--;
        
        if(inf_count>=0){
        $("#infants_one").val(inf_count);
         var passengers_count = $("#passenger_count").val();
        passengers_count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
        //alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
});
</script>

<script type="text/javascript">
    $('#passengers_one').on('click',function(){
       $('#contactForm').show(); 
    });
</script>
<script type="text/javascript">
    $('#done').on('click',function(){
       $('#contactForm').hide(); 
    });
</script>
<script>
  $("#datepicker1").prop('disabled',true);
  $("#multi-trip-hide").show();
   $('.multi-trip-show').hide(); 
    $("a[href='#one_way']").on("click",function(){
         $("#multi-trip-hide").show();
          $('.multi-trip-show').hide(); 
          $('.multi-city-extra').hide();
      $("#datepicker1").prop('disabled',true);   
    });
</script>
<script>
    $("a[href='#round_trip']").on("click",function(){
         $("#multi-trip-hide").show();
          $('.multi-trip-show').hide(); 
          $('.multi-city-extra').hide();
      $("#datepicker1").prop('disabled',false);   
    });

</script>
<script>
if (window.matchMedia('(max-width: 768px)').matches)
{
        $("a[href='#one_way']").on("click",function(){
         $("#multi-trip-hide").addClass("desktop-dhappa");
         $("#multi-trip-hide").removeClass("mobile-dhappa");
    });
        $("a[href='#round_trip']").on("click",function(){
        $("#multi-trip-hide").removeClass("desktop-dhappa");
        $("#multi-trip-hide").addClass("mobile-dhappa");
        });
} 
</script>
<script type="text/javascript">
    $("a[href='#multi_trip']").on('click',function(){
        $("#multi-trip-hide").hide();
       $('.multi-trip-show').show(); 
       $('.multi-city-extra').show();
       $('#multi_city').find(':input').prop('disabled', false);
    });
</script>
<script>
    $('.select2').select2()
.on("select2:open", function () {
    $('.select2-results__options').niceScroll({
       cursorcolor: "#5fdfe8",
        cursorwidth: "8px",
        autohidemode: false,
        cursorborder: "2px solid #5fdfe8",
        horizrailenabled: false,
    });
});
</script>
<script>                                       
// $('#add-city').click(function() {
//     alert("hello");
//     $('.multi-trip-show:last').before('<div class="row" class="multi-trip-show" style="display:none"><div class="col-xs-12 col-md-2"><h3>From</h3><input id="input2" type="text" name="from" id "single" class="form-control"></div><div class="col-xs-12 col-md-2"><h3>To</h3><input id="input3" type="text" name="to"  class="form-control"></div><div class="col-xs-12 col-md-2"><div class="date"><div class="depart"><h3>Depart</h3><input  id="datepicker" name="depart_date" type="text" value="mm/dd/yyyy" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = "mm/dd/yyyy";}" required></div></div></div><span class="remove">Remove Option</span></div>');
// });
// // $('.multi-trip-show').on('click','.remove',function() {
// //  	$(this).parent().remove();
// // });
</script>                             

<script>
var count=0
var row='<div class="row addition-multi-city"><div class="col-xs-12 col-md-2"><h3>From</h3><select class="select2 input-select"  name="from[]" required="required"><option value="">Source</option> <?php if(isset($airport)) { foreach($airport as $city) {?> <option value="<?php echo $city['CityCode'].",".$city['CountryCode'];?>"><?php echo $city['CityName'].",". $city['CityCode'];?></option><?php } }?></select></div><div class="col-xs-12 col-md-2"><h3>To</h3><select class="select2 input-select"  name="to[]" required="required" onchange="setOrigin(this.text);"><option value="">Destination</option><?php if(isset($airport)){ foreach($airport as $city) {?><option value="<?php echo $city['CityCode'].",".$city['CountryCode'];?>"><?php echo $city['CityName'].",". $city['CityCode'];?></option><?php }}?></select></div><div class="col-xs-12 col-md-2"><div class="date"><div class="depart"><h3>Depart</h3><input class="datepicker" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy"   required></i></div><div class="clear"></div><i class="fa fa-close close_multi" style=" position: absolute; color: #fff; font-size: 15px; bottom: 10px; right: 18px; "></div></div></div>';                                       
$('#addCityMulti').on('click',function() {
    if(count<3)
    {    
    $('#addCityMulti').before(row);
    $('.select2').select2()
.on("select2:open", function () {
    $('.select2-results__options').niceScroll({
       cursorcolor: "#5fdfe8",
        cursorwidth: "8px",
        autohidemode: false,
        cursorborder: "2px solid #5fdfe8",
        horizrailenabled: false,
    });
});

    var dates = $(".datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {

    $(".datepicker").closest('div.body').next().find('div.datepicker').datepicker('option', 'minDate', date);
  }
});
$(".datepicker").closest('div.body').next().find('div.datepicker').datepicker({ numberOfMonths: 2,changeMonth: true,});
    //$('.multi-city-extra').append(row);

    count++;
    }
    else
    {
        $('#addCityMulti').hide();
        swal({
  type: 'error',
  title: 'Oops...',
  text: 'You can add maximum cities!',
  footer: '<a href>Why do I have this issue?</a>'
})
    }
    //$('.multi-trip-show:last').before('<div class="row" class="multi-trip-show" style="display:none"><div class="col-xs-12 col-md-2"><h3>From</h3><input id="input2" type="text" name="from" id "single" class="form-control"></div><div class="col-xs-12 col-md-2"><h3>To</h3><input id="input3" type="text" name="to"  class="form-control"></div><div class="col-xs-12 col-md-2"><div class="date"><div class="depart"><h3>Depart</h3><input  id="datepicker" name="depart_date" type="text" value="mm/dd/yyyy" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = "mm/dd/yyyy";}" required></div></div></div><span class="remove">Remove Option</span></div>');
});
// $('.multi-trip-show').on('click','.remove',function() {
//  	$(this).parent().remove();
// });
</script>
<script>
function setOrigin(val)
{
    // let op_value=$(val).find("option:selected").val();
    // let op_text=$(val).find("option:selected").text();

    // let next=$('.select2').next('select');

    // // let next=$(val).closest('div.body').next().find('div.select2');
    // alert($(next).find("option:selected").text());
    //  var option = $('<option></option>').attr("value",op_value).text(op_text);
    // $(next).empty().append(option);
    //alert($(next).find("option:selected").text());
    //alert(next);
}
</script>
<script>

$(document).click(function(e){

    // Check if click was triggered on or within #menu_content
    if( $(e.target).closest("#passengers_one").length > 0 || $(e.target).closest("#contactForm").length>0 ) {
        return false;
    }
    else
    {
        $('#contactForm').css('display','none'); 
    }
    })

</script>
<script>
$(document).on('click','.close_multi',function(){
$(this).closest('.addition-multi-city').remove();    
});
</script>

