@extends('frontend.layouts.app')

@if(!empty($errors->first()))
<div class=' alert alert-danger'>{{$errors->first()}}</div>
@endif

@section('content')
<div class="page-template">
    <div class="container">
    {!! $page->description !!}                    
    </div>
</div>
@endsection
