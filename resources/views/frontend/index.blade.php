<!--Start: Header Information -->
@include('frontend.includes.header')
<!--End: Header Information -->

<!-- Start: Flash error Information -->
@if(session()->get('flash_error'))
    <script>
    alert('<?php  echo session()->get('flash_error');?>');
    </script>

@endif
@if(!empty($errors->first()))
<!--<div class=' alert alert-danger'>{{$errors->first()}}</div>-->
@endif
@if($errors->has('msg'))
 <script>
    alert('<?php  echo $errors->first('msg');?>');
    </script>

@endif
@include('frontend.includes.search')

<section class="pt-5">
    <div class="container">
        <div class="landingCard appendBottom40 superOfferSection page-section sticky-scroll" id="Offers_Listing">
            <div class="landingCardSlider">
                @if(isset($package)&& count($package)>0)
                @php $count_pack=0; @endphp
                @foreach($package as $iv)
                <?php
                $destination_id_str = $iv->destination_id;
                $destination_id_ar = explode('-', $destination_id_str);
                $dest_id = array();
                for ($i = 0; $i < count($destination_id_ar); $i++) {
                    $dest_id_ar = explode(",", $destination_id_ar[$i]);
                    $dest_id[$i] = $dest_id_ar[0];
                }
                ?>
                 <form method="get" action="{{route('frontend.single_package')}}" id="package_form_{{$count_pack}}">
                <input type="hidden" name="package" value="{{$iv->slug}}">
                @php
                $packagePrice = 0;
                $packageOff= 0;
                                                          if(isset($iv->standard_price)){
                                                          $packagePrice = $iv->standard_price;
                                                          $packageOff = $iv->standard_off;
                                                          }
                                                          elseif(isset($iv->delux_price)){
                                                          $packagePrice = $iv->delux_price;
                                                          $packageOff = $iv->delux_off;
                                                          }
                                                          elseif(isset($iv->premium_price)){
                                                          $packagePrice = $iv->premium_price;
                                                          $packageOff = $iv->premium_off;
                                                          }
                                                        @endphp
                <input type="hidden" name="package_price" value="{{$packagePrice}}">
                <input type="hidden" name="package_price_off" value="{{$packageOff}}">
                <?php
                $dates = array(date("d/m/Y", strtotime("+30 days")));
                $count_date = 1;
                ?>
                <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                <?php
                $destination_str = $iv->destination_id;
                $destination_explode = explode("-", $destination_str);
                $dn = array();
                $dest_ar = array();
                $dest_night = array();
                //print_r($destination_str);
                for ($i = 0; $i < count($destination_explode); $i++) {
                    $dn = explode(",", $destination_explode[$i]);
                    $dest_ar[$i] = $dn[0];
                    if (isset($dn[1]) && $dn[1] != null)
                        $dest_night[$i] = $dn[1];
                }
                ?>
                @foreach($dest_night as $destnight)
                <?php
                $d = explode('/', $dates[$count_date - 1]);
                $date = $d[2] . '-' . $d[1] . '-' . $d[0];
                //$day=$search_info['NoOfNights'];

                $date = new DateTime($date);
                $check_out = $date->add(new DateInterval('P' . $destnight . 'D'))->format('d/m/Y');
                // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                $dates[$count_date] = $check_out;
                ?>
                <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                <?php $count_date++; ?>
                @endforeach
                <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>
                @for($j=0;$j < count($dest_ar);$j++)
                @foreach($destination as $dest)
                @if($dest_ar[$j]==$dest->id)
                <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                @endif
                @endforeach
                @endfor
                <div>
                    <div class="slideItem" tabindex="-1" style="width: 100%; display: inline-block;">
                        <div class="gallery">
                            <a href="javascript:$('#package_form_{{$count_pack}}').submit();" class="imgBox" >
                                <img src="{{  asset('storage/app/public/img/package').'/'.$iv->featured_image}}" alt="{{$iv->title}}" width="600" height="400">
                                <span class="tags">Package</span>
                            </a>
                            <div class="desc">
                                <div class="new-details">
                                    <h3 class="slideOffer">Exclusive Offer</h3>
                                    <p class="text-muted m-0">{{$iv->title}}</p>
                                    
                                    
                                    <!-- <p class="m-0">Promo code : YATRAPZ</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
                 </form>
                @php $count_pack++; @endphp
                @endforeach
                @endif

            </div>
        </div>
    </div>
</section>
<!-- slder sec -->
<section class="slider_sec">
    <div class="container fix-cont">
        <div class="row">
            <div class="col-lg-12">
                <div class="slider_in">
                    <h4>Travel Talk</h4>
                    <h5>Sharing travel experiences - one story at a time</h5>
                    <div id="owl-carousel" class="owl-carousel owl-theme mt-5">
                        <div class="item">
                            <div class="img_wrp">
                                <div class="img_text">
                                    <h5>What is Lorem Ipsum?</h5>
                                    <p>
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                                <img src="https://goo.gl/TXsYn9" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img_wrp">
                                <div class="img_text">
                                    <h5>What is Lorem Ipsum?</h5>
                                    <p>
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                                <img src="https://goo.gl/5ADFEH" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img_wrp">
                                <div class="img_text">
                                    <h5>What is Lorem Ipsum?</h5>
                                    <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                </div>
                                <img src="https://goo.gl/Y78H36" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img_wrp">
                                <div class="img_text">
                                    <h5>What is Lorem Ipsum?</h5>
                                    <p>
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                                <img src="https://goo.gl/AcCvCK" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img_wrp">
                                <div class="img_text">
                                    <h5>What is Lorem Ipsum?</h5>
                                    <p>
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                                <img src="https://goo.gl/FioWab" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img_wrp">
                                <div class="img_text">
                                    <h5>What is Lorem Ipsum?</h5>
                                    <p>
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                                <img src="https://goo.gl/hpPnqU" alt="">
                            </div>
                        </div>
                        <div class="item">
                            <div class="img_wrp">
                                <div class="img_text">
                                    <h5>What is Lorem Ipsum?</h5>
                                    <p>
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    </p>
                                </div>
                                <img src="https://goo.gl/iMmTH2" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End: Content Information -->

<!--Start: Footer Information -->
@include('frontend.includes.footer')
<script>
    $(document).ready(function() {
  $('#btnSubmit').click(function() {
    $('#deleteFrm').submit();
  });

});
</script>    
<!--End: Footer Information -->

<!---------------------------------------------------------- End Flight Script----------------------------------------------------->


