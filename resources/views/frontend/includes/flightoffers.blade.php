<section class="innerpage-wrapper">
<!-- Start: package carousel -->
    <div class="container">
        <div class="thai-destination">
            <h2 style="text-align: center;" class="thai-dhead">Latest Package</h2>
            <div class="row pack-destination">
                <div class="col-xs-11 col-md-12 col-centered">
                    <div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="4000">
				        <div class="package-card carousel-inner" style="border:1px solid transparent">
        				    @if(isset($package)&& count($package)>0)
        				        @php $count_pack=0; @endphp
                                @foreach($package as $iv)
                                    <?php
                                        $destination_id_str=$iv->destination_id;
                                        $destination_id_ar = explode('-',$destination_id_str);
                                        $dest_id=array();
                                        for($i=0;$i<count($destination_id_ar);$i++)
                                        {
                                            $dest_id_ar=explode(",",$destination_id_ar[$i]);
                                          $dest_id[$i]=$dest_id_ar[0];
                                        }
                                    ?>
                                    <div class="item {{ $count_pack==0 ? 'active' : '' }}">
						                <div class="image-dest-home carousel-col" style="padding-top:0px" >
						                    <a href="#">
                                                <div  class="img-responsive" >
                                                    <!-- set package data -->
                                                    <form method="get" action="{{route('frontend.single_package')}}" id="package_form">
                                                        
                                                        <input type="hidden" name="package" value="{{$iv->slug}}">
                                                        @php
                                                          $packagePrice = 0;
                                                          $packageOff= 0;
                                                          if(isset($iv->standard_price)){
                                                          $packagePrice = $iv->standard_price;
                                                          $packageOff = $iv->standard_off;
                                                          }
                                                          elseif(isset($iv->delux_price)){
                                                          $packagePrice = $iv->delux_price;
                                                          $packageOff = $iv->delux_off;
                                                          }
                                                          elseif(isset($iv->premium_price)){
                                                          $packagePrice = $iv->premium_price;
                                                          $packageOff = $iv->premium_off;
                                                          }
                                                        @endphp
                                                        <input type="hidden" name="package_price" value="{{$packagePrice}}">
                                                        <input type="hidden" name="package_price_off" value="{{$packageOff}}">
                                                        <?php $dates = array(date("d/m/Y", strtotime("+30 days")));
                                                            $count_date=1;
                                                        ?>
                                                        <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                                                        <?php
                                                            $destination_str=$iv->destination_id;
                                                            $destination_explode = explode("-",$destination_str);
                                                            $dn=array();
                                                            $dest_ar=array();
                                                            $dest_night=array();
                                                            //print_r($destination_str);
                                                            for($i=0;$i<count($destination_explode);$i++)
                                                            {
                                                                $dn=explode(",",$destination_explode[$i]);
                                                                $dest_ar[$i]=$dn[0];
                                                                if(isset($dn[1])&& $dn[1]!=null)
                                                                $dest_night[$i]=$dn[1];
                                                            }
                                                        ?>
                                                        @foreach($dest_night as $destnight)
                                                            <?php
                                                                $d=explode('/',$dates[$count_date-1]);
                                                                $date=$d[2].'-'.$d[1].'-'.$d[0];
                                                                //$day=$search_info['NoOfNights'];

                                                                $date=new DateTime($date);
                                                                $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');
                                                                // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                                                                $dates[$count_date]=$check_out;
                                                            ?>
                                                            <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                                                            <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                                                            <?php $count_date++; ?>
                                                        @endforeach
                                                        <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>
                                                        @for($j=0;$j < count($dest_ar);$j++)
                                                          @foreach($destination as $dest)
                                                            @if($dest_ar[$j]==$dest->id)
                                                              <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                                                            @endif
                                                          @endforeach
                                                        @endfor
                                                        <span class="but-div">
                                                            <button type="submit" class="packard-btn" name="submit" >View Details</button>
                                                        </span>
                                                    </form>
                                                    <!-- End: set package data -->
                                                    <!--<button class="packard-btn">View Details</button>-->
                                                    <span>
                                                        <img src="{{  asset('storage/app/public/img/package').'/'.$iv->featured_image}}" data-src="{{'#img/package/'.$iv->featured_image}}" class=" img-responsive  lazyImg" alt="{{'file failed to load '}}" title="{{$iv->title}}" style="height: 170px;width: 100%;">
                                                    </span>
                                                </div>

    <!----------------------------------- start: package info ------------------------------------------>
                                                <div class="entry" style="padding:20px">
                                                    <article class="entry-content1">
                                                        <span class="post-title">
                                                            <a href="#" style="cursor:default;text-decoration:none">
                                                                {{$iv->title}} <br>
                                                            </a>
                                                        </span>
                                                    </article>
                                                    <article class="entry-content">
                                                        {{ ($iv->no_days)==0 ? " ": ($iv->no_days)." Nights/ " }}{{($iv->no_days+1)." Days"}}

                                                        <br><span class="higlight emphasize value">
                                                        <i class="fa fa-rupee"></i>&nbsp;
                                                        {{$packagePrice}}

                                                        (Per  Person)
                                                        </span><br>
                                                    </article>
                                                    <div class="entry-meta">
                                                        <div class="inclusion-details">
                                                            <div class="row package-inclu">
                                                                <?php
                                                                    $inclusion_str = $iv->inclusion;
                                                                    $inclusion_ar = explode("-",$inclusion_str);
                                                                ?>
                                                              @if(isset($inclusion_ar))
                                                                @for($i=0;$i< count($inclusion_ar);$i++)
                                                                  <div class="col-xs-2">
                                                                    @if($inclusion_ar[$i]=="Flights")
                                                                      <div class="inclusion-x Flights">
                                                                        <i class="fa fa-plane"></i>
                                                                        <p class="flight-ico">Flights</p>
                                                                      </div>
                                                                    @endif
                                                                    @if($inclusion_ar[$i]=="Hotels")
                                                                      <div class="inclusion-x hotels">
                                                                        <i class="fa fa-hotel"></i>
                                                                        <p class="hotels-ico">Hotel</p>
                                                                      </div>
                                                                    @endif
                                                                    @if($inclusion_ar[$i]=="Meals")
                                                                      <div class="inclusion-x Meals">
                                                                        <i class="fa fa-cutlery"></i>
                                                                        <p class="meals-ico">Meals</p>
                                                                      </div>
                                                                    @endif
                                                                    @if($inclusion_ar[$i]=="Activities")
                                                                      <div class="inclusion-x activities">
                                                                        <i class="fa fa-camera-retro"></i>
                                                                        <p class="activities-ico">Activities</p>
                                                                      </div>
                                                                    @endif
                                                                    @if($inclusion_ar[$i]=="Transfers")
                                                                      <div class="inclusion-x transfers">
                                                                        <i class="fa fa-exchange"></i>
                                                                        <p class="transfers-ico">Transfers</p>
                                                                      </div>
                                                                    @endif
                                                                  </div>
                                                                @endfor
                                                              @endif
                                                                <!--<span>-->
                                                                <!--    <img style="width: 22px; height: 22px; margin-right: 2px;" src="http://placehold.it/500/bbbbbb/fff&text=1" data-src="images/hotel-category.png" title="Hotel">-->
                                                                <!--</span>-->
                                                                <span class="pcard-button">
                                                                    <!--<a class="btn btn-danger" style="float: right;" href="#">View Details</a>&nbsp;&nbsp;-->
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
<!----------------------------------- end: package info -------------------------------------------->
                                            </a>
						                </div>
					                </div>
					                @php $count_pack++; @endphp
                                @endforeach
                            @else
                                @for($i=0;$i<5;$i++)
                				    <div class="item {{ $i==0 ? 'active' : '' }}" >
                                        <div data-content="{{'dasfasdf'}}" class="image-dest carousel-col" style="padding-top:0px">
                							<a href="{{route('frontend.all_package',$i)}}" target="_blank">
                                                <div  class="img-responsive" >
                                                    <span>
                                                        <img src="http://placehold.it/500/bbbbbb/fff&text=1" class="img-responsive" data-src="{{'#'}}" class=" img-responsive  lazyImg" alt="{{'file failed to load '}}" title=""style="height: 260px;width: 100%;">
                                                    </span>
                                            <!----------------------------------- start: package info ------------------------------------------>
                                                    <div class="entry" style="padding:20px">
                                                        <article class="entry-content1"><h2 class="post-title">
                                                            <a href="#">
                                                            <?php echo "Title title title"; ?> <br> </a>
                                                            </h2>
                                                        </article>
                                                        <article class="entry-content">
                                                            <?php echo "duration"; ?><br>
                                                            <small class="text-package">
                                                                <?php echo substr("sjahfas sdhajfhjas dsjhfui",0,50); ?>
                                                            </small>
                                                            <br><span class="price"><span><b>Price&nbsp;</b></span><span><span class="higlight emphasize value"><i class="fa fa-rupee"></i>&nbsp;
                                                            &#36; <?php echo "1212335"; ?>/-<small class="text-package">Per  Person</small></span><br>
                                                        </article>
                                                        <div class="entry-meta" style="padding: 0px 11px;">
                                                            <span>
                                                                <img style="width: 22px; height: 22px; margin-right: 2px;" src="http://placehold.it/500/bbbbbb/fff&text=1" data-src="images/hotel-category.png" title="Hotel">
                                                            </span>
                                                            <span class="pcard-button">
                                                                <!--<a class="btn btn-danger" style="float: right;" href="#">View Details</a>&nbsp;&nbsp;-->
                                                            </span>

                                                        </div>
                                                    </div>
                                            <!----------------------------------- end: package info -------------------------------------------->
                                                </div>
                                            </a>
                						</div>
                					</div>
            					@endfor
					       @endif
				        </div>
        				<!-- Controls -->
        				<div class="left carousel-control">
        					<a href="#carousel" role="button" data-slide="prev">
        						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        						<span class="sr-only">Previous</span>
        					</a>
        				</div>
        				<div class="right carousel-control">
        					<a href="#carousel" role="button" data-slide="next">
        						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        						<span class="sr-only">Next</span>
        					</a>
        				</div>
            		</div>
	            </div>
	        </div>
	    </div>
    </div>
<!--End: package carousel -->
</section>

<section class="refernearnsec">
    <div class="container">
        <div class="row">
            <a href="{{route('frontend.refer_and_earn')}}" class="earn-ban">
        <img src="{{asset('assets/home/images/invite-n-earn.png')}}" class="refernearnhome"></a>
        </div>
    </div>
</section>




<!--<section class="home-offer-sect">-->
<!--    <div class="main">-->

<!--<h2>Today's Hot Offers</h2>-->

<!--<div id="myBtnContainer">-->
<!--  <button class="btn active off-nav" onclick="filterSelection('all')"> Show all</button>-->
<!--  <button class="btn off-nav" onclick="filterSelection('nature')"> Hotels</button>-->
<!--  <button class="btn off-nav" onclick="filterSelection('cars')"> Flights</button>-->
<!--  <button class="btn off-nav" onclick="filterSelection('people')"> Tours</button>-->
<!--</div>-->

<!-- Portfolio Gallery Grid -->
<!--<div class="row offer-row">-->
<!--  <div class="column-offer nature">-->
<!--    <div class="offer-content">-->
<!--      <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Mountains" style="width:100%">-->
<!--      <h4>Mountains</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->
<!--  <div class="column-offer nature">-->
<!--    <div class="offer-content">-->
<!--    <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Lights" style="width:100%">-->
<!--      <h4>Lights</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--<div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->
<!--  <div class="column-offer nature">-->
<!--    <div class="offer-content">-->
<!--    <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Nature" style="width:100%">-->
<!--      <h4>Forest</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->

<!--  <div class="column-offer cars">-->
<!--    <div class="offer-content">-->
<!--      <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Car" style="width:100%">-->
<!--      <h4>Retro</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->
<!--  <div class="column-offer cars">-->
<!--    <div class="offer-content">-->
<!--    <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Car" style="width:100%">-->
<!--      <h4>Fast</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->
<!--  <div class="column-offer cars">-->
<!--    <div class="offer-content">-->
<!--    <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Car" style="width:100%">-->
<!--      <h4>Classic</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->

<!--  <div class="column-offer people">-->
<!--    <div class="offer-content">-->
<!--      <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Car" style="width:100%">-->
<!--      <h4>Girl</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!-- <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!-- <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!-- <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->
<!--  <div class="column-offer people">-->
<!--    <div class="offer-content">-->
<!--    <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Car" style="width:100%">-->
<!--      <h4>Man</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->
<!--  <div class="column-offer people">-->
<!--    <div class="offer-content">-->
<!--    <img src="https://tvlk.imgix.net/imageResource/2018/11/21/1542841119739-ad616f64e802fe5ef5a39c10c7e852de.png" alt="Car" style="width:100%">-->
<!--      <h4>Woman</h4>-->
<!--      <p>Lorem ipsum dolor..</p>-->
<!--      <button class="accordion">Section 1</button>-->
<!--<div class="panel">-->
<!--  <div class="price-div">-->
<!-- <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!--  <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!-- <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--  <div class="price-div">-->
<!-- <div class="Price"><span class="rupee-symbol">₹</span>3599<span class="deal-date"> 29 Dec</span></div>-->
<!--  <div class="offer-link"><button class="btn price-btn">Book Now</button></div>-->
<!--  </div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->
<!-- END GRID -->
<!--</div>-->

<!-- END MAIN -->
<!--</div>-->
<!--</section>-->

<section class="experience_thailand">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="exper-icon">
                    <div class="exper-img">
                    <img src="{{asset('assets/home/images/thailandmap.png')}}" class="exper-icon-img">
                    </div>
                    <p class="exp-desc">Dedicated Only to Thailand</p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="exper-icon">
                    <div class="exper-img">
                    <img src="{{asset('assets/home/images/transparencyontop.png')}}" class="exper-icon-img">
                    </div>
                    <p class="exp-desc">Transparency on the Top</p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="exper-icon">
                    <div class="exper-img">
                    <img src="{{asset('assets/home/images/flexibility.png')}}" class="exper-icon-img">
                    </div>
                    <p class="exp-desc">Flexibility To Clients</p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="exper-icon">
                    <div class="exper-img">
                    <img src="{{asset('assets/home/images/driver.png')}}" class="exper-icon-img">
                    </div>
                    <p class="exp-desc">Indian Guides/Drivers for Hassle Free Communication</p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="exper-icon">
                    <div class="exper-img">
                    <img src="{{asset('assets/home/images/travel-insurance.png')}}" class="exper-icon-img">
                    </div>
                    <p class="exp-desc">Better Security with Insurance & Free Sim Card</p>
                </div>
            </div>
            <div class="col-md-2">
                <div class="exper-icon">
                    <div class="exper-img">
                    <img src="{{asset('assets/home/images/hometohome.png')}}" class="exper-icon-img">
                    </div>
                    <p class="exp-desc">Home To Home Service</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="testimonials">
    <div class="row">
        <div class="col-md-6">
            <h2>From Our Amazing Customers!</h2>
            <p>At Thailandwale, nothing else matters to us more than the satisfaction of our customers. We work hard day and night to provide our customers the best experience of Thailand. Here are some words of appreciation from our amazing customers. If you have used our services and would like to share your experience with us then you can <a href="https://thailandwale.com/feedback-page">visit here.</a></p>
        </div>
        <div class="col-md-6">
            
<div class="flexslider carousel">
  <ul class="slides">
    <li>
      <div class="testimonials">
                  <blockquote><p>My hubby to be and I were looking for an affordable but exciting honeymoon package and after a lot of
thought we decided to contact ThailandWale. First of all – WOW. We had so much fun!!!!! We travelled
to Phuket, Pataya and Bangkok and really bonded a lot. Thanks to the team at ThailandWale because
they had recommended this 6 nights package after listening to me talk about what I wanted. They were
very courteous and well informed!</p></blockquote>
                  <div class="carousel-info">
                    
                    <div class="pull-left">
                      <span class="testimonials-name">Shobhna Paul</span>
                    </div>
            </div>
            </div>
    </li>
    <li>
      <div class="testimonials">
                  <blockquote><p>Gifted by parents a trip to Thailand for their 30 th Anniversary. Since they have not travelled abroad a lot,
the Thailandwale team recommended that we get a professional guide to help them out throughout
their stay. They also told us about all the Indian food places my parents could visit as they are old and
don’t really like Thai food. Their anniversary holiday was great and they came back very happy. Lots of
thanks to the team for their help in arranging everything!</p></blockquote>
                  <div class="carousel-info">
                    
                    <div class="pull-left">
                      <span class="testimonials-name">Rahul Gupta</span>
                    </div>
            </div>
            </div>
    </li>
        <li>
      <div class="testimonials">
                  <blockquote><p>Travelling is my life and I live for travel and photography. I really wanted to travel on my own to an
unexplored place so the holiday planner here advised me to check out their villages package where they
take you to three different villages that are unexplored and away from city life. It was exactly what I was
looking for. Basically I wanted a unique and cool holiday and they gave me this experience.</p></blockquote>
                  <div class="carousel-info">
                    
                    <div class="pull-left">
                      <span class="testimonials-name">Tiana Kumte</span>
                    </div>
            </div>
            </div>
    </li>
            <li>
      <div class="testimonials">
                  <blockquote><p>I am an HR manager and used ThailandWale to arrange a corporate trip for our senior level executives due to a very profitable quarter. The team suggested a Pataya and Bangkok package which was very reasonable and we stayed only at 5 star properties throughout. Planning a corporate trip can be very challenging but the team at Thailandwale did it very well. We are extremely satisfied and will definitely be planning more trips with them in the future.</p></blockquote>
                  <div class="carousel-info">
                    
                    <div class="pull-left">
                      <span class="testimonials-name">Upsana Dhupia</span>
                    </div>
            </div>
            </div>
    </li>
  </ul>
</div>
        </div>
    </div>
</section>
