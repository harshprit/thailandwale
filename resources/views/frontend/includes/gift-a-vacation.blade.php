@include('frontend.includes.header')
<section class="gift-holiday-section">
    
    <div id="imageCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#imageCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#imageCarousel" data-slide-to="1"></li>
    <li data-target="#imageCarousel" data-slide-to="2"></li>
  </ol>
 <div class="carousel-inner">
   <div class="item active">
     
       <div class="col-md-12">
         <img src="{{asset('assets/home/images/happy-festival.png')}}" class="img-responsive" />
       </div>
       
 
   </div>
   <div class="item">
    
       <div class="col-md-12">
         <img src="{{asset('assets/home/images/happy-festival.png')}}" class="img-responsive" />
       </div>
    
   </div>
   <div class="item">
   
       <div class="col-md-12">
         <img src="{{asset('assets/home/images/happy-festival.png')}}" class="img-responsive" />
       </div>
  
   </div>
  </div>
  <a class="left carousel-control" href="#imageCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left lefty-con" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control righty-con" href="#imageCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
 </div>
    
</section>

<section class="second-gift-section">
    <div class="col-lg-12 col-md-12 col-sm-12 gift-left">
        <h2 class="send-karo-hmare-saath">Give Your Loved Ones an Ultimate Thailand Experience</h2>
        <hr class="heading-line" />
        <p class="gift-a-getaway">Gift a Getaway through Thailandwale. Whether it’s for a Wedding or a reward to an Employee, Getaway Breaks are high on experience. Getaways are customizable to individual preferences and create wonderful memories.
Thailandwale offers flexible budget & offers with following gift types.
</p>

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 iconerss">
            <img class="gift-iconers" src="{{asset('assets/home/images/onlyb.png')}}">
            <h3 class="gift-texts">Planned Budget With Flexible Location & Dates</h3>
            <p class="gifts-desc">We let the gift giver decide the budget and reciever the location and dates of tours</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 iconerss">
            <img class="gift-iconers" src="{{asset('assets/home/images/onlybl.png')}}">
            <h3 class="gift-texts">Planned Budget & Locations With Flexible Dates</h3>
            <p class="gifts-desc">The gift giver decide the budget as well as the location and reciever dates of tours</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 iconerss">
            <img class="gift-iconers" src="{{asset('assets/home/images/onlybld.png')}}">
            <h3 class="gift-texts">Fully Planned</h3>
            <p class="gifts-desc">Here the gift giver plans the complete itenary of the including the budget, locations, dates etc</p>
            </div>
    </div>
    
</section>
<section class="third-gift-section">
    <h2 class="your-forms-heading">Enquire Now For Exciting Gift Options</h2>
    <hr class="heading-line" />
    <form action="submit">
        <div class="form-group gift-form-section">
                                
                                
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titly">
                                        <input type="text" class="form-control first_height" name="gifter_name" id="first_name" placeholder="Your Name" title="enter your name.">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titly">
                                        <input type="text" class="form-control first_height" name="gifter_mobile" id="first_name" placeholder="Mobile No." title="enter your mobile no..">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titly">
                                        <input type="email" class="form-control first_height" name="gifter_email" id="first_name" placeholder="Email Id" title="enter your email id.">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titly">
                                        <input type="textarea" class="form-control first_height" name="gifter_message" id="first_name" placeholder="Message" title="enter your message.">
                                    </div>
                                
        </div>
        <div class="gift-form-button">
        <button class="btn btn-lg btn-success centra" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Enquire</button>
        </div>
    </form>
</section>
@include('frontend.includes.footer')