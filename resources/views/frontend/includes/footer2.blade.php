<button type="button" class="btn btn-primary feedback" data-toggle="modal" data-target="#myModal">
    Feedback
  </button>
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Feedback Form</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <div class="imagebg"></div>
<div class="row " style="margin-top: 50px">
    <div class="col-md-6 col-md-offset-3 form-container">
        
        <p>
            Please provide your feedback below:
        </p>
        <form id="reused_form">
            <div class="row">
            {{ csrf_field() }}
                <div class="col-sm-12 form-group">
                <label>How do you rate your overall experience?</label>
                <p>
                    <label class="radio-inline">
                    <input type="radio" name="experience" id="radio_experience" value="bad" >
                    Bad
                    </label>

                    <label class="radio-inline">
                    <input type="radio" name="experience" id="radio_experience" value="average" >
                    Average
                    </label>

                    <label class="radio-inline">
                    <input type="radio" name="experience" id="radio_experience" value="good" >
                    Good
                    </label>
                </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 form-group">
                    <label for="comments">
                        Comments:</label>
                    <textarea class="form-control" type="textarea" name="comments" id="comments" placeholder="Your Comments" maxlength="6000" rows="7"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-group">
                    <label for="name">
                        Your Name:</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="col-sm-6 form-group">
                    <label for="email">
                        Email:</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>
            </div>

                        <div class="row">
                <div class="col-sm-12 form-group">
                    <button type="button" onclick="sendfeedback()" class="btn btn-lg btn-warning btn-block" >Post </button>
                </div>
            </div>

        </form>
        <div id="success_message" style="width:100%; height:100%; display:none; ">
            <h3>Posted your feedback successfully!</h3>
        </div>
        <div id="error_message"
                style="width:100%; height:100%; display:none; ">
                    <h3>Error</h3>
                    Sorry there was an error sending your form.

        </div>
    </div>
</div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
<footer class="pb-3 main-footer footerGradient">
    <div class="container">

        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">
                <!--Big Column-->
                <div class="big-column col-md-12 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!--Footer Column-->
                        <div class="footer-column col-md-7 col-sm-7 col-xs-12">
                            <div class="footer-widget logo-widget">
                                <div class="widget-content">
                                    <div class="text">
                                        <h4 class="footerH">Company Info</h4>
                                        <p class="text-justify">Established in 2016, Thailandwale is a young and vibrant collective of travel agents 
                                            who are focussed on providing memorable trips to Thailand. We work exclusively with a 
                                            customer-focussed mind set, which means that your satisfaction and happiness is our priority. 
                                            We provide end-to-end services to ensure that each aspect of your trip is taken care of. 
                                            From handling flight bookings and hotel reservations to planning your itinerary and offering 
                                            Indian guides in Thailand, we do it all!</p>
                                    </div>
                                    <div class="text">
                                        <h4 class="footerH">Product Offerings</h4>
                                        <p class="text-justify">We offer extensive range of services such as Flights, Hotels, Holidays, 
                                            Transfers & Activities specialised in Thailand with amazing offers and incentives.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-5 col-sm-5 col-xs-12">
                            <div class="footer-widget links-widget">
                                <div class="text">
                                    <h4 class="footerH">Feedback</h4>
                                    <p class="text-justify">We are a customer focused company and always loves to hear from you. 
                                        For feedback fillout the form by clicking Here.</p>
                                </div>
                            </div>

                        </div>
                        <div class="footer-column col-md-3 col-sm-4 col-xs-12">
                            <div class="footer-widget links-widget">

                            </div>

                        </div>

                    </div>
                </div>
                <!--Big Column-->
            </div>
            <div class="row clearfix">
                <div class="container text-center">
                    <div class="copyright">Copyright © 2019 Thailandwale. &nbsp; All Rights Reserved</div>
                </div>
            </div>
        </div>

    </div>

    <!--Footer Bottom-->

 	<!-- Page Scripts Starts -->
     <script src="{{asset('assets/home/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery.colorpanel.js')}}"></script>
	<script src="{{asset('assets/home/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery.flexslider.js')}}"></script>
	<script src="{{asset('assets/home/js/owl.carousel.min.js')}}"></script>	
	<script src="{{asset('assets/home/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-navigation.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-flex.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-date-picker.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery-ui.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery.immybox.min.js')}}"></script>
	<script src="{{asset('assets/home/js/select2.full.js')}}"></script>
	<script src="{{asset('assets/home/js/main.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery.session.js')}}"></script>
    <!--<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.js" type="text/javascript"></script>-->

		<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <!-- Bootstrap JavaScript -->


	<script type="text/javascript" src="{{asset('assets/home/js/moment.min.js')}}"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>

	<script language="JavaScript" src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>

    <script src="{{asset('assets/home/js/particles.js')}}"></script>
    <script src="{{asset('assets/home/js/main_1.js')}}"></script>
   
    <script>
$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
$(document).ready(function () {
    $('.landingCardSlider').slick({
          dots: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          responsive: [
                            {
                              breakpoint: 1200,
                              settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1
                              }
                            },
                            {
                              breakpoint: 1008,
                              settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                              }
                            },
                            {
                              breakpoint: 500,
                              settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                              }
                            },
                            

                          ]
        });

    $('#owl-carousel').owlCarousel({
        loop: true,
        margin: 50,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            768: {
                items: 3,
                nav: false
            },
            1000: {
                items: 3,
                nav: false,
                loop: true
            }
        }
    });
});
function sendfeedback(){
 

        //get the action-url of the form
        var actionurl = base_url+'submitform';
console.log($("#reused_form").serialize());
        //do your own request an handle the results
        $.ajax({
                url: actionurl,
                type: 'post',
                dataType: 'application/json',
                data: $("#reused_form").serialize(),
                headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
                success: function(data) {
               if(data.code=='200'){
                alert(data.msg);
               }
                }
        });
}
    </script>

</footer> 