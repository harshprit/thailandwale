<head>

<script type="text/javascript" src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>



<style>
.paxCounter ul li.selected {
    background: #164880;
    color: #fff;
    border-radius: 3px;
}
.filterOptins{
	overflow:auto;
    max-height: 390px;
}
</style>
</head>
<div id="colorlib-reservation">
			<!-- <div class="container"> -->
				<div class="row">
					<div class="search-wrap">
						<div class="container">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#flight"><i class="flaticon-plane"></i> Flight</a></li>
								<li><a data-toggle="tab" href="#hotel"><i class="flaticon-resort"></i> Hotel</a></li>
								<li><a data-toggle="tab" href="#car"><i class="flaticon-car"></i> Car Rent</a></li>
								<li><a data-toggle="tab" href="#cruises"><i class="flaticon-boat"></i> Cruises</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div id="flight" class="tab-pane fade in active">
								<form method="post"  action="{{route('frontend.search_flight')}}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="row">
				              	 <div class="col-md-2">
				              	 	<div class="form-group">
				                    <label for="date">From</label>
				                    <div class="form-field">
														<select class="templatingSelect2 form-control" name="from">
														<option value=""></option>
														@foreach($airport as $apt)
														@if($apt['CountryCode']=='TH'||$apt['CountryCode']=='IN')
														<option value="{{$apt['CityCode']}}">{{$apt['CityName']}} ({{$apt['CityCode']}})</option>
														@endif
														@endforeach

		</select>

				                    </div>
				                  </div>
				              	 </div>
<div class="col-md-2">
				              	 	<div class="form-group">
				                    <label for="date">To</label>
				                    <div class="form-field">
														<select class="templatingSelect2 form-control" name="to">
        <option value=""></option>
												@foreach($airport as $apt)
												@if($apt['CountryCode']=='TH'||$apt['CountryCode']=='IN')
												<option value="{{$apt['CityCode']}}">{{$apt['CityName']}} ({{$apt['CityCode']}})</option>
												@endif
												@endforeach
		</select>
				                    </div>
				                  </div>
				              	 </div>
				                <div class="col-md-2">
				                  <div class="form-group">
				                    <label for="date">Departure</label>
				                    <div class="form-field">
				                      <i class="icon icon-calendar2"></i>
				                      <input type="text" id="depart_date" class="form-control date" placeholder="Departure-Date" name="depart_date">
				                    </div>
				                  </div>
				                </div>

				                <div class="col-md-1">
				                  <div class="form-group">
				                    <label for="guests">Adults</label>
				                    <div class="form-field">
				                      <i class="icon icon-arrow-down3"></i>
				                      <select name="adults" id="adults" class="form-control">
				                        <option value="1">1</option>
				                        <option value="2">2</option>
				                        <option value="3">3</option>
				                        <option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
				                        <option value="7">7</option>
				                        <option value="8">8</option>
				                        <option value="9">9</option>
				                      </select>
				                    </div>
				                  </div>
				                </div>
<div class="col-md-1">
				                  <div class="form-group">
				                    <label for="guests">Childs</label>
				                    <div class="form-field">
				                      <i class="icon icon-arrow-down3"></i>
				                      <select name="child" id="child" class="form-control">
				                        <option value="0">0</option>
																<option value="1">1</option>
				                        <option value="2">2</option>
				                        <option value="3">3</option>
				                        <option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
				                      </select>
				                    </div>
				                  </div>
				                </div>

<div class="col-md-1">
				                  <div class="form-group">
				                    <label for="guests">Infant</label>
				                    <div class="form-field">
				                      <i class="icon icon-arrow-down3"></i>
				                      <select name="infant" id="infant" class="form-control">
				                        <option value="0">0</option>
																<option value="1">1</option>
				                        <option value="2">2</option>
				                        <option value="3">3</option>
				                        <option value="4">4</option>
																<option value="5">5</option>
																<option value="5">6</option>
				                      </select>
				                    </div>
				                  </div>
				                </div>
<div class="col-md-2">
				                  <div class="form-group">
				                    <label for="guests">Class</label>
				                    <div class="form-field">
				                      <i class="icon icon-arrow-down3"></i>
				                      <select name="class" id="class" class="form-control">
				                        <option value="2">Economy</option>
				                        <option value="3">Premium Economy</option>
																<option value="4">Business</option>
																<option value="5">Premium Business</option>
																<option value="6">First</option>
				                      </select>
				                    </div>
				                  </div>
				                </div>
											</div>
											<div class="row submit-flight">
				                  <input type="submit" name="submit" id="submit" value="Find Flights" class="btn btn-primary btn-block">
				                </div>
				            </form>
				         </div>
				         <div id="hotel" class="tab-pane fade">
						      <form method="post" class="colorlib-form" action="{{route('frontend.search_hotel')}}">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
				              	<div class="row">
				              	 <div class="col-md-4">
												 <label for="destination">Destination:</label>
									<div class="select" name="destination">
				              	 	 <input class="selec-dest" type="text" name="destination" id="destination" onclick="secdest();">
														<div id="sel_dest" style="display:none;">
														<ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="des_list" tabindex="0" style="top: -24px; left: 0.5px; width: 361px;">
														<li class="ui-autocomplete-category">Search Results</li>
														@foreach($cities as $city)
														<li aria-label="Search Results : {{$city->CityName}}" class="ui-menu-item" id="ui-id-400" tabindex="-1">
														<div class="autoCompleteItem">
														<p>
														<span class="autoCompleteItem__label">{{$city->CityName}},{{$city->CityId}}</span>
														<span class="autoCompleteItem__cntry">City<span></span></span></p>
														<p class="autoCompleteItem__desc"></p></div></li>
														@endforeach
														</ul></div>
									  </div>
				              	 </div>
				                <div class="col-md-2">
				                  <div class="form-group">
				                    <label for="date">Check-in:</label>
				                    <div class="form-field">
				                      <i class="icon icon-calendar2"></i>
				                      <input type="text" id="date" class="form-control date" placeholder="Check-in date" name="check_in_date">
				                    </div>
				                  </div>
				                </div>
				                <div class="col-md-2">
				                  <div class="form-group">
				                    <label for="date">Check-out:</label>
				                    <div class="form-field">
				                      <i class="icon icon-calendar2"></i>
				                      <input type="text" id="date" class="form-control date" placeholder="Check-out date" name="check_out_date">
				                    </div>
				                  </div>
				                </div>
				                <div class="col-md-2">
				                  <div class="form-group">
				                    <label for="guests">Room/Guests</label>
				                    <div class="form-field">
				                      <i class="icon icon-arrow-down3"></i>
				                      <input id="add_rooms" type="text" onclick="addrooms();" >
									  <input type="hidden" name="rooms" id="rooms">
									  <input type="hidden" name="guests" id="guests">
															<div class="filterOptins" id="add_rooms_filter" style="top: 171px; display: none; left: 791.5px;">
										<div class="">

										<div class="paxFilter" style="display: block;">
											<div class="allRooms" id="js-allRooms">
												<div class="clearfix roomOption" id="js-roomOption">
													<p class="roomOption__title" id="js-roomOption__title">
														<span class="roomCounter" id="js-roomCounter">ROOM 1</span>
														<span class="o-i-cross-dark removeRoom" id="js-removeRoom_0"></span></p>
														<p class="roomOption__status" id="js-roomOption__status" style="display: none;">6 Adults, 0 Children<span class="editOption" onclick="editRoomDetails(this);">Edit</span></p>
													<div class="roomOption__details" id="js-roomOption__details" style="display: block;">
														<div class="paxCounter"><div class="paxCounter__title" id="js-paxCounter__title_3">
															<p class="">Adult <span class="paxCounter__titleInst">(+12 yrs)</span></p>
														</div>
														<div class="paxCounter__counter">
															<ul class="adult_counter" id="js-adult_counter">
															<li>1</li>
															<li class="">2</li>
															<li class="">3</li>
															<li class="">4</li>
															<li class="">5</li>
															<li class="selected">6</li>
														</ul>
													</div>
												</div>
												<div class="paxCounter" id="hotel_children">
													<div class="paxCounter__title" id="js-paxCounter__title_4">
														<p class="">Children <span class="paxCounter__titleInst">1-12 yrs</span></p>
													</div>
													<div class="paxCounter__counter">
														<ul class="child_counter" id="js-child_counter" onclick="showAge()">
															<li>1</li>
															<li class="">2</li>
															<li>3</li>
															<li>4</li>
														</ul>
														<span class="o-i-cross-dark removePaxCounter hidden"></span>
														<div class="clearfix" id="allPaxAge" style="display: block;">
														</div>
										</div>
									</div>

							</div>
						</div>
					</div>
					<p class="append_bottom20">
						<button id="js-addGuestRoom" class="btn btn-default marL5">Add Room</button>
						<a class="close_pax pull-right" id="done">Done</a></p></div>


			</div>
		</div>
				                    </div>
				                  </div>
				                </div>
				                <div class="col-md-2">
				                  <input type="submit" name="submit" id="submit" value="Find Hotel" class="btn btn-primary btn-block">
				                </div>
				              </div>
				            </form>
						   </div>
						   <div id="car" class="tab-pane fade">
						   	<form method="post" class="colorlib-form">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
				              	<div class="row">
				              	 <div class="col-md-4">
				              	 	<div class="form-group">
				                    <label for="date">Where:</label>
				                    <div class="form-field">
				                      <input type="text" id="location" class="form-control" placeholder="Search Location">
				                    </div>
				                  </div>
				              	 </div>
				                <div class="col-md-3">
				                  <div class="form-group">
				                    <label for="date">Start Date:</label>
				                    <div class="form-field">
				                      <i class="icon icon-calendar2"></i>
				                      <input type="text" id="date" class="form-control date" placeholder="Check-in date">
				                    </div>
				                  </div>
				                </div>
				                <div class="col-md-3">
				                  <div class="form-group">
				                    <label for="date">Return Date:</label>
				                    <div class="form-field">
				                      <i class="icon icon-calendar2"></i>
				                      <input type="text" id="date" class="form-control date" placeholder="Check-out date">
				                    </div>
				                  </div>
				                </div>
				                <div class="col-md-2">
				                  <input type="submit" name="submit" id="submit" value="Find Car" class="btn btn-primary btn-block">
				                </div>
				              </div>
				            </form>
						   </div>
						   <div id="cruises" class="tab-pane fade">
						      <form method="post" class="colorlib-form">
				              	<div class="row">
				              	 <div class="col-md-4">
				              	 	<div class="form-group">
				                    <label for="date">Where:</label>
				                    <div class="form-field">
				                      <input type="text" id="location" class="form-control" placeholder="Search Location">
				                    </div>
				                  </div>
				              	 </div>
				                <div class="col-md-3">
				                  <div class="form-group">
				                    <label for="date">Start Date:</label>
				                    <div class="form-field">
				                      <i class="icon icon-calendar2"></i>
				                      <input type="text" id="date" class="form-control date" placeholder="Check-in date">
				                    </div>
				                  </div>
				                </div>
				                <div class="col-md-3">
				                  <div class="form-group">
				                    <label for="guests">Categories</label>
				                    <div class="form-field">
				                      <i class="icon icon-arrow-down3"></i>
				                      <select name="category" id="category" class="form-control">
				                        <option value="#">Suite</option>
				                        <option value="#">Super Deluxe</option>
				                        <option value="#">Balcony</option>
				                        <option value="#">Economy</option>
				                        <option value="#">Luxury</option>
				                      </select>
				                    </div>
				                  </div>
				                </div>
				                <div class="col-md-2">
				                  <input type="submit" name="submit" id="submit" value="Find Cruises" class="btn btn-primary btn-block">
				                </div>
				              </div>
				            </form>

						   </div>
			         </div>
					</div>
				</div>
			</div>
		</div>
		<script>
		var child_count=0;
		var adult_count=6;
		var rooms=1;
		$(document).ready(function(){
			$('#add_rooms').val(rooms+"Rooms, "+(child_count+adult_count)+"Guests");
			$('#rooms').val(rooms);
			$('#guests').val(child_count+adult_count);
		});
		$(document).ready(function(){

			function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement;
    }

		var ul = document.getElementById('js-adult_counter');
		ul.onclick = function(event) {
        var target = getEventTarget(event);
		target.classList.add('selected');
		adult_count=target.innerHTML;
		alert(adult_count);
		}
		});
		</script>
		<script>


		$(document).ready(function(){
		$('#done').on('click',function(){
			$('#add_rooms_filter').hide();
		});
		});
		</script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#js-addGuestRoom').on('click',function(event){
				event.preventDefault();
			$('#js-allRooms').append('											<div class="clearfix roomOption" id="js-roomOption"><p class="roomOption__title" id="js-roomOption__title"><span class="roomCounter" id="js-roomCounter">ROOM '+(++rooms)+'</span> <span class="o-i-cross-dark removeRoom" id="js-removeRoom_0"></span></p><p class="roomOption__status" id="js-roomOption__status" style="display: none;">6 Adults, 0 Children<span class="editOption" onclick="editRoomDetails(this);">Edit</span></p><div class="roomOption__details" id="js-roomOption__details" style="display: block;"><div class="paxCounter"><div class="paxCounter__title" id="js-paxCounter__title_3"><p class="">Adult <span class="paxCounter__titleInst">(+12 yrs)</span></p></div><div class="paxCounter__counter"><ul class="adult_counter" id="js-adult_counter"><li>1</li><li class="">2</li><li class="">3</li><li class="">4</li><li class="">5</li><li class="selected">6</li></ul></div></div><div class="paxCounter" id="hotel_children"><div class="paxCounter__title" id="js-paxCounter__title_4"><p class="">Children <span class="paxCounter__titleInst">1-12 yrs</span></p></div><div class="paxCounter__counter"><ul class="child_counter" id="js-child_counter" onclick="showAge()"><li>1</li><li class="">2</li><li>3</li><li>4</li></ul><span class="o-i-cross-dark removePaxCounter hidden"></span></div></div></div></div>');
			});

		});

		function showAge()
		{
			function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement;
    }

		var ul = document.getElementById('js-child_counter');
		ul.onclick = function(event) {
        var target = getEventTarget(event);
		target.classList.add('selected');
		child_count=target.innerHTML;
		var age="";
	for(i=1;i<=child_count;i++)
		{
age+='<div class="paxAgeCounter paxCounter"><p class="paxAgeCounter__title">Age of Child <span class="childCounter">'+i+'</span></p><ul class="paxCounter__counter"><li class="">1</li><li>2</li><li class="">3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>8</li><li>9</li><li>10</li><li>11</li><li>12</li></ul></div>';
}
$('#allPaxAge').html(age);

    };


		}
		function addrooms()
		{
			var count=0;
			if(count==0)
			{
			$('#add_rooms_filter').toggle();
			$('#sel_dest').hide();
			count=1;
			}
			else
			{

				$('#add_rooms_filter').hide();

			}
		}
		</script>

		<script>
		function secdest()
		{
			var count1=0;
			if(count1==0)
			{
				$('#add_rooms_filter').hide();
			$('#sel_dest').toggle();

			count1=1;
			}
			else
			{
				$('#sel_dest').hide();
			}
		}

		</script>
		<script>
$(document).ready(function(){
		function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement;
    }

		var ul = document.getElementById('des_list');
		ul.onclick = function(event) {
        var target = getEventTarget(event);
		$('#destination').val(target.innerHTML);
		//alert(adult_count);
		}
		});

</script>

<script type="text/javascript">

  $(document).ready(function(){
  function setCurrency (currency) {
    if (!currency.id) { return currency.text; }
    var $currency = $('<span class="glyphicon glyphicon-' + currency.element.value + '">' + currency.text + '</span>');
    return $currency;
  };
  $(".templatingSelect2").select2({
    placeholder: "Search Destination", //placeholder
    templateResult: setCurrency,
    templateSelection: setCurrency,
  });
	})
</script>
