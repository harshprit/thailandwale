<!doctype html>
<html lang="en">


<head>
    <title>@if(isset($package_detail->title)){{$package_detail->title}} @else Thailandwale @endif</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="./assets/home/images/favicon.png" type="image/x-icon">

    <!-- Google Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

    <!-- Datatable Stylesheet -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets/home/css/owl-carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/home/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/home/chosen_select/chosen.min.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


    <link rel="stylesheet" href="{{asset('assets/home/css/magnific-popup.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        var base_url = "{{asset('/')}}";
    </script>
</head>

<body class="bgGradient">



    <section class="mb-5">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light customNav">
                <a class="navbar-brand" href="{{asset('/')}}">
                    <img class="img-logo" src="{{asset('assets/home/images/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <ul class="nav navbar-nav navbar-right">
      <li><a href="#"  data-toggle="modal" data-target="#signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="#" data-toggle="modal" data-target="#signin"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
            </nav>
        </div>
    </section>



    <div class="modal fade" id="signin" role="dialog">

        <ul class="signup-bhi login-ki-class dropdown-menu" style="padding: 15px;min-width: 250px;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <li>

                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" class="register-form" id="login-form" action="{{route('frontend.auth.login')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label class="sr-only" for="emailInput">Email address</label>
                                <input type="email" name="email" class="form-control" id="emailInput" placeholder="Email address" required="">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="passwordInput">Password</label>
                                <input type="password" name="password" class="form-control" id="passwordInput" placeholder="Password" required="">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> Remember me
                                </label>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block">Sign in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
            <li class="divider"></li>
            <li>
                <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','google')}}" id="sign-in-google">Sign Up with Google</a>
                <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','facebook')}}" id="sign-in-facebook">Sign Up with Facebook</a>
            </li>
        </ul>
    </div>

    <div class="modal fade" id="signup" role="dialog">

        <ul class="login-ki-class dropdown-menu" style="padding: 15px;min-width: 250px;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <li>
                <div class="row">
                    <div class="col-md-12">
                        <form method="POST" class="register-form" id="register-form" action="{{route('frontend.auth.register')}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <fieldset>
                                <input class="form-control" placeholder="First Name" name="first_name" type="text">
                                <input class="form-control" placeholder="Last Name" name="last_name" type="text">
                                <input class="form-control middle" placeholder="E-mail" name="email" type="email">
                                <input class="form-control middle" placeholder="Password" name="password" type="password" value="">
                                <input class="form-control bottom" placeholder="Confirm Password" name="password_confirmation" type="password" value="">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="is_term_accept"> I agree with all statements in <a href="#" class="term-service">Terms of service</a>
                                    </label>
                                </div>

                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign Up">
                                <p class="text-center">OR</p>


                                <p class="text-center"><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#signin">Already have an account?</a></p>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </li>
            <li class="divider"></li>
            <li>
                <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','google')}}" id="sign-in-google">Sign Up with Google</a>
                <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','facebook')}}" id="sign-in-facebook">Sign Up with Facebook</a>
            </li>
        </ul>
    </div>

    <!--Script to active the tab -->