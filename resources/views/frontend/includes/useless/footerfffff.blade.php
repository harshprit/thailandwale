<section class="end-section">
    <h2>About Thailandwale</h2>
    <p>Thailand is a wonderful destination with fascinating history, culture, enchanting temples, exotic islands, gracious hospitality and innumerous entertainment opportunities. Even on a tight budget, Thailand does not disappoint anyone. Bangkok and Pattaya are the most popular destinations. Pattaya offers long stretches of palm fringed white sand beaches, water sport activities, freshest seafood and pulsating nightlife. Try the spicy street food at Bangkok, befriend locals, enjoy the pulsating nightlife and… shop! With high number of tourist arrivals, Bangkok &amp; Pattaya have excellent selection of cuisines as wells as an incredible choice of accommodation. More money or less money, everyone equally enjoys at the land of smiles.</p>
<h2>Why Choose Thailandwale?</h2>
    <p>Thailandwale.com is an established destination management company, which has been providing world-class solution, service and product. With a vision to provide the disciplined and quality travel solutions, Thailandwale.com has been designing &amp; delivering value-added, state-of-the-art services with a dynamic blend of creativity, reliability &amp; integrity. We are a service orientated, privately owned organization and in these ever moving times, we realize that speed of response and attention to detail are both of crucial importance to our clients. Thailandwale.com aims to set itself apart from the rest by focusing on providing high quality service that meets the needs of the Diplomatic Missions and applicants, maintaining integrity and professionalism.
</p>
<p>Our Thailand packages are customizable to your needs and quite unique in their offering, showcasing jeweled sites and destinations beyond tourist traps. The high degree of personalized and professional services and attention to minute details has won the appreciation of our clientele who patronize us. Doing a good job really matters to us.Organizing a holiday on your own can be stressful and frustrating, which isn't really the point of a holiday after all. Let us do the work for you! By booking with us, you save time and money by booking all transportation, accommodation and other services directly with a local agency that can negotiate good rates with suppliers. And to ensure that our customers receive the best value for their money, we work with only the most reputable licensed suppliers around the world.Our extensive network of trusted travel partners combined with our unique local expertise allows us to pass along great savings to you.
</p>
<p>Thailandwale.com feels proud to have made it with the help of the whole team. Our policies, procedures, tools and enablers are important to the execution of quality audits. Commitment is not just a word. Thailandwale Team is committed to delivering intelligent solutions for your outsourcing needs. Its subject matter experts understand your business objectives and provide insight and input to mitigate any potential risks.</p>
</section>
<section id="footer" class="ftr-heading-w ftr-heading-mgn-2 ftr-top-pad">
        
		<div id="footer-top" class="ftr-top-grey">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footer-widget ftr-about ftr-our-company">
						<div class="row">
						    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 footer-widget ftr-about ftr-our-company">
						        <h3 class="ftr-title">Product Offerings</h3>
						        <a class="ftr-links" href="">Flights</a>
						        <a class="ftr-links" href="">Hotels</a>
						        <a class="ftr-links" href="">Holidays</a>
						        <a class="ftr-links" href="">Customize Your Trip</a>
<a class="ftr-links" href="">Tohfa</a>
						    </div>
						    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 footer-widget ftr-about ftr-our-company">
						        <h3 class="ftr-title">Useful Links</h3>
						        <a class="ftr-links" href="">About Us</a>
						        <a class="ftr-links" href="">FAQs</a>
						        <a class="ftr-links" href="">Contact Us</a>
						        <a class="ftr-links" href="">T&amp;C</a>
						        <a class="ftr-links" href="">Privacy Policy</a>
						    </div>
						</div>
						
						
						
					</div><!-- end columns -->
					
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footer-widget ftr-in-touch ">
						<h3 class="ftr-title">Get Social</h3>
						
						
						<ul class="social-links list-inline list-unstyled">
							<li><a href="#"><span><i class="fa fa-facebook"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-twitter"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-google-plus"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-pinterest-p"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-instagram"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-linkedin"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-youtube-play"></i></span></a></li>
						</ul>
						
					</div><!-- end columns -->
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 footer-widget ftr-about ftr-our-company">
						        <h3 class="ftr-title">News</h3>
						        <a class="ftr-links" href="">Thailand News 1</a>
						        <a class="ftr-links" href="">Thailand News 2</a>
						        <a class="ftr-links" href="">Thailand News 3</a>
						        <a class="ftr-links" href="">Thailand News 4</a>
<a class="ftr-links" href="">Thailand News 5</a>
						    </div><!-- end columns -->  
				</div><!-- end row -->
			</div><!-- end container -->
		</div><!-- end footer-top -->

		<div id="footer-bottom" class="ftr-bot-none">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="ftr-countrier">
					    
					  
					  
					  <div class="row ftr-bottom-cop">
					  <div class="contact-details">
					      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 left">Have issues ? <a href="http://www.thailandwale.com/contactus">Contact Us</a>
					      </div>
					      
					      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 right">© 2018 www.thailanwale.com. All rights reserved.
					      </div>
					      </div>
					    </div>
					</div><!-- end columns -->
					
					
				</div><!-- end row -->
			</div><!-- end container -->
		</div><!-- end footer-bottom -->
		
	</section>
	
	
	<!-- Page Scripts Starts -->
	<script src="{{asset('assets/home/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery.colorpanel.js')}}"></script>
	<script src="{{asset('assets/home/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery.flexslider.js')}}"></script>
	<script src="{{asset('assets/home/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-navigation.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-flex.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-date-picker.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery-ui.js')}}"></script> 
	<script src="{{asset('assets/home/js/jquery.immybox.min.js')}}"></script>
	<script src="{{asset('assets/home/js/select2.full.js')}}"></script>
	
	<script type="text/javascript" src="{{asset('assets/home/js/moment.min.js')}}"></script> 
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script> 
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
	
	<script language="JavaScript" src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
	<script type='text/javascript'>

$(function() {

 // simple example
 $('#input1').immybox({
   choices: [
	 {text: 'Alabama', value: 'AL'},
	 {text: 'Alaska', value: 'AK'},
	 {text: 'Arizona', value: 'AZ'},
	 {text: 'Arkansas', value: 'AR'},
	 {text: 'California', value: 'CA'},
	 {text: 'Colorado', value: 'CO'},
	 {text: 'Connecticut', value: 'CT'},
	 {text: 'Delaware', value: 'DE'},
	 {text: 'Florida', value: 'FL'},
	 {text: 'Georgia', value: 'GA'},
	 {text: 'Hawaii', value: 'HI'},
	 {text: 'Idaho', value: 'ID'},
	 {text: 'Illinois', value: 'IL'},
	 {text: 'Indiana', value: 'IN'},
	 {text: 'Iowa', value: 'IA'},
	 {text: 'Kansas', value: 'KS'},
	 {text: 'Kentucky', value: 'KY'},
	 {text: 'Louisiana', value: 'LA'},
	 {text: 'Maine', value: 'ME'},
	 {text: 'Maryland', value: 'MD'},
	 {text: 'Massachusetts', value: 'MA'},
	 {text: 'Michigan', value: 'MI'},
	 {text: 'Minnesota', value: 'MN'},
	 {text: 'Mississippi', value: 'MS'},
	 {text: 'Missouri', value: 'MO'},
	 {text: 'Montana', value: 'MT'},
	 {text: 'Nebraska', value: 'NE'},
	 {text: 'Nevada', value: 'NV'},
	 {text: 'New Hampshire', value: 'NH'},
	 {text: 'New Jersey', value: 'NJ'},
	 {text: 'New Mexico', value: 'NM'},
	 {text: 'New York', value: 'NY'},
	 {text: 'North Carolina', value: 'NC'},
	 {text: 'North Dakota', value: 'ND'},
	 {text: 'Ohio', value: 'OH'},
	 {text: 'Oklahoma', value: 'OK'},
	 {text: 'Oregon', value: 'OR'},
	 {text: 'Pennsylvania', value: 'PA'},
	 {text: 'Rhode Island', value: 'RI'},
	 {text: 'South Carolina', value: 'SC'},
	 {text: 'South Dakota', value: 'SD'},
	 {text: 'Tennessee', value: 'TN'},
	 {text: 'Texas', value: 'TX'},
	 {text: 'Utah', value: 'UT'},
	 {text: 'Vermont', value: 'VT'},
	 {text: 'Virginia', value: 'VA'},
	 {text: 'Washington', value: 'WA'},
	 {text: 'West Virginia', value: 'WV'},
	 {text: 'Wisconsin', value: 'WI'},
	 {text: 'Wyoming', value: 'WY'}
   ],
   defaultSelectedValue: 'LA'
 });

});
</script>
<script type='text/javascript'>                          
      $(function(){

        // simple example
        $('#input_hotel').immybox({
          choices: [
              <?php if(isset($cities)) {foreach($cities as $city) {?>
            {text: '<?php echo $city->CityName.','.$city->CityId;?>', value: '<?php echo $city->CityId;?>'},
              <?php }} else if(isset($airport)) { foreach($airport as $air) {?>
				{text: "<?php echo $air['CityName'].','.$air['AirportCode'];?>", value: '<?php echo $air['AirportCode'];?>'},
				<?php }}?>
          ],
          defaultSelectedValue: 'LA'
        });

	

	  });
	  $(function(){
		$('.input3').immybox({
          choices: [
              <?php if(isset($airport)) { foreach($airport as $air) { 
				    ?>
				{text: "<?php echo $air['CityName'].','.$air['AirportCode'];?>", value: '<?php echo $air['AirportCode'];?>'},
				<?php }}?>
          ],
          defaultSelectedValue: 'LA'
        });  
	  });
    </script>
	
<!-- //Calendar --> 
<!-- Page Scripts Ends --> 
<script type="text/javascript">
    //      $(function () { 
    // var $popover = $('.popover-markup>.trigger').popover({
    //   html: true,
    //   placement: 'bottom',
    //   content: function () {
          
    //     return $(this).parent().find('.content').html();
    //   }
    // });

    // open popover & inital value in form
    // var passengers = [1,0,0];
    // $('.popover-markup>.trigger').click(function (e) {
    //     e.stopPropagation();
    //     $(".popover-content input").each(function(i) {
    //         $(this).val(passengers[i]);
    //     });
    // });
    // close popover
    // $(document).click(function (e) {
    //     if ($(e.target).is('.demise')) {        
    //         $('.popover-markup>.trigger').popover('hide');
    //     }
    // });
// store form value when popover closed
//  $popover.on('hide.bs.popover', function () {
//     $(".popover-content input").each(function(i) {
//         passengers[i] = $(this).val();
//     });
//   });
    // spinner(+-btn to change value) & total to parent input 
//     $(document).on('click', '.number-spinner a', function () {
//         var btn = $(this),
//         input = btn.closest('.number-spinner').find('input'),
//         total = $('#passengers').val(),
//         oldValue = input.val().trim();
        
//     if(btn.attr('data-dir') == 'up' && btn.attr('id')=='adult_up') {
//       if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#adult').val(total);
        
      
//       }
//     }else if(btn.attr('data-dir') == 'up' && btn.attr('id')=='child_up')
//     {
//         if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#child_one').val(total);
        
      
//       }
//     } else if(btn.attr('data-dir') == 'up' && btn.attr('id')=='inf_up')
//     {
//         if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#infants_one').val(total);
      
//       }
//     }
    
//     else {
//       if (oldValue > input.attr('min')) {
//         oldValue--;
//         total--;
      
//       }
//     }
//     $('#passengers').val(total);
    
//     input.val(oldValue);
//   });

//   $(".popover-markup>.trigger").popover('show');
// });
</script>


<!-- modal--> 
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 



<!-- modal--> 
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 
<script type="text/javascript">$(document).ready( function() {
    $('#myCarousel').carousel({
        interval:   4000
    });
    
    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(this).parent().addClass('active');        
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');    
            }
        }
        clickEvent = false;
    });
});</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 
<!--========= modal=======-->
<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-chevron-up glyphicon-chevron-down');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<script>
$(document).ready(function() {
$(".greenOutlineBtn").click(function () {
    $(".greenOutlineBtn").removeClass("active");
    $(this).addClass("active");   
});
});
</script>
                             
<script>
        window.oncroll = function() {myFunction()};
            
            function myFunction() {
                alert("hello");
                
                if (window.pageYOffset > 50||document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    document.getElementById("sidebar-hotel").className = "test";
                } else {
                    document.getElementById("sidebar-hotel").className = "";
                }
            }
</script>

@if(isset($show_login) && $show_login)
<script type="text/javascript">
$(window).load(function() {
    $('#signin').modal('show');
});
</script>
@endif


@if(isset($show_signup) && $show_signup)
<script type="text/javascript">
$(window).load(function() {
    $('#signup').modal('show');
});
</script>
@endif

<script>
$('#PromoCodeApply').on('click',function(){
swal({
  title: 'Are you sure?',
  text: "In Order to Redeam the Coupon You are Required to Login!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, Login!'
}).then((result) => {
  if (result.value) {
  $('#signin').modal();
  }
})
});

$('#PromoCodeApply_login').on('click',function(){
let promoCode=$('#PromoCode').val();
let amount=$('#total_fare_cost').html();
alert(amount);	
	$.ajax({
		url: "{{route('frontend.user.verify_coupon')}}",
		type:"POST",
		data:{_token:"{{csrf_token()}}",code:promoCode,amount:amount},
		dataType:"json",
		success:function(data)
		{
			for(key in data)
			{
				if(key!=='error')
                {
                    $('#promo_discount_amount').html(data[key]);
                    let new_total=parseInt(amount)-data[key];
                    $('#payable').html(new_total);
                    $('#payable_div').show();
                    $('#promo_div').show();
                    $('#PromoCode').prop('disabled','disabled');
                    $('#PromoCodeApply_login').html('Applied');


                }
                else
                {
                    alert(data[key]);
                }
			}
		},
		error:function()
		{
			
		}
		
	});
	
});

</script>


<script>
$(document).ready(function(){
$("form").attr("autocomplete", "off");    
});


</script>