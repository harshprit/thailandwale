<section id="destinations" class="section-padding">
        	<div class="container">
        		<div class="row">
        			<div class="col-sm-12">
                    	<div class="page-heading">
                        	<h2>thailandwale Offer </h2>
                            <hr class="heading-line" />
                        </div><!-- end page-heading -->
                        
                        <div class="row">
                        
                        	<div class="col-sm-6 col-md-6">
                        		<div class="main-block destination-block">
                                	<div class="row">
                                    	<div class="col-sm-12 col-md-6 col-md-push-6 no-pd-l">
                                        	<div class="main-img destination-img">
                                            	<a href="#">
                                                    <img src="assets/home/images/destination-1.jpg" class="img-responsive" alt="destination-img"/>
                                                </a>
                                            </div><!-- end destination-img -->
                                        </div><!-- end columns -->
                                        
                                		<div class="col-sm-12 col-md-6 col-md-pull-6 no-pd-r">
                                        	<div class="destination-info">
                                                <div class="destination-title">
                                                    <a href="#">Blue Beach</a>
                                                    <p class="country">Dubai</p>
                                                    <p>Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text of the printing.</p>                                      
                                                	<span class="destination-price">$899/Person</span>
                                                    <a href="#" class="btn btn-orange">Book Now</a>
                                                </div><!-- end destination-title -->
                                            </div><!-- end destination-info -->
                                        </div><!-- end columns -->
                                        
                                    </div><!-- end row -->	
                                </div><!-- end destination-block -->
                        	</div><!-- end columns -->
                        	
                            <div class="col-sm-6 col-md-6">
                        		<div class="main-block destination-block">
                                	<div class="row">
                                    	<div class="col-sm-12 col-md-6 col-md-push-6 no-pd-l">
                                        	<div class="main-img destination-img">
                                            	<a href="#">
                                                    <img src="assets/home/images/destination-2.jpg" class="img-responsive" alt="destination-img"/>
                                                </a>
                                            </div><!-- end destination-img -->
                                        </div><!-- end columns -->
                                        
                                		<div class="col-sm-12 col-md-6 col-md-pull-6 no-pd-r">
                                        	<div class="destination-info">
                                                <div class="destination-title">
                                                    <a href="#">Sydney Tour</a>
                                                    <p class="country">Australia</p>
                                                    <p>Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text of the printing.</p>                                      
                                                	<span class="destination-price">$899/Person</span>
                                                    <a href="#" class="btn btn-orange">Book Now</a>
                                                </div><!-- end destination-title -->
                                            </div><!-- end destination-info -->
                                        </div><!-- end columns -->
                                        
                                    </div><!-- end row -->	
                                </div><!-- end destination-block -->
                        	</div><!-- end columns -->
                            
                            <div class="col-sm-6 col-md-6">
                        		<div class="main-block destination-block">
                                	<div class="row">
                                    	<div class="col-sm-12 col-md-6 col-md-push-6 no-pd-l">
                                        	<div class="main-img destination-img">
                                            	<a href="#">
                                                    <img src="assets/home/images/destination-3.jpg" class="img-responsive" alt="destination-img"/>
                                                </a>
                                            </div><!-- end destination-img -->
                                        </div><!-- end columns -->
                                        
                                		<div class="col-sm-12 col-md-6 col-md-pull-6 no-pd-r">
                                        	<div class="destination-info">
                                                <div class="destination-title">
                                                    <a href="#">Wild Forest</a>
                                                    <p class="country">Africa</p>
                                                    <p>Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text of the printing.</p>                                      
                                                	<span class="destination-price">$899/Person</span>
                                                    <a href="#" class="btn btn-orange">Book Now</a>
                                                </div><!-- end destination-title -->
                                            </div><!-- end destination-info -->
                                        </div><!-- end columns -->
                                        
                                    </div><!-- end row -->	
                                </div><!-- end destination-block -->
                        	</div><!-- end columns -->
                            
                            <div class="col-sm-6 col-md-6">
                        		<div class="main-block destination-block">
                                	<div class="row">
                                    	<div class="col-sm-12 col-md-6 col-md-push-6 no-pd-l">
                                        	<div class="main-img destination-img">
                                            	<a href="#">
                                                    <img src="assets/home/images/destination-4.jpg" class="img-responsive" alt="destination-img"/>
                                                </a>
                                            </div><!-- end destination-img -->
                                        </div><!-- end columns -->
                                        
                                		<div class="col-sm-12 col-md-6 col-md-pull-6 no-pd-r">
                                        	<div class="destination-info">
                                                <div class="destination-title">
                                                    <a href="#">Beautiful Paris</a>
                                                    <p class="country">France</p>
                                                    <p>Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text of the printing.</p>                                      
                                                	<span class="destination-price">$899/Person</span>
                                                    <a href="#" class="btn btn-orange">Book Now</a>
                                                </div><!-- end destination-title -->
                                            </div><!-- end destination-info -->
                                        </div><!-- end columns -->
                                        
                                    </div><!-- end row -->	
                                </div><!-- end destination-block -->
                        	</div><!-- end columns -->
                            
                        </div><!-- end row -->
                        
                        <div class="view-all text-center">
                        	<a href="#" class="btn btn-g-border">View All</a>
                        </div><!-- end view-all -->
                    </div><!-- end columns -->
                </div><!-- end row -->
        	</div><!-- end container -->
        </section><!-- end destinations -->