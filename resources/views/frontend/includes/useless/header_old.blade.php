<!doctype html>
<html lang="en">
    

<head>
        <title>Thailandwale</title>
        <link rel="stylesheet" href="{{asset('assets/home/fonts-new/material-icon/css/material-design-iconic-font.min.css')}}">


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" href="assets/home/images/favicon.png" type="image/x-icon">
        
        <!-- Google Fonts -->	
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        
        <!-- Bootstrap Stylesheet -->	
        <link rel="stylesheet" href="{{asset('assets/home/css/bootstrap.min.css')}}">
        
        <!-- Font Awesome Stylesheet -->
        <link rel="stylesheet" href="{{asset('assets/home/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/home/css/select2.css')}}">
            
        <!-- Custom Stylesheets -->	
        <link rel="stylesheet" href="{{asset('assets/home/css/style.css')}}">
        <link rel="stylesheet" id="cpswitch" href="{{asset('assets/home/css/orange.css')}}">
        <link rel="stylesheet" href="{{asset('assets/home/css/responsive.css')}}">
        
        <!-- Flex Slider Stylesheet -->
        <link rel="stylesheet" href="{{asset('assets/home/css/flexslider.css')}}" type="text/css" />
        
        <!--Date-Picker Stylesheet-->
        
        <link rel="stylesheet" type="text/css" href="{{asset('assets/home/css/daterangepicker.css')}}" />
        
        <!-- Magnific Gallery -->
        <link rel="stylesheet" href="{{asset('assets/home/css/magnific-popup.css')}}">
        
        <!-- Color Panel -->
        <link rel="stylesheet" href="{{asset('assets/home/css/jquery.colorpanel.css')}}">
        <link rel='stylesheet' type='text/css' href="{{asset('assets/home/css/immybox.css')}}">
        <link rel="stylesheet" href="{{asset('assets/home/css/jquery-ui.css')}}" />
        <script src='https://code.jquery.com/jquery-1.10.2.min.js'></script>
        <script src='https://cdn.jsdelivr.net/npm/sweetalert2@7.28.1/dist/sweetalert2.all.min.js'></script>
        
        <script>
        window.oncroll = function() {myFunction()};
            
            function myFunction() {
                
                
                if (window.pageYOffset > 50||document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    document.getElementById("sidebar-hotel").className = "test";
                } else {
                    document.getElementById("sidebar-hotel").className = "";
                }
            }
</script>
        
    </head>
    
    
    <body id="flight-homepage">
   
    
    
        
        
        <!--============= TOP-BAR ===========-->
        <div id="top-bar" class="tb-text-white">
            <div class="container">
                <div class="row">          
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <nav class="navbar navbar-default main-navbar navbar-custom navbar-white" id="mynavbar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" id="menu-button">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>                        
                    </button>
                  
                    <a href="#" class="navbar-brand"><img src="{{asset('assets/home/images/logo-2.png')}}"></a>
                </div><!-- end navbar-header -->
                
                <div class="collapse navbar-collapse" id="myNavbar1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown active">{{ link_to_route('frontend.index', trans('navs.frontend.flights')) }}
                          			
                        </li>
                        
                        <li class="dropdown">{{ link_to_route('frontend.hotels', trans('navs.frontend.hotels')) }}
                         			
                        </li>
                        <li class="dropdown">{{ link_to_route('frontend.all_package', trans('navs.frontend.all_package')) }} 
                           		
                        </li>
                        <li class="dropdown"><a href="#">Hot Deals</a>
                           			
                        </li>
						 <li class="dropdown">{{ link_to_route('frontend.refer_and_earn', trans('navs.frontend.refer_and_earn')) }}
                           		
                        </li>
                     
                       
                    </ul>
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </nav><!-- end navbar -->
                        </div>
                    
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div id="links">
                            <ul class="list-unstyled list-inline">
                                @if(!access()->user())
                                <li><a href="#" data-toggle="modal" data-target="#signin"><span><i class="fa fa-lock"></i></span>Login</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#signup"><span><i class="fa fa-plus"></i></span>Sign Up</a></li>
                                @else
                                <li><a href="{!! route('frontend.auth.logout') !!}"  ><span><i class="fa fa-unlock"></i></span>Logout</a></li>
                                @endif
                                <li>
                                	<form>
                                    	<ul class="list-inline">
                                        	<li>
                                                <div class="form-group currency">
                                                    <span><i class="fa fa-angle-down"></i></span>
                                                    <select class="form-control">
                                                        <option value="">$</option>
                                                        <option value="">£</option>
                                                    </select>
                                                </div><!-- end form-group -->
											</li>
                                            
                                            <li>
                                                <div class="form-group language">
                                                    <span><i class="fa fa-angle-down"></i></span>
                                                    <select class="form-control">
                                                        <option value="">EN</option>
                                                        <option value="">UR</option>
                                                        <option value="">FR</option>
                                                        <option value="">IT</option>
                                                    </select>
                                                </div><!-- end form-group -->
                                            </li>
										</ul>
                                    </form>
                                </li>
                            </ul>
                        </div><!-- end links -->
                    </div><!-- end columns -->				
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end top-bar -->



        

           <section class="signup">
            <div class="container-login-signup modal fade" id="signup" role="dialog">
		
          <button type="button" class="close" data-dismiss="modal">&times;</button>
		  
          
        
                <div class="signup-content" >
                    <div class="signup-form">
                        <h2 class="form-title">Sign up</h2>
                        <form method="POST" class="register-form" id="register-form" action="{{route('frontend.auth.register')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="first-name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="first_name" id="first_name" placeholder="Your First Name"/>
                            </div>
                            <div class="form-group">
                                <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="last_name" id="last_name" placeholder="Your Last Name"/>
                            </div>

                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" id="email" placeholder="Your Email"/>
                            </div>
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="pass" placeholder="Password"/>
                            </div>
                            <div class="form-group">
                                <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="password_confirmation" id="re_pass" placeholder="Repeat your password"/>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="is_term_accept" id="agree-term" class="agree-term" />
                                <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree with all statements in  <a href="#" class="term-service">Terms of service</a></label>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit" value="Register"/>
                            </div>
                        </form>
                        <span class="or-line">Or</span>
                        <ul class="socials">
                                <li><a href="#"><i class="display-flex-center fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="display-flex-center fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="display-flex-center fa fa-google-plus"></i></a></li>
                            </ul>
                    </div>
                    <div class="signup-image">
                        <figure><img src="images/signup-image.jpg" alt="sing up image"></figure>
                        <a href="#" class="signup-image-link">I am already member</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Sing in  Form -->
        <section class="sign-in">
            <div class="container-login-signup modal fade" id="signin" role="dialog">
		
          <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="images/signin-image.jpg" alt="sing up image"></figure>
                        <a href="#" class="signup-image-link">Create an account</a>
                    </div>

                    <div class="signin-form">
                        <h2 class="form-title">Sign In</h2>
                        <form method="POST" class="register-form" id="login-form" action="{{route('frontend.auth.login')}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label for="your_name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="email" name="email" id="your_name" placeholder="Your Email"/>
                            </div>
                            <div class="form-group">
                                <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password" id="your_pass" placeholder="Password"/>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="remember" id="remember-me" class="agree-term" />
                                <label for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signin" id="signin" class="form-submit" value="Log in"/>
                            </div>
                        </form>
                        <div class="social-login">
                            <span class="social-label">Or login with</span>
                            <ul class="socials">
                                <li><a href="#"><i class="display-flex-center fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="display-flex-center fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="display-flex-center fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        