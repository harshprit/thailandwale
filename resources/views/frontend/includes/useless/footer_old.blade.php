<section id="footer" class="ftr-heading-w ftr-heading-mgn-2">
        
		<div id="footer-top" class="banner-padding ftr-top-grey">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 footer-widget ftr-about ftr-our-company">
						<h3 class="footer-heading">OUR COMPANY</h3>
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
						<ul class="social-links list-inline list-unstyled">
							<li><a href="#"><span><i class="fa fa-facebook"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-twitter"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-google-plus"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-pinterest-p"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-instagram"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-linkedin"></i></span></a></li>
							<li><a href="#"><span><i class="fa fa-youtube-play"></i></span></a></li>
						</ul>
					</div><!-- end columns -->
					
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 footer-widget ftr-in-touch">
						<h3 class="footer-heading">Get in Touch</h3>
						<form>
							<div class="row">
								<div class="col-sm-6 col-md-5 col-lg-5 slide-right-vis">
								
									<div class="form-group">
										 <input type="text" class="form-control" placeholder="Name"  required/>
									</div>
	
									<div class="form-group">
										 <input type="text" class="form-control" placeholder="Email"  required/>
									</div>
									
									<div class="form-group">
										 <input type="text" class="form-control" placeholder="Subject"  required/>
									</div>
									
								</div><!-- end columns -->
	
								<div class="col-sm-6 col-md-7 col-lg-7 slide-left-vis">
									<div class="form-group">
										<textarea class="form-control" placeholder="Your Message"></textarea>
									</div>
								</div><!-- end columns -->
								
								<div class="col-sm-12 text-center">
									<button class="btn btn-orange">Send</button>
								</div><!-- end butn -->
							</div><!-- end row -->
						</form>
					</div><!-- end columns -->
					
				</div><!-- end row -->
			</div><!-- end container -->
		</div><!-- end footer-top -->

		<div id="footer-bottom" class="ftr-bot-black">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="copyright">
					  
					</div><!-- end columns -->
					
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="terms">
						<ul class="list-unstyled list-inline">
							<li><a href="#">Terms & Condition</a></li>
							<li><a href="#">Privacy Policy</a></li>
						</ul>
					</div><!-- end columns -->
				</div><!-- end row -->
			</div><!-- end container -->
		</div><!-- end footer-bottom -->
		
	</section><!-- end footer -->
	
	
	<!-- Page Scripts Starts -->
	<script src="{{asset('assets/home/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery.colorpanel.js')}}"></script>
	<script src="{{asset('assets/home/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery.flexslider.js')}}"></script>
	<script src="{{asset('assets/home/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-navigation.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-flex.js')}}"></script>
	<script src="{{asset('assets/home/js/custom-date-picker.js')}}"></script>
	<script src="{{asset('assets/home/js/jquery-ui.js')}}"></script> 
	<script src="{{asset('assets/home/js/jquery.immybox.min.js')}}"></script>
	<script src="{{asset('assets/home/js/select2.full.js')}}"></script>
	
	<script type="text/javascript" src="{{asset('assets/home/js/moment.min.js')}}"></script> 
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script> 
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
	<script type='text/javascript'>

$(function() {

 // simple example
 $('#input1').immybox({
   choices: [
	 {text: 'Alabama', value: 'AL'},
	 {text: 'Alaska', value: 'AK'},
	 {text: 'Arizona', value: 'AZ'},
	 {text: 'Arkansas', value: 'AR'},
	 {text: 'California', value: 'CA'},
	 {text: 'Colorado', value: 'CO'},
	 {text: 'Connecticut', value: 'CT'},
	 {text: 'Delaware', value: 'DE'},
	 {text: 'Florida', value: 'FL'},
	 {text: 'Georgia', value: 'GA'},
	 {text: 'Hawaii', value: 'HI'},
	 {text: 'Idaho', value: 'ID'},
	 {text: 'Illinois', value: 'IL'},
	 {text: 'Indiana', value: 'IN'},
	 {text: 'Iowa', value: 'IA'},
	 {text: 'Kansas', value: 'KS'},
	 {text: 'Kentucky', value: 'KY'},
	 {text: 'Louisiana', value: 'LA'},
	 {text: 'Maine', value: 'ME'},
	 {text: 'Maryland', value: 'MD'},
	 {text: 'Massachusetts', value: 'MA'},
	 {text: 'Michigan', value: 'MI'},
	 {text: 'Minnesota', value: 'MN'},
	 {text: 'Mississippi', value: 'MS'},
	 {text: 'Missouri', value: 'MO'},
	 {text: 'Montana', value: 'MT'},
	 {text: 'Nebraska', value: 'NE'},
	 {text: 'Nevada', value: 'NV'},
	 {text: 'New Hampshire', value: 'NH'},
	 {text: 'New Jersey', value: 'NJ'},
	 {text: 'New Mexico', value: 'NM'},
	 {text: 'New York', value: 'NY'},
	 {text: 'North Carolina', value: 'NC'},
	 {text: 'North Dakota', value: 'ND'},
	 {text: 'Ohio', value: 'OH'},
	 {text: 'Oklahoma', value: 'OK'},
	 {text: 'Oregon', value: 'OR'},
	 {text: 'Pennsylvania', value: 'PA'},
	 {text: 'Rhode Island', value: 'RI'},
	 {text: 'South Carolina', value: 'SC'},
	 {text: 'South Dakota', value: 'SD'},
	 {text: 'Tennessee', value: 'TN'},
	 {text: 'Texas', value: 'TX'},
	 {text: 'Utah', value: 'UT'},
	 {text: 'Vermont', value: 'VT'},
	 {text: 'Virginia', value: 'VA'},
	 {text: 'Washington', value: 'WA'},
	 {text: 'West Virginia', value: 'WV'},
	 {text: 'Wisconsin', value: 'WI'},
	 {text: 'Wyoming', value: 'WY'}
   ],
   defaultSelectedValue: 'LA'
 });

});
</script>
<script type='text/javascript'>                          
      $(function(){

        // simple example
        $('#input_hotel').immybox({
          choices: [
              <?php if(isset($cities)) {foreach($cities as $city) {?>
            {text: '<?php echo $city->CityName.','.$city->CityId;?>', value: '<?php echo $city->CityId;?>'},
              <?php }} else if(isset($airport)) { foreach($airport as $air) {?>
				{text: "<?php echo $air['CityName'].','.$air['AirportCode'];?>", value: '<?php echo $air['AirportCode'];?>'},
				<?php }}?>
          ],
          defaultSelectedValue: 'LA'
        });

	

	  });
	  $(function(){
		$('.input3').immybox({
          choices: [
              <?php if(isset($airport)) { foreach($airport as $air) { 
				    ?>
				{text: "<?php echo $air['CityName'].','.$air['AirportCode'];?>", value: '<?php echo $air['AirportCode'];?>'},
				<?php }}?>
          ],
          defaultSelectedValue: 'LA'
        });  
	  });
    </script>
	
<!-- //Calendar --> 
<!-- Page Scripts Ends --> 
<script type="text/javascript">
    //      $(function () { 
    // var $popover = $('.popover-markup>.trigger').popover({
    //   html: true,
    //   placement: 'bottom',
    //   content: function () {
          
    //     return $(this).parent().find('.content').html();
    //   }
    // });

    // open popover & inital value in form
    // var passengers = [1,0,0];
    // $('.popover-markup>.trigger').click(function (e) {
    //     e.stopPropagation();
    //     $(".popover-content input").each(function(i) {
    //         $(this).val(passengers[i]);
    //     });
    // });
    // close popover
    // $(document).click(function (e) {
    //     if ($(e.target).is('.demise')) {        
    //         $('.popover-markup>.trigger').popover('hide');
    //     }
    // });
// store form value when popover closed
//  $popover.on('hide.bs.popover', function () {
//     $(".popover-content input").each(function(i) {
//         passengers[i] = $(this).val();
//     });
//   });
    // spinner(+-btn to change value) & total to parent input 
//     $(document).on('click', '.number-spinner a', function () {
//         var btn = $(this),
//         input = btn.closest('.number-spinner').find('input'),
//         total = $('#passengers').val(),
//         oldValue = input.val().trim();
        
//     if(btn.attr('data-dir') == 'up' && btn.attr('id')=='adult_up') {
//       if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#adult').val(total);
        
      
//       }
//     }else if(btn.attr('data-dir') == 'up' && btn.attr('id')=='child_up')
//     {
//         if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#child_one').val(total);
        
      
//       }
//     } else if(btn.attr('data-dir') == 'up' && btn.attr('id')=='inf_up')
//     {
//         if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#infants_one').val(total);
      
//       }
//     }
    
//     else {
//       if (oldValue > input.attr('min')) {
//         oldValue--;
//         total--;
      
//       }
//     }
//     $('#passengers').val(total);
    
//     input.val(oldValue);
//   });

//   $(".popover-markup>.trigger").popover('show');
// });
</script>


<!-- modal--> 
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 


<!-- modal--> 
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 
<script type="text/javascript">$(document).ready( function() {
    $('#myCarousel').carousel({
        interval:   4000
    });
    
    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(this).parent().addClass('active');        
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');    
            }
        }
        clickEvent = false;
    });
});</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 
<!--========= modal=======-->
<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-chevron-up glyphicon-chevron-down');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<script>
$(document).ready(function() {
$(".greenOutlineBtn").click(function () {
    $(".greenOutlineBtn").removeClass("active");
    $(this).addClass("active");   
});
});
</script>
                             
<script>
        window.oncroll = function() {myFunction()};
            
            function myFunction() {
                alert("hello");
                
                if (window.pageYOffset > 50||document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    document.getElementById("sidebar-hotel").className = "test";
                } else {
                    document.getElementById("sidebar-hotel").className = "";
                }
            }
</script>
@if(isset($show_login) && $show_login)
<script type="text/javascript">
$(window).load(function() {
    $('#signin').modal('show');
});
</script>
@endif


@if(isset($show_signup) && $show_signup)
<script type="text/javascript">
$(window).load(function() {
    $('#signup').modal('show');
});
</script>
@endif