<!-- Customize footer -->
      <script src="{{asset('assets/home/customize/js/jquery.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/moment.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/bootstrap.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/owl-carousel.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/blur-area.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/icheck.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/magnific-popup.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/ion-range-slider.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/sticky-kit.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/smooth-scroll.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/fotorama.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/bs-datepicker.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/typeahead.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/quantity-selector.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/countdown.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/window-scroll-action.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/custom.js')}}"></script>
<!-- End: Customize footer -->