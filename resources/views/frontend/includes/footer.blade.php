
<footer class="pb-3 main-footer footerGradient">
    <div class="container">
    
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">
                <!--Big Column-->
                <div class="big-column col-md-12 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <!--Footer Column-->
                        <div class="footer-column col-md-7 col-sm-7 col-xs-12">
                            <div class="footer-widget logo-widget">
                                <div class="widget-content">
                                    <div class="text">
                                        <h4 class="footerH">Company Info</h4>
                                        <p class="text-justify">Established in 2016, Thailandwale is a young and vibrant collective of travel agents 
                                            who are focussed on providing memorable trips to Thailand. We work exclusively with a 
                                            customer-focussed mind set, which means that your satisfaction and happiness is our priority. 
                                            We provide end-to-end services to ensure that each aspect of your trip is taken care of. 
                                            From handling flight bookings and hotel reservations to planning your itinerary and offering 
                                            Indian guides in Thailand, we do it all!</p>
                                    </div>
                                    <div class="text">
                                        <h4 class="footerH">Product Offerings</h4>
                                        <p class="text-justify">We offer extensive range of services such as Flights, Hotels, Holidays, 
                                            Transfers & Activities specialised in Thailand with amazing offers and incentives.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-5 col-sm-5 col-xs-12">
                            <div class="footer-widget links-widget">
                                <div class="text">
                                    <h4 class="footerH">Feedback</h4>
                                    <p class="text-justify">We are a customer focused company and always loves to hear from you. 
                                        For feedback fillout the form by clicking Here.</p>
                                </div>
                            </div>

                        </div>
                        <div class="footer-column col-md-3 col-sm-4 col-xs-12">
                            <div class="footer-widget links-widget">

                            </div>

                        </div>

                    </div>
                </div>
                <!--Big Column-->
            </div>
            <div class="row clearfix">
                <div class="container text-center">
                    <div class="copyright">Copyright © 2019 Thailandwale. &nbsp; All Rights Reserved</div>
                </div>
            </div>
        </div>
        
    </div>

    <!--Footer Bottom-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="{{asset('assets/home/chosen_select/chosen.jquery.min.js')}}"></script>
     <script src="{{asset('assets/home/js/owl-carousel.js')}}"></script>
    <script src="{{asset('assets/home/js/particles.js')}}"></script>
    <script src="{{asset('assets/home/js/main_1.js')}}"></script>
   
    <script>
$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
$(document).ready(function () {
    $('.landingCardSlider').slick({
          dots: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          responsive: [
                            {
                              breakpoint: 1200,
                              settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1
                              }
                            },
                            {
                              breakpoint: 1008,
                              settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                              }
                            },
                            {
                              breakpoint: 500,
                              settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                              }
                            },
                            

                          ]
        });

    $('#owl-carousel').owlCarousel({
        loop: true,
        margin: 50,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            768: {
                items: 3,
                nav: false
            },
            1000: {
                items: 3,
                nav: false,
                loop: true
            }
        }
    });
});
    </script>

</footer> 