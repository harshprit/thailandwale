  <!-- Customize package -->
        
        <link rel="stylesheet" href="{{asset('assets/home/customize/css/font-awesome.css')}}"/>
        <link rel="stylesheet" href="{{asset('assets/home/customize/css/lineicons.css')}}"/>
        <link rel="stylesheet" href="{{asset('assets/home/customize/css/weather-icons.css')}}"/>
        <link rel="stylesheet" href="{{asset('assets/home/customize/css/bootstrap.css')}}"/>
        <link rel="stylesheet" href="{{asset('assets/home/customize/css/styles.css')}}"/>
<!-- End: Customize Package -->