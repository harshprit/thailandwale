<section class="sticky">
<!-- <div class="popup">This is SPATAN</div> -->
<div class="container-fluid">
 <div class="row">
    <div class="col-md-12 col-xs-12">
     <div class="col-md-4 col-xs-12" style="border-right: 1px solid #000;">
      <div class="row">
        <div class="col-md-3 col-xs-3">
          <span id="dep_airline_logo"><img src="images/com.bets.airindia.ui.jpg" class="img-responsive" style="    width: 55%;"></span>
          <span><p id="dep_flightdetails">Spicejet  SG-161 </p></span>
        </div>
         <div class="col-md-3 col-xs-3">
          <h4><strong id="dep_time">15:50</strong></h4>
          <h3 id="dep_origin"> DEL</h3>
        </div>
       
         <div class="col-md-3 col-xs-3">
            <h4><strong id="dep_arrtime">16:06</strong></h4>
          <h3 id="dep_destination"> BOM</h3>
        </div>
         <div class="col-md-3 col-xs-3">
            <h4><i class="fas fa-rupee-sign"></i><span id="dep_fare"></span></h4>
         
        </div>
      </div>

     </div>
<div class="border-right"> </div>
     <div class="col-md-4 col-xs-12" style="border-right: 1px solid #000;">
        <div class="row">
        <div class="col-md-3 col-xs-3">
          <span id="ret_airline_logo"><img src="images/com.bets.airindia.ui.jpg" class="img-responsive" style="    width: 55%;"></span>
          <span><p id="ret_flightdetails">Spicejet  SG-161 </p></span>
        </div>
         <div class="col-md-3 col-xs-3">
          <h4><strong id="ret_time">15:50</strong></h4>
          <h3 id="ret_origin"> DEL</h3>
        </div>
       
         <div class="col-md-3 col-xs-3">
            <h4><strong id="ret_arrtime">16:06</strong></h4>
          <h3 id="ret_destination"> BOM</h3>
        </div>
         <div class="col-md-3 col-xs-3">
            <h4><i class="fas fa-rupee-sign"></i><span id="ret_fare"></span></h4>
         
        </div>
      </div>
    </div>
     <div class="col-md-4 col-xs-12">
      <div class="row">
        <div class="col-md-5">
          <h6>Round Trip Discount:</h6>
          <h5><i class="fas fa-rupee-sign"></i><span id="total_discount"></span></h5>
        </div>
         <div class="col-md-4">
          <h6>Total Amount</h6>
          <h2 class="total-h3"><i class="fas fa-rupee-sign"></i><span id="total_fare"></span></h2>
        </div>
         <div class="col-md-3">
		 <form method="post" action="{{route('frontend.flight_passenger_details_return')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
		    <input type="hidden" name="TraceId" value="{{$flights->Response->TraceId}}">
		    <input type="hidden" name="ob_ResultIndex" id="ob">
		    <input type="hidden" name="ib_ResultIndex" id="ib">
            <button type="submit" class="btn btn-danger">Book</button>
		 </form>
        </div>
      </div>
    </div>
  </div>
  </div>

</div>
  </section>
  <script>
  function getObIndex(val,dep_row)
  {
     // alert(dep_row);
	  $('#ob').val(val);
	  
	   var dep_image = $('#dep'+dep_row+' img[name=dep_img]').attr('src');
        var dep_airline =  $('#dep'+dep_row+' p[name=dep_airline]').text();
        var dep_flightno =  $('#dep'+dep_row+' p[name=dep_flightno]').text();
        var dep_datetime =  $('#dep'+dep_row+' a[name=dep_datetime]').text();
        var dep_arrtime =  $('#dep'+dep_row+' a[name=dep_arrtime]').text();
        var dep_fare =  $('#dep'+dep_row+' span[name=dep_fare]').text();
        var dep_org = $('#dep'+dep_row+' span[name=dep_origin]').text();
        var dep_dest = $('#dep'+dep_row+' span[name=dep_destination]').text();
             dep_discount = $('#dep'+dep_row+' span[name=dep_discount]').text();
    
        $("#dep_airline_logo").html("<img src='"+dep_image+"' class='img-responsive' style='width: 55%'>");
        $("#dep_flightdetails").html(dep_airline+" "+dep_flightno);
        $("#dep_time").html(dep_datetime);
        $("#dep_arrtime").html(dep_arrtime);
        $("#dep_fare").html(dep_fare);
        $("#dep_origin").html(dep_org);
        $("#dep_destination").html(dep_dest);
     var ret_id =  $("input[name=ret_flight]:checked").parents().eq(6).attr("id"); 
    
     var ret_discount=$('#'+ret_id+" span[name=ret_discount]").text();
     var ret_fare = $("#"+ret_id+" span[name=ret_fare]").text(); 
     //alert(ret_fare);
     
    var total_discount = parseInt(dep_discount.replace(/,/g,''))+parseInt(ret_discount.replace(/,/g,''));
   $("#total_discount").html((total_discount).toLocaleString('en'));
   var total_fare = parseInt(dep_fare.replace(/,/g,''))+parseInt(ret_fare.replace(/,/g,''));
   $("#total_fare").html((total_fare).toLocaleString('en'));
  }
  
  function getIbIndex(val,ret_row)
  {
	  $('#ib').val(val);
	  
	   var ret_image = $('#ret'+ret_row+' img[name=ret_img]').attr('src');
        var ret_airline =  $('#ret'+ret_row+' p[name=ret_airline]').text();
        var ret_flightno =  $('#ret'+ret_row+' p[name=ret_flightno]').text();
        var ret_datetime =  $('#ret'+ret_row+' a[name=ret_datetime]').text();
        var ret_arrtime =  $('#ret'+ret_row+' a[name=ret_arrtime]').text();
        var ret_fare =  $('#ret'+ret_row+' span[name=ret_fare]').text();
        var ret_org = $('#ret'+ret_row+' span[name=ret_origin]').text();
        var ret_dest = $('#ret'+ret_row+' span[name=ret_destination]').text();
             ret_discount = $('#ret'+ret_row+' span[name=ret_discount]').text();
    
        $("#ret_airline_logo").html("<img src='"+ret_image+"' class='img-responsive' style='width: 55%'>");
        $("#ret_flightdetails").html(ret_airline+" "+ret_flightno);
        $("#ret_time").html(ret_datetime);
        $("#ret_arrtime").html(ret_arrtime);
        $("#ret_fare").html(ret_fare);
        $("#ret_origin").html(ret_org);
        $("#ret_destination").html(ret_dest);
     var dep_id =  $("input[name=dep_flight]:checked").parents().eq(6).attr("id"); 
    
     var dep_discount=$('#'+dep_id+" span[name=dep_discount]").text();
     var dep_fare = $("#"+dep_id+" span[name=dep_fare]").text(); 
     //alert(ret_fare);
     
    var total_discount = parseInt(dep_discount.replace(/,/g,''))+parseInt(ret_discount.replace(/,/g,''));
   $("#total_discount").html((total_discount).toLocaleString('en'));
   var total_fare = parseInt(dep_fare.replace(/,/g,''))+parseInt(ret_fare.replace(/,/g,''));
   $("#total_fare").html((total_fare).toLocaleString('en'));
  }
  
  </script>