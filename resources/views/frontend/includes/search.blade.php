<section class="pt-5">
    <div class="container searchContainer mx-auto" if="searchContainer">
        <!-- Search Tab list Start -->
        <ul class="nav nav-tabs d-flex justify-content-center" id="myTab" role="tablist">
            <li class="nav-item pl-3">
                <a class="nav-link active" id="flight-tab" data-toggle="tab" href="#flight" role="tab" aria-controls="flight" aria-selected="true">
                    <span>
                        <img src="https://img.icons8.com/bubbles/50/000000/airport.png">
                    </span> <br>
                    <span>
                        Flight
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="hotel-tab" data-toggle="tab" href="#hotel" role="tab" aria-controls="hotel" aria-selected="false">
                    <span>
                        <img src="https://img.icons8.com/bubbles/50/000000/city.png">
                    </span> <br>
                    <span>
                        Hotel
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="holiday-tab" data-toggle="tab" href="#holiday" role="tab" aria-controls="holiday" aria-selected="false">
                    <span>
                        <img src="https://img.icons8.com/bubbles/50/000000/beach.png">
                    </span> <br>
                    <span>
                        Holiday
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tohfa-tab" data-toggle="tab" href="#tohfa" role="tab" aria-controls="tohfa" aria-selected="false">
                    <span>
                        <img src="https://img.icons8.com/bubbles/50/000000/gift.png">
                    </span> <br>
                    <span>
                        Tohfa
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="cabs-tab" data-toggle="tab" href="#cabs" role="tab" aria-controls="cabs" aria-selected="false">
                    <span>
                        <img src="https://img.icons8.com/bubbles/50/000000/fiat-500.png">
                    </span> <br>
                    <span>
                        Cabs
                    </span>
                </a>
            </li>
            <li class="nav-item  dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span>
                        <img class="rotate90" src="https://img.icons8.com/bubbles/50/000000/menu-2.png">
                    </span> <br>
                    <span>
                        More
                    </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item text-left" href="#">Visa</a>
                    <a class="dropdown-item text-left" href="#">Cabs</a>
                    <a class="dropdown-item text-left" href="#">Entertainment</a>
                    <a class="dropdown-item text-left" href="#">Destination Wedding</a>
                </div>
            </li>
        </ul>
        <!-- ======================== Search Tab list Ends ========================-->
        <!-- Search  Container  Start -->

    </div>
    <div class="tab-content container searchSubContainer" id="myTabContent">
        <!-- Flight Search Container Starts -->
        <div class="tab-pane fade show active " id="flight" role="tabpanel" aria-labelledby="home-tab">
            <form id="" method="post" action="{{route('frontend.search_flight')}}" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="travel_class" id="travel_class">
                <div class="row">
                    <div class="col-md-5">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item pl-3">
                                <a class="nav-link active bgTransparent" id="onewaySearch-tab" data-toggle="tab" href="#onewaySearch" role="tab" aria-controls="onewaySearch" aria-selected="true">
                                    <!-- <input type="radio" name="onewaySearch" id="oneWay"> -->
                                    <input type="hidden" name="journey_type" value="1">
                                    One Way
                                </a>
                            </li>
                            <li class="nav-item pl-3">
                                <a class="nav-link bgTransparent" id="roundtrip-tab" data-toggle="tab" href="#roundtrip" role="tab" aria-controls="roundtrip" aria-selected="true">
                                    <!-- <input type="radio" name="onewaySearch" id="oneWay"> -->
                                    Round Trip
                                </a>
                            </li>
                            <li class="nav-item pl-3">
                                <a class="nav-link bgTransparent" id="muticity-tab" data-toggle="tab" href="#muticity" role="tab" aria-controls="roundtrip" aria-selected="true">
                                    <!-- <input type="radio" name="onewaySearch" id="oneWay"> -->
                                    Multi City
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-7">
                        <small class="float-right">Book Domestic and International flights </small>
                    </div>
                </div>
                <!-- One way Div Container -->
                <div class="tab-content container pb-4" id="myTabSubContent">
                    <div class="tab-pane fade show active" id="onewaySearch" role="tabpanel" aria-labelledby="onewaySearch-tab">
                        <div class="container">

                            <div class="row fsw_inner">
                                <div class="form-group mb-0 col-md-3 pl-2 onhover1">
                                    <label class="mt-2 mb-0" for="fromCity">From</label>
                                    <select class="form-control chosen-select" id="input2" name="from[]" required="required">
                                        <option value="">Source</option>
                                        <?php
                                        if (isset($airport)) {
                                            foreach ($airport as $city) {
                                                ?>
                                                <option value="<?php echo $city['CityCode']; ?>"><?php echo $city['CityName'] . ',' . $city['CityCode']; ?></option>

                                            <?php }
                                    } ?>

                                    </select>
                                </div>

                                <div class="form-group mb-0 col-md-3 pl-4 onhover1">
                                    <label class="mt-2 mb-0" for="toCity">To</label>
                                    <select class="chosen-select form-control" name="to[]" id="input3" required="required">
                                        <option value="">Destination</option>
                                        <?php
                                        if (isset($airport)) {
                                            foreach ($airport as $city) {
                                                ?>
                                                <option value="<?php echo $city['CityCode']; ?>"><?php echo $city['CityName'] . ',' . $city['CityCode']; ?></option>

                                            <?php }
                                    }
                                    ?>
                                    </select>
                                </div>
                                <div class="form-group mb-0 col-md-2 onhover1">
                                    <label class="mt-2 mb-0 " for="datepicker">Departure</label>
                                    <input type="text" name="journey_date[]" class="form-control date" autocomplete="off" id="datepicker" placeholder="Date">
                                </div>
                                <!-- <div class="form-group mb-0 col-md-3 onhover1 travellers">
                                    <label class="mt-2 mb-0 " for="travellers">Travellers &amp; Class</label>
                                  <input type="text" class="form-control " id="travellers" placeholder="Select">
                                  <span class="font20"><span class="bold30">9</span>&nbsp;Travellers</span>
                                  <small id="classDesc" class="form-text text-muted mb-1">Bussiness</small>
                                </div> -->
                                <div class="form-group mb-0 col-md-3 onhover1 travellers">
                                    <div class="popover-markup">
                                        <div class="trigger form-group form-group-lg form-group-icon-left">
                                            <label style="text-align: left;">Passenger</label>
                                            <input type="hidden" name="passenger_count" id="passenger_count" value="1" autocomplete="off">
                                            <input type="text" name="passengers_one" id="passengers_one" value="1|Economy" autocomplete="off" class="form-control">
                                        </div>
                                        <div id="passengerCount" style="display:none">
                                            <div class="triangle"></div>
                                            <div class="content">
                                                <!-- adult row -->
                                                <div class="row">
                                                    <div class="form-group d-flex">
                                                        <div class="col-md-3 col-3">
                                                            <label class="control-label"><strong>Adults</strong><br>
                                                                <i> (+12 yrs)</i></label></div>
                                                        <div class="col-md-5 col-5">
                                                            <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn minus" data-dir="dwn" id="adult_dn"><i class="fas fa-minus"></i></a> </span>
                                                                <input type="text" name="adult_one" id="adult_one" class="form-control text-center" value="1">
                                                                <span class="input-group-btn"> <a class="btn  plus" id="adult_up"><i class="fas fa-plus"></i></a> </span> </div>
                                                        </div>
                                                        <div class="col-md-4 col-4"><button class="flight-class active" type="button" value="2"> Economy </button></div>
                                                    </div>
                                                </div>
                                                <!-- adult row end -->
                                                <!-- child row -->
                                                <div class="row">
                                                    <div class="form-group d-flex">
                                                        <div class="col-md-3 col-3">
                                                            <label class="control-label"><strong>Children</strong><br>
                                                                <i> (+12 yrs)</i></label>
                                                        </div>
                                                        <div class="col-md-5 col-5">
                                                            <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn minus" data-dir="dwn" id="child_dn"><i class="fas fa-minus"></i></a> </span>
                                                                <input type="text" name="child_one" id="child_one" class="form-control text-center" value="0">
                                                                <span class="input-group-btn"> <a class="btn  plus" id="child_up"><i class="fas fa-plus"></i></a> </span> </div>
                                                        </div>
                                                        <div class="col-md-4 col-4"><button class="flight-class" type="button" value="3">Premium Economy </button></div>
                                                    </div>
                                                </div>
                                                <!-- child row end -->
                                                <!-- infant row -->
                                                <div class="row">
                                                    <div class="form-group d-flex">
                                                        <div class="col-md-3 col-3">
                                                            <label class="control-label"><strong>Infants</strong><br>
                                                                <i> (0-2 yrs)</i></label>
                                                        </div>
                                                        <div class="col-md-5 col-5">
                                                            <div class="input-group number-spinner1"> <span class="input-group-btn"> <a class="btn minus" id="inf_dn" data-dir="dwn"><i class="fas fa-minus"></i></a> </span>
                                                                <input type="text" name="infants_one" id="infants_one" class="form-control text-center" value="0">
                                                                <span class="input-group-btn"> <a class="btn  plus" id="inf_up"><i class="fas fa-plus"></i></a> </span> </div>
                                                        </div>
                                                        <div class="col-md-4 col-4"><button class="flight-class" type="button" value="4"> Business </button></div>
                                                    </div>
                                                </div>
                                                <!-- infant row end -->
                                                <div class="row justify-content-center">
                                                    <div class="form-group donebtn">
                                                        <button class="btn btn-default btn-block demise" type="button" id="done">Done</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <span class="font20"><span class="bold30">9</span>&nbsp;Travellers</span>
                                    <small id="classDesc" class="form-text text-muted mb-1">Bussiness</small> -->
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="roundtrip" role="tabpanel" aria-labelledby="roundtrip-tab">
                        <div class="container">

                            <div class="row fsw_inner">
                                <div class="form-group mb-0 col-md-3 pl-2 onhover1">
                                    <label class="mt-2 mb-0" for="fromCity">From</label>
                                    <input type="text" class="form-control" id="fromCity" aria-describedby="fromCity" placeholder="Search...">
                                    <small id="fromCityDesc" class="form-text text-muted mb-1">From City Name.</small>
                                </div>
                                <div class="form-group mb-0 col-md-3 pl-4 onhover1">
                                    <label class="mt-2 mb-0" for="toCity">To</label>
                                    <input type="text" class="form-control" id="automplete-2" placeholder="Search...">
                                    <small id="toCityDesc" class="form-text text-muted mb-1">To City Name.</small>
                                </div>
                                <div class="form-group mb-0 col-md-2 onhover1 departure">
                                    <label class="mt-2 mb-0 " for="datepicker">Departure</label>
                                    <input type="text" class="form-control date" autocomplete="off" id="datepicker2" placeholder="Date">
                                </div>
                                <div class="form-group mb-0 col-md-2 onhover1 return">
                                    <label class="mt-2 mb-0 " for="datepicker">Return</label>
                                    <input type="text" class="form-control date" autocomplete="off" id="datepicker3" placeholder="Date">
                                </div>
                                <!-- <div class="form-group mb-0 col-md-3 onhover1 travellers">
                                    <label class="mt-2 mb-0 " for="travellers">Travellers &amp; Class</label>
                                  <input type="text" class="form-control " id="travellers" placeholder="Select">
                                  <span class="font20"><span class="bold30">9</span>&nbsp;Travellers</span>
                                  <small id="classDesc" class="form-text text-muted mb-1">Bussiness</small>
                                </div> -->
                                <div class="form-group mb-0 col-md-3 onhover1 travellers">
                                    <div class="popover-markup">
                                        <div class="trigger form-group form-group-lg form-group-icon-left">
                                            <label style="text-align: left;">Passenger</label>
                                            <input type="hidden" name="passenger_count" id="passenger_count" value="1" autocomplete="off">
                                            <input type="text" name="passengers_one" id="passengers_one" value="1|Economy" autocomplete="off" class="form-control">
                                        </div>
                                        <div id="passengerCount" style="display:none">
                                            <div class="triangle"></div>
                                            <div class="content">
                                                <!-- adult row -->
                                                <div class="row">
                                                    <div class="form-group d-flex">
                                                        <div class="col-md-3 col-3">
                                                            <label class="control-label"><strong>Adults</strong><br>
                                                                <i> (+12 yrs)</i></label></div>
                                                        <div class="col-md-5 col-5">
                                                            <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn minus" data-dir="dwn" id="adult_dn"><i class="fas fa-minus"></i></a> </span>
                                                                <input type="text" name="adult_one" id="adult_one" class="form-control text-center" value="1">
                                                                <span class="input-group-btn"> <a class="btn  plus" id="adult_up"><i class="fas fa-plus"></i></a> </span> </div>
                                                        </div>
                                                        <div class="col-md-4 col-4"><button class="flight-class active" type="button" value="2"> Economy </button></div>
                                                    </div>
                                                </div>
                                                <!-- adult row end -->
                                                <!-- child row -->
                                                <div class="row">
                                                    <div class="form-group d-flex">
                                                        <div class="col-md-3 col-3">
                                                            <label class="control-label"><strong>Children</strong><br>
                                                                <i> (+12 yrs)</i></label>
                                                        </div>
                                                        <div class="col-md-5 col-5">
                                                            <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn minus" data-dir="dwn" id="child_dn"><i class="fas fa-minus"></i></a> </span>
                                                                <input type="text" name="child_one" id="child_one" class="form-control text-center" value="0">
                                                                <span class="input-group-btn"> <a class="btn  plus" id="child_up"><i class="fas fa-plus"></i></a> </span> </div>
                                                        </div>
                                                        <div class="col-md-4 col-4"><button class="flight-class" type="button" value="3">Premium Economy </button></div>
                                                    </div>
                                                </div>
                                                <!-- child row end -->
                                                <!-- infant row -->
                                                <div class="row">
                                                    <div class="form-group d-flex">
                                                        <div class="col-md-3 col-3">
                                                            <label class="control-label"><strong>Infants</strong><br>
                                                                <i> (0-2 yrs)</i></label>
                                                        </div>
                                                        <div class="col-md-5 col-5">
                                                            <div class="input-group number-spinner1"> <span class="input-group-btn"> <a class="btn minus" id="inf_dn" data-dir="dwn"><i class="fas fa-minus"></i></a> </span>
                                                                <input type="text" name="infants_one" id="infants_one" class="form-control text-center" value="0">
                                                                <span class="input-group-btn"> <a class="btn  plus" id="inf_up"><i class="fas fa-plus"></i></a> </span> </div>
                                                        </div>
                                                        <div class="col-md-4 col-4"><button class="flight-class" type="button" value="4"> Business </button></div>
                                                    </div>
                                                </div>
                                                <!-- infant row end -->
                                                <div class="row justify-content-center">
                                                    <div class="form-group donebtn">
                                                        <button class="btn btn-default btn-block demise" type="button" id="done">Done</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <span class="font20"><span class="bold30">9</span>&nbsp;Travellers</span>
                                    <small id="classDesc" class="form-text text-muted mb-1">Bussiness</small> -->
                                </div>


                            </div>

                        </div>

                    </div>
                    <div class="tab-pane fade" id="muticity" role="tabpanel" aria-labelledby="muticity-tab">Multicity</div>
                </div>
                <div class="row justify-content-center">

                    <button type="submit" class="bold24 latoBlack searchBtn text-center">Search</button>
                </div>
            </form>
        </div>
        <!-- ======================== Flight Search Container Ends ========================-->
        <!-- Hotel Search Container Starts -->
        <div class="tab-pane fade" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">Hotel tab</div>
        <!-- ======================== Hotel Search Container Ends ========================-->
        <div class="tab-pane fade" id="holiday" role="tabpanel" aria-labelledby="holiday-tab">Holiday Tab</div>
        <div class="tab-pane fade" id="tohfa" role="tabpanel" aria-labelledby="tohfa-tab">Tohfa Tab</div>
        <div class="tab-pane fade" id="cabs" role="tabpanel" aria-labelledby="cabs-tab">Cab Tab</div>
    </div>
    <!-- ========================  Search Container  Ends ========================-->
</section>