<div class="sidenav-content">
            <div id="mySidenav" class="sidenav" >
                <!--<h2 id="web-name"><span><i class="fa fa-plane"></i></span>thailandwale</h2>-->

                <div id="main-menu">
                	<div class="closebtn">
                        <button class="btn btn-default" id="closebtn">&times;</button>
                    </div><!-- end close-btn -->
                    
                    <div class="list-group panel">
                    
                        <a href="#Home" class="list-group-item active" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-home link-icon"></i></span>Home</a>
                     
                        
                        <a href="{{route('frontend.index')}}" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-plane link-icon"></i></span>{{trans('navs.frontend.flights') }}</a>
                      
                        
                        <a href="#hotels-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-building link-icon"></i></span>Hotels</a>
                       
                        
                        <a href="#tours-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-globe link-icon"></i></span>Holidays</a>
                        
                        
                        <a href="#cruise-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-ship link-icon"></i></span>DESTINATIONS<span><i class="fa fa-chevron-down arrow"></i></span></a>
                        <div class="collapse sub-menu" id="cruise-links">
                            <a href="#" class="list-group-item">link 1</a>
                             <a href="#" class="list-group-item">link 1</a>
							  <a href="#" class="list-group-item">link 1</a> 
							  <a href="#" class="list-group-item">link 1</a>
							   <a href="#" class="list-group-item">link 1</a>
							    <a href="#" class="list-group-item">link 1</a>
								 <a href="#" class="list-group-item">link 1</a>
							  
                        </div><!-- end sub-menu -->
                        
                        <a href="#cars-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-car link-icon"></i></span>Hotdeals</a>
                       
                        
                        <a href="#features-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-puzzle-piece link-icon"></i></span>Visa</a>
                   
                        
                        <a href="#pages-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-clone link-icon"></i></span>Entertainment</a>
						<a href="#pages-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-clone link-icon"></i></span>Refer & earn</a>
						<a href="#pages-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-clone link-icon"></i></span>Trips</a>
						<a href="#pages-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-clone link-icon"></i></span>Cabs</a>
                      <!-- end sub-menu -->
                    </div><!-- end list-group -->
                </div><!-- end main-menu -->
            </div><!-- end mySidenav -->
        </div><!-- end sidenav-content -->