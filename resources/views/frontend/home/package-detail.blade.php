@include('frontend.includes.header')
@include('frontend.includes.customize-header')
@include('frontend.includes.nav1')

@if(Session::has('error'))
<div class=' alert alert-danger'>{{Session::get('error')}}</div>
@endif

<!-- Popup :  edit passenger details-->
@php
if(!isset($package_cost))
{
    $package_cost=0;
}

@endphp
@if(isset($package_detail))
<div id="package-edit" class="modal modal-content">
    <form action="{{route('frontend.single_package')}}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="package_id" value="{{$package_detail->id}}">
    <input type="hidden" name="package_price" value="{{$package_cost}}">
    <input type="hidden" name="package_price_off" value="{{$package_cost_off}}">
    
    
    <?php
                      $destination_str=$package_detail->destination_id;  
                      $destination_explode = explode("-",$destination_str);
                      $dn=array();
                      $dest_ar=array();
                      $dest_night=array();
                      //print_r($destination_str);
                      for($i=0;$i<count($destination_explode);$i++)
                      { 
                        $dn=explode(",",$destination_explode[$i]);
                        $dest_ar[$i]=$dn[0];
                        if(isset($dn[1])&& $dn[1]!=null)
                        $dest_night[$i]=$dn[1];
                      }
                    ?>
   
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Provide Details for Customized Package</h4>
    </div>
    <div class="modal-body">
        <div class="theme-search-area-form">
            <div class="row" data-gutter="20">
                <div class="col-md-6">
                    <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-border-light">
                        <div class="theme-search-area-section-inner">
                            <i class="theme-search-area-section-icon lin lin-location-pin"></i>
                            <input class="theme-search-area-section-input typeahead destination" type="text" id="origin" name="destination[]" placeholder="Serch your departure city" data-provide="typeahead">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light">
                        <div class="theme-search-area-section-inner">
                            <i class="theme-search-area-section-icon lin lin-calendar"></i>
                            <input class="theme-search-area-section-input datePickerEnd _mob-h" placeholder="Select Date" name="depart_date[]" value="" id="date" type="text">
                            <input class="theme-search-area-section-input _desk-h mobile-picker" placeholder="Select Date" value="" type="text">
                        </div>
                    </div>
                </div>
                                             
            </div>
            <input type="hidden" name="depart_date[]" id="depart_date" class="depart_date">
            <div class="row" data-gutter="20" id="test">
                <div class="col-md-12">
                    <div class="fullbacken">
                        <h5>Mention Number of Rooms &amp; Travellers</h5>
                        <div class="row kingpas" id="kingpas1">
                            <div class="col-md-2 col-sm-2">
                                <span class="fonw">Room 1:</span>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light quantity-selector" data-increment="Rooms">
                                    <label class="theme-search-area-section-label">Adult: (12+ Yrs)</label>
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-tag"></i>
                                        <input class="theme-search-area-section-input" value="1" type="text" name="adults[]">
                                        <div class="quantity-selector-box" id="HotelSearchRooms">
                                            <div class="quantity-selector-inner">
                                                <p class="quantity-selector-title">Adults</p>
                                                <ul class="quantity-selector-controls">
                                                    <li class="quantity-selector-decrement">
                                                        <a href="#">-</a>
                                                    </li>
                                                    <li class="quantity-selector-current">1</li>
                                                    <li class="quantity-selector-increment">
                                                        <a href="#">+</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3  col-sm-3">
                                <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light quantity-selector" data-increment="Rooms">
                                    <label class="theme-search-area-section-label">Child: (2-11 Yrs)</label>
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-tag"></i>
                                        <input class="theme-search-area-section-input" value="0" type="text" name="children[]">
                                        <div class="quantity-selector-box" id="HotelSearchRooms">
                                            <div class="quantity-selector-inner">
                                                <p class="quantity-selector-title">Children</p>
                                                <ul class="quantity-selector-controls">
                                                    <li class="quantity-selector-decrement">
                                                        <a href="#">-</a>
                                                    </li>
                                                    <li class="quantity-selector-current">1</li>
                                                    <li class="quantity-selector-increment">
                                                        <a href="#" class="child_age">+</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3  col-sm-3">
                                <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light quantity-selector" data-increment="Rooms">
                                    <label class="theme-search-area-section-label">Infant: (0-2 Yrs)</label>
                                    <div class="theme-search-area-section-inner">
                                        <i class="theme-search-area-section-icon lin lin-tag"></i>
                                        <input class="theme-search-area-section-input" value="0" type="text" name="infants[]">
                                        <div class="quantity-selector-box" id="HotelSearchRooms">
                                            <div class="quantity-selector-inner">
                                                <p class="quantity-selector-title">Rooms</p>
                                                <ul class="quantity-selector-controls">
                                                    <li class="quantity-selector-decrement">
                                                        <a href="#">-</a>
                                                    </li>
                                                    <li class="quantity-selector-current">0</li>
                                                    <li class="quantity-selector-increment">
                                                        <a href="#">+</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            <a href="#" class="addnew">Add New Room</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button  type="submit" name="edit_package"  class="rullybutton">Get a Call Back</button>
    </div>
    </form>
</div>
@endif

     <!-- #Popup :  edit passenger details-->
    <form id="package_form" method="post" action="{{route('frontend.process_package')}}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="theme-page-section theme-page-section-lg">
         <div class="container">
            <div class="row row-col-static row-col-mob-gap" id="sticky-parent" data-gutter="60">
               <div class="col-md-8 ">
                  <div class="theme-payment-page-sections">
                     <div class="fullimage">
                     @if(isset($package_detail))
                        <img src="{{url('storage/app/public/img/package').'/'.$package_detail->featured_image}}" class="img-responsive">
                     @else
                        <img src="{{asset('assets/home/customize/images/pack.jpg')}}" class="img-responsive">
                     @endif
                     </div>
                     <div class="row">
					 <?php $hotel_count=0;?>
					 @foreach($package_hotels as $hotels)
                        <div class="col-md-6 mb30">
						@if(isset($hotels))
							@foreach($hotels as $hotel)
                            @if($hotel->ResponseStatus==1)
                      
                           <div class="headback">
                              <h5 class="formhead"><i class="fa fa-bed"></i> Your Hotel In</h5>
                              <div class="theme-payment-page-sections-item">
                                 <div class="theme-search-results-item theme-payment-page-item-thumb">
                                    <div class="row" data-gutter="20">
                                       <div class="col-md-12">
                                          <h5 class="theme-search-results-item-title theme-search-results-item-title-lg">{{$hotel->HotelResults[0]->HotelName}}</h5>
										  <input type="hidden" name="HotelTraceId[]" value="{{$hotel->TraceId}}">
										  <input type="hidden" name="HotelResultIndex[]" value="{{$hotel->HotelResults[0]->ResultIndex}}">
										  <input type="hidden" name="HotelCode[]" value="{{$hotel->HotelResults[0]->HotelCode}}">
                                              @php 
                                              $package_cost+=$hotel->HotelResults[0]->Price->OfferedPrice;
                                              @endphp 
                                          
										  
                                          <div class="theme-search-results-item-rating">
                                             <ul class="theme-search-results-item-rating-stars">
                                                
												<?php $star=$hotel->HotelResults[0]->StarRating; for($k=0;$k<$star;$k++){?>
                                                <li class="active">
                                                   <i class="fa fa-star"></i>
                                                </li>
                                                <?php }?>
                                                    
                                                    <?php $star=5-$hotel->HotelResults[0]->StarRating; for($k=0;$k<$star;$k++){?>
                                                <li>
                                                   <i class="fa fa-star"></i>
                                                </li>
                                                <?php }?>
												
                                             </ul>
                                             <p class="theme-search-results-item-rating-title">160 reviews</p>
                                          </div>
                                          <ul class="theme-search-results-item-room-feature-list">
                                          
                                             <li>
                                                <i class="fa fa-map-marker theme-search-results-item-room-feature-list-icon"></i>
                                                <span class="theme-search-results-item-room-feature-list-title">{{$destination[$hotel_count+1]}}</span>
                                             </li>
                                          </ul>
                                          <p class="theme-search-results-item-location">
                                          <h5 class="theme-sidebar-section-title mt20">Booking Summary</h5>
                                          <ul class="theme-sidebar-section-summary-list">
                                             <li><i class="fa fa-users"></i>
                                            @php
                                                if(isset($passanger_count)){
                                                    if($passanger_count['adults']>0)
                                                    {
                                                        if($passanger_count['child']>0)
                                                        {
                                                            if($passanger_count['infant']>0)
                                                            {
                                                                $sum = $passanger_count['adults']+$passanger_count['child']+$passanger_count['infant'];                                                                
                                                            }
                                                            else{
                                                                $sum = $passanger_count['adults']+$passanger_count['child'];                                                                
                                                            }
                                                        } 
                                                        else{
                                                            $sum = $passanger_count['adults'];                                                            
                                                        }                                   
                                                        
                                                    }                                                                  
                                                }
                                                
                                            @endphp
                                              {{$sum}} Guest, {{$no_of_nights[$hotel_count]}} Nights, &nbsp; 
                                                <i class="fa fa-user"></i>
                                                @php
                                                    if(isset($passanger_count)){
                                                        if($passanger_count['adults']>0)
                                                        {
                                                            if($passanger_count['child']>0)
                                                            {
                                                                if($passanger_count['infant']>0)
                                                                {
                                                                    echo $passanger_count['adults']." Adults, ".$passanger_count['child']." Child, ".$passanger_count['infant']." Infant";
                                                                }
                                                                else{                                                                   
                                                                    echo $passanger_count['adults']." Adults, ".$passanger_count['child']." Child";
                                                                }
                                                            } 
                                                            else{                                                                
                                                                echo $passanger_count['adults']." Adults";
                                                            }                                   
                                                            
                                                        }                                                                  
                                                    }
                                                    
                                                @endphp 
                                            
                                             </li>
                                             <li><i class="fa fa-calendar"></i> Check-in: {{date('D,F d,Y',strtotime($hotel->CheckInDate))}}</li>
                                             <li><i class="fa fa-calendar"></i> Check-out: {{date('D,F d,Y',strtotime($hotel->CheckOutDate))}}</li>
                                          </ul>
                                          </p>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="theme-search-results-item-img-wrap">
                                             <img class="theme-search-results-item-img" src="{{$hotel->HotelResults[0]->HotelPicture}}" alt="Image Alternative text" title="Image Title" style="width: 100%;" />
                                          </div>
                                       </div>
                                       <button type="button" data-toggle="modal" data-target="#hotel<?php echo $hotel_count;?>" class="change-button">Change Hotel</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   
                            @endif
                            @endforeach
                            @endif
                        </div>
						
                    <div class="pp-hotel-po row modal fade" id="hotel<?php echo $hotel_count;?>" role="dialog">         
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div id="sidebar-hotel" class="col-xs-12 col-sm-12 col-md-3 side-bar left-side-bar">
                                        
                            <div class="side-bar-block filter-block">
                                <h3>Sort By Filter</h3>
                                
                                
                                <div class=" accordion panels-group" id="accordion ">
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading active">         
                                            <a href="#panel-1" data-toggle="collapse" >Select Category <span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->
                                        
                                        <div id="panel-1" class="panel-collapse collapse in">
                                            <div class="panel-body text-left ">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox"/>
                                                    <label for="check01"><span><i class="fa fa-check"></i></span>All</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check02" name="checkbox"/>
                                                    <label for="check02"><span><i class="fa fa-check"></i></span>Apartment</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check03" name="checkbox"/>
                                                    <label for="check03"><span><i class="fa fa-check"></i></span>Bed & Breakfast</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check04" name="checkbox"/>
                                                    <label for="check04"><span><i class="fa fa-check"></i></span>Guest House</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check05" name="checkbox"/>
                                                    <label for="check05"><span><i class="fa fa-check"></i></span>Hotels</label></li>        
                                                    <li class="custom-check"><input type="checkbox" id="check06" name="checkbox"/>
                                                    <label for="check06"><span><i class="fa fa-check"></i></span>Residence</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check07" name="checkbox"/>
                                                    <label for="check07"><span><i class="fa fa-check"></i></span>Resorts</label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading active">         
                                            <a href="#panel-2" data-toggle="collapse" >Facility<span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->
                                        
                                        <div id="panel-2" class="panel-collapse collapse in">
                                            <div class="panel-body text-left">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check08" name="checkbox"/>
                                                    <label for="check08"><span><i class="fa fa-check"></i></span>Air Conditioning</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check09" name="checkbox"/>
                                                    <label for="check09"><span><i class="fa fa-check"></i></span>Bathroom</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check10" name="checkbox"/>
                                                    <label for="check10"><span><i class="fa fa-check"></i></span>Cable Tv</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check11" name="checkbox"/>
                                                    <label for="check11"><span><i class="fa fa-check"></i></span>Parking</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check12" name="checkbox"/>
                                                    <label for="check12"><span><i class="fa fa-check"></i></span>Pool</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check13" name="checkbox"/>
                                                    <label for="check13"><span><i class="fa fa-check"></i></span>Wi-fi</label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading active">         
                                            <a href="#panel-3" data-toggle="collapse" >Rating <span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->
                                        
                                        <div id="panel-3" class="panel-collapse collapse in">
                                            <div class="panel-body text-left">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check14" name="checkbox"/>
                                                    <label for="check14"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"> </i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check15" name="checkbox"/>
                                                    <label for="check15"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check16" name="checkbox"/>
                                                    <label for="check16"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check17" name="checkbox"/>
                                                    <label for="check17"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check18" name="checkbox"/>
                                                    <label for="check18"><span><i class="fa fa-check"></i></span> <i class="fa fa-star"></i></label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->
                                    
                                </div><!-- end panel-group -->
                                
                                <div class="price-slider">
                                    <p><input type="text" id="amount" readonly></p>
                                    <div id="slider-range"></div>
                                </div><!-- end price-slider -->
                            </div><!-- end side-bar-block -->
                            
                            <div class="row">
                            
                                
                                <div class="col-xs-12 col-sm-6 col-md-12">    
                                    <div class="side-bar-block support-block">
                                        <h3>Need Help</h3>
                                        <p>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum.</p>
                                        <div class="support-contact">
                                            <span><i class="fa fa-phone"></i></span>
                                            <p>+1 123 1234567</p>
                                        </div><!-- end support-contact -->
                                    </div><!-- end side-bar-block -->
                                </div><!-- end columns -->
                                
                            </div><!-- end row -->
                        </div><!-- end columns --> 
                        
                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side">
                            @if(isset($hotels))
							@foreach($hotels as $hotel)
                            @if($hotel->ResponseStatus==1)
                            @foreach($hotel->HotelResults as $ht)
                            <div class="list-block main-block h-list-block">
                              <div class="list-content">
                                <div class="main-img list-img h-list-img">
                                        <a href="#">
                                            <img src="{{$ht->HotelPicture}}" class="img-responsive" alt="hotel-img" />
                                        </a>
                                        <div class="main-mask">
                                            <ul class="list-unstyled list-inline offer-price-1">
                                                <li class="price">{{$ht->Price->CurrencyCode}}{{$ht->Price->OfferedPrice}}<span class="divider">|</span><span class="pkg">Avg/Night</span></li>
                                                <li class="rating">
                                                <?php $star=$ht->StarRating; for($i=0;$i<$star;$i++){?>
                                                  <span><i class="fa fa-star"></i></span>
                                                <?php }?>
                                                    
                                                    <?php $star=5-$ht->StarRating; for($i=0;$i<$star;$i++){?>
                                                        <span><i class="fa fa-star lightgrey"></i></span>
                                                <?php }?>
                                                    
                                                </li>
                                            </ul>
                                        </div><!-- end main-mask -->
                                    </div><!-- end h-list-img -->
                                    
                                    <div class="list-info h-list-info">
                                        <h3 class="block-title"><a href="#">{{$ht->HotelName}}</a></h3>
                                        <p>{{ str_limit($ht->HotelDescription, $limit = 350, $end = '...') }}</p>
                                        <div class="row">
                                          <div class="col-md-2">
                                            <img src="assets/home/images/bell_service-128.png" style="width: 20px;">
                                          </div>
                                          <div class="col-md-2">
                                            <img src="assets/home/images/gym.png" style="width: 20px;">
                                          </div>
                                          <div class="col-md-2">
                                            <img src="assets/home/images/restaurant-food.png" style="width: 20px;">
                                          </div>
                                          <div class="col-md-2">
                                            <img src="assets/home/images/wifi.png" style="width: 20px;">
                                          </div>
                                          <div class="col-md-2">
                                            <img src="assets/home/images/swimming.png" style="width: 20px;">
                                          </div>
                                        </div>
                                        <a href="#" class="btn btn-orange btn-lg" onclick='changeHotel({{$i}},{{$ht->ResultIndex}},"{{$ht->HotelCode}}")'>Select</a>
                                     </div><!-- end h-list-info -->
                              </div><!-- end list-content -->
                            </div><!-- end h-list-block -->
                            @endforeach
                            @endif
                            @endforeach
                            @endif
                            
                            
                            <div class="pages">
                                <ol class="pagination">
                                    <li><a href="#" aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#" aria-label="Next"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>
                                </ol>
                            </div><!-- end pages -->
                        </div><!-- end columns -->

                    </div><!-- end row -->
					
						<?php $hotel_count++;?>
						@endforeach
                        <div class="col-md-6 mb30">
                           <div class="headback">
                              <h5 class="formhead"><i class="fa fa-plane"></i> Your Included Flights</h5>
							  
							  @if(isset($flights->Response->Results[0][0]))
										  <input type="hidden" name="FlightTraceId" value="{{$flights->Response->TraceId}}">
										  <input type="hidden" name="FlightResultIndex" value="{{$flights->Response->Results[0][0]->ResultIndex}}">
                                           @php
                                           $package_cost+=$flights->Response->Results[0][0]->Fare->OfferedFare;
                                           @endphp 
                                           @php $count_fpd = 1; @endphp
								  @foreach($flights->Response->Results[0][0]->Segments as $seg)
                              <div class="theme-payment-page-sections-item">
                                 <div class="theme-search-results-item theme-payment-page-item-thumb">
                                    <div class="row" data-gutter="20">
                                       <div class="col-md-9 ">
                                          <p class="theme-search-results-item-flight-payment-airline">You are flying {{$seg[0]->Airline->AirlineName}}</p>
                                          <h5 class="theme-search-results-item-title">{{$seg[0]->Origin->Airport->CityName}}, {{$seg[0]->Origin->Airport->CityCode}} &nbsp;&rarr;&nbsp; {{$seg[(count($seg))-1]->Destination->Airport->CityName}}, {{$seg[(count($seg))-1]->Destination->Airport->CityCode}}</h5>
                                          
                                          <a class="theme-search-results-item-flight-payment-details-link flight_details " >Flight Details &nbsp;
                                          <i class="fa fa-angle-down"></i>
                                          </a>
                                          <!-- popup hover -->
                                          <div class="flight_tipTip_cust_card" tabindex="{{$count_fpd++}}" style="display:none">
                                            <span class="flight_tiptip_arrow"></span>
                                            <span class="clearFix flight_details_tooltip">
                                            <!--<span class="modal_heading append_bottom alC width100 spanCenterAligned">-->
                                            <!--    Flight details-->
                                            <!--</span>-->
                                        <span class="clearFix">
                                            <span></span>
                                            <span class="clearFix mg-bot-20">
                                            <span class="rvw_flght_logo flL">
                                            <span class="block append_bottom3">
                                            <span><span class="flL append_bottom3 flight_logo " name="6E" style="background-position: 0px -1px;"></span>
                                            <span class="flL holC_mgr10" name="6E">
                                            <i class="fa fa-plane" style="color: #fff !important;font-size: 25px;"></i>
                                            </span></span></span></span>
                                            <span class="flL r_flght_sectn">
                                                <span class="clearFix mg-bot-10 cabin_bagage">
                                                    <span class="rvw_flght_terminal flL">
                                                        <span class="block fontSize13 append_bottom3">
                                                            <span class="light_gray">
                                                                {{$seg[0]->Origin->Airport->CityCode}}
                                                                <!-- CCU  -->
                                                            </span><strong>
                                                                    @php 
                                                                        $aDatetime = strtotime($seg[0]->StopPointArrivalTime); 
                                                                        $atime = date("H:i",$aDatetime);
                                                                        
                                                                        $dDatetime = strtotime($seg[(count($seg))-1]->StopPointDepartureTime); 
                                                                        $dtime = date("H:i",$dDatetime);
                                                                    
                                                                    @endphp
                                                                    {{$atime}}
                                                                    <!-- 21:45 -->
                                                                </strong>
                                                                </span></span><span class="rvw_flght_time flight_details_open flL">
                                                                <span class="ybbhbc back_brdr"><span class="fontSize11 ybbhbc">
                                                                    @php 
                                                                        $datetimed = new DateTime($seg[(count($seg))-1]->StopPointArrivalTime);
                                                                        $datetimea = new DateTime($seg[0]->StopPointDepartureTime);
                                                                        $interval = $datetimed->diff($datetimea);
                                                                        echo $interval->format('%h')."H ".$interval->format('%i')."m";
                                                                    @endphp
                                                                    <!-- 2h 35m -->
                                                                </span>
                                                                <span class="small_arrow"></span></span><span class="fontSize9 ybbhbc">
                                                                    {{$seg[0]->Origin->Airport->CityCode}}-{{$seg[(count($seg))-1]->Destination->Airport->CityCode}}
                                                                    <!-- CCU-DEL -->
                                                                </span>
                                                                </span><span class="rvw_flght_terminal flL last"><span class="block fontSize13 append_bottom3">
                                                                <span class="light_gray">
                                                                    {{$seg[(count($seg))-1]->Destination->Airport->CityCode}}
                                                                    <!-- DEL  -->
                                                                </span><strong>
                                                                    
                                                                    {{$dtime}}
                                                                    <!-- 00:20 -->
                                                                </strong></span></span></span>
                                                                <span class="fontSize11 light_gray flL">
                                                                    <span>
                                                                        {{$seg[0]->Airline->AirlineName." | ".$seg[0]->Airline->AirlineCode."-".$seg[0]->Airline->FlightNumber}}
                                                                        <!-- IndiGo | 6E-224 -->
                                                                    </span>
                                                                <span id="cabinInfo">| Cabin:
                                                                @php $fareclass =$seg[0]->Airline->FareClass;  @endphp 
                                                                @if(in_array($fareclass,array("K","L","Q","V","W","U","T","X","N","O","S")))
                                                                    {{"Discounted Economy"}}
                                                                @elseif(in_array($fareclass,array("Y","B","M","H")))
                                                                    {{"Economy"}}
                                                                @elseif(in_array($fareclass,array("W","E")))
                                                                    {{"Premium Economy"}}
                                                                @elseif(in_array($fareclass,array("D","I","Z")))
                                                                    {{"Discounted Business"}}
                                                                @elseif(in_array($fareclass,array("J","C","D")))
                                                                    {{"Business"}}
                                                                @elseif(in_array($fareclass,array("A","F")))
                                                                    {{"First"}}
                                                                @endif
                                                                </span></span></span></span></span>
                                                                <span class="clearFix"><span>
                                                                <span class="layover_tooltip ybbhbc mg-bot-20 clearFix">
                                                                    
                                                                    @if(count($seg)>1)
                                                                        Plane change at 
                                                                        {{$seg[0]->Destination->Airport->CityName}}
                                                                        <!-- Kolkata -->
                                                                        Waiting: 
                                                                     @php 
                                                                        $datetimed = new DateTime($seg[(count($seg))-1]->StopPointDepartureTime);
                                                                        $datetimea = new DateTime($seg[0]->StopPointArrivalTime);
                                                                        $interval = $datetimed->diff($datetimea);
                                                                        echo $interval->format('%h')."H ".$interval->format('%i')."m";
                                                                    @endphp
                                                                    @endif
                                                                     |
                                                                      <!-- 3h 5m -->
                                                                      </span>
                                                                </span>
                                                                </span></span></div>
                                        <!-- End: popup hover -->
                                       </div>
                                       <div class="col-md-3 ">
                                          <div class="theme-search-results-item-img-wrap">
										  <?php  
        												    $dirname = "assets/home/images/AirlineLogo";
        													$filename = glob("$dirname/*{$seg[0]->Airline->AirlineCode}*", GLOB_BRACE);
        									?>
        												@if(isset($filename) && count($filename)>0)
        												    
														<img class="theme-search-results-item-img _mob-h" src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" title="Image Title"/>
                                                        @endif										  
                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
							  @endforeach
							  							  <button type="button" data-toggle="modal" data-target="#flight" class="change-button">Change Flight</button>
							  <div class="row modal fade" id="flight" role="dialog">
							      <button type="button" class="close" data-dismiss="modal">&times;</button>
<div class="container">
    <div class="modal-header"><h2 style="text-align: center;">Change Your Flight</h2></div>
  <div class="row side-bar">
      <div class="col-md-3 margin-top-headline side-flightfilter filter-block">
    <div class="mtf-nav">
    <ul>
        <li><a id="fly_1" href="#" class="mtta">1</a></li>
        <li><a id="fly_2" href="#" class="mtta">2</a></li>
        <li><a id="fly_3" href="#" class="mtta">3</a></li>
        <li><a id="fly_4" href="#" class="mtta">4</a></li>
        <li><a id="fly_5" href="#" class="mtta active-mttab">5</a></li>
    </ul>
</div>
<div id="flytab_1" class="mttabs hide-mttabs">
    <div class="side-stops">
        <h3 class="fsidebar-head">Stops 1</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>
<div id="flytab_2" class="mttabs hide-mttabs">
<div class="side-stops">
        <h3 class="fsidebar-head">Stops 2</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>
<div id="flytab_3" class="mttabs hide-mttabs">
<div class="side-stops">
        <h3 class="fsidebar-head">Stops 3</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>

<div id="flytab_4" class="mttabs hide-mttabs">
<div class="side-stops">
        <h3 class="fsidebar-head">Stops 4</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>
    <div id="flytab_5" class="mttabs">
<div class="side-stops">
        <h3 class="fsidebar-head">Stops 1</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>
  </div>
 <!--   <div class="col-md-3 margin-top-headline">-->
       
 <!--     <div class="row">-->
       
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h5>Filter Results</h5>-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h5 style="text-align: right;"><a href="#">Reset All</a></h5>-->
 <!--       </div>-->
 <!--     </div>-->
 <!--   <div class="border-bottom"></div>-->
 <!--       <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h4>No. of Stops</h4>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <li class="active" style="border-right: 1px solid #ddd;">-->
 <!--             <a href="#tab_default_1" data-toggle="tab" style="padding-left: 31px;">-->
 <!--             <strong> 0 stop</strong> </a>-->
 <!--             <h6 style="padding-left: 22px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--           <li style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--             <strong> 1 stop</strong></a>-->
 <!--              <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--           <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--           <strong> 1+ stop</strong> </a>-->
 <!--            <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!-- <div class="border-bottom"></div>-->
 <!--  <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h5>Departure time from New Delhi</h5>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">-->
 <!--           <li class="active">-->
 <!--             <a href="#tab_default_1" data-toggle="tab">-->
 <!--            <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> Before 6am No Flights</h6>-->
 <!--           </li>-->
 <!--        </div>-->
 <!--        <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--           <li>-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--               <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> 6 AM-12 PM No Flights</h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;    height: 92px;">-->
 <!--           <li >-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6>12PM - 6 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3">-->
 <!--            <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--           <h6> After 6  PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!--  <div class="border-bottom"></div>-->
 <!--  <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h5>Departure time from Mumbai</h5>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">-->
 <!--           <li class="active">-->
 <!--             <a href="#tab_default_1" data-toggle="tab">-->
 <!--            <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> Before 6am</h6>-->
 <!--           </li>-->
 <!--        </div>-->
 <!--        <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--           <li>-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--               <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> 6 AM-12 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;">-->
 <!--           <li >-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6>12PM - 6 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3">-->
 <!--            <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--           <h6> After 6  PM </h6>-->
 <!--           </li>-->
 <!--           </div>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!-- <div class="border-bottom"></div>-->
 <!-- <div class="row">-->
 <!--   <div class="col-md-10 col-xs-12">-->
 <!--     <h5> Airlines</h5>-->
 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--       <i id="4" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--          <i id="3" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
 <!--    <i id="2" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
 <!--  <i id="item" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--<i id="1" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!--   </div>-->
 <!-- </div>-->

 <!--   </div>-->
 <div class="col-md-9 content-side">
      <div class="row multi-flight-list">
	  @if(isset($flights->Response->Results[0]))
	  
		  @foreach($flights->Response->Results[0] as $fd)
      <div class="result_p row mb3" id="ResSet_0">
                                        <div class="retun_inter_sep">
                                            <?php $seg_index=0; ?>
                                            @foreach($fd->Segments as $seg)
                                            <div class="fleft border_bot pad_em width_98">
                                                <span class="spl_icon_flight">
                                                    <kbd class="fleft width_98">
        												<?php  
        												    $dirname = "assets/home/images/AirlineLogo";
        													$filename = glob("$dirname/*{$seg[0]->Airline->AirlineCode}*", GLOB_BRACE);
        												?>
        												@if(isset($filename) && count($filename)>0)
        												    <img src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" class="fleft mr5">
                                                        @endif
        			                                    <code id="Code_nm_mob" class="font10 fleft lh_10 width_64 m_width_100">
        			                                        <em class="mobile_not">  {{$seg[0]->Airline->AirlineName}} </em> 
                                                            <em class="fleft width_100 font10">
                                                                <kbd class="fleft mr5 lh10" id="AirlineDetailsOutBound_0">
                                                                    {{$seg[0]->Airline->AirlineCode}} - {{$seg[0]->Airline->FlightNumber}} 
                                                                </kbd>
                                                                <em class="mobile_class" id="class_0" title="Flight Booking Class">
                                                                    <dfn class="class_re">Class -</dfn> 
                                                                    {{$seg[0]->Airline->FareClass}}
                                                                </em>
                                                            </em>
                                                        </code>
                                                    </kbd>
                                                </span>
                                                <input type="hidden" id="segLenOut_0" value="2">
                                                <input type="hidden" id="segLenIN_0" value="2">
                                                <span class="spl_duration_flight">
                                                    <kbd class="fleft width_100per">
                                                        <em id="departure_0">
                                                            {{$seg[0]->Origin->Airport->AirportCode}}  
                                                            <tt class="hightlight bold" id="deptTimeOut_0">
                                                                ({{date('H:i',strtotime($seg[0]->Origin->DepTime))}})
                                                            </tt>
                                                        </em>                                  
																<?php $count_seg=count($fd->Segments[$seg_index]); ?>
																@if($count_seg>1)
																    @foreach($fd->Segments[$seg_index] as $multi)
                                                                        <code> →</code> 
        															    <dfn id="SegOut_0_0">{{$multi->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_0" value="CCU"></dfn>
        															      
                                                                        <input type="hidden" id="destAirport_0" value="BKK">
                                                                    @endforeach
                                                                    <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[$count_seg-1][0]->Destination->ArrTime))}})</tt>
															    @else
    																<code> →</code> <em class="txt_aln_rt" id="arrival_0"> {{$seg[0]->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_1" value="BKK"> <tt class="hightlight bold">({{date('H:i',strtotime($seg[0]->Destination->ArrTime))}})</tt></em>
                                                                
                                                                    <input type="hidden" id="destAirport_0" value="BKK">
                                                                @endif
                                                                
                                                                
              <!--                                          <em id="departure_0">-->
              <!--                                              {{$seg[0]->Origin->Airport->AirportCode}}  -->
              <!--                                              <tt class="hightlight bold" id="deptTimeOut_0">-->
              <!--                                                  ({{date('H:i',strtotime($seg[0]->Origin->DepTime))}})-->
              <!--                                              </tt>-->
              <!--                                          </em>                                  -->
														<!--<code> →</code>-->
														<!--<em class="txt_aln_rt" id="arrival_0">-->
    										<!--		        {{$seg[0]->Destination->Airport->AirportCode}}  -->
    										<!--	            <tt class="hightlight bold">-->
    										<!--		            ({{date('H:i',strtotime($seg[0]->Destination->ArrTime))}})-->
    										<!--		        </tt>-->
													 <!--   </em>-->
                                                    </kbd>
                                                </span> 
                                                <span class="spl_dur_flight mobile_not" id="spldurationboxoutbound_0">
                                                    <kbd class="fleft width_100">
                                                        <span id="duration_0">
                                                              <b> 
                                                              <?php $datetimed = new DateTime($seg[0]->StopPointDepartureTime);
                                                                $datetimea = new DateTime($seg[0]->StopPointArrivalTime);
                                                                $interval = $datetimed->diff($datetimea);
                                                                echo $interval->format('%h')."h ".$interval->format('%i')."m"; ?>
                                                               </b></span><br>
                                                         
                                                           
                                                            <a class="stop_1" href="#!" id="outBoundStops_0">
                                                                <small>
                                                                    
                        
                                                                           @if(count($seg)==1)
                                                                            Non Stop
                                                                           @else
                                                                            {{(count($seg)-1)." Stop" }}
                                                                           @endif
                                                                        
                                                                    
                                                                </small></a>
                                                        
                                                    </kbd>
                                                                                                
                                                           <br>
														   @if(isset($seg[0]->NoOfSeatAvailable))
														   <code class="red">{{$seg[0]->NoOfSeatAvailable}} seat(s) left</code> 
														   @else
														   <code>{{"no seat(s) "}}</code>
															@endif
                                                       
                                                        <div class="outbound_stop_popup" id="outBoundStopInfo_0" style="display: none;">
                                                        <a class="fl_close ipad_close_btn" id="outBoundStopInfoClose_0" href="#"></a>
                                                            
                                                                <kbd class="fleft width_100per">
                                                                    <em>SG-253 A</em>
                                                                    <em> &nbsp;&nbsp; DEL(18:20)</em>
                                                                    <code> →</code> 
                                                                                             
                                                                    <em>CCU(20:30)</em>
                                                                </kbd>                                           
                                                            
                                                                <kbd class="fleft width_100per">
                                                                    <em>SG-83 A</em>
                                                                    <em> &nbsp;&nbsp; CCU(00:05)</em>
                                                                    <code> →</code> 
                                                                                             
                                                                    <em>BKK(04:10)</em>
                                                                </kbd>                                           
                                                            
                                                        </div>
                                                    
                                                </span>
                                                <?php $seg_index++; ?>
                                            </div>
                                        @endforeach
           
                                        </div>                        
                                        <span class="spl_price mobile_not" id="pub_fare_0">
                                            <kbd class="fleft Width_100 m_float_rt" id="pubPricekbd_0" style="visibility:visible;">
                                                <tt>Rs. </tt>
                                                <tt id="PubPrice_0">
                                                    {{" ".number_format(money_format('%!i',$fd->Fare->PublishedFare),0)}}
                                                </tt>
                                                
                                            </kbd> 
                                            <span class="icon_flight2 width-120 mobile_not">
                                                <a class="ruppe" href="#" id="FareRule_0"><span class="drop2"> Fare Rule</span></a>
                                                
                                                    <a class="close cur_default" href="#"><span class="drop2" id="isRefundable_0">Lowest Cancellation</span></a>
                                                   
                                                 
                                            </span>  
                                        </span>

                                        <span class="spl_price mobile_not" id="OfferPriceSpan_0" style="visibility:visible;">
                                            <span>Rs.</span>
                                            <span id="OfferPrice_0">
                                             {{" ".number_format(money_format('%!i',$fd->Fare->OfferedFare),0)}}
                                            </span>
                                            
                                        </span>
                                       
                                       <span class="book_now_emails">
                                        <span class="book_now_btn mt5"><a href="#" id="btnBookNow_0" class="btn_bg" onclick='changeFlight("{{$fd->ResultIndex}}")'>Select</a></span>
                                       
                                       </span>
                        
                                        <span class="spl_email spl_pt15">
                                            <input type="checkbox" id="chkEmailItinerary_0" name="emailItinerary" value="0"> Email
                                        </span>
                                         

                                        <div class="fare_rule_pop" id="FareRuleBlock_0" style="display: none;">
                                            <div class="head_pop" id="FareRuleHead">
                                                <span id="FareRuleHeadTitle">Fare Rules</span>
                                                <a href="#" id="btnHeadFareRuleBlockClose_0" class="fl_close"></a>
                                            </div>
                                            <div id="loadingMsg_0">
                                            </div>
                                            <div class="fare_footer" id="FareRuleFoot">
                                                <span class="close_btn cursor">
                                                    <input type="button" value="Close" id="btnFareRuleBlockClose_0">
                                                </span>
                                            </div>
                                        </div>

                                        
                                                    <p class="airline_rem"><span>Remarks for star Coupon.</span></p>
                                                
                                        
                                        <div class="stop_1 fleft relative width_100">     <p class="airline_rem">
                                            <span class="cursor" id="FreeBaggage_0"> Check-In Baggage :
                                          15 Kg ,15 Kg ,15 Kg ,15 Kg 
                                          
                                            </span>
                                             </p>
                                          
                                        </div>
                                         
                                            

                                    </div>
									@endforeach
									@endif
        </div>
      </div>
 
 
 
 
 
 
 
   
        </div>

      </div>
							      
							      </div>
							  
								@endif
								
							  
					
                           </div>
                        </div>
                     </div>
                     
                    <div class="row">
                     <div class="col-md-6 mb30">
                           <div class="headback">
                              <h5 class="formhead"><i class="fa fa-plane"></i> Your Activities</h5>
                                <div class="theme-payment-page-sections-item">
                                @if(!empty($package_detail->activity))
                                @php $activity = explode('-',$package_detail->activity);  @endphp
                               
                                @for($i=0;$i < count($activity);$i++)
                                @foreach($activity_detail as $acv)
                                @if($acv->id==$activity[$i])
                                <div class="activity_cards">
                                    <div class="col-md-3 activity_icon_class">
                                        <img src="{{asset('assets/home/images/'.$acv->category.'.png')}}" class="active-icon-t">
                                    </div>
                                    <div class="col-md-9 activity_detail_class">
                                        <h5 class="activity_ka_title">{{$acv->title}}</h5>
                                        <span class="date-activity">{{$acv->d_title}}</span>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                @endfor
                                @endif
                                </div>
                            </div>
                            </div>
                    </div>
                     
                    <div class="theme-payment-page-sections-item itinerary">
                         <h3 class="itenary-headi">Your Itinerary</h3>
                        @php 
                                                        
                            if(!empty($package_detail->day_title))
                            {

                            $explode_data = explode(',',$package_detail->day_title);
                            $explode_data1 = explode(',',$package_detail->day_destination);
                            $explode_data2 = explode('---',$package_detail->day_description);
                            for($i=0;$i < count($explode_data);$i++)
                            {
                            echo " <button type='button' class='accordion'>".$explode_data[$i]."</button><div class='panel'>" ;
                           

                            echo" <div class='form-group'> 
                           
                            <div class='col-lg-10'>
                            ".$explode_data1[$i]."
                            </div>
                            </div>";

                            echo" <div class='form-group'> 
                            
                            <div class='col-lg-10 mce-box'>
                            
                            ".$explode_data2[$i]."
                            
                            </div>
                            </div>";

                            echo "</div>";
                            }
                            }
                        
                        @endphp
                        
                        
                    </div>
                                                
                    <div class="theme-payment-page-sections-item itinerary">
                        <button type="button" class="accordion">T&C</button>
                            <div class="panel">
                            <p>Lorem ipsum...</p>
                            </div>


                                                </div>
                                                
                                                
                                            </div>
                                        </div>
               <div class="col-md-4 ">
                  <div class="sticky-col">
                     <div class="theme-sidebar-section _mb-10">
                     @php $count_destination=count($destination);
                            $count_iter=0;
                     @endphp
                        <h5 class="theme-sidebar-section-title">Itinerary - {{($count_destination-1)}} City</h5>
                        <div class="theme-sidebar-section-charges">
                           <div class="itenary-cities">
                            @foreach($destination as $key=>$dest)
                                @if($count_iter>0)
                                    @php $explode_dest=explode(',',$dest); @endphp
                                    @if($count_iter==1)
                                    {{$explode_dest[0].' '.$no_of_nights[$key-1].'N'}}
                                    @else
                                    {{', '.$explode_dest[0].' '.$no_of_nights[$key-1].'N'}}
                                    @endif
                                @endif
                                @php $count_iter++; @endphp
                            @endforeach
                           <!-- Bangkok 3N, Chiang Mai 4N -->
                           </div>
                           <div class="start-date-tour">
                              @php $tour_start_date = $flights->Response->Results[0][0]->Segments[0][0]->Origin->DepTime;
                                                
                              @endphp
                              {{date('dS M,Y',strtotime($tour_start_date))}}
                                <!-- 13th, November 2018 -->
                           </div>
                           <div class="no-person-tour">
                                @php
                                    if(isset($passanger_count)){
                                        if($passanger_count['adults']>0)
                                        {
                                            if($passanger_count['child']>0)
                                            {
                                                if($passanger_count['infant']>0)
                                                {
                                                    $sum = $passanger_count['adults']+$passanger_count['child']+$passanger_count['infant'];
                                                    echo "Adults (".$passanger_count['adults']."), Child (".$passanger_count['child']."), Infant (".$passanger_count['infant'].")";
                                                }
                                                else{
                                                    $sum = $passanger_count['adults']+$passanger_count['child'];
                                                    echo "Adults (".$passanger_count['adults']."), Child (".$passanger_count['child'].")";
                                                }
                                            } 
                                            else{
                                                $sum = $passanger_count['adults'];
                                                echo "Adults (".$passanger_count['adults'].")";
                                            }                                   
                                            
                                        }                                                                  
                                    }
                                    
                                @endphp
                           </div>
                           <div class="theme-payment-page-sections-item">
                        <div class="theme-payment-page-booking">
                           <div class="theme-payment-page-booking-header">
                              <p class="theme-payment-page-booking-subtitle">By clicking book now button you agree with terms and conditions.</p>
                              <p class="theme-payment-page-booking-price">
                                <div class="destination-info" id="destination-price">
                                    @if(isset($package_cost_off))
                                    <del>{{$package_cost}}</del>
                                    
                                    <span class="holC-discount-tag inlineB ng-binding">{{$package_cost_off."%"}} off</span>
                                    @endif
                                    <div class="destination-title">
                                        <a class="pack-cost" href="#">
                                        <i class="fa fa-lg fa-inr" aria-hidden="true"></i> 

                                        <?php 
                                        if(isset($package_cost_off))
                                        {
                                            $offer_val = ($package_cost_off/100)*$package_cost;
                                            $cost_val = $package_cost-$offer_val;
                                        }
                                        else
                                        {
                                            $cost_val = $package_cost;
                                        }
                                        
                                        ?>
                                        {{round($cost_val,0)}}
                                        </a>
                                        <p>Per person from {{$destination[0]}}</p>
                                                    
                                    </div>
                                            <!-- end destination-title --> 
                                </div>
                                <!-- <span class="theme-payment-page-booking-title">Total Price</span>1278.00 -->
                              </p>
                           </div>
                           <button id="customize_button" type="button" class="btn" data-toggle="modal" data-target="#package-edit"> Customize</button>
                           <a class="btn _tt-uc btn-primary-inverse btn-lg btn-block red_button" id="book_now" href="#">Book Now</a>
                        </div>
                     </div>
                        </div>
                     </div>
                     
                    
                     <div class="theme-sidebar-section _mb-10">
                        <ul class="theme-sidebar-section-features-list">
                           <li>
                              <h5 class="theme-sidebar-section-features-list-title">Manage your bookings!</h5>
                              <p class="theme-sidebar-section-features-list-body">You're in control of your booking - no registration required.</p>
                           </li>
                           <li>
                              <h5 class="theme-sidebar-section-features-list-title">Customer support available 24/7 worldwide!</h5>
                              <p class="theme-sidebar-section-features-list-body">For Hassle Free Bookings</p>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  </form>
     
      @include('frontend.includes.footer')
      @include('frontend.includes.customize-footer')	  
     <script>
     $(".flight_details").click(function(e){
    e.preventDefault();
    $(this).next(".flight_tipTip_cust_card").show(function(){$(this).focus();});
});
$(".flight_tipTip_cust_card").on('blur',function(){
    $(this).hide();
});
       
     </script>
	  <script>
	  $(document).on('click','#book_now',function(){
		 $('#package_form')[0].submit(); 
	  });
	  </script>
        
    <script>
        var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
    </script>
    
    <script>
    $(window).click(function(event) {
  if (!$(event.target).closest(".modal,.js-open-modal").length) {
    $("body").find(".modal").removeClass("visible");
  }
});

    </script>
    <script>
    function changeHotel(idSelector,resultIndex,hotelCode)
    {
       $('#htrd'+idSelector).val(resultIndex);
       $('#hc'+idSelector).val(hotelCode);
       alert(resultIndex);
    }
    function changeFlight(resultIndex)
    {
        $('#flrd').val(resultIndex);
        alert(resultIndex);
    }
    </script>
   </body>
    
  