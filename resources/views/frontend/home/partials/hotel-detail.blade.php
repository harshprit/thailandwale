
@foreach($hotels as $ht)
<div class="list-block main-block h-list-block">
  <div class="list-content">
    <div class="main-img list-img h-list-img">
            <a href="#">
                <img src="{{$ht->HotelPicture}}" class="img-responsive" alt="hotel-img" />
            </a>
            
        </div><!-- end h-list-img -->

        <div class="list-info h-list-info">
            <h3 class="block-title"><a href="#">{{$ht->HotelName}}</a></h3>
            <!--<p>{{ str_limit($ht->HotelDescription, $limit = 350, $end = '...') }}</p>-->
            <!-- <div class="row">
              <div class="col-md-2">
                <img src="assets/home/images/bell_service-128.png" style="width: 20px;">
              </div>
              <div class="col-md-2">
                <img src="assets/home/images/gym.png" style="width: 20px;">
              </div>
              <div class="col-md-2">
                <img src="assets/home/images/restaurant-food.png" style="width: 20px;">
              </div>
              <div class="col-md-2">
                <img src="assets/home/images/wifi.png" style="width: 20px;">
              </div>
              <div class="col-md-2">
                <img src="assets/home/images/swimming.png" style="width: 20px;">
              </div>
            </div> -->
            <div class="price-and-all">
                <ul class="list-unstyled list-inline offer-price-1">
                    <li class="price">{{$ht->Price->CurrencyCode}}{{$ht->Price->OfferedPriceRoundedOff}}<span class="divider">|</span><span class="pkg">Avg/Night</span></li>
                    <li class="rating">
                    <?php $star=$ht->StarRating; for($i=0;$i<$star;$i++){?>
                      <span><i class="fa fa-star"></i></span>
                    <?php }?>

                        <?php $star=5-$ht->StarRating; for($i=0;$i<$star;$i++){?>
                            <span><i class="fa fa-star lightgrey"></i></span>
                    <?php }?>

                    </li>
                    <li>
                        <a href="{{route('frontend.hotel_info',['TraceId' =>$trace_id ,'ResultIndex'=>$ht->ResultIndex,'HotelCode'=>$ht->HotelCode])}}" class="btn btn-orange btn-lg">BOOK NOW</a>
                    </li>
                </ul>
            </div><!-- end main-mask -->
         </div><!-- end h-list-info -->
  </div><!-- end list-content -->
</div><!-- end h-list-block -->
@endforeach
