    <div>
      <div class="multi-flight-list container-fluid">
	  @if(isset($flights->Response->Results[0]))
	  
		  @foreach($flight_result as $fd)
      <div class="result_p row mb3" id="ResSet_0">
                                        <div class="col-md-9 col-xs-12">
                                            <div class="fleft border_bot pad_em width_98">
                                            
                                                <span class="spl_icon_flight">
                                                    <span class="fleft width_98">
													<?php   $dirname = "assets/home/images/AirlineLogo";
														$filename = glob("$dirname/*{$fd->Segments[0][0]->Airline->AirlineCode}*", GLOB_BRACE); ?>
														@if(isset($filename) && count($filename)>0)
														<img src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" class="fleft mr5">
                                                        @endif
					                                                            								
                                                        
                                                        <code id="Code_nm_mob" class="font10 fleft lh_10 width_64 m_width_100"><em class="mobile_not">  {{$fd->Segments[0][0]->Airline->AirlineName}} </em> 
                                                        <em class="fleft width_100 font10">
                                                            <span class="fleft mr5 lh10" id="AirlineDetailsOutBound_0">{{$fd->Segments[0][0]->Airline->AirlineCode}} - {{$fd->Segments[0][0]->Airline->FlightNumber}} </span>
                                                            <em class="mobile_class" id="class_0" title="Flight Booking Class"><dfn class="class_re">Class -</dfn> {{$fd->Segments[0][0]->Airline->FareClass}}</em>
                                                        </em>
                                                            
                                                        </code>
                                                    </span>
                                                </span>
                                                
                                                 <input type="hidden" id="segLenOut_0" value="2">
                                                 <input type="hidden" id="segLenIN_0" value="2">
                                                <span class="spl_duration_flight">
                                                    <span class="fleft width_100per">
                                                        
                                                                <em id="departure_0">{{$fd->Segments[0][0]->Origin->Airport->AirportCode}} <input type="hidden" id="deptAirport_0" value="DEL">   <tt class="hightlight bold" id="deptTimeOut_0">({{date('H:i',strtotime($fd->Segments[0][0]->Origin->DepTime))}})</tt></em>                                  
																<?php $count_seg=count($fd->Segments[0]); ?>
																@if($count_seg>1)
																    @foreach($fd->Segments[0] as $intfl)
                                                                        <code> →</code> 
        															    <dfn id="SegOut_0_0">{{$intfl->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_0" value="CCU"></dfn>
        															      <!--<code> →</code> <em class="txt_aln_rt" id="arrival_0"> {{$intfl->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_1" value="BKK"> <tt class="hightlight bold">({{date('H:i',strtotime($intfl->Destination->ArrTime))}})</tt></em>-->
                                                                        <input type="hidden" id="destAirport_0" value="BKK">
                                                                    @endforeach
                                                                    <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[0][$count_seg-1]->Destination->ArrTime))}})</tt>
															    @else
    																<code> →</code> <em class="txt_aln_rt" id="arrival_0"> {{$fd->Segments[0][0]->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_1" value="BKK"> <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[0][0]->Destination->ArrTime))}})</tt></em>
                                                                
                                                                    <input type="hidden" id="destAirport_0" value="BKK">
                                                                @endif
                                                            
														
                                                            
                                                        
                                                    </span>
                                                </span> 
                                                <span class="spl_dur_flight mobile_not" id="spldurationboxoutbound_0">
                                                    <span class="fleft width_100">
                                                        <span id="duration_0">
                                                              <b> 
                                                              <?php $datetimed = new DateTime($fd->Segments[0][0]->StopPointDepartureTime);
                                                                $datetimea = new DateTime($fd->Segments[0][$count_seg-1]->StopPointArrivalTime);
                                                                $interval = $datetimed->diff($datetimea);
                                                                echo $interval->format('%h')."h ".$interval->format('%i')."m"; ?>
                                                               </b></span><br>
                                                         
                                                           
                                                            <a class="stop_1" href="#!" id="outBoundStops_0">
                                                                <small>
                                                                    
                        
                                                                           @if(count($fd->Segments[0])==1)
                                                                            Non Stop
                                                                           @else
                                                                            {{(count($fd->Segments[0])-1)." Stop" }}
                                                                           @endif
                                                                        
                                                                    
                                                                </small></a>
                                                        
                                                    </span>
                                                                                                
                                                           <br>
														   @if(isset($fd->Segments[0][0]->NoOfSeatAvailable))
														   <code class="red">{{$fd->Segments[0][0]->NoOfSeatAvailable}} seat(s) left</code> 
														   @else
														   <code>{{"no seat(s) "}}</code>
															@endif
                                                       
                                                        <!--<div class="outbound_stop_popup" id="outBoundStopInfo_0" style="display: none;">-->
                                                        <!--<a class="fl_close ipad_close_btn" id="outBoundStopInfoClose_0" href="#"></a>-->
                                                            
                                                        <!--        <span class="fleft width_100per">-->
                                                        <!--            <em>SG-253 A</em>-->
                                                        <!--            <em> &nbsp;&nbsp; DEL(18:20)</em>-->
                                                        <!--            <code> →</code> -->
                                                                                             
                                                        <!--            <em>CCU(20:30)</em>-->
                                                        <!--        </span>                                           -->
                                                            
                                                        <!--        <span class="fleft width_100per">-->
                                                        <!--            <em>SG-83 A</em>-->
                                                        <!--            <em> &nbsp;&nbsp; CCU(00:05)</em>-->
                                                        <!--            <code> →</code> -->
                                                                                             
                                                        <!--            <em>BKK(04:10)</em>-->
                                                        <!--        </span>                                           -->
                                                            
                                                        <!--</div>-->
                                                    
                                                </span>
                                            </div>
                                            <div class="fleft pad_em width_98">
                                          
                                                <span class="spl_icon_flight">
                                                    <span class="fleft width_98">
                                                        <?php   $dirname = "assets/home/images/AirlineLogo";
														$filename = glob("$dirname/*{$fd->Segments[1][0]->Airline->AirlineCode}*", GLOB_BRACE); ?>
														@if(isset($filename) && count($filename)>0)
														<img src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" class="fleft mr5">
														@endif
                                                        <code id="Code_nm_mob" class="font10 fleft lh_10 width_64 m_width_100"><em class="mobile_not"> {{$fd->Segments[1][0]->Airline->AirlineName}}</em> 
                                                        <em class="fleft width_100 font10"><span class="fleft mr5 lh10" id="AirlineDetailsInBound_0">{{$fd->Segments[1][0]->Airline->AirlineCode}} - {{$fd->Segments[1][0]->Airline->FlightNumber}}</span>
                                                            <em class="mobile_class" id="Em1" title="Flight Booking Class"><dfn class="class_re">Class -</dfn> {{$fd->Segments[1][0]->Airline->FareClass}}</em>
                                                        </em>                                                           
                                                        </code>
                                                    </span>
                                                </span>
                                            
                                                <span class="spl_duration_flight">
                                                    <span class="fleft width_100per pad_8">
                                                            <em id="departureIn_0">{{$fd->Segments[1][0]->Origin->Airport->AirportCode}}<tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[1][0]->Origin->DepTime))}})</tt></em>
                                                                <?php $count_seg=count($fd->Segments[1]); ?>
																@if($count_seg>1)
																    @foreach($fd->Segments[1] as $intfl)
                                                                        <code> →</code> 
        															    <dfn id="SegIN_0_0">{{$intfl->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrIn_0_0" value="CCU"></dfn>
        															      
                                                                        <input type="hidden" id="destAirport_0" value="BKK">
                                                                    @endforeach
                                                                    <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[1][$count_seg-1]->Destination->ArrTime))}})</tt>
															    @else
    																<code> →</code> <em class="txt_aln_rt" id="arriavalIn_0"> {{$fd->Segments[1][0]->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrIn_0_1" value="DEL"> <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[1][0]->Destination->ArrTime))}})</tt></em>
                                                                
                                                                    <input type="hidden" id="destAirport_0" value="DEL">
                                                                @endif
                                                    </span>  
                                               
                                                </span>
                                                <span class="spl_dur_flight mobile_not" id="spldurationboxinbound_0">
                                                    <span class="fleft width_100 pad_8 pad_top_0 pad_bottom_0">
                                                        <tt id="inBoundDuration_0">
                                                             <?php $datetimed = new DateTime($fd->Segments[1][0]->StopPointDepartureTime);
                                                                $datetimea = new DateTime($fd->Segments[1][$count_seg-1]->StopPointArrivalTime);
                                                                $interval = $datetimed->diff($datetimea);
                                                                echo $interval->format('%h')."h ".$interval->format('%i')."m"; ?>
                                                            
                                                        </tt><br>
                                                        
                                                           
                                                            <a class="stop_1" href="#!" id="inBoundStops_0">
                                                                <small>
                                                                      @if(count($fd->Segments[1])==1)
                                                                            Non Stop
                                                                           @else
                                                                            {{(count($fd->Segments[1])-1)." Stop" }}
                                                                           @endif
                                                                </small></a>
                                                        
                                                    </span>
                                                                                           
                                                           <br>
                                                               
                                                               @if(isset($fd->Segments[0][0]->NoOfSeatAvailable))
														   <code class="red">{{$fd->Segments[0][0]->NoOfSeatAvailable}} seat(s) left</code> 
														   @else
														   <code>{{"no seats available"}}</code>
															@endif
                                                                                                
                                                       
                                                        <!--<div class="inbound_stop_popup" id="inBoundStopInfo_0" style="display: none;">-->
                                                        <!--   <a class="fl_close ipad_close_btn" id="inBoundStopInfoClose_0" href="#"></a>-->
                                                            
                                                        <!--        <span class="fleft width_100per">-->
                                                        <!--            <em>SG-84 A</em>-->
                                                        <!--            <em> &nbsp;&nbsp; BKK(05:10)</em>-->
                                                        <!--            <code> →</code> -->
                                                                                                 
                                                        <!--            <em>CCU(06:10)</em>-->
                                                        <!--        </span>-->
                                                
                                                            
                                                        <!--        <span class="fleft width_100per">-->
                                                        <!--            <em>SG-264 A</em>-->
                                                        <!--            <em> &nbsp;&nbsp; CCU(20:50)</em>-->
                                                        <!--            <code> →</code> -->
                                                                                                 
                                                        <!--            <em>DEL(23:25)</em>-->
                                                        <!--        </span>-->
                                                
                                                            
                                                        <!--</div>-->
                                                    
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <?php $basefare = $fd->FareBreakdown[0]->BaseFare;
                                                    $tax = $fd->FareBreakdown[0]->Tax;
                                                    
                                                    $other_charges = $fd->Fare->OtherCharges;
                                                    $adult_count = $fd->FareBreakdown[0]->PassengerCount;
                                                    
                                                    if(isset($fd->FareBreakdown[1]))
                                                    $child_count = $fd->FareBreakdown[1]->PassengerCount;
                                                    else
                                                    $child_count=0;
                                                    
                                                    $other_charges_per_adult =$other_charges/($adult_count+$child_count); 
                                                    $pubfare_per_adult = ($basefare+$tax)/$adult_count+$other_charges_per_adult;
                                                    $offer_on_pubfare = $fd->Fare->CommissionEarned+$fd->Fare->PLBEarned+$fd->Fare->IncentiveEarned;
                                                    $offer_on_pubfare_per_adult = $offer_on_pubfare/($adult_count+$child_count);
                                                    $offeredfare_per_adult = $pubfare_per_adult-$offer_on_pubfare_per_adult;
                                                    
                                                   //$fare_per_adult = ($basefare+$tax)/$adult_count;
                                             ?>
                                        <span class="spl_price mobile_not" id="pub_fare_0">
                                            <span class="fleft Width_100 m_float_rt" id="pubPricespan_0" style="visibility:visible;">
                                                <tt>Rs. </tt>
                                                <tt id="PubPrice_0">
                                                    {{" ".number_format(money_format('%!i',($pubfare_per_adult)),2)}}
                                                </tt>
                                                
                                            </span> 
                                            <span class="icon_flight2 mobile_not">
                                                <a class="ruppe" href="#" id="FareRule_0"><span class="drop2"> Fare Rule</span></a>
                                                
                                                    <a class="close cur_default" href="#"><span class="drop2" id="isRefundable_0">Lowest Cancellation</span></a>
                                                   
                                                 
                                            </span>  
                                        </span>

                                        <span class="spl_price mobile_not" id="OfferPriceSpan_0" style="visibility:visible;">
                                            <span>Rs.</span>
                                            <span id="OfferPrice_0">
                                             {{" ".number_format(money_format('%!i',($offeredfare_per_adult)),2)}}
                                            </span>
                                            
                                        </span>
                                       
                                       <span class="book_now_emails">
                                        <span class="book_now_btn mt5"><a href="{{route('frontend.flight_passenger_details',['TraceId' =>$flights->Response->TraceId ,'ResultIndex'=>$fd->ResultIndex])}}" id="btnBookNow_0" class="btn_bg">Book Now</a></span>
                                       
                                       </span>
                        </div>
                                        <!--<span class="spl_email spl_pt15">-->
                                        <!--    <input type="checkbox" id="chkEmailItinerary_0" name="emailItinerary" value="0"> Email-->
                                        <!--</span>-->
                                         

                                        <div class="fare_rule_pop" id="FareRuleBlock_0" style="display: none;">
                                            <div class="head_pop" id="FareRuleHead">
                                                <span id="FareRuleHeadTitle">Fare Rules</span>
                                                <a href="#" id="btnHeadFareRuleBlockClose_0" class="fl_close"></a>
                                            </div>
                                            <div id="loadingMsg_0">
                                            </div>
                                            <div class="fare_footer" id="FareRuleFoot">
                                                <span class="close_btn cursor">
                                                    <input type="button" value="Close" id="btnFareRuleBlockClose_0">
                                                </span>
                                            </div>
                                        </div>

                                        
                                                    
                                                
                                        
                                        <div class="stop_1 fleft relative width_100">    
                                            <!--<div id="Segmetbaggage_FreeBaggage_0" class="bag_operatedBy_popup mobile_not" style="display: none;">-->
                                           
                                       
                                            <!--        <span class="fleft width_100">-->
                                            <!--            <em id="departure_0">-->
                                            <!--            DEL  <input type="hidden" id="originApt_0" value="DEL"> <tt class="hightlight bold" id="DeptTimeAirline_0">(18:20)</tt></em> -->
                                            <!--            <span>→</span>-->
                                            <!--            <em> CCU<tt class="hightlight bold" id="ArrTimeAirline_0">(20:30)</tt>-->
                                            <!--               15 Kg-->
                                            <!--            </em>-->
                                                              
                                               
                                            <!--        </span> -->
                                       
                                            <!--        <span class="fleft width_100">-->
                                            <!--            <em id="fltdept_0_1">-->
                                            <!--            CCU  <tt class="hightlight bold" id="DeptTimeAirline_0">(00:05)</tt></em> -->
                                            <!--            <span>→</span>-->
                                            <!--            <em> BKK<tt class="hightlight bold" id="ArrTimeAirline_0">(04:10)</tt>-->
                                            <!--               15 Kg-->
                                            <!--            </em>-->
                                                              
                                               
                                            <!--        </span> -->
                                       
                                            <!--        <span class="fleft width_100">-->
                                            <!--            <em id="fltdept_0_2">-->
                                            <!--            BKK  <tt class="hightlight bold" id="DeptTimeAirline_0">(05:10)</tt></em> -->
                                            <!--            <span>→</span>-->
                                            <!--            <em> CCU<tt class="hightlight bold" id="ArrTimeAirline_0">(06:10)</tt>-->
                                            <!--               15 Kg-->
                                            <!--            </em>-->
                                                              
                                               
                                            <!--        </span> -->
                                       
                                            <!--        <span class="fleft width_100">-->
                                            <!--            <em id="fltdept_0_3">-->
                                            <!--            CCU  <tt class="hightlight bold" id="DeptTimeAirline_0">(20:50)</tt></em> -->
                                            <!--            <span>→</span>-->
                                            <!--            <em id="arrival_0"> DEL<tt class="hightlight bold" id="ArrTimeAirline_0">(23:25)</tt>-->
                                            <!--               15 Kg-->
                                            <!--            </em>-->
                                                              
                                               
                                            <!--        </span> -->
                                             
                                            <!--      </div>-->
                                        </div>
                                         
                                            

                                    </div>
									@endforeach
									@endif
        </div>
      </div>