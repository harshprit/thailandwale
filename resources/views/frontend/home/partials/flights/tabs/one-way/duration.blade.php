<div class="tab-pane container-fluid active" id="1">
<?php  $i=1;$j=1; ?>
@foreach($flight_result as $fd)
             <div class="row row-pad-remove">
              <div class="col-md-12 col-xs-12 listing-page-tab">
                
                <div class="col-md-2 col-xs-3">
                  
                   <?php   $dirname = "assets/home/images/AirlineLogo";
                    $filename = glob("$dirname/*{$fd->AirlineCode}*", GLOB_BRACE); ?>
                    @if(isset($filename) && count($filename)>0)
                    <img src="{{ asset($filename[0])}}" class="img-responsive" style="width: 50%">
                    @endif
                 <a class="flight-listname" href="#"> 
                    <p>
                    
                        
                          {{$fd->Segments[0][0]->Airline->AirlineName}}
                        
                    
                    </p>
                    <span>
                        
                                   {{$fd->Segments[0][0]->Airline->AirlineCode}}-{{$fd->Segments[0][0]->Airline->FlightNumber}}
                              
                    </span>
                 </a>
                </div>
                <div class="col-md-2 col-xs-3">
                  <h3 class="flightlist-time"><a href="#"> 
                    
                           @php 
                                $dDatetime=strtotime($fd->Segments[0][0]->Origin->DepTime);
                                $dtime = date("H:i",$dDatetime);
                           @endphp
                           {{ $dtime }}
                    
                  </a></h3>
                  <h4 class="flightlist-cities"> 
                    <a href="#">
                         
                                {{$fd->Segments[0][0]->Origin->Airport->CityName}}
                            
                       
                    </a>
                  </h4>
                </div>
                <div class="col-md-2 col-xs-3">
                  <h3 class="flightlist-time"> <a href="#">
                   
                            @php
                                $aDatetime=strtotime($fd->Segments[0][count($fd->Segments[0])-1]->Destination->ArrTime);
                                $atime = date("H:i",$aDatetime);
                           @endphp
                           {{ $atime }}
                        
                  </a></h3>
                  <h4 class="flightlist-cities">
                      <a href="#">
                         
                                {{$fd->Segments[0][count($fd->Segments[0])-1]->Destination->Airport->CityName}}
                            
                       
                      </a>
                  </h4>
                </div>
                 <div class="col-md-2 col-xs-3">
                  <h3 class="flightlist-time"> <a href="#">
                       <?php $datetimed = new DateTime($fd->Segments[0][0]->Origin->DepTime);
                            $datetimea = new DateTime($fd->Segments[0][count($fd->Segments[0])-1]->Destination->ArrTime);
                            $interval = $datetimed->diff($datetimea);
                            echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?>
                      
                  </a></h3>
                  <h4 class="flightlist-cities">
                     
                           @if(count($fd->Segments[0])==1)
                            Non Stop
                           @else
                           <div class="tooltip">
                            {{(count($fd->Segments[0])-1)." Stop" }}
                            
                            <span class="tooltiptext">
                                @for($stops=0;$stops<count($fd->Segments[0]);$stops++)
                                <span class="tooltip-text">
                           {{ date("H:i",strtotime($fd->Segments[0][$stops]->Origin->DepTime)) }}
                                {{$fd->Segments[0][$stops]->Origin->Airport->CityName}}
                                {{ date("H:i",strtotime($fd->Segments[0][$stops]->Destination->ArrTime)) }}
                        
                                {{$fd->Segments[0][$stops]->Destination->Airport->CityName}}</span> @endfor</span></div></h4>
                           
                           @endif
                           
                           
                </div>
                <div class="col-md-2 col-xs-5" style="text-align: right;">
                 <h3 class="flightlist-price"> Fare: <span class="flightlist-person">(per person)</span>  <a href="#"> <i class="fa fa-inr" aria-hidden="true"></i><span>
                     <?php $basefare = $fd->FareBreakdown[0]->BaseFare;
                            $tax = $fd->FareBreakdown[0]->Tax;
                            $other_charges = $fd->Fare->OtherCharges;
                            $adult_count = $fd->FareBreakdown[0]->PassengerCount;
                            
                            if(isset($fd->FareBreakdown[1]))
                            $child_count = $fd->FareBreakdown[1]->PassengerCount;
                            else
                            $child_count=0;
                            
                            $other_charges_per_adult =$other_charges/($adult_count+$child_count); 
                             $pubfare_per_adult = ($basefare+$tax)/($adult_count)+$other_charges_per_adult;
                            //$pubfare_per_adult = ($basefare+$tax)/($adult_count+$child_count);
                            $offer_on_pubfare = $fd->Fare->CommissionEarned+$fd->Fare->PLBEarned+$fd->Fare->IncentiveEarned;
                            $offer_on_pubfare_per_adult = $offer_on_pubfare/($adult_count+$child_count);
                            $offeredfare_per_adult = $pubfare_per_adult-$offer_on_pubfare_per_adult;
                            
                     ?>
                     {{" ".number_format(round($offeredfare_per_adult,0))}}</span></a></h3>
                </div>
                <div class="col-md-2 col-xs-5">
                     <a class="flightlist-bookbtn" href="{{route('frontend.flight_passenger_details',['TraceId' =>$flights->Response->TraceId ,'ResultIndex'=>$fd->ResultIndex])}}" class="btn btn-danger btn-lg">BOOK NOW</a>
                </div>
                <div class="col-md-12 col-xs-12 hlcol">
<div class="row row-pad-remove hlrow">
    <div class="col-sm-12 hlcol">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
       
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{$i}}">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}" aria-expanded="false" aria-controls="collapse{{$i}}">
                            <i class="more-less glyphicon glyphicon-chevron-down"></i>
                           Flights Details
                        </a>
                    </h4>
                </div>
                <div id="collapse{{$i}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-xs-12">
                            
                             <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
                                        <li class="active">
                                            <?php $k=$j; ?>
                                            <a href="#tab_default_{{$j++}}" data-toggle="tab">Flights Details </a>
                                        </li>
                                        <li>
                                          <a href="#tab_default_{{$j++}}" data-toggle="tab">
                                          Fare Details </a>
                                        </li>
                                        <li>
                                          <a href="#tab_default_{{$j++}}" data-toggle="tab">
                                          Baggage Details </a>
                                        </li>
                                        <li>
                                          <a href="#tab_default_{{$j++}}" data-toggle="tab">
                                          Cancellation Fee </a>
                                        </li>
                                        <li>
                                          <a href="#tab_default_{{$j++}}" data-toggle="tab">
                                          Date Change Fee </a>
                                        </li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        <!-- Flight details -->
                                         <div class="tab-pane active container-fluid" id="tab_default_{{$k++}}">
                                                <?php $flight_start_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointDepartureTime; 
                                                 $start = date("j M Y, D", strtotime($flight_start_date));
                                                 ?>
                                               <div class="row" style="margin-top:20px; font-size:15px;"> 
                                               <span class="flightlist-dddate">{{$start}}</span>
                                            <div class="col-md-2 col-xs-3">
                                              
                                               <?php   $dirname = "assets/home/images/AirlineLogo";
                                                $filename = glob("$dirname/*{$fd->AirlineCode}*", GLOB_BRACE); ?>
                                                @if(isset($filename) && count($filename)>0)
                                                <img src="{{ asset($filename[0])}}" class="img-responsive" style="width: 50%">
                                                @endif
                                             <a class="flight-listname" href="#"> 
                                                <p>
                                                @foreach($fd->Segments as $segment)
                                                    @foreach($segment as $sg)
                                                      {{$sg->Airline->AirlineName}}
                                                    @endforeach
                                                @endforeach
                                                </p>
                                                <span>
                                                    @foreach($fd->Segments as $sg)
                                                            @foreach($sg as $seg)
                                                               {{$seg->Airline->AirlineCode}}-{{$seg->Airline->FlightNumber}}
                                                            @endforeach
                                                        @endforeach
                                                </span>
                                             </a>
                                            </div>
                                            <div class="col-md-2 col-xs-3">
                                              <h3 class="flightlist-time"><a href="#"> 
                                                @foreach($fd->Segments as $sg)
                                                    @foreach($sg as $time)
                                                       <?php 
                                                            $dDatetime=strtotime($time->StopPointDepartureTime);
                                                            $dtime = date("H:i",$dDatetime);
                                                       ?>
                                                       {{ $dtime }}
                                                    @endforeach
                                                @endforeach
                                              </a></h3>
                                              <h4 class="flightlist-cities"> 
                                                <a href="#">
                                                     @foreach($fd->Segments as $segment)
                                                        @foreach($segment as $sg)
                                                            {{$sg->Origin->Airport->CityName}}
                                                        @endforeach
                                                     @endforeach
                                                   
                                                </a>
                                              </h4>
                                            </div>
                                            <div class="col-md-2 col-xs-3">
                                              <h3 class="flightlist-time"> <a href="#">
                                               @foreach($fd->Segments as $sg)
                                                    @foreach($sg as $time)
                                                        <?php 
                                                            $aDatetime=strtotime($time->StopPointArrivalTime);
                                                            $atime = date("H:i",$aDatetime);
                                                       ?>
                                                       {{ $atime }}
                                                    @endforeach
                                                @endforeach
                                              </a></h3>
                                              <h4 class="flightlist-cities">
                                                  <a href="#">
                                                     @foreach($fd->Segments as $segment)
                                                        @foreach($segment as $sg)
                                                            {{$sg->Destination->Airport->CityName}}
                                                        @endforeach
                                                     @endforeach 
                                                   
                                                  </a>
                                              </h4>
                                            </div>
                                             <div class="col-md-2 col-xs-3">
                                              <h3 class="flightlist-time"> <a href="#">
                                                   <?php $datetimed = new DateTime($time->StopPointDepartureTime);
                                                        $datetimea = new DateTime($time->StopPointArrivalTime);
                                                        $interval = $datetimed->diff($datetimea);
                                                        echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?>
                                                  
                                              </a></h3>
                                              <h4 class="flightlist-cities"><a href="#"> 
                                              @foreach($fd->Segments as $sg)
                                                    @foreach($sg as $seg)
                                                       @if($seg->StopPoint=="")
                                                        Non Stop
                                                       @else
                                                        {{$seg->StopPoint." Stop" }}
                                                       @endif
                                                    @endforeach
                                                @endforeach
                                              </a></h4>
                                            </div>
                                            <div class="col-md-2 col-xs-4">
                                                <span class="partially-refundable">Partially Refundable</span> 
                                            <a class="flightlist-class" href="#">
                                                @foreach($cabinClass as $key=>$iv) 
                                                    @if($search_info['Segments'][0]['FlightCabinClass']==$key)
                                                        {{$iv}}
                                                    @endif
                                                @endforeach
                                            </a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Flight details end -->
                                       
                                        <div class="tab-pane" id="tab_default_{{$k++}}">
                                          <table class="table table-responsive">
                                              <thead>
                                                  <tr>
                                                  <th colspan="2">Fare Breakup</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>Base Fare (
                                                      {{$fd->FareBreakdown[0]->PassengerCount." Adult"}},
                                                      @if(isset($fd->FareBreakdown[1]->PassengerCount))
                                                      {{$fd->FareBreakdown[1]->PassengerCount." Children"}},
                                                      @endif
                                                      @if(isset($fd->FareBreakdown[2]->PassengerCount))
                                                      {{$fd->FareBreakdown[2]->PassengerCount." Infant"}}
                                                      @endif
                                                      )</td>
                                                      <td><i class="fa fa-inr" aria-hidden="true"></i><span>{{" ".number_format(round($fd->Fare->BaseFare,0))}} </span></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Fees & Surcharge</td>
                                                      <?php $fee=($fd->Fare->Tax)+($fd->Fare->OtherCharges)-$offer_on_pubfare; ?>
                                                      <td><i class="fa fa-inr" aria-hidden="true"></i><span>{{" ".number_format(round($fee,0))}} </span></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Total</td>
                                                      <?php $total = $fee+($fd->Fare->BaseFare); ?>
                                                      <td><i class="fa fa-inr" aria-hidden="true"></i><span>{{" ".number_format(round($total,0))}} </span></td>
                                                  </tr>
                                              </tbody>
                                              <tfoot>
                                                  <tr>
                                                      <td colspan="2">
                                                          Note: <span class="partially-refundable">Partially Refundable</span>
                                                      </td>
                                                  </tr>
                                              </tfoot>
                                          </table>
                                        </div>
                                        <div class="tab-pane" id="tab_default_{{$k++}}">
                                          <table class="table table-responsive">
                                              <thead>
                                                  <tr>
                                                  <th>{{$flights->Response->Origin}} - {{$flights->Response->Destination}}</th>
                                                  <th> Cabin </th>
                                                  <th> Checkin </th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>Adult</td>
                                                      <td><span>
                                                        @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                @if(isset($sg->CabinBaggage)&& $sg->CabinBaggage!="")
                                                                {{$sg->CabinBaggage}}
                                                                @else
                                                                    {{"-"}}
                                                                @endif
                                                            
                                                            @endforeach
                                                        @endforeach 
                                                     </span></td>
                                                      <td>
                                                          <span>
                                                          @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                @if(isset($sg->Baggage)&& $sg->Baggage!="")
                                                                {{$sg->Baggage}}
                                                                @else
                                                                    {{"-"}}
                                                                @endif
                                                            @endforeach
                                                        @endforeach 
                                                        </span>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Child</td>
                                                     <td><span>
                                                         @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                @if(isset($sg->CabinBaggage)&& $sg->CabinBaggage!="")
                                                                {{$sg->CabinBaggage}}
                                                                @else
                                                                    {{"-"}}
                                                                @endif
                                                            
                                                            @endforeach
                                                        @endforeach 
                                                     </span></td>
                                                      <td>
                                                          <span>
                                                          @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                @if(isset($sg->Baggage)&& $sg->Baggage!="")
                                                                {{$sg->Baggage}}
                                                                @else
                                                                    {{"-"}}
                                                                @endif
                                                            @endforeach
                                                        @endforeach 
                                                        </span>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Infant</td>
                                                     
                                                      <td><span>0 Kgs</span></td>
                                                      <td><span>0 Kgs</span></td>
                                                  </tr>
                                              </tbody>
                                              
                                          </table>
                                        </div>
                                            <div class="tab-pane" id="tab_default_{{$k++}}">
                                          <table class="table table-responsive">
                                              <thead>
                                                  <tr>
                                                  <th>{{$flights->Response->Origin}} - {{$flights->Response->Destination}}</th>
                                                  <th> per passenger fee </th>
                                                  
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>24 hrs-365 days</td>
                                                      <td><span>
                                                        @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                {{$sg->CabinBaggage}}
                                                            @endforeach
                                                        @endforeach 
                                                     </span></td>
                                                      
                                                  </tr>
                                                  <tr>
                                                      <td>0-24 hrs</td>
                                                     <td><span>
                                                        @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                {{$sg->CabinBaggage}}
                                                            @endforeach
                                                        @endforeach 
                                                     </span></td>
                                                     
                                                  </tr>
                                                
                                              </tbody>
                                              
                                          </table>
                                        </div>
                                            <div class="tab-pane" id="tab_default_{{$k++}}">
                                          <table class="table table-responsive">
                                              <thead>
                                                  <tr>
                                                  <th>{{$flights->Response->Origin}} - {{$flights->Response->Destination}}</th>
                                                  <th> per passenger fee </th>
                                                 
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>24 hrs-365 days</td>
                                                      <td><span>
                                                        <i class="fa fa-inr" aria-hidden="true"></i><span>{{" ".number_format(round($fd->Fare->BaseFare,0))}} + 300*</span>
                                                     </span></td>
                                                      
                                                  </tr>
                                                  <tr>
                                                      <td>0-24 hrs</td>
                                                     <td><span>
                                                       Non-Changeable
                                                     </span></td>
                                                      
                                                  </tr>
                                                 
                                              </tbody>
                                              
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div><!-- panel-group -->
    </div>
</div>
              </div>
            </div>

             </div>
             <?php $i++; ?>
@endforeach

           
             
          </div>