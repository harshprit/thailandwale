@if(isset($package_hotels))
	@foreach($package_hotels as $hotels)
            @if($hotels->ResponseStatus==1)
            @foreach($hotels->HotelResults as $hotel)
            @if($hotel->HotelCode==$hotelCode && $hotel->ResultIndex==$resultIndex)
<h5 class="theme-search-results-item-title theme-search-results-item-title-lg">{{$hotel->HotelName}}</h5>
										  <input type="hidden" name="HotelTraceId[]" value="{{$hotels->TraceId}}">
										  <input type="hidden" name="HotelResultIndex[]" value="{{$hotel->ResultIndex}}">
										  <input type="hidden" name="HotelCode[]" value="{{$hotel->HotelCode}}">
                                             
                                          <div class="theme-search-results-item-rating">
                                             <ul class="theme-search-results-item-rating-stars">
                                                
												<?php $star=$hotel->StarRating; for($k=0;$k<5;$k++){?>
												@if($k<$star)
                                                <li class="active">
                                                   <i class="fa fa-star" style="color:unset"></i>
                                                </li>
                                                @else
                                                    <li>
                                                        <i class="fa fa-star" style="color:unset"></i>
                                                    </li>
                                                @endif
                                                <?php }?>
                                            </ul>
                                             
                                          </div>
                                          <ul class="theme-search-results-item-room-feature-list">
                                          
                                             <li>
                                                <i class="fa fa-map-marker theme-search-results-item-room-feature-list-icon"></i>
                                                <span class="theme-search-results-item-room-feature-list-title">{{$hotel->HotelAddress}}</span>
                                             </li>
                                          </ul>
                                          <p class="theme-search-results-item-location">
                                          <h5 class="theme-sidebar-section-title mt20">Booking Summary</h5>
                                          <ul class="theme-sidebar-section-summary-list">
                                             <li><i class="fa fa-users"></i>
                                                 @php
                                                if(isset($hotels->RoomGuests)){
                                                    if($hotels->RoomGuests[0]->NoOfAdults>0)
                                                    {
                                                        if($hotels->RoomGuests[0]->NoOfChild>0)
                                                        {
                                                            $sum=$hotels->RoomGuests[0]->NoOfAdults+$hotels->RoomGuests[0]->NoOfChild;                                                                
                                                        } 
                                                        else{
                                                            $sum = $hotels->RoomGuests[0]->NoOfAdults;                                                            
                                                        }                                   
                                                        
                                                    }                                                                  
                                                }
                                                
                                                @endphp
                                                {{$sum}} Guest, 
                                                @php
                                                    $in = new DateTime($hotels->CheckInDate);
                                                    $out = new DateTime($hotels->CheckOutDate);
                                                    $no_of_nights = $in->diff($out);
                                                @endphp
                                                {{$no_of_nights->format("%a")}} Nights &nbsp;
                                                
                                                <i class="fa fa-user"></i>
                                                @php
                                                 if(isset($hotels->RoomGuests)){
                                                    if($hotels->RoomGuests[0]->NoOfAdults>0)
                                                    {
                                                        if($hotels->RoomGuests[0]->NoOfChild>0)
                                                        {
                                                            echo $hotels->RoomGuests[0]->NoOfAdults." Adults, ".$hotels->RoomGuests[0]->NoOfChild." Child";                                                                
                                                        } 
                                                        else{
                                                            echo $hotels->RoomGuests[0]->NoOfAdults." Adults";                                                            
                                                        }                                   
                                                        
                                                    }                                                                  
                                                }
                                                @endphp
                                             </li>
                                             <li><i class="fa fa-calendar"></i> Check-in: {{date('D,F d,Y',strtotime($hotels->CheckInDate))}}</li>
                                             <li><i class="fa fa-calendar"></i> Check-out: {{date('D,F d,Y',strtotime($hotels->CheckOutDate))}}</li>
                                          </ul>
                                          </p>
                                          <div class="col-md-12">
                                          <div class="theme-search-results-item-img-wrap">
                                             <img class="theme-search-results-item-img" src="{{$hotel->HotelPicture}}" alt="Image Alternative text" title="Image Title" style="width: 100%;" />
                                          </div>
                                       </div>
                        @endif
               @endforeach                           
            @endif
            @endforeach
            
        @endif