  <div>
      <div>
        <div>
          <div class="row">
          <div class="col-md-2 col-xs-2">
            <img src="{{asset('assets/home/images/59878-200.png')}}" style="width:36%;">
          </div>
            <div class="col-md-10 col-xs-10">
                <?php $flight_origin=$flights->Response->Results[0][0]->Segments[0][0]->Origin->Airport->CityName; ?>
                <?php $flight_dest=$flights->Response->Results[0][0]->Segments[0][count($flights->Response->Results[0][0]->Segments[0])-1]->Destination->Airport->CityName;?> 
                 <?php $flight_start_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointDepartureTime; 
                 $start = date("D, j M, Y", strtotime($flight_start_date));
                 ?>
                 
                 <?php $flight_end_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointArrivalTime; ?>
                <strong>{{$flight_origin}} to {{$flight_dest}}</strong> - <span>{{$start}}</span>
              <h6><span id="total_flights"></span></h6>
           
          </div>
        </div>
            <div class="tabbable-panel">
        <div class="tabbable-line">
          <!--<ul class="nav nav-tabs ">-->

          <!--  <li class="active">-->
          <!--    <a href="#1" data-toggle="tab">-->
          <!--    DURATION </a>-->
          <!--  </li>-->

          <!--  <li>-->
          <!--    <a href="#2" data-toggle="tab">-->
          <!--    DEPARTURE </a>-->
          <!--  </li>-->
          <!--  <li>-->
          <!--    <a href="#3" data-toggle="tab">-->
          <!--   ARRIVAL </a>-->
          <!--  </li>-->
          <!--     <li>-->
          <!--    <a href="#4" data-toggle="tab">-->
          <!--   PRICE </a>-->
          <!--  </li>-->
          <!--</ul>-->

          <div class="tab-content">
           
            {!!view('frontend.home.partials.flights.tabs.one-way.duration',compact('flight_result','flights','cabinClass','search_info'))!!}

        </div>
      </div>
        </div>
  
      </div>
    </div>
  </div>