@foreach($flights->Response->Results[0] as $flight)
@if($flight->ResultIndex==$resultIndex)
										  <input type="hidden" name="FlightTraceId" value="{{$flights->Response->TraceId}}">
										  <input type="hidden" name="FlightResultIndex" value="{{$flight->ResultIndex}}">
                                            
                                           @php $count_fpd = 1; @endphp
								  @foreach($flight->Segments as $seg)
                              <div class="theme-payment-page-sections-item">
                                 <div class="theme-search-results-item theme-payment-page-item-thumb">
                                    <div class="row" data-gutter="20">
                                       <div class="col-md-9 ">
                                          <p class="theme-search-results-item-flight-payment-airline">You are flying {{$seg[0]->Airline->AirlineName}}</p>
                                          <h5 class="theme-search-results-item-title">{{$seg[0]->Origin->Airport->CityName}}, {{$seg[0]->Origin->Airport->CityCode}} &nbsp;&rarr;&nbsp; {{$seg[(count($seg))-1]->Destination->Airport->CityName}}, {{$seg[(count($seg))-1]->Destination->Airport->CityCode}}</h5>
                                          
                                          <a class="theme-search-results-item-flight-payment-details-link flight_details " >Flight Details &nbsp;
                                          <i class="fa fa-angle-down"></i>
                                          </a>
                                          <!-- popup hover -->
                                          <div class="flight_tipTip_cust_card" tabindex="{{$count_fpd++}}" style="display:none">
                                            <span class="flight_tiptip_arrow"></span>
                                            <span class="clearFix flight_details_tooltip">
                                            <!--<span class="modal_heading append_bottom alC width100 spanCenterAligned">-->
                                            <!--    Flight details-->
                                            <!--</span>-->
                                        <span class="clearFix">
                                            <span></span>
                                            <span class="clearFix mg-bot-20">
                                            <span class="rvw_flght_logo flL">
                                            <span class="block append_bottom3">
                                            <span><span class="flL append_bottom3 flight_logo " name="6E" style="background-position: 0px -1px;"></span>
                                            <span class="flL holC_mgr10" name="6E">
                                            <i class="fa fa-plane" style="color: #fff !important;font-size: 25px;"></i>
                                            </span></span></span></span>
                                            <span class="flL r_flght_sectn">
                                                <span class="clearFix mg-bot-10 cabin_bagage">
                                                    <span class="rvw_flght_terminal flL">
                                                        <span class="block fontSize13 append_bottom3">
                                                            <span class="light_gray">
                                                                {{$seg[0]->Origin->Airport->CityCode}}
                                                                <!-- CCU  -->
                                                            </span><strong>
                                                                    @php 
                                                                        $aDatetime = strtotime($seg[0]->StopPointArrivalTime); 
                                                                        $atime = date("H:i",$aDatetime);
                                                                        
                                                                        $dDatetime = strtotime($seg[(count($seg))-1]->StopPointDepartureTime); 
                                                                        $dtime = date("H:i",$dDatetime);
                                                                    
                                                                    @endphp
                                                                    {{$atime}}
                                                                    <!-- 21:45 -->
                                                                </strong>
                                                                </span></span><span class="rvw_flght_time flight_details_open flL">
                                                                <span class="ybbhbc back_brdr"><span class="fontSize11 ybbhbc">
                                                                    @php 
                                                                        $datetimed = new DateTime($seg[(count($seg))-1]->StopPointArrivalTime);
                                                                        $datetimea = new DateTime($seg[0]->StopPointDepartureTime);
                                                                        $interval = $datetimed->diff($datetimea);
                                                                        echo $interval->format('%h')."H ".$interval->format('%i')."m";
                                                                    @endphp
                                                                    <!-- 2h 35m -->
                                                                </span>
                                                                <span class="small_arrow"></span></span><span class="fontSize9 ybbhbc">
                                                                    {{$seg[0]->Origin->Airport->CityCode}}-{{$seg[(count($seg))-1]->Destination->Airport->CityCode}}
                                                                    <!-- CCU-DEL -->
                                                                </span>
                                                                </span><span class="rvw_flght_terminal flL last"><span class="block fontSize13 append_bottom3">
                                                                <span class="light_gray">
                                                                    {{$seg[(count($seg))-1]->Destination->Airport->CityCode}}
                                                                    <!-- DEL  -->
                                                                </span><strong>
                                                                    
                                                                    {{$dtime}}
                                                                    <!-- 00:20 -->
                                                                </strong></span></span></span>
                                                                <span class="fontSize11 light_gray flL">
                                                                    <span>
                                                                        {{$seg[0]->Airline->AirlineName." | ".$seg[0]->Airline->AirlineCode."-".$seg[0]->Airline->FlightNumber}}
                                                                        <!-- IndiGo | 6E-224 -->
                                                                    </span>
                                                                <span id="cabinInfo">| Cabin:
                                                                @php $fareclass =$seg[0]->Airline->FareClass;  @endphp 
                                                                @if(in_array($fareclass,array("K","L","Q","V","W","U","T","X","N","O","S")))
                                                                    {{"Discounted Economy"}}
                                                                @elseif(in_array($fareclass,array("Y","B","M","H")))
                                                                    {{"Economy"}}
                                                                @elseif(in_array($fareclass,array("W","E")))
                                                                    {{"Premium Economy"}}
                                                                @elseif(in_array($fareclass,array("D","I","Z")))
                                                                    {{"Discounted Business"}}
                                                                @elseif(in_array($fareclass,array("J","C","D")))
                                                                    {{"Business"}}
                                                                @elseif(in_array($fareclass,array("A","F")))
                                                                    {{"First"}}
                                                                @endif
                                                                </span></span></span></span></span>
                                                                <span class="clearFix"><span>
                                                                <span class="layover_tooltip ybbhbc mg-bot-20 clearFix">
                                                                    
                                                                    @if(count($seg)>1)
                                                                        Plane change at 
                                                                        {{$seg[0]->Destination->Airport->CityName}}
                                                                        <!-- Kolkata -->
                                                                        Waiting: 
                                                                     @php 
                                                                        $datetimed = new DateTime($seg[(count($seg))-1]->StopPointDepartureTime);
                                                                        $datetimea = new DateTime($seg[0]->StopPointArrivalTime);
                                                                        $interval = $datetimed->diff($datetimea);
                                                                        echo $interval->format('%h')."H ".$interval->format('%i')."m";
                                                                    @endphp
                                                                    @endif
                                                                     |
                                                                      <!-- 3h 5m -->
                                                                      </span>
                                                                </span>
                                                                </span></span></div>
                                        <!-- End: popup hover -->
                                       </div>
                                       <div class="col-md-3 ">
                                          <div class="theme-search-results-item-img-wrap">
										  <?php  
        												    $dirname = "assets/home/images/AirlineLogo";
        													$filename = glob("$dirname/*{$seg[0]->Airline->AirlineCode}*", GLOB_BRACE);
        								    ?>
        												@if(isset($filename) && count($filename)>0)
        												    
														<img class="theme-search-results-item-img _mob-h" src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" title="Image Title"/>
                                                        @endif										  
                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
							  @endforeach
							  @endif
@endforeach
