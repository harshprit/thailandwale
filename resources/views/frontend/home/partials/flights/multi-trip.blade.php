    <div class="">
      <div class="multi-flight-list">
	  @if(isset($flight_result))
	  
		  @foreach($flight_result as $fd)
      <div class="result_p mb3" id="ResSet_0">
                                        <div class="col-md-9">
                                            <?php $seg_index=0; ?>
                                            @foreach($fd->Segments as $seg)
                                            <div class="fleft border_bot pad_em width_98">
                                                <span class="spl_icon_flight">
                                                    <span class="fleft width_98">
        												<?php  
        												    $dirname = "assets/home/images/AirlineLogo";
        													$filename = glob("$dirname/*{$seg[0]->Airline->AirlineCode}*", GLOB_BRACE);
        												?>
        												@if(isset($filename) && count($filename)>0)
        												    <img src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" class="fleft mr5">
                                                        @endif
        			                                    <code id="Code_nm_mob" class="font10 fleft lh_10 width_64 m_width_100">
        			                                        <em class="mobile_not">  {{$seg[0]->Airline->AirlineName}} </em> 
                                                            <em class="fleft width_100 font10">
                                                                <span class="fleft mr5 lh10" id="AirlineDetailsOutBound_0">
                                                                    {{$seg[0]->Airline->AirlineCode}} - {{$seg[0]->Airline->FlightNumber}} 
                                                                </span>
                                                                <em class="mobile_class" id="class_0" title="Flight Booking Class">
                                                                    <dfn class="class_re">Class -</dfn> 
                                                                    {{$seg[0]->Airline->FareClass}}
                                                                </em>
                                                            </em>
                                                        </code>
                                                    </span>
                                                </span>
                                                <input type="hidden" id="segLenOut_0" value="2">
                                                <input type="hidden" id="segLenIN_0" value="2">
                                                <span class="spl_duration_flight">
                                                    <span class="fleft width_100per">
                                                        <em id="departure_0">
                                                            {{$seg[0]->Origin->Airport->AirportCode}}  
                                                            <tt class="hightlight bold" id="deptTimeOut_0">
                                                                ({{date('H:i',strtotime($seg[0]->Origin->DepTime))}})
                                                            </tt>
                                                        </em>                                  
																<?php $count_seg=count($fd->Segments[$seg_index]); ?>
																@if($count_seg>1)
																    @foreach($fd->Segments[$seg_index] as $multi)
                                                                        <code> →</code> 
        															    <dfn id="SegOut_0_0">{{$multi->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_0" value="CCU"></dfn>
        															      
                                                                        <input type="hidden" id="destAirport_0" value="BKK">
                                                                    @endforeach
                                                                    <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[$count_seg-1][0]->Destination->ArrTime))}})</tt>
															    @else
    																<code> →</code> <em class="txt_aln_rt" id="arrival_0"> {{$seg[0]->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_1" value="BKK"> <tt class="hightlight bold">({{date('H:i',strtotime($seg[0]->Destination->ArrTime))}})</tt></em>
                                                                
                                                                    <input type="hidden" id="destAirport_0" value="BKK">
                                                                @endif
                                                                
                                                                
              <!--                                          <em id="departure_0">-->
              <!--                                              {{$seg[0]->Origin->Airport->AirportCode}}  -->
              <!--                                              <tt class="hightlight bold" id="deptTimeOut_0">-->
              <!--                                                  ({{date('H:i',strtotime($seg[0]->Origin->DepTime))}})-->
              <!--                                              </tt>-->
              <!--                                          </em>                                  -->
														<!--<code> →</code>-->
														<!--<em class="txt_aln_rt" id="arrival_0">-->
    										<!--		        {{$seg[0]->Destination->Airport->AirportCode}}  -->
    										<!--	            <tt class="hightlight bold">-->
    										<!--		            ({{date('H:i',strtotime($seg[0]->Destination->ArrTime))}})-->
    										<!--		        </tt>-->
													 <!--   </em>-->
                                                    </span>
                                                </span> 
                                                <span class="spl_dur_flight mobile_not" id="spldurationboxoutbound_0">
                                                    <span class="fleft width_100">
                                                        <span id="duration_0">
                                                              <b> 
                                                              <?php $datetimed = new DateTime($seg[0]->StopPointDepartureTime);
                                                                $datetimea = new DateTime($seg[0]->StopPointArrivalTime);
                                                                $interval = $datetimed->diff($datetimea);
                                                                echo $interval->format('%h')."h ".$interval->format('%i')."m"; ?>
                                                               </b></span><br>
                                                         
                                                           
                                                            <a class="stop_1" href="#!" id="outBoundStops_0">
                                                                <small>
                                                                    
                        
                                                                           @if(count($seg)==1)
                                                                            Non Stop
                                                                           @else
                                                                            {{(count($seg)-1)." Stop" }}
                                                                           @endif
                                                                        
                                                                    
                                                                </small></a>
                                                        
                                                    </span>
                                                                                                
                                                           <br>
														   @if(isset($seg[0]->NoOfSeatAvailable))
														   <code class="red">{{$seg[0]->NoOfSeatAvailable}} seat(s) left</code> 
														   @else
														   <code>{{"no seat(s) "}}</code>
															@endif
                                                       
                                                        <div class="outbound_stop_popup" id="outBoundStopInfo_0" style="display: none;">
                                                        <a class="fl_close ipad_close_btn" id="outBoundStopInfoClose_0" href="#"></a>
                                                            
                                                                <span class="fleft width_100per">
                                                                    <em>SG-253 A</em>
                                                                    <em> &nbsp;&nbsp; DEL(18:20)</em>
                                                                    <code> →</code> 
                                                                                             
                                                                    <em>CCU(20:30)</em>
                                                                </span>                                           
                                                            
                                                                <span class="fleft width_100per">
                                                                    <em>SG-83 A</em>
                                                                    <em> &nbsp;&nbsp; CCU(00:05)</em>
                                                                    <code> →</code> 
                                                                                             
                                                                    <em>BKK(04:10)</em>
                                                                </span>                                           
                                                            
                                                        </div>
                                                    
                                                </span>
                                                <?php $seg_index++; ?>
                                            </div>
                                        @endforeach
           
                                        </div> 
                                        <div class="col-md-3 col-xs-12">
                                            <?php $basefare = $fd->FareBreakdown[0]->BaseFare;
                                                    $tax = $fd->FareBreakdown[0]->Tax;
                                                    
                                                    $other_charges = $fd->Fare->OtherCharges;
                                                    $adult_count = $fd->FareBreakdown[0]->PassengerCount;
                                                    
                                                    if(isset($fd->FareBreakdown[1]))
                                                    $child_count = $fd->FareBreakdown[1]->PassengerCount;
                                                    else
                                                    $child_count=0;
                                                    
                                                    $other_charges_per_adult =$other_charges/($adult_count+$child_count); 
                                                    $pubfare_per_adult = ($basefare+$tax)/$adult_count+$other_charges_per_adult;
                                                    $offer_on_pubfare = $fd->Fare->CommissionEarned+$fd->Fare->PLBEarned+$fd->Fare->IncentiveEarned;
                                                    $offer_on_pubfare_per_adult = $offer_on_pubfare/($adult_count+$child_count);
                                                    $offeredfare_per_adult = $pubfare_per_adult-$offer_on_pubfare_per_adult;
                                                    
                                                   //$fare_per_adult = ($basefare+$tax)/$adult_count;
                                             ?>
                                        <span class="spl_price mobile_not" id="pub_fare_0">
                                            <span class="fleft Width_100 m_float_rt" id="pubPricespan_0" style="visibility:visible;">
                                                <tt>Rs. </tt>
                                                <tt id="PubPrice_0">
                                                    {{" ".number_format(money_format('%!i',($pubfare_per_adult)),0)}}
                                                </tt>
                                                
                                            </span> 
                                            <span class="icon_flight2 mobile_not">
                                                <a class="ruppe" href="#" id="FareRule_0"><span class="drop2"> Fare Rule</span></a>
                                                
                                                    <a class="close cur_default" href="#"><span class="drop2" id="isRefundable_0">Lowest Cancellation</span></a>
                                                   
                                                 
                                            </span>  
                                        </span>

                                        <span class="spl_price mobile_not" id="OfferPriceSpan_0" style="visibility:visible;">
                                            <span>Rs.</span>
                                            <span id="OfferPrice_0">
                                             {{" ".number_format(money_format('%!i',($offeredfare_per_adult)),0)}}
                                            </span>
                                            
                                        </span>
                                       
                                       <span class="book_now_emails">
                                        <span class="book_now_btn mt5"><a href="{{route('frontend.flight_passenger_details',['TraceId' =>$flights->Response->TraceId ,'ResultIndex'=>$fd->ResultIndex])}}" id="btnBookNow_0" class="btn_bg">Book Now</a></span>
                                       
                                       </span>
                                    </div>  
                                        <!--<span class="spl_email spl_pt15">-->
                                        <!--    <input type="checkbox" id="chkEmailItinerary_0" name="emailItinerary" value="0"> Email-->
                                        <!--</span>-->
                                         

                                        <div class="fare_rule_pop" id="FareRuleBlock_0" style="display: none;">
                                            <div class="head_pop" id="FareRuleHead">
                                                <span id="FareRuleHeadTitle">Fare Rules</span>
                                                <a href="#" id="btnHeadFareRuleBlockClose_0" class="fl_close"></a>
                                            </div>
                                            <div id="loadingMsg_0">
                                            </div>
                                            <div class="fare_footer" id="FareRuleFoot">
                                                <span class="close_btn cursor">
                                                    <input type="button" value="Close" id="btnFareRuleBlockClose_0">
                                                </span>
                                            </div>
                                        </div>

                                        
                                                    <p class="airline_rem"><span>Remarks for star Coupon.</span></p>
                                                
                                        
                                        <div class="stop_1 fleft relative width_100">     <p class="airline_rem">
                                            <span class="cursor" id="FreeBaggage_0"> Check-In Baggage :
                                          15 Kg ,15 Kg ,15 Kg ,15 Kg 
                                          
                                            </span>
                                             </p>
                                          
                                        </div>
                                         
                                            

                                    </div>
									@endforeach
									@endif
        </div>
      </div>