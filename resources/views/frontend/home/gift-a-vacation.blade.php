@include('frontend.includes.header')
<section class="gift-holiday-section">
    
  
       <div class="col-md-12">
         <img src="{{asset('assets/home/images/unhappy-festival.png')}}" class="img-responsive" />
       </div>
       
 
    
</section>

<section class="innerpage-wrapper">
            <div class="gift-vacation">
                <div class="container">
                    <div class="row sjlt">
                        <div class="page-heading mg-bot-55">
                                <h2 style="margin-top: 28px;">Gift Your Loved One Vacation to Thailand</h2>
                                <hr class="heading-line" />
                            </div>
                       
                        <div class="gift-content">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 gift-left-cont">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gift-icon">
                                    <img src="{{asset('assets/home/images/giftbox.png')}}" class="gift-icon-img">
                                </div>
                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 gift-text">
                                    <h4 class="gift-icon-txt"><span class="gift-span">Gift Memories Not Asset:</span>These days, young travellers cherish memories more than possessions. By gifting your loved one a package to Thailand, you can help them create fantastic memories. </h4>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gift-icon">
                                    <img src="{{asset('assets/home/images/rating.png')}}" class="gift-icon-img">
                                </div>
                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 gift-text">
                                    <h4 class="gift-icon-txt"><span class="gift-span">Easily Select Package:</span>Choose from a wide range of exciting packages and gift the one that you think your loved ones will value the most!</h4>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gift-icon">
                                    <img src="{{asset('assets/home/images/budget.png')}}" class="gift-icon-img">
                                </div>
                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 gift-text">
                                    <h4 class="gift-icon-txt"><span class="gift-span">Flexibility to Set Your Budget:</span>We allow customisations to each package so that you can fit them in your budget and give your loved ones the perfect gift!</h4>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 gift-icon">
                                    <img src="{{asset('assets/home/images/love.png')}}" class="gift-icon-img">
                                </div>
                                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 gift-text">
                                    <h4 class="gift-icon-txt"><span class="gift-span">Personalize You Message:</span>Add a personalised message to your gift to make it even more valuable for your loved ones!</h4>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 gift-right-cont">
                                <div class="gift-image-right">
                                    <img class="gifty-image" src="{{asset('assets/home/images/best-wishes.png')}}">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </section>
        
<section class="second-gift-section">
<h2 class="send-karo-hmare-saath">Give Your Loved Ones an Ultimate Thailand Experience</h2>
    <div class="col-lg-12 col-md-12 col-sm-12 gift-left">
        <hr class="heading-line" />
        <p class="gift-a-getaway">A great gift is one that has a lot of thought behind it. Being able to give your loved ones something that they will truly appreciate can be a very rewarding experience. These days, many people aspire to travel and see the world, and as a friend, you can help make your loved one’s dreams come true. 
</p>
            <h2 style="text-align: center;">Choose between three types of packages:</h2>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 iconerss">
            
            <img class="gift-iconers" src="{{asset('assets/home/images/onlyb.png')}}">
            <h3 class="gift-texts">Planned Budget With Flexible Location & Dates</h3>
            <p class="gifts-desc">Pick locations and dates that suit your loved ones taste to give them a vacation they’ll always cherish!</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 iconerss">
            <img class="gift-iconers" src="{{asset('assets/home/images/onlybl.png')}}">
            <h3 class="gift-texts">Planned Budget & Locations With Flexible Dates</h3>
            <p class="gifts-desc">Pick any of our prepared packages with flexible dates so that all you have to do is surprise loved ones with dates that suit them!</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 iconerss">
            <img class="gift-iconers" src="{{asset('assets/home/images/onlybld.png')}}">
            <h3 class="gift-texts">Fully Planned</h3>
            <p class="gifts-desc">Pick a fully planned package that is curated with care by our expert travel and holiday planners! </p>
            </div>
    </div>
    
</section>
<section class="third-gift-section">
    <h2 class="your-forms-heading">Enquire Now For Exciting Gift Options</h2>
    <hr class="heading-line" />
    <form action="submit">
        <div class="form-group gift-form-section">
                                
                                
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titly">
                                        <input type="text" class="form-control first_height" name="gifter_name" id="first_name" placeholder="Your Name" title="enter your name.">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titly">
                                        <input type="text" class="form-control first_height" name="gifter_mobile" id="first_name" placeholder="Mobile No." title="enter your mobile no..">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titly">
                                        <input type="email" class="form-control first_height" name="gifter_email" id="first_name" placeholder="Email Id" title="enter your email id.">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titly">
                                        <input type="textarea" class="form-control first_height" name="gifter_message" id="first_name" placeholder="Message" title="enter your message.">
                                    </div>
                                
        </div>
        <div class="gift-form-button">
        <button class="btn btn-lg btn-success centra" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Enquire</button>
        </div>
    </form>
</section>
@include('frontend.includes.footer')