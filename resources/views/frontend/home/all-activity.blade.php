@include('frontend.includes.header')

<section class="activities_page_section">
    <div class="container">
        <div class="col-md-3">
            <div class="area_reigion">
                <div class="area_reigion_header">
                <h3>
                Browse by Region
                </h3> 
                </div>
                <div class="area_reigion_main area_reigion_main_en">
                <ul>
                @foreach($activity_count as $act_count)
                <li>
                    
                </li>                
               @endforeach 
                </ul>
                </div>
                <div class="area_reigion_footer"></div>
                </div>
                
            <div class="area_reigion">
                <div class="area_reigion_header">
                <h3>
                Browse by Categories
                </h3> 
                </div>
                <div class="panel panel-default">
                                        <div class="panel-heading active">
                                            <a href="#panel-1" data-toggle="collapse" >Select Category <span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->

                                        <div id="panel-1" class="panel-collapse collapse in">
                                            <div class="panel-body text-left ">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check01" name="category"/>
                                                    <label for="check01"><span><i class="fa fa-check"></i></span>All</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check02" name="category"/>
                                                    <label for="check02"><span><i class="fa fa-check"></i></span>Apartment</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check03" name="category"/>
                                                    <label for="check03"><span><i class="fa fa-check"></i></span>Bed & Breakfast</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check04" name="category"/>
                                                    <label for="check04"><span><i class="fa fa-check"></i></span>Guest House</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check05" name="category"/>
                                                    <label for="check05"><span><i class="fa fa-check"></i></span>Hotels</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check06" name="category"/>
                                                    <label for="check06"><span><i class="fa fa-check"></i></span>Residence</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check07" name="category"/>
                                                    <label for="check07"><span><i class="fa fa-check"></i></span>Resorts</label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div>
                
                </div>
        </div>
        
        <div class="col-md-9">
            @foreach($activity as $av)
            <div class="act-right">
                <div class="col-md-3">
                    <div class="activity_item_left">
                        <div class="sale_item">
                            <a href="#"><img class="photo " src="{{asset('assets/home/images/'.$av->category.'.png')}}"></a>
                            <span class="sale">&nbsp;</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="activity_item_right2 text">
                        <div class="ranking_wrapper">
                            <div class="review_rank item"><a href="#">{{$av->category}}</a></div>
                        </div><!-- END ranking_wrapper -->

                        <div class="activity_item_title">
                            <a href="/en/asia/thailand/phuket/a/149419">
                                {{$av->title}}         
                            </a>
                            <span class="activity_item_location2">{{$av->d_title}}</span>

                        </div><!-- END activity_item_title -->
                        <div class="clear"></div>
                        <div class="text">
                            <?php  echo $av->description; ?>
                        </div>
                        <div class="features_list_wrapper">
                            <div class="features_list_item label_icon_transportation">
                                Inclusion:
                                <span><?php  echo $av->inclusions; ?></span>
                            </div>
                        </div><!-- END features_list_wrapper -->
                        <div class="features_list_wrapper">
                            <div class="features_list_item label_icon_transportation">
                                Exclusion:
                                <span><?php  echo $av->exclusions; ?></span>

                            </div>
                        </div><!-- END features_list_wrapper -->

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="activity_item_left3">
                        <div class="box">
                            <div class="price_review_wrapper">
                                <div class="price_wrap">
                                    <p class="price">
                                        <i class="fas fa-rupee-sign"></i>{{number_format($av->price)}} 
                                        <br/><label style="font-size:12px;text-transform:capitalize">Per Person</label> 
                                    </p>
                                    
                                </div><!-- price_wrap -->

                                <!--Start Check Combo Label-->
                                <!--Start Check Combo Label-->

                            </div><!-- END price_review_wrapper -->
                            <div class="available_date">
                                {{$av->suitable_for}}                     
                            </div>

                            <div class="activity_buy_button">
                                <button type="submit" class="btn btn-activity">Buy Now</button>
                            </div>

                        </div><!-- END box -->
                    </div>
                </div>
                
            </div>
            @endforeach
        </div>
        
    </div>
    
</section>
@include('frontend.includes.footer')