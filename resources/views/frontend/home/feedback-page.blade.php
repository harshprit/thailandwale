@include('frontend.includes.header')
<section class="visa_section">
    <div class="container">


    <div class="row">
    
        <div class="col-md-8 col-xs-12" > 
        <br>
        <h2 class="beliya"><u>User Feedback</u></h2>
        
        <p align="justify" class="visa_text">What matters for us the most is experience of our customers. We really want that all our customers should have a delightful experience dealing with us. Whether it is about our services or our portal. Therefore, we encourage our customers to share their valuable feedback so that we can put our focus on things which can enhance the overall customer experience.
</p>
        
        </div>
         
         
        
     
            <div class="col-md-4 col-xs-12">
        

            <div class="widget-sidebar">
                
            <div class="boxes">
              <h3><span class="fa fa-envelope"></span> Need Our Help ?</h3>
              <h5>WE WOULD BE HAPPY TO HELP YOU!</h5>
              <h5><i class="fa fa-phone"></i> <a href="tel:+8755169330">(+91) 74043 40404</a></h5>
              <h5>hello@thailandwale.com</h5>
            </div>
            
          </div>



</div>
<div class="col-md-12 feedback-ka-form">
    <h2 style="text-align: center; margin-top: 50px;">Share Your Valuable Feedback</h2>
    
    {!! Form::open(['route'=>'frontend.store.feedback', 'id'=>'feedback-form', 'method'=>'post']) !!}
         
                <div class='required'>
                    
                <input class='' placeholder="Your Name" name="name" size='20' type='text'>
                </div>
            
               <div class='card required'>
               <input autocomplete='off' required="true" placeholder="Your Email" name="email" class=' card-number' size='20' type='email'>
               </div>
            
              <div class='card required'>
             
              <input autocomplete='off' required="true" placeholder="Your Number" name="number" class=' card-number' size='20' type='text'>
              </div>
              
              <div class='card required'>
             
              <select name="feedback_for" class=" select">
              <option value="Website">Website</option>
              <option value="Product">Product</option>
              <option value="Pre Sales">Pre Sales</option>
              <option value="Post Sales">Post Sales</option>
              <option value="Other">Other</option>
              </select>
              </div>
            
              <div class='cvc required'>
             
              <input autocomplete='off' class=' card-cvc' name="message" placeholder="Description" size='20' type='text'>
              </div>
    
           
           
              <div class='form-group'>
              <label class='control-label'></label>
              <button class=' btn btn-primary feedback-button' type='submit'> Send Feedback </button>
              </div>

              {!! Form::close() !!}  

</div>
</div>
   </div>
   </section>
@include('frontend.includes.footer')
@include('frontend.includes.footer')