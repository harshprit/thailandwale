@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')

<div class="pasflightdet">
<div class="tl_container mt5 mob_top_0">
        <div class="col-md-8" id="mobile_pax_detail">          
            <div class="fleft width_100">
                        <div class="htd_heading"><dfn>Review Details</dfn></div>
               <div class="fleft width_100">                
                
                   <div class="htd_head package-inclu" id="mobile_flight_info_head">
                    <h1>Flight Information</h1>
                    <span class="htd_modigy"></span>
                   </div>
                   
                        <div class="htd_databox mobile_not" id="mobile_flight_info_cont">
                    <div class="htd_databoxin">
                        <table cellpadding="0" cellspacing="0" width="100%" class="htd_table mobile_not">
                            <tbody><tr>
                                <th align="left">Flight No</th>
                                <th align="left">Origin</th>
                                <th align="left">Destination</th>
                                <th align="left">Dep Date Time</th>
                                <th align="left">Arr Date Time</th>                  
                                <th align="left">Class</th> 
                            </tr>
                            <?php 
                                $fc=0;
                            ?>
                    @foreach($flights->Segments as $flight)
                            @foreach($flight as $fl) 
                            <tr>
                                <td valign="top"><span id="airline_code">
                                    <?php $airline_code=$fl->Airline->AirlineCode;
                                          $flight_no = $fl->Airline->FlightNumber;
                                          $fare_class = $fl->Airline->FareClass; ?>
                                    {{$airline_code}}-{{$flight_no}}
                                </span></td>
                    <?php $flight_origin=$fl->Origin->Airport->CityName; ?>
                    <?php $flight_dest=$fl->Destination->Airport->CityName;?> 
                    <?php $flight_start_date=$fl->StopPointDepartureTime; 
                     $start = date("d/m/Y H:i", strtotime($flight_start_date));
                    ?>
                    
                    <?php $flight_end_date=$fl->StopPointArrivalTime; 
                     $end = date("d/m/Y H:i", strtotime($flight_end_date));
                    ?>
                                <td><span>{{$flight_origin." "}}</span> </td>
                                
                                <td><span>{{$flight_dest." "}}</span>  </td>
                                
                                <td><span>{{$start." hrs"}}</span> </td>
                                
                                <td><span>{{$end." hrs"}}</span> </td>                    
                                <td valign="top"><span>{{$fare_class}}</span></td>
                            </tr>
                            @endforeach
                        @endforeach
                        
                        </tbody></table>
                    </div>
                </div>
        <?php   $dirname = "assets/home/images/AirlineLogo";
                $filename = glob("$dirname/*{$flights->AirlineCode}*", GLOB_BRACE); ?>  
                
         <div class="ht_detlcontainer desktop_not" id="mobile_flight_info_cont_mobile">
            <div class="flight_details_of_oneway">
                                    <span class="fl_logo_oneway">
                                         <img src="{{ asset($filename[0])}}" alt="Airline logo"  class="fleft mr mt3 img-responsive">
                                         <code class="font11" id="AirlineOutBoundSpan">
                                            <kbd>
                                                <?php $airline_name=$flights->Segments[0][0]->Airline->AirlineName; ?>
                                                {{$airline_name}}
                                            </kbd><br>
                                            <kbd>
                                              <?php $airline_code=$flights->Segments[0][0]->Airline->AirlineCode;
                                              $flight_no = $flights->Segments[0][0]->Airline->FlightNumber;
                                              $fare_class = $flights->Segments[0][0]->Airline->FareClass; ?>
                                                {{$airline_code}}-{{$flight_no}}  
                                            </kbd>
                                            <tt>{{$fare_class}}</tt>
                                        </code>
                                    </span>
                                        <?php $stop_point = count($flights->Segments[0]); ?>
                                        <?php $flight_origin=$flights->Segments[0][0]->Origin->Airport->CityCode; ?>
                                        <?php $flight_dest=$flights->Segments[0][$stop_point-1]->Destination->Airport->CityCode;?> 
                                        <?php $flight_start_date=$flights->Segments[0][0]->StopPointDepartureTime; 
                                         $start_date = date("D, d M Y", strtotime($flight_start_date));
                                         $start_time = date("H:i", strtotime($flight_start_date));
                                    ?>
                                        
                                        <?php $flight_end_date=$flights->Segments[0][$stop_point-1]->StopPointArrivalTime; 
                                         $end_date = date("D, d M Y", strtotime($flight_end_date));
                                         $end_time = date("H:i", strtotime($flight_end_date));
                                        ?>
                                    <span class="fl_det_oneway">
                                        <em class="fl_dest_oneway">
                                          {{$flight_origin}}
                                        </em>
                                        <em class="fl_time_oneway">
                                           <tt>{{$start_date}} </tt> <kbd class=" mobile_not">|</kbd> <tt>{{$start_time}}</tt>                                           
                                        </em>
                                    </span>
                                    <span class="fl_det_oneway m_border_rt_none">
                                        <em class="fl_dest_oneway">
                                        {{$flight_dest}} 
                                        </em>
                                        <em class="fl_time_oneway">
                                           <tt>{{$end_date}}</tt> <kbd class=" mobile_not">|</kbd>  <tt>{{$end_time}}</tt>                                           
                                        </em>
                                    </span>
                                    <span class="fl_dur_oneway">
                                       <code class="m_d_left"> 
                                        <em class="bold">
                                            Duration :
                                        </em>
                                        <em class="">
                                            <tt>
                                                <?php $datetimed = new DateTime($flights->Segments[0][0]->StopPointDepartureTime);
                                                $datetimea = new DateTime($flights->Segments[0][$stop_point-1]->StopPointArrivalTime);
                                                $interval = $datetimed->diff($datetimea);
                                                ?>
                                            {{$interval->format('%H')."h ".$interval->format('%i')."m"}}
                                            </tt>
                                        </em>
                                       </code>
                                       <!--<code class="desktop_not m_d_right"><a href="#null" id="fareSummaryMobile">Fare Detail</a></code>-->
                                    </span>
        </div>
            </div>
                   
            </div>
                     
                
                     
                    
                   
                    </div>
           
                 <input type="hidden" id="TravelDepDate" name="TravelDepDate" value="15/08/2018">
            
                      
         <div class="htd_head">
            <h1>Passenger Details</h1>
            
            <!--<span class="fright link">-->
            <!--    <a href="#" id="btnBack">Change Pax Details</a>-->
            <!--</span>-->
        </div>
        <div class="htd_databox">
            <?php $i=1;
            $adult_basefare=0.00;
            $adult_yqtax =0.00;
            $adult_tax = 0.00;
            $adult_tfee = 0.00;
            
            $child_basefare=0.00;
             $child_yqtax =0.00;
            $child_tax = 0.00;
            $child_tfee = 0.00;
            
            $infant_basefare=0.00;
             $infant_yqtax =0.00;
            $infant_tax = 0.00;
            $infant_tfee = 0.00; ?>
        @foreach($passenger as $psg)
           <div class="htd_databoxin">
                <div class="htd_heading"><dfn>Passenger {{$i}}</dfn>
                    @if($psg['PaxType']==1)
                    - (Adult)
                    <?php $adult_basefare=$psg['Fare']['BaseFare'];
                        $adult_yqtax =$psg['Fare']['YQTax'];
                        $adult_tax =$psg['Fare']['Tax'];
                        $adult_tfee = $psg['Fare']['AirTransFee'];
                    ?>
                    @elseif($psg['PaxType']==2)
                    - (Child)
                    <?php $child_basefare=$psg['Fare']['BaseFare'];
                        $child_yqtax =$psg['Fare']['YQTax'];
                        $child_tax =$psg['Fare']['Tax'];
                        $child_tfee =$psg['Fare']['AirTransFee'];
                    ?>
                    @else
                    - (infant)
                    <?php $infant_basefare=$psg['Fare']['BaseFare'];
                         $infant_yqtax =$psg['Fare']['YQTax'];
                        $infant_tax = $psg['Fare']['Tax'];
                        $infant_tfee =$psg['Fare']['AirTransFee'];
                                ?>
                    @endif
                </div>
                <div class="htd_formbox" id="rev_details_page">
                    <div class="frm_left">
                        <div class="htd_frmrow">
                        <label>Name :</label>
                            <code class="">{{$psg['Title']}} {{$psg['FirstName']}} {{$psg['LastName']}} </code>
                        </div>

                        <div class="htd_frmrow">
                            <label>Gender : </label>
                            <code class="">
                            @if($psg['Gender']==1)
                                Male
                            @else
                                Female
                            @endif
                            </code>
                        </div>
                        
                         <div class="htd_frmrow">
                            <label>D.O.B : </label>
                            <code class="">{{date('d-m-Y',strtotime($psg['DateOfBirth']))}}</code>
                        </div>
                        
                        <div class="htd_frmrow">
                            <label>Address : </label>
                            <code>{{$psg['AddressLine1']}}</code>
                            @if(!empty($psg['AddressLine2']))
                            <code>{{$psg['AddressLine2']}}</code>
                            @endif
                        </div>
                          
                    </div>
                    <div class="frm_right"> 
                      
                        <div class="htd_frmrow">
                            @if(!empty($psg['PassportNo']))
                            <label>Passport No. : </label>
                            <code class="">{{$psg['PassportNo']}}</code>
                            @endif
                        </div>
                        
                       <div class="htd_frmrow">
                           @if(!empty($psg['PassportExpiry']))
                            <label>Passport Exp : </label>
                            <code class="">{{$psg['PassportExpiry']}}</code>
                            @endif
                        </div>
                        
                    </div>
                </div>
               

                <div class="htd_formbox bt_none" id="rev_details_page">
                    <div class="frm_left">
                    
                    </div>  
                    <div class="frm_right">
                    
                       
                     </div> 
                </div>
               
           </div>
           <?php $i++; ?>
        @endforeach
          
        
        </div>
             <div class="htd_head"><h1>Fare Rule</h1></div>
        <div class="htd_databox">
           <div class="htd_databoxin">
                    
                
                   	        <div class="htd_formboxfarerule htd_formboxfarerulechangedheight text-content" id="divFareRule">
						   
						        @if(isset($fareRule->Response->FareRules))
						        @foreach($fareRule->Response->FareRules as $iv)
						        
								<div class="fleft width_100">
									<div class="htd_frmrow">
									  @foreach($flights->Segments[0] as $seg)
									    @if($seg->Origin->Airport->CityCode==$iv->Origin)
										<code>{{$iv->Airline}}:{{$seg->Origin->Airport->CityName}}({{$iv->Origin}}) -{{$seg->Destination->Airport->CityName}} ({{$iv->Destination}})</code>
										@endif
										@endforeach
									</div>
								</div>
									<div class="fleft width_100">
										 <?php $farerule= nl2br($iv->FareRuleDetail); ?>
										 <?php echo $farerule; ?> 
									</div>
							
							    @endforeach
							 @else
							        <div class="fleft width_100">
							           <h3 style="color:#f91a03">{{"Access Denied!!!"}}</h3> 
							            </div>
							 @endif
							</div>
                
           </div>
        </div>
        <div class="htd_head pos_rel">
                            <h1>Term &amp; Conditions:</h1>
                        </div>
                        <div class="htd_databox">
                            
                               <div class="htd_frmrow">
                                 <input id="termsConditions" name="termsConditions" type="checkbox" class="fleft mr5" style="margin-top:-2px;"> 
                                 <div class="fleft width_84 ml5">
                                   I have reviewed and agreed on the fares and commission offered for this booking.
                                     
                                 </div>
                                   
                                     
                               </div>
                                <div class="htd_frmrow" id="msgTermsAndCondition" style="display:none;">
                                    <span class="red font11" style="margin-left:22px;"><em>You need to agree to the Terms and Conditions to Proceed.</em></span>
                                </div>
                              

                            </div>

                           
            
            <input type="hidden" id="ISCSCAgent" name="ISCSCAgent" value="False">
            <input type="hidden" id="ISOxiAgent" name="ISOxiAgent" value="False">
            <input type="hidden" id="isB2BPGEnable" name="isB2BPGEnable" value="False">
            <input type="hidden" id="isTboPG" name="isTboPG" value="False">
						@if(!access()->user())
                        <div class="col-md-6">
                            <label for="guest">Continue as a guest</label>
                            <input type="text" id="guest" name="guest_email" placeholder="Enter your email id..." class="form-control">
                        </div>
                        @endif
                              <div class="col-md-6">
                                  @if(!access()->user())
                                <label for="login">Not Logged in?</label>
                                <input type="button" data-toggle="modal" data-target="#signin" id="login" value="LOG IN" name="login" class="btn" style="color:#fff;background-color:#3f51b5">
                                  @else
                                <label for="login">Proceed Payment with</label>
                                <input type="text"  id="login" value="{{access()->user()->email}}" name="user_email" class="form-control" readonly>
                                @endif
                        </div>
            

          <div class="btnrow">
            <span id="btnPanel">
              
                                @if(isset($booking_type))
                                <form method="post" action="{{route('frontend.package.booking')}}">
                                    @csrf
                                    <input type="hidden" name="TraceId" value="{{$trace_id}}">
                                    <input type="hidden" name="ResultIndex" value="{{$result_index}}">
                                    <input type="hidden" name="is_lcc" value="@if($is_lcc) 1 @else 0 @endif">
                                    <input type="submit" value="Proceed to Payment" class="btn_main fright mr">
                                </form>
                                @endif
                                @if(!isset($result_index_return))
                                @if(!$is_lcc)
                                <a href="{{route('frontend.flight_hold',['TraceId' =>$trace_id ,'ResultIndex'=>$result_index])}}" id="btnTicket" class="fright btn_main mr">Hold Ticket</a>
                                <a href="{{route('frontend.flight_ticket_non_lcc_direct',['TraceId' =>$trace_id ,'ResultIndex'=>$result_index])}}" id="btnTicket" class="fright btn_main mr">Ticket</a>
                                @else
                                <a href="{{route('frontend.flight_ticket_lcc_intl',['TraceId' =>$trace_id ,'ResultIndex'=>$result_index])}}" id="btnTicket" class="fright btn_main mr">Ticket</a>
                                @endif
                                @endif
                                @if(isset($result_index_return))
                                @if($is_lcc_return && $is_lcc)
                                <a href="{{route('frontend.flight_ticket_lcc',['TraceId' =>$trace_id ,'ResultIndex'=>$result_index,'ResultIndexReturn'=>$result_index_return])}}" id="btnTicket" class="fright btn_main mr">Ticket</a>
                                @elseif(!$is_lcc && $is_lcc_return)
                                <a href="{{route('frontend.ob_non_lcc',['TraceId' =>$trace_id ,'ResultIndex'=>$result_index,'ResultIndexReturn'=>$result_index_return])}}" id="btnTicket" class="fright btn_main mr">Ticket</a>
                                @elseif($is_lcc && !$is_lcc_return)
                                <a href="{{route('frontend.ib_non_lcc',['TraceId' =>$trace_id ,'ResultIndex'=>$result_index,'ResultIndexReturn'=>$result_index_return])}}" id="btnTicket" class="fright btn_main mr">Ticket</a>                                
                                @else
                                <a href="{{route('frontend.flight_hold_return',['TraceId' =>$trace_id ,'ResultIndex'=>$result_index,'ResultIndexReturn'=>$result_index_return])}}" id="btnTicket" class="fright btn_main mr">Hold Ticket</a>
                                <a href="{{route('frontend.flight_ticket_non_lcc_direct_return',['TraceId' =>$trace_id ,'ResultIndex'=>$result_index,'ResultIndexReturn'=>$result_index_return])}}" id="btnTicket" class="fright btn_main mr                                
                                @endif
                                @endif
                                
             </span>         
            </div>
        </div>
        
        <div class="alertpop h1 reqb_pop" id="IsOnRequestBooking" style="display:none;">
             <a href="#" id="OnrequestPopUpClose" class="fl_close_1 fl_close"></a>
            <div class="head_pop">
                <span> This is not a confirmed booking , For confirmation kindly email booking PF(Booking PF number ) to lcc@travelboutiqueonline.com .Otherwise this booking will not be confirmed.</span>
               
            </div>            
            <div class="fare_footer pad_top_0">
                <span class="close_btn cursor">
                    <input type="button" id="btnOkForOnrequest" value="OK">
                        <input type="button" id="btnCancelForOnrequest" value="Cancel">
                </span>
            </div>
        </div>
        <!-- Fare Details -->
        <div class="col-md-3 mobfilter_display_none" id="mobile_queue_filter">
                <h3 class="hopdf">Promo Code </h3>                  
                <div class="deal_cont" id="lblPromoCode">
                         
                <div class="drdr_in">                        
                    <label class="promo_error" id="errorPromoCode"></label>
                        
                        <input id="PromoCode" name="PromoCode" type="text" class="promoinput fleft valid" value="" placeholder="Promo Code" maxlength="10">
                         
                    <div class="fright">
						@if(access()->user())
                        <a id="PromoCodeApply_login" href="#" class="btn_main fright mr5" >Apply</a>
						@else
						<a id="PromoCodeApply" href="#" class="btn_main fright mr5" >Apply</a>
						@endif
						
                </div>
                </div>       
                       
                           
                          
                           
                </div>
                <br><br>
            
            
            <h3 class="hopdf">Booking Summary </h3>
                  <!--<div class="deal_head cursor">-->
                      
                  <!--    <a href="#" id="divSalesSummaryHead">+ Show Details</a>-->
                      
                  <!--</div>-->
            <div class="deal_cont">
                   
                @foreach($flights->Segments as $flsegment)
                    @foreach($flsegment as $flseg)
                    <?php $flight_origin=$flseg->Origin->Airport->CityCode; ?>
                    <?php $flight_dest=$flseg->Destination->Airport->CityCode;?> 
                    <?php $flight_start_date=$flseg->StopPointDepartureTime; 
                     $start = date("d M y", strtotime($flight_start_date));
                     $start_time = date("H:i",strtotime($flight_start_date));
                    ?>
                    
                    <?php $flight_end_date=$flseg->StopPointArrivalTime; 
                     $end = date("d M y", strtotime($flight_end_date));
                     $end_time = date("H:i",strtotime($flight_end_date));
                    ?>
                   <div class="deal_head">
                    <div class="flight_details_top">
                        <span class="col-md-4">{{$start}} </span>
                        <span class="col-md-4"> 
                             <?php $airline_code=$flseg->Airline->AirlineCode;
                                  $flight_no = $flseg->Airline->FlightNumber;
                                  $fare_class = $flseg->Airline->FareClass; ?>
                                    {{$airline_code}}{{$flight_no}}
                        </span>
                        <span class="col-md-4">{{$fare_class}} -Class</span>
                    </div>
                     <div class="flight_details_top">
                        <span class="col-md-4">Dept:</span>
                        <span class="col-md-4">
                            {{$flight_origin}}
                        </span>
                        <span class="col-md-4">{{"@".$start_time." hrs"}}</span>
                    </div>
                     
                     <div class="flight_details_top">
                        <span class="col-md-4">Arr:</span>
                        <span class="col-md-4">
                            {{$flight_dest}}
                        </span>
                        <span class="col-md-4">{{"@".$end_time." hrs"}}</span>
                    </div>           
                 </div>
                 @endforeach
                @endforeach
                @if(isset($ibFlights->Segments[0]))
                @foreach($ibFlights->Segments[0] as $flseg)
                    <?php $flight_origin=$flseg->Origin->Airport->CityCode; ?>
                    <?php $flight_dest=$flseg->Destination->Airport->CityCode;?> 
                    <?php $flight_start_date=$flseg->StopPointDepartureTime; 
                     $start = date("d M y", strtotime($flight_start_date));
                     $start_time = date("H:i",strtotime($flight_start_date));
                    ?>
                    
                    <?php $flight_end_date=$flseg->StopPointArrivalTime; 
                     $end = date("d M y", strtotime($flight_end_date));
                     $end_time = date("H:i",strtotime($flight_end_date));
                    ?>
                   <div class="deal_head">
                    <div class="flight_details_top">
                        <span class="col-md-4">{{$start}} </span>
                        <span class="col-md-4"> 
                             <?php $airline_code=$flseg->Airline->AirlineCode;
                                  $flight_no = $flseg->Airline->FlightNumber;
                                  $fare_class = $flseg->Airline->FareClass; ?>
                                    {{$airline_code}}{{$flight_no}}
                        </span>
                        <span class="col-md-4">{{$fare_class}} -Class</span>
                    </div>
                     <div class="flight_details_top">
                        <span class="col-md-4">Dept:</span>
                        <span class="col-md-4">
                            {{$flight_origin}}
                        </span>
                        <span class="col-md-4">{{"@".$start_time." hrs"}}</span>
                    </div>
                     
                     <div class="flight_details_top">
                        <span class="col-md-4">Arr:</span>
                        <span class="col-md-4">
                            {{$flight_dest}}
                        </span>
                        <span class="col-md-4">{{"@".$end_time." hrs"}}</span>
                    </div>           
                 </div>
                @endforeach
                @endif
                @if(isset($flights))
                    <?php
                        $adult_price=array(
                        'base'=>$flights->FareBreakdown[0]->BaseFare/$flights->FareBreakdown[0]->PassengerCount,
                        'tax'=>$flights->FareBreakdown[0]->Tax/$flights->FareBreakdown[0]->PassengerCount,
                        'yq_tax'=>$flights->FareBreakdown[0]->YQTax/$flights->FareBreakdown[0]->PassengerCount,
                        'adl_txn_fee_ofd'=>$flights->FareBreakdown[0]->AdditionalTxnFeeOfrd/$flights->FareBreakdown[0]->PassengerCount,
                        'adl_txn_fee_pbd'=>$flights->FareBreakdown[0]->AdditionalTxnFeePub/$flights->FareBreakdown[0]->PassengerCount,
                    ); ?>
                    @if(isset($flights->FareBreakdown[1]))
                    <?php
                     $child_price=array(
                        'base'=>$flights->FareBreakdown[1]->BaseFare/$flights->FareBreakdown[1]->PassengerCount,
                        'tax'=>$flights->FareBreakdown[1]->Tax/$flights->FareBreakdown[1]->PassengerCount,
                        'yq_tax'=>$flights->FareBreakdown[1]->YQTax/$flights->FareBreakdown[1]->PassengerCount,
                        'adl_txn_fee_ofd'=>$flights->FareBreakdown[1]->AdditionalTxnFeeOfrd/$flights->FareBreakdown[1]->PassengerCount,
                        'adl_txn_fee_pbd'=>$flights->FareBreakdown[1]->AdditionalTxnFeePub/$flights->FareBreakdown[1]->PassengerCount,
                    ); ?>
                    @endif
                    @if(isset($flights->FareBreakdown[2]))
                    <?php
                     $affent_price=array(
                        'base'=>$flights->FareBreakdown[2]->BaseFare/$flights->FareBreakdown[2]->PassengerCount,
                        'tax'=>$flights->FareBreakdown[2]->Tax/$flights->FareBreakdown[2]->PassengerCount,
                        'yq_tax'=>$flights->FareBreakdown[2]->YQTax/$flights->FareBreakdown[2]->PassengerCount,
                        'adl_txn_fee_ofd'=>$flights->FareBreakdown[2]->AdditionalTxnFeeOfrd/$flights->FareBreakdown[2]->PassengerCount,
                        'adl_txn_fee_pbd'=>$flights->FareBreakdown[2]->AdditionalTxnFeePub/$flights->FareBreakdown[2]->PassengerCount,
                    ); 
                    ?>
                      @endif  
                    
                @endif
                @if(isset($ibFlights))
                    <?php
                        $adult_price_return=array(
                        'base'=>$ibFlights->FareBreakdown[0]->BaseFare/$ibFlights->FareBreakdown[0]->PassengerCount,
                        'tax'=>$ibFlights->FareBreakdown[0]->Tax/$ibFlights->FareBreakdown[0]->PassengerCount,
                        'yq_tax'=>$ibFlights->FareBreakdown[0]->YQTax/$ibFlights->FareBreakdown[0]->PassengerCount,
                        'adl_txn_fee_ofd'=>$ibFlights->FareBreakdown[0]->AdditionalTxnFeeOfrd/$ibFlights->FareBreakdown[0]->PassengerCount,
                        'adl_txn_fee_pbd'=>$ibFlights->FareBreakdown[0]->AdditionalTxnFeePub/$ibFlights->FareBreakdown[0]->PassengerCount,
                    ); 
                    ?>
                    @if(isset($ibFlights->FareBreakdown[1]))
                     <?php
                     $child_price_return=array(
                        'base'=>$ibFlights->FareBreakdown[1]->BaseFare/$ibFlights->FareBreakdown[1]->PassengerCount,
                        'tax'=>$ibFlights->FareBreakdown[1]->Tax/$ibFlights->FareBreakdown[1]->PassengerCount,
                        'yq_tax'=>$ibFlights->FareBreakdown[1]->YQTax/$ibFlights->FareBreakdown[1]->PassengerCount,
                        'adl_txn_fee_ofd'=>$ibFlights->FareBreakdown[1]->AdditionalTxnFeeOfrd/$ibFlights->FareBreakdown[1]->PassengerCount,
                        'adl_txn_fee_pbd'=>$ibFlights->FareBreakdown[1]->AdditionalTxnFeePub/$ibFlights->FareBreakdown[1]->PassengerCount,
                    ); 
                    ?>
                    @endif
                    @if(isset($ibFlights->FareBreakdown[2]))
                    <?php
                     $affent_price_return=array(
                        'base'=>$ibFlights->FareBreakdown[2]->BaseFare/$ibFlights->FareBreakdown[2]->PassengerCount,
                        'tax'=>$ibFlights->FareBreakdown[2]->Tax/$ibFlights->FareBreakdown[2]->PassengerCount,
                        'yq_tax'=>$ibFlights->FareBreakdown[2]->YQTax/$ibFlights->FareBreakdown[2]->PassengerCount,
                        'adl_txn_fee_ofd'=>$ibFlights->FareBreakdown[2]->AdditionalTxnFeeOfrd/$ibFlights->FareBreakdown[2]->PassengerCount,
                        'adl_txn_fee_pbd'=>$ibFlights->FareBreakdown[2]->AdditionalTxnFeePub/$ibFlights->FareBreakdown[2]->PassengerCount,
                    ); 
                    ?>
                    
                    @endif
                    
                @endif
                
                 <?php
        $adult_count=0;$child_count=0;$infant_count=0;
        if(isset($flights->FareBreakdown[0]))
        $adult_count = ($flights->FareBreakdown[0]->PassengerCount);
        if(isset($flights->FareBreakdown[1]))
        $child_count = ($flights->FareBreakdown[1]->PassengerCount);
        if(isset($flights->FareBreakdown[2]))
        $infant_count = ($flights->FareBreakdown[2]->PassengerCount);
        ?>
                   
                <div id="mainFareDiv" class="deal_price" style="display:block;">
                    <h3 class="hopdf">Fare / Pax Type</h3>
                     
                         <div class="drdr">
                       <div class="drdr_in">
                      <div class="raterow">
                        <label>Adult</label>
                        <code> 
                        @if(isset($adult_price)&& $adult_price!="")
                            @if(isset($adult_price_return)&& $adult_price_return!="") 
                                <?php $ib_ob_adult_price = $adult_price['base']+$adult_price_return['base'] ?>
                                {{"Rs. ".$ib_ob_adult_price}}
                            @else
                                {{"Rs. ".$adult_price['base']}}
                            @endif
                        @else
                        Rs. 0.00
                        @endif
                        </code>
                     </div>
                      <div class="raterow">
                        <label>Tax and S.Charges</label>
                        <code>
                            @if(isset($adult_price)&& $adult_price!="")
                                <?php $ob_tns=round(($adult_price['tax']+($flights->Fare->OtherCharges/($adult_count+$child_count))-(($flights->Fare->CommissionEarned+$flights->Fare->PLBEarned+$flights->Fare->IncentiveEarned)/($adult_count+$child_count))),2); ?>
                                 @if(isset($adult_price_return)&& $adult_price_return!="") 
                                <?php 
                                $ib_tns=round(($adult_price_return['tax']+($ibFlights->Fare->OtherCharges/($adult_count+$child_count))-(($ibFlights->Fare->CommissionEarned+$ibFlights->Fare->PLBEarned+$ibFlights->Fare->IncentiveEarned)/($adult_count+$child_count))),2); //$adult_price_return['tax']; //$adult_price_return['yq_tax'];
                                $ib_ob_tns = $ob_tns+$ib_tns; ?>
                                {{"Rs. ".$ib_ob_tns}}
                                @else
                                    {{"Rs. ".$ob_tns}}
                                @endif
                           
                            @else
                            Rs. 0.00
                            @endif
                        </code>
                      </div>
                      <!--<div class="raterow">-->
                      <!--  <label>T. Fee</label>-->
                      <!--  <code>-->
                            
                      <!--      Rs.0.00 -->
                               
                      <!--  </code>-->
                      <!--</div>-->
                      
                           
                    </div>
                       </div>  
                         <div class="drdr_in total">
                       <div class="raterow">
                         <label>Total<br></label>
                         <code><b class="bold"> 
                          @if(isset($adult_price)&& $adult_price!="")
                         <?php $ob_adult_total = $adult_price['base']+$ob_tns;         //$adult_price['tax']; //$adult_price['yq_tax']; ?>
                         
                                
                                 @if(isset($adult_price_return)&& $adult_price_return!="") 
                                <?php 
                                $ib_adult_total=$adult_price_return['base']+$ib_tns;               //$adult_price_return['tax'];//$adult_price_return['yq_tax'];
                                $ib_ob_adult_total = $ob_adult_total+$ib_adult_total; ?>
                                {{"Rs. ".$ib_ob_adult_total}}
                                @else
                                    {{"Rs. ".$ob_adult_total}} 
                                @endif
                           @else
                           Rs. 0.00
                           @endif
                        </b></code>
                      </div>
                   </div>
                       
                         <div class="drdr">
                       <div class="drdr_in">
                      <div class="raterow">
                        <label>Child</label>
                        <code> 
                        @if(isset($child_price)&& $child_price!="")
                            @if(isset($child_price_return)&& $child_price_return!="") 
                                <?php $ib_ob_child_price = $child_price['base']+$child_price_return['base'] ?>
                                {{"Rs. ".$ib_ob_child_price}}
                            @else
                                {{"Rs. ".$child_price['base']}}
                            @endif
                        @else
                        Rs. 0.00
                        @endif
                        </code>
                     </div>
                      <div class="raterow">
                        <label>Tax and S.Charges</label>
                        <code> 
                            @if(isset($child_price)&& $child_price!="")
                                <?php $ob_tns_child=round(($child_price['tax']+($flights->Fare->OtherCharges/($adult_count+$child_count))-(($flights->Fare->CommissionEarned+$flights->Fare->PLBEarned+$flights->Fare->IncentiveEarned)/($adult_count+$child_count))),2);  //$child_price['tax'];//$child_price['yq_tax']; ?>
                                 @if(isset($child_price_return)&& $child_price_return!="") 
                                <?php 
                                $ib_tns_child=round(($child_price_return['tax']+($ibFlights->Fare->OtherCharges/($adult_count+$child_count))-(($ibFlights->Fare->CommissionEarned+$ibFlights->Fare->PLBEarned+$ibFlights->Fare->IncentiveEarned)/($adult_count+$child_count))),2);  //$child_price_return['tax'];//$child_price_return['yq_tax'];
                                $ib_ob_tns_child = $ob_tns_child+$ib_tns_child; ?>
                                {{"Rs. ".$ib_ob_tns_child}}
                                @else
                                    {{"Rs. ".$ob_tns_child}}
                                @endif
                           
                            @else
                            Rs. 0.00
                            @endif
                        </code>
                      </div>
                      <!--<div class="raterow">-->
                      <!--  <label>T. Fee</label>-->
                      <!--  <code>Rs.0.00 -->
                               
                      <!--  </code>-->
                      <!--</div>-->
                      
                           
                    </div>
                       </div>  
                         <div class="drdr_in total">
                       <div class="raterow">
                         <label>Total <br></label>
                         <code><b class="bold"> 
                          @if(isset($child_price)&& $child_price!="")
                         <?php $ob_child_total = $child_price['base']+$ob_tns_child; //$child_price['tax'];//$child_price['yq_tax']; ?>
                         
                                
                                 @if(isset($child_price_return)&& $child_price_return!="") 
                                <?php 
                                $ib_child_total=$child_price_return['base']+$ib_tns_child;   //$child_price_return['tax'];//$child_price_return['yq_tax'];
                                $ib_ob_child_total = $ob_child_total+$ib_child_total; ?>
                                {{"Rs. ".$ib_ob_child_total}}
                                @else
                                    {{"Rs. ".$ob_child_total}} 
                                @endif
                           @else
                           Rs. 0.00
                           @endif
                        </b></code>
                      </div>
                   </div>
                       
                        <div class="drdr">
                        <div class="drdr_in">
                        <div class="raterow">
                        <label>Infant</label>
                        <code> 
                        @if(isset($affent_price)&& $affent_price!="")
                            @if(isset($affent_price_return)&& $affent_price_return!="") 
                                <?php $ib_ob_affent_price = $affent_price['base']+$affent_price_return['base'] ?>
                                {{"Rs. ".$ib_ob_affent_price}}
                            @else
                                {{"Rs. ".$affent_price['base']}}
                            @endif
                        @else
                        Rs. 0.00
                        @endif
                        </code>
                     </div>
                      <div class="raterow">
                        <label>Tax and S.Charges</label>
                        <code>
                         @if(isset($affent_price)&& $affent_price!="")
                                <?php $ob_tns=$affent_price['tax'];//$affent_price['yq_tax']; ?>
                                 @if(isset($affent_price_return)&& $affent_price_return!="") 
                                <?php 
                                $ib_tns=$affent_price_return['tax'];//$affent_price_return['yq_tax'];
                                $ib_ob_tns = $ob_tns+$ib_tns; ?>
                                {{"Rs. ".$ib_ob_tns}}
                                @else
                                    {{"Rs. ".$ob_tns}}
                                @endif
                           
                            @else
                            Rs. 0.00
                            @endif
                        </code>
                      </div>
                      <!--<div class="raterow">-->
                      <!--  <label>T. Fee</label>-->
                      <!--  <code>Rs.0.00 -->
                               
                      <!--  </code>-->
                      <!--</div>-->
                      
                           
                    </div>
                       </div>  
                         <div class="drdr_in total">
                       <div class="raterow">
                         <label>Total <br></label>
                         <code><b class="bold">
                          @if(isset($affent_price)&& $affent_price!="")
                         <?php $ob_affent_total = $affent_price['base']+$affent_price['tax'];//$affent_price['yq_tax']; ?>
                         
                                
                                 @if(isset($affent_price_return)&& $affent_price_return!="") 
                                <?php 
                                $ib_affent_total=$affent_price_return['base']+$affent_price_return['tax'];//$affent_price_return['yq_tax'];
                                $ib_ob_affent_total = $ob_affent_total+$ib_affent_total; ?>
                                {{"Rs. ".$ib_ob_affent_total}}
                                @else
                                    {{"Rs. ".$ob_affent_total}} 
                                @endif
                           @else
                           Rs. 0.00
                           @endif
                        </b></code>
                      </div>
                   </div>
                       
                          <div class="drdr_in">
                           <div class="raterow">
                             <label>Adult x <span id="adultcount"><?php echo $adult_count; ?></span></label>
                            <code> Rs.<code id="total_adult_cost">
                                 
                                 
                                 @if(!empty($ib_ob_adult_total)&& $ib_ob_adult_total!=" ")
                                 {{$ib_ob_adult_total*$adult_count}}
                                 @elseif(!empty($ob_adult_total)&& $ob_adult_total!=" ")
                                    {{$ob_adult_total*$adult_count}}
                                 @else
                                   {{"0.00"}}
                                 @endif
                             </code>
                            </code>
                           </div>
                         </div>
                      
                          <div class="drdr_in">
                           <div class="raterow">
                             <label>Child x <span id="childcount"><?php echo $child_count; ?></span></label>
                            <code>Rs.<code id="total_child_cost">
                                  @if(!empty($ib_ob_child_total)&& $ib_ob_child_total!=" ")
                                 {{$ib_ob_child_total*$child_count}}
                                 @elseif(!empty($ob_child_total)&& $ob_child_total!=" ")
                                    {{$ob_child_total*$child_count}}
                                 @else
                                   {{"0.00"}}
                                 @endif
                                  </code>
                            </code>
                           </div>
                         </div>
                      
                          <div class="drdr_in">
                           <div class="raterow">
                             <label>Infant x <?php echo $infant_count; ?></label>
                            <code>Rs.<code id="total_infant_cost">
                                   @if(!empty($ib_ob_affent_total)&& $ib_ob_affent_total!=" ")
                                 {{$ib_ob_affent_total*$infant_count}}
                                 @elseif(!empty($ob_affent_total)&& $ob_affent_total!=" ")
                                    {{$ob_affent_total*$infant_count}}
                                 @else
                                   {{"0.00"}}
                                 @endif
                                  </code>
                            </code>
                           </div>
                         </div> 
                      
                   <div class="drdr_in">
                      <div class="raterow">
                          <input type="hidden" value="0" id="obbagSelected">
                          <input type="hidden" value="0" id="obbagCharges">
                          <input type="hidden" value="0" id="ibbagSelected">
                          <input type="hidden" value="0" id="ibbagCharges">
                        <label>Excess Baggage  (<code style="float:none;" id="bagSelected">{{$baggage_selected}}Kg</code> )</label>
                        <code> Rs. <code id="bagCharges">{{$baggage_cost}}</code>
                            
                        </code>                         
                     </div>
                  </div>
                  
                    <div class="drdr_in">
                      <div class="raterow">
                           <input type="hidden" value="0" id="obmealPlatter">
                          <input type="hidden" value="0" id="obmealCharge">
                          <input type="hidden" value="0" id="ibmealPlatter">
                          <input type="hidden" value="0" id="ibmealCharge">
                        <label>Meal   (<code style="float:none;" id="mealPlatter">{{$meal_platter}}</code>Platter )</label>
                      <code>Rs. <code id="mealCharge">{{$meal_cost}}</code>
                          
                      </code>
                     </div>
                    </div>
                      
                  
                    <div class="drdr_in">
                      <div class="raterow">
                        <label>Special Service </label>
                      <code>Rs. <code id="specialServiceCharge">0.00</code>
                          
                      </code>
                     </div>
                    </div>

                    
                  
                    <div class="drdr_in">
                      <div class="raterow">
                        <label>Total GST </label>
                      <code>Rs. <code id="total_gst_cost">0.00</code>
                          
                      </code>
                     </div>
                    </div>

                
                    <div class="drdr_in">
                      <div class="raterow">
                        <label>TWS </label>
                      <code>Rs. <code id="tws">0.00</code>
                          
                      </code>
                     </div>
                    </div>
                    <div class="drdr_in">
                      <div class="raterow">
                        <label>GST @tws </label>
                      <code>Rs. <code id="gst_tws">0.00</code>
                          
                      </code>
                     </div>
                    </div>
                    
                   <div class="grandtotal">
                                             
                    <label>Total Pub. Fare</label>                     
                       
                    <code><b>Rs.</b>
                    
                    <b id="firsttot" class="bold">
                      <code id="total_fare_cost"></code>
                    </b>
                          
                    </code>
                  </div>
				  
				  
				  <div class="drdr_in" id="promo_div"style="display:none">
                      <div class="raterow">
                        <label>Promo Discount</label>
                        <code id="promo_discount_amount">Rs. 0.00
                              
                        </code>
                     </div>
                  </div>
                  
                  <div class="grandtotal" id="payable_div" style="display:none">
                    
                    <label>Total Payable Amount</label>
                    
                    <code><b class="bold" id="payable">Rs.42,241.20
                         
                         </b></code>
                  </div>
                    
                         
                </div>
                     
                </div>
            </div>
        <!-- End: Fare Details -->
        
                    </div>
					</div>
@include('frontend.includes.footer')
<script>
$('.fright').on('click',function(e){
    if(!$('#termsConditions').prop('checked')==true || $('#guest').val()=="")
    {
	if($('#guest').val()=="")
	{
		        swal({
  type: 'error',
  title: 'Oops...',
  text: 'Please Login or Input an Email to continue as guest!',
  footer: '<a href="#" data-toggle="modal" data-target="#signin">Click here?</a> Login.'
})
	}
	else
	{
	        swal({
  type: 'error',
  title: 'Oops...',
  text: 'Please accept Terms and Condition in order to proceed with booking!',
  footer: '<a href>Click here?</a> to see the terms and conditions.'
})
	}

        e.preventDefault();
    }

})
</script>
<script>

    $(document).ready(function(){
       $("#chkBoxGST").on("click",function(){
           if($("#chkBoxGST").is(':checked'))
         $("#GSTDataBlock").show();  
         else
         $("#GSTDataBlock").hide();  
       });
       $("#chkBoxRoamer").on("click",function(){
          if($("#chkBoxRoamer").is(":checked"))
          $("#RoamerDataBlock").show();
          else
          $("#RoamerDataBlock").hide();
       });
    });
</script>

<script>
    function calculateTotalFare(){
        var adult_count = $("#adultcount").html();
        var child_count = $("#childcount").html();
        var tws = 150*(parseInt(adult_count)+parseInt(child_count));
        var adult = $("#total_adult_cost").html(); 
        var child=$("#total_child_cost").html();
        var infant=$("#total_infant_cost").html();
        var baggage=$("#bagCharges").html();
        var meal=$("#mealCharge").html();
        var special_service=$("#specialServiceCharge").html();
        var gst=$("#total_gst_cost").html();
        var gsttws=(tws*18)/100;
         var total_fare = parseFloat(adult)+parseFloat(child)+parseFloat(infant)+parseFloat(baggage)+parseFloat(meal)+parseFloat(special_service)+parseFloat(gst)+parseFloat(tws)+parseFloat(gsttws);
         var tf = total_fare.toFixed(2);
         //alert(total_fare);
        // alert("adult:"+adult+"child:"+child+"infant:"+infant+"baggage:"+baggage+"meal:"+meal+"special_service:"+special_service+"gst:"+gst);
        
        $("#tws").html(tws);
        $("#gst_tws").html(gsttws.toFixed(2));
       $("#total_fare_cost").html(tf); 
    }
    calculateTotalFare();
    
</script>