@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')
<div class=" form-flights">
<div class="container">
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="flights-headline">
            <h3>Book Domestic & International Hotels</h3>
          </div>
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="tab-content">
          <div class="tab-pane active" id="tab_default_1">
		  <form method="post"  action="{{route('frontend.search_hotel')}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="row">
					 <form method="post"  action="{{route('frontend.search_flight')}}" id="hotel-search-form" >
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-xs-6 col-md-2">
                              <h3>Find Hotel</h3>
                              <input id='input_hotel' type='text'name="dest" class='form-control' placeholder="Enter City" required>
                              <input type="hidden" id ="destination" name="destination">
                            </div>
                        <div class="col-xs-6 col-md-2">
                              <div class="date">
                            <div class="depart">
                                  <h3> Check-In</h3>
                                  <input  id="datepicker" name="check_in" type="text" autocomplete="off" placeholder="mm/dd/yyyy" onfocus="this.value = '';"  required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>
                        <div class="col-xs-6 col-md-2">
                              <div class="return">
                            <h3>Check-Out</h3>
                            <input  id="datepicker1" name="check_out" type="text" autocomplete="off" placeholder="mm/dd/yyyy"  required>
                          </div>
                            </div>
                        <div class="col-xs-6 col-md-2">
                              <div class="">
                            <div class="trigger form-group form-group-lg form-group-icon-left"><i class="fa fa-users input-icon input-icon-highlight"></i>
                                  <label>Passenger</label>
                                  <input type="text" name="passengers" id="passengers" class="form-control flip"   onclick="myFunction()" placeholder="Rooms|Passengers" required>
                                </div>
                            <div id="panel" >
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <div class="row" style="width: 434px;">
                              <div class="col-md-2">
                                <label class="control-label"><strong>Room(s)</strong>
                                     </label>
                                     <select name="rooms" id="rooms" onchange="showGuest(this.value);">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                              </select>
                              </div>


                                 <div class="col-md-2">
                                  <div id="adults">

                                <label class="control-label"><strong>Adult(s)</strong><br>
                                      </label>
                                         <select onchange="count_adults();" name="adults[]" id="adults1">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                              </select>
                              </div>
                              </div>
                                 <div class="col-md-8" >
                                <div class="form-group" id="children">
                               <div class="row kids-row">
                                <div class="col-md-6">
                                <label class="control-label">
                                <strong>Child</strong></label>
                                <select name="children[]" id="children1" onchange="showAges(this.value,1);">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                </select>
                                </div>
                              <div class="col-md-8">
                              <div class="row kids-selection" id="child1"></div>
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>

                               <div class="col-md-12">
                                  <a class="btn btn-primary btn-block demise close" data-dismiss="modal">Done</a> </div>
                                </div>
                          </div>
                            </div>
                        <div class="col-xs-12 col-md-2">
                              <button type="sumbmit" class="btn btn-warning form-search-btn">Search</button>
                            </div>
					</form>
                      </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
		 @include('frontend.includes.flightoffers')

		@include('frontend.includes.footer')
