@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')
<style>
    .tabbable-panel h6 {
        margin-left:0px;
    }
</style>


<div class="container">
    <div class="row">
      <div class="col-md-12 flights-form">
       
          <div class="col-md-3 col-xs-12 right-border">
            <span><a href="#" data-toggle="modal" data-target="#modal_show">ROUND-TRIP </a></span>
            <P> <a href="#" data-toggle="modal" data-target="#modal_show">New Delhi to Mumbai </a></P>
        </div>
        <div class="col-md-2 col-xs-6 right-border">
          <span> <a href="#" data-toggle="modal" data-target="#modal_show"> DEPARTURE </a> </span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-2  col-xs-6 right-border">
          <span> <a href="#" data-toggle="modal" data-target="#modal_show"> Return </a> </span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Adult</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>01</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Child</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>--</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Infant</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>--</span>
          </a> </p>
        </div>
        <div class="col-md-2 col-xs-12">
          <button type="button" class="btn btn-success">Search</button>
        </div>
   
    
        <div class="modal fade" tabindex="-1" role="dialog" id="modal_show">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header modal-flights">
             <div class="col-md-12 flights-form">
       
          <div class="col-md-3 col-xs-12 right-border">
            <span><a href="#" data-toggle="modal" data-target="#modal_show">ROUND-TRIP </a></span>
            <P> <a href="#">New Delhi to Mumbai </a></P>
        </div>
        <div class="col-md-2 col-xs-6 right-border">
          <span> <a href="#"> DEPARTURE </a> </span>
          <p><a href="#"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-2  col-xs-6 right-border">
          <span> <a href="#"> Return </a> </span>
          <p><a href="#"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-1 col-xs-4">
         <span> <a href="#">Adult</a></span>
          <p><a href="#"> 
            <span>01</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#">Child</a></span>
          <p><a href="#"> 
            <span>--</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#">Infant</a></span>
          <p><a href="#"> 
            <span>--</span>
          </a> </p>
        </div>
        <div class="col-md-2 col-xs-12">
          <button type="button" class="btn btn-success">Search</button>
        </div>
   
    </div>
          </div>
          <div class="modal-body">
            <form method="POST" action="#">
                <div class="row">
                  <div class="col-md-12">
                      <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs ">
            <li class="active">
              <a href="#tab_default_1" data-toggle="tab">
              ONE WAY </a>
            </li>
            <li>
              <a href="#tab_default_2" data-toggle="tab">
              ROUND TRIP </a>
            </li>
           
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_default_1">
             <div class="row">
             
                  <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group"  style="margin-top:20px;">
                    
                    <input type="search"  required="required" class="form-control input-sm" value="Select City">
                  </div>
                </div>
              
              <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group" style="margin-top:20px;">
                    
                    <input type="search"  required="required" class="form-control input-sm" value="Select City">
                  </div>
                </div>
             </div>
             <div class="row">
              <div class="col-md-3 col-xs-6">
                 <label for="adresse" style="color: #77ab63"> Departure</label>
                <div class="form-group">
                    
                    <input type="date" name="nom" id="nom" required="required" class="form-control input-sm" value="">
                  </div>
             </div>
             <div class="col-md-3 col-xs-6">
               <label for="adresse" style="color: #77ab63">Return</label>
                <div class="form-group">
                    
                    <input type="date" name="nom" id="nom" required="required" class="form-control input-sm" value="">
                  </div>
             </div>
              </div>
              <div class="row">
    
    

    <div class=" col-md-3 col-sm-2">
      <label style="color: #77ab63">ADULT: 12+ YRS</label> <!-- purely semantic -->
      <div class="form-control center merge-bottom-input" name="first">0</div>

      <div class="btn-group btn-block" role="group" aria-label="plus-minus">
        <button type="button" class="btn btn-sm btn-danger minus-button merge-top-left-button" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></button>
        <button type="button" class="btn btn-sm btn-success plus-button merge-top-right-button"><span class="glyphicon glyphicon-plus"></span></button>
      </div><!-- end button group -->
    </div> <!-- end column -->


    <div class=" col-md-3 col-sm-2">
      <label style="color: #77ab63">CHILD: 2-12 YRS</label>
      <div class="form-control  center merge-bottom-input" name="second">0</div>

      <div class="btn-group btn-block" role="group" aria-label="plus-minus">
        <button type="button" class="btn btn-sm btn-danger minus-button merge-top-left-button" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></button>
        <button type="button" class="btn btn-sm btn-success plus-button merge-top-right-button"><span class="glyphicon glyphicon-plus"></span></button>
      </div><!-- end button group -->
    </div> <!-- end column -->


    <div class=" col-md-3 col-sm-2">
      <label style="color: #77ab63">INFANT: 0-2 YRS</label>
      <div class="form-control  center merge-bottom-input" name="third">0</div>

      <div class="btn-group btn-block" role="group" aria-label="plus-minus">
        <button type="button" class="btn btn-sm btn-danger minus-button merge-top-left-button" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></button>
        <button type="button" class="btn btn-sm btn-success plus-button merge-top-right-button"><span class="glyphicon glyphicon-plus"></span></button>
      </div><!-- end button group -->
    </div> <!-- end column -->
    
    <div class="col-sm-3"></div>
<div class="col-sm-3"></div>
  </div><!-- end row -->
  <div class="tabbable-line">
          <ul class="nav nav-tabs " style="margin-top: 22px;">
            <li class="active">
              <a href="#tab_default_1" data-toggle="tab">
              ECONOMY </a>
            </li>
            <li>
              <a href="#tab_default_2" data-toggle="tab">
              BUSINESS</a>
            </li>
           
          </ul>
           </div>
           <button type="submit" class="btn btn-lg" style="width: 159px;
    padding: 12px;
    margin-top: 20px;">Cancle</button>
           <button type="submit" class="btn btn-danger btn-lg" style="width: 159px;
    padding: 12px;
    margin-top: 20px;">Search</button>
          </div>
        </div>
      </div>
              </div>
              </div>
            </div>
             </form>
            </div>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
</div>

</div>
</form>
</div>
<!--======= flights listing form ================-->
<!--================= container ================-->
<div class="container">
  <div class="row">
    <div class="col-md-3 margin-top-headline">
       
      <div class="row">
       
        <div class="col-md-6 col-xs-6">
          <h5>Filter Results</h5>
        </div>
        <div class="col-md-6 col-xs-6">
          <h5 style="text-align: right;"><a href="#">Reset All</a></h5>
        </div>
      </div>
    <div class="border-bottom"></div>
        <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <h4>No. of Stops</h4>
          <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs" style="border: 1px solid #ddd;">
            <li class="active" style="border-right: 1px solid #ddd;">
              <a href="#tab_default_1" data-toggle="tab" style="padding-left: 31px;">
              <strong> 0 stop</strong> </a>
              <h6 style="padding-left: 22px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>
            </li>
            <li style="border-right: 1px solid #ddd; text-decoration-line: none;">
              <a href="#tab_default_2" data-toggle="tab">
              <strong> 1 stop</strong></a>
               <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>
            </li>
            <li style="text-decoration-line: none;">
              <a href="#tab_default_3" data-toggle="tab">
            <strong> 1+ stop</strong> </a>
             <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>
            </li>
          </ul>
         
        </div>
      </div>
    </div>
  </div>
  <div class="border-bottom"></div>
   <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <h5>Departure time from New Delhi</h5>
          <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs" style="border: 1px solid #ddd;">
            <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">
            <li class="active">
              <a href="#tab_default_1" data-toggle="tab">
             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>
             <h6> Before 6am No Flights</h6>
            </li>
         </div>
         <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">
            <li>
              <a href="#tab_default_2" data-toggle="tab">
                <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>
             <h6> 6 AM-12 PM No Flights</h6>
            </li>
          </div>
          <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;    height: 92px;">
            <li >
              <a href="#tab_default_3" data-toggle="tab">
              <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>
             <h6>12PM - 6 PM </h6>
            </li>
          </div>
          <div class="col-md-3 col-xs-3">
             <li style="text-decoration-line: none;">
              <a href="#tab_default_3" data-toggle="tab">
              <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>
            <h6> After 6  PM </h6>
            </li>
          </div>
          </ul>
         
        </div>
      </div>
    </div>
  </div>
   <div class="border-bottom"></div>
   <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <h5>Departure time from Mumbai</h5>
          <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs" style="border: 1px solid #ddd;">
            <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">
            <li class="active">
              <a href="#tab_default_1" data-toggle="tab">
             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>
             <h6> Before 6am</h6>
            </li>
         </div>
         <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">
            <li>
              <a href="#tab_default_2" data-toggle="tab">
                <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>
             <h6> 6 AM-12 PM </h6>
            </li>
          </div>
          <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;">
            <li >
              <a href="#tab_default_3" data-toggle="tab">
              <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>
             <h6>12PM - 6 PM </h6>
            </li>
          </div>
          <div class="col-md-3 col-xs-3">
             <li style="text-decoration-line: none;">
              <a href="#tab_default_3" data-toggle="tab">
              <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>
            <h6> After 6  PM </h6>
            </li>
          </div>
          </ul>
         
        </div>
      </div>
    </div>
  </div>
  <div class="border-bottom"></div>
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <h5> Airlines</h5>
      <div class="Airlines-section">
      <div class="row">
        
        <div class="col-md-2 col-xs-2">
          <img src="images/unnamed.png">
        </div>
        <div class="col-md-6 col-xs-6">
          <h4>India Go</h4>
          <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>
        </div>
        <div class="col-md-3 col-xs-3">
           
        <i id="4" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>

        </div>
      </div>
    </div>

      <div class="Airlines-section">
      <div class="row">
        
        <div class="col-md-2 col-xs-2">
          <img src="images/unnamed.png">
        </div>
        <div class="col-md-6 col-xs-6">
          <h4>India Go</h4>
          <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>
        </div>
        <div class="col-md-3 col-xs-3">
           
           <i id="3" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>
        </div>
      </div>
    </div>

      <div class="Airlines-section">
      <div class="row">
        
        <div class="col-md-2 col-xs-2">
          <img src="images/unnamed.png">
        </div>
        <div class="col-md-6 col-xs-6">
          <h4>India Go</h4>
          <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>
        </div>
        <div class="col-md-3 col-xs-3">
     <i id="2" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>

        </div>
      </div>
    </div>

      <div class="Airlines-section">
      <div class="row">
        
        <div class="col-md-2 col-xs-2">
          <img src="images/unnamed.png">
        </div>
        <div class="col-md-6 col-xs-6">
          <h4>India Go</h4>
          <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>
        </div>
        <div class="col-md-3 col-xs-3">
   <i id="item" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>

        </div>
      </div>
    </div>
      <div class="Airlines-section">
      <div class="row">
        
        <div class="col-md-2 col-xs-2">
          <img src="images/unnamed.png">
        </div>
        <div class="col-md-6 col-xs-6">
          <h4>India Go</h4>
          <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>
        </div>
        <div class="col-md-3 col-xs-3">
           
 <i id="1" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>

        </div>
      </div>
    </div>
    </div>
  </div>

    </div>
    <div class="col-md-9">
      <div class="row">
<!-----------------------------------------------------Departure Flights----------------------------------------->
        <div class="col-md-6 col-xs-12">
          <div class="row">
          <div class="col-md-2 col-xs-2">
            <img src="{{asset('assets/home/images/59878-200.png')}}" style="width:76%;">
          </div>
            <div class="col-md-10 col-xs-10">
                <?php $stop_point = count($flights->Response->Results[0][0]->Segments[0]); ?>
                 <?php $flight_origin=$flights->Response->Results[0][0]->Segments[0][0]->Origin->Airport->CityName; ?>
                <?php $flight_dest=$flights->Response->Results[0][0]->Segments[0][$stop_point-1]->Destination->Airport->CityName;?> 
                 <?php $flight_start_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointDepartureTime; 
                 $start = date("D, j M, Y", strtotime($flight_start_date));
                 ?>
                 
                 <?php $flight_end_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointArrivalTime; ?>
                
              <strong>{{$flight_origin}} to {{$flight_dest}}</strong> - <span>{{$start}}</span>
              <?php $total_dp_flights = count($flights->Response->Results[0]); ?>
              <h6><span id="total_departure_flights">{{$total_dp_flights." Flights found"}}</span></h6>
          </div>
        </div>
            <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs ">

            <li class="active">
              <a href="#1" data-toggle="tab">
              DURATION </a>
            </li>

            <li>
              <a href="#2" data-toggle="tab">
              DEPARTURE </a>
            </li>
            <li>
              <a href="#3" data-toggle="tab">
             ARRIVAL </a>
            </li>
               <li>
              <a href="#4" data-toggle="tab">
             PRICE </a>
            </li>
          </ul>

          <div class="tab-content">

@if(isset($flights->Response->Results[0]))
<?php $dep_row_id=1; ?>
@foreach($flights->Response->Results[0] as $dep)
            <div class="tab-pane active" id="dep{{$dep_row_id}}">

            <div class="row">
              <div class="col-md-12 col-xs-12 listing-page-tab">
                <div class="col-md-1 col-xs-1">
                  <div class="radio">
                    <label><a href="#">
                      <input type="radio" name="dep_flight" value="{{$dep->ResultIndex}}" onclick="getObIndex(this.value,{{$dep_row_id}})" ></a>
                    </label>
                  </div>
                </div>
                 <div class="col-md-2 col-xs-2">
                   <?php   $dirname = "assets/home/images/AirlineLogo";
                    $filename = glob("$dirname/*{$dep->AirlineCode}*", GLOB_BRACE); ?>
                    @if(isset($filename) && count($filename)>0)
                    <img name="dep_img" src="{{ asset($filename[0])}}" class="img-responsive" style="width: 100%">
                    @endif
                    <a href="#"> 
                    <p name="dep_airline" style="font-size: smaller;">
                        
                    @foreach($dep->Segments as $segment)
                        @if(count($segment)>=1)
                        {{$segment[0]->Airline->AirlineName}}
                        @endif
                        <!--@foreach($segment as $sg)-->
                        <!--  {{$sg->Airline->AirlineName}}-->
                        <!--@endforeach-->
                    @endforeach
                    </p>
                    <p name="dep_flightno" style="font-size:xx-small">
                        @foreach($dep->Segments as $sg)
                            @if(count($sg)>=1)
                               {{$sg[0]->Airline->AirlineCode}}-{{$sg[0]->Airline->FlightNumber}}
                            @endif
                                <!--@foreach($sg as $seg)-->
                                <!--   {{$seg->Airline->AirlineCode}}-{{$seg->Airline->FlightNumber}}-->
                                <!--@endforeach-->
                        @endforeach
                    </p>
                 </a>
                </div>
                <div class="col-md-3 col-xs-2">
                  <h5><a name="dep_datetime" href="#"> 
                     @foreach($dep->Segments as $sg)
                        @if(count($sg)>=1)
                            <?php 
                                $dDatetime=strtotime($sg[0]->StopPointDepartureTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                            {{ $dtime }}
                        @endif
                        <!--@foreach($sg as $time)-->
                        <!--   
                        <!--   {{ $dtime }}-->
                        <!--@endforeach-->
                    @endforeach
                  </a></h5>
                  <?php $stop_point = count($dep->Segments[0]); ?>
                  <span name="dep_origin" style="display:none">{{$dep->Segments[0][0]->Origin->Airport->CityCode}}</span>
                   <span name="dep_destination" style="display:none">{{$dep->Segments[0][$stop_point-1]->Destination->Airport->CityCode}}</span>
                   <span name="dep_discount" style="display:none">0</span>
                  <h6> <a href="#">
                     @foreach($dep->Segments as $sg)
                        
                           @if(count($sg)==1)
                            Non Stop
                           @else
                            {{(count($sg)-1)." Stop" }}
                           @endif
                        
                    @endforeach
                  </a></h6>
                </div>
                <div class="col-md-3 col-xs-3">
                  <h5> <a name="dep_arrtime" href="#"> 
                     @foreach($dep->Segments as $sg)
                         <?php 
                        $stop_point = count($sg); ?>
                        @if(count($sg)>1)
                            <?php 
                            
                                $dDatetime=strtotime($sg[$stop_point-1]->StopPointArrivalTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                            {{ $dtime }}
                        @else
                             <?php 
                                $dDatetime=strtotime($sg[0]->StopPointArrivalTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                            {{ $dtime }}
                        @endif
                        <!--@foreach($sg as $time)-->
                       
                        <!--   {{ $dtime }}-->
                        <!--@endforeach-->
                    @endforeach
                  </a></h5>
                  <h6><a href="#">
                    <?php 
                        $stop_point = count($dep->Segments[0]); ?>
                        @if($stop_point>1)
                        <?php
                        $datetimed = new DateTime($dep->Segments[0][0]->StopPointDepartureTime);
                        $datetimea = new DateTime($dep->Segments[0][$stop_point-1]->StopPointArrivalTime);
                        $interval = $datetimed->diff($datetimea);
                        echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?>
                        @else
                        <?php
                         $datetimed = new DateTime($dep->Segments[0][0]->StopPointDepartureTime);
                        $datetimea = new DateTime($dep->Segments[0][0]->StopPointArrivalTime);
                        $interval = $datetimed->diff($datetimea);
                        echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?>
                        @endif
                
                  </a></h6>
                </div>
                <div class="col-md-3 col-xs-3" style="text-align: right;">
                  <a href="#"> <i class="fa fa-inr" aria-hidden="true"></i>
                    <span name="dep_fare">
                      <?php $basefare = $dep->FareBreakdown[0]->BaseFare;
                            $tax = $dep->FareBreakdown[0]->Tax;
                            $adult_count = $dep->FareBreakdown[0]->PassengerCount;
                            $fare_per_adult = ($basefare+$tax)/$adult_count;
                     ?>
                     {{" ".number_format(round(($fare_per_adult),0))}}
                    </span></a>
                </div>
              </div>
              <div id="dep_tooltip">
                  <?php $stop_point = count($dep->Segments[0]); ?>
                    @for($i=0;$i<$stop_point;$i++)
                         {{$dep->Segments[0][$i]->Airline->AirlineCode}}-{{$dep->Segments[0][$i]->Airline->FlightNumber." ".$dep->Segments[0][$i]->Airline->FareClass}} 
                         {{$dep->Segments[0][$i]->Origin->Airport->CityCode}}
                          <?php 
                                $dDatetime=strtotime($dep->Segments[0][$i]->StopPointDepartureTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                           {{"(".$dtime.")"}} <i class="fa fa-long-arrow-right"></i> {{$dep->Segments[0][$i]->Destination->Airport->CityCode}}
                            <?php 
                                $aDatetime=strtotime($dep->Segments[0][$i]->StopPointArrivalTime);
                                $atime = date("H:i",$aDatetime);
                           ?>
                           {{"(".$atime.")"}} <br>
                    @endfor
              </div>
            </div>
         </div>
      <?php $dep_row_id++; ?>  
@endforeach
@endif
            </div>
        </div>
    </div>
</div>
        
<!--------------------------------Return Flights-------------------------------------->
        <div class="col-md-6 col-xs-12">
          <div class="col-md-2 col-xs-2">
           <img src="{{asset('assets/home/images/59878-200.png')}}" style="width:76%;">
          </div>
            <div class="col-md-10 col-xs-10">
                  <?php $stop_point = count($flights->Response->Results[1][0]->Segments[0]); ?>
              <?php $flight_origin=$flights->Response->Results[1][0]->Segments[0][0]->Origin->Airport->CityName; ?>
                <?php $flight_dest=$flights->Response->Results[1][0]->Segments[0][$stop_point-1]->Destination->Airport->CityName;?> 
                 <?php $flight_start_date=$flights->Response->Results[1][0]->Segments[0][0]->StopPointDepartureTime; 
                 $start = date("D, j M, Y", strtotime($flight_start_date));
                 ?>
                 
                 <?php $flight_end_date=$flights->Response->Results[1][0]->Segments[0][$stop_point-1]->StopPointArrivalTime; ?>
                
              <strong>{{$flight_origin}} to {{$flight_dest}}</strong> - <span>{{$start}}</span>
             <?php $total_rt_flights = count($flights->Response->Results[1]); ?>
              <h6><span id="total_return_flights">{{$total_rt_flights." Flights found"}}</span></h6>
          </div>
            <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs ">

            <li class="active">
              <a href="#5" data-toggle="tab">
              DURATION </a>
            </li>

            <li>
              <a href="#6" data-toggle="tab">
              DEPARTURE </a>
            </li>
            <li>
              <a href="#7" data-toggle="tab">
             ARRIVAL </a>
            </li>
               <li>
              <a href="#8" data-toggle="tab">
             PRICE </a>
            </li>
          </ul>

          <div class="tab-content">

@if(isset($flights->Response->Results[1]))
<?php $ret_row_id=1; ?>
@foreach($flights->Response->Results[1] as $ret)
            <div class="tab-pane active" id="ret{{$ret_row_id}}">

            <div class="row">
              <div class="col-md-12 col-xs-12 listing-page-tab">
                <div class="col-md-1 col-xs-1">
                  <div class="radio">
                  <label><a href="#"><input type="radio" name="ret_flight" value="{{$ret->ResultIndex}}" onclick="getIbIndex(this.value,{{$ret_row_id}})"></a></label>
                  </div>
                </div>
                <div class="col-md-2 col-xs-2">
                   <?php   $dirname = "assets/home/images/AirlineLogo";
                    $filename = glob("$dirname/*{$ret->AirlineCode}*", GLOB_BRACE); ?>
                    @if(isset($filename) && count($filename)>0)
                    <img name="ret_img"  src="{{ asset($filename[0])}}" class="img-responsive" style="width: 100%">
                    @endif
                    <a href="#"> 
                    <p name="ret_airline" style="    font-size: smaller">
                    @foreach($ret->Segments as $segment)
                        @if(count($segment)>=1)
                            {{$segment[0]->Airline->AirlineName}}
                        @endif
                        <!--@foreach($segment as $sg)-->
                        <!--  {{$sg->Airline->AirlineName}}-->
                        <!--@endforeach-->
                    @endforeach
                    </p>
                    <p  name="ret_flightno" style="font-size:xx-small">
                        @foreach($ret->Segments as $sg)
                             @if(count($sg)>=1)
                               {{$sg[0]->Airline->AirlineCode}}-{{$sg[0]->Airline->FlightNumber}}
                            @endif
                                <!--@foreach($sg as $seg)-->
                                <!--   {{$seg->Airline->AirlineCode}}-{{$seg->Airline->FlightNumber}}-->
                                <!--@endforeach-->
                        @endforeach
                    </p>
                 </a>
                </div>
                <div class="col-md-3 col-xs-2">
                  <h5><a name="ret_datetime" href="#"> 
                     @foreach($ret->Segments as $sg)
                         @if(count($sg)>=1)
                            <?php 
                                $dDatetime=strtotime($sg[0]->StopPointDepartureTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                            {{ $dtime }}
                        @endif
                        <!--@foreach($sg as $time)-->
                        
                        <!--   {{ $dtime }}-->
                        <!--@endforeach-->
                    @endforeach
                  </a></h5>
                   <?php $stop_point = count($ret->Segments[0]); ?>
                   <span name="ret_origin" style="display:none">{{$ret->Segments[0][0]->Origin->Airport->CityCode}}</span>
                   <span name="ret_destination" style="display:none">{{$ret->Segments[0][$stop_point-1]->Destination->Airport->CityCode}}</span>
                   <span name="ret_discount" style="display:none">0</span>
                   <h6> <a href="#">
                     @foreach($ret->Segments as $sg)
                         @if(count($sg)==1)
                            Non Stop
                           @else
                            {{(count($sg)-1)." Stop" }}
                           @endif
                    @endforeach
                  </a></h6>
                </div>
                <div class="col-md-3 col-xs-3">
                  <h5> <a name="ret_arrtime" href="#"> 
                     @foreach($ret->Segments as $sg)
                          <?php 
                        $stop_point = count($sg); ?>
                        @if(count($sg)>1)
                            <?php 
                            
                                $dDatetime=strtotime($sg[$stop_point-1]->StopPointArrivalTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                            {{ $dtime }}
                        @else
                             <?php 
                                $dDatetime=strtotime($sg[0]->StopPointArrivalTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                            {{ $dtime }}
                        @endif
                        <!--@foreach($sg as $time)-->
                        
                        <!--   {{ $dtime }}-->
                        <!--@endforeach-->
                    @endforeach
                  </a></h5>
                  <h6><a href="#">
                      <?php 
                        $stop_point = count($ret->Segments[0]); ?>
                        @if($stop_point>1)
                        <?php
                        $datetimed = new DateTime($ret->Segments[0][0]->StopPointDepartureTime);
                        $datetimea = new DateTime($ret->Segments[0][$stop_point-1]->StopPointArrivalTime);
                        $interval = $datetimed->diff($datetimea);
                        echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?>
                        @else
                        <?php
                         $datetimed = new DateTime($ret->Segments[0][0]->StopPointDepartureTime);
                        $datetimea = new DateTime($ret->Segments[0][0]->StopPointArrivalTime);
                        $interval = $datetimed->diff($datetimea);
                        echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?>
                        @endif
                  </a></h6>
                </div>
                <div class="col-md-3 col-xs-3" style="text-align: right;">
                  <a href="#"> <i class="fa fa-inr" aria-hidden="true"></i>
                    <span name="ret_fare">
                      <?php $basefare = $ret->FareBreakdown[0]->BaseFare;
                            $tax = $ret->FareBreakdown[0]->Tax;
                            $adult_count = $ret->FareBreakdown[0]->PassengerCount;
                            $fare_per_adult = ($basefare+$tax)/$adult_count;
                     ?>
                     {{" ".number_format(round(($fare_per_adult),0))}}
                    </span></a>
                </div>
                
              </div>
              <div id="ret_tooltip">
                  <?php $stop_point = count($ret->Segments[0]); ?>
                    @for($i=0;$i<$stop_point;$i++)
                         {{$ret->Segments[0][$i]->Airline->AirlineCode}}-{{$ret->Segments[0][$i]->Airline->FlightNumber." ".$ret->Segments[0][$i]->Airline->FareClass}} 
                         {{$ret->Segments[0][$i]->Origin->Airport->CityCode}}
                          <?php 
                                $dDatetime=strtotime($ret->Segments[0][$i]->StopPointDepartureTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                           {{"(".$dtime.")"}} <i class="fa fa-long-arrow-right"></i> {{$ret->Segments[0][$i]->Destination->Airport->CityCode}}
                            <?php 
                                $aDatetime=strtotime($ret->Segments[0][$i]->StopPointArrivalTime);
                                $atime = date("H:i",$aDatetime);
                           ?>
                           {{"(".$atime.")"}} <br>
                    @endfor
              </div>
            </div>
         </div>
         <?php $ret_row_id++; ?> 
    @endforeach
@endif
            </div>
        </div>
    </div>
</div>
      </div>
    </div>
  </div>
</div>
    @include('frontend.includes.sticky-container')  
	@include('frontend.includes.footer')
<script>
 $(function() {
     var dep_fare=0;
     var ret_fare=0;
     var dep_discount=0;
     var ret_discount=0;
     
    var $dep_radios = $('#dep1 input:radio[name=dep_flight]');
    if($dep_radios.is(':checked') === false) {
        $dep_radios.prop('checked', true);
        $('#ob').val($dep_radios.val());
        
        var dep_image = $('#dep1 img[name=dep_img]').attr('src');
        var dep_airline =  $('#dep1 p[name=dep_airline]').text();
        var dep_flightno =  $('#dep1 p[name=dep_flightno]').text();
        var dep_datetime =  $('#dep1 a[name=dep_datetime]').text();
        var dep_arrtime =  $('#dep1 a[name=dep_arrtime]').text();
            dep_fare =  $('#dep1 span[name=dep_fare]').text();
        var dep_org = $('#dep1 span[name=dep_origin]').text();
        var dep_dest = $('#dep1 span[name=dep_destination]').text();
            dep_discount = $('#dep1 span[name=dep_discount]').text();
    
        $("#dep_airline_logo").html("<img src='"+dep_image+"' class='img-responsive' style='width: 55%'>");
        $("#dep_flightdetails").html(dep_airline+" "+dep_flightno);
        $("#dep_time").html(dep_datetime);
        $("#dep_arrtime").html(dep_arrtime);
        $("#dep_fare").html(dep_fare);
        $("#dep_origin").html(dep_org);
        $("#dep_destination").html(dep_dest);
        
    }
    
     var $ret_radios = $('#ret1 input:radio[name=ret_flight]');
    if($ret_radios.is(':checked') === false) {
        $ret_radios.prop('checked', true);
         $('#ib').val($ret_radios.val());
         
        var ret_image = $('#ret1 img[name=ret_img]').attr('src');
        var ret_airline =  $('#ret1 p[name=ret_airline]').text();
        var ret_flightno =  $('#ret1 p[name=ret_flightno]').text();
        var ret_datetime =  $('#ret1 a[name=ret_datetime]').text();
        var ret_arrtime =  $('#ret1 a[name=ret_arrtime]').text();
             ret_fare =  $('#ret1 span[name=ret_fare]').text();
        var ret_org = $('#ret1 span[name=ret_origin]').text();
        var ret_dest = $('#ret1 span[name=ret_destination]').text();
            ret_discount = $('#ret1 span[name=ret_discount]').text();
    
        $("#ret_airline_logo").html("<img src='"+ret_image+"' class='img-responsive' style='width: 55%'>");
        $("#ret_flightdetails").html(ret_airline+" "+ret_flightno);
        $("#ret_time").html(ret_datetime);
        $("#ret_arrtime").html(ret_arrtime);
        $("#ret_fare").html(ret_fare);
        $("#ret_origin").html(ret_org);
        $("#ret_destination").html(ret_dest);
   }
   var total_discount = parseInt(dep_discount.replace(/,/g,''))+parseInt(ret_discount.replace(/,/g,''));
   $("#total_discount").html((total_discount).toLocaleString('en'));
   var total_fare = parseInt(dep_fare.replace(/,/g,''))+parseInt(ret_fare.replace(/,/g,''));
   $("#total_fare").html((total_fare).toLocaleString('en'));
    
});
</script>