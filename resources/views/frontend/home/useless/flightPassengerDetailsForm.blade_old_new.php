@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')
<style>
#airline_name{
     padding-left: 20px;
    font-weight: 600;
    font-size: smaller;   
 }
#airline_code{
  padding-left: 20px;
    font-size: smaller;   
 }
table tr>td{
     vertical-align:middle!important;
     text-align:center;
 }
.airline_label{
     color: #54af82;
    font-weight: 600;
    padding-right: 10px;
 }
input{
     color: indianred;
 }
.error_msg{
    font-size:12px;
    color:#e53935;
    display:none
}
</style>
<!--@if($errors->any())-->
<!--<h4>{{$errors->first()}}</h4>-->
<!--@endif-->
<div class="pasflightdet">
  @if(!empty($flights)||$flights!=null)
 
<form id="PassengerDetail" name="PassengerDetail" action="{{route('frontend.booking_review')}}" method="post" novalidate="novalidate">
    {{ csrf_field() }}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="trace_id" value={{$trace_id}}>
<input type="hidden" name="result_index" value={{$result_index}}>
@if(isset($result_index_return))
<input type="hidden" name="result_index_return" value={{$result_index_return}}>
@endif

<input type="hidden" name="is_lcc" value="{{$is_lcc}}">

@if(isset($is_lcc_return))
<input type="hidden" name="is_lcc_return" value="{{$is_lcc_return}}">
@endif

<div class="tl_container mt5 mob_top_0">
      <div id="flight_details">
        <table class="table table-responsive table-bordered">
            @if(isset($flights->Segments))
            
            @foreach($flights->Segments as $seg)
                <tr class="warning">
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <?php $dirname = "assets/home/images/AirlineLogo";
                            $filename = glob("$dirname/*{$seg[0]->Airline->AirlineCode}*", GLOB_BRACE); ?>
                           
                            <tr>
                            
                                <td rowspan="2">
                                <img src="{{ asset($filename[0])}}" alt="Airline logo"  class="img-responsive">
                                </td>
                                <td>
                                <span id="airline_name">
                                    <?php $airline_name=$seg[0]->Airline->AirlineName; ?>
                                    {{$airline_name}}
                                </span>
                                </td> 
                               
                               
                            </tr>
                            <tr>
                                <td>
                                     <span id="airline_code">
                                    <?php $airline_code=$seg[0]->Airline->AirlineCode;
                                          $flight_no = $seg[0]->Airline->FlightNumber;
                                          $fare_class = $seg[0]->Airline->FareClass; ?>
                                    {{$airline_code}}-{{$flight_no}}-{{$fare_class}}
                                </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <?php $stop_point = count($seg); ?>
                    <?php $flight_origin=$seg[0]->Origin->Airport->CityName; ?>
                    <?php $flight_dest=$seg[$stop_point-1]->Destination->Airport->CityName;?> 
                    <?php $flight_start_date=$seg[0]->StopPointDepartureTime; 
                     $start = date("D, d M Y| H:i", strtotime($flight_start_date));
                    ?>
                    
                    <?php $flight_end_date=$seg[$stop_point-1]->StopPointArrivalTime; 
                     $end = date("D, d M Y| H:i", strtotime($flight_end_date));
                    ?>
                    <td><span class="airline_label">{{$flight_origin." "}}</span>{{$start." hrs"}}</td>
                    <td><span class="airline_label">{{$flight_dest." "}}</span>{{$end." hrs"}}</td>
                    <td><span class="airline_label">Duration:</span>
                        	<?php $datetimed = new DateTime($seg[0]->StopPointDepartureTime);
                            $datetimea = new DateTime($seg[$stop_point-1]->StopPointArrivalTime);
                            $interval = $datetimed->diff($datetimea);
                             ?>
                            {{$interval->format('%H')."h ".$interval->format('%i')."m"}}
                    </td>
                </tr>
                @endforeach
                @endif
                @if(!empty($ibFlights))
                <tr class="warning">
                    <td>
                        <table cellspacing="0" cellpadding="0">
                            <?php $dirname = "assets/home/images/AirlineLogo";
                            $filename = glob("$dirname/*{$ibFlights->AirlineCode}*", GLOB_BRACE); ?>
                            <tr>
                                <td rowspan="2">
                                    <img src="{{ asset($filename[0])}}" alt="Airline logo"  class="img-responsive">
                                </td>
                                <td>
                                <span id="airline_name">
                                    <?php $airline_name=$ibFlights->Segments[0][0]->Airline->AirlineName; ?>
                                    {{$airline_name}}
                                </span>
                                </td> 
                               
                            </tr>
                            <tr>
                                <td>
                                     <span id="airline_code">
                                    <?php $airline_code=$ibFlights->Segments[0][0]->Airline->AirlineCode;
                                          $flight_no = $ibFlights->Segments[0][0]->Airline->FlightNumber;
                                          $fare_class = $ibFlights->Segments[0][0]->Airline->FareClass; ?>
                                    {{$airline_code}}-{{$flight_no}}-{{$fare_class}}
                                </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <?php $stop_point = count($ibFlights->Segments[0]); ?>
                    <?php $flight_origin=$ibFlights->Segments[0][0]->Origin->Airport->CityName; ?>
                    <?php $flight_dest=$ibFlights->Segments[0][$stop_point-1]->Destination->Airport->CityName;?> 
                    <?php $flight_start_date=$ibFlights->Segments[0][0]->StopPointDepartureTime; 
                     $start = date("D, d M Y| H:i", strtotime($flight_start_date));
                    ?>
                    
                    <?php $flight_end_date=$ibFlights->Segments[0][$stop_point-1]->StopPointArrivalTime; 
                     $end = date("D, d M Y| H:i", strtotime($flight_end_date));
                    ?>
                    <td><span class="airline_label">{{$flight_origin." "}}</span>{{$start." hrs"}}</td>
                    <td><span class="airline_label">{{$flight_dest." "}}</span>{{$end." hrs"}}</td>
                    <td><span class="airline_label">Duration:</span>
                        	<?php $datetimed = new DateTime($ibFlights->Segments[0][0]->StopPointDepartureTime);
                            $datetimea = new DateTime($ibFlights->Segments[0][$stop_point-1]->StopPointArrivalTime);
                            $interval = $datetimed->diff($datetimea);
                             ?>
                            {{$interval->format('%H')."h ".$interval->format('%i')."m"}}
                    </td>
                </tr>
                @endif
            
        </table>
    </div>
	<div class="ht_guestdtl" id="mobile_pax_detail">
	  		<div class="htd_databox">
        <?php
        $adult_count=0;$child_count=0;$infant_count=0;
        if(isset($flights->FareBreakdown[0]))
        $adult_count = ($flights->FareBreakdown[0]->PassengerCount);
        if(isset($flights->FareBreakdown[1]))
        $child_count = ($flights->FareBreakdown[1]->PassengerCount);
        if(isset($flights->FareBreakdown[2]))
        $infant_count = ($flights->FareBreakdown[2]->PassengerCount);
        
        $passenger_count=$adult_count+$child_count+$infant_count; 
        
        
        ?>
        <input type="hidden" value="{{$adult_count}}" id="adult_count">
		<?php $tab=0; ?>
	    @for($i=1;$i<=$passenger_count;$i++)
	        
			 <div class="htd_formbox" id="mobile_pax_d_con">
			 <div class="htd_heading"><dfn class="fleft">Passenger {{$i}} - <span>
			     @if($i<=$adult_count)
			    (Adult {{$i}})
				<?php
                             $price=$adult_price;

                             $pax_type=1;
                             if(isset($adult_price_return))
                                 {
                                     $return_price=$adult_price_return;
                                 }
				?>
			    @elseif($i<=($adult_count+$child_count))
			   <?php $c=1; ?>
			    (Child {{$c}})
				<?php $price=$child_price;
                   if(isset($child_price_return))
                   {
                       $return_price=$child_price_return;
                   }

				$pax_type=2;
				?>
			  <?php  $c++;;; ?>
			  @elseif($i<=($adult_count+$child_count+$infant_count))
                <?php $inf = 1; ?>			  
			    (Infant {{$inf}})
				<?php $price=$affent_price;
				$pax_type=3;
                    if(isset($affent_price_return))
                    {
                        $return_price=$affent_price_return;
                    }
				?>
			    <?php $inf++; ?>
			    @endif
			 </span></dfn>
			 </div>
			 
			 <input type="hidden" name="PaxType[]" value={{$pax_type}}>
			 <input type="hidden" name="BaseFare[]" value="{{$price['base']}}">
			<input type="hidden" name="Tax[]" value="{{$price['tax']}}">
			<input type="hidden" name="TransactionFee[]" value="0">
			<input type="hidden" name="YQTax[]" value="{{$price['yq_tax']}}">
			<input type="hidden" name="AdditionalTxnFeeOfrd[]" value="{{$price['adl_txn_fee_ofd']}}">
			<input type="hidden" name="AdditionalTxnFeePub[]" value="{{$price['adl_txn_fee_pbd']}}">
			<input type="hidden" name="AirTransFee[]" value="0">

                 @if(isset($return_price))
                     <input type="hidden" name="BaseFareReturn[]" value="{{$price['base']}}">
                     <input type="hidden" name="TaxReturn[]" value="{{$price['tax']}}">
                     <input type="hidden" name="TransactionFeeReturn[]" value="0">
                     <input type="hidden" name="YQTaxReturn[]" value="{{$price['yq_tax']}}">
                     <input type="hidden" name="AdditionalTxnFeeOfrdReturn[]" value="{{$price['adl_txn_fee_ofd']}}">
                     <input type="hidden" name="AdditionalTxnFeePubReturn[]" value="{{$price['adl_txn_fee_pbd']}}">
                     <input type="hidden" name="AirTransFeeReturn[]" value="0">

                     @endif



			<?php $e = $i-1;  ?>
                   <div class="fleft width_100">
                    <div class="frm_left">
                        <div class="htd_frmrow">
                        <label>First Name : <sup class="red font11">*</sup></label>                        
                        <code class="fleft width_60 fly_br_rt i_pad68">
                        <select id="Title.{{$e}}" name="Title[]" class="title width_25 fly_select mr5 error" tabindex="{{$tab++}}" aria-required="true" aria-describedby="Title_0-error" required="required">
                            <option value="">Title</option>
                                <option value="Mr" <?php echo old('Title.'.$e)=="Mr"?"selected":""; ?>>Mr</option>
                            @if($pax_type==1)
                                <option value="Mrs" <?php echo old('Title.'.$e)=="Mrs"?"selected":""; ?>>Mrs</option>
                            @endif   
                                <option value="Ms" <?php echo old('Title.'.$e)=="Ms"?"selected":""; ?>>Ms</option>
                                <option value="Miss" <?php echo old('Title.'.$e)=="Miss"?"selected":""; ?>>Miss</option>   
                                <option value="Mstr" <?php echo old('Title.'.$e)=="Mstr"?"selected":""; ?>>Mstr</option>  
                        </select>
                        
                        @if ($errors->has('Title.'.$e))
                            <em id="mobileNo_0-error" class="error">{{ $errors->first('Title.'.$e) }}</em>
                	        <!--<span class="text-danger">{{ $errors->first('Title.'.$e) }}</span>-->
                        @endif
                       
                        <!--<em id="Title_0-error" class="error">Please select a valid title.</em>-->
                        <input class="fname width_68 i_pad_59 error" id="FirstName.{{$e}}"  name="FirstName[]"  type="text" tabindex="{{$tab++}}" value="{{old('FirstName.'.$e)}}" aria-required="true" aria-describedby="FirstName_0-error" required="required">
                        <span id="firstname" class="error_msg">*First Name is required</span>
                        @if ($errors->has('FirstName.'.$e))
                            <em id="mobileNo_0-error" class="error">{{ $errors->first('FirstName.'.$e) }}</em>
                	        <!--<span class="text-danger">{{ $errors->first('FirstName.'.$e) }}</span>-->
                        @endif
                        
                        <span class="fleft width_100" style="display:none">
                            <!-- <em style="display:none;">Please select a valid title.</em>-->
                            <!--<em id="erroFirstName_0" style="display:none;">Please select a valid First Name.</em>-->
                        </span>
                        </code>  
                        </div>                    
                    </div>
                    <div class="frm_right">
                   <div class="htd_frmrow">
                       <label>Last Name :<sup class="red font11">*</sup> </label>
                       <code class="fleft width_60 fly_br_rt i_pad68">                             
                         <input id="LastName.{{$e}}" name="LastName[]" class="lname width_98 error" type="text" tabindex="{{$tab++}}" value="{{old('LastName.'.$e)}}" aria-required="true" aria-describedby="LastName_0-error">
                         <span id="lastname" class="error_msg">*Last Name is required</span>
                         @if ($errors->has('LastName.'.$e))
                             <em id="mobileNo_0-error" class="error">{{ $errors->first('LastName.'.$e) }}</em>
        	                <!--<span class="text-danger">{{ $errors->first('LastName.'.$e) }}</span>-->
                        @endif
                         <!--<em id="erroLastName_0" style="display:none;">Please select a valid Last Name.</em>                              -->
                       </code>
                   </div> 
                </div>

                </div>
                
                   <div class="frm_left">                         
                        <div class="htd_frmrow">
                            <label>Gender : <sup class="red">*</sup></label>
                            <code class="fleft width_60">
                            <select id="Gender.{{$e}}" name="Gender[]" class="gender width_47 fly_select m_width_60 valid" tabindex="{{$tab++}}" aria-required="true">
                                <option>Choose</option>
                                 <option value="1" <?php echo old('Gender.'.$e)==1?"selected":""; ?>>Male</option>
                                <option value="2" <?php echo old('Gender.'.$e)==2?"selected":""; ?>>Female</option>
                            </select>
                             <span id="lastname" class="error_msg">*Please select gender</span>
                             @if ($errors->has('Gender.'.$e))
                                 <em id="mobileNo_0-error" class="error">{{ $errors->first('Gender.'.$e) }}</em>
        	                    <!--<span class="text-danger">{{ $errors->first('Gender.'.$e) }}</span>-->
                            @endif
                             <!--<em class="red font10 ml5" id="genErr_0" style="display:none;">Gender is mandatory.</em>-->
                            </code>
                        </div>                         
                        <div class="htd_frmrow">
                        
                         
                              <label>D.O.B :<sup class="red font11">*</sup></label> 
                            <code class="fleft width_62">
                            <select id="DobDay.{{$e}}" name="DobDay[]" data-pax-type="{{$pax_type}}" class="width65 fly_select" tabindex="{{$tab++}}" required="required"> 
                                 <option value="">Day</option>
									@for($day=1;$day<32;$day++)
                                    <option value="{{$day}}" <?php echo old('DobDay.'.$e)==$day?"selected":""; ?>>{{$day}}</option>
									@endfor
                                    
                            </select>
                            
                            <select id="DobMonth.{{$e}}" name="DobMonth[]" data-pax-type="{{$pax_type}}" class="width77 fly_select" tabindex="{{$tab++}}" required="required">
                                   <option value="">Month</option>
                                
                                   @for($month=1;$month<=12;$month++)

                                    <option value="{{$month}}" <?php echo old('DobMonth.'.$e)==$month?"selected":""; ?>>{{date('F', mktime(0,0,0,$month, 1, date('Y')))}}</option>
                                  @endfor                                                        
                            </select>
                           
                                 <select id="DobYear.{{$e}}" name="DobYear[]" data-pax-type="{{$pax_type}}" class="adultdob width65 fly_select error" tabindex="{{$tab++}}" required="required" aria-required="true" aria-describedby="adultDobYear_0-error">
                             
                                   <option value="">Year</option>
                                   <?php 
                                   if($i<=$adult_count){
                                      
                                    $min_age=idate('Y', strtotime('-12 years'));
                                    $max_age = 1875;
                                  }
                                  elseif($i<=($adult_count+$child_count)){
                                   $current_date = date("Y-m-d");
                                    $min_age=idate('Y', strtotime('-2 years'));
                                    $max_age=$min_age-12;
                                  }
                                   elseif($i<=($adult_count+$child_count+$infant_count)){
                                   
                                    $min_age=idate("Y");
                                    $max_age=$min_age-2;
                                  }
                                   ?>
                                  @for($y=$min_age;$y>$max_age;$y--)
                                    <option value="{{$y}}" <?php echo old('DobYear.'.$e)==$y?"selected":""; ?>>{{$y}}</option>
                                @endfor
                                    
                                
                            </select>
                        <!--@if(($pax_type!=1)||stripos(($flights->Segments[0][0]->Airline->AirlineName),"AirAsia")===true)-->
                        <!--@endif-->
                            @if ($errors->has('DobDay.'.$e)||$errors->has('DobMonth.'.$e)||$errors->has('DobYear.'.$e))
                                @if ($errors->has('DobMonth.'.$e)||$errors->has('DobYear.'.$e))
                                    @if ($errors->has('DobYear.'.$e))
                                        <em id="mobileNo_0-error" class="error">{{"Enter valid Date of birth"}}</em>
        	                           <!--<span class="text-danger">{{"Enter valid Date of birth"}}</span>-->
        	                        @else
        	                           <em id="mobileNo_0-error" class="error">{{"Enter valid Date of birth"}}</em>
                                    @endif
                                @else 
        	                        <em id="mobileNo_0-error" class="error">{{"Enter valid Date of birth"}}</em>
                                @endif
        	                @endif
        	           
                             
                            <!--<em id="adultDobYear_0-error" class="error">Date of birth is mandatory.</em>-->
                            <!--<em class="red font11" id="DobError_0" style="display:none;">Date of birth is mandatory.</em>-->
                            <!-- <em class="red font11" id="pDOBA_0" style="display:none;">Date of birth is mandatory with passport number.</em>-->
                            </code>
                        </div>
                         
                     @if($i==1)
                       <div class="htd_frmrow">
                       
                         <label>Address :<sup class="red font11">*</sup></label>
                       
                          
                            <code class="fleft width_60">
                                <input id="addressLine1" name="addressLine1" type="text" class="width_98 valid" tabindex="{{$tab++}}" value="{{old('addressLine1')}}" aria-required="true">
                                    @if ($errors->has('addressLine1'))
                                    <em id="mobileNo_0-error" class="error">{{$errors->first('addressLine1')}}</em>
            	                    <!--<span class="text-danger">{{$errors->first('addressLine1')}}</span>-->
            	                    @endif
                            </code>
                           
                            <!--<div class="red font11" id="erradd1" style="display:none">Special Character (\) not allowed in Address</div>-->
                        </div>
                        <div class="htd_frmrow">
                          <label>&nbsp; </label>
                            <code class="fleft width_60">
                              <input id="addressLine2" name="addressLine2" type="text" class="width_98" tabindex="{{$tab++}}" value="{{old('addressLine2')}}">
                              @if ($errors->has('addressLine2'))
                                <em id="mobileNo_0-error" class="error">{{$errors->first('addressLine2')}}</em>
        	                   <!--<span class="text-danger">{{$errors->first('addressLine2')}}</span>-->
        	                  @endif
                            </code>
                           
                          <!--<div class="red font11" id="erradd2" style="display:none">Special Character (\) not allowed in Address</div>-->
                        </div>
                        <div class="htd_frmrow">
                           <label id="lblCity">City :</label>
                          <code class="fleft width_60"><input id="City" name="City" type="text" class="width_98 valid" tabindex="{{$tab++}}" value="{{old('City')}}"></code>
                           @if ($errors->has('City'))
                                <em id="mobileNo_0-error" class="error">{{$errors->first('City')}}</em>
        	                   <!--<span class="text-danger">{{$errors->first('City')}}</span>-->
        	               @endif
                        </div>
                       
                       @endif       
                    
                   </div>
                    
                   <div class="frm_right">                        
                        @if($i==1)
                         <div class="htd_frmrow">
                            <label>Mobile : <sup class="red font11">*</sup> </label>
                            <code class="fleft width_60">
                            <input id="mobileNo" name="mobileNo" type="text" class="error  width_98 mobileNOClass" tabindex="{{$tab++}}" value="{{old('mobileNo')}}" aria-required="true" aria-describedby="mobileNo_0-error">
                            @if ($errors->has('mobileNo'))
                                <em id="mobileNo_0-error" class="error">{{$errors->first('mobileNo')}}</em>
        	                   <!--<span class="text-danger">{{$errors->first('mobileNo')}}</span>-->
        	                @endif
                            <!--<em id="mobileNo_0-error" class="error">Please enter Mobile number</em>  -->
                            </code>
                            
                        </div>
                         <div class="htd_frmrow">
                         
                         <label>Email :<sup class="red font11">*</sup></label>
                                                     
                            <code class="fleft width_60">
                               <input id="email" name="email" type="text" class="emailClass width_98 error" tabindex="{{$tab++}}" value="{{old('email')}}" aria-required="true" aria-describedby="email-error">
                               @if ($errors->has('email'))
                                <em id="mobileNo_0-error" class="error">{{$errors->first('email')}}</em>
        	                   <!--<span class="text-danger">{{$errors->first('email')}}</span>-->
        	                   @endif
                               <!--<em id="email-error" class="error">This field is required.</em>-->
                                <!--<em class="red font11" id="emailerg" style="display:none;">Please Enter valid email.</em>-->
                            </code>
                             
                         </div>
                                             
                        @endif
                        
                       <div class="htd_frmrow">                  
                            <label>Country :</label>
                            <code class="fleft width_62">
                            <select id="Country.{{$e}}" name="Country[]" class="country width_98 fly_select width_62 valid" tabindex="{{$tab++}}" aria-required="true">
                                <option value="">Choose</option>
                                    @if(isset($countries))                                
                                        @foreach($countries as $ctr)
                                         <option value="{{$ctr->sortname}}" <?php echo old('Country.'.$e)==$ctr->sortname? "selected":""; ?>>{{$ctr->name}}</option>
                                        @endforeach
                                    @endif
                                         
                            </select>
                            @if ($errors->has('Country.'.$e))
                                <em id="mobileNo_0-error" class="error">{{$errors->first('Country.'.$e)}}</em>
        	                @endif
                            <!--<em style="display:none;">Please Select Country</em>-->
                            </code>
                         </div>
                       
                        
                         <div class="htd_frmrow">
                            <label>Nationality : <sup class="red font11">*</sup></label>
                            <code class="fleft width_62">
                            <select id="Nationality.{{$e}}" name="Nationality[]" class="nationality  width_98 fly_select width_62" tabindex="{{$tab++}}">
                                <option value="">Choose</option>
                                @if(isset($countries))
                                @foreach($countries as $ctr)
                                         <option value="{{$ctr->sortname}}" <?php echo old('Nationality.'.$e)==$ctr->sortname? "selected":""; ?>>{{$ctr->name}}</option>
                                          @endforeach                  
                                          @endif
                            </select>
                             
                          <!--<em id="bhuerr_0" class="red font11" style="display:none;">Please Select Nationality</em>-->
                            </code>
                        </div>
                          
                         
                          
                                                    
                    </div>
                   
                     <div class="fleft width_100">
                      <div class="frm_left">
                      
                    <div class="htd_frmrow">
                         
                          <label>Passport No. :</label>
                         
                           
                            <code class="fleft width_60">
                            <input id="PassportNo.{{$e}}" name="PassportNo[]" type="text" maxlength="16" class="passport  width_98 valid" tabindex="{{$tab++}}" value="{{old('PassportNo.'.$e)}}">
                            <br><em class="red font11" id="passNo_0" style="display:none;">Please Enter Passport No.</em>
                            </code>
                           
                        </div>
                       

                   </div>
                      <div class="frm_right">
  
                         <div class="htd_frmrow"> 
                           
                          <label>Passport Exp :</label>
                         
                            <code class="fleft width_62">
                            <select id="PassExpDay.{{$e}}" name="PassExpDay[]" class="width65 fly_select" tabindex="{{$tab++}}">
                                <option  value="">Day</option>
                                    @for($d=1;$d<32;$d++)
                                    <option value="{{$d}}" <?php echo old('PassExpDay.'.$e)==$d? "selected":""; ?>>{{$d}}</option>
                                    @endfor
                                    
                            </select>
                            <select id="PassExpMonth.{{$e}}" name="PassExpMonth[]" class="width77 fly_select" tabindex="{{$tab++}}">
                                <option value="">Month</option>
                                
                                  @for($month=1;$month<=12;$month++)
                                    <option value="{{$month}}" <?php echo old('PassExpMonth.'.$e)==$month? "selected":""; ?>>{{date('F', mktime(0,0,0,$month, 1, date('Y')))}}</option>
                                  @endfor                                
                                    
                                
                            </select>
                            <select id="PassExpYear.{{$e}}" name="PassExpYear[]" class="passExp width65 fly_select" tabindex="{{$tab++}}">
                                <option value="" selected="selected">Year</option>
                                    @for($year=2018;$year<2100;$year++)
                                    <option value="{{$year}}" <?php echo old('PassExpYear.'.$e)==$year? "selected":""; ?>>{{$year}}</option>
                                    @endfor
                                
                            </select>
                             <br><em class="red font11" id="pED_0" style="display:none;">Please Enter Valid Date.</em>
                            </code>                            
                        </div>
                          
                      </div>
                     </div>
                   <div class="fleft width_100">
                   
                    <div class="frm_left">
                   
                       
                    </div>
                    <div class="frm_right">
                           
                           
                       
                    </div>
                </div>
                @if($i<=($adult_count+$child_count))
					<?php $ssr_passenger = $adult_count+$child_count; ?>
				<div class="fleft width_100">
										 <div class="frm_left">
												 @if(isset($ssr->Response->Baggage))
												 
												 @foreach($ssr->Response->Baggage as $key=>$baggage)
												 <?php $baggage_result = count($baggage); ?>
															<div class="htd_frmrow">
																<label class="air_bagg"> &nbsp;</label> 
																<code class="fleft width_62"> {{$baggage[0]->Origin}} - {{$baggage[$baggage_result-1]->Destination}} </code>
															
																<label class="air_bagg">Select Excess Baggage<br>(Extra charges will be applicable) : </label>                                            
																<code class="fleft width_62">                                                                
															         @if($key==0)
																    <select id="baggage_{{$i}}" onchange="return selectedBaggage({{$ssr_passenger}});" name="baggage[]" class="baggage width_98 fly_select width_96" tabindex="{{$tab++}}">
																    @elseif($key==1)
																    <select id="baggage_ret_{{$i}}" onchange="return selectedReturnBaggage({{$ssr_passenger}});" name="baggage_ret[]" class="baggage width_98 fly_select width_96" tabindex="{{$tab++}}">
																    @endif
															     
																        @foreach($baggage as $bg)
																			<option value="{{$bg->Code}}">
																			   @if($bg->Weight==0||$bg->Weight==NULL)
																			   {{$bg->Code}}
																			   @else
																			  {{$bg->Weight."Kg - ₹".$bg->Price}}
																			   @endif
																			</option>                                                                
																		   
																		@endforeach
																		  
																</select>
																<span class="baggage_details">
																	<kbd>Baggage Weight : <code style="float:none;" id="bagWt_0_indi_1_seg_0">0</code> Kg</kbd>
																	<kbd>Baggage Charges : <em>Rs.</em> <code style="float:none;" id="bagCha_0_indi_1_seg_0">0</code>
																		
																	</kbd>
																	 <kbd id="bagdes_0_indi_1_seg_0" style="display:none;">Baggage Desc : <code style="float:none;" id="bagdesc_0_indi_1_seg_0"></code></kbd>
																 
																</span>
															   </code>
							                            </div>
							                     @endforeach
							                    @endif
							
							
													@if(isset($ssr->Response->MealDynamic))
													   @foreach($ssr->Response->MealDynamic as $key=>$mealdynamic)
													    <?php $meal_result = count($mealdynamic); ?>
															<div class="htd_frmrow">
															  <label class="air_bagg">Meal Preferences : </label>
															  <code class="fleft width_62">
															  {{$mealdynamic[0]->Origin}} - {{$mealdynamic[$meal_result-1]->Destination}} 
															  
															    @if($key==0)
																    <select id="meal_{{$i}}" onchange="return selectedMeal({{$ssr_passenger}})" name="meal[]" class="mealExtra width_98 fly_select width_96" tabindex="{{$tab++}}">
															    @elseif($key==1)
																    <select id="meal_ret_{{$i}}" onchange="return selectedReturnMeal({{$ssr_passenger}})" name="meal_ret[]" class="mealExtra width_98 fly_select width_96" tabindex="{{$tab++}}">
																@endif
															  
															   @foreach($mealdynamic as $meal)
																  <option value="{{$meal->Code}}">
																      @if($meal->Price==0||$meal->Price==null)
																      {{$meal->Code}}
																      @else
																      {{$meal->AirlineDescription." - Rs.".$meal->Price}}
																      @endif
																    </option>
															@endforeach
															  </select>
															<span class="baggage_details">
															   <kbd>Meal Qunatity : <code style="float:none;" id="mealQuat_0_indi_1_seg_0">0</code> Platter</kbd>
															   <kbd>Meal Charges :<em>Rs.</em> <code style="float:none;" id="mealCha_0_indi_1_seg_0">0</code>
																
															  </kbd>
															</span>
															</code>
														 </div>
													    @endforeach	
													@endif
													
	<!-----------------------------------------------SSR return Baggage and Meal Details-------------------------------------------------->
	                                                
														 @if(isset($ssr_return->Response->Baggage[0]))
															<div class="htd_frmrow">
																<label class="air_bagg"> &nbsp;</label>
																<code class="fleft width_62"> {{$ssr_return->Response->Baggage[0][0]->Origin}} - {{$ssr_return->Response->Baggage[0][0]->Destination}} </code>
															
																<label class="air_bagg">Select Excess Baggage<br>(Extra charges will be applicable) : </label>                                            
																<code class="fleft width_62">                                                                
															   
																<select id="baggage_ret_{{$i}}" onchange="return selectedReturnBaggage({{$ssr_passenger}});" name="baggage_ret[]" class="baggage width_98 fly_select width_96" tabindex="{{$tab++}}">
																						
																			@foreach($ssr_return->Response->Baggage[0] as $bgr)
																			<option value="{{$bgr->Code}}">
																			   @if($bgr->Weight==0||$bgr->Weight==NULL)
																			   {{$bgr->Code}}
																			   @else
																			  {{$bgr->Weight."Kg - ₹".$bgr->Price}}
																			   @endif
																			</option>                                                                
																		   
																			@endforeach
																		  
																</select>
																<span class="baggage_details">
																	<kbd>Baggage Weight : <code style="float:none;" id="bagWt_0_indi_1_seg_0">0</code> Kg</kbd>
																	<kbd>Baggage Charges : <em>Rs.</em> <code style="float:none;" id="bagCha_0_indi_1_seg_0">0</code>
																		
																	</kbd>
																	 <kbd id="bagdes_0_indi_1_seg_0" style="display:none;">Baggage Desc : <code style="float:none;" id="bagdesc_0_indi_1_seg_0"></code></kbd>
																 
																</span>
															   </code>
							</div>
							@endif
							
							
													@if(isset($ssr_return->Response->MealDynamic[0]))
													   
															<div class="htd_frmrow">
															  <label class="air_bagg">Meal Preferences : </label>
															  <code class="fleft width_62">
															  {{$ssr_return->Response->MealDynamic[0][0]->Origin}} - {{$ssr_return->Response->MealDynamic[0][0]->Destination}} 
															  <select id="meal_ret_{{$i}}" onchange="return selectedReturnMeal({{$ssr_passenger}})" name="meal_ret[]" class="mealExtra width_98 fly_select width_96" tabindex="{{$tab++}}">
															   @foreach($ssr_return->Response->MealDynamic[0] as $rmeal)
																  <option value="{{$rmeal->Code}}">
																      @if($rmeal->Price==0||$rmeal->Price==null)
																      {{$rmeal->Code}}
																      @else
																      {{$rmeal->AirlineDescription." - Rs.".$rmeal->Price}}
																      @endif
																    </option>
															@endforeach
															  </select>
															<span class="baggage_details">
															   <kbd>Meal Quantity : <code style="float:none;" id="mealQuat_0_indi_1_seg_0">0</code> Platter</kbd>
															   <kbd>Meal Charges :<em>Rs.</em> <code style="float:none;" id="mealCha_0_indi_1_seg_0">0</code>
																
															  </kbd>
															</span>
															</code>
														 </div>
														
													@endif
													
																	   <div class="htd_frmrow">
																		  
																			<code class="fleft width_62">  
																			  
																				<input type="hidden" id="Seat_0_indi_1_seg_0" name="Seat_0_indi_1_seg_0"> 
																				 </code>
																		   </div>
																	   
																	   <div class="htd_frmrow">
																		  
																			<code class="fleft width_62">  
																			  
																				<input type="hidden" id="Seat_0_indi_1_seg_1" name="Seat_0_indi_1_seg_1"> 
																				 </code>
																		   </div>
																			
										</div>

																		
										</div>                              
									@endif	  
								<input type="hidden" id="userAction_0" name="userAction_0" value="">
								<input type="hidden" id="CustomerID_0" name="CustomerID_0" value="">
                </div>
                @if($i==1)
			        <div class="fleft width_100">
					<div class="htd_heading">
					
				  <dfn class="fleft"><input id="chkBoxGST" name="chkBoxGST" type="checkbox">  GST Detail <b class="red">(Note : Please fill GST Details only for corporate customer)</b></dfn>
				  </div>
					 <div class="htd_databox" id="GSTDataBlock" style="display:none;">
					<div class="htd_formbox">
					<div class="frm_left">
					 <div class="htd_frmrow">
					 <label>GST Number :</label>
					 <code class="fleft width_60">
					 <input type="text" class="width_98 valid" id="gstNumber" name="gstNumber" value="">
					<span id="errormandatorynumber" class="error" style="display:none;">Please enter GST Number.</span>
					 </code>
					</div>  
					<div class="htd_frmrow">
					 <label>GST Company Contact No: </label>
					 <code class="fleft width_60">
					 <input type="text" class="width_98 valid" id="gstContact" name="gstContact" value="">
					<span id="errorcontactno" class="error" style="display:none;">Please enter 10-14 digit mobile number.</span>
					<span id="errorMandatorycontactno" class="error" style="display:none;">Please enter Contact Number.</span>
					 </code>
					</div>
					 <div class="htd_frmrow">
					 <label>GST Company Email : </label>
					 <code class="fleft width_60">
					 <input type="text" class="width_98 valid" id="gstEmail" name="gstEmail" value="">
					<span id="erroremail" class="error" style="display:none;">Please enter Correct Email Format.</span>
					 <span id="errormandatory" class="error" style="display:none;">Please enter Email.</span>
					 </code>
					</div>
					</div>
					<div class="frm_right">
					 <div class="htd_frmrow">
					 <label>GST Company Name : </label>
					 <code class="fleft width_60">
					 <input type="text" class="width_98 valid" id="gstName" name="gstName" value="">
					 <span id="errorMandatorygstName" class="error" style="display:none;">Please enter GST Name.</span>
					 </code>
					</div>  
					<div class="htd_frmrow">
					 <label>GST Company Address : </label>
					 <code class="fleft width_60">
					 <input type="text" class="width_98 valid" id="gstaddress" name="gstaddress" maxlength="65" value="">
					 <span id="erroraddress" class="error" style="display:none;">Special Character (\) not allowed in Address</span>
					 <span id="errormandatoryaddress" class="error" style="display:none;">Please enter Address.</span>
					 </code>
					</div>
					</div>
					</div>
					</div>
					<div class="htd_heading" style="padding: 2px;"></div>
					</div>
					<div class="fleft width_100">
					<div class="htd_heading">
					
				  <dfn class="fleft"><input id="chkBoxRoamer" name="chkBoxRoamer" type="checkbox"> Push Booking to Roamer </dfn>
				  </div>
					 <div class="htd_databox" id="RoamerDataBlock" style="display:none;">
					<div class="htd_formbox">
					<div class="frm_left">
					<div class="htd_frmrow">
					 <label>Customer Mobile No: </label>
					 <code class="fleft width_60">
						 <input class="fleft width_20 pad_em mb5" type="text" value="+91" id="roamerCountryCode" name="roamerCountryCode">
						 <input class="fleft width_70 pad_em mb5" type="text" id="roamerContact" name="roamerContact" maxlength="10">
					<span id="errorroamer" class="error" style="display:none;">Please enter 12 digit mobile number with Country Code.</span>
					 </code>
					</div>
					 
					</div>
					
					</div>
					</div>
					<div class="htd_heading" style="padding: 2px;"></div>
					</div>
					@endif
	@endfor
				
						<div class="htd_head"><h1>Fare Rule</h1></div>
						<div class="htd_databox">
				   <div class="htd_databoxin">
						
						
							<div class="htd_formboxfarerule htd_formboxfarerulechangedheight text-content" id="divFareRule">
						   
						        @foreach($fareRule->Response->FareRules as $iv)
						        
								<div class="fleft width_100">
									<div class="htd_frmrow">
									    @for($i=0;$i<count($flights->Segments);$i++) 
									        @for($j=0;$j<count($flights->Segments[$i]);$j++)
        									    @if($flights->Segments[$i][$j]->Origin->Airport->CityCode==$iv->Origin && $flights->Segments[$i][$j]->Destination->Airport->CityCode==$iv->Destination) 
        										<code>{{$iv->Airline}}:{{$flights->Segments[$i][$j]->Origin->Airport->CityName}}({{$iv->Origin}}) -{{$flights->Segments[$i][$j]->Destination->Airport->CityName}} ({{$iv->Destination}})</code>
        										<?php break 2; ?>
        										@endif
    										    
    										@endfor
    										
										@endfor
									</div>
								</div>
									<div class="fleft width_100">
										 <?php $farerule= nl2br($iv->FareRuleDetail); ?>
										 <?php echo $farerule; ?> 
									</div>
							
							    @endforeach
						    @if(isset($fareRule_return->Response->FareRules))
						        @foreach($fareRule_return->Response->FareRules as $fr)
						            <div class="fleft width_100">
									<div class="htd_frmrow">
									    @foreach($ibFlights->Segments[0] as $seg)
									    @if($seg->Origin->Airport->CityCode==$fr->Origin)
										<code>{{$fr->Airline}}:{{$seg->Origin->Airport->CityName}}({{$fr->Origin}}) -{{$seg->Destination->Airport->CityName}} ({{$fr->Destination}})</code>
										@endif
										@endforeach
									</div>
								</div>
									<div class="fleft width_100">
										 <?php $farerule= nl2br($fr->FareRuleDetail); ?>
										 <?php echo $farerule; ?> 
									</div>
						        @endforeach
						    @endif
							</div>
						
				   </div>
				</div>
				<div class="btnrow">
					<a id="seatMapSelection" href="#" class="btn_main fright mr5">Select Seat(s) </a>
				  </div>
				  <div class="btnrow">
					<input  type="submit" id="continue" href="#!" class="btn_main fright mr5" value='Proceed to Booking Review'>
				  </div>
		</div>
	</div>
	<div class="special_deals mobfilter_display_none" id="mobile_queue_filter">
             <a id="filterCloseBtn" class="fl_close_for_mobile ipad_not desktop_not" style="position:absolute; right:10px; top:7px;"></a>
            
                <h2>Promo Code </h2>                  
                <div class="deal_cont" id="lblPromoCode">
                         
                <div class="drdr_in">                        
                    <label class="promo_error" id="errorPromoCode"></label>
                        
                        <input id="PromoCode" name="PromoCode" type="text" class="promoinput fleft valid" value="" placeholder="Promo Code" maxlength="10">
                         
                    <div class="fright">
						@if(access()->user())
                        <a id="PromoCodeApply_login" href="#" class="btn_main fright mr5" >Apply</a>
						@else
						<a id="PromoCodeApply" href="#" class="btn_main fright mr5" >Apply</a>
						@endif
						
                </div>
                </div>       
                       
                           
                          
                           
                </div>
                <br><br>
            
            
            <h2>Sale Summary </h2>
                  <div class="deal_head cursor">
                      
                      <a href="#" id="divSalesSummaryHead">+ Show Details</a>
                      
                  </div>
            <div class="deal_cont">
                   
                @foreach($flights->Segments as $flsegment)
                    @foreach($flsegment as $flseg)
                    <?php $flight_origin=$flseg->Origin->Airport->CityCode; ?>
                    <?php $flight_dest=$flseg->Destination->Airport->CityCode;?> 
                    <?php $flight_start_date=$flseg->StopPointDepartureTime; 
                     $start = date("d M y", strtotime($flight_start_date));
                     $start_time = date("H:i",strtotime($flight_start_date));
                    ?>
                    
                    <?php $flight_end_date=$flseg->StopPointArrivalTime; 
                     $end = date("d M y", strtotime($flight_end_date));
                     $end_time = date("H:i",strtotime($flight_end_date));
                    ?>
                   <div class="deal_head">
                    <div class="flight_details_top bord_bottom">
                        <em class="width65 bold">{{$start}} </em>
                        <em class="width65 bold align_center"> 
                             <?php $airline_code=$flseg->Airline->AirlineCode;
                                  $flight_no = $flseg->Airline->FlightNumber;
                                  $fare_class = $flseg->Airline->FareClass; ?>
                                    {{$airline_code}}{{$flight_no}}
                        </em>
                        <em class="width65 align_right bold">{{$fare_class}} -Class</em>
                    </div>
                     <div class="flight_details_top">
                        <em class="width65 font-normal">Dept:</em>
                        <em class="width65 font-normal align_center">
                            {{$flight_origin}}
                        </em>
                        <em class="width65 align_right font-normal">{{"@".$start_time." hrs"}}</em>
                    </div>
                     
                     <div class="flight_details_top">
                        <em class="width65 font-normal">Arr:</em>
                        <em class="width65 font-normal align_center">
                            {{$flight_dest}}
                        </em>
                        <em class="width65 align_right font-normal">{{"@".$end_time." hrs"}}</em>
                    </div>           
                 </div>
                 @endforeach
                @endforeach
                @if(isset($ibFlights->Segments[0]))
                @foreach($ibFlights->Segments[0] as $flseg)
                    <?php $flight_origin=$flseg->Origin->Airport->CityCode; ?>
                    <?php $flight_dest=$flseg->Destination->Airport->CityCode;?> 
                    <?php $flight_start_date=$flseg->StopPointDepartureTime; 
                     $start = date("d M y", strtotime($flight_start_date));
                     $start_time = date("H:i",strtotime($flight_start_date));
                    ?>
                    
                    <?php $flight_end_date=$flseg->StopPointArrivalTime; 
                     $end = date("d M y", strtotime($flight_end_date));
                     $end_time = date("H:i",strtotime($flight_end_date));
                    ?>
                   <div class="deal_head">
                    <div class="flight_details_top bord_bottom">
                        <em class="width65 bold">{{$start}} </em>
                        <em class="width65 bold align_center"> 
                             <?php $airline_code=$flseg->Airline->AirlineCode;
                                  $flight_no = $flseg->Airline->FlightNumber;
                                  $fare_class = $flseg->Airline->FareClass; ?>
                                    {{$airline_code}}{{$flight_no}}
                        </em>
                        <em class="width65 align_right bold">{{$fare_class}} -Class</em>
                    </div>
                     <div class="flight_details_top">
                        <em class="width65 font-normal">Dept:</em>
                        <em class="width65 font-normal align_center">
                            {{$flight_origin}}
                        </em>
                        <em class="width65 align_right font-normal">{{"@".$start_time." hrs"}}</em>
                    </div>
                     
                     <div class="flight_details_top">
                        <em class="width65 font-normal">Arr:</em>
                        <em class="width65 font-normal align_center">
                            {{$flight_dest}}
                        </em>
                        <em class="width65 align_right font-normal">{{"@".$end_time." hrs"}}</em>
                    </div>           
                 </div>
                @endforeach
                @endif
                   
                          <div id="mainFareDiv" class="deal_price" style="display:block;">
                    <h3>Fare / Pax Type</h3>
                     
                         <div class="drdr">
                       <div class="drdr_in">
                      <div class="raterow">
                        <label>Adult</label>
                        <code> 
                        @if(isset($adult_price)&& $adult_price!="")
                            @if(isset($adult_price_return)&& $adult_price_return!="") 
                                <?php $ib_ob_adult_price = $adult_price['base']+$adult_price_return['base'] ?>
                                {{"Rs. ".$ib_ob_adult_price}}
                            @else
                                {{"Rs. ".$adult_price['base']}}
                            @endif
                        @else
                        Rs. 0.00
                        @endif
                        </code>
                     </div>
                      <div class="raterow">
                        <label>Tax and S.Charges</label>
                        <code>
                            @if(isset($adult_price)&& $adult_price!="")
                                <?php $ob_tns=$adult_price['tax']; ?>
                                 @if(isset($adult_price_return)&& $adult_price_return!="") 
                                <?php 
                                $ib_tns=$adult_price_return['tax']; //$adult_price_return['yq_tax'];
                                $ib_ob_tns = $ob_tns+$ib_tns; ?>
                                {{"Rs. ".$ib_ob_tns}}
                                @else
                                    {{"Rs. ".$ob_tns}}
                                @endif
                           
                            @else
                            Rs. 0.00
                            @endif
                        </code>
                      </div>
                      <div class="raterow">
                        <label>T. Fee</label>
                        <code>
                            
                            Rs.0.00 
                               
                        </code>
                      </div>
                      
                           
                    </div>
                       </div>  
                         <div class="drdr_in total">
                       <div class="raterow">
                         <label>Total<br></label>
                         <code><b class="bold"> 
                          @if(isset($adult_price)&& $adult_price!="")
                         <?php $ob_adult_total = $adult_price['base']+$adult_price['tax']; //$adult_price['yq_tax']; ?>
                         
                                
                                 @if(isset($adult_price_return)&& $adult_price_return!="") 
                                <?php 
                                $ib_adult_total=$adult_price_return['base']+$adult_price_return['tax'];//$adult_price_return['yq_tax'];
                                $ib_ob_adult_total = $ob_adult_total+$ib_adult_total; ?>
                                {{"Rs. ".$ib_ob_adult_total}}
                                @else
                                    {{"Rs. ".$ob_adult_total}} 
                                @endif
                           @else
                           Rs. 0.00
                           @endif
                        </b></code>
                      </div>
                   </div>
                       
                         <div class="drdr">
                       <div class="drdr_in">
                      <div class="raterow">
                        <label>Child</label>
                        <code> 
                        @if(isset($child_price)&& $child_price!="")
                            @if(isset($child_price_return)&& $child_price_return!="") 
                                <?php $ib_ob_child_price = $child_price['base']+$child_price_return['base'] ?>
                                {{"Rs. ".$ib_ob_child_price}}
                            @else
                                {{"Rs. ".$child_price['base']}}
                            @endif
                        @else
                        Rs. 0.00
                        @endif
                        </code>
                     </div>
                      <div class="raterow">
                        <label>Tax and S.Charges</label>
                        <code> 
                            @if(isset($child_price)&& $child_price!="")
                                <?php $ob_tns=$child_price['tax'];//$child_price['yq_tax']; ?>
                                 @if(isset($child_price_return)&& $child_price_return!="") 
                                <?php 
                                $ib_tns=$child_price_return['tax'];//$child_price_return['yq_tax'];
                                $ib_ob_tns = $ob_tns+$ib_tns; ?>
                                {{"Rs. ".$ib_ob_tns}}
                                @else
                                    {{"Rs. ".$ob_tns}}
                                @endif
                           
                            @else
                            Rs. 0.00
                            @endif
                        </code>
                      </div>
                      <div class="raterow">
                        <label>T. Fee</label>
                        <code>Rs.0.00 
                               
                        </code>
                      </div>
                      
                           
                    </div>
                       </div>  
                         <div class="drdr_in total">
                       <div class="raterow">
                         <label>Total <br></label>
                         <code><b class="bold"> 
                          @if(isset($child_price)&& $child_price!="")
                         <?php $ob_child_total = $child_price['base']+$child_price['tax'];//$child_price['yq_tax']; ?>
                         
                                
                                 @if(isset($child_price_return)&& $child_price_return!="") 
                                <?php 
                                $ib_child_total=$child_price_return['base']+$child_price_return['tax'];//$child_price_return['yq_tax'];
                                $ib_ob_child_total = $ob_child_total+$ib_child_total; ?>
                                {{"Rs. ".$ib_ob_child_total}}
                                @else
                                    {{"Rs. ".$ob_child_total}} 
                                @endif
                           @else
                           Rs. 0.00
                           @endif
                        </b></code>
                      </div>
                   </div>
                       
                        <div class="drdr">
                        <div class="drdr_in">
                        <div class="raterow">
                        <label>Infant</label>
                        <code> 
                        @if(isset($affent_price)&& $affent_price!="")
                            @if(isset($affent_price_return)&& $affent_price_return!="") 
                                <?php $ib_ob_affent_price = $affent_price['base']+$affent_price_return['base'] ?>
                                {{"Rs. ".$ib_ob_affent_price}}
                            @else
                                {{"Rs. ".$affent_price['base']}}
                            @endif
                        @else
                        Rs. 0.00
                        @endif
                        </code>
                     </div>
                      <div class="raterow">
                        <label>Tax and S.Charges</label>
                        <code>
                         @if(isset($affent_price)&& $affent_price!="")
                                <?php $ob_tns=$affent_price['tax'];//$affent_price['yq_tax']; ?>
                                 @if(isset($affent_price_return)&& $affent_price_return!="") 
                                <?php 
                                $ib_tns=$affent_price_return['tax'];//$affent_price_return['yq_tax'];
                                $ib_ob_tns = $ob_tns+$ib_tns; ?>
                                {{"Rs. ".$ib_ob_tns}}
                                @else
                                    {{"Rs. ".$ob_tns}}
                                @endif
                           
                            @else
                            Rs. 0.00
                            @endif
                        </code>
                      </div>
                      <div class="raterow">
                        <label>T. Fee</label>
                        <code>Rs.0.00 
                               
                        </code>
                      </div>
                      
                           
                    </div>
                       </div>  
                         <div class="drdr_in total">
                       <div class="raterow">
                         <label>Total <br></label>
                         <code><b class="bold">
                          @if(isset($affent_price)&& $affent_price!="")
                         <?php $ob_affent_total = $affent_price['base']+$affent_price['tax'];//$affent_price['yq_tax']; ?>
                         
                                
                                 @if(isset($affent_price_return)&& $affent_price_return!="") 
                                <?php 
                                $ib_affent_total=$affent_price_return['base']+$affent_price_return['tax'];//$affent_price_return['yq_tax'];
                                $ib_ob_affent_total = $ob_affent_total+$ib_affent_total; ?>
                                {{"Rs. ".$ib_ob_affent_total}}
                                @else
                                    {{"Rs. ".$ob_affent_total}} 
                                @endif
                           @else
                           Rs. 0.00
                           @endif
                        </b></code>
                      </div>
                   </div>
                       
                    <h3 class="mt5imp">Total Fare</h3>
                    
                          <div class="drdr_in">
                           <div class="raterow">
                             <label>Adult x <?php echo $adult_count; ?></label>
                            <code> Rs.<code id="total_adult_cost">
                                 
                                 
                                 @if(!empty($ib_ob_adult_total)&& $ib_ob_adult_total!=" ")
                                 {{$ib_ob_adult_total*$adult_count}}
                                 @elseif(!empty($ob_adult_total)&& $ob_adult_total!=" ")
                                    {{$ob_adult_total}}
                                 @else
                                   {{"0.00"}}
                                 @endif
                             </code>
                            </code>
                           </div>
                         </div>
                      
                          <div class="drdr_in">
                           <div class="raterow">
                             <label>Child x <?php echo $child_count; ?></label>
                            <code>Rs.<code id="total_child_cost">
                                  @if(!empty($ib_ob_child_total)&& $ib_ob_child_total!=" ")
                                 {{$ib_ob_child_total*$child_count}}
                                 @elseif(!empty($ob_child_total)&& $ob_child_total!=" ")
                                    {{$ob_child_total}}
                                 @else
                                   {{"0.00"}}
                                 @endif
                                  </code>
                            </code>
                           </div>
                         </div>
                      
                          <div class="drdr_in">
                           <div class="raterow">
                             <label>Infant x <?php echo $infant_count; ?></label>
                            <code>Rs.<code id="total_infant_cost">
                                   @if(!empty($ib_ob_affent_total)&& $ib_ob_affent_total!=" ")
                                 {{$ib_ob_affent_total*$infant_count}}
                                 @elseif(!empty($ob_affent_total)&& $ob_affent_total!=" ")
                                    {{$ob_affent_total}}
                                 @else
                                   {{"0.00"}}
                                 @endif
                                  </code>
                            </code>
                           </div>
                         </div>
                      
                   <div class="drdr_in">
                      <div class="raterow">
                          <input type="hidden" value="0" id="obbagSelected">
                          <input type="hidden" value="0" id="obbagCharges">
                          <input type="hidden" value="0" id="ibbagSelected">
                          <input type="hidden" value="0" id="ibbagCharges">
                        <label>Excess Baggage  (<code style="float:none;" id="bagSelected">0Kg</code> )</label>
                        <code> Rs. <code id="bagCharges">0.00</code>
                            
                        </code>                         
                     </div>
                  </div>
                  
                    <div class="drdr_in">
                      <div class="raterow">
                           <input type="hidden" value="0" id="obmealPlatter">
                          <input type="hidden" value="0" id="obmealCharge">
                          <input type="hidden" value="0" id="ibmealPlatter">
                          <input type="hidden" value="0" id="ibmealCharge">
                        <label>Meal   (<code style="float:none;" id="mealPlatter">0</code>Platter )</label>
                      <code>Rs. <code id="mealCharge">0.00</code>
                          
                      </code>
                     </div>
                    </div>
                      
                  
                    <div class="drdr_in">
                      <div class="raterow">
                        <label>Special Service </label>
                      <code>Rs. <code id="specialServiceCharge">0.00</code>
                          
                      </code>
                     </div>
                    </div>

                    
                  
                    <div class="drdr_in">
                      <div class="raterow">
                        <label>Total GST </label>
                      <code>Rs. <code id="total_gst_cost">0.00</code>
                          
                      </code>
                     </div>
                    </div>

                

                   <div class="grandtotal">
                                             
                    <label>Total Pub. Fare</label>                     
                       
                    <code><b>Rs.</b>
                    
                    <b id="firsttot" class="bold">
                      <code id="total_fare_cost"></code>
                    </b>
                          
                    </code>
                  </div>
				  
				  
				  <div class="drdr_in" id="promo_div"style="display:none">
                      <div class="raterow">
                        <label>Promo Discount</label>
                        <code id="promo_discount_amount">Rs. 0.00
                              
                        </code>
                     </div>
                  </div>
                  
                  <div class="grandtotal" id="payable_div" style="display:none">
                    
                    <label>Total Payable Amount</label>
                    
                    <code><b class="bold" id="payable">Rs.42,241.20
                         
                         </b></code>
                  </div>
                    
                         
                </div>
                     
                </div>

                
               
                  <div class="fleft width_100 align_center pt5 pb  desktop_not">
                     <a id="filterCloseBtn" class="btn_continue_s desktop_not" href="#">Close</a>
                  </div>       
            </div>
</div>
</form>
 
@endif
</div>
@include('frontend.includes.footer')
<script>

    $(document).ready(function(){
       $("#chkBoxGST").on("click",function(){
           if($("#chkBoxGST").is(':checked'))
         $("#GSTDataBlock").show();  
         else
         $("#GSTDataBlock").hide();  
       });
       $("#chkBoxRoamer").on("click",function(){
          if($("#chkBoxRoamer").is(":checked"))
          $("#RoamerDataBlock").show();
          else
          $("#RoamerDataBlock").hide();
       });
    });
</script>
<script>
function selectedBaggage(bag_psg){
      var total_baggage = 0;
      var total_baggage_price = 0;
    for(var b=1;b<=bag_psg;b++){
        var baggage_details =$("#baggage_"+b+" option:selected").text().replace(/[^a-zA-Z 0-9]+/g, ""); 
        var baggage = baggage_details.split(" ");
            //alert(baggage);
        var stringArray = new Array();
        var w="";
        var price=0;
    
        for(var i =0; i < baggage.length; i++){
            
                //alert(isNaN(baggage[i]));
                var x=baggage[i].charCodeAt(0);
                if(!isNaN(x))
                {
                    stringArray.push(baggage[i]);
                }
            
            
        
        }
        if(stringArray[0]!="No"){
        baggage_weight= stringArray[0].split("Kg");
         total_baggage = parseInt(total_baggage)+parseInt(baggage_weight[0]);
        }
        else{
           baggage_weight[0]=0;
         total_baggage = parseInt(total_baggage)+parseInt(baggage_weight[0]);  
        }
        
        if(!isNaN(stringArray[1])){
            baggage_price = stringArray[1];
            total_baggage_price = parseFloat(total_baggage_price)+parseFloat(baggage_price);
            
         }
       
      // alert(total_baggage_price);
    
    }
    var bag_cost = total_baggage_price.toFixed(2);
    var ret_total_baggage = $("#ibbagSelected").val();
    var ret_bag_charges = $("#ibbagCharges").val();
    var ib_ob_total_baggage = parseInt(ret_total_baggage)+parseInt(total_baggage);
    var ib_ob_bag_charges = parseFloat(ret_bag_charges)+parseFloat(bag_cost);
    $("#obbagSelected").val(total_baggage);
    $("#obbagCharges").val(bag_cost);
    
    $("#bagSelected").html(ib_ob_total_baggage+"Kg");
    $("#bagCharges").html(ib_ob_bag_charges);
    calculateTotalFare();
}

function selectedReturnBaggage(bag_psg){
      var total_baggage_ret = 0;
      var total_baggage_ret_price = 0;
    for(var b=1;b<=bag_psg;b++){
        var baggage_ret_details =$("#baggage_ret_"+b+" option:selected").text().replace(/[^a-zA-Z 0-9]+/g, ""); 
        var baggage_ret = baggage_ret_details.split(" ");
            //alert(baggage);
        var stringArray = new Array();
        var w="";
        var price=0;
    
        for(var i =0; i < baggage_ret.length; i++){
            
                //alert(isNaN(baggage[i]));
                var x=baggage_ret[i].charCodeAt(0);
                if(!isNaN(x))
                {
                    stringArray.push(baggage_ret[i]);
                }
            
            
        
        }
        if(stringArray[0]!="No"){
        baggage_ret_weight= stringArray[0].split("Kg");
         total_baggage_ret = parseInt(total_baggage_ret)+parseInt(baggage_ret_weight[0]);
        }
        else{
           baggage_ret_weight[0]=0;
         total_baggage_ret = parseInt(total_baggage_ret)+parseInt(baggage_ret_weight[0]);  
        }
        
        if(!isNaN(stringArray[1])){
            baggage_ret_price = stringArray[1];
            total_baggage_ret_price = parseFloat(total_baggage_ret_price)+parseFloat(baggage_ret_price);
            
         }
       
      // alert(total_baggage_price);
    
    }
    var bag_cost = total_baggage_ret_price.toFixed(2);
    var ob_total_baggage = $("#obbagSelected").val();
    var ob_bag_charges = $("#obbagCharges").val();
    var ib_ob_total_baggage = parseInt(ob_total_baggage)+parseInt(total_baggage_ret);
    var ib_ob_bag_charges = parseFloat(ob_bag_charges)+parseFloat(bag_cost);
    $("#ibbagSelected").val(total_baggage_ret);
    $("#ibbagCharges").val(bag_cost);
    
    $("#bagSelected").html(ib_ob_total_baggage+"Kg");
    $("#bagCharges").html(ib_ob_bag_charges);
    calculateTotalFare();
}
    
</script>
<script>
function selectedMeal(meal_psg){
      var total_meal = 0;
      var total_meal_price = 0;
    for(var b=1;b<=meal_psg;b++){
        var meal_details =$("#meal_"+b+" option:selected").text().replace(/[^a-zA-Z 0-9]+/g, ""); 
        var meal = meal_details.split("Rs");
            //alert(baggage);
        var stringArray = new Array();
        var w="";
        var price=0;
    
        for(var i =0; i < meal.length; i++){
            
                //alert(isNaN(baggage[i]));
                var x=meal[i].charCodeAt(0);
                if(!isNaN(x))
                {
                    stringArray.push(meal[i]);
                }
            
             
        
        }
       
        if(stringArray[0].trim()!="No Meal"){
           total_meal++;
        }
       
     
        if(!isNaN(stringArray[1])){
            meal_price = stringArray[1];
            total_meal_price = parseFloat(total_meal_price)+parseFloat(meal_price);
          }
     }
    var meal_cost = total_meal_price.toFixed(2);
   
    var ret_meal_platter = $("#ibmealPlatter").val();
    var ret_meal_charge = $("#ibmealCharge").val();
    var ib_ob_meal_platter = parseInt(ret_meal_platter)+parseInt(total_meal);
    var ib_ob_meal_charge = parseFloat(ret_meal_charge)+parseFloat(meal_cost);
    $("#obmealPlatter").val(total_meal);
    $("#obmealCharge").val(meal_cost);
    
    $("#mealPlatter").html(ib_ob_meal_platter);
    $("#mealCharge").html(ib_ob_meal_charge);
    calculateTotalFare();
}

function selectedReturnMeal(meal_psg){
      var total_meal = 0;
      var total_meal_price = 0;
    for(var b=1;b<=meal_psg;b++){
        var meal_details =$("#meal_ret_"+b+" option:selected").text().replace(/[^a-zA-Z 0-9]+/g, ""); 
        var meal = meal_details.split("Rs");
            //alert(baggage);
        var stringArray = new Array();
        var w="";
        var price=0;
    
        for(var i =0; i < meal.length; i++){
            
                //alert(isNaN(baggage[i]));
                var x=meal[i].charCodeAt(0);
                if(!isNaN(x))
                {
                    stringArray.push(meal[i]);
                }
            
             
        
        }
       
        if(stringArray[0].trim()!="No Meal"){
           total_meal++;
        }
       
     
        if(!isNaN(stringArray[1])){
            meal_price = stringArray[1];
            total_meal_price = parseFloat(total_meal_price)+parseFloat(meal_price);
          }
     }
    var meal_cost = total_meal_price.toFixed(2);
   
    var meal_platter = $("#obmealPlatter").val();
    var ob_meal_charge = $("#obmealCharge").val();
    var ib_ob_meal_platter = parseInt(meal_platter)+parseInt(total_meal);
    var ib_ob_meal_charge = parseFloat(ob_meal_charge)+parseFloat(meal_cost);
    $("#ibmealPlatter").val(total_meal);
    $("#ibmealCharge").val(meal_cost);
    
    $("#mealPlatter").html(ib_ob_meal_platter);
    $("#mealCharge").html(ib_ob_meal_charge);
    calculateTotalFare();
}
    
</script>
<script>
    function calculateTotalFare(){
        var adult = $("#total_adult_cost").html(); 
        var child=$("#total_child_cost").html();
        var infant=$("#total_infant_cost").html();
        var baggage=$("#bagCharges").html();
        var meal=$("#mealCharge").html();
        var special_service=$("#specialServiceCharge").html();
        var gst=$("#total_gst_cost").html();
         var total_fare = parseFloat(adult)+parseFloat(child)+parseFloat(infant)+parseFloat(baggage)+parseFloat(meal)+parseFloat(special_service)+parseFloat(gst);
         var tf = total_fare.toFixed(2);
         //alert(total_fare);
        // alert("adult:"+adult+"child:"+child+"infant:"+infant+"baggage:"+baggage+"meal:"+meal+"special_service:"+special_service+"gst:"+gst);
       $("#total_fare_cost").html(tf); 
    }
    calculateTotalFare();
    
</script>
<!------------------------------------------------------------Passenger form validation----------------------------------------------------->
<script>
$(document).ready(function(){
   $("input[name='FirstName[]']").on("blur",function(){
       var fname = $(this).val();
       if(fname==null||fname==""){
        $(this).focus();   
        $(this).next().show();
       }
       else{
        $(this).next().hide(); 
       }
   }) ;
   $("input[name='LastName[]']").on("blur",function(){
       var lname = $(this).val();
       if(lname==null||lname==""){
        $(this).focus();   
        $(this).next().show();
       }
       else{
        $(this).next().hide(); 
       }
   }) ;
//   $("input[name='Gender[]']").on("blur",function(){
//       var gender = $(this).val();
//       if(gender==null||gender==""){
//         $(this).focus();   
//         $(this).next().show();
//       }
//       else{
//         $(this).next().hide(); 
//       }
//   }) ;
$("form").on("submit",function(e){
    if(!domestic())
    {
        swal({
  type: 'error',
  title: 'Oops...',
  text: 'Date of Birth is mandatory!',
  footer: '<a href>Why do I have this issue?</a>'
})
        e.preventDefault();
    }
    //return false;
});
function domestic(){
    let flag=1;
       
       var airline = $.trim($("#airline_name").text());
      
        var myControls = document.getElementsByName('DobDay[]');
       
        for (var i = 0; i < myControls.length; i++) {
            var aDay = myControls[i].value;
            var aMonth = document.getElementsByName('DobMonth[]')[i].value;
            var aYear =  document.getElementsByName('DobYear[]')[i].value;
            var pax_type = myControls[i].getAttribute("data-pax-type");
           
               // alert(pax_type);
            if(pax_type==1 && (airline.toLowerCase().indexOf('air india')>=0))
            {   
                if((aDay=="" || aDay==null)||(aMonth=="" || aMonth==null)||(aYear=="" || aYear==null))
                {
                    var p = document.createElement("span");
                    var error = document.createTextNode("Date of birth is mandatory");
                    p.appendChild(error);
                    
                     //document.getElementsByName('DobYear[]')[i].parentNode.removeChild(p);
                    document.getElementsByName('DobYear[]')[i].parentNode.insertBefore(p, document.getElementsByName('DobYear[]')[i].nextSibling);
                    flag=0;
                   // myControls[i].appendChild(p);
                    //return false;
                }
                else
                {
                    flag=1;
                }
                
            }
        }
        
        if(flag!=0)
        {
            
            return true;
        }
      
      else
      {
        
      return false;
      }
    }
});


</script>






