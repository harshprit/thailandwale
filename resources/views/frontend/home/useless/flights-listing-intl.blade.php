@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')




<div class="container">
    <div class="row">
      <div class="col-md-12 flights-form">
       
          <div class="col-md-3 col-xs-12 right-border">
            <span><a href="#" data-toggle="modal" data-target="#modal_show">ROUND-TRIP </a></span>
            <P> <a href="#" data-toggle="modal" data-target="#modal_show">New Delhi to Mumbai </a></P>
        </div>
        <div class="col-md-2 col-xs-6 right-border">
          <span> <a href="#" data-toggle="modal" data-target="#modal_show"> DEPARTURE </a> </span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-2  col-xs-6 right-border">
          <span> <a href="#" data-toggle="modal" data-target="#modal_show"> Return </a> </span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Adult</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>01</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Child</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>--</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Infant</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>--</span>
          </a> </p>
        </div>
        <div class="col-md-2 col-xs-12">
          <button type="button" class="btn btn-success">Search</button>
        </div>
   
    
        <div class="modal fade" tabindex="-1" role="dialog" id="modal_show">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header modal-flights">
             <div class="col-md-12 flights-form">
       
          <div class="col-md-3 col-xs-12 right-border">
            <span><a href="#" data-toggle="modal" data-target="#modal_show">ROUND-TRIP </a></span>
            <P> <a href="#">New Delhi to Mumbai </a></P>
        </div>
        <div class="col-md-2 col-xs-6 right-border">
          <span> <a href="#"> DEPARTURE </a> </span>
          <p><a href="#"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-2  col-xs-6 right-border">
          <span> <a href="#"> Return </a> </span>
          <p><a href="#"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-1 col-xs-4">
         <span> <a href="#">Adult</a></span>
          <p><a href="#"> 
            <span>01</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#">Child</a></span>
          <p><a href="#"> 
            <span>--</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#">Infant</a></span>
          <p><a href="#"> 
            <span>--</span>
          </a> </p>
        </div>
        <div class="col-md-2 col-xs-12">
          <button type="button" class="btn btn-success">Search</button>
        </div>
   
    </div>
          </div>
          <div class="modal-body">
              <form method="post"  action="{{route('frontend.search_flight')}}" novalidate>
<section class=" form-flights">
      <div class="container">
    <div class="row">
          <div class="col-md-12">
        <div class="row">
              
         <div class="tabbable-panel">
     <div class="tabbable-line">
     <div class="col-md-12 col-xs-12">
   
		 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="travel_class" id="travel_class">
            <div id="tab" class="btn-group btn-group-justified" data-toggle="buttons">
        <a href="#one_way" class="btn btn-default active" data-toggle="tab">
          <input type="radio" name="journey_type" value="1" checked="checked"/>ONE WAY
        </a>
        <a href="#round_trip" class="btn btn-default" data-toggle="tab">
          <input type="radio" name="journey_type" value="2" />ROUND TRIP
        </a>
        <a href="#multi_trip" class="btn btn-default" data-toggle="tab">
          <input type="radio" name="journey_type" value="3"/>MULTI-TRIP
        </a>
    
      </div>
     </div>

   

      <div class="tab-content">
        <div class="tab-pane active" id="">	
        
                          <div class="row">
                        <div class="col-xs-12 col-md-6">
                              <h3>From</h3>
                        <select class="select2" id='input2' name="from[]" required="required">
                              <option value="">Source</option>  
                                <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?> 
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php } }?>

                        </select>
                            </div>
                        <div class="col-xs-12 col-md-6">
                              <h3>To</h3>
                        <select class="select2" id='input3' name="to[]" onchange="setOrigin(this);"  required="required">
                            <option value="">Destination</option>
                              <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?>
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php }
                                 }?>
                        </select>
                        
                            </div>
                            


                        <div class="col-xs-12 col-md-6">
                            <div class="date">
                                <div class="depart">
                                  <h3>Depart</h3>
                                  <input id="datepicker" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy"   required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>
                        <div class="col-xs-12 col-md-6" id="multi-trip-hide">
                              <div class="return">
                            <h3>Return</h3>
                          <input  id="datepicker1" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" onfocus="this.value = '';"  required>
                          </div>
                            </div>

					  <div class="col-xs-12 col-md-6">
                        <div class="popover-markup"> 
                            <div class="trigger form-group form-group-lg form-group-icon-left"><i class="fa fa-users input-icon input-icon-highlight"></i>
                                  <label>Passenger</label>
                                  <input type="hidden" name="passenger_count" id="passenger_count" value="1">
                                  <input type="text" name="passengers_one" id="passengers_one" value="1|Economy" class="form-control">
                            </div>
                            <div id="contactForm" style="display:none">
                                  <div class="triangle"></div>
                            <div class="content">
                                <!-- adult row -->
							<div class="row">
                                  <div class="form-group">
                                      <div class="col-md-3">
                                <label class="control-label"><strong>Adults</strong><br>
                                      <i> (+12 yrs)</i></label></div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn btn-danger" data-dir="dwn" id="adult_dn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="adult_one" id="adult_one" class="form-control text-center" value="1">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="adult_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="2"> Economy </button></div>
                              </div>
							  </div>
							  <!-- adult row end -->
							  <!-- child row -->
							  	<div class="row">
                                  <div class="form-group">
                                      <div class="col-md-3">
                                <label class="control-label"><strong>Children</strong><br>
                                      <i> (+12 yrs)</i></label>
                                      </div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn btn-danger" data-dir="dwn" id="child_dn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="child_one" id="child_one" class="form-control text-center" value="0">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="child_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="3">Premium Economy </button></div>
                              </div>
							  </div>
							 <!-- child row end -->
							 <!-- infant row -->
							  <div class="row">
                                <div class="form-group">
                                    <div class="col-md-3">
                                <label class="control-label"><strong>Infants</strong><br>
                                      <i> (0-2 yrs)</i></label>
                                      </div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner1"> <span class="input-group-btn"> <a class="btn btn-danger" id="inf_dn" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="infants_one" id="infants_one" class="form-control text-center" value="0">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="inf_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="4"> Business </button></div>
                              </div>
							  </div>
							  <!-- infant row end -->
							  <div class="row">
							      <div class="form-group">
                                  <button class="btn btn-default btn-block demise" type="button" id="done">Done</button>
                                  </div>
                              </div>
								  </div>
								  </div>
                          </div>
                            </div>                    

                             <!--- Multi City Starts from Here --->
          <div class="row multi-city-extra" id="multi_city" style="display:none">
                        <div class="row">
                        <div class="col-xs-12 col-md-4">
                              <h3>From</h3>
                        <select class="select2 input-select"  name="from[]" required="required">
                              <option value="">Source</option>  
                                <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?> 
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php } }?>

                        </select>
                            </div>
                        <div class="col-xs-12 col-md-4">
                              <h3>To</h3>
                        <select class="select2 input-select"  name="to[]" required="required" onchange="setOrigin(this.value);">
                            <option value="">Destination</option>
                              <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?>
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php }
                                 }?>
                        </select>
                            </div>
                            


                        <div class="col-xs-12 col-md-4">
                            <div class="date">
                                <div class="depart">
                                  <h3>Depart</h3>
                                  <input  name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" class="datepicker"  required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>		
						</div>
                        
                        <button id="addCityMulti" class="btn btn-warning form-search-btn" type="button">Add City</button>
                        
                      </div>
                      <!---- Multi City Ends Here--->							

							
						
					
                            <input type="submit" class="btn btn-warning form-search-btn" id="test1212" value="Search">
                   
                      </div>
					  </div>
        
      </div>
    </div>
  </div>
</div>
</div>
</div>

          </div>
            
        </div>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
</div>

</div>
</form>
</div>
<!--======= flights listing form ================-->
<!--================= container ================-->
<div class="container">
  <div class="row side-bar">
      <div class="col-md-3 margin-top-headline side-flightfilter filter-block">
    <div class="side-stops">
        <h3 class="fsidebar-head">Stops</h3>
<ul class="flight-stop-list">
    <li class="custom-check">
       <input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check">
       <input type="checkbox" id="check01" name="checkbox">
       <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check">
       <input type="checkbox" id="check01" name="checkbox">
       <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
       <span class="fside-counts">(10)</span>
    </li>
    
</ul>
    </div><div class="side-flytime">
        <h3 class="fsidebar-head">Departure Time</h3>
<ul class="flight-flytime-list">
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
</ul>
    </div><div class="side-flytime">
        <h3 class="fsidebar-head">Return Time</h3>
<ul class="flight-flytime-list">
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
</ul>
    </div>
<div class="side-airlines">
        <h3 class="fsidebar-head">Airlines</h3>
<ul class="flight-airlines-list">
            
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
        </ul>
    </div>
<div class="side-airfare">
        <h3 class="fsidebar-head">Fare Range</h3>
<ul class="flight-fare-list">
            <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
</li>
            
            <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
</li>
</ul>

    </div>
  </div>
 <!--   <div class="col-md-3 margin-top-headline">-->
       
 <!--     <div class="row">-->
       
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h5>Filter Results</h5>-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h5 style="text-align: right;"><a href="#">Reset All</a></h5>-->
 <!--       </div>-->
 <!--     </div>-->
 <!--   <div class="border-bottom"></div>-->
 <!--       <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h4>No. of Stops</h4>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <li class="active" style="border-right: 1px solid #ddd;">-->
 <!--             <a href="#tab_default_1" data-toggle="tab" style="padding-left: 31px;">-->
 <!--             <strong> 0 stop</strong> </a>-->
 <!--             <h6 style="padding-left: 22px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--           <li style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--             <strong> 1 stop</strong></a>-->
 <!--              <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--           <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--           <strong> 1+ stop</strong> </a>-->
 <!--            <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!-- <div class="border-bottom"></div>-->
 <!--  <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h5>Departure time from New Delhi</h5>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">-->
 <!--           <li class="active">-->
 <!--             <a href="#tab_default_1" data-toggle="tab">-->
 <!--            <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> Before 6am No Flights</h6>-->
 <!--           </li>-->
 <!--        </div>-->
 <!--        <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--           <li>-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--               <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> 6 AM-12 PM No Flights</h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;    height: 92px;">-->
 <!--           <li >-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6>12PM - 6 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3">-->
 <!--            <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--           <h6> After 6  PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!--  <div class="border-bottom"></div>-->
 <!--  <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h5>Departure time from Mumbai</h5>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">-->
 <!--           <li class="active">-->
 <!--             <a href="#tab_default_1" data-toggle="tab">-->
 <!--            <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> Before 6am</h6>-->
 <!--           </li>-->
 <!--        </div>-->
 <!--        <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--           <li>-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--               <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> 6 AM-12 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;">-->
 <!--           <li >-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6>12PM - 6 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3">-->
 <!--            <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--           <h6> After 6  PM </h6>-->
 <!--           </li>-->
 <!--           </div>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!-- <div class="border-bottom"></div>-->
 <!-- <div class="row">-->
 <!--   <div class="col-md-10 col-xs-12">-->
 <!--     <h5> Airlines</h5>-->
 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--       <i id="4" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--          <i id="3" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
 <!--    <i id="2" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
 <!--  <i id="item" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--<i id="1" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!--   </div>-->
 <!-- </div>-->

 <!--   </div>-->
    <div class="col-md-9 content-side">
      <div class="row multi-flight-list">
	  @if(isset($flights->Response->Results[0]))
	  
		  @foreach($flights->Response->Results[0] as $fd)
      <div class="result_p row mb3" id="ResSet_0">
                                        <div class="retun_inter_sep">
                                            <div class="fleft border_bot pad_em width_98">
                                            
                                                <span class="spl_icon_flight">
                                                    <kbd class="fleft width_98">
													<?php   $dirname = "assets/home/images/AirlineLogo";
														$filename = glob("$dirname/*{$fd->Segments[0][0]->Airline->AirlineCode}*", GLOB_BRACE); ?>
														@if(isset($filename) && count($filename)>0)
														<img src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" class="fleft mr5">
                                                        @endif
					                                                            								
                                                        
                                                        <code id="Code_nm_mob" class="font10 fleft lh_10 width_64 m_width_100"><em class="mobile_not">  {{$fd->Segments[0][0]->Airline->AirlineName}} </em> 
                                                        <em class="fleft width_100 font10">
                                                            <kbd class="fleft mr5 lh10" id="AirlineDetailsOutBound_0">{{$fd->Segments[0][0]->Airline->AirlineCode}} - {{$fd->Segments[0][0]->Airline->FlightNumber}} </kbd>
                                                            <em class="mobile_class" id="class_0" title="Flight Booking Class"><dfn class="class_re">Class -</dfn> {{$fd->Segments[0][0]->Airline->FareClass}}</em>
                                                        </em>
                                                            
                                                        </code>
                                                    </kbd>
                                                </span>
                                                
                                                 <input type="hidden" id="segLenOut_0" value="2">
                                                 <input type="hidden" id="segLenIN_0" value="2">
                                                <span class="spl_duration_flight">
                                                    <kbd class="fleft width_100per">
                                                        
                                                                <em id="departure_0">{{$fd->Segments[0][0]->Origin->Airport->AirportCode}} <input type="hidden" id="deptAirport_0" value="DEL">   <tt class="hightlight bold" id="deptTimeOut_0">({{date('H:i',strtotime($fd->Segments[0][0]->Origin->DepTime))}})</tt></em>                                  
																<?php $count_seg=count($fd->Segments[0]); ?>
																@if($count_seg>1)
																    @foreach($fd->Segments[0] as $intfl)
                                                                        <code> →</code> 
        															    <dfn id="SegOut_0_0">{{$intfl->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_0" value="CCU"></dfn>
        															      <!--<code> →</code> <em class="txt_aln_rt" id="arrival_0"> {{$intfl->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_1" value="BKK"> <tt class="hightlight bold">({{date('H:i',strtotime($intfl->Destination->ArrTime))}})</tt></em>-->
                                                                        <input type="hidden" id="destAirport_0" value="BKK">
                                                                    @endforeach
                                                                    <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[0][$count_seg-1]->Destination->ArrTime))}})</tt>
															    @else
    																<code> →</code> <em class="txt_aln_rt" id="arrival_0"> {{$fd->Segments[0][0]->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_1" value="BKK"> <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[0][0]->Destination->ArrTime))}})</tt></em>
                                                                
                                                                    <input type="hidden" id="destAirport_0" value="BKK">
                                                                @endif
                                                            
														
                                                            
                                                        
                                                    </kbd>
                                                </span> 
                                                <span class="spl_dur_flight mobile_not" id="spldurationboxoutbound_0">
                                                    <kbd class="fleft width_100">
                                                        <span id="duration_0">
                                                              <b> 
                                                              <?php $datetimed = new DateTime($fd->Segments[0][0]->StopPointDepartureTime);
                                                                $datetimea = new DateTime($fd->Segments[0][$count_seg-1]->StopPointArrivalTime);
                                                                $interval = $datetimed->diff($datetimea);
                                                                echo $interval->format('%h')."h ".$interval->format('%i')."m"; ?>
                                                               </b></span><br>
                                                         
                                                           
                                                            <a class="stop_1" href="#!" id="outBoundStops_0">
                                                                <small>
                                                                    
                        
                                                                           @if(count($fd->Segments[0])==1)
                                                                            Non Stop
                                                                           @else
                                                                            {{(count($fd->Segments[0])-1)." Stop" }}
                                                                           @endif
                                                                        
                                                                    
                                                                </small></a>
                                                        
                                                    </kbd>
                                                                                                
                                                           <br>
														   @if(isset($fd->Segments[0][0]->NoOfSeatAvailable))
														   <code class="red">{{$fd->Segments[0][0]->NoOfSeatAvailable}} seat(s) left</code> 
														   @else
														   <code>{{"no seat(s) "}}</code>
															@endif
                                                       
                                                        <!--<div class="outbound_stop_popup" id="outBoundStopInfo_0" style="display: none;">-->
                                                        <!--<a class="fl_close ipad_close_btn" id="outBoundStopInfoClose_0" href="#"></a>-->
                                                            
                                                        <!--        <kbd class="fleft width_100per">-->
                                                        <!--            <em>SG-253 A</em>-->
                                                        <!--            <em> &nbsp;&nbsp; DEL(18:20)</em>-->
                                                        <!--            <code> →</code> -->
                                                                                             
                                                        <!--            <em>CCU(20:30)</em>-->
                                                        <!--        </kbd>                                           -->
                                                            
                                                        <!--        <kbd class="fleft width_100per">-->
                                                        <!--            <em>SG-83 A</em>-->
                                                        <!--            <em> &nbsp;&nbsp; CCU(00:05)</em>-->
                                                        <!--            <code> →</code> -->
                                                                                             
                                                        <!--            <em>BKK(04:10)</em>-->
                                                        <!--        </kbd>                                           -->
                                                            
                                                        <!--</div>-->
                                                    
                                                </span>
                                            </div>
                                            <div class="fleft pad_em width_98">
                                          
                                                <span class="spl_icon_flight">
                                                    <kbd class="fleft width_98">
                                                        <?php   $dirname = "assets/home/images/AirlineLogo";
														$filename = glob("$dirname/*{$fd->Segments[1][0]->Airline->AirlineCode}*", GLOB_BRACE); ?>
														@if(isset($filename) && count($filename)>0)
														<img src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" class="fleft mr5">
														@endif
                                                        <code id="Code_nm_mob" class="font10 fleft lh_10 width_64 m_width_100"><em class="mobile_not"> {{$fd->Segments[1][0]->Airline->AirlineName}}</em> 
                                                        <em class="fleft width_100 font10"><kbd class="fleft mr5 lh10" id="AirlineDetailsInBound_0">{{$fd->Segments[1][0]->Airline->AirlineCode}} - {{$fd->Segments[1][0]->Airline->FlightNumber}}</kbd>
                                                            <em class="mobile_class" id="Em1" title="Flight Booking Class"><dfn class="class_re">Class -</dfn> {{$fd->Segments[1][0]->Airline->FareClass}}</em>
                                                        </em>                                                           
                                                        </code>
                                                    </kbd>
                                                </span>
                                            
                                                <span class="spl_duration_flight">
                                                    <kbd class="fleft width_100per pad_8">
                                                            <em id="departureIn_0">{{$fd->Segments[1][0]->Origin->Airport->AirportCode}}<tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[1][0]->Origin->DepTime))}})</tt></em>
                                                                <?php $count_seg=count($fd->Segments[1]); ?>
																@if($count_seg>1)
																    @foreach($fd->Segments[1] as $intfl)
                                                                        <code> →</code> 
        															    <dfn id="SegIN_0_0">{{$intfl->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrIn_0_0" value="CCU"></dfn>
        															      
                                                                        <input type="hidden" id="destAirport_0" value="BKK">
                                                                    @endforeach
                                                                    <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[1][$count_seg-1]->Destination->ArrTime))}})</tt>
															    @else
    																<code> →</code> <em class="txt_aln_rt" id="arriavalIn_0"> {{$fd->Segments[1][0]->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrIn_0_1" value="DEL"> <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[1][0]->Destination->ArrTime))}})</tt></em>
                                                                
                                                                    <input type="hidden" id="destAirport_0" value="DEL">
                                                                @endif
                                                    </kbd>  
                                               
                                                </span>
                                                <span class="spl_dur_flight mobile_not" id="spldurationboxinbound_0">
                                                    <kbd class="fleft width_100 pad_8 pad_top_0 pad_bottom_0">
                                                        <tt id="inBoundDuration_0">
                                                             <?php $datetimed = new DateTime($fd->Segments[1][0]->StopPointDepartureTime);
                                                                $datetimea = new DateTime($fd->Segments[1][$count_seg-1]->StopPointArrivalTime);
                                                                $interval = $datetimed->diff($datetimea);
                                                                echo $interval->format('%h')."h ".$interval->format('%i')."m"; ?>
                                                            
                                                        </tt><br>
                                                        
                                                           
                                                            <a class="stop_1" href="#!" id="inBoundStops_0">
                                                                <small>
                                                                      @if(count($fd->Segments[1])==1)
                                                                            Non Stop
                                                                           @else
                                                                            {{(count($fd->Segments[1])-1)." Stop" }}
                                                                           @endif
                                                                </small></a>
                                                        
                                                    </kbd>
                                                                                           
                                                           <br><code class="red">
                                                               
                                                               @if(isset($fd->Segments[0][0]->NoOfSeatAvailable))
														   <code class="red">{{$fd->Segments[0][0]->NoOfSeatAvailable}} seat(s) left</code> 
														   @else
														   <code>{{"no seats available"}}</code>
															@endif
                                                               </code>                                    
                                                       
                                                        <!--<div class="inbound_stop_popup" id="inBoundStopInfo_0" style="display: none;">-->
                                                        <!--   <a class="fl_close ipad_close_btn" id="inBoundStopInfoClose_0" href="#"></a>-->
                                                            
                                                        <!--        <kbd class="fleft width_100per">-->
                                                        <!--            <em>SG-84 A</em>-->
                                                        <!--            <em> &nbsp;&nbsp; BKK(05:10)</em>-->
                                                        <!--            <code> →</code> -->
                                                                                                 
                                                        <!--            <em>CCU(06:10)</em>-->
                                                        <!--        </kbd>-->
                                                
                                                            
                                                        <!--        <kbd class="fleft width_100per">-->
                                                        <!--            <em>SG-264 A</em>-->
                                                        <!--            <em> &nbsp;&nbsp; CCU(20:50)</em>-->
                                                        <!--            <code> →</code> -->
                                                                                                 
                                                        <!--            <em>DEL(23:25)</em>-->
                                                        <!--        </kbd>-->
                                                
                                                            
                                                        <!--</div>-->
                                                    
                                                </span>
                                            </div>
                                        </div>                        
                                        <span class="spl_price mobile_not" id="pub_fare_0">
                                            <kbd class="fleft Width_100 m_float_rt" id="pubPricekbd_0" style="visibility:visible;">
                                                <tt>Rs. </tt>
                                                <tt id="PubPrice_0">
                                                    {{" ".number_format(money_format('%!i',$fd->Fare->PublishedFare),2)}}
                                                </tt>
                                                
                                            </kbd> 
                                            <span class="icon_flight2 width-120 mobile_not">
                                                <a class="ruppe" href="#" id="FareRule_0"><span class="drop2"> Fare Rule</span></a>
                                                
                                                    <a class="close cur_default" href="#"><span class="drop2" id="isRefundable_0">Lowest Cancellation</span></a>
                                                   
                                                 
                                            </span>  
                                        </span>

                                        <span class="spl_price mobile_not" id="OfferPriceSpan_0" style="visibility:visible;">
                                            <span>Rs.</span>
                                            <span id="OfferPrice_0">
                                             {{" ".number_format(money_format('%!i',$fd->Fare->OfferedFare),2)}}
                                            </span>
                                            
                                        </span>
                                       
                                       <span class="book_now_emails">
                                        <span class="book_now_btn mt5"><a href="{{route('frontend.flight_passenger_details',['TraceId' =>$flights->Response->TraceId ,'ResultIndex'=>$fd->ResultIndex])}}" id="btnBookNow_0" class="btn_bg">Book Now</a></span>
                                       
                                       </span>
                        
                                        <span class="spl_email spl_pt15">
                                            <input type="checkbox" id="chkEmailItinerary_0" name="emailItinerary" value="0"> Email
                                        </span>
                                         

                                        <div class="fare_rule_pop" id="FareRuleBlock_0" style="display: none;">
                                            <div class="head_pop" id="FareRuleHead">
                                                <span id="FareRuleHeadTitle">Fare Rules</span>
                                                <a href="#" id="btnHeadFareRuleBlockClose_0" class="fl_close"></a>
                                            </div>
                                            <div id="loadingMsg_0">
                                            </div>
                                            <div class="fare_footer" id="FareRuleFoot">
                                                <span class="close_btn cursor">
                                                    <input type="button" value="Close" id="btnFareRuleBlockClose_0">
                                                </span>
                                            </div>
                                        </div>

                                        
                                                    <p class="airline_rem"><span>Remarks for star Coupon.</span></p>
                                                
                                        
                                        <div class="stop_1 fleft relative width_100">     <p class="airline_rem">
                                            <span class="cursor" id="FreeBaggage_0"> Check-In Baggage :
                                          15 Kg ,15 Kg ,15 Kg ,15 Kg 
                                          
                                            </span>
                                             </p>
                                            <!--<div id="Segmetbaggage_FreeBaggage_0" class="bag_operatedBy_popup mobile_not" style="display: none;">-->
                                           
                                       
                                            <!--        <kbd class="fleft width_100">-->
                                            <!--            <em id="departure_0">-->
                                            <!--            DEL  <input type="hidden" id="originApt_0" value="DEL"> <tt class="hightlight bold" id="DeptTimeAirline_0">(18:20)</tt></em> -->
                                            <!--            <kbd>→</kbd>-->
                                            <!--            <em> CCU<tt class="hightlight bold" id="ArrTimeAirline_0">(20:30)</tt>-->
                                            <!--               15 Kg-->
                                            <!--            </em>-->
                                                              
                                               
                                            <!--        </kbd> -->
                                       
                                            <!--        <kbd class="fleft width_100">-->
                                            <!--            <em id="fltdept_0_1">-->
                                            <!--            CCU  <tt class="hightlight bold" id="DeptTimeAirline_0">(00:05)</tt></em> -->
                                            <!--            <kbd>→</kbd>-->
                                            <!--            <em> BKK<tt class="hightlight bold" id="ArrTimeAirline_0">(04:10)</tt>-->
                                            <!--               15 Kg-->
                                            <!--            </em>-->
                                                              
                                               
                                            <!--        </kbd> -->
                                       
                                            <!--        <kbd class="fleft width_100">-->
                                            <!--            <em id="fltdept_0_2">-->
                                            <!--            BKK  <tt class="hightlight bold" id="DeptTimeAirline_0">(05:10)</tt></em> -->
                                            <!--            <kbd>→</kbd>-->
                                            <!--            <em> CCU<tt class="hightlight bold" id="ArrTimeAirline_0">(06:10)</tt>-->
                                            <!--               15 Kg-->
                                            <!--            </em>-->
                                                              
                                               
                                            <!--        </kbd> -->
                                       
                                            <!--        <kbd class="fleft width_100">-->
                                            <!--            <em id="fltdept_0_3">-->
                                            <!--            CCU  <tt class="hightlight bold" id="DeptTimeAirline_0">(20:50)</tt></em> -->
                                            <!--            <kbd>→</kbd>-->
                                            <!--            <em id="arrival_0"> DEL<tt class="hightlight bold" id="ArrTimeAirline_0">(23:25)</tt>-->
                                            <!--               15 Kg-->
                                            <!--            </em>-->
                                                              
                                               
                                            <!--        </kbd> -->
                                             
                                            <!--      </div>-->
                                        </div>
                                         
                                            

                                    </div>
									@endforeach
									@endif
        </div>
      </div>
        </div>

      </div>
    </div>
  </div>
  </div>
  
  
  
  
  
  
  
		@include('frontend.includes.footer')