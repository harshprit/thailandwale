@include('frontend.includes.header')
@include('frontend.includes.nav1')
<!--========== packages ================--> 
<!--===== INNERPAGE-WRAPPER ============-->
<section class="single-package-page">
  <div id="SinglePackage" class="single-package-details">
    
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-9 package-overview">
<!--Carousel Wrapper-->
  <div id="myCarousel" class="carousel slide package-slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class=""></li>
      <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="2" class=""></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item">
        <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="Los Angeles" style="">
      </div>

      <div class="item active">
        <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="Chicago" style="">
      </div>
    
      <div class="item">
        <img src="https://www.w3schools.com/bootstrap/chicago.jpg" alt="New york" style="">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
<div class="board">
         <div class="board-inner">
            <ul id="myTabs" class="nav nav-tabs">
               <div class="liner"></div>
               <li class=""><a href="#home" data-toggle="tab" aria-expanded="true"><span class="round-tabs one"><img src="{{asset('assets/home/images/destinations-icon.png')}}" style="
    width: 42px;
">
<span>Itinerary</span></span></a></li>
               <li class=""><a href="#profile" data-toggle="tab" aria-expanded="false"><span class="round-tabs two"><i class="fa fa-check-circle-o"></i>
<span>Inclusions</span></span></a></li>
               <li class=""><a href="#messages" data-toggle="tab" aria-expanded="false"><span class="round-tabs three"><i class="fa fa-ban"></i>
<span>Exclusions</span></span></a></li>
               
               <li class=""><a href="#doner" data-toggle="tab" aria-expanded="false"><span class="round-tabs five"><i class="glyphicon glyphicon-question-sign"></i><span>Enquire</span></span></a></li>
            </ul>
         </div>
         <div class="tab-content">
            <div id="home" class="tab-pane fade active in">
               <div class="row user-menu-container square">
                  <div class="col-md-7 user-details">
                     <div class="row coralbg white">
                        <div class="col-md-6 no-pad">
                           <div class="user-pad">
                              <h3>Welcome back, Jessica</h3>
                              <h4 class="white"><i class="fa fa-check-circle-o"></i> San Antonio, TX</h4>
                              <h4 class="white"><i class="fa fa-twitter"></i> CoolesOCool</h4>
                              <button type="button" href="#" class="btn btn-labeled btn-info"><span class="btn-label"><i class="fa fa-pencil"></i></span>Update</button>
                           </div>
                        </div>
                        <div class="col-md-6 no-pad">
                           <div class="user-image"><img src="https://farm7.staticflickr.com/6163/6195546981_200e87ddaf_b.jpg" class="img-responsive thumbnail"></div>
                        </div>
                     </div>
                     <div class="row overview">
                        <div class="col-md-4 user-pad text-center">
                           <h3><i class="fa fa-money" style="padding-right: 10px;"></i>Walltet</h3>
                           <h4><i class="fa fa-inr"></i>2,460.00</h4>
                        </div>
                        <div class="col-md-4 user-pad text-center">
                           <h3>FOLLOWING</h3>
                           <h4>456</h4>
                        </div>
                        <div class="col-md-4 user-pad text-center">
                           <h3>APPRECIATIONS</h3>
                           <h4>4,901</h4>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-1 user-menu-btns">
                     <div id="responsive" class="btn-group-vertical square"><a href="#" class="btn btn-block btn-default active">
                        Passport
                        </a> <a href="#" class="btn btn-default">
                        VISA
                        </a>
                     </div>
                  </div>
                  <div class="col-md-4 user-menu user-pad">
                     <div class="user-menu-content active">
                        <h3>
                           Recent Interactions
                        </h3>
                        <ul class="user-menu-list">
                           <li>
                              <h4><i class="fa fa-user coral"></i> Roselynn Smith followed you.</h4>
                           </li>
                           <li>
                              <h4><i class="fa fa-heart-o coral"></i> Jonathan Hawkins followed you.</h4>
                           </li>
                           <li>
                              <h4><i class="fa fa-paper-plane-o coral"></i> Gracie Jenkins followed you.</h4>
                           </li>
                           <li><button type="button" href="#" class="btn btn-labeled btn-success"><span class="btn-label"><i class="fa fa-bell-o"></i></span>View all activity</button></li>
                        </ul>
                     </div>
                     <div class="user-menu-content">
                        <h3>
                           Your Inbox
                        </h3>
                        <ul class="user-menu-list">
                           <li>
                              <h4>From Roselyn Smith <small class="coral"><strong>NEW</strong> <i class="fa fa-clock-o"></i> 7:42 A.M.</small></h4>
                           </li>
                           <li>
                              <h4>From Jonathan Hawkins <small class="coral"><i class="fa fa-clock-o"></i> 10:42 A.M.</small></h4>
                           </li>
                           <li>
                              <h4>From Georgia Jennings <small class="coral"><i class="fa fa-clock-o"></i> 10:42 A.M.</small></h4>
                           </li>
                           <li><button type="button" href="#" class="btn btn-labeled btn-danger"><span class="btn-label"><i class="fa fa-envelope-o"></i></span>View All Messages</button></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div id="profile" class="tab-pane fade">
               
            </div>
            <div id="messages" class="tab-pane fade">
               
            </div>
            
            <div id="doner" class="tab-pane fade">
               
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
<!--/.Carousel Wrapper--> 
        </div>
        <!-- end columns -->
        
        
<div class="package-info details col-md-3">
						<h3 class="package-title">men's shoes fashion</h3>
						
						<p class="package-description">Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p>
						<h4 class="price"><span>$180</span> Per Person</h4>
						
						
						<h5 class="colors">Includes:<span class="color orange not-available" data-toggle="tooltip" title="Not In store"></span>
							<span class="color green"></span>
							<span class="color blue"></span>
						</h5>
						<div class="action">
							<button class="book-package btn btn-default" type="button">Book Package</button>
							<button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>
						</div>
					</div>

        <!-- end columns --> 
        
      </div>
      <!-- end row --> 
    <!-- end container --> 
  </div>
  <!-- end hotel-listing --> 
</section>
<!-- end innerpage-wrapper --> 
<!--=============== packages ==============--> 
@include('frontend.includes.footer')
<script>
    $('.select2').select2();
</script>
<script type="text/javascript">
  var vm = new Vue({
  el: document.body,
  data: {
    a: 0,
    b: 1
  }
})
</script>
