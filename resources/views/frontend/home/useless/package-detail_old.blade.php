<!DOCTYPE HTML>
<html lang="en">
   <head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
      <title>Thailandwale | Customize Package</title>
      
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/font-awesome.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/lineicons.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/weather-icons.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/bootstrap.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/styles.css')}}"/>
   </head>
 <body>
      <div id="top-bar" class="tb-text-white">
            <div class="container">
                <div class="row">          
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <nav class="navbar navbar-default main-navbar navbar-custom navbar-white affix-top" id="mynavbar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" id="menu-button">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>                        
                    </button>
                  
                    <a href="#" class="navbar-brand"><img src="http://thailandwale.com/thailandwale/assets/home/images/logo-2.png"></a>
                </div><!-- end navbar-header -->
                
                <div class="collapse navbar-collapse" id="myNavbar1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown active"><a href="http://thailandwale.com/thailandwale">Flights</a>
                          			
                        </li>
                        
                        <li class="dropdown"><a href="http://thailandwale.com/thailandwale/hotels">Hotels</a>
                         			
                        </li>
                        <li class="dropdown"><a href="http://thailandwale.com/thailandwale/all_package">Holidays</a> 
                           		
                        </li>
                        <li class="dropdown"><a href="#">Hot Deals</a>
                           			
                        </li>
						 <li class="dropdown"><a href="#">Refer &amp; earn</a>
                           		
                        </li>
                     
                       
                    </ul>
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </nav><!-- end navbar -->
                        </div>
                    
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div id="links">
                            <ul class="list-unstyled list-inline">
                                <li><a href="#"><span><i class="fa fa-lock"></i></span>Login</a></li>
                                <li><a href="#"><span><i class="fa fa-plus"></i></span>Sign Up</a></li>
                                <li>

                                    	

                                </li>
                            </ul>
                        </div><!-- end links -->
                    </div><!-- end columns -->				
                </div><!-- end row -->
            </div><!-- end container -->
        </div>
		
		
		
		
	<form id="package_form" method="post" action="{{route('frontend.process_package')}}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="theme-page-section theme-page-section-lg apck-class">
         <div class="container">
            <div class="row row-col-static row-col-mob-gap" id="sticky-parent" data-gutter="60">
               <div class="col-md-8 ">
                  <div class="theme-payment-page-sections">
                     <div class="fullimage">
                        <img src="{{asset('assets/home/customize/images/pack.jpg')}}" class="img-responsive">
                     </div>
                     <div class="row">
					 <?php $i=0;?>
					 @foreach($package_hotels as $hotels)
                        <div class="col-md-6 mb30">
						@if(isset($hotels))
							@foreach($hotels as $hotel)
                            @if($hotel->ResponseStatus==1)
							
                           <div class="headback">
                              <h5 class="formhead"><i class="fa fa-bed"></i> Your Hotel In</h5>
                              <div class="theme-payment-page-sections-item">
                                 <div class="theme-search-results-item theme-payment-page-item-thumb">
                                    <div class="row" data-gutter="20">
                                       <div class="col-md-12">
                                          <h5 class="theme-search-results-item-title theme-search-results-item-title-lg">{{$hotel->HotelResults[0]->HotelName}}</h5>
										  <input type="hidden" name="HotelTraceId[]" value="{{$hotel->TraceId}}">
										  <input type="hidden" name="HotelResultIndex[]" value="{{$hotel->HotelResults[0]->ResultIndex}}">
										  <input type="hidden" name="HotelCode[]" value="{{$hotel->HotelResults[0]->HotelCode}}">
										  
                                          <div class="theme-search-results-item-rating">
                                             <ul class="theme-search-results-item-rating-stars">
                                                
												<?php $star=$hotel->HotelResults[0]->StarRating; for($k=0;$k<$star;$k++){?>
                                                <li class="active">
                                                   <i class="fa fa-star"></i>
                                                </li>
                                                <?php }?>
                                                    
                                                    <?php $star=5-$hotel->HotelResults[0]->StarRating; for($k=0;$k<$star;$k++){?>
                                                <li>
                                                   <i class="fa fa-star"></i>
                                                </li>
                                                <?php }?>
												
                                             </ul>
                                             <p class="theme-search-results-item-rating-title">160 reviews</p>
                                          </div>
                                          <ul class="theme-search-results-item-room-feature-list">
                                          
                                             <li>
                                                <i class="fa fa-map-marker theme-search-results-item-room-feature-list-icon"></i>
                                                <span class="theme-search-results-item-room-feature-list-title">{{$destination[$i+1]}}</span>
                                             </li>
                                          </ul>
                                          <p class="theme-search-results-item-location">
                                          <h5 class="theme-sidebar-section-title mt20">Booking Summary</h5>
                                          <ul class="theme-sidebar-section-summary-list">
                                             <li><i class="fa fa-users"></i> {{$passanger_count['adults']}} Guest, {{$no_of_nights[$i]}} Nights, &nbsp; 
                                                <i class="fa fa-user"></i> {{$passanger_count['child']}} Children 
                                             </li>
                                             <li><i class="fa fa-calendar"></i> Check-in: {{date('D,F d,Y',strtotime($hotel->CheckInDate))}}</li>
                                             <li><i class="fa fa-calendar"></i> Check-out: {{date('D,F d,Y',strtotime($hotel->CheckOutDate))}}</li>
                                          </ul>
                                          </p>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="theme-search-results-item-img-wrap">
                                             <img class="theme-search-results-item-img" src="{{$hotel->HotelResults[0]->HotelPicture}}" alt="Image Alternative text" title="Image Title" style="width: 100%;" />
                                          </div>
                                       </div>
                                    </div>
                                    <button type="button" data-toggle="modal" data-target="#hotel<?php echo $i;?>" class="change-button">Change Hotel</button>
                                 </div>
                              </div>
                           </div>
						   
                            @endif
                            @endforeach
                            @endif
                        </div>
						
                    <div class="row modal fade" id="hotel<?php echo $i;?>" role="dialog">         
                    <button type="button" class="close" data-dismiss="modal">&times;</button>    
                        <div id="sidebar-hotel" class="col-xs-12 col-sm-12 col-md-3 side-bar left-side-bar">
                                        
                            <div class="side-bar-block filter-block">
                                <h3>Sort By Filter</h3>
                                
                                
                                <div class=" accordion panels-group" id="accordion ">
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading active">         
                                            <a href="#panel-1" data-toggle="collapse" >Select Category <span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->
                                        
                                        <div id="panel-1" class="panel-collapse collapse in">
                                            <div class="panel-body text-left ">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox"/>
                                                    <label for="check01"><span><i class="fa fa-check"></i></span>All</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check02" name="checkbox"/>
                                                    <label for="check02"><span><i class="fa fa-check"></i></span>Apartment</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check03" name="checkbox"/>
                                                    <label for="check03"><span><i class="fa fa-check"></i></span>Bed & Breakfast</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check04" name="checkbox"/>
                                                    <label for="check04"><span><i class="fa fa-check"></i></span>Guest House</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check05" name="checkbox"/>
                                                    <label for="check05"><span><i class="fa fa-check"></i></span>Hotels</label></li>        
                                                    <li class="custom-check"><input type="checkbox" id="check06" name="checkbox"/>
                                                    <label for="check06"><span><i class="fa fa-check"></i></span>Residence</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check07" name="checkbox"/>
                                                    <label for="check07"><span><i class="fa fa-check"></i></span>Resorts</label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading active">         
                                            <a href="#panel-2" data-toggle="collapse" >Facility<span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->
                                        
                                        <div id="panel-2" class="panel-collapse collapse in">
                                            <div class="panel-body text-left">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check08" name="checkbox"/>
                                                    <label for="check08"><span><i class="fa fa-check"></i></span>Air Conditioning</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check09" name="checkbox"/>
                                                    <label for="check09"><span><i class="fa fa-check"></i></span>Bathroom</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check10" name="checkbox"/>
                                                    <label for="check10"><span><i class="fa fa-check"></i></span>Cable Tv</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check11" name="checkbox"/>
                                                    <label for="check11"><span><i class="fa fa-check"></i></span>Parking</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check12" name="checkbox"/>
                                                    <label for="check12"><span><i class="fa fa-check"></i></span>Pool</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check13" name="checkbox"/>
                                                    <label for="check13"><span><i class="fa fa-check"></i></span>Wi-fi</label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading active">         
                                            <a href="#panel-3" data-toggle="collapse" >Rating <span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->
                                        
                                        <div id="panel-3" class="panel-collapse collapse in">
                                            <div class="panel-body text-left">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check14" name="checkbox"/>
                                                    <label for="check14"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"> </i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check15" name="checkbox"/>
                                                    <label for="check15"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check16" name="checkbox"/>
                                                    <label for="check16"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check17" name="checkbox"/>
                                                    <label for="check17"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check18" name="checkbox"/>
                                                    <label for="check18"><span><i class="fa fa-check"></i></span> <i class="fa fa-star"></i></label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->
                                    
                                </div><!-- end panel-group -->
                                
                                <div class="price-slider">
                                    <p><input type="text" id="amount" readonly></p>
                                    <div id="slider-range"></div>
                                </div><!-- end price-slider -->
                            </div><!-- end side-bar-block -->
                            
                            <div class="row">
                            
                                
                                <div class="col-xs-12 col-sm-6 col-md-12">    
                                    <div class="side-bar-block support-block">
                                        <h3>Need Help</h3>
                                        <p>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum.</p>
                                        <div class="support-contact">
                                            <span><i class="fa fa-phone"></i></span>
                                            <p>+1 123 1234567</p>
                                        </div><!-- end support-contact -->
                                    </div><!-- end side-bar-block -->
                                </div><!-- end columns -->
                                
                            </div><!-- end row -->
                        </div><!-- end columns --> 
                        
                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side">
                            @if(isset($hotels))
							@foreach($hotels as $hotel)
                            @if($hotel->ResponseStatus==1)
                            @foreach($hotel->HotelResults as $ht)
                            <div class="list-block main-block h-list-block">
                              <div class="list-content">
                                <div class="main-img list-img h-list-img">
                                        <a href="#">
                                            <img src="{{$ht->HotelPicture}}" class="img-responsive" alt="hotel-img" />
                                        </a>
                                        <div class="main-mask">
                                            <ul class="list-unstyled list-inline offer-price-1">
                                                <li class="price">{{$ht->Price->CurrencyCode}}{{$ht->Price->OfferedPrice}}<span class="divider">|</span><span class="pkg">Avg/Night</span></li>
                                                <li class="rating">
                                                <?php $star=$ht->StarRating; for($k=0;$k<$star;$k++){?>
                                                  <span><i class="fa fa-star"></i></span>
                                                <?php }?>
                                                    
                                                    <?php $star=5-$ht->StarRating; for($k=0;$k<$star;$k++){?>
                                                        <span><i class="fa fa-star lightgrey"></i></span>
                                                <?php }?>
                                                    
                                                </li>
                                            </ul>
                                        </div><!-- end main-mask -->
                                    </div><!-- end h-list-img -->
                                    
                                    <div class="list-info h-list-info">
                                        <h3 class="block-title"><a href="#">{{$ht->HotelName}}</a></h3>
                                        <p>{{ str_limit($ht->HotelDescription, $limit = 350, $end = '...') }}</p>
                                        <div class="row">
                                          <div class="col-md-2">
                                            <img src="assets/home/images/bell_service-128.png" style="width: 20px;">
                                          </div>
                                          <div class="col-md-2">
                                            <img src="assets/home/images/gym.png" style="width: 20px;">
                                          </div>
                                          <div class="col-md-2">
                                            <img src="assets/home/images/restaurant-food.png" style="width: 20px;">
                                          </div>
                                          <div class="col-md-2">
                                            <img src="assets/home/images/wifi.png" style="width: 20px;">
                                          </div>
                                          <div class="col-md-2">
                                            <img src="assets/home/images/swimming.png" style="width: 20px;">
                                          </div>
                                        </div>
                                        <a href="#" class="btn btn-orange btn-lg">Select</a>
                                     </div><!-- end h-list-info -->
                              </div><!-- end list-content -->
                            </div><!-- end h-list-block -->
                            @endforeach
                            @endif
                            @endforeach
                            @endif
                            
                            
                            <div class="pages">
                                <ol class="pagination">
                                    <li><a href="#" aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#" aria-label="Next"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>
                                </ol>
                            </div><!-- end pages -->
                        </div><!-- end columns -->

                    </div><!-- end row -->
					
						<?php $i++;?>
						@endforeach
                        <div class="col-md-6 mb30">
                           <div class="headback">
                              <h5 class="formhead"><i class="fa fa-plane"></i> Your Included Flights</h5>
							  
							  @if(isset($flights->Response->Results[0][0]))
										  <input type="hidden" name="FlightTraceId" value="{{$flights->Response->TraceId}}">
										  <input type="hidden" name="FlightResultIndex" value="{{$flights->Response->Results[0][0]->ResultIndex}}">
							  
								  @foreach($flights->Response->Results[0][0]->Segments as $seg)
                              <div class="theme-payment-page-sections-item">
                                 <div class="theme-search-results-item theme-payment-page-item-thumb">
                                    <div class="row" data-gutter="20">
                                       <div class="col-md-9 ">
                                          <p class="theme-search-results-item-flight-payment-airline">You are flying {{$seg[0]->Airline->AirlineName}}</p>
                                          <h5 class="theme-search-results-item-title">{{$seg[0]->Origin->Airport->CityName}}, {{$seg[0]->Origin->Airport->CityCode}} &nbsp;&rarr;&nbsp; {{$seg[(count($seg))-1]->Destination->Airport->CityName}}, {{$seg[(count($seg))-1]->Destination->Airport->CityCode}}</h5>
                                          
                                          <a class="theme-search-results-item-flight-payment-details-link" href="#FlightPaymentDetails" data-toggle="collapse" aria-expanded="false" aria-controls="FlightPaymentDetails">Flight Details &nbsp;
                                          <i class="fa fa-angle-down"></i>
                                          </a>
                                          <div class="collapse _pt-20" id="FlightPaymentDetails">
                                             <div class="theme-search-results-item-flight-detail-items">
                                                <div class="theme-search-results-item-flight-details">
                                                   <div class="row">
                                                      <div class="col-md-3 ">
                                                         <div class="theme-search-results-item-flight-details-info">
                                                            <h5 class="theme-search-results-item-flight-details-info-title">Depart</h5>
                                                            <p class="theme-search-results-item-flight-details-info-date">{{date('D,F y',strtotime($seg[0]->Origin->DepTime))}}</p>
                                                            <p class="theme-search-results-item-flight-details-info-cities">{{$seg[0]->Origin->Airport->CityName}}</p>
                                                            <p class="theme-search-results-item-flight-details-info-fly-time">20h 50m</p>
                                                            <p class="theme-search-results-item-flight-details-info-stops">{{count($seg)-1}} Stops</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-md-9 ">
                                                         <div class="theme-search-results-item-flight-details-schedule">
                                                            <ul class="theme-search-results-item-flight-details-schedule-list">
                                                               <li>
                                                                  <i class="fa fa-plane theme-search-results-item-flight-details-schedule-icon"></i>
                                                                  <div class="theme-search-results-item-flight-details-schedule-dots"></div>
                                                                  <p class="theme-search-results-item-flight-details-schedule-date">Tue, May 17</p>
                                                                  <div class="theme-search-results-item-flight-details-schedule-time">
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-item">08:40
                                                                     <span>am</span>
                                                                     </span>
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-separator">&mdash;</span>
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-item">12:50
                                                                     <span>am</span>
                                                                     </span>
                                                                  </div>
                                                                  <p class="theme-search-results-item-flight-details-schedule-fly-time">4h 10m</p>
                                                                  <div class="theme-search-results-item-flight-details-schedule-destination">
                                                                     <div class="theme-search-results-item-flight-details-schedule-destination-item">
                                                                        <p class="theme-search-results-item-flight-details-schedule-destination-title">
                                                                           <b>SEN</b>Southend
                                                                        </p>
                                                                        <p class="theme-search-results-item-flight-details-schedule-destination-city">London</p>
                                                                     </div>
                                                                     <div class="theme-search-results-item-flight-details-schedule-destination-separator">
                                                                        <span>&rarr;</span>
                                                                     </div>
                                                                     <div class="theme-search-results-item-flight-details-schedule-destination-item">
                                                                        <p class="theme-search-results-item-flight-details-schedule-destination-title">
                                                                           <b>SVO</b>Sheremetyevo
                                                                        </p>
                                                                        <p class="theme-search-results-item-flight-details-schedule-destination-city">Moscow</p>
                                                                     </div>
                                                                  </div>
                                                                  <ul class="theme-search-results-item-flight-details-schedule-features">
                                                                     <li>9419 LOT</li>
                                                                     <li>Wide-body jet</li>
                                                                     <li>Boeing 747-400</li>
                                                                  </ul>
                                                               </li>
                                                               <li>
                                                                  <i class="fa fa-exchange theme-search-results-item-flight-details-schedule-icon"></i>
                                                                  <div class="theme-search-results-item-flight-details-schedule-dots"></div>
                                                                  <p class="theme-search-results-item-flight-details-schedule-date">Tue, May 17</p>
                                                                  <div class="theme-search-results-item-flight-details-schedule-time">
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-item">12:50
                                                                     <span>am</span>
                                                                     </span>
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-separator">&mdash;</span>
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-item">03:30
                                                                     <span>pm</span>
                                                                     </span>
                                                                  </div>
                                                                  <p class="theme-search-results-item-flight-details-schedule-fly-time">2h 40m</p>
                                                                  <p class="theme-search-results-item-flight-details-schedule-transfer">Change planes in Moscow(SVO)</p>
                                                               </li>
                                                              
                                    
                                          
                                                            </ul>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="theme-search-results-item-flight-details">
                                                   <div class="row">
                                                      <div class="col-md-3 ">
                                                         <div class="theme-search-results-item-flight-details-info">
                                                            <h5 class="theme-search-results-item-flight-details-info-title">Arrival</h5>
                                                            <p class="theme-search-results-item-flight-details-info-date">Tue, May 23</p>
                                                            <p class="theme-search-results-item-flight-details-info-cities">New York &rarr; London</p>
                                                            <p class="theme-search-results-item-flight-details-info-fly-time">20h 20m</p>
                                                            <p class="theme-search-results-item-flight-details-info-stops">non-stop</p>
                                                         </div>
                                                      </div>
                                                      <div class="col-md-9 ">
                                                         <div class="theme-search-results-item-flight-details-schedule">
                                                            <ul class="theme-search-results-item-flight-details-schedule-list">
                                                               <li>
                                                                  <i class="fa fa-plane theme-search-results-item-flight-details-schedule-icon"></i>
                                                                  <div class="theme-search-results-item-flight-details-schedule-dots"></div>
                                                                  <p class="theme-search-results-item-flight-details-schedule-date">Tue, May 23</p>
                                                                  <div class="theme-search-results-item-flight-details-schedule-time">
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-item">08:20
                                                                     <span>pm</span>
                                                                     </span>
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-separator">&mdash;</span>
                                                                     <span class="theme-search-results-item-flight-details-schedule-time-item">04:40
                                                                     <span>pm</span>
                                                                     </span>
                                                                  </div>
                                                                  <p class="theme-search-results-item-flight-details-schedule-fly-time">20h 20m</p>
                                                                  <div class="theme-search-results-item-flight-details-schedule-destination">
                                                                     <div class="theme-search-results-item-flight-details-schedule-destination-item">
                                                                        <p class="theme-search-results-item-flight-details-schedule-destination-title">
                                                                           <b>STN</b>Stansed
                                                                        </p>
                                                                        <p class="theme-search-results-item-flight-details-schedule-destination-city">London</p>
                                                                     </div>
                                                                     <div class="theme-search-results-item-flight-details-schedule-destination-separator">
                                                                        <span>&rarr;</span>
                                                                     </div>
                                                                     <div class="theme-search-results-item-flight-details-schedule-destination-item">
                                                                        <p class="theme-search-results-item-flight-details-schedule-destination-title">
                                                                           <b>EWR</b>Newark Liberty
                                                                        </p>
                                                                        <p class="theme-search-results-item-flight-details-schedule-destination-city">New York</p>
                                                                     </div>
                                                                  </div>
                                                                  <ul class="theme-search-results-item-flight-details-schedule-features">
                                                                     <li>8649 SWISS</li>
                                                                     <li>Narrow-body jet</li>
                                                                     <li>Boeing 747-400</li>
                                                                  </ul>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-md-3 ">
                                          <div class="theme-search-results-item-img-wrap">
										  <?php  
        												    $dirname = "assets/home/images/AirlineLogo";
        													$filename = glob("$dirname/*{$seg[0]->Airline->AirlineCode}*", GLOB_BRACE);
        												?>
        												@if(isset($filename) && count($filename)>0)
        												    
														<img class="theme-search-results-item-img _mob-h" src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" title="Image Title"/>
                                                        @endif
										  
                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
							  @endforeach
							  <button type="button" data-toggle="modal" data-target="#flight" class="change-button">Change Flight</button>
							  <div class="row modal fade" id="flight" role="dialog">
							      <button type="button" class="close" data-dismiss="modal">&times;</button>
<div class="container">
    <div class="modal-header"><h2 style="text-align: center;">Change Your Flight</h2></div>
  <div class="row side-bar">
      <div class="col-md-3 margin-top-headline side-flightfilter filter-block">
    <div class="mtf-nav">
    <ul>
        <li><a id="fly_1" href="#" class="mtta">1</a></li>
        <li><a id="fly_2" href="#" class="mtta">2</a></li>
        <li><a id="fly_3" href="#" class="mtta">3</a></li>
        <li><a id="fly_4" href="#" class="mtta">4</a></li>
        <li><a id="fly_5" href="#" class="mtta active-mttab">5</a></li>
    </ul>
</div>
<div id="flytab_1" class="mttabs hide-mttabs">
    <div class="side-stops">
        <h3 class="fsidebar-head">Stops 1</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>
<div id="flytab_2" class="mttabs hide-mttabs">
<div class="side-stops">
        <h3 class="fsidebar-head">Stops 2</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>
<div id="flytab_3" class="mttabs hide-mttabs">
<div class="side-stops">
        <h3 class="fsidebar-head">Stops 3</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>

<div id="flytab_4" class="mttabs hide-mttabs">
<div class="side-stops">
        <h3 class="fsidebar-head">Stops 4</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>
    <div id="flytab_5" class="mttabs">
<div class="side-stops">
        <h3 class="fsidebar-head">Stops 1</h3>
        <ul class="flight-stop-list">
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check">
           <input type="checkbox" id="check01" name="checkbox">
           <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
           <span class="fside-counts">(10)</span>
        </li>
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="filter_ftime0"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <span class="fside-counts count-fftime">(10)</span>
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Return Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
        <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
           <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <span class="fside-counts">(10)</span>
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
                
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
    </li>
            </ul>
    
        </div>
    </div>
  </div>
 <!--   <div class="col-md-3 margin-top-headline">-->
       
 <!--     <div class="row">-->
       
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h5>Filter Results</h5>-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h5 style="text-align: right;"><a href="#">Reset All</a></h5>-->
 <!--       </div>-->
 <!--     </div>-->
 <!--   <div class="border-bottom"></div>-->
 <!--       <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h4>No. of Stops</h4>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <li class="active" style="border-right: 1px solid #ddd;">-->
 <!--             <a href="#tab_default_1" data-toggle="tab" style="padding-left: 31px;">-->
 <!--             <strong> 0 stop</strong> </a>-->
 <!--             <h6 style="padding-left: 22px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--           <li style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--             <strong> 1 stop</strong></a>-->
 <!--              <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--           <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--           <strong> 1+ stop</strong> </a>-->
 <!--            <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!-- <div class="border-bottom"></div>-->
 <!--  <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h5>Departure time from New Delhi</h5>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">-->
 <!--           <li class="active">-->
 <!--             <a href="#tab_default_1" data-toggle="tab">-->
 <!--            <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> Before 6am No Flights</h6>-->
 <!--           </li>-->
 <!--        </div>-->
 <!--        <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--           <li>-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--               <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> 6 AM-12 PM No Flights</h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;    height: 92px;">-->
 <!--           <li >-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6>12PM - 6 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3">-->
 <!--            <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--           <h6> After 6  PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!--  <div class="border-bottom"></div>-->
 <!--  <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h5>Departure time from Mumbai</h5>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">-->
 <!--           <li class="active">-->
 <!--             <a href="#tab_default_1" data-toggle="tab">-->
 <!--            <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> Before 6am</h6>-->
 <!--           </li>-->
 <!--        </div>-->
 <!--        <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--           <li>-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--               <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6> 6 AM-12 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;">-->
 <!--           <li >-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--            <h6>12PM - 6 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3">-->
 <!--            <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="images/black-sun-png-black-sun-2-icon-256.png" style="    width: 21px;"></a>-->
 <!--           <h6> After 6  PM </h6>-->
 <!--           </li>-->
 <!--           </div>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!-- <div class="border-bottom"></div>-->
 <!-- <div class="row">-->
 <!--   <div class="col-md-10 col-xs-12">-->
 <!--     <h5> Airlines</h5>-->
 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--       <i id="4" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--          <i id="3" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
 <!--    <i id="2" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
 <!--  <i id="item" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="images/unnamed.png">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--<i id="1" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!--   </div>-->
 <!-- </div>-->

 <!--   </div>-->
 <div class="col-md-9 content-side">
      <div class="row multi-flight-list">
	  @if(isset($flights->Response->Results[0]))
	  
		  @foreach($flights->Response->Results[0] as $fd)
      <div class="result_p row mb3" id="ResSet_0">
                                        <div class="retun_inter_sep">
                                            <?php $seg_index=0; ?>
                                            @foreach($fd->Segments as $seg)
                                            <div class="fleft border_bot pad_em width_98">
                                                <span class="spl_icon_flight">
                                                    <kbd class="fleft width_98">
        												<?php  
        												    $dirname = "assets/home/images/AirlineLogo";
        													$filename = glob("$dirname/*{$seg[0]->Airline->AirlineCode}*", GLOB_BRACE);
        												?>
        												@if(isset($filename) && count($filename)>0)
        												    <img src="{{ asset($filename[0])}}" alt="{{$filename[0]}}" class="fleft mr5">
                                                        @endif
        			                                    <code id="Code_nm_mob" class="font10 fleft lh_10 width_64 m_width_100">
        			                                        <em class="mobile_not">  {{$seg[0]->Airline->AirlineName}} </em> 
                                                            <em class="fleft width_100 font10">
                                                                <kbd class="fleft mr5 lh10" id="AirlineDetailsOutBound_0">
                                                                    {{$seg[0]->Airline->AirlineCode}} - {{$seg[0]->Airline->FlightNumber}} 
                                                                </kbd>
                                                                <em class="mobile_class" id="class_0" title="Flight Booking Class">
                                                                    <dfn class="class_re">Class -</dfn> 
                                                                    {{$seg[0]->Airline->FareClass}}
                                                                </em>
                                                            </em>
                                                        </code>
                                                    </kbd>
                                                </span>
                                                <input type="hidden" id="segLenOut_0" value="2">
                                                <input type="hidden" id="segLenIN_0" value="2">
                                                <span class="spl_duration_flight">
                                                    <kbd class="fleft width_100per">
                                                        <em id="departure_0">
                                                            {{$seg[0]->Origin->Airport->AirportCode}}  
                                                            <tt class="hightlight bold" id="deptTimeOut_0">
                                                                ({{date('H:i',strtotime($seg[0]->Origin->DepTime))}})
                                                            </tt>
                                                        </em>                                  
																<?php $count_seg=count($fd->Segments[$seg_index]); ?>
																@if($count_seg>1)
																    @foreach($fd->Segments[$seg_index] as $multi)
                                                                        <code> →</code> 
        															    <dfn id="SegOut_0_0">{{$multi->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_0" value="CCU"></dfn>
        															      
                                                                        <input type="hidden" id="destAirport_0" value="BKK">
                                                                    @endforeach
                                                                    <tt class="hightlight bold">({{date('H:i',strtotime($fd->Segments[$count_seg-1][0]->Destination->ArrTime))}})</tt>
															    @else
    																<code> →</code> <em class="txt_aln_rt" id="arrival_0"> {{$seg[0]->Destination->Airport->AirportCode}} <input type="hidden" id="fltarrOut_0_1" value="BKK"> <tt class="hightlight bold">({{date('H:i',strtotime($seg[0]->Destination->ArrTime))}})</tt></em>
                                                                
                                                                    <input type="hidden" id="destAirport_0" value="BKK">
                                                                @endif
                                                                
                                                                
              <!--                                          <em id="departure_0">-->
              <!--                                              {{$seg[0]->Origin->Airport->AirportCode}}  -->
              <!--                                              <tt class="hightlight bold" id="deptTimeOut_0">-->
              <!--                                                  ({{date('H:i',strtotime($seg[0]->Origin->DepTime))}})-->
              <!--                                              </tt>-->
              <!--                                          </em>                                  -->
														<!--<code> →</code>-->
														<!--<em class="txt_aln_rt" id="arrival_0">-->
    										<!--		        {{$seg[0]->Destination->Airport->AirportCode}}  -->
    										<!--	            <tt class="hightlight bold">-->
    										<!--		            ({{date('H:i',strtotime($seg[0]->Destination->ArrTime))}})-->
    										<!--		        </tt>-->
													 <!--   </em>-->
                                                    </kbd>
                                                </span> 
                                                <span class="spl_dur_flight mobile_not" id="spldurationboxoutbound_0">
                                                    <kbd class="fleft width_100">
                                                        <span id="duration_0">
                                                              <b> 
                                                              <?php $datetimed = new DateTime($seg[0]->StopPointDepartureTime);
                                                                $datetimea = new DateTime($seg[0]->StopPointArrivalTime);
                                                                $interval = $datetimed->diff($datetimea);
                                                                echo $interval->format('%h')."h ".$interval->format('%i')."m"; ?>
                                                               </b></span><br>
                                                         
                                                           
                                                            <a class="stop_1" href="#!" id="outBoundStops_0">
                                                                <small>
                                                                    
                        
                                                                           @if(count($seg)==1)
                                                                            Non Stop
                                                                           @else
                                                                            {{(count($seg)-1)." Stop" }}
                                                                           @endif
                                                                        
                                                                    
                                                                </small></a>
                                                        
                                                    </kbd>
                                                                                                
                                                           <br>
														   @if(isset($seg[0]->NoOfSeatAvailable))
														   <code class="red">{{$seg[0]->NoOfSeatAvailable}} seat(s) left</code> 
														   @else
														   <code>{{"no seat(s) "}}</code>
															@endif
                                                       
                                                        <div class="outbound_stop_popup" id="outBoundStopInfo_0" style="display: none;">
                                                        <a class="fl_close ipad_close_btn" id="outBoundStopInfoClose_0" href="#"></a>
                                                            
                                                                <kbd class="fleft width_100per">
                                                                    <em>SG-253 A</em>
                                                                    <em> &nbsp;&nbsp; DEL(18:20)</em>
                                                                    <code> →</code> 
                                                                                             
                                                                    <em>CCU(20:30)</em>
                                                                </kbd>                                           
                                                            
                                                                <kbd class="fleft width_100per">
                                                                    <em>SG-83 A</em>
                                                                    <em> &nbsp;&nbsp; CCU(00:05)</em>
                                                                    <code> →</code> 
                                                                                             
                                                                    <em>BKK(04:10)</em>
                                                                </kbd>                                           
                                                            
                                                        </div>
                                                    
                                                </span>
                                                <?php $seg_index++; ?>
                                            </div>
                                        @endforeach
           
                                        </div>                        
                                        <span class="spl_price mobile_not" id="pub_fare_0">
                                            <kbd class="fleft Width_100 m_float_rt" id="pubPricekbd_0" style="visibility:visible;">
                                                <tt>Rs. </tt>
                                                <tt id="PubPrice_0">
                                                    {{" ".number_format(money_format('%!i',$fd->Fare->PublishedFare),0)}}
                                                </tt>
                                                
                                            </kbd> 
                                            <span class="icon_flight2 width-120 mobile_not">
                                                <a class="ruppe" href="#" id="FareRule_0"><span class="drop2"> Fare Rule</span></a>
                                                
                                                    <a class="close cur_default" href="#"><span class="drop2" id="isRefundable_0">Lowest Cancellation</span></a>
                                                   
                                                 
                                            </span>  
                                        </span>

                                        <span class="spl_price mobile_not" id="OfferPriceSpan_0" style="visibility:visible;">
                                            <span>Rs.</span>
                                            <span id="OfferPrice_0">
                                             {{" ".number_format(money_format('%!i',$fd->Fare->OfferedFare),0)}}
                                            </span>
                                            
                                        </span>
                                       
                                       <span class="book_now_emails">
                                        <span class="book_now_btn mt5"><a href="#" id="btnBookNow_0" class="btn_bg">Select</a></span>
                                       
                                       </span>
                        
                                        <span class="spl_email spl_pt15">
                                            <input type="checkbox" id="chkEmailItinerary_0" name="emailItinerary" value="0"> Email
                                        </span>
                                         

                                        <div class="fare_rule_pop" id="FareRuleBlock_0" style="display: none;">
                                            <div class="head_pop" id="FareRuleHead">
                                                <span id="FareRuleHeadTitle">Fare Rules</span>
                                                <a href="#" id="btnHeadFareRuleBlockClose_0" class="fl_close"></a>
                                            </div>
                                            <div id="loadingMsg_0">
                                            </div>
                                            <div class="fare_footer" id="FareRuleFoot">
                                                <span class="close_btn cursor">
                                                    <input type="button" value="Close" id="btnFareRuleBlockClose_0">
                                                </span>
                                            </div>
                                        </div>

                                        
                                                    <p class="airline_rem"><span>Remarks for star Coupon.</span></p>
                                                
                                        
                                        <div class="stop_1 fleft relative width_100">     <p class="airline_rem">
                                            <span class="cursor" id="FreeBaggage_0"> Check-In Baggage :
                                          15 Kg ,15 Kg ,15 Kg ,15 Kg 
                                          
                                            </span>
                                             </p>
                                          
                                        </div>
                                         
                                            

                                    </div>
									@endforeach
									@endif
        </div>
      </div>
 
 
 
 
 
 
 
   
        </div>

      </div>
							      
							      </div>
							  
								@endif
							  
					
                           </div>
                        </div>
            <div class="col-md-6 mb30">
                           <div class="headback">
                              <h5 class="formhead"><i class="fa fa-plane"></i> Your Activities</h5>
                                <div class="theme-payment-page-sections-item">
                                <div class="activity_cards">
                                    <div class="col-md-3 activity_icon_class">
                                        <img src="{{asset('assets/home/images/transfers.png')}}" class="active-icon-t">
                                    </div>
                                    <div class="col-md-9 activity_detail_class">
                                        <h5 class="activity_ka_title">Bangkok Airport to Hotel Transfer</h5>
                                        <span class="date-activity">23, Nov 2018</span>
                                    </div>
                                </div>
                                
                                <div class="activity_cards">
                                    <div class="col-md-3 activity_icon_class">
                                        <img src="{{asset('assets/home/images/transfers.png')}}" class="active-icon-t">
                                    </div>
                                    <div class="col-md-9 activity_detail_class">
                                        <h5 class="activity_ka_title">Bangkok Airport to Hotel Transfer</h5>
                                        <span class="date-activity">23, Nov 2018</span>
                                    </div>
                                </div>
                                
                                <div class="activity_cards">
                                    <div class="col-md-3 activity_icon_class">
                                        <img src="{{asset('assets/home/images/transfers.png')}}" class="active-icon-t">
                                    </div>
                                    <div class="col-md-9 activity_detail_class">
                                        <h5 class="activity_ka_title">Bangkok Airport to Hotel Transfer</h5>
                                        <span class="date-activity">23, Nov 2018</span>
                                    </div>
                                </div>
                                <button type="button" data-toggle="modal" data-target="#activity<?php echo $i;?>" class="change-button">Change Activity</button>
                                </div>
                            </div>
                            </div>         
                     </div>
                     
                     <div class="theme-payment-page-sections-item itinerary">
                         <h3 class="itenary-headi">Your Itinerary</h3>
                         <button class="accordion">Day 1</button>
<div class="panel">
  <p>Lorem ipsum...</p>
</div>

<button class="accordion">Day 2</button>
<div class="panel">
  <p>Lorem ipsum...</p>
</div>

<button class="accordion">Day 3</button>
<div class="panel">
  <p>Lorem ipsum...</p>
</div>
                    </div>
                    
                    <div class="theme-payment-page-sections-item itinerary">
                         <button class="accordion">T&C</button>
<div class="panel">
  <p>Lorem ipsum...</p>
</div>


                    </div>
                    
                     
                  </div>
               </div>
               <div class="col-md-4 ">
                  <div class="sticky-col">
                     <div class="theme-sidebar-section _mb-10">
                        <h5 class="theme-sidebar-section-title">Itinerary - 1 City</h5>
                        <div class="theme-sidebar-section-charges">
                           <div class="itenary-cities">Bangkok 3N, Chiang Mai 4N
                           </div>
                           <div class="start-date-tour">13th, November 2018</div>
                           <div class="no-person-tour">Adults (3), Childs(1)</div>
                           <div class="theme-payment-page-sections-item">
                        <div class="theme-payment-page-booking">
                           <div class="theme-payment-page-booking-header">
                              <p class="theme-payment-page-booking-subtitle">By clicking book now button you agree with terms and conditions.</p>
                              <p class="theme-payment-page-booking-price"><span class="theme-payment-page-booking-title">Total Price</span>1278.00</p>
                           </div>
                           <a class="btn _tt-uc btn-primary-inverse btn-lg btn-block" id="book_now" href="#">Book Now</a>
                        </div>
                     </div>
                        </div>
                     </div>
                     
                    
                     <div class="theme-sidebar-section _mb-10">
                        <ul class="theme-sidebar-section-features-list">
                           <li>
                              <h5 class="theme-sidebar-section-features-list-title">Manage your bookings!</h5>
                              <p class="theme-sidebar-section-features-list-body">You're in control of your booking - no registration required.</p>
                           </li>
                           <li>
                              <h5 class="theme-sidebar-section-features-list-title">Customer support available 24/7 worldwide!</h5>
                              <p class="theme-sidebar-section-features-list-body">For Hassle Free Bookings</p>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  </form>
      <div class="theme-copyright">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <p class="theme-copyright-text">Copyright &copy; 2018
                     <a href="#">Jaunt Makers Pvt. Ltd.</a>. All rights reserved.
                  </p>
               </div>
               <div class="col-md-6">
                  <ul class="theme-copyright-social">
                     <li>
                        <a class="fa fa-facebook" href="#"></a>
                     </li>
                     <li>
                        <a class="fa fa-google" href="#"></a>
                     </li>
                     <li>
                        <a class="fa fa-twitter" href="#"></a>
                     </li>
                     <li>
                        <a class="fa fa-youtube-play" href="#"></a>
                     </li>
                     <li>
                        <a class="fa fa-instagram" href="#"></a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div> 
      <script src="{{asset('assets/home/customize/js/jquery.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/moment.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/bootstrap.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/owl-carousel.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/blur-area.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/icheck.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/magnific-popup.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/ion-range-slider.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/sticky-kit.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/smooth-scroll.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/fotorama.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/bs-datepicker.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/typeahead.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/quantity-selector.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/countdown.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/window-scroll-action.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/custom.js')}}"></script>
	  <script>
	  $('#book_now').on('click',function(){
		 $('#package_form')[0].submit(); 
	  });
	  </script>
        
    <script>
        var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
    </script>
    <script>
    $(window).click(function(event) {
  if (!$(event.target).closest(".modal,.js-open-modal").length) {
    $("body").find(".modal").removeClass("visible");
  }
});

    </script>
   </body>
</html>