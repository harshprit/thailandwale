@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')
	<style type="text/css">.append_bottom15 {
    margin-bottom: 15px;
}
.review_steps {
    border-bottom: 1px solid #FAF9F9;
}
.review_borderbottom {
    border-bottom: 1px solid #D8D8D8;
    padding-bottom: 20px;
    padding-top: 20px;
}
.review_steps_info {
    border-bottom: 1px solid #CACACA;
    margin: 0 auto 20px;
    position: relative;
    width: 45%;
}
.step_left {
    left: -30px;
    position: absolute;
    top: -16px;
    width: 80px;
}
.selected .step_icon_select {
    display: block;
    height: 24px;
    margin: 4px auto 5px;
    width: 24px;
    background: url(http://localhost/coinomotion/checkmark.png);
    background-repeat: no-repeat !important;
    background-size: 24px 24px;
}
.block {
    display: block;
}
.step_label {
    color: #838383;
    font-size: 10px;
    font-weight: bold;
    line-height: 10px;
    text-align: center;
}
.edit_blueTxt {
    color: #1E52A4;
}
.step_center {
    left: 45%;
    position: absolute;
    top: -16px;
    width: 66px;
}
.active .step_icon {
    background-position: -139px -237px;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    height: 30px;
    margin: 0 auto 5px;
    padding-top: 6px;
    text-align: center;
    width: 30px;
}
.step_icon {
    background: #000;
    font-size: 0px;
}
.active .step_label {
    color: #2F2F2F;
}
.step_right {
    position: absolute;
    right: -21px;
    top: -16px;
    width: 65px;
}
.tobe_select .step_icon {
    background-position: -143px -271px;
    color: #B1B1B1;
    font-size: 12px;
    font-weight: bold;
    height: 24px;
    margin: 4px auto 7px;
    padding-top: 4px;
    text-align: center;
    width: 24px;
}
.traveller_info_section {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #D4D4D4;
    border-radius: 3px;
}
.append_bottom12 {
    margin-bottom: 12px;
}
.main-div {
    background: none repeat scroll 0 0 #FFFFFF;
}
body {
    background: none repeat scroll 0 0 #E8E7E4;
}
.traveller_info_left {
    background: none repeat scroll 0 0 #FFFFFF;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;
    padding: 15px 0;
}
.traveller_info_right {
    background: none repeat scroll 0 0 #F8F8F8;
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    padding: 15px 0 4px;
}
.info_left_top.last {
    border-bottom: medium none;
    margin-bottom: 0;
    padding-bottom: 0;
}
.review_adult_txt {
    font-size: 12px;
    color: #878787;
    line-height: 12px;
}
.trvler_ttl_amnt {
    color: #CB3904;
    font-size: 16px;
}
.open_icon_markwrapper {
    cursor: pointer;
    padding: 4px 0 4px 7px;
}
.trvlrs_amt_head {
    font-size: 14px;
    color: #2f2f2f;
}
.adult_icon {
    width: 20px;
    height: 20px;
    display: inline-block;
    vertical-align: baseline;
    margin-right: 4px;
}
.adult_icon {
    background: url('http://localhost/coinomotion/adult-icon.png');
    background-repeat: no-repeat !important;
    background-size: 30px 27px;
}
.review_adult_txt .rvw_black {
    margin-right: 5px;
    padding-right: 5px;
    border-right: solid 1px #adadad;
}
.child_icon {
    width: 14px;
    height: 14px;
    display: inline-block;
    vertical-align: baseline;
    margin-right: 4px;
    background: url(http://localhost/coinomotion/child-icon.png);
    background-repeat: no-repeat !important;
    background-size: 100% 100%;
}
.infant_icon {
    width: 12px;
    height: 12px;
    display: inline-block;
    vertical-align: baseline;
    margin-right: 4px;
    background: url(http://localhost/coinomotion/infant-icon.png);
    background-repeat: no-repeat !important;
    background-size: 100% 100%;
}
.city_revwinfo {
    font-size: 16px;
    font-weight: bold;
    line-height: 18px;
    margin-right: 10px;
}
.main-div {
    border-top: 1px solid #D4D4D4;
    padding: 10px 0;
    position: relative;
}
.main_div_closeicon {
    background-position: -48px -35px;
    height: 16px;
    position: absolute;
    right: 15px;
    top: 15px;
    width: 16px;
}
.itineary_dateinfo {
    color: #2F2F2F;
    font-size: 16px;
    font-weight: bold;
    line-height: 16px;
}
.refundable_txt {
    background: none repeat scroll 0 0 #5A9344;
    color: #FFFFFF;
    font-size: 12px;
    padding: 1px 0;
    text-align: center;
}
.append_bottom12 {
    margin-bottom: 12px;
}
.itnry_cityname {
    color: #B1B1B1;
    font-size: 18px;
    line-height: 22px;
}
.main-div .segmented_btn.first {
    margin: 0;
    width: 35%;
}
.segmented_btn.active-button {
    border-bottom: 4px solid #fdd000;
}
.segmented_btn:hover {
    text-decoration: none;
    color: #2f2f2f;
}
.segmented_btn.first {
    border-bottom-left-radius: 3px;
    border-left: 1px solid #D4D4D4;
    border-top-left-radius: 3px;
}
.main-div .segmented_btn.last {
    margin: 0;
    width: 65%;
}
.segmented_btn.last {
    border-bottom-right-radius: 3px;
    border-right: 1px solid #D4D4D4;
    border-top-right-radius: 3px;
}
.segmented_btn {
    border: solid 1px #d4d4d4;
    border-right: 0;
    line-height: 33px;
    background: #fff;
    display: block;
    text-align: center;
    height: 35px;
    color: #2f2f2f;
}
.fare_rules {
    color: #2F2F2F;
    font-size: 12px;
    margin-right: 10px;
}
.fare_border_one {
    border-bottom: 1px solid #D8D8D8;
    border-top: 1px solid #F2F2F2;
    padding-bottom: 12px;
    padding-top: 6px;
}
.total_base {
    font-size: 12px;
    font-weight: bold;
    margin-bottom: 12px;
}
.fare_border {
    border-bottom: 1px solid #F2F2F2;
    padding-bottom: 5px;
}
.append_bottom5 {
    margin-bottom: 5px;
}
.fare_txt {
    font-size: 11px;
}
.rules_tabular th {
    text-align: left;
    font-weight: normal;
    padding: 6px 10px;
    color: #878787;
    border: solid 1px #adadad;
    background: #f8f8f8;
    font-size: 11px;
}
.grand_total {
    background: none repeat scroll 0 0 #E8E7E5;
    font-size: 12px;
    font-weight: bold;
    padding: 6px 10px;
}
.rules_tabular td {
    border: solid 1px #adadad;
    padding: 6px 10px;
    background: #fff;
    vertical-align: top;
    font-size: 11px;
}
.append_bottom8 {
    margin-bottom: 8px;
}
.append_bottom6 {
    margin-bottom: 6px;
}
.light_gray {
    color: #838383;
}
.hide-details {
	display: none;
}
.open-details {
	display: block;
}
.row.segmented_btn, #fflyerdrop {
    cursor: pointer;
}
.passenger_wrapper {
    border-bottom: 1px solid #D8D8D8;
    padding-bottom: 16px;
}
.passenger_details {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #BCBCBC;
    border-radius: 3px;
    padding-bottom: 15px;
    padding-top: 15px;
}
.new_details_passenger:last-child {
    border: 0px;
}
.new_details_passenger {
    border-bottom: 1px solid #ccc;
    margin-bottom: 15px;
    padding-bottom: 15px;
}
.passenger_detail_left {
    padding: 0 0 10px;
}
.details_of_passenger {
    border-bottom: 1px solid #d8d8d8;
    margin-bottom: 15px;
}
.append_bottom10 {
    margin-bottom: 10px;
}
.adult_label {
    font-size: 14px;
    line-height: 14px;
    margin-top: 10px;
}
.gender_option input[readonly] {
    cursor: pointer;
    background: #fff;
}
.fa.fa-chevron-down {
    height: 5px;
    position: absolute;
    right: 12%;
    top: 30%;
    width: 10px;
    color: #000;
}
.flL {
    float: left;
}
</style>

<div class="review_steps clearfix append_bottom15 ng-scope" id="reviewSteps2">
		<div class="review_borderbottom">
			<p class="review_steps_info">
				<span class="step_left selected"> 
					<span class="block step_icon">1</span> 
					<span class="block step_icon_select"></span> 
					<span class="block step_label">REVIEW 
                        (<span class="edit_blueTxt">EDIT</span>)
                    </span>
				</span> 
				<span class="step_center active">
					<span class="block step_icon">2</span> <span class="block step_icon_select"></span> <span class="block step_label">TRAVELLERS</span>
				</span> <span class="step_right tobe_select"> <span class="block step_icon">3</span> <span class="block step_icon_select"></span> <span class="block step_label">PAYMENTS</span>
				</span>

			</p>

		</div>
	</div>
	<div class="container ng-scope">

		<div data-review-flight-summary="" data-flight-component="flightComponent" data-fare-component="fareComponent" data-fare="getTotalFare()">
 <div class="traveller_info_section clearfix col-lg-12 append_bottom12">
	<div class="row">
   
		<div class="traveller_info_left pull-left col-lg-9 col-md-9 col-sm-8 col-xs-12">
			<!-- ngRepeat: combi in flightComponent --><span ng-repeat="combi in flightComponent" class="ng-scope">
        
			<p class="info_left_top clearfix hidden-xs visible-stb last" ng-class="{'last':$last }">
				<span class="logo_part col-lg-3 col-md-3 col-sm-3 col-xs-2">
				     <?php   $dirname = "assets/home/images/AirlineLogo";
                    $filename = glob("$dirname/*{$flights->Response->Results[0][0]->AirlineCode}*", GLOB_BRACE); ?>
                    
                       <span class="review_logoitnry_section pull-left  logo L9W_sm"> 
                        <img src="{{ asset($filename[0])}}" class="img-responsive" style="width: 100%">
                       </span>
                       <span class="pull-left hidden-xs ng-binding">
                           <?php $airline_name=$flights->Response->Results[0][0]->Segments[0][0]->Airline->AirlineName; ?>
                           {{$airline_name}}
                           <!--Jet Airways-->
                        </span>
                    
				</span>
				<?php $flight_origin=$flights->Response->Results[0][0]->Segments[0][0]->Origin->Airport->CityName; ?>
                <?php $flight_dest=$flights->Response->Results[0][0]->Segments[0][0]->Destination->Airport->CityName;?> 
                <?php $flight_start_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointDepartureTime; 
                 $start = date("H:i, d M, Y", strtotime($flight_start_date));
                 ?>
                 
                 <?php $flight_end_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointArrivalTime; 
                     $end = date("H:i, d M, Y", strtotime($flight_end_date));
                 ?>
				<span class="city_time_info col-lg-3 col-md-3 col-sm-4 col-xs-4">
					<span class="pull-left city_revwname hidden-lg hidden-md hidden-sm ng-binding">DEL</span>
					<span class="pull-left city_revwname hidden-xs ng-binding">{{$flight_origin}}</span><br>
					<span class="city_revwinfo pull-left ng-binding">
					    {{$start}}
					    <!--07:05, 05 Aug, 2018-->
					</span>
				</span>
				
				<span class="itineary_info col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<span class="fa fa-arrow-right itineary_arrow pull-left"></span>
				</span>
			
				<span class="city_time_info col-lg-3 col-md-3 col-sm-4 col-xs-4">
					<span class="pull-left city_revwname hidden-lg hidden-md hidden-sm ng-binding">BLR</span>
					<span class="pull-left city_revwname hidden-xs ng-binding">{{$flight_dest}}</span><br>
					<span class="city_revwinfo pull-left ng-binding">
					    {{$end}}
					    <!--09:50, 05 Aug, 2018-->
				    </span>
				</span>
			
				<span class="city_time_info col-lg-2 col-md-2 col-sm-2 hidden-sm hidden-xs">
					<span class="pull-left city_revwname ng-binding">
					    @foreach($flights->Response->Results[0][0]->Segments as $sg)
                            @foreach($sg as $seg)
                               @if($seg->StopPoint=="")
                                Non Stop
                               @else
                                {{$seg->StopPoint." Stop" }}
                               @endif
                            @endforeach
                        @endforeach
					    <!--Non stop-->
					</span> <br>
 						<span class="city_revwinfo pull-left ng-binding">
 						    	<?php $datetimed = new DateTime($flights->Response->Results[0][0]->Segments[0][0]->StopPointDepartureTime);
                            $datetimea = new DateTime($flights->Response->Results[0][0]->Segments[0][0]->StopPointArrivalTime);
                            $interval = $datetimed->diff($datetimea);
                             ?>
                            {{$interval->format('%h')."H ".$interval->format('%i')."m"}}
 						    <!--2h 45m-->
 						</span>
					</span>
				
			</p>
			<p class="clearfix hidden-lg hidden-md hidden-sm hidden-stb">
				<!-- Logo-Part -->
				<span class="logo_part review_logoitnry_section_mbl col-xs-2 logo L9W_sm"></span>
				<!-- /Logo-Part -->
				<!-- Time-City-Info -->
				<span class="city_time_info col-xs-2 flight_detail_align"><span class="city_revwname ng-binding">DEL</span></span>
				<!-- /Time-City-Info -->
				<!-- Itineary-Info -->
				<span class="itineary_info col-xs-2 flight_detail_align"> <span class="fa fa-arrow-right itineary_arrow pull-left"></span>
				</span>
				<!-- Itineary-Info -->
				<!-- Time-City-Info -->
				<span class="city_time_info col-xs-2 row flight_detail_align"><span class="city_revwname ng-binding">BLR</span></span>
				<!-- /Time-City-Info -->
				<!-- Price-Info -->
				<span class="price_info col-xs-4 text-right pull-right"> <span class="block trvler_ttl_amnt ng-binding" ng-show="$first"><span class="INR">Rs.</span>
						284,753</span> 

                        <span class="open_icon_markwrapper block flight_detail_align" ng-click="toggleFltDetails()" ng-show="$last"> <a href="javascript:angular.noop()" class="flight_details_link ">Details</a> <a class="open_icon_mark" href="javascript:angular.noop()"></a> 
				</span>
				</span>
				<!-- /Price-Info -->
			</p>

			</span><!-- end ngRepeat: combi in flightComponent -->
			<!-- For-Mobile-Only -->
			
			<!-- /For-Mobile-Only -->
		
		</div>

		<!-- /Traveller-Info-Left-Section -->
		<!-- Traveller-Info-Right-Section -->
		<div ng-class="{'two_flights': flightComponent.length==2}" class="traveller_info_right pull-left col-lg-3 col-md-3 col-sm-4 text-right hidden-xs visible-stb">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<p class="review_adult_txt append_bottom6">
					<span class="trvlrs_amt_head">
						Total Travelers:
					</span> 
					<span class="adult_icon"></span>
					<!-- ngIf: searchRequest.noOfChd>0 || searchRequest.noOfInfnt>0 --><span ng-if="searchRequest.noOfChd>0 || searchRequest.noOfInfnt>0" class="ng-scope">
						<span class="rvw_black ng-binding">3 </span>
					</span><!-- end ngIf: searchRequest.noOfChd>0 || searchRequest.noOfInfnt>0 -->
					<!-- ngIf: searchRequest.noOfChd==0 && searchRequest.noOfInfnt==0 -->
       	            <!-- ngIf: searchRequest.noOfChd>0 --><span ng-if="searchRequest.noOfChd>0" class="ng-scope">
						<!-- ngIf: searchRequest.noOfInfnt>0 --><span ng-if="searchRequest.noOfInfnt>0" class="ng-scope">
							<span class="child_icon"></span> <span class="rvw_black ng-binding">3</span>
						</span><!-- end ngIf: searchRequest.noOfInfnt>0 -->
						<!-- ngIf: searchRequest.noOfInfnt==0 -->
					</span><!-- end ngIf: searchRequest.noOfChd>0 -->
                   	<!-- ngIf: searchRequest.noOfInfnt>0 --><span ng-if="searchRequest.noOfInfnt>0" class="ng-scope">
						<span class="infant_icon"></span> <span class="rvw_black last ng-binding">3</span>
					</span><!-- end ngIf: searchRequest.noOfInfnt>0 -->
				</p>
			<p class="trvler_ttl_amnt ng-binding">
				<span class="trvlrs_amt_head">
					Total Amount:
				</span>
				<span class="INR">Rs. </span> 284,753
			</p>
				<p class="clearfix open_icon_markwrapper">
					<a id="flightdetailstog" class="flight_details_link">Flight Details</a> <a class="open_icon_mark" href="javascript:angular.noop()"></a>
				</p> 

			</div>
		</div>
		<!-- /Traveller-Info-Right-Section -->
	</div>






<!-- ngRepeat: flight in flightComponent track by $index --><div id="flightdetailsmainblock" class="clearfix main-div row hide-details">
  <!-- ngIf: $first --><a class="fa fa-close main_div_closeicon ng-scope" href="javascript:angular.noop();" ng-click="flightDetails.displayFlightDetails=false;flightDetails.displayItineraryDetails=!flightDetails.displayItineraryDetails;" data-ng-if="$first"></a><!-- end ngIf: $first -->
<!-- Itinerary-Fare-Tab -->
    <div class="itinerary_fare_tab clearfix append_bottom15">
      <div class="wrapper_tab clearfix col-lg-4 col-md-10 col-sm-8 col-xs-11"> <!-- ngIf: $first --><a class="row segmented_btn list_view_btn first pull-left active-button" id="showitinerarydetails">&nbsp;ITINERARY</a><!-- end ngIf: $first -->
     <a class="row segmented_btn map_view_btn last pull-left" id="showfaredetails"> &nbsp;FARE DETAILS &amp; RULES</a> </div>
    </div>
    <!-- /Itinerary-Fare-Tab -->
       <!-- Itineary -->
    <div class="itinerary_tabdata" id="showitinerarydetailsblock">
    <div class="col-lg-12 row append_bottom12">
    <span class="itineary_dateinfo col-lg-8 col-md-8 col-sm-8 col-xs-8 ng-binding">05 Aug 2018, Sun</span>
    <span class="col-lg-2 col-md-2 col-sm-2 col-xs-3 refundable_txt ng-scope" ng-show="canPenCallOff" ng-if="flight.refundable>0">Partially Refundable</span>
    <!-- ngIf: flight.refundable>0 -->
	<!-- ngIf: flight.refundable>0 -->
	<!-- ngIf: flight.nonRefundable>0 -->    
</div>




<!-- Itinerary-Sectional-Info -->
<!-- ngRepeat: flightDetails in flight.legs --><div data-ng-repeat="flightDetails in flight.legs" class="ng-scope">
    <div class="itineary_sectional_info append_bottom20 col-lg-12 row">

 <!-- Stop-Section -->
         <!-- ngIf: flightDetails.lo -->
         <!-- /Stop-Section -->


<!-- Logo-Part -->
      <div class="itineary_logo col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <!-- Logo-Section -->
        <span class="logoitnry_section pull-left logo L9W_sm"></span>
        <!-- /Logo-Section -->
        <!-- Logo-Text -->
        <span class="logo_txt pull-left"><span class="block logo_mobile_review flght_name noWrap ng-binding">Jet Airways</span><span class="block fligh_number noWrap ng-binding">9W-807</span></span>
        <!-- /Logo-Text -->
      </div>
      <!-- /Logo-Part -->
      <!-- Itineary-Departure -->
      <div class="itineary_departure col-lg-2 col-md-2 col-sm-2 col-xs-3"> <span class="block time RobotoRegular ng-binding">07:05</span> <span class="block itnry_cityname ng-binding">DEL</span> <span class="block city_name ng-binding">New Delhi</span> </div>
      <!-- /Itineary-Departure -->
      <!-- Itineary-Arrow -->
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-1"><span class="fa fa-arrow-right itineary_arrow pull-left"></span></div>
      <!-- /Itineary-Arrow -->
      <!-- Itineary-Arrival -->
      <div class="itineary_departure col-lg-2 col-md-2 col-sm-2 col-xs-3"> <span class="block time RobotoRegular ng-binding">09:50</span> <span class="block itnry_cityname ng-binding">BLR</span> <span class="block city_name ng-binding">Bangalore</span> </div>
      <!-- /Itineary-Arrival -->
      <!-- Itineary-Duration -->
      <div class="itineary_duration col-lg-2 col-md-2 col-sm-2 col-xs-3"> <span class="block time RobotoRegular ng-binding">2h 45m</span> <span class="block itnry_cityname noWrap ng-binding" style="width: 75px;">Non stop</span> <span class="block city_name ng-binding">Business</span> </div>
      <!-- /Itineary-Duration -->





		

     </div>
<!-- ngRepeat: flight in flightDetails.v.split('</br>') -->
 
</div><!-- end ngRepeat: flightDetails in flight.legs -->
        
 </div>

    <!-- /Itineary -->
    <!-- Fare-Details -->
    <!-- ngIf: $first --><div class="fare_details clearfix hide-details" id="showfaredetailsblock">
      <!-- Fare-Rules -->
      <div class="fare_rules clearfix">
        <!-- Fare-Left-Section -->
        <div class="fare_left_section pull-left col-lg-4 col-md-5 col-sm-5 col-xs-12">
            
            <span>
             
               <p class="clearfix fare_txt fare_border RobotoRegular append_bottom5" ng-class="{'append_bottom5':$index != (fareComponent.baseFares.length -1)}">
                  <span class="pull-left ng-binding">3 Adult(s)</span>
                  <span class="pull-right ng-binding"><span class="INR">Rs.</span> 118,695</span>
               </p>

               <!-- ngIf: fareComponent.baseFares[1] != null && fareComponent.baseFares[1].noOfPax && searchRequest.noOfChd > 0 --><p class="clearfix fare_txt fare_border RobotoRegular ng-scope append_bottom5" ng-class="{'append_bottom5':$index != (fareComponent.baseFares.length -1)}" ng-if="fareComponent.baseFares[1] != null &amp;&amp; fareComponent.baseFares[1].noOfPax &amp;&amp; searchRequest.noOfChd > 0">
                  <span class="pull-left ng-binding">3 Child(s)</span>
                  <span class="pull-right ng-binding"><span class="INR">Rs.</span> 6,300</span>
               </p><!-- end ngIf: fareComponent.baseFares[1] != null && fareComponent.baseFares[1].noOfPax && searchRequest.noOfChd > 0 -->

               <!-- ngIf: fareComponent.baseFares[1] != null && fareComponent.baseFares[1].noOfPax && searchRequest.noOfChd == 0 && searchRequest.noOfInfnt > 0 -->

               <!-- ngIf: fareComponent.baseFares[2] != null && fareComponent.baseFares[2].noOfPax --><p class="clearfix fare_txt fare_border RobotoRegular ng-scope append_bottom5" ng-class="{'append_bottom5':$index != (fareComponent.baseFares.length -1)}" ng-if="fareComponent.baseFares[2] != null &amp;&amp; fareComponent.baseFares[2].noOfPax">
                  <span class="pull-left ng-binding">3 Infant(s)</span>
                  <span class="pull-right ng-binding"><span class="INR">Rs.</span> 118,695</span>
               </p><!-- end ngIf: fareComponent.baseFares[2] != null && fareComponent.baseFares[2].noOfPax -->
			
            </span>
      
          <p class="total_base fare_border_one clearfix"> <span class="pull-left">Total (Base Fare)</span> <span class="pull-right ng-binding"><span class="INR">Rs.</span>243,690</span> </p>
     
         <!-- ngRepeat: tax in fareComponent.newTaxSummary --><span data-ng-repeat="tax in fareComponent.newTaxSummary" class="ng-scope">
          <p class="clearfix fare_txt fare_border RobotoRegular append_bottom5" ng-class="{'append_bottom5':$index != (fareComponent.taxSummary.length -1)}">
            
                <span ng-if="tax.tdm=='RCF_DM'" class="ng-scope"><span class="pull-left col-lg-8 col-md-8 col-sm-8 col-xs-8">Regional Connectivity Fund</span></span><!-- end ngIf: tax.tdm=='RCF_DM' -->
			
                <span class="pull-right ng-binding"><span class="INR">Rs.</span> 1,125</span> </p>
        </span><!-- end ngRepeat: tax in fareComponent.newTaxSummary --><span data-ng-repeat="tax in fareComponent.newTaxSummary" class="ng-scope">
          <p class="clearfix fare_txt fare_border RobotoRegular append_bottom5" ng-class="{'append_bottom5':$index != (fareComponent.taxSummary.length -1)}">
               
				<span ng-if="tax.tdm=='YQ_DM'" class="ng-scope"><span class="pull-left col-lg-8 col-md-8 col-sm-8 col-xs-8">Airline Fuel Charge</span></span><!-- end ngIf: tax.tdm=='YQ_DM' -->
			
                <span class="pull-right ng-binding"><span class="INR">Rs.</span> 7,200</span> </p>
        </span><!-- end ngRepeat: tax in fareComponent.newTaxSummary --><span data-ng-repeat="tax in fareComponent.newTaxSummary" class="ng-scope">
          <p class="clearfix fare_txt fare_border RobotoRegular" ng-class="{'append_bottom5':$index != (fareComponent.taxSummary.length -1)}">
              <span ng-if="tax.tdm=='GOODS_SERVICE_TAX_DM'" class="ng-scope"><span class="pull-left col-lg-8 col-md-8 col-sm-8 col-xs-8">GST</span></span><!-- end ngIf: tax.tdm=='GOODS_SERVICE_TAX_DM' -->
                <span class="pull-right ng-binding"><span class="INR">Rs.</span> 30,243</span> </p>
        </span><!-- end ngRepeat: tax in fareComponent.newTaxSummary --><span data-ng-repeat="tax in fareComponent.newTaxSummary" class="ng-scope">
          <p class="clearfix fare_txt fare_border RobotoRegular append_bottom5" ng-class="{'append_bottom5':$index != (fareComponent.taxSummary.length -1)}">
              
               
                <span ng-if="tax.tdm=='PSF_DM'" class="ng-scope"><span class="pull-left col-lg-8 col-md-8 col-sm-8 col-xs-8">Passenger Service Fee</span></span><!-- end ngIf: tax.tdm=='PSF_DM' -->
			
                <span class="pull-right ng-binding"><span class="INR">Rs.</span> 924</span> </p>
        </span><!-- end ngRepeat: tax in fareComponent.newTaxSummary --><span data-ng-repeat="tax in fareComponent.newTaxSummary" class="ng-scope">
          <p class="clearfix fare_txt fare_border RobotoRegular append_bottom5" ng-class="{'append_bottom5':$index != (fareComponent.taxSummary.length -1)}">
               <span ng-if="tax.tdm=='UDF_DM'" class="ng-scope"><span class="pull-left col-lg-8 col-md-8 col-sm-8 col-xs-8">User Development Fee</span></span><!-- end ngIf: tax.tdm=='UDF_DM' -->
			
                <span class="pull-right ng-binding"><span class="INR">Rs.</span> 72</span> </p>
        </span><!-- end ngRepeat: tax in fareComponent.newTaxSummary -->

          <p class="total_base fare_border_one clearfix">
                  <span class="pull-left">
                        Total (Fees &amp; Surcharges)
                  </span>
                  <span class="pull-right ng-binding">
                        <span class="INR">Rs.</span>
                        39,564
                  </span>
          </p>

         <!-- ngRepeat: (service,serviceDetails) in fareComponent.services --><div data-ng-repeat="(service,serviceDetails) in fareComponent.services" class="ng-scope">
          <!-- ngIf: serviceDetails.selected --><p class="clearfix fare_txt fare_border RobotoRegular append_bottom5 ng-scope" ng-if="serviceDetails.selected"> <span class="pull-left ng-binding">Insurance </span> <span class="pull-right ng-binding"><span class="INR">Rs. </span> 1,494</span> </p><!-- end ngIf: serviceDetails.selected -->
        </div><!-- end ngRepeat: (service,serviceDetails) in fareComponent.services --><div data-ng-repeat="(service,serviceDetails) in fareComponent.services" class="ng-scope">
          <!-- ngIf: serviceDetails.selected -->
        </div><!-- end ngRepeat: (service,serviceDetails) in fareComponent.services --><div data-ng-repeat="(service,serviceDetails) in fareComponent.services" class="ng-scope">
          <!-- ngIf: serviceDetails.selected -->
        </div><!-- end ngRepeat: (service,serviceDetails) in fareComponent.services --><div data-ng-repeat="(service,serviceDetails) in fareComponent.services" class="ng-scope">
          <!-- ngIf: serviceDetails.selected --><p class="clearfix fare_txt fare_border RobotoRegular append_bottom5 ng-scope" ng-if="serviceDetails.selected"> <span class="pull-left ng-binding">Charity </span> <span class="pull-right ng-binding"><span class="INR">Rs. </span> 5</span> </p><!-- end ngIf: serviceDetails.selected -->
        </div><!-- end ngRepeat: (service,serviceDetails) in fareComponent.services --><div data-ng-repeat="(service,serviceDetails) in fareComponent.services" class="ng-scope">
          <!-- ngIf: serviceDetails.selected -->
        </div><!-- end ngRepeat: (service,serviceDetails) in fareComponent.services --><div data-ng-repeat="(service,serviceDetails) in fareComponent.services" class="ng-scope">
          <!-- ngIf: serviceDetails.selected -->
        </div><!-- end ngRepeat: (service,serviceDetails) in fareComponent.services --> 

			<div data-ng-show="cancAssurAdded == true" <p="" class="clearfix fare_txt RobotoRegular append_bottom5 hide-details"> <span class="pull-left ng-binding">ZERO Cancellation </span> <span class="pull-right ng-binding"><span class="INR">Rs. </span> </span> <p></p>
			</div>

          <p class="total_base fare_border_one clearfix" ng-show="getTotalServices()!=0 || cancAssurAdded == true" "=""><span class="pull-left">TOTAL SERVICES</span><span class="pull-right ng-binding"><span class="INR"> Rs.</span>1,499</span></p>
		  <!-- Conv Fee Addition -->
		  <div class="total_base fare_border_convfee clearfix fd_fee__tooltip hide-details" ng-show="convFeeABReviewAdded"><span class="pull-left ">MMT Convenience Fee
				<a href="javascript:void(0)" class="make_relative inlineB fee_info__icon">
                <!-- MMT Convenience Fee Tooltip -->
      			<div class="fee_tooltip">
      			<div class="fee_tooltip__box roboto-medium">This fee helps us maintain 24*7 support center for valuable customers as you, ensuring that you get the best of services at all times</div>
      			</div>
      			<!-- /MMT Convenience Fee Tooltip -->
                </a> 
				</span>
				<span class="pull-right ng-binding"><span class="INR"> Rs.</span>0</span></div>
		  <!-- / Conv Fee Addition -->
          <p class="grand_total clearfix"><span class="pull-left">GRAND TOTAL</span><span class="pull-right ng-binding"><span class="INR"> Rs.</span>284,753</span></p>
        </div>
        <!-- /Fare-Left-Section -->
        <!-- Space -->
        <div class="col-lg-1 col-md-1 col-sm-1 hidden-stb hidden-xs">&nbsp;</div>
        <!-- /Space -->
        <!-- Fare-Right-Section -->
	     <!-- ngIf: dataLoadedTraveller --><div data-cancellation-penalty="" ng-if="dataLoadedTraveller" data-flags="flags" data-flights="flightComponent" data-index="-1" data-request="searchRequest" class="ng-scope ng-isolate-scope">  <div class="" style="display:block;" data-ng-class="{'fare_rulesection':travellerPenaltyDiv, 'clearfix':travellerPenaltyDiv}">
 <div class="fare_right_section pull-right col-lg-7 col-md-8 col-sm-7 col-xs-12">
                <!-- Cancellation-Fee -->
                <p class="overlay_cart_messages fr_heading_mobile append_bottom8 hidden-xs">Cancellation fee <span class="light_gray">(per passenger)</span></p>
                <!-- [ Visible in Mobile only ] -->
                <!-- ngRepeat: data in  dynamicPenalities --><p class="overlay_cart_messages light_gray append_bottom5 hidden-lg hidden-md hidden-sm hidden-stb ng-scope" data-ng-repeat="data in  dynamicPenalities"><strong class="ng-binding">DEL-BLR</strong> (Cancellation fee)</p><!-- end ngRepeat: data in  dynamicPenalities -->
                <!-- /[ Visible in Mobile only ] -->
                <!-- ngRepeat: data in  dynamicPenalities --><table cellpadding="0" cellspacing="0" border="0" width="100%" class="rules_tabular append_bottom6 ng-scope" data-ng-repeat="data in  dynamicPenalities">
                  <tbody>
                  <tr>
                    <th class="hidden-xs visible-stb fare_col1">Sector</th>
                    <th class="text-center text_mobile_left fare_col2" colspan="2">Cancellation Fee</th>
                  </tr>
                  
                  <tr>
                  
                    <!-- ngIf: data.timeWindows.length > 1 --><td style="width: 80px;" class="hidden-xs visible-stb fare_col1 ng-binding ng-scope" data-ng-if="data.timeWindows.length > 1">DEL-BLR</td><!-- end ngIf: data.timeWindows.length > 1 -->
               
                    <!-- ngIf: data.timeWindows.length > 1 --><td style="width: 200px;" data-ng-if="data.timeWindows.length > 1" class="ng-scope">
                        <!-- ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow>=48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow>=48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow>=48 --><!-- end ngRepeat: days in data.timeWindows -->
                   
                   
                     <!-- ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<48 && days.toTimeWindow>48 --><p data-ng-class="{'fare_col2' : $index==0}" data-ng-repeat="days in data.timeWindows" data-ng-if="days.fromTimeWindow<48 &amp;&amp; days.toTimeWindow>48" class="ng-binding ng-scope fare_col2">
                    24 hrs  -365 days to departure</p><!-- end ngIf: days.fromTimeWindow<48 && days.toTimeWindow>48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<48 && days.toTimeWindow>48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<48 && days.toTimeWindow>48 --><!-- end ngRepeat: days in data.timeWindows -->
                   
                     <!-- ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0 && days.toTimeWindow<=48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0 && days.toTimeWindow<=48 --><p data-ng-class="{'fare_col2' : $index==0}" data-ng-repeat="days in data.timeWindows" data-ng-if="days.fromTimeWindow<=48 &amp;&amp; days.fromTimeWindow>=0 &amp;&amp; days.toTimeWindow<=48" class="ng-binding ng-scope">
                    1-24 hours to departure</p><!-- end ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0 && days.toTimeWindow<=48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0 && days.toTimeWindow<=48 --><p data-ng-class="{'fare_col2' : $index==0}" data-ng-repeat="days in data.timeWindows" data-ng-if="days.fromTimeWindow<=48 &amp;&amp; days.fromTimeWindow>=0 &amp;&amp; days.toTimeWindow<=48" class="ng-binding ng-scope">
                    0-1 hours to departure</p><!-- end ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0 && days.toTimeWindow<=48 --><!-- end ngRepeat: days in data.timeWindows -->
                     </td><!-- end ngIf: data.timeWindows.length > 1 -->
                      
                     <!-- ngIf: data.timeWindows.length > 1 --><td style="width: 220px;" data-ng-if="data.timeWindows.length > 1" class="ng-scope">
                    <!-- ngRepeat: fee in data.timeWindows --><!-- ngIf: fee.fromTimeWindow>=0 --><p data-ng-repeat="fee in data.timeWindows" data-ng-if="fee.fromTimeWindow>=0" class="ng-scope">                    
                    <span ng-show="fee.nonRefundable == 1" ng-class="{slashed_value:(fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1)}" class="ng-binding hide-details">Fare is Non-Refundable (Only taxes and government fees will be refunded)</span>
                    <span ng-show="fee.nonRefundable == 0" ng-class="{slashed_value:(fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1)}" class="ng-binding"><span class="INR">Rs.</span> 4256 + <span class="INR">Rs.</span> 300*</span>
                    <span ng-show="fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1" class="ng-binding hide-details"><span class="INR"> Rs.</span>0</span>
                    </p><!-- end ngIf: fee.fromTimeWindow>=0 --><!-- end ngRepeat: fee in data.timeWindows --><!-- ngIf: fee.fromTimeWindow>=0 --><p data-ng-repeat="fee in data.timeWindows" data-ng-if="fee.fromTimeWindow>=0" class="ng-scope">                    
                    <span ng-show="fee.nonRefundable == 1" ng-class="{slashed_value:(fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1)}" class="ng-binding hide-details">Fare is Non-Refundable (Only taxes and government fees will be refunded)</span>
                    <span ng-show="fee.nonRefundable == 0" ng-class="{slashed_value:(fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1)}" class="ng-binding"><span class="INR">Rs.</span> 4256 + <span class="INR">Rs.</span> 300*</span>
                    <span ng-show="fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1" class="ng-binding hide-details"><span class="INR"> Rs.</span>4256</span>
                    </p><!-- end ngIf: fee.fromTimeWindow>=0 --><!-- end ngRepeat: fee in data.timeWindows --><!-- ngIf: fee.fromTimeWindow>=0 --><p data-ng-repeat="fee in data.timeWindows" data-ng-if="fee.fromTimeWindow>=0" class="ng-scope">                    
                    <span ng-show="fee.nonRefundable == 1" ng-class="{slashed_value:(fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1)}" class="ng-binding hide-details">Fare is Non-Refundable (Only taxes and government fees will be refunded)</span>
                    <span ng-show="fee.nonRefundable == 0" ng-class="{slashed_value:(fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1)}" class="ng-binding"><span class="INR">Rs.</span> 45797 + <span class="INR">Rs.</span> 300*</span>
                    <span ng-show="fee.waiver != undefined &amp;&amp; fee.waiver != null &amp;&amp; fee.waiver != 0 &amp;&amp; flags.cancAssurAdded == 1" class="ng-binding hide-details"><span class="INR"> Rs.</span>45797</span>
                    </p><!-- end ngIf: fee.fromTimeWindow>=0 --><!-- end ngRepeat: fee in data.timeWindows -->
                     </td><!-- end ngIf: data.timeWindows.length > 1 -->
                       
                  </tr>
                 
                  
                  
                  <tr>
                     <!-- ngRepeat: fee in data.timeWindows --><!-- ngIf: data.timeWindows.length ==1 --><!-- end ngRepeat: fee in data.timeWindows --><!-- ngIf: data.timeWindows.length ==1 --><!-- end ngRepeat: fee in data.timeWindows --><!-- ngIf: data.timeWindows.length ==1 --><!-- end ngRepeat: fee in data.timeWindows -->
                    <!-- ngIf: data.timeWindows.length ==1 -->
                   </tr>
                </tbody></table><!-- end ngRepeat: data in  dynamicPenalities -->
                
                
                <p class="light_gray fontSize11 append_bottom8">* MakeMyTrip cancellation service fee</p>
                <!-- /Cancellation-Fee -->
                <!-- Date-Change-Fee -->
                <p class="overlay_cart_messages fr_heading_mobile append_bottom8 hidden-xs">Date change fee <span class="light_gray">(per passenger)</span></p>
                <!-- [ Visible in Mobile only ] -->
                <!-- ngRepeat: data in  dynamicPenalities --><p class="overlay_cart_messages light_gray append_bottom5 hidden-lg hidden-md hidden-sm hidden-stb ng-scope" data-ng-repeat="data in  dynamicPenalities"><strong class="ng-binding">DEL-BLR</strong> (Date change fee)</p><!-- end ngRepeat: data in  dynamicPenalities -->
                <!-- /[ Visible in Mobile only ] -->
                <!-- ngRepeat: data in  dynamicPenalities --><table cellpadding="0" cellspacing="0" border="0" width="100%" class="rules_tabular append_bottom6 ng-scope" data-ng-repeat="data in  dynamicPenalities">
                  <tbody><tr>
                    <th class="hidden-xs visible-stb fare_col1">Sector</th>
                    <th class="text-center text_mobile_left fare_col2" colspan="2">Date change Fee</th>
                  </tr>
                  <tr>

                 <!-- ngIf: data.timeWindows.length > 1 --><td class="hidden-xs visible-stb fare_col1 ng-binding ng-scope" data-ng-if="data.timeWindows.length > 1">DEL-BLR</td><!-- end ngIf: data.timeWindows.length > 1 -->
                   
                    <!-- ngIf: data.timeWindows.length > 1 --><td data-ng-if="data.timeWindows.length > 1" class="ng-scope">   <!-- ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow>=48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow>=48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow>=48 --><!-- end ngRepeat: days in data.timeWindows -->
                   
                   
                     <!-- ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<48 && days.toTimeWindow>48 --><p data-ng-class="{'fare_col2' : $index==0}" data-ng-repeat="days in data.timeWindows" data-ng-if="days.fromTimeWindow<48 &amp;&amp; days.toTimeWindow>48" class="ng-binding ng-scope fare_col2">
                    24 hrs  -365 days to departure</p><!-- end ngIf: days.fromTimeWindow<48 && days.toTimeWindow>48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<48 && days.toTimeWindow>48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<48 && days.toTimeWindow>48 --><!-- end ngRepeat: days in data.timeWindows -->
                   
                     <!-- ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0  && days.toTimeWindow<=48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0  && days.toTimeWindow<=48 --><p data-ng-class="{'fare_col2' : $index==0}" data-ng-repeat="days in data.timeWindows" data-ng-if="days.fromTimeWindow<=48 &amp;&amp; days.fromTimeWindow>=0  &amp;&amp; days.toTimeWindow<=48" class="ng-binding ng-scope">
                    1-24 hours to departure</p><!-- end ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0  && days.toTimeWindow<=48 --><!-- end ngRepeat: days in data.timeWindows --><!-- ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0  && days.toTimeWindow<=48 --><p data-ng-class="{'fare_col2' : $index==0}" data-ng-repeat="days in data.timeWindows" data-ng-if="days.fromTimeWindow<=48 &amp;&amp; days.fromTimeWindow>=0  &amp;&amp; days.toTimeWindow<=48" class="ng-binding ng-scope">
                    0-1 hours to departure</p><!-- end ngIf: days.fromTimeWindow<=48 && days.fromTimeWindow>=0  && days.toTimeWindow<=48 --><!-- end ngRepeat: days in data.timeWindows -->
                                         </td><!-- end ngIf: data.timeWindows.length > 1 -->
                      
                       <!-- ngIf: data.timeWindows.length > 1 --><td data-ng-if="data.timeWindows.length > 1" class="ng-scope">
                    <!-- ngRepeat: fee in data.timeWindows --><!-- ngIf: fee.fromTimeWindow>=0 --><p data-ng-repeat="fee in data.timeWindows" data-ng-if="fee.fromTimeWindow>=0" class="ng-scope">
                    <span ng-show="fee.nonChangeable == 1" class="hide-details"><span class="ng-binding">Non-Changeable</span></span>
                    <span ng-show="fee.nonChangeable == 0" class="ng-binding"><span class="INR">Rs.</span> 0 + <span class="INR">Rs.</span> 300*</span>
                    </p><!-- end ngIf: fee.fromTimeWindow>=0 --><!-- end ngRepeat: fee in data.timeWindows --><!-- ngIf: fee.fromTimeWindow>=0 --><p data-ng-repeat="fee in data.timeWindows" data-ng-if="fee.fromTimeWindow>=0" class="ng-scope">
                    <span ng-show="fee.nonChangeable == 1" class="hide-details"><span class="ng-binding">Non-Changeable</span></span>
                    <span ng-show="fee.nonChangeable == 0" class="ng-binding"><span class="INR">Rs.</span> 0 + <span class="INR">Rs.</span> 300*</span>
                    </p><!-- end ngIf: fee.fromTimeWindow>=0 --><!-- end ngRepeat: fee in data.timeWindows --><!-- ngIf: fee.fromTimeWindow>=0 --><p data-ng-repeat="fee in data.timeWindows" data-ng-if="fee.fromTimeWindow>=0" class="ng-scope">
                    <span ng-show="fee.nonChangeable == 1" class="hide-details"><span class="ng-binding">Non-Changeable</span></span>
                    <span ng-show="fee.nonChangeable == 0" class="ng-binding"><span class="INR">Rs.</span> 45797 + <span class="INR">Rs.</span> 300*</span>
                    </p><!-- end ngIf: fee.fromTimeWindow>=0 --><!-- end ngRepeat: fee in data.timeWindows -->
                     </td><!-- end ngIf: data.timeWindows.length > 1 -->

                  </tr>
                  
                   
                  <tr>
                     <!-- ngRepeat: fee in data.timeWindows --><!-- ngIf: data.timeWindows.length ==1 --><!-- end ngRepeat: fee in data.timeWindows --><!-- ngIf: data.timeWindows.length ==1 --><!-- end ngRepeat: fee in data.timeWindows --><!-- ngIf: data.timeWindows.length ==1 --><!-- end ngRepeat: fee in data.timeWindows -->
                    <!-- ngIf: data.timeWindows.length ==1 -->
                   </tr>
                </tbody></table><!-- end ngRepeat: data in  dynamicPenalities -->
                <p class="light_gray fontSize11 append_bottom8">* MakeMyTrip Date Change service fee</p>
                <!-- /Date-Change-Fee -->
                <!-- Baggage-Allowance -->
                <p class="overlay_cart_messages fr_heading_mobile append_bottom8 hidden-xs">Baggage allowance</p>
                <!-- [ Visible in Mobile only ] -->
                <!-- ngRepeat: data in  dynamicPenalities --><p class="overlay_cart_messages light_gray append_bottom5 hidden-lg hidden-md hidden-sm hidden-stb ng-scope" data-ng-repeat="data in  dynamicPenalities"><strong class="ng-binding">DEL-BLR</strong> (Baggage allowance)</p><!-- end ngRepeat: data in  dynamicPenalities -->
                <!-- /[ Visible in Mobile only ] -->

                <!-- ngRepeat: data in  dynamicPenalities --><table cellpadding="0" cellspacing="0" border="0" width="100%" class="rules_tabular for_baggage append_bottom6 ng-scope" data-ng-repeat="data in  dynamicPenalities">
                  <tbody><tr>
                    <th class="hidden-xs visible-stb fare_col1">Sector</th>
                    <th class="text-center fare_col2">Adult</th>
                    <th class="text-center fare_col3">Child</th>
                    <th class="text-center fare_col4">Infant</th>
                  </tr>
                  <tr>
                    <td class="hidden-xs visible-stb fare_col1 ng-binding">DEL-BLR</td>
                    <td class="text-center fare_col2 ng-binding">2 Pieces (Max 30 kgs)</td>
                    <td class="text-center fare_col3 ng-binding">2 Pieces (Max 30 kgs)</td>
                    <td class="text-center fare_col4 ng-binding"> </td>
                  </tr>
                </tbody></table><!-- end ngRepeat: data in  dynamicPenalities -->
                <!-- /Baggage-Allowance -->
              </div></div>
			
			
			
	</div><!-- end ngIf: dataLoadedTraveller -->
   
        <!-- /Fare-Right-Section -->
      </div>
      <!-- /Fare-Rules -->
    </div><!-- end ngIf: $first -->
    <!-- Fare-Details -->
  </div><!-- end ngRepeat: flight in flightComponent track by $index -->

</div>

<!--- Corporate Fail Modal -->
<div id="corporateFailModal" class="modal fade bs-example-modal-error" tabindex="-1" role="dialog" aria-labelledby="sorry_popup" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body paddTB15">
        <p class="overlay_heading_normalCase clearfix"> <span class="error_icon_1"></span> Oops! </p>
        <div class="append_bottom10">
          <p class="overlay_cart_messages append_bottom10">Looks like the MyBusiness engines need some greasing! Please try your request again. If that doesn’t work, please email us at <a href="mailto:mysupport@makemytrip.com">mysupport@makemytrip.com</a> or call us at 1860 500 8747 to help you out.</p>
          <!--div class="clearfix we_sorry_bottom text-center">
            < p class="light_gray">Not inserted as not required</p>
          </div -->
        </div>
        <!-- button segment -->
        <div class="clearfix">
          <p class="col-xs-8 col-xs-offset-2"> <a class="btn btn-block btn-primary-red btn-md" ng-click="redirectToHome()" title="Try Again" data-dismiss="modal" type="button">Try Again</a> </p>
        </div>
        <!-- /button segment --> 
      </div>
    </div>
  </div>
	</div>

<!--- Corporate Fail Modal End -->

<!-- Corporate Review Fail Model-->
<div id="corporateReviewFailModal" class="modal fade bs-example-modal-error" tabindex="-1" role="dialog" aria-labelledby="sorry_popup" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body paddTB15">
        <p class="overlay_heading_normalCase clearfix"> <span class="error_icon_1"></span> We are sorry! </p>
        <div class="append_bottom10">
          <p class="overlay_cart_messages append_bottom10 ng-binding"></p>
          <div class="clearfix we_sorry_bottom text-center">
            <p>Need help? Email us at <a href="mailto:mysupport@makemytrip.com">mysupport@makemytrip.com</a> or call us at 1860 500 8747</p>
          </div>
        </div>
        <!-- button segment -->
        <div class="clearfix">
          <p class="col-xs-8 col-xs-offset-2"> <a class="btn btn-block btn-primary-red btn-md" ng-click="corpRedirectToHome()" title="Try Again" data-dismiss="modal" type="button">Try Again</a> </p>
        </div>
        <!-- /button segment --> 
      </div>
    </div>
  </div>
	</div>

<!--/ Corporate Review Fail Model -->

<div class="modal " id="holdFailModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">Unable to confirm seat(s)</h4>
      </div>
      <div class="modal-body">Our system indicates that the selected seat(s) may no longer be available at the same price. Proceeding further is most likely to result in a booking failure with the airline systems.  Please select a different flight for your trip.
					
      </div>
        <div class="modal-footer">
			<div class="clearfix row">
						<span class="col-xs-7"> <a ng-click="redirectSearch(redirectURL)" href="javascript:angular.noop()" class="btn btn-block btn-primary-red btn-md ng-binding" name="holdFailedRedirect" id="holdFailedRedirect" title="Hold Failed Redirect">Search Again</a>
						</span>
			</div>
      </div>
    </div>
  </div>
</div>

<div class="modal " id="sessionExpire" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">Session Expired!</h4>
      </div>
      <div class="modal-body">
	Your session has expired! We are reprocessing your request.
		
      </div>
        <div class="modal-footer">
<span class="col-xs-7"> <a ng-click="reloadTraveller()" href="javascript:angular.noop()" class="btn btn-block btn-primary-red btn-md ng-binding" name="ok" title="Ok">Ok</a></span>
      </div>
    </div>
  </div>
</div>
<div class="modal " id="duplicateBooking" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">A similar booking already exists in our system !</h4>
      </div>
      <div class="modal-body">
    <div ng-bind-html="duplicateData" class="ng-binding"></div>
     </div>
        <div class="modal-footer">
			<div class="clearfix row">
						<!-- ngIf: restAPIFlow=='false' -->
	                    <!-- ngIf: restAPIFlow=='true' --><span class="col-xs-5 ng-scope" ng-if="restAPIFlow=='true'"> <a ng-click="setDuplicateBooking(false);checkValidateNew()" href="javascript:angular.noop()" class="btn btn-block btn-primary-red btn-md ng-binding" name="ok" title="Ok">Ok</a></span><!-- end ngIf: restAPIFlow=='true' -->

						 <span class="col-xs-5"> <a ng-click="closeDuplicateBooking()" href="javascript:angular.noop()" class="btn btn-block btn-primary-red btn-md ng-binding" name="Cancel" title="Cancel">Cancel</a>
						</span>
			</div>
      </div>
    </div>
  </div>
</div>
<div class="modal " id="invalidCancelAssured" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">Invalid Cancellation Assurance!</h4>
      </div>
      <div class="modal-body">
    <div ng-bind-html="duplicateData" class="ng-binding"></div>
     </div>
        <div class="modal-footer">
			<div class="clearfix row">
						<!-- ngIf: restAPIFlow=='false' -->
	                    <!-- ngIf: restAPIFlow=='true' --><span class="col-xs-5 ng-scope" ng-if="restAPIFlow=='true'"> <a ng-click="setInvalidCancAssured(false);checkValidateNew()" href="javascript:angular.noop()" class="btn btn-block btn-primary-red btn-md ng-binding" name="ok" title="Ok">Ok</a></span><!-- end ngIf: restAPIFlow=='true' -->

						 <span class="col-xs-5"> <a ng-click="closeinvalidCancelAssurance()" href="javascript:angular.noop()" class="btn btn-block btn-primary-red btn-md ng-binding" name="Cancel" title="Cancel">Cancel</a>
						</span>
			</div>
      </div>
    </div>
  </div>
</div>


<!-- Ecoupon Validation Failure message modal -->
<div class="modal fade" id="couponFailed" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">Ecoupon Failed  !</h4>
      </div>
      <div class="modal-body">
    <div ng-bind-html="couponFailureMessage" class="ng-binding"></div>
     </div>
        <div class="modal-footer">
			<div class="clearfix row">
						<span class="col-xs-5"> <a ng-click="closeCouponModal()" href="javascript:angular.noop()" class="btn btn-block btn-primary-red btn-md ng-binding" name="ok" title="Ok">Ok</a></span>
						
			</div>
      </div>
    </div>
  </div>
</div>



<!-- last name warning message modal -->

<div class="modal " id="EmptyLastName" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
		<span class="Ex_msg flL"></span>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="emptyLstName">Empty Last Name</h4>
      </div>
      <div class="modal-body">
        The last name is missing for one or more traveler(s). The name(s) will be modified as per the special airline rules for handling missing last-names.
		<br> 
		<span ng-bind-html="paxWithEmptyLastNames" class="ng-binding"></span>
		<b>Proceed only if you do not have a last name and you have a valid ID proof without a  last name. </b>
      </div>
        <div class="modal-footer">
		<span class="col-xs-5"> 
            <a href="javascript:angular.noop()" class="btn btn-block btn-secondary btn-md ng-binding AddLastname" data-dismiss="modal" name="AddLastName" title="AddLastName">Add Last Name</a>
		</span>

		<!-- ngIf: restAPIFlow=='true' --><span class="col-xs-5 ng-scope" ng-if="restAPIFlow=='true'"><a ng-click="checkValidateNew()" href="javascript:angular.noop()" ng-class="{'chf_loading':cntuToBook}" ng-disabled="cntuToBook" class="btn btn-block btn-primary-red btn-md ng-binding ContinueToBook" name="ContinueToBook" title="ContinueToBook">Continue to book</a>
		</span><!-- end ngIf: restAPIFlow=='true' -->
		<!-- ngIf: restAPIFlow=='false' -->
      </div>
    </div>
  </div>
</div>

  
  </div>
		 <!-- ngIf: false --> 


		<p class="clearfix append_bottom15">
<span class="col-lg-8 col-md-8 col-sm-8 col-xs-8 row pull-left">
	<span class="mentioned_proof append_bottom8 make_block">
		Please enter names as mentioned on your government ID proof
	</span>
</span>
 	<a class="btn btn-md btn-secondary need_help col-lg-1 col-md-2 col-sm-2 col-xs-4 pull-right hide-details" href="javascript:angular.noop();" ng-show="false">Need Help??</a>
		</p>
		
			<!-- corp approval modal-->            
         	<div id="corpApprovalModal" class="modal fade approval-overlay " tabindex="-1" role="dialog" aria-hidden="true">
            	<div class="modal-body ">
            	<span class="close_overlay" data-dismiss="modal"></span>
					<div>
						<h4>Request Approval</h4>
						<label class="radio clearfix">
                        	<input type="radio" name="dept1" checked="" ng-model="corpSkipped" value="false" class="ng-pristine ng-untouched ng-valid">
                            <span class="radio__outer append_right9"><span class="radio__inner"></span></span>
                            <span class="label-text"> Request approval from manager</span>
                        </label>
                        <div class="choice-info">
                        	This will send an approval request to your manager. You will get a notification to continue the booking when the request gets approved. 
                         	<div class="reason-section">
                        		<p> Please enter valid reason for this booking</p>
                        		<input type="text" ng-model="rData.corpReason" placeholder="E.g. Urgent meeting in Gurgaon, Attending sales conference" class="ng-pristine ng-untouched ng-valid"> 
                        	</div>
                    	</div>
                    	<label class="radio clearfix">
                    		<input type="radio" name="dept1" checked="" ng-model="corpSkipped" value="true" class="ng-pristine ng-untouched ng-valid">
                        	<span class="radio__outer append_right9"><span class="radio__inner"></span></span>
                        	<span class="label-text"> I want to skip approval.</span>
                    	</label>
                    	<p class="choice-info">
                        	We understand that sometimes you need to book in a hurry. Please note that if you skip approval, you’ll have to use your personal payment methods to pay for the booking.
                    	</p>
                    	<p class="clearfix">
                        	<input type="button" class="pull-right btn-primary-red proceed-btn marL10" value="Proceed" ng-click="prePaymentCorporate()">
                        	<input type="button" data-dismiss="modal" class="pull-right btn-secondary proceed-btn " value="Cancel" ng-click="closeCorpApprovalModal()">
                    	</p>
					</div>
				</div>
         	</div>
		 	<!-- /corp approval modal-->
			<!-- /corp meal disclaimer modal-->
			<div id="corpMealDisclaimerModal" class="modal fade approval-overlay " tabindex="-1" role="dialog" aria-hidden="true">
            	<div class="modal-body ">
            	<span class="close_overlay" data-dismiss="modal"></span>
					<div>
						<h4>You have not selected your free meals.<br> Sometimes it takes more time to get the list of meals from the airlines.</h4>
                    	<p class="clearfix text-center">
                        	<input type="button" data-dismiss="modal" class="btn-secondary wrap_content" value="YES, SELECT MEALS" ng-click="closeCorpMealDisclaimerModal()">
                        	<input type="button" class="btn-primary-red  marL10 wrap_content" value="NO, CONTINUE WITH BOOKING" ng-click="closeMealsAndContinueToPayment()">
                    	</p>
					</div>
				</div>
         	</div>
		  <!-- /corp meal disclaimer modal-->
<div class="clearfix">  
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
  	<div class="row">
        <!-- Passenger-Detail-left wrapper non corporate user -->
        <div class="passenger_wrapper clearfix" ng-show="!false">

	        <!-- Passenger-Details -->
            <div class="passenger_details col-xs-12">

                <!-- Passenger-Detail-Left -->
                <div class="passenger_detail_left new_details_passenger">

				<!-- [ Hidden till Tablets ] -->
                    <!-- ngIf: rData.loggedIn -->
                <!-- /[ Hidden till Tablets ] -->

				<!-- ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_passenger">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                   Adult 1:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' --><p class="clearfix flyer_mobile adult_label flyer_no pax_0" ng-if="!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; ffAirlines.length > 0 
                         &amp;&amp; language != 'hi'">

					<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="gender_option col-lg-5 col-md-5 col-sm-5 col-xs-12 frequent_drop noPadding" id="fflyerdrop">Frequent Flyer No. (Optional)
                          <span id="ffindicate" class="fa fa-chevron-down inlineB marL10"></span>
                    </span>
				</p><!-- end ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
			<div id="fflyerblock" class="hide-details">
                <p class="clearfix flyer_earn_point pax_0">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_0">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

            </div>
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_passenger">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                   Adult 2:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' --><p class="clearfix flyer_mobile adult_label flyer_no pax_1" ng-if="!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; ffAirlines.length > 0 
                         &amp;&amp; language != 'hi'">

					<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="gender_option col-lg-5 col-md-5 col-sm-5 col-xs-12 frequent_drop noPadding" data-ng-click="toggleFrequentFlyer(pax, paxIndex)">Frequent Flyer No. (Optional)
                          <span class="fare_ruleicn inlineB marL10"></span>
                    </span>
				</p><!-- end ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
				
                <p class="clearfix flyer_earn_point pax_1">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_1">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

                
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_passenger">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                   Adult 3:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' --><p class="clearfix flyer_mobile adult_label flyer_no pax_2" ng-if="!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; ffAirlines.length > 0 
                         &amp;&amp; language != 'hi'">

					<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="gender_option col-lg-5 col-md-5 col-sm-5 col-xs-12 frequent_drop noPadding" data-ng-click="toggleFrequentFlyer(pax, paxIndex)">Frequent Flyer No. (Optional)
                          <span class="fare_ruleicn inlineB marL10"></span>
                    </span>
				</p><!-- end ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
				
                <p class="clearfix flyer_earn_point pax_2">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_2">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

                
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_passenger">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    CHILD 1:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' --><p class="clearfix flyer_mobile adult_label flyer_no pax_3" ng-if="!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; ffAirlines.length > 0 
                         &amp;&amp; language != 'hi'">

					<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="gender_option col-lg-5 col-md-5 col-sm-5 col-xs-12 frequent_drop noPadding" data-ng-click="toggleFrequentFlyer(pax, paxIndex)">Frequent Flyer No. (Optional)
                          <span class="fare_ruleicn inlineB marL10"></span>
                    </span>
				</p><!-- end ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
				
                <p class="clearfix flyer_earn_point pax_3">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_3">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

                
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_passenger">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    CHILD 2:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' --><p class="clearfix flyer_mobile adult_label flyer_no pax_4" ng-if="!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; ffAirlines.length > 0 
                         &amp;&amp; language != 'hi'">

					<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="gender_option col-lg-5 col-md-5 col-sm-5 col-xs-12 frequent_drop noPadding" data-ng-click="toggleFrequentFlyer(pax, paxIndex)">Frequent Flyer No. (Optional)
                          <span class="fare_ruleicn inlineB marL10"></span>
                    </span>
				</p><!-- end ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
				
                <p class="clearfix flyer_earn_point pax_4">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_4">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

                
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_passenger">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    CHILD 3:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' --><p class="clearfix flyer_mobile adult_label flyer_no pax_5" ng-if="!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; ffAirlines.length > 0 
                         &amp;&amp; language != 'hi'">

					<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="gender_option col-lg-5 col-md-5 col-sm-5 col-xs-12 frequent_drop noPadding" data-ng-click="toggleFrequentFlyer(pax, paxIndex)">Frequent Flyer No. (Optional)
                          <span class="fare_ruleicn inlineB marL10"></span>
                    </span>
				</p><!-- end ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
				
                <p class="clearfix flyer_earn_point pax_5">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_5">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

                
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_infant">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                   Infant 1:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
				
                <p class="clearfix flyer_earn_point pax_6">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_6">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

                
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_infant">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                   Infant 2:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
				
                <p class="clearfix flyer_earn_point pax_7">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_7">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

                
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" class="ng-scope">

                <div ng-class="{'details_of_passenger':!checkPaxType(pax.paxLabel,'INFANT') &amp;&amp; !$last, 'details_of_infant':checkPaxType(pax.paxLabel,'INFANT')}" class="details_of_infant">
                <p class="clearfix append_bottom10">
                
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                   Infant 3:
                </span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="First Name"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Last Name"></span>
                <!-- Adult-Last-Name --> 
                
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info hide-details" ng-show="false">PASSENGER LIST</span>
                    <a class="pull-left passenger_icon hide-details" href="javascript:angular.noop();" ng-show="false" ng-click="showMaxPaxPopup()"></a>
					
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                </p>
					<p ng-show="validationErrrorMessage[$index].length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
							<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
							</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
						</p>

                <p class="clearfix append_bottom30">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->

                <!-- Age -->
                 <span class="age col-lg-4 col-md-5 col-sm-4 col-xs-12 row hide-details" ng-show="(checkPaxType(pax.paxLabel,'CHILD'))"><input type="tel" ng-model="pax.age" ng-blur="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Age"><span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> <span class="error_icon_part pull-left" style="margin-left:0"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span></span>
				</span>
				<span class="select_age col-lg-4 col-md-5 col-sm-4 col-xs-12 row" ng-show="checkPaxType(pax.paxLabel,'INFANT')">
					<select ng-change="validateAge(pax.age,pax.paxLabel,$index)" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="pax.age">
						<option value="">Select  Age (0 to 2 yrs)</option>
						<!-- ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="0" class="ng-binding ng-scope">
      						1 Year or less
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions --><option ng-selected="false" ng-repeat="infantAgeOption in infantAgeOptions" value="1" class="ng-binding ng-scope">
      						1 to 2 Year(s)
    					</option><!-- end ngRepeat: infantAgeOption in infantAgeOptions -->
					</select>

					<span ng-show="validationAgeMessage[$index].length > 0" class="pull-left hide-details" style="margin-top:10px;"> 
						<span class="error_icon_part pull-left" style="margin-left:0"></span> 
						<span class="error_icon_txtinfo pull-left ng-binding"></span>
					</span>
				</span>
                <!-- Age -->
                </p>  


                <!--new changes for flyer number option-->
				
                <!-- ngIf: !checkPaxType(pax.paxLabel,'INFANT') && ffAirlines.length > 0 
                         && language != 'hi' -->
				
                <p class="clearfix flyer_earn_point pax_8">

					<span class="col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
					<span class="col-xs-12 red_text flyer_points_mobile">To earn miles, ensure the frequent flyer number corresponds with the traveler name.
                   
                    </span>
				</p>

				<div class="frequent_flyer pax_8">
				
                <!-- ngRepeat: ffAliance in ffAirlines --><div class="clearfix append_bottom10 make_relative mobile_input ff_0" ng-repeat="ffAliance in ffAirlines" ng-init="ffAllianceIndex = $index">
                   
                   <!-- Adult-Label -->
                   <span class="col-lg-2 col-md-2 col-sm-2 row col-xs-12 hidden-xs">
                      &nbsp;
                   </span>
                   <!-- /Adult-Label -->
                
                   <!-- Adult-First-Name -->
                   <div id="ffnAl" class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option">

					    <input type="text" readonly="" class="input-md form-control light_gray select_drop" value="Select Airline" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)">

                            <a class="flL fa fa-chevron-down" href="javascript:angular.noop()" data-ng-click="toggleAlliance(paxIndex, ffAllianceIndex)"></a> 
					        
                            <ul id="fful" class="dropdown-menu new_drop_menu" ng-class="{'new_drop_menu':ffAliance.length > 3}">

						      <!-- ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="9W_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Jet Airways [9W]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Air New Zealand [NZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AZ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Alitalia [AZ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="NH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        All Nippon [NH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="AA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        American Airlines [AA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="OS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Austrian Airlines [OS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SN_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Brussels Airlines [SN]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="CX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Cathay Pacific [CX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Dragonair [KA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Emirates [EK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="EY_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Etihad Airways [EY]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Garud Indonesia [GA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="GF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Gulf Air [GF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KQ_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Kenya Airways [KQ]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="KL_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        KLM Royal Dutch [KL]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Lufthansa [LH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="MH_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Malaysia Airlines [MH]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="QF_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Qantas Airways [QF]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="SA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        South African [SA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="LX_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Swiss Intl Air [LX]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="TK_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Turkish Airlines [TK]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="UA_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        United [UA]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="US_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        US Airways [US]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance --><li data-ng-repeat="ffAirline in ffAliance" value="VS_DEL-BLR" class="ng-scope">

                                      <a href="javascript:angular.noop()" data-ng-click="testli(ffAirline.title, paxIndex, pax, ffAirline.value, ffAllianceIndex)" class="ng-binding">
                                        Virgin Atlantic [VS]
                                      </a>
                              </li><!-- end ngRepeat: ffAirline in ffAliance -->
						    </ul>

				   </div>
                   <!-- Adult-First-Name -->
                
                   <!-- Adult-Last-Name -->
                <span class="col-lg-5 col-md-5 col-sm-4 col-xs-6 gender_option new_flyer">
					<input type="text" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="Enter FFN" ng-model="pax.freqFlyerNumber[ffAllianceIndex]">
				</span>
                <!-- Adult-Last-Name --> 
                
                
                 
                </div><!-- end ngRepeat: ffAliance in ffAirlines --> 
				</div>

                
 			<!--/new changes for flyer number option-->

                </div>  

				</div><!-- end ngRepeat: pax in rData.paxList -->
                </div>
                <!-- /Passenger-Detail-Left -->
            </div>
            <!-- /Passenger-Details -->
        </div>
        <!-- Passenger-Detail-left wrapper for non corporate user-->
        
		<!-- Passenger-Detail-left wrapper for corporate user STARTS-->
        <!-- Passenger-Detail-left wrapper -->
        <div class="passenger_wrapper no_border clearfix hide-details" ng-show="false">
	        <!-- Passenger-Details -->
            <div class="passenger_details col-xs-12">
                <!-- Passenger-Detail-Left -->
                <div class="passenger_detail_left new_details_passenger">
               
          	<!-- ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope details_of_passenger">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 1
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope details_of_passenger">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 2
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope details_of_passenger">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 3
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope details_of_passenger">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 4
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope details_of_passenger">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 5
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope details_of_passenger">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 6
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope details_of_passenger">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 7
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope details_of_passenger">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 8
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList --><div ng-repeat="pax in rData.paxList" ng-init="paxIndex = $index" ng-class="{'details_of_passenger':!$last}" class="ng-scope">
               
                <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row col-xs-12 ng-binding">
                    Colleague 9
                   
                </span>
              
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12 "><input type="text" placeholder="Work Email" class="input-md form-control work-email error_field ng-pristine ng-untouched ng-valid ui-autocomplete-input" ng-blur="validateWorkEmail(pax.email,$index)" ng-model="pax.email" data-corporate-traveller="" autocomplete="off"></span>
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Contact Number" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.mobile" ng-blur="isValidWorkNumber(pax.mobile, $index)"></span>
              
               
                </p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessageWorkEmail[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                
                  <p class="clearfix append_bottom10">
                <!-- Adult-Label -->
              <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Adult-First-Name -->
                <span class="first_name col-lg-5 col-md-5 col-sm-4 col-xs-12"><input type="text" placeholder="First Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.fname" ng-keyup="validateLength(pax.fname,$event,$index,'first')" ng-blur="validateNames(pax.fname,'First',$index,pax.paxLabel)"></span>
                <!-- Adult-First-Name -->
                <!-- Adult-Last-Name -->
                <span class="last_name col-lg-5 col-md-5 col-sm-4 col-xs-12 row"><input type="text" placeholder="Last Name" class="input-md form-control ng-pristine ng-untouched ng-valid" ng-model="pax.lname" ng-keyup="validateLength(pax.lname,$event,$index,'last')" ng-blur="validateLastNames(pax.lname,'Last',$index,pax.paxLabel)"></span>
                <!-- Adult-Last-Name --> 
                
				</p>
                <p class="clearfix error_case_traveller append_bottom10 hide-details" ng-show="validationErrrorMessage[$index].length > 0">
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">&nbsp;</span>
                <span class="error_icon_part pull-left"></span>
                <span class="error_icon_txtinfo pull-left ng-binding"></span>
                </p>
                <!-- [ Visible in Tablet Only ] -->
                <span class="pull-right passenger_drop_info hidden-lg hidden-md hidden-xs">
                    <span class="pull-left passen_txt_info">Traveler List</span>
                    <a href="#" class="pull-left passenger_icon"></a>
                </span>
                <!-- /[ Visible in Tablet Only ] -->     
                <p></p>
                <p class="clearfix">
                <!-- Adult-Label -->
                <span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs">&nbsp;</span>
                <!-- /Adult-Label -->
                <!-- Gender-Option -->
                <span class="gender_option col-lg-5 col-md-5 col-sm-4 col-xs-12 append_bottom30">
<a class="segmented_btn list_view_btn first pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6 active" ng-click="updateGender(pax,'M');" ng-class="{active:pax.gender=='M'}" href="javascript:angular.noop();">MALE</a>
                <a class="segmented_btn map_view_btn last pull-left col-lg-6 col-md-6 col-sm-6 col-xs-6" ng-click="updateGender(pax,'F');" ng-class="{active:pax.gender=='F'}" href="javascript:angular.noop();">FEMALE</a>     
                </span>
                <!-- /Gender-Option -->
               
                </p>  
				
           
               
            
                </div><!-- end ngRepeat: pax in rData.paxList -->
                  
                  
                <!-- /Passenger-Detail-Left -->
            </div>
            <!-- /Passenger-Details -->
        </div>
</div>
        <!-- Passenger-Detail-left wrapper -->        <!-- Passenger-Detail-left wrapper for coroparte user ENDS-->


        <!-- Add/Update -->
        <p class="clearfix add_update_trvlr append_bottom10 hide-details" ng-show="!false &amp;&amp; rData.loggedIn">
             <span class="pull-left checkbox">
             <span class="checkbox_state pull-right" ng-class="{'active':addTravellersToMasterList}" ng-click="addTravellersToMasterList=!addTravellersToMasterList"></span>
             <input type="checkbox" class="pull-right"> </span>
             <span class="pull-left add_updte_txt">Add above travelers to my Traveler List</span>
             <span class="pull-left add_updte_txt_1 hidden-xs visible-stb">(This information will only be used to improve your experience with us.)</span>
        </p>
        <!-- /Add/Update -->
         
        
          <!-- Contact-Details -->
          <div class="contact_details append_bottom15">
          <p class="contct_txt append_bottom10">CONTACT DETAILS</p>
          <p class="clearfix append_bottom5">
          <span class="pull-left col-lg-2 col-md-2 col-sm-3 col-xs-4 row mobile_code"><input type="text" ng-blur="validateCountryCode(rData.countryCode)" ng-placeholder="+91" ng-model="rData.countryCode" class="input-md form-control ng-pristine ng-untouched ng-valid" placeholder="+91"></span>
          <span class="pull-left col-lg-1 col-md-1 col-sm-1 col-xs-1 dashed_info">-</span>
          <span class="pull-left col-lg-5 col-md-5 col-sm-7 col-xs-8 row phone_number phone_number_row"><input type="tel" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" ng-model="rData.mobileNo" ng-blur="isValidPhoneNumber(rData.mobileNo)" placeholder="Mobile Number"></span>
          <span class="pull-left col-lg-4 col-md-4 mobi_txt">Your Mobile number will be used only for sending flight related communication.</span>
          </p>
		  <!-- ngIf: validCode -->	
			
			
			<p ng-show="validationMobileMessage.length > 0" class="clearfix error_case_traveller append_bottom10 hide-details">
				<span class="adult_label col-lg-2 col-md-2 col-sm-2 row hidden-xs pull-left">
				</span> <span class="error_icon_part pull-left"></span> <span class="error_icon_txtinfo pull-left ng-binding"></span>
			</p>		

          <!-- <p class="mobi_txt append_bottom15">Your Mobile number will be used only for sending flight related communication.</p> -->
		

                <div class="alert alert-danger error_case hide-details" ng-show="isFreeMealSelected">
						<span class="error_icon pull-left"></span>Your seat-upgrade allows for free seats &amp; meals. Please choose using the “Add Services” Button.<button type="button" class="close pull-right" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
					  
					  
					<div class="alert alert-danger error_case ng-binding hide-details" ng-show="InvalidTravelDetails">
						<span class="error_icon pull-left"></span>  <button type="button" class="close pull-right" data-dismiss="alert" aria-hidden="true">×</button>
					</div>


		<!-- GST related changes -->
		<br>
        <div ng-show="!false" class=" gst_section append_bottom20 ">
        	<div class="clearfix ">
             	<span class="pull-left checkbox gst_cb_area">
                	<span class="checkbox_state pull-right" id="gst_checkbox" ng-click="gstClick()"></span>
                	<input class="pull-right" type="checkbox">
             	</span>
		        <div class="pull-left gst_right_sect">  
	            	<p class="font16 append_bottom3">GST number for business travel(optional)</p>
             		<p class="mobi_txt ">To claim credit of GST charged by airlines/MMT, please enter your company's GST number</p>
            		<div class="clearfix gst_form_area">
                		<p class="row append_bottom5">
                			<span class="pull-left col-md-4"><input placeholder="Registration Number" class="input-md form-control ng-pristine ng-untouched ng-valid" type="text" ng-model="rData.gstNumber"></span>
                    		<span class="pull-left col-md-4"><input placeholder="Registered Company Name" class="input-md form-control light_gray ng-pristine ng-untouched ng-valid" type="text" ng-model="rData.gstCompanyName"></span>
                		</p>
                		<!-- error case -->
						<p class="row  hide-details" ng-show="gstErrorMessage &amp;&amp; gstErrorMessage!=''">
    	            		<span class="error_icon_part pull-left"></span>
        	        		<span class="error_icon_txtinfo pull-left ng-binding"></span>
                		</p>
                		<!-- /error case -->
            		</div>
           		</div>
           </div>
           <p class="gst_note ng-binding" style="display: none;">
           		<strong> NOTE: </strong> Appending of Customer GSTN for customer invoicing is not supported by Air Ventura and TrueJet . This may impact your total GST Tax credits.
           </p>
		</div> 
		<!-- /GST related changes -->


		<div class="alert alert-danger error_case ng-binding hide-details" ng-show="InvalidForm">
						<span class="error_icon pull-left"></span>  <button type="button" class="close pull-right" data-dismiss="alert" aria-hidden="true">×</button>
					</div>

<!-- Add-On-Service -->
<!--ancillaries--> 
<!-- ngIf: canShowMealsDiv() -->
<!-- ngIf: canShowSeatsDiv() --> 
<!-- ngIf: canShowBaggageDiv() --> 

<!--/ancillaries-->
       
<div class="clearfix append_bottom16 col-xs-12 filter_applied_wrapper setTop2nd">								
				<div class="freeze_screen" style="display:none">
					<div class="filter_applied_section col-md-4 col-sm-6 col-xs-12 text-center filter_popup_centre_align" style="z-index:999;">
						<p class="filter_applied_heading append_bottom12 loader" style="display:none"></p>
        				<div class="filter_applied_heading append_bottom12"><p></p>
        					<p class="append_bottom12">
           	 				<img alt="loader.." src="v2/images/responsive/loader-responsive.gif">
        				</p></div>
					</div>	
				</div> 
			</div>


<div class="modal fade addons_click dblblack_overlay" tabindex="-1" role="dialog" aria-labelledby="addons" aria-hidden="true" id="doubleBlack">
            <div class="modal-dialog">
                <div class="modal-content">
                        <!-- Modal Body -->
                        <div class="modal-body">
					<!-- Begins:Double Black -->
							<p class="append_bottom25"><span class="db_icon_big"></span></p>
							<p class="font24 text-black ng-binding"></p>
                            <p class="font14 append_bottom22 line-ht18 light_grey ng-binding"></p>
                             <p>
                            	<a ng-click="closeDoubleBlackValidationModal()" href="javascript:angular.noop()" data-dismiss="modal" class="db_link margin_right40">MODIFY BOOKING</a>
                                <a ng-click="submitForm(true)" href="javascript:angular.noop()" class="db_link lato-bold">CONTINUE TO BOOK</a>
                            </p>
				<!-- Ends: Double Black overlay -->
								
                         </div>
                         <!-- /Modal Body -->
                </div>
            </div>
        </div>  
<!-- black-->
           
	      <div class="clearfix append_bottom6 row">
<p class="clearfix append_bottom8 button50per">
               <a class="btn btn-lg btn-primary-red col-lg-5 col-md-5 col-sm-5 col-xs-12 dblblack_bloker ng-binding" href="javascript:angular.noop()" ng-class="{'chf_loading':cuntToPayment}" ng-disabled="cuntToPayment" data-ng-click="submitForm()">Continue to payment</a>
          
 </p>
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
	          
          <p class="mobi_txt ng-binding hide-details" data-ng-show="rData.ancillaryCost>0 ||rData.freeSeatCost==-1">
               You have added services for Amount
               <span class="INR"> Rs.</span>0
               </p> 
         </div>
            
          </div>
         

          <p class="clearfix append_bottom8 button50per hide-details" ng-show="false">
            <a href="javascript:angular.noop();" ng-class="{'chf_loading':cuntToPayment}" class="btn btn-lg btn-primary-red col-lg-5 col-md-5 col-sm-5 col-xs-12 ng-binding" data-ng-click="submitForm()">Continue to payment</a>
            <span class="col-lg-1 col-md-1 col-sm-1 col-xs-1 row hidden-xs visible-stb">&nbsp;</span>
            <a href="javascript:angular.noop()" class="btn btn-lg btn-secondary col-lg-5 col-md-5 col-sm-5 col-xs-12" data-ng-click="showAncillaryModal()">Add travel services</a>
          </p>
          <p class="mobi_txt hide-details" ng-show="false">Choose Seats &amp; add services like meal, baggage etc.</p>
          </div>
          <!-- Contact-Details -->        
     </div>
  </div>
  <!-- /Passanger details and contact form --> 
  
    <!-- Passenger-Detail-Right -->
    <div ng-show="!false" style="padding-right:0;" class="col-lg-4 col-md-4 hidden-sm hidden-stb hidden-xs pull-right">
        <div style="padding-top:10px; padding-bottom:10px;" class="passenger_detail_right passenger_details">
    <div class="col-lg-12 col-md-12">
     <p class="passenger_txt append_bottom6">Traveler List</p>	 
     
     
     <!-- scrollbar area -->  
               
      <!-- ngIf: rData.loggedIn -->
            
     <!-- /scrollbar area -->
     
     
      <!-- Passenger list Empty -->
      <p class="passngr_listing clearfix append_bottom3 hide-details" ng-show="rData.loggedIn &amp;&amp; rData.masterPaxList.length == 0">
        You do not have any passenger saved in your list, start adding passengers so that you do not have to type every time.
      </p>
      <!-- /Passenger list Empty -->
      
     <!-- Passanger not logged In -->
     <p class="passngr_listing clearfix append_bottom3" ng-show="!rData.loggedIn">
        <a href="javascript:angular.noop();" ng-click="showCommonHdrLogin()">Login</a> to manage traveler(s) in your account, for one-click access in future bookings.<br>
        No need to re-type traveler details every time.
     </p>
      <!-- /Passanger list not logged In -->      
    
         </div>
    </div>
    </div>
    <!-- /Passenger-Detail-Right -->
  </div>
	<!-- Login user pop-up -->
	<!--div href="javascript:angular.noop()" data-login-user data-search-key="fareComponent.searchKey"></div-->
	<!-- Ancillary pop-up -->
	<div data-ancillary-services="" data-validate="validateForm()">

<div id="ancillaryModal" role="dialog" data-ng-class="{'modal':!rData.flowType, fade:!rData.flowType ,modal1:!rData.flowType,webCheckin_seatmap:rData.flowType}" class="modal fade modal1">
<div data-ng-class="{'modal-dialog':!rData.flowType, seatmap_ovrlay:!rData.flowType}" class="modal-dialog seatmap_ovrlay">
                <div data-ng-class="{'modal-content':!rData.flowType, overlay_cases:!rData.flowType, 'margin_padding':true}" class="modal-content overlay_cases margin_padding">
                        <!-- Modal Header -->
                        <div data-ng-class="{'modal-header':!rData.flowType}" class="modal-header">
                          <!-- ngIf: !rData.flowType --><p class="overlay_heading clearfix ng-scope" data-ng-click="hideAncillary();" data-ng-if="!rData.flowType">
                                <!-- ngIf: !meals && !seatSummary -->Select  SEATS  <a data-dismiss="modal" class="close_overlay pull-right" aria-hidden="true" href="#">&nbsp;</a>
                          </p><!-- end ngIf: !rData.flowType -->
                        </div>
                       
                        <div data-ng-class="{'modal-body':!rData.flowType}" class="modal-body">
                      
                            	<div class="clearfix border_bottom hidden-xs" id="seatmap_legends_desktop">
                                    <div class="col-xs-4">
                                        <p class="uppercase light_gray"><strong>Select your seats:</strong></p>
                                        <p class="fontSize12 light_gray">Tap your passenger &amp; select a seat</p>
                                    </div>
                                    
                                    <div class="col-xs-8">
                                         <div class="row append_bottom5" data-ng-init="group1=[0,1,2]">
                                            <!-- ngRepeat: i in group1 --><!-- ngIf: selectedLegendArray[i] --><!-- end ngRepeat: i in group1 --><!-- ngIf: selectedLegendArray[i] --><!-- end ngRepeat: i in group1 --><!-- ngIf: selectedLegendArray[i] --><!-- end ngRepeat: i in group1 -->
                                         </div>
                                        
                                         <!-- ngIf: selectedLegendArray.length > 3 -->  
                                         <!-- ngIf: selectedLegendArray.length > 6 -->
                                    </div>
                                </div>
                                
                                <!-- /seatmap top section [ Visible in Desktop and Tablet ] -->
                              <div class="jSeatCarousel-wrapper border_bottom clearfix text-center hidden-lg hidden-md hidden-sm">
                                <div class="jSeatCarousel flight_seat_carousel" data-jcarousel="true" id="seatMapCarousel">
                                  <ul style="left: 0px; top: 0px;">
                                    <!-- ngRepeat: seatDetail in seatArray track by $index -->
                                  </ul>
                                </div>
        						<a href="javascript:angular.noop()" id="seatPrev" class="jSeatCarousel-control-prev" data-jcarouselcontrol="true" data-ng-click="prevPaxDetail()"></a>
        						 <a href="javascript:angular.noop()" id="seatNext" class="jSeatCarousel-control-next" data-jcarouselcontrol="true" data-ng-click="nextPaxDetail()"></a>
                                </div>
                               
                                
                                
                                
                                
                                
                                <!-- flight seat carousel [ Visible in Small Tablets and Mobile ] -->
                              
                                <!-- /flight seat carousel [ Visible in Small Tablets and Mobile ] -->
                                
                                <!-- seatmap top section [ Visible in Small Tablets and Mobile ] -->
                            	<div class="clearfix border_bottom hidden-lg hidden-md hidden-sm top_legends_mobile">
                                        <!-- ngRepeat: (groupId,groupDesc) in selectedLegendMap track by $index -->
                                </div>
                                <!-- /seatmap top section [ Visible in Small Tablets and Mobile ] -->
                              
                                <!-- seatmap section -->
                                <div class="clearfix seatmap_section">
                                	<!-- left panel [ Visible in Desktop and Tablet ] -->
                                	<div class="col-sm-4 flights_tabNav hidden-xs">
                                    	<!-- ngRepeat: sector in sector_key_length track by $index -->
                                        
                                       
                                        <div class="flights_tabNav_bottom">
                                        
                                            <!-- total price -->
                                            <p class="total_seat_pricewrapper append_bottom12">
                                                <span class="total_seat_price clearfix">
                                                    <span class="pull-left">Total Seat price:</span>
                                                    <span class="pull-right ng-binding">
                                                        <span class="INR">Rs.</span> 0
                                                    </span>
                                                </span>
                                            </p>
                                            <!-- /total price -->
                                            
                                            <p class="clearfix append_bottom20" data-ng-show="!rData.flowType">
                                            <a title="Done" class="btn btn-lg btn-primary-red btn-block hidden-xs" href="javascript:angular.noop()" data-ng-click="showSeatMapDiv('seatSummary')">Done</a></p>
                                           
                                            
                                             <p class="clearfix append_bottom20 hide-details" data-ng-show="rData.flowType">
                                             <a title="Done" class="btn btn-lg btn-primary-red btn-block hidden-xs" href="javascript:angular.noop()" data-ng-disabled="showButton()" data-ng-click="showWebCheckInSummary()">Done</a></p>
                                              
                                              	<div class="alert alert-danger error_case hide-details" data-ng-show="showWebCheckinErrorMessage &amp;&amp; rData.flowType">
						                         <span class="error_icon pull-left"></span>please select your seats! 
						                  <!--   <button class="close pull-right" type="button" data-ng-click="showWebCheckinErrorMessage=false;" aria-hidden="true">&times;</button> -->
					                         </div>
                                              
                                              
                                            <!-- Tnc -->
                                             <div class="Tnc" data-ng-click="showTnC=!showTnC">
                                              <p class="light_gray uppercase append_bottom4 tnc_toggle_handler">
                                              	<a href="javascript:angular.noop()" class="make_block">
	                                              	<span class="toggle_state" data-ng-show="showTnC">-</span> 
                                                  <span class="toggle_state hide-details" data-ng-show="!showTnC">+</span>
    	                                            Terms &amp; conditions
                                                </a>
                                              </p>
                                               </div>
                                               <div class="toggle_btm_info col-xs-12" data-ng-show="showTnC" style="text-align:justify">
                                               
                                              <p class="fontSize00">Emergency row seats are allowed only for travelers between 15 and 65 years of age. These seats are not allowed for guests with reduced mobility, visually/speech impared guests, expectant mothers. 
</p>
                                            </div>
                                             
                                           
                                            <!-- /Tnc -->
                                        </div>
                                        
                                    </div>
                                    <!-- /left panel [ Visible in Desktop and Tablet ] -->
                                    
            <!-- right panel -->
		<div class="col-sm-8 col-xs-12 seatmap_innerrightWrap">
			<div class="seat_selection_scroll append_bottom8">
				<!-- Boeing 738-800 template -->
				<!-- ngIf: currentAirCraft['equipmentName']=='738-800' -->
					<!-- /Boeing 738-800 template -->
				
				
				<!-- Boeing A320-200 template -->
					<!-- ngIf: currentAirCraft['equipmentName']=='320-200' -->
				
				
				<!-- /boeing A320-200 template -->
				
				
				<!-- Boeing Q400 template -->
				<!-- ngIf: currentAirCraft['equipmentName']=='Q400' -->
				<!-- Boeing Q400 template -->
				
				<!-- AT7 template -->
				<!-- ngIf: currentAirCraft['equipmentName']=='AT7' -->
				<!-- Boeing Q400 template -->
				
				<!-- Boeing 737-900 template -->
				<!-- ngIf: currentAirCraft['equipmentName']=='737-900' -->

				<!-- ngIf: currentAirCraft['equipmentName']=='737-800' -->

				<!-- Boeing 737-900 template -->
			</div>
		</div>














		<!-- /right panel -->
                                </div>
                                <!-- /seatmap section -->
                                
                                <!-- Done button [ Visible in Mobile and Small Tablet only ] -->
                                <!-- ngIf: !rData.flowType --><div class="clearfix ng-scope" data-ng-if="!rData.flowType">
	                                <a href="javascript:angular.noop()" class="btn btn-lg btn-primary-red btn-block hidden-lg hidden-md hidden-sm done_btn_mobile hidden-xs" title="Done" data-ng-click="showSeatMapDiv('seatSummary');populateAncillary()">Done</a>
                                </div><!-- end ngIf: !rData.flowType -->
                                
                                 <!-- ngIf: rData.flowType -->
                                <!-- /Done button [ Visible in Mobile and Small Tablet only ] -->
                            </div>
                    
                            
       <!-- Start  - Seat Summmary  -->
       
 <div data-ng-show="seatSummary" class="hide-details">
 
 <div class="section_box append_bottom16 no_padding hidden-xs" id="seatSummarySection">
      	<div class="clearfix">
       		<p class="overlay_subheading append_bottom5 border_bottom col-xs-12">Seat selection summary</p>
         </div>
                 <div class="col-xs-12">
                 
                  	<!-- record 1 -->
                  	<!-- ngRepeat: sector in sector_key_length -->
                      <!-- /record 1 -->
                      
          		</div>
              
		</div>
  
   <!-- [ Visible in Mobile and Small Tablets only ] -->
   
   <!-- ngRepeat: sector in sector_key_length -->
  		 
	
   <!-- /[ Visible in Mobile and Small Tablets only ] -->
   
    <!-- total seat price -->
	 <div class="total_seat_price text-right ng-binding">
		Total Seat price: &nbsp;&nbsp; <span class="INR">Rs.</span> 0
	 </div>
	 <!--/total seat price -->
	 
	  <div class="section_box clearfix travel_summary_txt append_bottom16 hidden-xs">
	   Travel services summary: <strong class="ng-binding">Rs. 0</strong>
		  <span class="fontSize11 light_gray" data-ng-show="!showNoTravelService(rData.priceMap)">(You have'nt added any travel sevices)</span>
	  </div>
	  <div>
		  <p class="clearfix">
			<a title="I am Done" class="btn btn-lg btn-primary-red col-sm-5 col-xs-12 pull-right" href="javascript:angular.noop()" data-ng-click="hideAncillaryModal();populateAncillary()">
				I am Done
			 </a>
		  </p>
   	  </div>
    </div>
     

	<div id="popover_HTML" class="seatTooltipContents hide">
    	<div class="inner_carbonPopver_content">
            <p class="light_gray append_bottom3 seatLabelMain"></p>
            <ul class=" seatDescription append_bottom8 light_gray fontSize11 seat_pref_list ">
           	    <li></li>
            </ul>
            <p><span class="seatDesc"> </span><span class="seatPriceDiv">(<span class="INR">Rs.</span> <span class="seatPrice"></span>)</span></p>
		</div>
    </div>
    
    
       
      <!-- End  - Seat Summmary  -->
       </div> 
      <div class="make_relative" data-ng-show="meals">
                                  <!-- Add-ons-Meal Section for Desktop,Medium,Small -->
                                    <!-- styles for filters -->
<!-- /styles for filters -->
<div class="addons_section for_large no_padding clearfix col-sm-12 append_bottom16">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
    <!-- Adult-Name -->
    <div class="adult_name_info col-sm-4 col-xs-3" data-ng-click="hideAncillary()">
    <!-- ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope">
       	<p class="adult_label font_size12 "><strong class="ng-binding">ADULT 1:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope">
       	<p class="adult_label font_size12 "><strong class="ng-binding">ADULT 2:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope">
       	<p class="adult_label font_size12 "><strong class="ng-binding">ADULT 3:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope">
       	<p class="adult_label font_size12 "><strong class="ng-binding">CHILD 1:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope">
       	<p class="adult_label font_size12 "><strong class="ng-binding">CHILD 2:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope">
       	<p class="adult_label font_size12 "><strong class="ng-binding">CHILD 3:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope hide-details">
       	<p class="adult_label font_size12 "><strong class="ng-binding">INFANT 1:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope hide-details">
       	<p class="adult_label font_size12 "><strong class="ng-binding">INFANT 2:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList --><div data-ng-repeat="pax in rData.paxList" data-hide-details="pax.paxLabel.startsWith('I')" class="ng-scope hide-details">
       	<p class="adult_label font_size12 "><strong class="ng-binding">INFANT 3:</strong></p>
      	<p class="adult_label_name border_bottom ng-binding"> </p>
     </div><!-- end ngRepeat: pax in rData.paxList -->
    </div>
    <!-- /Adult-Name -->
    <!-- Meal-Description -->
    <!-- ngRepeat: data in rData.ancillaryData track by $index --></div>
  <!-- /Adult-Info -->


    <!-- FAQ [ Visible till Small Tablets ] -->
    <div class="hidden-xs visible-stb" data-ng-click="showDetails=!showDetails">
        <p class="wht_btmInfo col-xs-12">
            <a href="javascript:angular.noop()">
            <span class="toggle_state hide-details" data-ng-show="showDetails">-</span> 
            <span class="toggle_state" data-ng-show="!showDetails">+</span>
             FAQ's &amp; Terms and Conditions</a>
        </p>
        <!-- toggle div -->
        <div style="display:block;" class="toggle_btm_info col-xs-12 hide-details" data-ng-show="showDetails">
            <p class="uppercase append_bottom10">Meals - General information: </p>
            <ul class="append_bottom20 faq_list">
            	<li>The selected meals are attached to your ticket automatically. </li>
                <li>Pre-booked meals will be served during your flight. Bon Appetit!</li>
                
            </ul>

			<p class="uppercase append_bottom10">Meals - Cancellation and refunds:</p>
            <ul class="append_bottom20 faq_list">
            	<li>Pre-booked meals are fully refundable. In case of any cancellations or date changes made on the ticket, the amount paid for the meals will be refunded.</li>
            </ul>
        </div>
        <!-- /toggle div -->
    </div>
    <!-- /FAQ [ Visible till Small Tablets ] -->
</div>                                  <!-- /Add-ons-Meal Section for Desktop,Medium,Small -->
                                  <!-- Add-ons-Mobile Section for Mobile -->
                                    <!-- styles for filters -->
<!-- /styles for filters -->
<!-- ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">ADULT 1:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">ADULT 2:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">ADULT 3:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">CHILD 1:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">CHILD 2:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">CHILD 3:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope hideAddon" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">INFANT 1:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope hideAddon" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">INFANT 2:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList --><div class="addons_section clearfix col-sm-12 hidden-lg hidden-md hidden-sm hidden-stb append_bottom16 ng-scope hideAddon" data-ng-repeat="pax in rData.paxList" data-ng-class="{'hideAddon':pax.paxLabel.startsWith('I')}">
  <!-- Adult-Info -->
  <div class="adult_info clearfix">
      <p class="adult_label font_size12 append_bottom12 ng-binding" data-ng-click="hideAncillary()"><strong class="ng-binding">INFANT 3:</strong> &nbsp;  </p>
    <!-- ngRepeat: data in rData.ancillaryData track by $index -->
  </div>
  <!-- /Adult-Info -->
  <!-- Term-And-Condition -->
  <div class="term_condition"></div>
  <!-- /Term-And-Condition -->
</div><!-- end ngRepeat: pax in rData.paxList -->
                                  <!-- /Add-ons-Mobile Section for Mobile -->                             
                                  
                                  <!-- Meal dropdown -->
                                  <div style="position:absolute;z-index:2000;right:0;display:block;top:104.5px;" class="addons_section meal_dropdown_options col-xs-8 hide-details" data-ng-show="showMeals">
      
                                    <form role="form" action="#" method="post" name="meal_select" class="ng-pristine ng-valid">

                                    </form>
                                  </div>
                                  <!-- /Meal dropdown -->
                                                      
							  </div>   
							  

                             <p class="text-right append_bottom10 addons_amount" data-ng-show="meals">
                               Addons Amount: 
                                <span class="red_text ng-binding">
                                    <span class="INR">Rs.</span> 0
                                </span>
                             </p>
                             <p class="clearfix" data-ng-show="meals">
                             	<a title="Continue to Payment" class="btn btn-lg btn-primary-red col-sm-5 col-xs-12 pull-right" href="javascript:angular.noop()" data-ng-click="populateAncillary();hideAncillaryModal();addOns!=addOns;">
                                I am Done 
                                </a>
                             </p>
                             <!-- /Amount section -->
                         </div>
                         <!-- /Modal Body -->
                </div>
            </div>
            
            
            
     <div class="modal fade" id="noAncillaryModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       <p class="overlay_heading clearfix">
        No Ancillary Available   <a data-dismiss="modal" class="close_overlay pull-right" aria-hidden="true" href="#">&nbsp;</a>
           </p>
      </div>
      <div class="modal-body">
       No Add Ons available on this flight  
     </div>
        <div class="modal-footer">
			<div class="clearfix row">
		</div>
      </div>
    </div>
  </div>
</div>



     <div class="modal fade" id="showError" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       <p class="overlay_heading clearfix">
                seats not found
          <a data-dismiss="modal" class="close_overlay pull-right" aria-hidden="true" href="javascript:angular.noop()">&nbsp;</a>
           </p>
      </div>
      <div class="modal-body" align="center">
      Something went wrong in attempting web check-in for your itinerary. 
        Either the airline's service is not responding or your booking does not qualify for web check-in. 

     </div>
    </div>
  </div>
</div>



            </div>
  </div>
  

@include('frontend.includes.footer')
  <script type="text/javascript">
  	     document.getElementById("showfaredetails").onclick = function () {
    openFareDetails();};

function openFareDetails () {
	var w = document.getElementById("showitinerarydetailsblock");
    w.classList.remove("open-details");
	w.classList.add("hide-details");
	var x = document.getElementById("showfaredetailsblock");
    x.classList.add("open-details");
	var y = document.getElementById("showfaredetails");
	y.classList.add("active-button");
	var z = document.getElementById("showitinerarydetails");
	z.classList.remove("active-button");
	
}
     document.getElementById("showitinerarydetails").onclick = function () {
    openitineraryDetails();};

function openitineraryDetails () {
	var w = document.getElementById("showfaredetailsblock");
    w.classList.remove("open-details");
	w.classList.add("hide-details");
	var x = document.getElementById("showitinerarydetailsblock");
    x.classList.add("open-details");
	var y = document.getElementById("showitinerarydetails");
	y.classList.add("active-button");
	var z = document.getElementById("showfaredetails");
	z.classList.remove("active-button");
	
}

     document.getElementById("flightdetailstog").onclick = function () {
    openMainFlightDetails();};

function openMainFlightDetails () {

var w = document.getElementById("flightdetailsmainblock");
if (w.classList.contains("hide-details"))
{
    w.classList.remove("hide-details");
}
else
{
	w.classList.add("hide-details");
}
}
// document.getElementById("fflyerdrop").onclick = function () {
// openMainFlightDetails();};

// function openMainFlightDetails () {

// var w = document.getElementById("fflyerblock");
// if (w.classList.contains("hide-details"))
// {
//     w.classList.remove("hide-details");
// }
// else
// {
// 	w.classList.add("hide-details");
// }
// }
//      document.getElementById("fflyerdrop").onclick = function () {
//     openMainFlightDetails();};

// function openMainFlightDetails () {

// var w = document.getElementById("fflyerblock");
// var x = document.getElementById("ffindicate");

// if (w.classList.contains("hide-details"))
// {
//     w.classList.remove("hide-details");
// 	x.classList.remove("fa-chevron-down");
// 	x.classList.add("fa-chevron-up");
	
// }
// else
// {
// 	w.classList.add("hide-details");
// 	x.classList.add("fa-chevron-down");
// 	x.classList.remove("fa-chevron-up");
// }

  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript">$(document).ready(function() {
$(".details_of_passenger").click(function () {

	// var x=$(".details_of_passenger .ff_0");
	// for(i=0;i<x.length;i++)
	// {
	// if(x[i].classList.contains('hide-details'))
	// {
	// 	x[i].classList.remove('hide-details');
	// }
	// else
	// {
	// 	x[i].classList.add('hide-details');
	// }

 //    }
var x=$(".details_of_passenger .ff_0") ;
$(".details_of_passenger .ff_0").each(function(i){
	
		if(x[i].hasClass('hide-details'))
		{
			$(this).removeClass('hide-details');
		}
		else
		{
			$(this).addClass('hide-details');
		}
	
});

 

    // $(".tab").addClass("active"); // instead of this do the below
});
});
</script>


