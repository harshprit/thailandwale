@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')
<style>
.partially-refundable {
    font-size: 11px;
    font-weight: 600;
    color: #e53f3b;
    display: block;
    padding: 10px 0;
}
</style>
<div class="container">
    <div class="row">
      <div class="col-md-12 flights-form">
       
          <div class="col-md-3 col-xs-12 right-border">
            <span><a href="#" data-toggle="modal" data-target="#modal_show">ROUND-TRIP </a></span>
            <P> <a href="#" data-toggle="modal" data-target="#modal_show">New Delhi to Mumbai </a></P>
        </div>
        <div class="col-md-2 col-xs-6 right-border">
          <span> <a href="#" data-toggle="modal" data-target="#modal_show"> DEPARTURE </a> </span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-2  col-xs-6 right-border">
          <span> <a href="#" data-toggle="modal" data-target="#modal_show"> Return </a> </span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Adult</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>01</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Child</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>--</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Infant</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            <span>--</span>
          </a> </p>
        </div>
        <div class="col-md-2 col-xs-12">
          <button type="button" class="btn btn-success">Search</button>
        </div>
   
    
        <div class="modal fade" tabindex="-1" role="dialog" id="modal_show">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header modal-flights">
             <div class="col-md-12 flights-form">
       
          <div class="col-md-3 col-xs-12 right-border">
            <span><a href="#" data-toggle="modal" data-target="#modal_show">ROUND-TRIP </a></span>
            <P> <a href="#">New Delhi to Mumbai </a></P>
        </div>
        <div class="col-md-2 col-xs-6 right-border">
          <span> <a href="#"> DEPARTURE </a> </span>
          <p><a href="#"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-2  col-xs-6 right-border">
          <span> <a href="#"> Return </a> </span>
          <p><a href="#"> <i class="fas fa-calendar-alt"> </i> <span>11</span> 
            <span> <span>Jun 18</span> <span>MON</span></span>
          </a> </p>

        </div> 
        <div class="col-md-1 col-xs-4">
         <span> <a href="#">Adult</a></span>
          <p><a href="#"> 
            <span>01</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#">Child</a></span>
          <p><a href="#"> 
            <span>--</span>
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#">Infant</a></span>
          <p><a href="#"> 
            <span>--</span>
          </a> </p>
        </div>
        <div class="col-md-2 col-xs-12">
          <button type="button" class="btn btn-success">Search</button>
        </div>
   
    </div>
          </div>
          <div class="modal-body">
              <form method="post"  action="{{route('frontend.search_flight')}}" novalidate>
<section class=" form-flights">
      <div class="container">
    <div class="row">
          <div class="col-md-12">
        <div class="row">
              
         <div class="tabbable-panel">
     <div class="tabbable-line">
     <div class="col-md-12 col-xs-12">
   
		 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="travel_class" id="travel_class">
            <div id="tab" class="btn-group btn-group-justified" data-toggle="buttons">
        <a href="#one_way" class="btn btn-default active" data-toggle="tab">
          <input type="radio" name="journey_type" value="1" checked="checked"/>ONE WAY
        </a>
        <a href="#round_trip" class="btn btn-default" data-toggle="tab">
          <input type="radio" name="journey_type" value="2" />ROUND TRIP
        </a>
        <a href="#multi_trip" class="btn btn-default" data-toggle="tab">
          <input type="radio" name="journey_type" value="3"/>MULTI-TRIP
        </a>
    
      </div>
     </div>

   

      <div class="tab-content">
        <div class="tab-pane active" id="">	
        
                          <div class="row">
                        <div class="col-xs-12 col-md-6">
                              <h3>From</h3>
                        <select class="select2" id='input2' name="from[]" required="required">
                              <option value="">Source</option>  
                                <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?> 
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php } }?>

                        </select>
                            </div>
                        <div class="col-xs-12 col-md-6">
                              <h3>To</h3>
                        <select class="select2" id='input3' name="to[]" onchange="setOrigin(this);"  required="required">
                            <option value="">Destination</option>
                              <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?>
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php }
                                 }?>
                        </select>
                        
                            </div>
                            


                        <div class="col-xs-12 col-md-6">
                            <div class="date">
                                <div class="depart">
                                  <h3>Depart</h3>
                                  <input id="datepicker" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy"   required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>
                        <div class="col-xs-12 col-md-6" id="multi-trip-hide">
                              <div class="return">
                            <h3>Return</h3>
                          <input  id="datepicker1" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" onfocus="this.value = '';"  required>
                          </div>
                            </div>

					  <div class="col-xs-12 col-md-6">
                        <div class="popover-markup"> 
                            <div class="trigger form-group form-group-lg form-group-icon-left"><i class="fa fa-users input-icon input-icon-highlight"></i>
                                  <label>Passenger</label>
                                  <input type="hidden" name="passenger_count" id="passenger_count" value="1">
                                  <input type="text" name="passengers_one" id="passengers_one" value="1|Economy" class="form-control">
                            </div>
                            <div id="contactForm" style="display:none">
                                  <div class="triangle"></div>
                            <div class="content">
                                <!-- adult row -->
							<div class="row">
                                  <div class="form-group">
                                      <div class="col-md-3">
                                <label class="control-label"><strong>Adults</strong><br>
                                      <i> (+12 yrs)</i></label></div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn btn-danger" data-dir="dwn" id="adult_dn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="adult_one" id="adult_one" class="form-control text-center" value="1">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="adult_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="2"> Economy </button></div>
                              </div>
							  </div>
							  <!-- adult row end -->
							  <!-- child row -->
							  	<div class="row">
                                  <div class="form-group">
                                      <div class="col-md-3">
                                <label class="control-label"><strong>Children</strong><br>
                                      <i> (+12 yrs)</i></label>
                                      </div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn btn-danger" data-dir="dwn" id="child_dn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="child_one" id="child_one" class="form-control text-center" value="0">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="child_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="3">Premium Economy </button></div>
                              </div>
							  </div>
							 <!-- child row end -->
							 <!-- infant row -->
							  <div class="row">
                                <div class="form-group">
                                    <div class="col-md-3">
                                <label class="control-label"><strong>Infants</strong><br>
                                      <i> (0-2 yrs)</i></label>
                                      </div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner1"> <span class="input-group-btn"> <a class="btn btn-danger" id="inf_dn" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="infants_one" id="infants_one" class="form-control text-center" value="0">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="inf_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="4"> Business </button></div>
                              </div>
							  </div>
							  <!-- infant row end -->
							  <div class="row">
							      <div class="form-group">
                                  <button class="btn btn-default btn-block demise" type="button" id="done">Done</button>
                                  </div>
                              </div>
								  </div>
								  </div>
                          </div>
                            </div>                    

                             <!--- Multi City Starts from Here --->
          <div class="row multi-city-extra" id="multi_city" style="display:none">
                        <div class="row">
                        <div class="col-xs-12 col-md-4">
                              <h3>From</h3>
                        <select class="select2 input-select"  name="from[]" required="required">
                              <option value="">Source</option>  
                                <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?> 
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php } }?>

                        </select>
                            </div>
                        <div class="col-xs-12 col-md-4">
                              <h3>To</h3>
                        <select class="select2 input-select"  name="to[]" required="required" onchange="setOrigin(this.value);">
                            <option value="">Destination</option>
                              <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?>
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php }
                                 }?>
                        </select>
                            </div>
                            


                        <div class="col-xs-12 col-md-4">
                            <div class="date">
                                <div class="depart">
                                  <h3>Depart</h3>
                                  <input  name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" class="datepicker"  required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>		
						</div>
                        
                        <button id="addCityMulti" class="btn btn-warning form-search-btn" type="button">Add City</button>
                        
                      </div>
                      <!---- Multi City Ends Here--->							

							
						
					
                            <input type="submit" class="btn btn-warning form-search-btn" id="test1212" value="Search">
                   
                      </div>
					  </div>
        
      </div>
    </div>
  </div>
</div>
</div>
</div>

          </div>
            
        </div>
  </div>
    </section>
    </form>
<!--            <form method="POST" action="#">-->
<!--                <div class="row">-->
<!--                  <div class="col-md-12">-->
<!--                      <div class="tabbable-panel">-->
<!--        <div class="tabbable-line">-->
<!--          <ul class="nav nav-tabs ">-->
<!--            <li class="active">-->
<!--              <a href="#tab_default_1" data-toggle="tab">-->
<!--              ONE WAY </a>-->
<!--            </li>-->
<!--            <li>-->
<!--              <a href="#tab_default_2" data-toggle="tab">-->
<!--              ROUND TRIP </a>-->
<!--            </li>-->
           
<!--          </ul>-->
<!--          <div class="tab-content">-->
<!--            <div class="tab-pane active" id="tab_default_1">-->
<!--             <div class="row">-->
             
<!--                  <div class="col-xs-6 col-sm-6 col-md-6">-->
<!--                  <div class="form-group"  style="margin-top:20px;">-->
                    
<!--                    <input type="search"  required="required" class="form-control input-sm" value="Select City">-->
<!--                  </div>-->
<!--                </div>-->
              
<!--              <div class="col-xs-6 col-sm-6 col-md-6">-->
<!--                  <div class="form-group" style="margin-top:20px;">-->
                    
<!--                    <input type="search"  required="required" class="form-control input-sm" value="Select City">-->
<!--                  </div>-->
<!--                </div>-->
<!--             </div>-->
<!--             <div class="row">-->
<!--              <div class="col-md-3 col-xs-6">-->
<!--                 <label for="adresse" style="color: #77ab63"> Departure</label>-->
<!--                <div class="form-group">-->
                    
<!--                    <input type="date" name="nom" id="nom" required="required" class="form-control input-sm" value="">-->
<!--                  </div>-->
<!--             </div>-->
<!--             <div class="col-md-3 col-xs-6">-->
<!--               <label for="adresse" style="color: #77ab63">Return</label>-->
<!--                <div class="form-group">-->
                    
<!--                    <input type="date" name="nom" id="nom" required="required" class="form-control input-sm" value="">-->
<!--                  </div>-->
<!--             </div>-->
<!--              </div>-->
<!--              <div class="row">-->
    
    

<!--    <div class=" col-md-3 col-sm-2">-->
<!--      <label style="color: #77ab63">ADULT: 12+ YRS</label> <!-- purely semantic -->-->
<!--      <div class="form-control center merge-bottom-input" name="first">0</div>-->

<!--      <div class="btn-group btn-block" role="group" aria-label="plus-minus">-->
<!--        <button type="button" class="btn btn-sm btn-danger minus-button merge-top-left-button" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></button>-->
<!--        <button type="button" class="btn btn-sm btn-success plus-button merge-top-right-button"><span class="glyphicon glyphicon-plus"></span></button>-->
<!--      </div><!-- end button group -->-->
<!--    </div> <!-- end column -->-->


<!--    <div class=" col-md-3 col-sm-2">-->
<!--      <label style="color: #77ab63">CHILD: 2-12 YRS</label>-->
<!--      <div class="form-control  center merge-bottom-input" name="second">0</div>-->

<!--      <div class="btn-group btn-block" role="group" aria-label="plus-minus">-->
<!--        <button type="button" class="btn btn-sm btn-danger minus-button merge-top-left-button" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></button>-->
<!--        <button type="button" class="btn btn-sm btn-success plus-button merge-top-right-button"><span class="glyphicon glyphicon-plus"></span></button>-->
<!--      </div><!-- end button group -->-->
<!--    </div> <!-- end column -->-->


<!--    <div class=" col-md-3 col-sm-2">-->
<!--      <label style="color: #77ab63">INFANT: 0-2 YRS</label>-->
<!--      <div class="form-control  center merge-bottom-input" name="third">0</div>-->

<!--      <div class="btn-group btn-block" role="group" aria-label="plus-minus">-->
<!--        <button type="button" class="btn btn-sm btn-danger minus-button merge-top-left-button" disabled="disabled"><span class="glyphicon glyphicon-minus"></span></button>-->
<!--        <button type="button" class="btn btn-sm btn-success plus-button merge-top-right-button"><span class="glyphicon glyphicon-plus"></span></button>-->
<!--      </div><!-- end button group -->-->
<!--    </div> <!-- end column -->-->
    
<!--    <div class="col-sm-3"></div>-->
<!--<div class="col-sm-3"></div>-->
<!--  </div><!-- end row -->-->
<!--  <div class="tabbable-line">-->
<!--          <ul class="nav nav-tabs " style="margin-top: 22px;">-->
<!--            <li class="active">-->
<!--              <a href="#tab_default_1" data-toggle="tab">-->
<!--              ECONOMY </a>-->
<!--            </li>-->
<!--            <li>-->
<!--              <a href="#tab_default_2" data-toggle="tab">-->
<!--              BUSINESS</a>-->
<!--            </li>-->
           
<!--          </ul>-->
<!--           </div>-->
<!--           <button type="submit" class="btn btn-lg" style="width: 159px;-->
<!--    padding: 12px;-->
<!--    margin-top: 20px;">Cancle</button>-->
<!--           <button type="submit" class="btn btn-danger btn-lg" style="width: 159px;-->
<!--    padding: 12px;-->
<!--    margin-top: 20px;">Search</button>-->
<!--          </div>-->
<!--        </div>-->
<!--      </div>-->
<!--              </div>-->
<!--              </div>-->
<!--            </div>-->
<!--             </form>-->
            </div>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
</div>

</div>
</form>
</div>
<!--======= flights listing form ================-->
<!--================= container ================-->
<div class="container">
  <div class="row side-bar">
      <div class="col-md-3 margin-top-headline side-flightfilter filter-block desktop-dhappa">
    <div class="side-stops">
        <h3 class="fsidebar-head">Stops</h3>
<ul class="flight-stop-list">
    <li class="custom-check">
       <input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>0 Stops</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check">
       <input type="checkbox" id="check01" name="checkbox">
       <label for="check02"><span><i class="fa fa-check"></i></span>0 Stops</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check">
       <input type="checkbox" id="check01" name="checkbox">
       <label for="check03"><span><i class="fa fa-check"></i></span>0 Stops</label>
       <span class="fside-counts">(10)</span>
    </li>
    
</ul>
    </div><div class="side-flytime">
        <h3 class="fsidebar-head">Departure Time</h3>
<ul class="flight-flytime-list">
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check04"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
       <label for="check01"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
       <span class="fside-counts">(10)</span>
    </li>
        </ul>
    </div>
<div class="side-airlines">
        <h3 class="fsidebar-head">Airlines</h3>
<ul class="flight-airlines-list">
            
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Spicejet</label><span class="fside-counts">(10)</span>
</li>
        </ul>
    </div>
<div class="side-airfare">
        <h3 class="fsidebar-head">Fare Range</h3>
<ul class="flight-fare-list">
            <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>Under 2000</label><span class="fside-counts">(10)</span>
</li>
            
            <li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>2001-4000</label><span class="fside-counts">(10)</span>
</li>
<li class="custom-check"><input type="checkbox" id="check01" name="checkbox">
   <label for="check01"><span><i class="fa fa-check"></i></span>4001-6000</label><span class="fside-counts">(10)</span>
</li>
        </ul>

    </div>
  </div>
 <!--   <div class="col-md-3 margin-top-headline">-->
       
 <!--     <div class="row">-->
       
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h5>Filter Results</h5>-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h5 style="text-align: right;"><a href="#">Reset All</a></h5>-->
 <!--       </div>-->
 <!--     </div>-->
 <!--   <div class="border-bottom"></div>-->
 <!--       <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h4>No. of Stops</h4>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <li class="active" style="border-right: 1px solid #ddd;">-->
 <!--             <a href="#tab_default_1" data-toggle="tab" style="padding-left: 31px;">-->
 <!--             <strong> 0 stop</strong> </a>-->
 <!--             <h6 style="padding-left: 22px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--           <li style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--             <strong> 1 stop</strong></a>-->
 <!--              <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--           <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--           <strong> 1+ stop</strong> </a>-->
 <!--            <h6 style="padding-left: 13px;"><i class="fa fa-inr" aria-hidden="true"></i> 14,256</h6>-->
 <!--           </li>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!-- <div class="border-bottom"></div>-->
 <!--  <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h5>Departure time from New Delhi</h5>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">-->
 <!--           <li class="active">-->
 <!--             <a href="#tab_default_1" data-toggle="tab">-->
 <!--            <img src="{{asset('images/black-sun-png-black-sun-2-icon-256.png')}}" style="    width: 21px;"></a>-->
 <!--            <h6> Before 6am No Flights</h6>-->
 <!--           </li>-->
 <!--        </div>-->
 <!--        <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--           <li>-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--               <img src="{{asset('images/black-sun-png-black-sun-2-icon-256.png')}}" style="    width: 21px;"></a>-->
 <!--            <h6> 6 AM-12 PM No Flights</h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;    height: 92px;">-->
 <!--           <li >-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="{{asset('images/black-sun-png-black-sun-2-icon-256.png')}}" style="    width: 21px;"></a>-->
 <!--            <h6>12PM - 6 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3">-->
 <!--            <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="{{asset('images/black-sun-png-black-sun-2-icon-256.png')}}" style="    width: 21px;"></a>-->
 <!--           <h6> After 6  PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!--  <div class="border-bottom"></div>-->
 <!--  <div class="row">-->
 <!--   <div class="col-md-12"> -->
      <!-- Nav tabs -->
 <!--     <h5>Departure time from Mumbai</h5>-->
 <!--         <div class="tabbable-panel">-->
 <!--       <div class="tabbable-line">-->
 <!--         <ul class="nav nav-tabs" style="border: 1px solid #ddd;">-->
 <!--           <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd;">-->
 <!--           <li class="active">-->
 <!--             <a href="#tab_default_1" data-toggle="tab">-->
 <!--            <img src="{{asset('images/black-sun-png-black-sun-2-icon-256.png')}}" style="    width: 21px;"></a>-->
 <!--            <h6> Before 6am</h6>-->
 <!--           </li>-->
 <!--        </div>-->
 <!--        <div class="col-md-3 col-xs-3"  style="border-right: 1px solid #ddd; text-decoration-line: none;">-->
 <!--           <li>-->
 <!--             <a href="#tab_default_2" data-toggle="tab">-->
 <!--               <img src="{{asset('images/black-sun-png-black-sun-2-icon-256.png')}}" style="    width: 21px;"></a>-->
 <!--            <h6> 6 AM-12 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3" style=" border-right: 1px solid #ddd;text-decoration-line: none;">-->
 <!--           <li >-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="{{asset('images/black-sun-png-black-sun-2-icon-256.png')}}" style="    width: 21px;"></a>-->
 <!--            <h6>12PM - 6 PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         <div class="col-md-3 col-xs-3">-->
 <!--            <li style="text-decoration-line: none;">-->
 <!--             <a href="#tab_default_3" data-toggle="tab">-->
 <!--             <img src="{{asset('images/black-sun-png-black-sun-2-icon-256.png')}}" style="    width: 21px;"></a>-->
 <!--           <h6> After 6  PM </h6>-->
 <!--           </li>-->
 <!--         </div>-->
 <!--         </ul>-->
         
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!-- </div>-->
 <!-- <div class="border-bottom"></div>-->
 <!-- <div class="row">-->
 <!--   <div class="col-md-12 col-xs-12">-->
 <!--     <h5> Airlines</h5>-->
 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <//?php   $dirname = "assets/home/images/AirlineLogo";-->
 <!--       $filename = glob("$dirname/*{6E}*", GLOB_BRACE); ?>-->
 <!--         @if(isset($filename) && count($filename)>0)-->
 <!--         <img src="{{ asset($filename[0])}}" height="50px" width="50px">-->
 <!--         @endif-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--       <i id="4" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="{{asset('images/unnamed.png')}}">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--          <i id="3" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->
 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="{{asset('images/unnamed.png')}}">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
 <!--    <i id="2" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->

 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="{{asset('images/unnamed.png')}}">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
 <!--  <i id="item" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!--     <div class="Airlines-section">-->
 <!--     <div class="row">-->
        
 <!--       <div class="col-md-2 col-xs-2">-->
 <!--         <img src="{{asset('images/unnamed.png')}}">-->
 <!--       </div>-->
 <!--       <div class="col-md-6 col-xs-6">-->
 <!--         <h4>India Go</h4>-->
 <!--         <h6><i class="fa fa-inr" aria-hidden="true"></i> 14,352 Onwards</h6>-->
 <!--       </div>-->
 <!--       <div class="col-md-3 col-xs-3">-->
           
 <!--<i id="1" class="fa fa-square-o fa-5x" onclick="change(this.id)"></i>-->

 <!--       </div>-->
 <!--     </div>-->
 <!--   </div>-->
 <!--   </div>-->
 <!-- </div>-->

 <!--   </div>-->
    <div class="col-md-9 content-side">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="row">
          <div class="col-md-2 col-xs-2">
            <img src="{{asset('assets/home/images/59878-200.png')}}" style="width:36%;">
          </div>
            <div class="col-md-10 col-xs-10">
                <?php $flight_origin=$flights->Response->Results[0][0]->Segments[0][0]->Origin->Airport->CityName; ?>
                <?php $flight_dest=$flights->Response->Results[0][0]->Segments[0][0]->Destination->Airport->CityName;?> 
                 <?php $flight_start_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointDepartureTime; 
                 $start = date("D, j M, Y", strtotime($flight_start_date));
                 ?>
                 
                 <?php $flight_end_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointArrivalTime; ?>
                <strong>{{$flight_origin}} to {{$flight_dest}}</strong> - <span>{{$start}}</span>
              <h6><span id="total_flights"></span></h6>
           
          </div>
        </div>
            <div class="tabbable-panel">
        <div class="tabbable-line">
          <ul class="nav nav-tabs ">

            <li class="active">
              <a href="#1" data-toggle="tab">
              DURATION </a>
            </li>

            <li>
              <a href="#2" data-toggle="tab">
              DEPARTURE </a>
            </li>
            <li>
              <a href="#3" data-toggle="tab">
             ARRIVAL </a>
            </li>
               <li>
              <a href="#4" data-toggle="tab">
             PRICE </a>
            </li>
          </ul>

          <div class="tab-content">


            <div class="tab-pane active" id="1">
<?php $flight_result=$flights->Response->Results[0]; $i=1;$j=1; ?>
@foreach($flight_result as $fd)
             <div class="row">
              <div class="col-md-12 col-xs-12 listing-page-tab">
                
                <div class="col-md-2 col-xs-2">
                  
                   <?php   $dirname = "assets/home/images/AirlineLogo";
                    $filename = glob("$dirname/*{$fd->AirlineCode}*", GLOB_BRACE); ?>
                    @if(isset($filename) && count($filename)>0)
                    <img src="{{ asset($filename[0])}}" class="img-responsive" style="width: 50%">
                    @endif
                 <a class="flight-listname" href="#"> 
                    <p>
                    @foreach($fd->Segments as $segment)
                        @foreach($segment as $sg)
                          {{$sg->Airline->AirlineName}}
                        @endforeach
                    @endforeach
                    </p>
                    <span>
                        @foreach($fd->Segments as $sg)
                                @foreach($sg as $seg)
                                   {{$seg->Airline->AirlineCode}}-{{$seg->Airline->FlightNumber}}
                                @endforeach
                            @endforeach
                    </span>
                 </a>
                </div>
                <div class="col-md-2 col-xs-2">
                  <h3 class="flightlist-time"><a href="#"> 
                    @foreach($fd->Segments as $sg)
                        @foreach($sg as $time)
                           <?php 
                                $dDatetime=strtotime($time->StopPointDepartureTime);
                                $dtime = date("H:i",$dDatetime);
                           ?>
                           {{ $dtime }}
                        @endforeach
                    @endforeach
                  </a></h3>
                  <h4 class="flightlist-cities"> 
                    <a href="#">
                         @foreach($fd->Segments as $segment)
                            @foreach($segment as $sg)
                                {{$sg->Origin->Airport->CityName}}
                            @endforeach
                         @endforeach
                       
                    </a>
                  </h4>
                </div>
                <div class="col-md-2 col-xs-2">
                  <h3 class="flightlist-time"> <a href="#">
                   @foreach($fd->Segments as $sg)
                        @foreach($sg as $time)
                            <?php 
                                $aDatetime=strtotime($time->StopPointArrivalTime);
                                $atime = date("H:i",$aDatetime);
                           ?>
                           {{ $atime }}
                        @endforeach
                    @endforeach
                  </a></h3>
                  <h4 class="flightlist-cities">
                      <a href="#">
                         @foreach($fd->Segments as $segment)
                            @foreach($segment as $sg)
                                {{$sg->Destination->Airport->CityName}}
                            @endforeach
                         @endforeach 
                       
                      </a>
                  </h4>
                </div>
                 <div class="col-md-2 col-xs-2">
                  <h3 class="flightlist-time"> <a href="#">
                       <?php $datetimed = new DateTime($time->StopPointDepartureTime);
                            $datetimea = new DateTime($time->StopPointArrivalTime);
                            $interval = $datetimed->diff($datetimea);
                            echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?>
                      
                  </a></h3>
                  <h4 class="flightlist-cities"><a href="#"> 
                  @foreach($fd->Segments as $sg)
                        @foreach($sg as $seg)
                           @if($seg->StopPoint=="")
                            Non Stop
                           @else
                            {{$seg->StopPoint." Stop" }}
                           @endif
                        @endforeach
                    @endforeach
                  </a></h4>
                </div>
                <div class="col-md-2 col-xs-2" style="text-align: right;">
                 <h3 class="flightlist-price"> Fare: <span class="flightlist-person">(per person)</span>  <a href="#"> <i class="fa fa-inr" aria-hidden="true"></i><span>
                     <?php $basefare = $fd->FareBreakdown[0]->BaseFare;
                            $tax = $fd->FareBreakdown[0]->Tax;
                            $adult_count = $fd->FareBreakdown[0]->PassengerCount;
                            $fare_per_adult = ($basefare+$tax)/$adult_count;
                     ?>
                     {{" ".number_format(round((getPriceChange('Flight',$fare_per_adult)),0))}}</span></a></h3>
                </div>
                <div class="col-md-2 col-xs-2">
                     <a class="flightlist-bookbtn" href="{{route('frontend.flight_passenger_details',['TraceId' =>$flights->Response->TraceId ,'ResultIndex'=>$fd->ResultIndex])}}" class="btn btn-danger btn-lg">BOOK NOW</a>
                </div>
                <div class="col-md-12 col-xs-12 hlcol">
<div class="row hlrow">
    <div class="col-sm-12 hlcol">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
       
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{$i}}">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}" aria-expanded="false" aria-controls="collapse{{$i}}">
                            <i class="more-less glyphicon glyphicon-chevron-down"></i>
                           Flights Details
                        </a>
                    </h4>
                </div>
                <div id="collapse{{$i}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12">
                            
                             <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
                                        <li class="active">
                                            <?php $k=$j; ?>
                                            <a href="#tab_default_{{$j++}}" data-toggle="tab">Flights Details </a>
                                        </li>
                                        <li>
                                          <a href="#tab_default_{{$j++}}" data-toggle="tab">
                                          Fare Details </a>
                                        </li>
                                        <li>
                                          <a href="#tab_default_{{$j++}}" data-toggle="tab">
                                          Baggage Details </a>
                                        </li>
                                        <li>
                                          <a href="#tab_default_{{$j++}}" data-toggle="tab">
                                          Cancellation Fee </a>
                                        </li>
                                        <li>
                                          <a href="#tab_default_{{$j++}}" data-toggle="tab">
                                          Date Change Fee </a>
                                        </li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        <!-- Flight details -->
                                         <div class="tab-pane active" id="tab_default_{{$k++}}">
                                                <?php $flight_start_date=$flights->Response->Results[0][0]->Segments[0][0]->StopPointDepartureTime; 
                                                 $start = date("j M Y, D", strtotime($flight_start_date));
                                                 ?>
                                               <div class="row" style="margin-top:20px;margin-bottom: 15px;font-size:20px;padding-left:12px"> 
                                               <span class="flightlist-dddate">{{$start}}</span>
                                            <div class="col-md-2 col-xs-2">
                                              
                                               <?php   $dirname = "assets/home/images/AirlineLogo";
                                                $filename = glob("$dirname/*{$fd->AirlineCode}*", GLOB_BRACE); ?>
                                                @if(isset($filename) && count($filename)>0)
                                                <img src="{{ asset($filename[0])}}" class="img-responsive" style="width: 50%">
                                                @endif
                                             <a class="flight-listname" href="#"> 
                                                <p>
                                                @foreach($fd->Segments as $segment)
                                                    @foreach($segment as $sg)
                                                      {{$sg->Airline->AirlineName}}
                                                    @endforeach
                                                @endforeach
                                                </p>
                                                <span>
                                                    @foreach($fd->Segments as $sg)
                                                            @foreach($sg as $seg)
                                                               {{$seg->Airline->AirlineCode}}-{{$seg->Airline->FlightNumber}}
                                                            @endforeach
                                                        @endforeach
                                                </span>
                                             </a>
                                            </div>
                                            <div class="col-md-2 col-xs-2">
                                              <h3 class="flightlist-time"><a href="#"> 
                                                @foreach($fd->Segments as $sg)
                                                    @foreach($sg as $time)
                                                       <?php 
                                                            $dDatetime=strtotime($time->StopPointDepartureTime);
                                                            $dtime = date("H:i",$dDatetime);
                                                       ?>
                                                       {{ $dtime }}
                                                    @endforeach
                                                @endforeach
                                              </a></h3>
                                              <h4 class="flightlist-cities"> 
                                                <a href="#">
                                                     @foreach($fd->Segments as $segment)
                                                        @foreach($segment as $sg)
                                                            {{$sg->Origin->Airport->CityName}}
                                                        @endforeach
                                                     @endforeach
                                                   
                                                </a>
                                              </h4>
                                            </div>
                                            <div class="col-md-2 col-xs-2">
                                              <h3 class="flightlist-time"> <a href="#">
                                               @foreach($fd->Segments as $sg)
                                                    @foreach($sg as $time)
                                                        <?php 
                                                            $aDatetime=strtotime($time->StopPointArrivalTime);
                                                            $atime = date("H:i",$aDatetime);
                                                       ?>
                                                       {{ $atime }}
                                                    @endforeach
                                                @endforeach
                                              </a></h3>
                                              <h4 class="flightlist-cities">
                                                  <a href="#">
                                                     @foreach($fd->Segments as $segment)
                                                        @foreach($segment as $sg)
                                                            {{$sg->Destination->Airport->CityName}}
                                                        @endforeach
                                                     @endforeach 
                                                   
                                                  </a>
                                              </h4>
                                            </div>
                                             <div class="col-md-2 col-xs-2">
                                              <h3 class="flightlist-time"> <a href="#">
                                                   <?php $datetimed = new DateTime($time->StopPointDepartureTime);
                                                        $datetimea = new DateTime($time->StopPointArrivalTime);
                                                        $interval = $datetimed->diff($datetimea);
                                                        echo $interval->format('%h')."H ".$interval->format('%i')."m"; ?>
                                                  
                                              </a></h3>
                                              <h4 class="flightlist-cities"><a href="#"> 
                                              @foreach($fd->Segments as $sg)
                                                    @foreach($sg as $seg)
                                                       @if($seg->StopPoint=="")
                                                        Non Stop
                                                       @else
                                                        {{$seg->StopPoint." Stop" }}
                                                       @endif
                                                    @endforeach
                                                @endforeach
                                              </a></h4>
                                            </div>
                                            <div class="col-md-2 col-xs-2">
                                                <span class="partially-refundable">Partially Refundable</span> 
                                            <a class="flightlist-class" href="#">
                                                @foreach($cabinClass as $key=>$iv) 
                                                    @if($search_info['Segments'][0]['FlightCabinClass']==$key)
                                                        {{$iv}}
                                                    @endif
                                                @endforeach
                                            </a>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- Flight details end -->
                                       
                                        <div class="tab-pane" id="tab_default_{{$k++}}">
                                          <table class="table table-responsive">
                                              <thead>
                                                  <tr>
                                                  <th colspan="2">Fare Breakup</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>Base Fare (
                                                      {{$fd->FareBreakdown[0]->PassengerCount." Adult"}},
                                                      @if(isset($fd->FareBreakdown[1]->PassengerCount))
                                                      {{$fd->FareBreakdown[1]->PassengerCount." Children"}},
                                                      @endif
                                                      @if(isset($fd->FareBreakdown[2]->PassengerCount))
                                                      {{$fd->FareBreakdown[2]->PassengerCount." Infant"}}
                                                      @endif
                                                      )</td>
                                                      <td><i class="fa fa-inr" aria-hidden="true"></i><span>{{" ".number_format(round(($fd->Fare->BaseFare),0))}} </span></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Fees & Surcharge</td>
                                                      <?php $fee=($fd->Fare->Tax)+($fd->Fare->OtherCharges) ?>
                                                      <td><i class="fa fa-inr" aria-hidden="true"></i><span>{{" ".number_format(round(($fee),0))}} </span></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Total</td>
                                                      <?php $total = $fee+($fd->Fare->BaseFare); ?>
                                                      <td><i class="fa fa-inr" aria-hidden="true"></i><span>{{" ".number_format(round(($total),0))}} </span></td>
                                                  </tr>
                                              </tbody>
                                              <tfoot>
                                                  <tr>
                                                      <td colspan="2">
                                                          Note: <span class="partially-refundable">Partially Refundable</span>
                                                      </td>
                                                  </tr>
                                              </tfoot>
                                          </table>
                                        </div>
                                        <div class="tab-pane" id="tab_default_{{$k++}}">
                                          <table class="table table-responsive">
                                              <thead>
                                                  <tr>
                                                  <th>{{$flights->Response->Origin}} - {{$flights->Response->Destination}}</th>
                                                  <th> Cabin </th>
                                                  <th> Checkin </th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>Adult</td>
                                                      <td><span>
                                                        @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                @if(isset($sg->CabinBaggage)&& $sg->CabinBaggage!="")
                                                                {{$sg->CabinBaggage}}
                                                                @else
                                                                    {{"-"}}
                                                                @endif
                                                            
                                                            @endforeach
                                                        @endforeach 
                                                     </span></td>
                                                      <td>
                                                          <span>
                                                          @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                @if(isset($sg->Baggage)&& $sg->Baggage!="")
                                                                {{$sg->Baggage}}
                                                                @else
                                                                    {{"-"}}
                                                                @endif
                                                            @endforeach
                                                        @endforeach 
                                                        </span>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Child</td>
                                                     <td><span>
                                                         @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                @if(isset($sg->CabinBaggage)&& $sg->CabinBaggage!="")
                                                                {{$sg->CabinBaggage}}
                                                                @else
                                                                    {{"-"}}
                                                                @endif
                                                            
                                                            @endforeach
                                                        @endforeach 
                                                     </span></td>
                                                      <td>
                                                          <span>
                                                          @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                @if(isset($sg->Baggage)&& $sg->Baggage!="")
                                                                {{$sg->Baggage}}
                                                                @else
                                                                    {{"-"}}
                                                                @endif
                                                            @endforeach
                                                        @endforeach 
                                                        </span>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Infant</td>
                                                     
                                                      <td><span>0 Kgs</span></td>
                                                      <td><span>0 Kgs</span></td>
                                                  </tr>
                                              </tbody>
                                              
                                          </table>
                                        </div>
                                            <div class="tab-pane" id="tab_default_{{$k++}}">
                                          <table class="table table-responsive">
                                              <thead>
                                                  <tr>
                                                  <th>{{$flights->Response->Origin}} - {{$flights->Response->Destination}}</th>
                                                  <th> per passenger fee </th>
                                                  
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>24 hrs-365 days</td>
                                                      <td><span>
                                                        @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                {{$sg->CabinBaggage}}
                                                            @endforeach
                                                        @endforeach 
                                                     </span></td>
                                                      
                                                  </tr>
                                                  <tr>
                                                      <td>0-24 hrs</td>
                                                     <td><span>
                                                        @foreach($fd->Segments as $segment)
                                                            @foreach($segment as $sg)
                                                                {{$sg->CabinBaggage}}
                                                            @endforeach
                                                        @endforeach 
                                                     </span></td>
                                                     
                                                  </tr>
                                                
                                              </tbody>
                                              
                                          </table>
                                        </div>
                                            <div class="tab-pane" id="tab_default_{{$k++}}">
                                          <table class="table table-responsive">
                                              <thead>
                                                  <tr>
                                                  <th>{{$flights->Response->Origin}} - {{$flights->Response->Destination}}</th>
                                                  <th> per passenger fee </th>
                                                 
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <tr>
                                                      <td>24 hrs-365 days</td>
                                                      <td><span>
                                                        <i class="fa fa-inr" aria-hidden="true"></i><span>{{" ".number_format(round(($fd->Fare->BaseFare),0))}} + 300*</span>
                                                     </span></td>
                                                      
                                                  </tr>
                                                  <tr>
                                                      <td>0-24 hrs</td>
                                                     <td><span>
                                                       Non-Changeable
                                                     </span></td>
                                                      
                                                  </tr>
                                                 
                                              </tbody>
                                              
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div><!-- panel-group -->
    </div>
</div>
              </div>
            </div>

             </div>
             <?php $i++; ?>
@endforeach

           
             
          </div>
        </div>
      </div>
        </div>
  
      </div>
    </div>
  </div>
  </div>
</div>
@include('frontend.includes.footer')
<script>
    $("#total_flights").html("<?php echo --$i; ?> Flights Found");
</script>
<script type='text/javascript'>

$(function() {

 // simple example
 $('#input1').immybox({
   choices: [
	 {text: 'Alabama', value: 'AL'},
	 {text: 'Alaska', value: 'AK'},
	 {text: 'Arizona', value: 'AZ'},
	 {text: 'Arkansas', value: 'AR'},
	 {text: 'California', value: 'CA'},
	 {text: 'Colorado', value: 'CO'},
	 {text: 'Connecticut', value: 'CT'},
	 {text: 'Delaware', value: 'DE'},
	 {text: 'Florida', value: 'FL'},
	 {text: 'Georgia', value: 'GA'},
	 {text: 'Hawaii', value: 'HI'},
	 {text: 'Idaho', value: 'ID'},
	 {text: 'Illinois', value: 'IL'},
	 {text: 'Indiana', value: 'IN'},
	 {text: 'Iowa', value: 'IA'},
	 {text: 'Kansas', value: 'KS'},
	 {text: 'Kentucky', value: 'KY'},
	 {text: 'Louisiana', value: 'LA'},
	 {text: 'Maine', value: 'ME'},
	 {text: 'Maryland', value: 'MD'},
	 {text: 'Massachusetts', value: 'MA'},
	 {text: 'Michigan', value: 'MI'},
	 {text: 'Minnesota', value: 'MN'},
	 {text: 'Mississippi', value: 'MS'},
	 {text: 'Missouri', value: 'MO'},
	 {text: 'Montana', value: 'MT'},
	 {text: 'Nebraska', value: 'NE'},
	 {text: 'Nevada', value: 'NV'},
	 {text: 'New Hampshire', value: 'NH'},
	 {text: 'New Jersey', value: 'NJ'},
	 {text: 'New Mexico', value: 'NM'},
	 {text: 'New York', value: 'NY'},
	 {text: 'North Carolina', value: 'NC'},
	 {text: 'North Dakota', value: 'ND'},
	 {text: 'Ohio', value: 'OH'},
	 {text: 'Oklahoma', value: 'OK'},
	 {text: 'Oregon', value: 'OR'},
	 {text: 'Pennsylvania', value: 'PA'},
	 {text: 'Rhode Island', value: 'RI'},
	 {text: 'South Carolina', value: 'SC'},
	 {text: 'South Dakota', value: 'SD'},
	 {text: 'Tennessee', value: 'TN'},
	 {text: 'Texas', value: 'TX'},
	 {text: 'Utah', value: 'UT'},
	 {text: 'Vermont', value: 'VT'},
	 {text: 'Virginia', value: 'VA'},
	 {text: 'Washington', value: 'WA'},
	 {text: 'West Virginia', value: 'WV'},
	 {text: 'Wisconsin', value: 'WI'},
	 {text: 'Wyoming', value: 'WY'}
   ],
   defaultSelectedValue: 'LA'
 });

});
</script>
<script type='text/javascript'>                          
      $(function(){

        // simple example
        $('#input_hotel').immybox({
          choices: [
              				{text: "Agra,AGR", value: 'AGR'},
								{text: "Agatti Island,AGX", value: 'AGX'},
								{text: "Aizawl,AJL", value: 'AJL'},
								{text: "Akola,AKD", value: 'AKD'},
								{text: "Ahmedabad,AMD", value: 'AMD'},
								{text: "Amritsar,ATQ", value: 'ATQ'},
								{text: "Bhubaneshwar,BBI", value: 'BBI'},
								{text: "Vadodara,BDQ", value: 'BDQ'},
								{text: "Bellary,BEP", value: 'BEP'},
								{text: "Bhuj,BHJ", value: 'BHJ'},
								{text: "Bhopal,BHO", value: 'BHO'},
								{text: "Bhavnagar,BHU", value: 'BHU'},
								{text: "Bangkok,BKK", value: 'BKK'},
								{text: "Bangkok,DMK", value: 'DMK'},
								{text: "Bangalore,BLR", value: 'BLR'},
								{text: "Mumbai,BOM", value: 'BOM'},
								{text: "Car Nicobar,CBD", value: 'CBD'},
								{text: "Calicut,CCJ", value: 'CCJ'},
								{text: "Kolkata,CCU", value: 'CCU'},
								{text: "Cuddapah,CDP", value: 'CDP'},
								{text: "Chiang Rai,CEI", value: 'CEI'},
								{text: "Coimbatore,CJB", value: 'CJB'},
								{text: "Chiang Mai,CNX", value: 'CNX'},
								{text: "Cooch Behar,COH", value: 'COH'},
								{text: "Kochi,COK", value: 'COK'},
								{text: "Dhanbad,DBD", value: 'DBD'},
								{text: "Dehra Dun,DED", value: 'DED'},
								{text: "Delhi,DEL", value: 'DEL'},
								{text: "Dharamsala,DHM", value: 'DHM'},
								{text: "Dibrugarh,DIB", value: 'DIB'},
								{text: "Diu,DIU", value: 'DIU'},
								{text: "Dimapur,DMU", value: 'DMU'},
								{text: "Guwahati,GAU", value: 'GAU'},
								{text: "Gaya,GAY", value: 'GAY'},
								{text: "Goa,GOI", value: 'GOI'},
								{text: "Gorakhpur,GOP", value: 'GOP'},
								{text: "Gwalior,GWL", value: 'GWL'},
								{text: "Hubli,HBX", value: 'HBX'},
								{text: "Hat Yai,HDY", value: 'HDY'},
								{text: "Hua Hin,HHQ", value: 'HHQ'},
								{text: "Khajuraho,HJR", value: 'HJR'},
								{text: "Phuket,HKT", value: 'HKT'},
								{text: "Hyderabad,HYD", value: 'HYD'},
								{text: "Indore,IDR", value: 'IDR'},
								{text: "Imphal,IMF", value: 'IMF'},
								{text: "Nasik,ISK", value: 'ISK'},
								{text: "Agartala,IXA", value: 'IXA'},
								{text: "Bagdogra,IXB", value: 'IXB'},
								{text: "Chandigarh,IXC", value: 'IXC'},
								{text: "Allahabad,IXD", value: 'IXD'},
								{text: "Mangalore,IXE", value: 'IXE'},
								{text: "Belgaum,IXG", value: 'IXG'},
								{text: "Kailashahar,IXH", value: 'IXH'},
								{text: "Lilabari,IXI", value: 'IXI'},
								{text: "Jammu,IXJ", value: 'IXJ'},
								{text: "Keshod,IXK", value: 'IXK'},
								{text: "Leh,IXL", value: 'IXL'},
								{text: "Madurai,IXM", value: 'IXM'},
								{text: "Pathankot,IXP", value: 'IXP'},
								{text: "Ranchi,IXR", value: 'IXR'},
								{text: "Silchar,IXS", value: 'IXS'},
								{text: "Aurangabad,IXU", value: 'IXU'},
								{text: "Jamshedpur,IXW", value: 'IXW'},
								{text: "Kandla,IXY", value: 'IXY'},
								{text: "Port Blair,IXZ", value: 'IXZ'},
								{text: "Jaipur,JAI", value: 'JAI'},
								{text: "Jodhpur,JDH", value: 'JDH'},
								{text: "Jamnagar,JGA", value: 'JGA'},
								{text: "Jagdalpur,JGB", value: 'JGB'},
								{text: "Jabalpur,JLR", value: 'JLR'},
								{text: "Jorhat,JRH", value: 'JRH'},
								{text: "Jaisalmer,JSA", value: 'JSA'},
								{text: "Krabi,KBV", value: 'KBV'},
								{text: "Khon Kaen,KKC", value: 'KKC'},
								{text: "Kolhapur,KLH", value: 'KLH'},
								{text: "Kanpur,KNU", value: 'KNU'},
								{text: "Kota,KTU", value: 'KTU'},
								{text: "Kulu,KUU", value: 'KUU'},
								{text: "Malda,LDA", value: 'LDA'},
								{text: "Lucknow,LKO", value: 'LKO'},
								{text: "Loei,LOE", value: 'LOE'},
								{text: "Lop Buri,LOP", value: 'LOP'},
								{text: "Lampang,LPT", value: 'LPT'},
								{text: "latur,ltu", value: 'ltu'},
								{text: "Ludhiana,LUH", value: 'LUH'},
								{text: "Chennai,MAA", value: 'MAA'},
								{text: "Mae Sot,MAQ", value: 'MAQ'},
								{text: "Mysore,MYQ", value: 'MYQ'},
								{text: "Nagpur,NAG", value: 'NAG'},
								{text: "Nakhon Ratchasima,NAK", value: 'NAK'},
								{text: "Narathiwat,NAW", value: 'NAW'},
								{text: "Nanded,NDC", value: 'NDC'},
								{text: "Daman,NMB", value: 'NMB'},
								{text: "Nakon Si Thammarat,NST", value: 'NST'},
								{text: "Neyveli,NVY", value: 'NVY'},
								{text: "Bilaspur,PAB", value: 'PAB'},
								{text: "Pattani,PAN", value: 'PAN'},
								{text: "Patna,PAT", value: 'PAT'},
								{text: "Porbandar,PBD", value: 'PBD'},
								{text: "Pantnagar,PGH", value: 'PGH'},
								{text: "Phitsanulok,PHS", value: 'PHS'},
								{text: "Pune,PNQ", value: 'PNQ'},
								{text: "Pondicherry,PNY", value: 'PNY'},
								{text: "Phrae,PRH", value: 'PRH'},
								{text: "Pattaya,PYX", value: 'PYX'},
								{text: "Rajkot,RAJ", value: 'RAJ'},
								{text: "Durgapur,RDP", value: 'RDP'},
								{text: "Rewa,REW", value: 'REW'},
								{text: "Rajahmundry,RJA", value: 'RJA'},
								{text: "Rajouri,RJI", value: 'RJI'},
								{text: "SURFACE,RNK", value: 'RNK'},
								{text: "Raipur,RPR", value: 'RPR'},
								{text: "Rourkela,RRK", value: 'RRK'},
								{text: "Shillong,SHL", value: 'SHL'},
								{text: "Simla,SLV", value: 'SLV'},
								{text: "Sakon Nakhon,SNO", value: 'SNO'},
								{text: "Sholapur,SSE", value: 'SSE'},
								{text: "Surat,STV", value: 'STV'},
								{text: "Srinagar,SXR", value: 'SXR'},
								{text: "Salem,SXV", value: 'SXV'},
								{text: "Tuticorin,TCR", value: 'TCR'},
								{text: "Tezu,TEI", value: 'TEI'},
								{text: "Tezpur,TEZ", value: 'TEZ'},
								{text: "Sukhothai,THS", value: 'THS'},
								{text: "Tirupati,TIR", value: 'TIR'},
								{text: "Satna,TNI", value: 'TNI'},
								{text: "Trivandrum,TRV", value: 'TRV'},
								{text: "Tiruchirapally,TRZ", value: 'TRZ'},
								{text: "Trang,TST", value: 'TST'},
								{text: "Ubon Ratchathani,UBP", value: 'UBP'},
								{text: "Udaipur,UDR", value: 'UDR'},
								{text: "Surat Thani,URT", value: 'URT'},
								{text: "Koh Samui,USM", value: 'USM'},
								{text: "Udon Thani,UTH", value: 'UTH'},
								{text: "Utapao,UTP", value: 'UTP'},
								{text: "Vijayawada,VGA", value: 'VGA'},
								{text: "Varanasi,VNS", value: 'VNS'},
								{text: "Vishakhapatnam,VTZ", value: 'VTZ'},
				          ],
          defaultSelectedValue: 'LA'
        });

	

	  });
	  $(function(){
		$('.input3').immybox({
          choices: [
              				{text: "Agra,AGR", value: 'AGR'},
								{text: "Agatti Island,AGX", value: 'AGX'},
								{text: "Aizawl,AJL", value: 'AJL'},
								{text: "Akola,AKD", value: 'AKD'},
								{text: "Ahmedabad,AMD", value: 'AMD'},
								{text: "Amritsar,ATQ", value: 'ATQ'},
								{text: "Bhubaneshwar,BBI", value: 'BBI'},
								{text: "Vadodara,BDQ", value: 'BDQ'},
								{text: "Bellary,BEP", value: 'BEP'},
								{text: "Bhuj,BHJ", value: 'BHJ'},
								{text: "Bhopal,BHO", value: 'BHO'},
								{text: "Bhavnagar,BHU", value: 'BHU'},
								{text: "Bangkok,BKK", value: 'BKK'},
								{text: "Bangkok,DMK", value: 'DMK'},
								{text: "Bangalore,BLR", value: 'BLR'},
								{text: "Mumbai,BOM", value: 'BOM'},
								{text: "Car Nicobar,CBD", value: 'CBD'},
								{text: "Calicut,CCJ", value: 'CCJ'},
								{text: "Kolkata,CCU", value: 'CCU'},
								{text: "Cuddapah,CDP", value: 'CDP'},
								{text: "Chiang Rai,CEI", value: 'CEI'},
								{text: "Coimbatore,CJB", value: 'CJB'},
								{text: "Chiang Mai,CNX", value: 'CNX'},
								{text: "Cooch Behar,COH", value: 'COH'},
								{text: "Kochi,COK", value: 'COK'},
								{text: "Dhanbad,DBD", value: 'DBD'},
								{text: "Dehra Dun,DED", value: 'DED'},
								{text: "Delhi,DEL", value: 'DEL'},
								{text: "Dharamsala,DHM", value: 'DHM'},
								{text: "Dibrugarh,DIB", value: 'DIB'},
								{text: "Diu,DIU", value: 'DIU'},
								{text: "Dimapur,DMU", value: 'DMU'},
								{text: "Guwahati,GAU", value: 'GAU'},
								{text: "Gaya,GAY", value: 'GAY'},
								{text: "Goa,GOI", value: 'GOI'},
								{text: "Gorakhpur,GOP", value: 'GOP'},
								{text: "Gwalior,GWL", value: 'GWL'},
								{text: "Hubli,HBX", value: 'HBX'},
								{text: "Hat Yai,HDY", value: 'HDY'},
								{text: "Hua Hin,HHQ", value: 'HHQ'},
								{text: "Khajuraho,HJR", value: 'HJR'},
								{text: "Phuket,HKT", value: 'HKT'},
								{text: "Hyderabad,HYD", value: 'HYD'},
								{text: "Indore,IDR", value: 'IDR'},
								{text: "Imphal,IMF", value: 'IMF'},
								{text: "Nasik,ISK", value: 'ISK'},
								{text: "Agartala,IXA", value: 'IXA'},
								{text: "Bagdogra,IXB", value: 'IXB'},
								{text: "Chandigarh,IXC", value: 'IXC'},
								{text: "Allahabad,IXD", value: 'IXD'},
								{text: "Mangalore,IXE", value: 'IXE'},
								{text: "Belgaum,IXG", value: 'IXG'},
								{text: "Kailashahar,IXH", value: 'IXH'},
								{text: "Lilabari,IXI", value: 'IXI'},
								{text: "Jammu,IXJ", value: 'IXJ'},
								{text: "Keshod,IXK", value: 'IXK'},
								{text: "Leh,IXL", value: 'IXL'},
								{text: "Madurai,IXM", value: 'IXM'},
								{text: "Pathankot,IXP", value: 'IXP'},
								{text: "Ranchi,IXR", value: 'IXR'},
								{text: "Silchar,IXS", value: 'IXS'},
								{text: "Aurangabad,IXU", value: 'IXU'},
								{text: "Jamshedpur,IXW", value: 'IXW'},
								{text: "Kandla,IXY", value: 'IXY'},
								{text: "Port Blair,IXZ", value: 'IXZ'},
								{text: "Jaipur,JAI", value: 'JAI'},
								{text: "Jodhpur,JDH", value: 'JDH'},
								{text: "Jamnagar,JGA", value: 'JGA'},
								{text: "Jagdalpur,JGB", value: 'JGB'},
								{text: "Jabalpur,JLR", value: 'JLR'},
								{text: "Jorhat,JRH", value: 'JRH'},
								{text: "Jaisalmer,JSA", value: 'JSA'},
								{text: "Krabi,KBV", value: 'KBV'},
								{text: "Khon Kaen,KKC", value: 'KKC'},
								{text: "Kolhapur,KLH", value: 'KLH'},
								{text: "Kanpur,KNU", value: 'KNU'},
								{text: "Kota,KTU", value: 'KTU'},
								{text: "Kulu,KUU", value: 'KUU'},
								{text: "Malda,LDA", value: 'LDA'},
								{text: "Lucknow,LKO", value: 'LKO'},
								{text: "Loei,LOE", value: 'LOE'},
								{text: "Lop Buri,LOP", value: 'LOP'},
								{text: "Lampang,LPT", value: 'LPT'},
								{text: "latur,ltu", value: 'ltu'},
								{text: "Ludhiana,LUH", value: 'LUH'},
								{text: "Chennai,MAA", value: 'MAA'},
								{text: "Mae Sot,MAQ", value: 'MAQ'},
								{text: "Mysore,MYQ", value: 'MYQ'},
								{text: "Nagpur,NAG", value: 'NAG'},
								{text: "Nakhon Ratchasima,NAK", value: 'NAK'},
								{text: "Narathiwat,NAW", value: 'NAW'},
								{text: "Nanded,NDC", value: 'NDC'},
								{text: "Daman,NMB", value: 'NMB'},
								{text: "Nakon Si Thammarat,NST", value: 'NST'},
								{text: "Neyveli,NVY", value: 'NVY'},
								{text: "Bilaspur,PAB", value: 'PAB'},
								{text: "Pattani,PAN", value: 'PAN'},
								{text: "Patna,PAT", value: 'PAT'},
								{text: "Porbandar,PBD", value: 'PBD'},
								{text: "Pantnagar,PGH", value: 'PGH'},
								{text: "Phitsanulok,PHS", value: 'PHS'},
								{text: "Pune,PNQ", value: 'PNQ'},
								{text: "Pondicherry,PNY", value: 'PNY'},
								{text: "Phrae,PRH", value: 'PRH'},
								{text: "Pattaya,PYX", value: 'PYX'},
								{text: "Rajkot,RAJ", value: 'RAJ'},
								{text: "Durgapur,RDP", value: 'RDP'},
								{text: "Rewa,REW", value: 'REW'},
								{text: "Rajahmundry,RJA", value: 'RJA'},
								{text: "Rajouri,RJI", value: 'RJI'},
								{text: "SURFACE,RNK", value: 'RNK'},
								{text: "Raipur,RPR", value: 'RPR'},
								{text: "Rourkela,RRK", value: 'RRK'},
								{text: "Shillong,SHL", value: 'SHL'},
								{text: "Simla,SLV", value: 'SLV'},
								{text: "Sakon Nakhon,SNO", value: 'SNO'},
								{text: "Sholapur,SSE", value: 'SSE'},
								{text: "Surat,STV", value: 'STV'},
								{text: "Srinagar,SXR", value: 'SXR'},
								{text: "Salem,SXV", value: 'SXV'},
								{text: "Tuticorin,TCR", value: 'TCR'},
								{text: "Tezu,TEI", value: 'TEI'},
								{text: "Tezpur,TEZ", value: 'TEZ'},
								{text: "Sukhothai,THS", value: 'THS'},
								{text: "Tirupati,TIR", value: 'TIR'},
								{text: "Satna,TNI", value: 'TNI'},
								{text: "Trivandrum,TRV", value: 'TRV'},
								{text: "Tiruchirapally,TRZ", value: 'TRZ'},
								{text: "Trang,TST", value: 'TST'},
								{text: "Ubon Ratchathani,UBP", value: 'UBP'},
								{text: "Udaipur,UDR", value: 'UDR'},
								{text: "Surat Thani,URT", value: 'URT'},
								{text: "Koh Samui,USM", value: 'USM'},
								{text: "Udon Thani,UTH", value: 'UTH'},
								{text: "Utapao,UTP", value: 'UTP'},
								{text: "Vijayawada,VGA", value: 'VGA'},
								{text: "Varanasi,VNS", value: 'VNS'},
								{text: "Vishakhapatnam,VTZ", value: 'VTZ'},
				          ],
          defaultSelectedValue: 'LA'
        });  
	  });
    </script>
	
<!-- //Calendar --> 
<!-- Page Scripts Ends --> 
<script type="text/javascript">
    //      $(function () { 
    // var $popover = $('.popover-markup>.trigger').popover({
    //   html: true,
    //   placement: 'bottom',
    //   content: function () {
          
    //     return $(this).parent().find('.content').html();
    //   }
    // });

    // open popover & inital value in form
    // var passengers = [1,0,0];
    // $('.popover-markup>.trigger').click(function (e) {
    //     e.stopPropagation();
    //     $(".popover-content input").each(function(i) {
    //         $(this).val(passengers[i]);
    //     });
    // });
    // close popover
    // $(document).click(function (e) {
    //     if ($(e.target).is('.demise')) {        
    //         $('.popover-markup>.trigger').popover('hide');
    //     }
    // });
// store form value when popover closed
//  $popover.on('hide.bs.popover', function () {
//     $(".popover-content input").each(function(i) {
//         passengers[i] = $(this).val();
//     });
//   });
    // spinner(+-btn to change value) & total to parent input 
//     $(document).on('click', '.number-spinner a', function () {
//         var btn = $(this),
//         input = btn.closest('.number-spinner').find('input'),
//         total = $('#passengers').val(),
//         oldValue = input.val().trim();
        
//     if(btn.attr('data-dir') == 'up' && btn.attr('id')=='adult_up') {
//       if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#adult').val(total);
        
      
//       }
//     }else if(btn.attr('data-dir') == 'up' && btn.attr('id')=='child_up')
//     {
//         if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#child_one').val(total);
        
      
//       }
//     } else if(btn.attr('data-dir') == 'up' && btn.attr('id')=='inf_up')
//     {
//         if(oldValue < input.attr('max')){
//         oldValue++;
//         total++;
//         $('#infants_one').val(total);
      
//       }
//     }
    
//     else {
//       if (oldValue > input.attr('min')) {
//         oldValue--;
//         total--;
      
//       }
//     }
//     $('#passengers').val(total);
    
//     input.val(oldValue);
//   });

//   $(".popover-markup>.trigger").popover('show');
// });
</script>


<!-- modal--> 
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 



<!-- modal--> 
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 
<script type="text/javascript">$(document).ready( function() {
    $('#myCarousel').carousel({
        interval:   4000
    });
    
    var clickEvent = false;
    $('#myCarousel').on('click', '.nav a', function() {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(this).parent().addClass('active');        
    }).on('slid.bs.carousel', function(e) {
        if(!clickEvent) {
            var count = $('.nav').children().length -1;
            var current = $('.nav li.active');
            current.removeClass('active').next().addClass('active');
            var id = parseInt(current.data('slide-to'));
            if(count == id) {
                $('.nav li').first().addClass('active');    
            }
        }
        clickEvent = false;
    });
});</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#bar-btn").click(function(){
            $(".menu").toggle();
        });

        $("#my-popup").click(function(){
            $("#modal-popup").fadeIn();
        });
        
        $("#modal-popup").click(function(){
            $("#modal-popup").fadeOut();
        });
    });
</script> 
<!--========= modal=======-->
<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-chevron-up glyphicon-chevron-down');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<script>
$(document).ready(function() {
$(".greenOutlineBtn").click(function () {
    $(".greenOutlineBtn").removeClass("active");
    $(this).addClass("active");   
});
});
</script>
                             
<script>
        window.oncroll = function() {myFunction()};
            
            function myFunction() {
                alert("hello");
                
                if (window.pageYOffset > 50||document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    document.getElementById("sidebar-hotel").className = "test";
                } else {
                    document.getElementById("sidebar-hotel").className = "";
                }
            }
</script>                    <script>
                    $(document).ready(function(){
                        $('#multi_city').find(':input').prop('disabled', true);
                    });
                    
                    </script>
<script>

$(document).ready(function(){

    
     
      $("#input3").on("change",function(){
         // alert("hello");
            var source = $("#input2").val();
            var destination = $("#input3").val();
           // alert(source+" "+destination);
           if(source=="" && destination==""){
                $("#input2").focus();
            }
             else if(source==destination)
            {
                  alert("Source and Destination can not be same.");
                    $('#input3 option[value=""]').prop('selected', true);
                    $("#select2-input3-container").text("Destination");
                  $("#input3").focus();
            }
           
            
      });
});
// $(function() {
//       $( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
//       });
$(document).ready(function() {
$(".flight-class").click(function () {
    
    $(".flight-class").removeClass("active");
    // $(".tab").addClass("active"); // instead of this do the below
    $('#travel_class').val($(this).val());
    $(this).addClass("active");
    // alert($(this).val());
    var passengers_count = $("#passenger_count").val();
     //alert(passengers_count);
    var t_class = travelClassInfo();
    
    $("#passengers_one").val(passengers_count+"|"+t_class);
    
    
});
});
</script> 
<script type="text/javascript">
  
    var dates = $("#datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {
    $("#datepicker1").datepicker('option', 'minDate', date);
  }
});
$("#datepicker1").datepicker({ numberOfMonths: 2,changeMonth: true,});
</script>

<script>
    var dates = $(".datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {
        $(".datepicker").closest('div.body').next().find('div.datepicker').datepicker('option', 'minDate', date);
  }
});
$(".datepicker").closest('div.body').next().find('div.datepicker').datepicker({ numberOfMonths: 2,changeMonth: true,});
</script>





<script>
 function travelClassInfo(){
         var travel_class= $("#travel_class").val();
        if(travel_class==""||travel_class==null)
        { travel_class="Economy"; }
        else if(travel_class==2){
            travel_class="Economy";
        }
        else if(travel_class==3){
            travel_class="Premium Economy";
        }
        else if(travel_class==4){
            travel_class="Business";
        }
        else{
            travel_class = "Economy";
        }
        return travel_class;
    }

$(document).ready(function(){
   
//increment the value of passenger
    $("#adult_up").on("click",function(){
        var ad_count = $("#adult_one").val();
        ad_count++;
        $("#adult_one").val(ad_count);
       
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
     $("#child_up").on("click",function(){
        
        var ch_count = $("#child_one").val();
        ch_count++;
        $("#child_one").val(ch_count);
        
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
    $("#inf_up").on("click",function(){
        var inf_count = $("#infants_one").val();
        inf_count++;
        $("#infants_one").val(inf_count);
        
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
//decrement the value of passenger
     $("#adult_dn").on("click",function(){
        var ad_count = $("#adult_one").val();
        ad_fside-counts count--;
        
        if(ad_count>=1){
        $("#adult_one").val(ad_count);
         var passengers_count = $("#passenger_count").val();
        passengers_fside-counts count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
       // alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
     $("#child_dn").on("click",function(){
        var ch_count = $("#child_one").val();
        ch_fside-counts count--;
        
       if(ch_count>=0){
        $("#child_one").val(ch_count);
         var passengers_count = $("#passenger_count").val();
        passengers_fside-counts count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
        //alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
    $("#inf_dn").on("click",function(){
        var inf_count = $("#infants_one").val();
        inf_fside-counts count--;
        
        if(inf_count>=0){
        $("#infants_one").val(inf_count);
         var passengers_count = $("#passenger_count").val();
        passengers_fside-counts count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
        //alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
});
</script>

<script type="text/javascript">
    $('#passengers_one').on('click',function(){
       $('#contactForm').show(); 
    });
</script>
<script type="text/javascript">
    $('#done').on('click',function(){
       $('#contactForm').hide(); 
    });
</script>
<script>
  $("#datepicker1").prop('disabled',true);
  $("#multi-trip-hide").show();
   $('.multi-trip-show').hide(); 
    $("a[href='#one_way']").on("click",function(){
         $("#multi-trip-hide").show();
          $('.multi-trip-show').hide(); 
          $('.multi-city-extra').hide();
      $("#datepicker1").prop('disabled',true);   
    });
</script>
<script>
    $("a[href='#round_trip']").on("click",function(){
         $("#multi-trip-hide").show();
          $('.multi-trip-show').hide(); 
          $('.multi-city-extra').hide();
      $("#datepicker1").prop('disabled',false);   
    });
</script>
<script type="text/javascript">
    $("a[href='#multi_trip']").on('click',function(){
        $("#multi-trip-hide").hide();
       $('.multi-trip-show').show(); 
       $('.multi-city-extra').show();
       $('#multi_city').find(':input').prop('disabled', false);
    });
</script>
<script>
    $('.select2').select2()
.on("select2:open", function () {
    $('.select2-results__options').niceScroll({
       cursorcolor: "#5fdfe8",
        cursorwidth: "8px",
        autohidemode: false,
        cursorborder: "2px solid #5fdfe8",
        horizrailenabled: false,
    });
});
</script>
<script>                                       
// $('#add-city').click(function() {
//     alert("hello");
//     $('.multi-trip-show:last').before('<div class="row" class="multi-trip-show" style="display:none"><div class="col-xs-12 col-md-2"><h3>From</h3><input id="input2" type="text" name="from" id "single" class="form-control"></div><div class="col-xs-12 col-md-2"><h3>To</h3><input id="input3" type="text" name="to"  class="form-control"></div><div class="col-xs-12 col-md-2"><div class="date"><div class="depart"><h3>Depart</h3><input  id="datepicker" name="depart_date" type="text" value="mm/dd/yyyy" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = "mm/dd/yyyy";}" required></div></div></div><span class="remove">Remove Option</span></div>');
// });
// // $('.multi-trip-show').on('click','.remove',function() {
// //  	$(this).parent().remove();
// // });
</script>                             

<script>
var count=0
var row='<div class="row addition-multi-city"><div class="col-xs-12 col-md-4"><h3>From</h3><select class="select2 input-select"  name="from[]" required="required"><option value="">Source</option>  <option value="AGR,IN">Agra,AGR</option> <option value="AGX,IN">Agatti Island,AGX</option> <option value="AJL,IN">Aizawl,AJL</option> <option value="AKD,IN">Akola,AKD</option> <option value="AMD,IN">Ahmedabad,AMD</option> <option value="ATQ,IN">Amritsar,ATQ</option> <option value="BBI,IN">Bhubaneshwar,BBI</option> <option value="BDQ,IN">Vadodara,BDQ</option> <option value="BEP,IN">Bellary,BEP</option> <option value="BHJ,IN">Bhuj,BHJ</option> <option value="BHO,IN">Bhopal,BHO</option> <option value="BHU,IN">Bhavnagar,BHU</option> <option value="BKK,TH">Bangkok,BKK</option> <option value="BKK,TH">Bangkok,BKK</option> <option value="BLR,IN">Bangalore,BLR</option> <option value="BOM,IN">Mumbai,BOM</option> <option value="CBD,IN">Car Nicobar,CBD</option> <option value="CCJ,IN">Calicut,CCJ</option> <option value="CCU,IN">Kolkata,CCU</option> <option value="CDP,IN">Cuddapah,CDP</option> <option value="CEI,TH">Chiang Rai,CEI</option> <option value="CJB,IN">Coimbatore,CJB</option> <option value="CNX,TH">Chiang Mai,CNX</option> <option value="COH,IN">Cooch Behar,COH</option> <option value="COK,IN">Kochi,COK</option> <option value="DBD,IN">Dhanbad,DBD</option> <option value="DED,IN">Dehra Dun,DED</option> <option value="DEL,IN">Delhi,DEL</option> <option value="DHM,IN">Dharamsala,DHM</option> <option value="DIB,IN">Dibrugarh,DIB</option> <option value="DIU,IN">Diu,DIU</option> <option value="DMU,IN">Dimapur,DMU</option> <option value="GAU,IN">Guwahati,GAU</option> <option value="GAY,IN">Gaya,GAY</option> <option value="GOI,IN">Goa,GOI</option> <option value="GOP,IN">Gorakhpur,GOP</option> <option value="GWL,IN">Gwalior,GWL</option> <option value="HBX,IN">Hubli,HBX</option> <option value="HDY,TH">Hat Yai,HDY</option> <option value="HHQ,TH">Hua Hin,HHQ</option> <option value="HJR,IN">Khajuraho,HJR</option> <option value="HKT,TH">Phuket,HKT</option> <option value="HYD,IN">Hyderabad,HYD</option> <option value="IDR,IN">Indore,IDR</option> <option value="IMF,IN">Imphal,IMF</option> <option value="ISK,IN">Nasik,ISK</option> <option value="IXA,IN">Agartala,IXA</option> <option value="IXB,IN">Bagdogra,IXB</option> <option value="IXC,IN">Chandigarh,IXC</option> <option value="IXD,IN">Allahabad,IXD</option> <option value="IXE,IN">Mangalore,IXE</option> <option value="IXG,IN">Belgaum,IXG</option> <option value="IXH,IN">Kailashahar,IXH</option> <option value="IXI,IN">Lilabari,IXI</option> <option value="IXJ,IN">Jammu,IXJ</option> <option value="IXK,IN">Keshod,IXK</option> <option value="IXL,IN">Leh,IXL</option> <option value="IXM,IN">Madurai,IXM</option> <option value="IXP,IN">Pathankot,IXP</option> <option value="IXR,IN">Ranchi,IXR</option> <option value="IXS,IN">Silchar,IXS</option> <option value="IXU,IN">Aurangabad,IXU</option> <option value="IXW,IN">Jamshedpur,IXW</option> <option value="IXY,IN">Kandla,IXY</option> <option value="IXZ,IN">Port Blair,IXZ</option> <option value="JAI,IN">Jaipur,JAI</option> <option value="JDH,IN">Jodhpur,JDH</option> <option value="JGA,IN">Jamnagar,JGA</option> <option value="JGB,IN">Jagdalpur,JGB</option> <option value="JLR,IN">Jabalpur,JLR</option> <option value="JRH,IN">Jorhat,JRH</option> <option value="JSA,IN">Jaisalmer,JSA</option> <option value="KBV,TH">Krabi,KBV</option> <option value="KKC,TH">Khon Kaen,KKC</option> <option value="KLH,IN">Kolhapur,KLH</option> <option value="KNU,IN">Kanpur,KNU</option> <option value="KTU,IN">Kota,KTU</option> <option value="KUU,IN">Kulu,KUU</option> <option value="LDA,IN">Malda,LDA</option> <option value="LKO,IN">Lucknow,LKO</option> <option value="LOE,TH">Loei,LOE</option> <option value="LOP,TH">Lop Buri,LOP</option> <option value="LPT,TH">Lampang,LPT</option> <option value="ltu,IN">latur,ltu</option> <option value="LUH,IN">Ludhiana,LUH</option> <option value="MAA,IN">Chennai,MAA</option> <option value="MAQ,TH">Mae Sot,MAQ</option> <option value="MYQ,IN">Mysore,MYQ</option> <option value="NAG,IN">Nagpur,NAG</option> <option value="NAK,TH">Nakhon Ratchasima,NAK</option> <option value="NAW,TH">Narathiwat,NAW</option> <option value="NDC,IN">Nanded,NDC</option> <option value="NMB,IN">Daman,NMB</option> <option value="NST,TH">Nakon Si Thammarat,NST</option> <option value="NVY,IN">Neyveli,NVY</option> <option value="PAB,IN">Bilaspur,PAB</option> <option value="PAN,TH">Pattani,PAN</option> <option value="PAT,IN">Patna,PAT</option> <option value="PBD,IN">Porbandar,PBD</option> <option value="PGH,IN">Pantnagar,PGH</option> <option value="PHS,TH">Phitsanulok,PHS</option> <option value="PNQ,IN">Pune,PNQ</option> <option value="PNY,IN">Pondicherry,PNY</option> <option value="PRH,TH">Phrae,PRH</option> <option value="PYX,TH">Pattaya,PYX</option> <option value="RAJ,IN">Rajkot,RAJ</option> <option value="RDP,IN">Durgapur,RDP</option> <option value="REW,IN">Rewa,REW</option> <option value="RJA,IN">Rajahmundry,RJA</option> <option value="RJI,IN">Rajouri,RJI</option> <option value="RNK,IN">SURFACE,RNK</option> <option value="RPR,IN">Raipur,RPR</option> <option value="RRK,IN">Rourkela,RRK</option> <option value="SHL,IN">Shillong,SHL</option> <option value="SLV,IN">Simla,SLV</option> <option value="SNO,TH">Sakon Nakhon,SNO</option> <option value="SSE,IN">Sholapur,SSE</option> <option value="STV,IN">Surat,STV</option> <option value="SXR,IN">Srinagar,SXR</option> <option value="SXV,IN">Salem,SXV</option> <option value="TCR,IN">Tuticorin,TCR</option> <option value="TEI,IN">Tezu,TEI</option> <option value="TEZ,IN">Tezpur,TEZ</option> <option value="THS,TH">Sukhothai,THS</option> <option value="TIR,IN">Tirupati,TIR</option> <option value="TNI,IN">Satna,TNI</option> <option value="TRV,IN">Trivandrum,TRV</option> <option value="TRZ,IN">Tiruchirapally,TRZ</option> <option value="TST,TH">Trang,TST</option> <option value="UBP,TH">Ubon Ratchathani,UBP</option> <option value="UDR,IN">Udaipur,UDR</option> <option value="URT,TH">Surat Thani,URT</option> <option value="USM,TH">Koh Samui,USM</option> <option value="UTH,TH">Udon Thani,UTH</option> <option value="UTP,TH">Utapao,UTP</option> <option value="VGA,IN">Vijayawada,VGA</option> <option value="VNS,IN">Varanasi,VNS</option> <option value="VTZ,IN">Vishakhapatnam,VTZ</option></select></div><div class="col-xs-12 col-md-4"><h3>To</h3><select class="select2 input-select"  name="to[]" required="required" onchange="setOrigin(this.text);"><option value="">Destination</option><option value="AGR,IN">Agra,AGR</option><option value="AGX,IN">Agatti Island,AGX</option><option value="AJL,IN">Aizawl,AJL</option><option value="AKD,IN">Akola,AKD</option><option value="AMD,IN">Ahmedabad,AMD</option><option value="ATQ,IN">Amritsar,ATQ</option><option value="BBI,IN">Bhubaneshwar,BBI</option><option value="BDQ,IN">Vadodara,BDQ</option><option value="BEP,IN">Bellary,BEP</option><option value="BHJ,IN">Bhuj,BHJ</option><option value="BHO,IN">Bhopal,BHO</option><option value="BHU,IN">Bhavnagar,BHU</option><option value="BKK,TH">Bangkok,BKK</option><option value="BKK,TH">Bangkok,BKK</option><option value="BLR,IN">Bangalore,BLR</option><option value="BOM,IN">Mumbai,BOM</option><option value="CBD,IN">Car Nicobar,CBD</option><option value="CCJ,IN">Calicut,CCJ</option><option value="CCU,IN">Kolkata,CCU</option><option value="CDP,IN">Cuddapah,CDP</option><option value="CEI,TH">Chiang Rai,CEI</option><option value="CJB,IN">Coimbatore,CJB</option><option value="CNX,TH">Chiang Mai,CNX</option><option value="COH,IN">Cooch Behar,COH</option><option value="COK,IN">Kochi,COK</option><option value="DBD,IN">Dhanbad,DBD</option><option value="DED,IN">Dehra Dun,DED</option><option value="DEL,IN">Delhi,DEL</option><option value="DHM,IN">Dharamsala,DHM</option><option value="DIB,IN">Dibrugarh,DIB</option><option value="DIU,IN">Diu,DIU</option><option value="DMU,IN">Dimapur,DMU</option><option value="GAU,IN">Guwahati,GAU</option><option value="GAY,IN">Gaya,GAY</option><option value="GOI,IN">Goa,GOI</option><option value="GOP,IN">Gorakhpur,GOP</option><option value="GWL,IN">Gwalior,GWL</option><option value="HBX,IN">Hubli,HBX</option><option value="HDY,TH">Hat Yai,HDY</option><option value="HHQ,TH">Hua Hin,HHQ</option><option value="HJR,IN">Khajuraho,HJR</option><option value="HKT,TH">Phuket,HKT</option><option value="HYD,IN">Hyderabad,HYD</option><option value="IDR,IN">Indore,IDR</option><option value="IMF,IN">Imphal,IMF</option><option value="ISK,IN">Nasik,ISK</option><option value="IXA,IN">Agartala,IXA</option><option value="IXB,IN">Bagdogra,IXB</option><option value="IXC,IN">Chandigarh,IXC</option><option value="IXD,IN">Allahabad,IXD</option><option value="IXE,IN">Mangalore,IXE</option><option value="IXG,IN">Belgaum,IXG</option><option value="IXH,IN">Kailashahar,IXH</option><option value="IXI,IN">Lilabari,IXI</option><option value="IXJ,IN">Jammu,IXJ</option><option value="IXK,IN">Keshod,IXK</option><option value="IXL,IN">Leh,IXL</option><option value="IXM,IN">Madurai,IXM</option><option value="IXP,IN">Pathankot,IXP</option><option value="IXR,IN">Ranchi,IXR</option><option value="IXS,IN">Silchar,IXS</option><option value="IXU,IN">Aurangabad,IXU</option><option value="IXW,IN">Jamshedpur,IXW</option><option value="IXY,IN">Kandla,IXY</option><option value="IXZ,IN">Port Blair,IXZ</option><option value="JAI,IN">Jaipur,JAI</option><option value="JDH,IN">Jodhpur,JDH</option><option value="JGA,IN">Jamnagar,JGA</option><option value="JGB,IN">Jagdalpur,JGB</option><option value="JLR,IN">Jabalpur,JLR</option><option value="JRH,IN">Jorhat,JRH</option><option value="JSA,IN">Jaisalmer,JSA</option><option value="KBV,TH">Krabi,KBV</option><option value="KKC,TH">Khon Kaen,KKC</option><option value="KLH,IN">Kolhapur,KLH</option><option value="KNU,IN">Kanpur,KNU</option><option value="KTU,IN">Kota,KTU</option><option value="KUU,IN">Kulu,KUU</option><option value="LDA,IN">Malda,LDA</option><option value="LKO,IN">Lucknow,LKO</option><option value="LOE,TH">Loei,LOE</option><option value="LOP,TH">Lop Buri,LOP</option><option value="LPT,TH">Lampang,LPT</option><option value="ltu,IN">latur,ltu</option><option value="LUH,IN">Ludhiana,LUH</option><option value="MAA,IN">Chennai,MAA</option><option value="MAQ,TH">Mae Sot,MAQ</option><option value="MYQ,IN">Mysore,MYQ</option><option value="NAG,IN">Nagpur,NAG</option><option value="NAK,TH">Nakhon Ratchasima,NAK</option><option value="NAW,TH">Narathiwat,NAW</option><option value="NDC,IN">Nanded,NDC</option><option value="NMB,IN">Daman,NMB</option><option value="NST,TH">Nakon Si Thammarat,NST</option><option value="NVY,IN">Neyveli,NVY</option><option value="PAB,IN">Bilaspur,PAB</option><option value="PAN,TH">Pattani,PAN</option><option value="PAT,IN">Patna,PAT</option><option value="PBD,IN">Porbandar,PBD</option><option value="PGH,IN">Pantnagar,PGH</option><option value="PHS,TH">Phitsanulok,PHS</option><option value="PNQ,IN">Pune,PNQ</option><option value="PNY,IN">Pondicherry,PNY</option><option value="PRH,TH">Phrae,PRH</option><option value="PYX,TH">Pattaya,PYX</option><option value="RAJ,IN">Rajkot,RAJ</option><option value="RDP,IN">Durgapur,RDP</option><option value="REW,IN">Rewa,REW</option><option value="RJA,IN">Rajahmundry,RJA</option><option value="RJI,IN">Rajouri,RJI</option><option value="RNK,IN">SURFACE,RNK</option><option value="RPR,IN">Raipur,RPR</option><option value="RRK,IN">Rourkela,RRK</option><option value="SHL,IN">Shillong,SHL</option><option value="SLV,IN">Simla,SLV</option><option value="SNO,TH">Sakon Nakhon,SNO</option><option value="SSE,IN">Sholapur,SSE</option><option value="STV,IN">Surat,STV</option><option value="SXR,IN">Srinagar,SXR</option><option value="SXV,IN">Salem,SXV</option><option value="TCR,IN">Tuticorin,TCR</option><option value="TEI,IN">Tezu,TEI</option><option value="TEZ,IN">Tezpur,TEZ</option><option value="THS,TH">Sukhothai,THS</option><option value="TIR,IN">Tirupati,TIR</option><option value="TNI,IN">Satna,TNI</option><option value="TRV,IN">Trivandrum,TRV</option><option value="TRZ,IN">Tiruchirapally,TRZ</option><option value="TST,TH">Trang,TST</option><option value="UBP,TH">Ubon Ratchathani,UBP</option><option value="UDR,IN">Udaipur,UDR</option><option value="URT,TH">Surat Thani,URT</option><option value="USM,TH">Koh Samui,USM</option><option value="UTH,TH">Udon Thani,UTH</option><option value="UTP,TH">Utapao,UTP</option><option value="VGA,IN">Vijayawada,VGA</option><option value="VNS,IN">Varanasi,VNS</option><option value="VTZ,IN">Vishakhapatnam,VTZ</option></select></div><div class="col-xs-12 col-md-4"><div class="date"><div class="depart"><h3>Depart</h3><input class="datepicker" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy"   required></div><div class="clear"></div></div></div></div>';                                       
$('#addCityMulti').on('click',function() {
    if(count<3)
    {    
    $('#addCityMulti').before(row);
    $('.select2').select2()
.on("select2:open", function () {
    $('.select2-results__options').niceScroll({
       cursorcolor: "#5fdfe8",
        cursorwidth: "8px",
        autohidemode: false,
        cursorborder: "2px solid #5fdfe8",
        horizrailenabled: false,
    });
});

    var dates = $(".datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {

    $(".datepicker").closest('div.body').next().find('div.datepicker').datepicker('option', 'minDate', date);
  }
});
$(".datepicker").closest('div.body').next().find('div.datepicker').datepicker({ numberOfMonths: 2,changeMonth: true,});
    //$('.multi-city-extra').append(row);

    count++;
    }
    else
    {
        $('#addCityMulti').hide();
        swal({
  type: 'error',
  title: 'Oops...',
  text: 'You can add maximum cities!',
  footer: '<a href>Why do I have this issue?</a>'
})
    }
    //$('.multi-trip-show:last').before('<div class="row" class="multi-trip-show" style="display:none"><div class="col-xs-12 col-md-2"><h3>From</h3><input id="input2" type="text" name="from" id "single" class="form-control"></div><div class="col-xs-12 col-md-2"><h3>To</h3><input id="input3" type="text" name="to"  class="form-control"></div><div class="col-xs-12 col-md-2"><div class="date"><div class="depart"><h3>Depart</h3><input  id="datepicker" name="depart_date" type="text" value="mm/dd/yyyy" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = "mm/dd/yyyy";}" required></div></div></div><span class="remove">Remove Option</span></div>');
});
// $('.multi-trip-show').on('click','.remove',function() {
//  	$(this).parent().remove();
// });
</script>
<script>
function setOrigin(val)
{
    // let op_value=$(val).find("option:selected").val();
    // let op_text=$(val).find("option:selected").text();

    // let next=$('.select2').next('select');

    // // let next=$(val).closest('div.body').next().find('div.select2');
    // alert($(next).find("option:selected").text());
    //  var option = $('<option></option>').attr("value",op_value).text(op_text);
    // $(next).empty().append(option);
    //alert($(next).find("option:selected").text());
    //alert(next);
}
</script>