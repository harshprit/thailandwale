@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')

        <!--=================== PAGE-COVER =================-->
        <section class="page-cover" id="cover-hotel-grid-list">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                      <h1 class="page-title">Hotel Listing </h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Hotel Listing </li>
                        </ul>
                    </div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end page-cover -->
        <!--================== search form ============-->
<div class="container-fluid">
        <div class=" form-flights">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-xs-12">
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  <div class="flights-headline">
                    <h3>Book Domestic & International Hotels</h3>
                  </div>
                </div>

              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_default_1">
              <form method="post"  action="{{route('frontend.search_hotel')}}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                             <div class="row">
                   <form method="post"  action="{{route('frontend.search_flight')}}" id="hotel-search-form" >
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="col-xs-6 col-md-4">
                                      <h3>Find Hotel</h3>
                                      <input id='input_hotel' type='text'name="dest" class='form-control' placeholder="Enter City" required>
                                      <input type="hidden" id ="destination" name="destination">
                                    </div>
                                <div class="col-xs-6 col-md-2">
                                      <div class="date">
                                    <div class="depart">
                                          <h3> Check-In</h3>
                                          <input  id="datepicker" name="check_in" type="text" autocomplete="off" placeholder="mm/dd/yyyy" onfocus="this.value = '';"  required>
                                        </div>
                                    <div class="clear"></div>
                                  </div>
                                    </div>
                                <div class="col-xs-6 col-md-2">
                                      <div class="return">
                                    <h3>Check-Out</h3>
                                    <input  id="datepicker1" name="check_out" type="text" autocomplete="off" placeholder="mm/dd/yyyy"  required>
                                  </div>
                                    </div>
                                <div class="col-xs-6 col-md-2">
                                      <div class="">
                                    <div class="trigger form-group form-group-lg form-group-icon-left"><i class="fa fa-users input-icon input-icon-highlight"></i>
                                          <label>Passenger</label>
                                          <input type="text" name="passengers" id="passengers" class="form-control flip"   onclick="myFunction()" placeholder="Rooms|Passengers" required>
                                        </div>
                                    <div id="panel" >
                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                    <div class="row" style="width: 434px;">
                                      <div class="col-md-2">
                                        <label class="control-label"><strong>Room(s)</strong>
                                             </label>
                                             <select name="rooms" id="rooms" onchange="showGuest(this.value);">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                      </select>
                                      </div>


                                         <div class="col-md-2">
                                          <div id="adults">

                                        <label class="control-label"><strong>Adult(s)</strong><br>
                                              </label>
                                                 <select onchange="count_adults();" name="adults[]" id="adults1">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                      </select>
                                      </div>
                                      </div>
                                         <div class="col-md-8" >
                                        <div class="form-group" id="children">
                                       <div class="row kids-row">
                                        <div class="col-md-6">
                                        <label class="control-label">
                                        <strong>Child</strong></label>
                                        <select name="children[]" id="children1" onchange="showAges(this.value,1);">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        </select>
                                        </div>
                                      <div class="col-md-8">
                                      <div class="row kids-selection" id="child1"></div>
                                      </div>
                                      </div>
                                      </div>
                                      </div>
                                      </div>

                                       <div class="col-md-12">
                                          <a class="btn btn-primary btn-block demise close" data-dismiss="modal">Done</a> </div>
                                        </div>
                                  </div>
                                    </div>
                                <div class="col-xs-12 col-md-2">
                                      <button type="sumbmit" class="btn btn-warning form-search-btn">Search</button>
                                    </div>
                  </form>
                              </div>
          </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>

  </div>
<!-- search form end ====-->
    <!--===== INNERPAGE-WRAPPER ====-->
        <section class="innerpage-wrapper">
          <div id="hotel-listing" class="innerpage-section-padding">
                <div class="container">
                    <div class="row">

                        <div id="sidebar-hotel" class="desktop-dhappa col-sm-12 col-md-3 side-bar left-side-bar">

                            <div class="side-bar-block filter-block">
                                <h3>Sort By Filter</h3>


                                <div class=" accordion panels-group" id="accordion ">

                                    <div class="panel panel-default">
                                        <div class="panel-heading active">
                                            <a href="#panel-1" data-toggle="collapse" >Select Category <span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->

                                        <div id="panel-1" class="panel-collapse collapse in">
                                            <div class="panel-body text-left ">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check01" name="category"/>
                                                    <label for="check01"><span><i class="fa fa-check"></i></span>All</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check02" name="category"/>
                                                    <label for="check02"><span><i class="fa fa-check"></i></span>Apartment</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check03" name="category"/>
                                                    <label for="check03"><span><i class="fa fa-check"></i></span>Bed & Breakfast</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check04" name="category"/>
                                                    <label for="check04"><span><i class="fa fa-check"></i></span>Guest House</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check05" name="category"/>
                                                    <label for="check05"><span><i class="fa fa-check"></i></span>Hotels</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check06" name="category"/>
                                                    <label for="check06"><span><i class="fa fa-check"></i></span>Residence</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check07" name="category"/>
                                                    <label for="check07"><span><i class="fa fa-check"></i></span>Resorts</label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->

                                    <div class="panel panel-default">
                                        <div class="panel-heading active">
                                            <a href="#panel-2" data-toggle="collapse" >Facility<span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->

                                        <div id="panel-2" class="panel-collapse collapse in">
                                            <div class="panel-body text-left">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check08" name="facility" value="ac"/>
                                                    <label for="check08"><span><i class="fa fa-check"></i></span>Air Conditioning</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check09" name="facility" value="bathroom"/>
                                                    <label for="check09"><span><i class="fa fa-check"></i></span>Bathroom</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check10" name="facility" value="cable_tv"/>
                                                    <label for="check10"><span><i class="fa fa-check"></i></span>Cable Tv</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check11" name="facility" value="parking"/>
                                                    <label for="check11"><span><i class="fa fa-check"></i></span>Parking</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check12" name="facility" value="pool"/>
                                                    <label for="check12"><span><i class="fa fa-check"></i></span>Pool</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check13" name="facility" value="wifi"/>
                                                    <label for="check13"><span><i class="fa fa-check"></i></span>Wi-fi</label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->

                                    <div class="panel panel-default">
                                        <div class="panel-heading active">
                                            <a href="#panel-3" data-toggle="collapse" >Rating <span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->

                                        <div id="panel-3" class="panel-collapse collapse in">
                                            <div class="panel-body text-left">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check14" name="ratings" value="5"/>
                                                    <label for="check14"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"> </i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check15" name="ratings" value="4"/>
                                                    <label for="check15"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check16" name="ratings" value="3"/>
                                                    <label for="check16"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check17" name="ratings" value="2"/>
                                                    <label for="check17"><span><i class="fa fa-check"></i></span><i class="fa fa-star"></i> <i class="fa fa-star"></i></label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check18" name="ratings" value="1"/>
                                                    <label for="check18"><span><i class="fa fa-check"></i></span> <i class="fa fa-star"></i></label></li>
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div><!-- end panel-default -->

                                             <div class="panel panel-default">
                                        <div class="panel-heading active">         
                                            <a href="#panel-3" data-toggle="collapse" aria-expanded="true" class="">Price<span><i class="fa fa-angle-down"></i></span></a>
                                        </div><!-- end panel-heading -->
                                        
                                        <div id="panel-3" class="panel-collapse collapse in" aria-expanded="true" style="">
                                            <div class="panel-body text-left">
                                                <ul class="list-unstyled">
                                                    <li class="custom-check"><input type="checkbox" id="check19" name="price" value="2000">
                                                    <label for="check19"><span><i class="fa fa-check"></i></span>Under &#8377;2000</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check20" name="price" value="4000">
                                                    <label for="check20"><span><i class="fa fa-check"></i></span>&#8377;2001 - &#8377;4000</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check21" name="price" value="6000">
                                                    <label for="check21"><span><i class="fa fa-check"></i></span>&#8377;4001 - &#8377;6000</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check22" name="price" value="8000">
                                                    <label for="check22"><span><i class="fa fa-check"></i></span>&#8377;6001 - &#8377;8000</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check23" name="price" value="10000">
                                                    <label for="check23"><span><i class="fa fa-check"></i></span>&#8377;8001 - &#8377;10000</label></li>
                                                    <li class="custom-check"><input type="checkbox" id="check24" name="price" value="10001">
                                                    <label for="check24"><span><i class="fa fa-check"></i></span>&#8377;10001 and above</label></li>
                                                    
                                                    
                                                </ul>
                                            </div><!-- end panel-body -->
                                        </div><!-- end panel-collapse -->
                                    </div>

                                </div><!-- end panel-group -->

                                <div class="price-slider">
                                    <p><input type="text" id="amount" readonly></p>
                                    <div id="slider-range"></div>
                                </div><!-- end price-slider -->
                            </div><!-- end side-bar-block -->

                            <div class="row">


                                <div class="col-xs-12 col-sm-6 col-md-12">
                                    <div class="side-bar-block support-block">
                                        <h3>Need Help</h3>
                                        <p>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum.</p>
                                        <div class="support-contact">
                                            <span><i class="fa fa-phone"></i></span>
                                            <p>+91 7404340404</p>
                                        </div><!-- end support-contact -->
                                    </div><!-- end side-bar-block -->
                                </div><!-- end columns -->

                            </div><!-- end row -->
                        </div><!-- end columns -->

                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side">
                          {!! view('frontend.home.partials.hotel-detail',compact('hotels')) !!}


                            <div class="pages">
                                <ol class="pagination">
                                    <li><a href="#" aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#" aria-label="Next"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>
                                </ol>
                            </div><!-- end pages -->
                        </div><!-- end columns -->

                    </div><!-- end row -->
              </div><!-- end container -->
            </div><!-- end hotel-listing -->
        </section><!-- end innerpage-wrapper -->
        <script>
        window.oncroll =function myFunction() {
         
                if (window.pageYOffset > 50||document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                    document.getElementById("sidebar-hotel").className = "test";
                } else {
                    document.getElementById("sidebar-hotel").className = "";
                }
            }
</script>
        @include('frontend.includes.footer')

<script>
$(document).ready(function(){
    
    $('input[type="checkbox"]').click(function(){
        let ratings=[];
        let prices=[];
        let facility=[];
        let locations=[];
        let category=[];
        $('input[name="ratings"]:checked').each(function() {
            ratings.push(this.value);
            });
            $('input[name="facility"]:checked').each(function() {
            facility.push(this.value);
            });
            $('input[name="category"]:checked').each(function() {
            category.push(this.value);
            });
            $('input[name="price"]:checked').each(function() {
            prices.push(this.value);
            });

            alert(prices);
   
    
});

});
</script>