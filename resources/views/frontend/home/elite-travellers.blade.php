@include('frontend.includes.header')
<section class="visa_section">
    <div class="container">


    <div class="row">
    
        <div class="col-md-8 col-xs-12" > 
        <br>
        <h2 class="beliya"><u>Elite Travellers</u></h2>
        
        <p align="justify" class="visa_text">At Thailand Wale, we understand that no two travellers are alike. While some people love the idea of a shoe-string budget, others like to live it up in the lap of luxury. This is why we offer our Elite Traveller packages. These packages are designed to meet all your standards, so that you can relax and enjoy a vacation exactly the way you pictured it. From travelling in your own private jets to staying only at the best hotels a city can offer, we can arrange it all for you. Furthermore, we can also arrange specific experiences like dining at Michelin star restaurants, enjoying amazing entertainment options and travelling within the city in luxury vehicles.
</br></br>
If you want your vacation to be a fantastic one, reach out to our holiday planners. By speaking to you about your needs, we ensure that everything you expect and receive is up to the mark! We work with high-end vendors who are highly rated, in order to make this possible. With years of experience in managing and creating great holidays, we are the right people to help you enjoy yourself to the fullest. 
</p>
        
        </div>
         
         
        
     
            <div class="col-md-4 col-xs-12">
        

            <div class="widget-sidebar">
                
            <div class="boxes">
              <h3><span class="fa fa-envelope"></span> Need Our Help ?</h3>
              <h5>WE WOULD BE HAPPY TO HELP YOU!</h5>
              <h5><i class="fa fa-phone"></i> <a href="tel:+8755169330">(+91) 74043 40404</a></h5>
              <h5>info@thailandwale.com</h5>
            </div>
            
            
            <br>
          
           <div class="boxes">
              
         <form method="post">
           <br>
            
                <div class='form-row'>
                <div class='form-group required'>
                    
                <input class='form-control' placeholder="Your Name" name="name" size='20' type='text'>
                </div>
                </div>
            
               <div class='form-row'>
               <div class='form-group card required'>
               <input autocomplete='off' required="true" placeholder="Your Email" name="email" class='form-control card-number' size='20' type='email'>
               </div>
               </div>
            
              <div class='form-row'>
              <div class='form-group card required'>
             
              <input autocomplete='off' required="true" placeholder="Your Number" name="number" class='form-control card-number' size='20' type='text'>
              </div>
              </div>
            
              <div class='form-row'>
              <div class='form-group cvc required'>
             
              <input autocomplete='off' class='form-control card-cvc' name="message" placeholder="Description" size='20' type='text'>
              </div>
              </div>
    
           
           
              <div class='form-row'>
              <div class='form-group'>
              <label class='control-label'></label>
              <button class='form-control btn btn-primary visa-button' type='submit'> Send Query →</button>
              </div>
              </div>

              </form>  
              
              
          </div>
          <br>
            
            </div>



</div>
</div>
   </div>
   </section>
@include('frontend.includes.footer')
