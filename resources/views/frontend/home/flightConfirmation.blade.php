@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')
<div class="pasflightdet">
<div class="tl_container mt5" id="mainResDiv">
        <div class="ht_guestdtl" id="mobile_pax_detail">
        <input type="hidden" id="departureDate" name="departureDate" value="28/09/2018">  
       
             
          
              <div class="ht_detlcontainer">
              <div class="fleft fcb_container">
              <div class="htd_agencyname">  
                                    
              <div class="agname" style="display:block;">Thailandwale </div>
              <address class="pa_none">
                Delhi,
                <br>
                Delhi<br>
                 Phone:  9780814728<br>
              </address>            
                         
              
              <p style="display:none;"><b>Booked by : </b> </p>


              </div>
                <div class="htd_cancel back_to" style="display:none;">

                    </div>
                  <div class="htd_checkin">
                
                       <p><span> PNR : </span> <span id="pnr">{{$fb_cnf->Response->FlightItinerary->PNR}}</span></p>
                       <p class="back_to" style="display:none;"><a href="#">Refresh PNR</a></p>
                       <p id="BookingStatus"> Hold </p>
                    
                      <p><b>Travel Date : </b> {{$fb_cnf->Response->FlightItinerary->Segments[0]->Origin->DepTime}}</p> 
                    </div>
              </div>
          </div>
                
        <div class="fleft width_100 mt5">
        <div class="ht_detlcontainer pa_none">
        <div class="fleft fcb_container">            
           
            <div class="booking_hd_det"><h1>Booking Details</h1>   
                
                
         <a class="desktop_not fright mt" style="color:#094D87" href="#" id="fareSummaryMobile">Fare Detail</a>
        </div>
        </div>
        </div>
           
        <div class="htd_head" id="mobile_flight_info_head"><h1>Flight Information </h1>
             
            <div class="fleft pl5">
         
                 </div>
             
        <div class="pushtoroamer">
                <span><a href="#" title="Roamer Description"><i class="fa fa-question-circle" aria-hidden="true" id="roamerDescLink"></i></a></span>
                <span class="mr5 ml"><a href="#null" class="roamer_btn btn_bg" id="pushToRoamer">Push To Roamer</a></span>                   
        </div>
         
        <div class="roamer_popup" style="display:none;" id="roamerPopup">
       <div class="puchroamer_head htd_head"><b class="fleft">Push Booking to Roamer App</b><a href="#" id="roamerClosebtn" class="fl_close_norm" style="margin-top: -22px !important;margin-right: -22px !important;"></a></div>
       
      <div id="roamerError" class="center width_100 pad_em mb5"></div>
       <div class="roamer_row">
       <b>Enter Guest Mobile No.</b>
       <span>
      <input class="fleft width_20 pad_em mb5" type="text" value="+91" id="roamerCountryCode">
     <input class="fleft width_70 pad_em mb5" type="text" id="roamerMobileNo" maxlength="10">
      </span>
      </div>
          <div class="fleft width_100 mt5 align_center">
            <a href="#null" class="roamer_btn btn_bg new_roamer_btn" id="pushBooking">Push Booking</a>
           <a href="#null" class="btn_bg" id="roamerBookingClose">Close</a>
           </div>
      </div>
        <div class="romar_bg" id="roamerDescription" style="display: none;">
    <a href="#" id="roamerDescClose" class="fl_close_norm" style="margin-top: -17px !important;margin-right: -22px !important;"></a>
	<h1>What is <img src="Images/romar_txt.png" alt="Roamer"></h1>
	<ul class="romar_list">
	<li>Roamer is your personal e-planner for trip management.</li>
		<li>It organizes all your reservations based on bookings pushed to it.</li>
		<li>Get instant push notifications &amp; travel reminders.</li>
		<li>Hassle – free offline access to bookings pushed by your travel agents.</li>
	</ul>
	<h1>Who can use <img src="Images/romar_txt.png" alt="Roamer"></h1>
		<h2>Travel Agents</h2>
			<ul class="romar_list">
				<li>Chat online with your customers and provide instant support.</li>
				<li>Have easy access to all your pushed bookings in one screen.</li>
			</ul>
		<h2>End Customers</h2>
			<ul class="romar_list">
				<li>Get instant in – app support from your agent.</li>
				<li>Automatically combines all bookings pushed by your travel agent in a single trip on the basis of itinerary details (destination / travel dates).</li>
				<li>Get instant notifications on bookings pushed.</li>
				<li>Automatic status updates for bookings.</li>
				<li>Flight delay / reschedule notifications.</li>
				<li>Sync once and enjoy offline access .</li>
			</ul>
	
<h2>Download the App to know more</h2>
<a target="_blank" href="https://play.google.com/store/apps/details?id=com.techmaster.roamer.app"><img class="roamer_appimg" src="Images/google_play.png" alt="Google Play App"></a>
<a target="_blank" href="https://itunes.apple.com/us/app/roamer-app/id1225527020?ls=1&amp;mt=8"><img class="roamer_appimg" src="Images/apple.png" alt="Apple App"></a>
</div>
  </div>

             
                             <div class="htd_databox">
                                  <span id="Span4"> <a href="#null" id="btnTicket" class="btn_main fright mr5"> Ticket </a> </span>  
                             </div>
                    
        <div class="htd_databox" id="mobile_flight_info_cont">
        
           <div class="htd_databoxin">
                
           <table cellpadding="0" cellspacing="0" width="100%" class="htd_table mobile_not">
                <tbody><tr>
                    <th width="90" align="left">Flight No</th>
                    <th align="left">Origin</th>
                    <th align="left">Destination</th>
                    <th align="left">Dep Date Time</th>
                    <th align="left">Arr Date Time</th>                  
                    <th align="left">Class</th>
                    <th align="left">Status</th>
                </tr>
              @if(isset($fb_cnf->Response->FlightItinerary->Segments))
              @foreach($fb_cnf->Response->FlightItinerary->Segments as $seg)
                <tr>
                    <td valign="top">{{$seg->Airline->AirlineCode."-".$seg->Airline->FlightNumber}}</td>
                    <td><span>{{$seg->Origin->Airport->AirportCode}}</span> </td>
                    
                    <td><span>{{$seg->Destination->Airport->AirportCode}}</span>  </td>
                    
                     <td><span>
                         <?php 
                              $start = date("d M Y H:i", strtotime($seg->Origin->DepTime));
                         ?>
                         {{$start." hrs"}}
                         <!--28/09/2018 16:00 hrs-->
                         </span> </td>
                    
                   
                    <td><span>
                         <?php 
                              $arrival = date("d M Y H:i", strtotime($seg->Destination->ArrTime));
                         ?>
                         {{$arrival." hrs"}}
                         
                        <!--28/09/2018 18:10 hrs-->
                        </span> </td>                    
                    <td valign="top"><span>{{$seg->Airline->FareClass}}</span></td>
                    <td>
                    
                    <span id="segmentStatus_0">{{$seg->Status}}</span>
                    </td>
                </tr>
                @endforeach
              @endif  
            </tbody></table>

           <table cellpadding="0" cellspacing="0" width="100%" class="htd_table pad_not desktop_not">
                <tbody><tr>
                    <th width="220" align="left">Flight No</th>
                    <th width="150" align="left">Origin</th>
                    <th width="150" align="left">Destination</th>
                </tr>
                 @if(isset($fb_cnf->Response->FlightItinerary->Segments))
              @foreach($fb_cnf->Response->FlightItinerary->Segments as $seg)
                <tr>
                    <td valign="top">
                       {{$seg->Airline->AirlineCode."-".$seg->Airline->FlightNumber}}
                        <span>{{$seg->Status}}</span>
                    </td>
                    <td>
                        <span>{{$seg->Origin->Airport->AirportCode}}</span><br>
                        <?php  $start_date = date("d M Y", strtotime($seg->Origin->DepTime));
                               $start_time = date("H:i", strtotime($seg->Origin->DepTime));
                        ?>
                        <span class="mob_date">{{$start_date}} </span>
                        <span>{{$start_time." hrs"}}</span>
                    </td>
                    
                    <td>
                        <span>{{$seg->Destination->Airport->AirportCode}}</span><br>
                         <?php  $arr_date = date("d M Y", strtotime($seg->Destination->ArrTime));
                               $arr_time = date("H:i", strtotime($seg->Destination->ArrTime));
                        ?>
                        <span class="mob_date"> {{$arr_date}} </span>
                        <span>{{$arr_time." hrs"}} </span>
                     </td>
                </tr>
                 @endforeach
              @endif   
            </tbody></table>
          </div>
        </div>
        
         <input type="hidden" id="TravelDepDate" name="TravelDepDate" value="28/09/2018">
            

        <div class="htd_head"><h1>Fare Rule</h1></div>
        <div class="htd_databox">
           <div class="htd_databoxin">
                
                
                    <div class="htd_formboxfarerule text-content" id="divFareRule">
        @if(isset($fb_cnf->Response->FlightItinerary->FareRules))
            @for($i=0;$i<count($fb_cnf->Response->FlightItinerary->FareRules);$i++)
            
                        <div class="fleft width_100">
                            <div class="htd_frmrow">
                                <code>{{$fb_cnf->Response->FlightItinerary->FareRules[$i]->Airline}}:{{$fb_cnf->Response->FlightItinerary->Segments[$i]->Origin->Airport->CityName}}({{$fb_cnf->Response->FlightItinerary->FareRules[$i]->Origin}}) - {{$fb_cnf->Response->FlightItinerary->Segments[$i]->Origin->Airport->CityName}}({{$fb_cnf->Response->FlightItinerary->FareRules[$i]->Destination}})</code>
                            </div>
                        </div>
                            <div class="fleft width_100">
                            {{$fb_cnf->Response->FlightItinerary->FareRules[$i]->FareRuleDetail}}
                            </div>
               
            @endfor
        @endif
                </div>
           </div>
        </div>
          <div class="htd_head">
            <h1>Passenger Details</h1>
             
        </div>
        @if(isset($fb_cnf->Response->FlightItinerary->Passenger))
        <?php $i=1; ?>
        @foreach($fb_cnf->Response->FlightItinerary->Passenger as $psg)
        <div class="htd_databox">
           <div class="htd_databoxin">
                <div class="htd_heading"><dfn>Passenger&nbsp;{{$i++}}&nbsp;-&nbsp;</dfn>
                @if($psg->PaxType==1)
                (Adult)
                @elseif($psg->PaxType==2)
                (Child)
                @elseif($psg->PaxType==3)
                (Infant)
                @endif
                

                </div>
                <div class="htd_formbox">
                    <div class="frm_left m_fcb_left">
                        <div class="htd_frmrow">
                            <label>Name : </label>
                            <code>{{$psg->Title." ".$psg->FirstName." ".$psg->LastName}}</code>
                        </div>
                        
                        <div class="htd_frmrow">
                            <label>Gender : </label>
                            <code>
                                @if($psg->Gender==1)
                                {{"Male"}}
                                @else
                                {{"Female"}}
                                @endif
                            </code>
                        </div>
                        
                         <div class="htd_frmrow">
                            <label>Address : </label>
                            @if(!empty($psg->AddressLine1))
                            <code>{{$psg->AddressLine1}}</code>
                            @endif
                            @if(!empty($psg->AddressLine2))
                            <code>{{"OR ".$psg->AddressLine2}}</code>
                            @endif
                        </div>
                        
                    </div>
                    <div class="frm_right m_fcb_left">
                    
                        <div class="htd_frmrow">
                            <label>Mobile No. </label>
                            @if(isset($psg->ContactNo))
                            <code>{{$psg->ContactNo}} </code>
                            @endif
                        </div>
                        
                       <div class="htd_frmrow">
                            <label>Dob : </label>
                            @if($psg->DateOfBirth)
                            <?php $dob = date("d M Y", strtotime($psg->DateOfBirth)) ?>
                            <code>{{$dob}}</code>
                            @endif
                        </div>
                        
                        <div class="htd_frmrow">
                            
                            <label>Email : </label>
                            @if(isset($psg->Email))
                            <code>{{$psg->Email}}</code>
                            @endif
                        </div>
                        
                    </div>
                </div>
                 
                <div class="htd_formbox">
                    <div class=" m_fcb_left">
                    
                    </div>  
                    
                </div>
               
               

        </div>
        </div>
        @endforeach
        @endif
       
        
               <input type="hidden" id="ISCSCAgent" name="ISCSCAgent" value="False">
              <input type="hidden" id="ISOxiAgent" name="ISOxiAgent" value="False">
            <input type="hidden" id="isB2BPGEnable" name="isB2BPGEnable" value="False">
  
                    
                    <div class="btnrow btn_mar">
                        <span class="fright">
                             
                               <input type="hidden" id="hSource" name="hSource" value="Indigo">
                               <input type="hidden" id="hPNR" name="hPNR" value="CBWL6G">
                               <input type="hidden" id="hBookingId" name="hBookingId" value="1383361">
                                <span class="fleft mr mt5"> You have <b class="red">Rs. 1,290,386.00</b> left as your cash balance. </span>  
                                  
                                  <span id="Span3"> <a href="#null" id="btnTicket" class="btn_main fleft mr5"> Ticket </a> </span>  
                              
                        </span>
                    </div>  
                  
              <input type="hidden" id="HoldBookingTicket" name="HoldBookingTicket">
              <input type="hidden" id="SessionStampId" name="SessionStampId" value="1682018135433890">
            
    
                      
        <div class="htd_databox" style="display:block;">
           <div class="htd_databoxin">
                <div class="htd_heading" id="ShowSSRInfo"><a href="#null" id="ssrinfo"><dfn>SSR Information</dfn></a> &nbsp; 
                    </div>
           </div>
             <div id="ssrResults" style="display:none;">

             </div>
            <div id="no_ssr_div" style="display:none;">
                <span class="fleft bold padding-5 notice-board-orange-bg contract-right-top-heading-width">SSR information is not available</span>
            </div>
             <div id="loadingMsg" style="display: none;">
                       <img src="images/loaderNew.gif" alt="loading" class="fleft"> <b class="fleft ml mt">Loading SSR Information...</b>
               </div>            
        </div>
      
                

        
        <!--<div class="htd_databox">-->
        <!--   <div class="htd_databoxin">-->
        <!--        <div class="htd_heading"><dfn>Booking History </dfn><span class="back_to fright"><a href="#null" id="BkHistory">Close</a></span></div>              -->
        <!--        <div class="row mscl" id="bkdatatbl" style="">-->
        <!--        <table cellpadding="0" cellspacing="0" width="100%" class="htd_table">-->
        <!--         <tbody><tr>-->
        <!--            <th class="text_aln_left" valign="top">Date</th>             -->
        <!--            <th class="text_aln_left">Time</th>             -->
        <!--            <th class="text_aln_left">Status</th>             -->
        <!--            <th class="text_aln_left">Narration</th>             -->
        <!--            <th class="text_aln_left">User Name</th>             -->
        <!--        </tr>-->
                
        <!--         <tr>-->
        <!--            <td valign="top">16/Aug/2018</td>-->
        <!--            <td><span>1:56 PM</span> </td>-->
        <!--            <td><span>Booking</span> </td>-->
        <!--            <td><span>Booking is on hold. (through BookingEngine Service),  Booked By Auto (IP Address :- 27.5.22.167) | MSDTC OFF</span> </td>                  -->
        <!--            <td><span>Utkarsh Sharma</span> </td>  -->
        <!--        </tr>-->
                               
        <!--    </tbody></table>-->
        <!--   </div>-->
        <!--   </div>-->
        <!--</div>-->
        
        <!--<div class="htd_databox" style="display:none;">-->
        <!--   <div class="htd_databoxin">-->
        <!--        <div class="htd_heading"><dfn>Show Comment</dfn><span class="back_to fright"><a href="#">Close</a></span></div>-->
        <!--       <div class="row mscl">-->
        <!--        <table cellpadding="0" cellspacing="0" width="100%" class="htd_table">-->
        <!--         <tbody><tr>-->
        <!--            <th class="text_aln_left" valign="top">Date</th>             -->
        <!--            <th class="text_aln_left">Time</th>           -->
        <!--            <th class="text_aln_left">Status</th>             -->
        <!--            <th class="text_aln_left">Narration</th>             -->
        <!--            <th class="text_aln_left">User NAme</th>             -->
        <!--            <th class="text_aln_left">&nbsp;</th>             -->
        <!--        </tr>-->
        <!--         <tr>-->
        <!--            <td valign="top">31/07/2013</td>-->
        <!--            <td><span>1523hrs</span> </td>-->
        <!--            <td><span>Booking</span> </td>-->
        <!--            <td><span>Booking is in ready state.(IP Address :- 203.88.135.151)</span> </td>-->
        <!--            <td><span>Raxa Rajput</span> </td> -->
        <!--            <td><a class="link" href="#">Edit &amp; Reply</a></td>                   -->
        <!--        </tr>-->
        <!--        <tr>-->
        <!--            <td valign="top">31/07/2013</td>-->
        <!--            <td><span>1524hrs</span> </td>-->
        <!--            <td><span>Ticketing</span> </td>-->
        <!--            <td><span>Booking is in ready state.(IP Address :- 203.88.135.151)</span> </td>-->
        <!--            <td><span>Raxa Rajput</span> </td>  -->
        <!--            <td><a class="link" href="#">Edit &amp; Reply</a></td>                  -->
        <!--        </tr>-->
        <!--        <tr>-->
        <!--            <td valign="top">31/07/2013</td>-->
        <!--            <td><span>1524hrs</span> </td>-->
        <!--            <td><span>Payment</span> </td>-->
        <!--            <td><span>Invoice generated</span> </td>-->
        <!--            <td><span>Raxa Rajput</span> </td>-->
        <!--            <td><a class="link" href="#">Edit &amp; Reply</a></td>                    -->
        <!--        </tr>-->
        <!--        <tr>-->
        <!--            <td valign="top">31/07/2013</td>-->
        <!--            <td><span>1524hrs</span> </td>-->
        <!--            <td><span>Ticketing</span> </td>-->
        <!--            <td><span>Booking is in ready state.(IP Address :- 203.88.135.151)</span> </td>-->
        <!--            <td><span>Raxa Rajput</span> </td> -->
        <!--            <td><a class="link" href="#">Edit &amp; Reply</a></td>                   -->
        <!--        </tr>-->
        <!--    </tbody></table>-->
        <!--        </div>-->
        <!--</div>-->
        <!--   </div>-->
        <!--<div class="htd_databox" style="display:none;">-->
        <!--   <div class="htd_databoxin">-->
        <!--   <div class="frm_left">-->
        <!--        <div class="htd_frmrow">-->
        <!--            <label>Service type :</label> -->
        <!--            <code><select class="width166 fly_select"><option>Wheel Chair</option></select></code>-->
        <!--        </div>-->
                
        <!--    </div>-->
        <!--    <div class="frm_right">-->
        <!--        <div class="htd_frmrow">-->
        <!--            <label>Enter Comments :</label>-->
        <!--            <code><textarea class="fright width_220" rows="" cols=""></textarea></code>-->
        <!--        </div>-->
        <!--        <div class="htd_frmrow">-->
        <!--            <code class="fright"><input type="button" value="Submit"></code>-->
        <!--            <code class="fright width_160"><em class="fleft width73"><input class="mr5" type="radio">Public</em><em class="fleft width90"><input class="mr5" type="radio">Private</em></code>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--    </div>-->
        <!--    </div>-->
            <div class="btnrow">
			</div>
            </div>
            </div>
             <div class="special_deals mobfilter_display_none" id="mobile_queue_filter">
            <h2>Sale Summary <a id="filterCloseBtn" class="fl_close_for_mobile ipad_not desktop_not"></a></h2>
            <div class="deal_head cursor">
                
                <a href="#" id="divSalesSummaryHead">+ Show Details</a>
                

            </div>
             <div class="deal_cont">
             
                 <div class="deal_head">
                    <div class="flight_details_top bord_bottom">
                        <em class="width65 bold">28 Sep 18 </em>
                        <em class="width65 bold align_center">6E755 </em>
                        <em class="width65 align_right bold">HO Class</em>
                    </div>
                     <div class="flight_details_top">
                        <em class="width65 bold">Dept:</em>
                        <em class="width65 font-normal align_center">DEL</em>
                        <em class="width65 align_right font-normal">@1600 hrs</em>
                    </div>
                     
                     <div class="flight_details_top">
                        <em class="width65 bold">Arr:</em>
                        <em class="width65 font-normal align_center">BOM</em>
                        <em class="width65 align_right font-normal">@1810 hrs</em>
                    </div>           
                </div>
                                  
                    
                       <div id="mainFareDiv" class="deal_price" style="display:block;">
                       <h3>Fare / Pax Type</h3>
                     
                            <div class="drdr">
                                    <div class="drdr_in">
                                      <div class="raterow">
                                        <label>Adult</label>
                                        <code>Rs.867.00
                                            
                                             </code>
                                     </div>
                                      <div class="raterow">
                                        <label>Tax and S.Charges:</label>
                                        <code>Rs.1,690.09
                                              
                                             </code>
                                      </div>
                                       <div class="raterow">
                                        <label>T. Fee</label>
                                        <code>Rs.0.00
                                              
                                        </code>
                                      </div>
                                  
                                      
                                  </div>
                                  </div>
                            <div class="drdr total">
                               <div class="raterow">
                                   <label>Total <br></label>
                                    <code><b class="bold">Rs.2,557.09
                                           
                                         </b></code>
                               </div>
                            </div>
                        
                            <div class="drdr">
                                    <div class="drdr_in">
                                      <div class="raterow">
                                        <label>Child</label>
                                        <code>Rs.867.00
                                              
                                        </code>
                                     </div>
                                      <div class="raterow">
                                        <label>Tax and S.Charges</label>
                                        <code>Rs.1,690.09
                                              
                                             </code>
                                      </div>
                                       <div class="raterow">
                                        <label>T. Fee</label>
                                        <code>Rs.0.00
                                              
                                             </code>
                                      </div>
                                   
                                        
                                  </div>
                                  </div>
                            <div class="drdr total">
                               <div class="raterow">
                                  <label>Total <br></label>
                                    <code><b class="bold">Rs.2,557.09
                                           
                                         </b></code>
                               </div>
                            </div>
                        
                            <div class="drdr">
                                    <div class="drdr_in">
                                      <div class="raterow">
                                        <label>Infant</label>
                                        <code>Rs.1,190.48
                                               
                                             </code>
                                     </div>
                                      <div class="raterow">
                                        <label>Tax and S.Charges</label>
                                        <code>Rs.59.52
                                             
                                             </code>
                                      </div>
                                       <div class="raterow">
                                        <label>T. Fee</label>
                                        <code>Rs.0.00
                                               
                                             </code>
                                      </div>
                                     

                                      
                                  </div>
                                  </div>
                            <div class="drdr total">
                                <div class="raterow">
                                    <label>Total <br></label>
                                    <code><b class="bold">Rs.1,250.00
                                                
                                    </b></code>
                                </div>
                           </div>
                        

                                 <h3 class="mt5imp">Total Fare</h3>

                                  
                                            <div class="drdr_in">
                                            <div class="raterow">
                                                <label>Adult x 1</label>
                                                <code>Rs.2,557.09
                                                    
                                                     </code>
                                            </div>
                                            </div>
                                  
                                            <div class="drdr_in">
                                            <div class="raterow">
                                                <label>Child x 1</label>
                                                <code>Rs.2,557.09
                                                    
                                                     </code>
                                            </div>
                                            </div>
                                  
                                            <div class="drdr_in">
                                            <div class="raterow">
                                                <label>Infant x 1</label>
                                                <code>Rs.1,250.00
                                                            
                                                </code>
                                            </div>
                                            </div>
                                 
                        <div class="drdr_in">
                      <div class="raterow">
                        <label>Meal(<code style="float:none;">1</code>Platter)</label>
                        <code>Rs.500.00
                            
                        </code>
                     </div>
                  </div>
                    
                        <div class="drdr_in">
                      <div class="raterow">
                        <label>Total GST</label>
                        <code>Rs.72.00
                            
                        </code>
                     </div>
                  </div>
                    
                        <div class="grandtotal">
                            
                        <label>Total Pub. Fare</label>
                            
                        <code><b class="bold">Rs.6,936.18
                            
                              </b></code>
                        </div>
                  </div>
                 
                 
                                     </div>
                 <div id="divSalesSummary" class="deal_price" style="display:none;">
                  <h3>Fare / Pax Type</h3>
                   <div class="drdr">
                      <div class="drdr_in">
                       <div class="flight_details_top pb5">
                            <em class="width65 bold">&nbsp;</em>
                           
                            <em class="width65 align_right  bold">Published</em>
                            <em class="width65 align_right bold">Offered</em>
                           
                       </div>               
                   
                             <div class="flight_details_top">
                                <em class="width65 bold">Adult:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.867.00
                                    
                                </em>
                                <em class="width65 align_right font-normal">Rs.820.98
                                    
                                </em>
                                 
                            </div>

                             <div class="flight_details_top">
                                <em class="width65 bold">OT Tax and S.Charges:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.1,190.09
                                 
                                </em>
                                <em class="width65 align_right font-normal">Rs.1,190.09
                                     
                                </em>
                                 
                            </div>
                             <div class="flight_details_top">
                                <em class="width65 bold">YQ Tax:</em>
                                 

                                <em class="width65 align_right  font-normal">Rs.500.00
                                     
                                </em>
                                <em class="width65 align_right font-normal">Rs.500.00
                                     
                                </em>
                                 
                            </div>
                             <div class="flight_details_top">
                                <em class="width65 bold">T. Fee:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.0.00
                                     
                                </em>
                                <em class="width65 align_right font-normal">Rs.0.00
                                     
                                </em>
                                 
                            </div>
                        
                          
                             <div class="flight_details_top total padding_top_bot_5">
                                
                                <em class="width65 bold">Total:</em>
                                  
                                <em class="width65 align_right  bold">Rs.2,557.09
                                     
                                </em>
                                <em class="width65 align_right bold">Rs.2,511.07
                                     
                                </em>
                                 
                            </div>
                    
                             <div class="flight_details_top">
                                <em class="width65 bold">Child:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.867.00
                                    
                                </em>
                                <em class="width65 align_right font-normal">Rs.820.98
                                      
                                </em>
                                 
                            </div>
                             <div class="flight_details_top">
                                <em class="width65 bold">OT Tax and S.Charges:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.1,190.09
                                      
                                </em>
                                <em class="width65 align_right font-normal">Rs.1,190.09
                                      
                                </em>
                                 
                            </div>
                             <div class="flight_details_top">
                                <em class="width65 bold">YQ Tax:</em>

                                 

                                <em class="width65 align_right  font-normal">Rs.500.00
                                      
                                </em>
                                <em class="width65 align_right font-normal">Rs.500.00
                                      
                                </em>
                                 
                            </div>
                             <div class="flight_details_top">
                                <em class="width65 bold">T. Fee:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.0.00
                                      
                                </em>
                                <em class="width65 align_right font-normal">Rs.0.00
                                      
                                </em>
                                 
                            </div>
                          
                          
                             <div class="flight_details_top total padding_top_bot_5">
                                <em class="width65 bold">Total:</em>
                                 
                                <em class="width65 align_right  bold">Rs.2,557.09
                                      
                                </em>
                                <em class="width65 align_right bold">Rs.2,511.07
                                      
                                </em>
                                 
                            </div>
                                              
                             <div class="flight_details_top">
                                <em class="width65 bold">Infant:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.1,190.48
                                    
                                </em>
                                <em class="width65 align_right font-normal">Rs.1,190.48
                                    
                                </em>
                                 
                            </div>
                             <div class="flight_details_top">
                                <em class="width65 bold">OT Tax and S.Charges:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.59.52
                                    
                                </em>
                                <em class="width65 align_right font-normal">Rs.59.52
                                    
                                </em>
                                 
                            </div>
                             <div class="flight_details_top">
                                <em class="width65 bold">YQ Tax:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.0.00
                                    
                                </em>
                                <em class="width65 align_right font-normal">Rs.0.00
                                    
                                </em>
                                 
                            </div>
                             <div class="flight_details_top">
                                <em class="width65 bold">T. Fee:</em>
                                 
                                <em class="width65 align_right  font-normal">Rs.0.00
                                    
                                </em>
                                <em class="width65 align_right font-normal">Rs.0.00
                                    
                                </em>
                                 
                            </div>
                             

                          
                             <div class="flight_details_top total padding_top_bot_5">
                                <em class="width65 bold">Total:</em>

                                 
                                <em class="width65 align_right  bold">Rs.1,250.00
                                    
                                </em>
                                <em class="width65 align_right bold">Rs.1,250.00
                                    
                                </em>
                                 
                            </div>
                     
                            <div class="drdr_in">
                            <div class="raterow">
                                <label>Adult x 1</label>
                                <code>Rs.2,557.09
                                    
                                </code>
                            </div>
                            </div>
                     
                            <div class="drdr_in">
                            <div class="raterow">
                                <label>Child x 1</label>
                                <code>Rs.2,557.09
                                    
                                </code>
                            </div>
                            </div>
                     
                            <div class="drdr_in">
                            <div class="raterow">
                                <label>Infant x 1</label>
                                <code>Rs.1,250.00
                                    
                                </code>
                            </div>
                            </div>
                    
                                              </div>

                    
                      <div class="drdr_in">
                          <div class="raterow">
                            <label>Meal (<code style="float:none;">1</code>Platter)</label>
                            <code>Rs.500.00
                                
                            </code>
                         </div>
                      </div>
                  
                    <div class="grandtotal grandtotal_inner">
                          <div class="raterow">
                              
                    <label>Total Pub. Fare</label>
                              
                    <code><b>Rs.6,864.18
                        
                          </b></code>
                              </div>
                          <div class="raterow">                   
                    <label>Comm. Earned (-)</label>
                    <code><b>Rs.33.28
                        
                          </b></code> 
                              </div>
                         <div class="raterow">
                    <label>Transaction Fee (-)</label> 
                    <code><b>Rs.0.00
                        
                          </b></code>
                             </div> 

                         
                            <div class="raterow">
                    <label>TDS (+)</label>
                    <code><b>Rs.9.98
                        
                          </b></code>
                                </div>
                         
                    
                                      <div class="raterow">
                    <label>PLB Earned (-)</label>
                    <code><b>Rs.35.06
                        
                          </b></code>
                                          </div>
                         <div class="raterow">
                    <label>TDS on PLB (+)</label>
                    <code><b>Rs.10.52
                        
                          </b></code> 
                        </div>
                          <div class="raterow">
                    <label>Incntv Earned (-)</label>
                    <code><b>Rs.23.70
                        
                          </b></code> 
                       </div>
                          <div class="raterow">
                    <label>TDS on Incntv (+)</label>
                    <code><b>Rs.7.12
                        
                          </b></code>
                              </div>
                        
                        <div class="raterow">
                    <label>Total GST(+)</label>
                    <code><b>Rs.72.00
                        
                          </b></code>
                              </div>
                        
                   
                  </div>

                  <div class="total_payble align_right">
                    <label class=" text_aln_left">Total Payable:</label>
                    <code><b class="bold">Rs.6,871.76
                        
                          </b></code>
                  </div>
                   <div class="total_payble align_right">
                    <label class=" text_aln_left">Total Commission Earned</label>
                    <code><b class="bold">Rs.92.04
                        
                          </b></code>
                 </div>
                                               
                       </div>
                  </div>
                
                 <div>
                     
                 </div>
                <div class="fleft width_100 align_center pt5 pb desktop_not">
                <a id="filterCloseBtn" class="btn_continue_s desktop_not" href="#">Close</a>
            </div>
         </div>

        <div id="BlockDivForSmallScreen" class="bg_block desktop_not mobfilter_display_none" style="display: none;"></div>

    </div>
	</div>
	@include('frontend.includes.footer')
