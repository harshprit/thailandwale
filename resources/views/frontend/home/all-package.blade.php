@include('frontend.includes.header')
@include('frontend.includes.nav1')
@if(Session::has('error'))
<div class=' alert alert-danger'>{{Session::get('error')}}</div>
@endif
<!--========== packages ================--> 
<!--===== INNERPAGE-WRAPPER ============-->
<section class="innerpage-wrapper">
  <div id="hotel-listing" class="innerpage-section-padding all-package-page">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 side-bar left-side-bar desktop-dhappa">
          <div class="side-bar-block filter-block">
            <h3>Sort By Filter</h3>
            <div class=" accordion panels-group" id="accordion ">
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-1" data-toggle="collapse" >Select Category <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-1" class="panel-collapse collapse in">
                  <div class="panel-body text-left ">
                    <form>
                      <select class="form-control select2">
                        <option>Select</option>
                        <option>Car</option>
                        <option>Bike</option>
                        <option>Scooter</option>
                        <option>Cycle</option>
                        <option>Horse</option>
                      </select>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-2" data-toggle="collapse" >Budget<span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-2" class="panel-collapse collapse in">
                  <div class="panel-body text-left">
                    <form>
                      <div>
                        <input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter">
                        <label for="subscribeNews" style="color: #000;">Up To <i class="fa fa-inr" aria-hidden="true"></i> 19999</label>
                      </div>
                      <div>
                        <input type="checkbox" id="subscribeNews1" name="subscribe" value="newsletter">
                        <label for="subscribeNews1" style="color: #000;"><i class="fa fa-inr" aria-hidden="true"></i>20000 To <i class="fa fa-inr" aria-hidden="true"></i>29999</label>
                      </div>
                      <div>
                        <input type="checkbox" id="subscribeNews2" name="subscribe" value="newsletter">
                        <label for="subscribeNews2" style="color: #000;"><i class="fa fa-inr" aria-hidden="true"></i>30000 To <i class="fa fa-inr" aria-hidden="true"></i>39999</label>
                      </div>
                      <div>
                        <input type="checkbox" id="subscribeNews3" name="subscribe" value="newsletter">
                        <label for="subscribeNews3" style="color: #000;"><i class="fa fa-inr" aria-hidden="true"></i>40000 To <i class="fa fa-inr" aria-hidden="true"></i>49999</label>
                      </div>
                      <div>
                        <input type="checkbox" id="subscribeNews4" name="subscribe" value="newsletter">
                        <label for="subscribeNews4" style="color: #000;"><i class="fa fa-inr" aria-hidden="true"></i>50000 To <i class="fa fa-inr" aria-hidden="true"></i>74999</label>
                      </div>
                      <div>
                        <input type="checkbox" id="subscribeNews6" name="subscribe" value="newsletter">
                        <label for="subscribeNews6" style="color: #000;"><i class="fa fa-inr" aria-hidden="true"></i>75000 To <i class="fa fa-inr" aria-hidden="true"></i>99999</label>
                      </div>
                      <div>
                        <input type="checkbox" id="subscribeNews5" name="subscribe" value="newsletter">
                        <label for="subscribeNews5" style="color: #000;"><i class="fa fa-inr" aria-hidden="true"></i> 100000 And above</label>
                      </div>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-3" data-toggle="collapse" >Duration <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-3" class="panel-collapse collapse in">
                  <div class="panel-body text-left">
                    <div>
                      <input type="checkbox" id="subscribeNews7" name="subscribe" value="newsletter">
                      <label for="subscribeNews7" style="color: #000;">Up To 3 Nights</label>
                    </div>
                    <div>
                      <input type="checkbox" id="subscribeNews8" name="subscribe" value="newsletter">
                      <label for="subscribeNews8" style="color: #000;">4 Nights</label>
                    </div>
                    <div>
                      <input type="checkbox"  name="subscribe" value="newsletter">
                      <label  style="color: #000;">5 To 7 Nights</label>
                    </div>
                    <div>
                      <input type="checkbox"  name="subscribe" value="newsletter">
                      <label  style="color: #000;">8 To 10 Nights</label>
                    </div>
                    <div>
                      <input type="checkbox"  name="subscribe" value="newsletter">
                      <label  style="color: #000;">11 Nights And above</label>
                    </div>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-4" data-toggle="collapse" >Hotel Choice <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-4" class="panel-collapse collapse in">
                  <div class="panel-body text-left ">
                    <form>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Up To 3 Stars</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">4 Stars</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">5 Stars</label>
                      </div>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-5" data-toggle="collapse" >Travel Option <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-5" class="panel-collapse collapse in">
                  <div class="panel-body text-left ">
                    <form>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">By Flight</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">By Car</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">By Bus</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Includes Sightseeing</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Includes Meals</label>
                      </div>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-6" data-toggle="collapse" >Themes <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-6" class="panel-collapse collapse in">
                  <div class="panel-body text-left ">
                    <form>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Romantic</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Leisure</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Adventure</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Culture</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Once in a Lifetime</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Historical</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Kid-friendly</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Wellness</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Detox</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Spiritual</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Unurban</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Indulgence</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Wildlife</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Heritage</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Shopping</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Nightlife</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Festivals and Events</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Foodie</label>
                      </div>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-7" data-toggle="collapse" >Suitable For <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-7" class="panel-collapse collapse in">
                  <div class="panel-body text-left ">
                    <form>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Families</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Couples</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Friends</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Senior Couples</label>
                      </div>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-8" data-toggle="collapse" >Places <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-8" class="panel-collapse collapse in">
                  <div class="panel-body text-left ">
                    <form>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Udaipur</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Leh</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Jaipur</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Allepey</label>
                      </div>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-9" data-toggle="collapse" >Type of packages <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-9" class="panel-collapse collapse in">
                  <div class="panel-body text-left ">
                    <form>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Online Deals</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Non Online Deals</label>
                      </div>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default -->
              
              <div class="panel panel-default">
                <div class="panel-heading active"> <a href="#panel-10" data-toggle="collapse" >Flexibility <span><i class="fa fa-angle-down"></i></span></a> </div>
                <!-- end panel-heading -->
                
                <div id="panel-10" class="panel-collapse collapse in">
                  <div class="panel-body text-left ">
                    <form>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Flexible Bundles</label>
                      </div>
                      <div>
                        <input type="checkbox"  name="subscribe" value="newsletter">
                        <label  style="color: #000;">Fixed Bundles</label>
                      </div>
                    </form>
                  </div>
                  <!-- end panel-body --> 
                </div>
                <!-- end panel-collapse --> 
              </div>
              <!-- end panel-default --> 
            </div>
            <!-- end panel-group --> 
            
          </div>
          <!-- end side-bar-block -->
          
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-12">
              <div class="side-bar-block support-block">
                <h3>Need Help</h3>
                <p>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum.</p>
                <div class="support-contact"> <span><i class="fa fa-phone"></i></span>
                  <p>+1 123 1234567</p>
                </div>
                <!-- end support-contact --> 
              </div>
              <!-- end side-bar-block --> 
            </div>
            <!-- end columns --> 
            
          </div>
          <!-- end row --> 
        </div>
        <!-- end columns -->
        
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        
  @if(isset($package)&& count($package)>0)     
  @foreach($package as $iv)

  <?php
        $destination_id_str=$iv->destination_id; 
        $destination_id_ar = explode('-',$destination_id_str);
        $dest_id=array();
        for($i=0;$i<count($destination_id_ar);$i++)
        {
            $dest_id_ar=explode(",",$destination_id_ar[$i]);
          $dest_id[$i]=$dest_id_ar[0];
        }
       
   ?>
  @if(!empty($source_id))
       @php $source=explode('-',$source_id) @endphp
  @if($source[0]=="destination")
  @foreach($dest_id as $did)
  @if($did==$source[1])
      
       
  <div class="col-sm-6 col-md-12 package-card">
    <div class="row pack-header">
      <div class="col-md-9">
        <h2 class="packages-headline">{{strtoupper($iv->title)." - "}}{{ ($iv->no_days)==0 ? " ": ($iv->no_days)."Nights/" }}{{($iv->no_days+1)." Days"}} </h2>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 tour_type hidden-xs">
        <label class="packtype text-capitalize">tour type</label>
        <select class="package-class PackType">
          <?php 
          $package_type_str = $iv->package_type;
          $package_type_array = explode(",",$package_type_str);
          $count=0;
          //  var_dump($package_type_array);
          
          ?>
         
            @foreach($packagetype as $ptype)
              @for($j=0;$j < count($package_type_array);$j++)
                @if($ptype->id==$package_type_array[$j]) 
                  <?php $count++; ?> 
                  <option value="{{$ptype->id}}" {{ $count==1 ?"selected":""}} >{{$ptype->name}}</option> 
                @endif
              @endfor 
            @endforeach
          
          <!-- <option value="1">Deluxe</option> -->
        </select>
      </div>
    </div>
    <div class="main-block destination-block" id="packages">
      <div class="row">
        <div class="col-sm-12 col-md-4  no-pd-l">
          <div class="main-img destination-img"> 
            <a href="#"> <img src="{{  asset('storage/app/public/img/package').'/'.$iv->featured_image}}" class="img-responsive" alt="destination-img"/> </a>
          </div>
          <!-- end destination-img --> 
        </div>
        <!-- end columns -->
        <div class="col-sm-12 col-md-5  no-pd-r all-pack-info">
          <div class="destination-info">
            <div class="pack-inclusion-title">
              <h5>The Package Includes:</h5>
            </div>
            <!-- end destination-title --> 
          </div>
          <div class="inclusion-details">
            <div class="row package-inclu">
              <?php
                $inclusion_str = $iv->inclusion;
                $inclusion_ar = explode("-",$inclusion_str); 
              ?>
              @if(isset($inclusion_ar))
                @for($i=0;$i< count($inclusion_ar);$i++)
                  <div class="col-xs-3">
                    @if($inclusion_ar[$i]=="Flights")
                      <div class="inclusion-x Flights">
                        <i class="fa fa-plane"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Hotels")
                      <div class="inclusion-x hotels">
                        <i class="fa fa-hotel"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Meals")
                      <div class="inclusion-x Meals">
                        <i class="fa fa-cutlery"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Activities")
                      <div class="inclusion-x activities">
                        <i class="fa fa-camera-retro"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Transfers")
                      <div class="inclusion-x transfers">
                        <i class="fa fa-exchange"></i>
                      </div>
                    @endif
                  </div>
                @endfor
              @endif
              
            </div>
          </div>
          <div class="other-pack-info">
            <div class="row">
              <div class="col-md-6 col-xs-6">
                <div class="pack-other-title">
                  <h5>Duration</h5>
                  <div class="other-pack-info">
                    <?php echo ($iv->no_days)==0 ? " ": ($iv->no_days)." <i class='fa fa-moon-o'></i> ".($iv->no_days+1)." <i class='fa fa-sun-o'></i>"; ?>
                  </div>
                </div>
              </div>            
              <div class="col-md-6 col-xs-6">
                <div class="pack-other-title">
                  <h5>Destinations</h5>
                  <div class="other-pack-info destinations">
                    <?php
                      $destination_str=$iv->destination_id;  
                      $destination_explode = explode("-",$destination_str);
                      $dn=array();
                      $dest_ar=array();
                      for($i=0;$i<count($destination_explode);$i++)
                      { 
                        $dn=explode(",",$destination_explode[$i]);
                        $dest_ar[$i]=$dn[0];
                        if(isset($dn[1])&& $dn[1]!=null)
                        $dest_night[$i]=$dn[1];
                      }
                    ?>
                    <?php
                      for($j=0;$j<count($dest_ar);$j++)
                      {
                        foreach($destination as $dest)
                        {
                          if($dest_ar[$j]==$dest->id)
                          {
                            if($j>0)
                            {
                              echo " <i class='fa fa-arrow-right'></i>".$dest->title;
                              
                            }
                            else
                            {
                              echo $dest->title;
                            }
                          }
                        }
                      }
                    ?>
                  </div>
                </div>
              </div>
              <div class="hotel-star-rat">
                <span class="hotel-category-title">Hotel Category:</span>
                <span class="standard">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-o"></i>
                  <i class="fa fa-star-o"></i>
                </span>
                <span class="delux" style="display:none">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-o"></i>
                </span>
                <span class="premium" style="display:none">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                </span>
                
              </div>
            </div>
            <!-- end destination-title --> 
          </div>
          <!-- end destination-info --> 
        </div>
        <!-- end columns -->

        <!-- <input type="hidden" class="standard_price" value="{{$iv->standard_price}}">
        <input type="hidden" class="standard_off" value="{{$iv->standard_off}}">
        <input type="hidden" class="delux_price" value="{{$iv->delux_price}}">
        <input type="hidden" class="delux_off" value="{{$iv->delux_off}}">
        <input type="hidden" class="premium_price" value="{{$iv->premium_price}}">
        <input type="hidden" class="premium_off" value="{{$iv->premium_off}}"> -->
       
        <div class="col-sm-12 col-md-3 no-pd-r standard_price_section">

          @if($iv->standard_off ==''||$iv->standard_off==null)
            <div class="destination-title"> 
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i>{{$iv->standard_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
                  
                  <form id="package_form" method="post" action="{{route('frontend.package.single.booking')}}">
                            @csrf
                            <input type="hidden" name="package_id" value="{{$package_detail->id}}">
                            <input type="hidden" name="package_basefare" value="{{$iv->standard_price}}">
                            <input type="hidden" name="package_basefare_off" value="{{$iv->standard_off}}">
                            
                    </form>
                  
                  
              <a href="#" class="btn btn-orange book-online">Book Online</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-info">View Details</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>    
            </div>
            <!-- end destination-title --> <!-- </div> -->
            <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price">
              <del>{{$iv->standard_price}}</del>
              <span class="holC-discount-tag inlineB ng-binding">{{$iv->standard_off."%"}} off</span>
              <div class="destination-title">
                <a class="pack-cost" href="#">
                  <i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->standard_off/100)*$iv->standard_price;
                    $cost_val = $iv->standard_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a>
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="get" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package" value="{{$iv->slug}}">
                      @php
                                                          $packagePrice = 0;
                                                          $packageOff= 0;
                                                          if(isset($iv->standard_price)){
                                                          $packagePrice = $iv->standard_price;
                                                          $packageOff = $iv->standard_off;
                                                          }
                                                          elseif(isset($iv->delux_price)){
                                                          $packagePrice = $iv->delux_price;
                                                          $packageOff = $iv->delux_off;
                                                          }
                                                          elseif(isset($iv->premium_price)){
                                                          $packagePrice = $iv->premium_price;
                                                          $packageOff = $iv->premium_off;
                                                          }
                                                        @endphp
                 
                 <input type="hidden" name="package_price" value="{{$packagePrice}}">
                 <input type="hidden" name="package_price_off" value="{{$packageOff}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package -->
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                  <!-- end destination-info --> 
                <!-- </div> -->
                <!-- end columns --> 
          @endif
        </div>
        <div class="col-sm-12 col-md-3 no-pd-r delux_price_section" style="display:none">
          @if($iv->delux_off ==''||$iv->delux_off==null)
            <div class="destination-title">
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i> {{$iv->delux_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-orange">Book Online</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-info">View Details</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>    
            </div>
            <!-- end destination-title --> 
                  <!-- </div> -->
                  <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price"> <del>{{$iv->delux_price}}</del> <span class="holC-discount-tag inlineB ng-binding">{{$iv->delux_off."%"}} off</span>
              <div class="destination-title">
                <a class="pack-cost" href="#"><i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->delux_off/100)*$iv->delux_price;
                      $cost_val = $iv->delux_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a>
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="post" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package_id" value="{{$iv->id}}">
                 <input type="hidden" name="package_price" value="{{$iv->delux_price}}">
                 <input type="hidden" name="package_price_off" value="{{$iv->delux_off}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package --> 
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                  <!-- end destination-info --> 
                <!-- </div> -->
                <!-- end columns --> 
          @endif
        </div>
        <div class="col-sm-12 col-md-3 no-pd-r premium_price_section" style="display:none">
          @if($iv->premium_off ==''||$iv->premium_off==null)
            <div class="destination-title"> 
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i> {{$iv->premium_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-orange">Book Online</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-info">View Details</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>    
            </div>
            <!-- end destination-title --> 
                  <!-- </div> -->
                  <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price">
              <del>{{$iv->premium_price}}</del>
              <span class="holC-discount-tag inlineB ng-binding">{{$iv->premium_off."%"}} off</span>
              <div class="destination-title"> 
                <a class="pack-cost" href="#">
                  <i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->premium_off/100)*$iv->premium_price;
                    $cost_val = $iv->premium_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a> 
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="post" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package_id" value="{{$iv->id}}">
                 <input type="hidden" name="package_price" value="{{$iv->premium_price}}">
                 <input type="hidden" name="package_price_off" value="{{$iv->premium_off}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package --> 
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                <!-- end destination-info --> 
           <!-- </div>
           </div> -->
                <!-- end columns --> 
          @endif
        </div>
        {{ Form::close() }}
              <!-- end row --> 
      </div>
            <!-- end destination-block --> 
    </div>
    <!-- end package card -->
  </div>
  @endif
  @endforeach
  @endif <!--end destination wise package-->
  @if($source[0]=="theme")
  
   @if($iv->package_theme==$source[1])
  <div class="col-sm-6 col-md-12 package-card">
    <div class="row pack-header">
      <div class="col-md-9">
        <h2 class="packages-headline">{{strtoupper($iv->title)." - "}}{{ ($iv->no_days)==0 ? " ": ($iv->no_days)."Nights/" }}{{($iv->no_days+1)." Days"}} </h2>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 tour_type hidden-xs">
        <label class="packtype text-capitalize">tour type</label>
        <select class="package-class PackType">
          <?php 
          $package_type_str = $iv->package_type;
          $package_type_array = explode(",",$package_type_str);
          $count=0;
          //  var_dump($package_type_array);
          
          ?>
         
            @foreach($packagetype as $ptype)
              @for($j=0;$j < count($package_type_array);$j++)
                @if($ptype->id==$package_type_array[$j]) 
                  <?php $count++; ?> 
                  <option value="{{$ptype->id}}" {{ $count==1 ?"selected":""}} >{{$ptype->name}}</option> 
                @endif
              @endfor 
            @endforeach
          
          <!-- <option value="1">Deluxe</option> -->
        </select>
      </div>
    </div>
    <div class="main-block destination-block" id="packages">
      <div class="row">
        <div class="col-sm-12 col-md-4  no-pd-l">
          <div class="main-img destination-img"> 
            <a href="#"> <img src="{{  asset('storage/app/public/img/package').'/'.$iv->featured_image}}" class="img-responsive" alt="destination-img"/> </a>
          </div>
          <!-- end destination-img --> 
        </div>
        <!-- end columns -->
        <div class="col-sm-12 col-md-5  no-pd-r all-pack-info">
          <div class="destination-info">
            <div class="pack-inclusion-title">
              <h5>The Package Includes:</h5>
            </div>
            <!-- end destination-title --> 
          </div>
          <div class="inclusion-details">
            <div class="row package-inclu">
              <?php
                $inclusion_str = $iv->inclusion;
                $inclusion_ar = explode("-",$inclusion_str); 
              ?>
              @if(isset($inclusion_ar))
                @for($i=0;$i< count($inclusion_ar);$i++)
                  <div class="col-xs-3">
                    @if($inclusion_ar[$i]=="Flights")
                      <div class="inclusion-x Flights">
                        <i class="fa fa-plane"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Hotels")
                      <div class="inclusion-x hotels">
                        <i class="fa fa-hotel"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Meals")
                      <div class="inclusion-x Meals">
                        <i class="fa fa-cutlery"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Activities")
                      <div class="inclusion-x activities">
                        <i class="fa fa-camera-retro"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Transfers")
                      <div class="inclusion-x transfers">
                        <i class="fa fa-exchange"></i>
                      </div>
                    @endif
                  </div>
                @endfor
              @endif
              
            </div>
          </div>
          <div class="other-pack-info">
            <div class="row">
              <div class="col-md-6 col-xs-6">
                <div class="pack-other-title">
                  <h5>Duration</h5>
                  <div class="other-pack-info">
                    <?php echo ($iv->no_days)==0 ? " ": ($iv->no_days)." <i class='fa fa-moon-o'></i> ".($iv->no_days+1)." <i class='fa fa-sun-o'></i>"; ?>
                  </div>
                </div>
              </div>            
              <div class="col-md-6 col-xs-6">
                <div class="pack-other-title">
                  <h5>Destinations</h5>
                  <div class="other-pack-info destinations">
                    <?php
                      $destination_str=$iv->destination_id;  
                      $destination_explode = explode("-",$destination_str);
                      $dn=array();
                      $dest_ar=array();
                      for($i=0;$i<count($destination_explode);$i++)
                      { 
                        $dn=explode(",",$destination_explode[$i]);
                        $dest_ar[$i]=$dn[0];
                        if(isset($dn[1])&& $dn[1]!=null)
                        $dest_night[$i]=$dn[1];
                      }
                    ?>
                    <?php
                      for($j=0;$j<count($dest_ar);$j++)
                      {
                        foreach($destination as $dest)
                        {
                          if($dest_ar[$j]==$dest->id)
                          {
                            if($j>0)
                            {
                              echo " <i class='fa fa-arrow-right'></i>".$dest->title;
                              
                            }
                            else
                            {
                              echo $dest->title;
                            }
                          }
                        }
                      }
                    ?>
                  </div>
                </div>
              </div>
              <div class="hotel-star-rat">
                <span class="hotel-category-title">Hotel Category:</span>
                <span class="standard">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-o"></i>
                  <i class="fa fa-star-o"></i>
                </span>
                <span class="delux" style="display:none">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-o"></i>
                </span>
                <span class="premium" style="display:none">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                </span>
                
              </div>
            </div>
            <!-- end destination-title --> 
          </div>
          <!-- end destination-info --> 
        </div>
        <!-- end columns -->

        <!-- <input type="hidden" class="standard_price" value="{{$iv->standard_price}}">
        <input type="hidden" class="standard_off" value="{{$iv->standard_off}}">
        <input type="hidden" class="delux_price" value="{{$iv->delux_price}}">
        <input type="hidden" class="delux_off" value="{{$iv->delux_off}}">
        <input type="hidden" class="premium_price" value="{{$iv->premium_price}}">
        <input type="hidden" class="premium_off" value="{{$iv->premium_off}}"> -->
        <div class="col-sm-12 col-md-3 no-pd-r standard_price_section">
          @if($iv->standard_off ==''||$iv->standard_off==null)
            <div class="destination-title"> 
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i>{{$iv->standard_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-orange">Book Online</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-info">View Details</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>    
            </div>
            <!-- end destination-title --> <!-- </div> -->
            <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price">
              <del>{{$iv->standard_price}}</del>
              <span class="holC-discount-tag inlineB ng-binding">{{$iv->standard_off."%"}} off</span>
              <div class="destination-title">
                <a class="pack-cost" href="#">
                  <i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->standard_off/100)*$iv->standard_price;
                    $cost_val = $iv->standard_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a>
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="post" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package_id" value="{{$iv->id}}">
                 <input type="hidden" name="package_price" value="{{$iv->delux_price}}">
                 <input type="hidden" name="package_price_off" value="{{$iv->delux_off}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package --> 
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                  <!-- end destination-info --> 
                <!-- </div> -->
                <!-- end columns --> 
          @endif
        </div>
        <div class="col-sm-12 col-md-3 no-pd-r delux_price_section" style="display:none">
          @if($iv->delux_off ==''||$iv->delux_off==null)
            <div class="destination-title">
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i> {{$iv->delux_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-orange">Book Online</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-info">View Details</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>    
            </div>
            <!-- end destination-title --> 
                  <!-- </div> -->
                  <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price"> <del>{{$iv->delux_price}}</del> <span class="holC-discount-tag inlineB ng-binding">{{$iv->delux_off."%"}} off</span>
              <div class="destination-title">
                <a class="pack-cost" href="#"><i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->delux_off/100)*$iv->delux_price;
                      $cost_val = $iv->delux_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a>
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="post" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package_id" value="{{$iv->id}}">
                 <input type="hidden" name="package_price" value="{{$iv->delux_price}}">
                 <input type="hidden" name="package_price_off" value="{{$iv->delux_off}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package --> 
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                  <!-- end destination-info --> 
                <!-- </div> -->
                <!-- end columns --> 
          @endif
        </div>
        <div class="col-sm-12 col-md-3 no-pd-r premium_price_section" style="display:none">
          @if($iv->premium_off ==''||$iv->premium_off==null)
            <div class="destination-title"> 
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i> {{$iv->premium_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-orange">Book Online</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-info">View Details</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>    
            </div>
            <!-- end destination-title --> 
                  <!-- </div> -->
                  <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price">
              <del>{{$iv->premium_price}}</del>
              <span class="holC-discount-tag inlineB ng-binding">{{$iv->premium_off."%"}} off</span>
              <div class="destination-title"> 
                <a class="pack-cost" href="#">
                  <i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->premium_off/100)*$iv->premium_price;
                    $cost_val = $iv->premium_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a>
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="post" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package_id" value="{{$iv->id}}">
                 <input type="hidden" name="package_price" value="{{$iv->delux_price}}">
                 <input type="hidden" name="package_price_off" value="{{$iv->delux_off}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package --> 
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                <!-- end destination-info --> 
           <!-- </div>
           </div> -->
                <!-- end columns --> 
          @endif
        </div>
              <!-- end row --> 
      </div>
            <!-- end destination-block --> 
    </div>
    <!-- end package card -->
  </div>
  @endif
  
  @endif <!--end theme wise package-->

  @endif <!--end source_id -->
  @if(empty($source_id))
  <div class="col-sm-6 col-md-12 package-card">
    <div class="row pack-header">
      <div class="col-md-9">
        <h2 class="packages-headline">{{strtoupper($iv->title)." - "}}{{ ($iv->no_days)==0 ? " ": ($iv->no_days)."Nights/" }}{{($iv->no_days+1)." Days"}} </h2>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 tour_type hidden-xs">
        <label class="packtype text-capitalize">tour type</label>
        <select class="package-class PackType">
          <?php 
          $package_type_str = $iv->package_type;
          $package_type_array = explode(",",$package_type_str);
          $count=0;
          //  var_dump($package_type_array);
          
          ?>
         
            @foreach($packagetype as $ptype)
              @for($j=0;$j < count($package_type_array);$j++)
                @if($ptype->id==$package_type_array[$j]) 
                  <?php $count++; ?> 
                  <option value="{{$ptype->id}}" {{ $count==1 ?"selected":""}} >{{$ptype->name}}</option> 
                @endif
              @endfor 
            @endforeach
          
          <!-- <option value="1">Deluxe</option> -->
        </select>
      </div>
    </div>
    <div class="main-block destination-block" id="packages">
      <div class="row">
        <div class="col-sm-12 col-md-4  no-pd-l">
          <div class="main-img destination-img"> 
            <a href="#"> <img src="{{  asset('storage/app/public/img/package').'/'.$iv->featured_image}}" class="img-responsive" alt="destination-img"/> </a>
          </div>
          <!-- end destination-img --> 
        </div>
        <!-- end columns -->
        <div class="col-sm-12 col-md-5  no-pd-r all-pack-info">
          <div class="destination-info">
            <div class="pack-inclusion-title">
              <h5>The Package Includes:</h5>
            </div>
            <!-- end destination-title --> 
          </div>
          <div class="inclusion-details">
            <div class="row package-inclu">
              <?php
                $inclusion_str = $iv->inclusion;
                $inclusion_ar = explode("-",$inclusion_str); 
              ?>
              @if(isset($inclusion_ar))
                @for($i=0;$i< count($inclusion_ar);$i++)
                  <div class="col-xs-3">
                    @if($inclusion_ar[$i]=="Flights")
                      <div class="inclusion-x Flights">
                        <i class="fa fa-plane"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Hotels")
                      <div class="inclusion-x hotels">
                        <i class="fa fa-hotel"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Meals")
                      <div class="inclusion-x Meals">
                        <i class="fa fa-cutlery"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Activities")
                      <div class="inclusion-x activities">
                        <i class="fa fa-camera-retro"></i>
                      </div>
                    @endif
                    @if($inclusion_ar[$i]=="Transfers")
                      <div class="inclusion-x transfers">
                        <i class="fa fa-exchange"></i>
                      </div>
                    @endif
                  </div>
                @endfor
              @endif
              
            </div>
          </div>
          <div class="other-pack-info">
            <div class="row">
              <div class="col-md-6 col-xs-6">
                <div class="pack-other-title">
                  <h5>Duration</h5>
                  <div class="other-pack-info">
                    <?php echo ($iv->no_days)==0 ? " ": ($iv->no_days)." <i class='fa fa-moon-o'></i> ".($iv->no_days+1)." <i class='fa fa-sun-o'></i>"; ?>
                  </div>
                </div>
              </div>            
              <div class="col-md-6 col-xs-6">
                <div class="pack-other-title">
                  <h5>Destinations</h5>
                  <div class="other-pack-info destinations">
                    <?php
                      $destination_str=$iv->destination_id;  
                      $destination_explode = explode("-",$destination_str);
                      $dn=array();
                      $dest_ar=array();
                      $dest_night=array();
                      //print_r($destination_str);
                      for($i=0;$i<count($destination_explode);$i++)
                      { 
                        $dn=explode(",",$destination_explode[$i]);
                        $dest_ar[$i]=$dn[0];
                        if(isset($dn[1])&& $dn[1]!=null)
                        $dest_night[$i]=$dn[1];
                      }
                    ?>
                    <?php
                      for($j=0;$j<count($dest_ar);$j++)
                      {
                        foreach($destination as $dest)
                        {
                          if($dest_ar[$j]==$dest->id)
                          {
                            if($j>0)
                            {
                              echo " <i class='fa fa-arrow-right'></i>".$dest->title;
                              
                            }
                            else
                            {
                              echo $dest->title;
                            }
                          }
                        }
                      }
                    ?>
                  </div>
                </div>
              </div>
              <div class="hotel-star-rat">
                <span class="hotel-category-title">Hotel Category:</span>
                <span class="standard">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-o"></i>
                  <i class="fa fa-star-o"></i>
                </span>
                <span class="delux" style="display:none">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star-o"></i>
                </span>
                <span class="premium" style="display:none">
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                </span>
                
              </div>
            </div>
            <!-- end destination-title --> 
          </div>
          <!-- end destination-info --> 
        </div>
        <!-- end columns -->

        <!-- <input type="hidden" class="standard_price" value="{{$iv->standard_price}}">
        <input type="hidden" class="standard_off" value="{{$iv->standard_off}}">
        <input type="hidden" class="delux_price" value="{{$iv->delux_price}}">
        <input type="hidden" class="delux_off" value="{{$iv->delux_off}}">
        <input type="hidden" class="premium_price" value="{{$iv->premium_price}}">
        <input type="hidden" class="premium_off" value="{{$iv->premium_off}}"> -->
        <div class="col-sm-12 col-md-3 no-pd-r standard_price_section">
          @if($iv->standard_off ==''||$iv->standard_off==null)
            <div class="destination-title"> 
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i>{{$iv->standard_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-orange">Book Online</a>
              </div>
              <!-- <a href="{{route('frontend.customize_package')}}" class="btn btn-info">View Details</a> -->
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>
            </div>
            <!-- end destination-title --> <!-- </div> -->
            <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price">
              <del>{{$iv->standard_price}}</del>
              <span class="holC-discount-tag inlineB ng-binding">{{$iv->standard_off."%"}} off</span>
              <div class="destination-title">
                <a class="pack-cost" href="#">
                  <i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->standard_off/100)*$iv->standard_price;
                    $cost_val = $iv->standard_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a>
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="post" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package_id" value="{{$iv->id}}">
                 <input type="hidden" name="package_price" value="{{$iv->delux_price}}">
                 <input type="hidden" name="package_price_off" value="{{$iv->delux_off}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package --> 
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                  <!-- end destination-info --> 
                <!-- </div> -->
                <!-- end columns --> 
          @endif
        </div>
        <div class="col-sm-12 col-md-3 no-pd-r delux_price_section" style="display:none">
          @if($iv->delux_off ==''||$iv->delux_off==null)
            <div class="destination-title">
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i> {{$iv->delux_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-orange">Book Online</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-info">View Details</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>    
            </div>
            <!-- end destination-title --> 
                  <!-- </div> -->
                  <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price"> <del>{{$iv->delux_price}}</del> <span class="holC-discount-tag inlineB ng-binding">{{$iv->delux_off."%"}} off</span>
              <div class="destination-title">
                <a class="pack-cost" href="#"><i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->delux_off/100)*$iv->delux_price;
                      $cost_val = $iv->delux_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a>
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="post" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package_id" value="{{$iv->id}}">
                 <input type="hidden" name="package_price" value="{{$iv->delux_price}}">
                 <input type="hidden" name="package_price_off" value="{{$iv->delux_off}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package --> 
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                  <!-- end destination-info --> 
                <!-- </div> -->
                <!-- end columns --> 
          @endif
        </div>
        <div class="col-sm-12 col-md-3 no-pd-r premium_price_section" style="display:none">
          @if($iv->premium_off ==''||$iv->premium_off==null)
            <div class="destination-title"> 
              <a class="pack-cost" href="#">
                <i class="fa fa-lg fa-inr" aria-hidden="true"></i> {{$iv->premium_price}}
              </a>
              <p>Per person from New Delhi</p>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-orange">Book Online</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn btn-info">View Details</a>
              </div>
              <div class="col-md-12 col-xs-4">
              <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
              </div>    
            </div>
            <!-- end destination-title --> 
                  <!-- </div> -->
                  <!-- end destination-info -->
          @else
            <div class="destination-info" id="destination-price">
              <del>{{$iv->premium_price}}</del>
              <span class="holC-discount-tag inlineB ng-binding">{{$iv->premium_off."%"}} off</span>
              <div class="destination-title"> 
                <a class="pack-cost" href="#">
                  <i class="fa fa-lg fa-inr" aria-hidden="true"></i> 
                  <?php $offer_val = ($iv->premium_off/100)*$iv->premium_price;
                    $cost_val = $iv->premium_price-$offer_val;
                  ?>
                  {{round($cost_val,0)}}
                </a>
                <p>Per person from New Delhi</p>
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn btn-orange">Book Online</a>
                </div>
                <!-- set fields of package -->
                <div class="col-md-12 col-xs-4">
                <form method="post" action="{{route('frontend.single_package')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="package_id" value="{{$iv->id}}">
                 <input type="hidden" name="package_price" value="{{$iv->delux_price}}">
                 <input type="hidden" name="package_price_off" value="{{$iv->delux_off}}">
                 <?php $dates = array(date("d/m/Y", strtotime("+30 days"))); 
                        $count_date=1;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{date('d/m/Y', strtotime('+30 days'))}}">
                 
                 @foreach($dest_night as $destnight)
                 <?php 
                      $d=explode('/',$dates[$count_date-1]);
                      $date=$d[2].'-'.$d[1].'-'.$d[0];
                      //$day=$search_info['NoOfNights'];
                      
                      $date=new DateTime($date);
                      $check_out= $date->add(new DateInterval('P'.$destnight.'D'))->format('d/m/Y');




                        // $next_date=strtotime('+2 days',strtotime($dates[$count_date-1]));
                         $dates[$count_date]=$check_out;
                 ?>
                 <input type="hidden" name="depart_date[]"  class="depart_date" value="{{$check_out}}">
                 <input type="hidden"  name="no_of_nights[]" value="{{$destnight}}"/>
                 <?php $count_date++; ?>
                 @endforeach
                 

                 <input type="hidden" id="origin" name="destination[]" value="Delhi,DL,India"/>


                @for($j=0;$j < count($dest_ar);$j++)                      
                  @foreach($destination as $dest)                        
                    @if($dest_ar[$j]==$dest->id)                          
                      <input type="hidden" id="origin" name="destination[]" value="{{$dest->title.','.$dest->flight_citycode.','.$dest->hotel_citycode}}"/>
                    @endif
                  @endforeach
                @endfor
                
                
                
                 
                <input type="submit" class="btn btn-info" name="submit" value="View Details">
                </form>
                </div>
                <!-- end:# set fields of package --> 
                <div class="col-md-12 col-xs-4">
                <a href="#" class="btn" data-toggle="modal" data-target="#myModal" style="border: 1px solid;color: #3f51b5;border-radius: 23px;"> Customize</a>
                </div>
              </div>
                    <!-- end destination-title --> 
            </div>
                <!-- end destination-info --> 
           <!-- </div>
           </div> -->
                <!-- end columns --> 
          @endif
        </div>
              <!-- end row --> 
      </div>
            <!-- end destination-block --> 
    </div>
    <!-- end package card -->
  </div>
  @endif <!-- end default view -->
  @endforeach
  @endif
          <!-- end columns --> 
           
           <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
          <!-- end columns --> 
          <!-- end columns -->
          
     
          
          <div class="pages">
            <ol class="pagination">
              <li><a href="#" aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#" aria-label="Next"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>
            </ol>
          </div>
          <!-- end pages --> 
        </div>
        <!-- end columns --> 
        
      </div>
      <!-- end row --> 
    </div>
    <!-- end container --> 
  </div>
  <!-- end hotel-listing --> 
</section>
<!-- end innerpage-wrapper --> 
<!--=============== packages ==============--> 
@include('frontend.includes.footer')
<script>
    $('.select2').select2();
</script>
<script type="text/javascript">
  var vm = new Vue({
  el: document.body,
  data: {
    a: 0,
    b: 1
  }
})
</script>
<script>
$(document).ready(function(){
  //By default hotel category as per first package type
  $(".PackType").each(function(){
    let pack_type = $(this).find("option:selected").text();
    if(pack_type=="Standard")
    {
      $(this).parent().parent().next().children().children(".standard_price_section").show();
      $(this).parent().parent().next().children().children(".delux_price_section").hide();
      $(this).parent().parent().next().children().children(".premium_price_section").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".delux").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".standard").show();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".premium").hide();
    }
    if(pack_type=="Delux")
    {
      $(this).parent().parent().next().children().children(".standard_price_section").hide();
      $(this).parent().parent().next().children().children(".delux_price_section").show();
      $(this).parent().parent().next().children().children(".premium_price_section").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".delux").show();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".standard").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".premium").hide();
    }
    if(pack_type=="Premium")
    {
      $(this).parent().parent().next().children().children(".standard_price_section").hide();
      $(this).parent().parent().next().children().children(".delux_price_section").hide();
      $(this).parent().parent().next().children().children(".premium_price_section").show();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".delux").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".standard").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".premium").show();
    }
  });
  
  //hotel category as per the change of package type
  $(document).on("change",".PackType",function(){
    let pack_type=$(this).find("option:selected").text();
    // or use this script(let selected_val=this.options[this.selectedIndex].text;)
    if(pack_type=="Standard")
    {
      $(this).parent().parent().next().children().children(".standard_price_section").show();
      $(this).parent().parent().next().children().children(".delux_price_section").hide();
      $(this).parent().parent().next().children().children(".premium_price_section").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".delux").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".standard").show();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".premium").hide();
    }
    if(pack_type=="Delux")
    {
      $(this).parent().parent().next().children().children(".standard_price_section").hide();
      $(this).parent().parent().next().children().children(".delux_price_section").show();
      $(this).parent().parent().next().children().children(".premium_price_section").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".delux").show();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".standard").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".premium").hide();
    }
    if(pack_type=="Premium")
    {
      $(this).parent().parent().next().children().children(".standard_price_section").hide();
      $(this).parent().parent().next().children().children(".delux_price_section").hide();
      $(this).parent().parent().next().children().children(".premium_price_section").show();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".delux").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".standard").hide();
      $(this).parent().parent().next().children().children().next().children(".other-pack-info").children().children(".hotel-star-rat").children(".premium").show();
    }
  });

 });
</script>

  	  <script>
	  $(document).on('click','.book-online',function(){
		 $('#package_form')[0].submit(); 
	  });
	  </script>
