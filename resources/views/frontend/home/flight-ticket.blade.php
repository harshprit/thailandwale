<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>A simple, clean, and responsive HTML invoice template</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 14px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .fl-right {
    text-align: right;
}

.footer-support td {
    text-align: center !important;
}

.fl-center {
    text-align: center !important;
    width: 10% !important;
}

.travelly td {
    text-align: left !important;
}

.tip strong {
    margin-right: 10px;
}

.important strong {
    margin-right: 10px;
}

h2.flight-blabla-main {
    margin: 0px;
    font-size: 30px;
    line-height: 35px;
}

p.flight-airport-name {
    margin: 0px;
}

p.tototo {
    margin: 0px;
}

p.tmtmtm {
    margin: 0px;
}

p.clclcl {
    margin: 0px;
}

table.flight-blabla {
    margin: 20px auto;
}

    h2.flight-tofrom {
    margin: 0px;
}

h2.bokd {
    margin: 0px;
    font-size: 14px;
}

td.flight-blabla-td {
    width: 33%;
}

span.date-flight {
    font-size: 12px;
    margin-left: 10px;
}
    
    

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoicy td:nth-child(2) {
    min-width: 100px;
    width: 130px !important;
}

.invoicy td:nth-child(4) {
    min-width: 100px;
    width: 130px;
}

.invoicy td:nth-child(3) {
    min-width: 100px;
    width: 115px;
}
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
        font-size: 12px;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
       
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
               
                    
                        
                            <td class="title">
                                <img src="http://thailandwale.com/thailandwale/assets/home/images/logo-2.png" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
                               <h2 class="ticket-text">Ticket</h2>
                            </td>
                       
                    
              
            </tr>
        </table> 
        <hr>
        <table cellpadding="0" cellspacing="0"> 
            <tr class="information">
                
                            <td>
                                <h2 class="flight-tofrom">New Delhi to Bangkok <span class="date-flight">Tue, Sep 18 2018</span></h2>
                            </td>
                            
                            <td>
                                <h2 class="bokd">Booking ID: 89765467</h2>
                            </td>
                        
            </tr>
        </table>  
        <hr>
        <table cellpadding="0" cellspacing="0">
            <tr class="flight-name">
                
                            <td>
                                <h2 class="flight-name-code">SpiceJet SG 894</h2>
                            </td>
                            
                            <td>
                                
                            </td>
                        
            </tr>

            
        </table>
        
        
                <table class="flight-blabla">
                    <tr>
                        <td class="flight-blabla-td fl-right">
                            <h2 class="flight-blabla-main">DEL, 18:15</h2>
                            <span class="date-flight-blabla">Tue, Sep 18 2018</span>
                            <p class="flight-airport-name">New Delhi - Indra Gandhi International Airport</p>
                        </td>
                        
                        <td class="flight-blabla-td fl-center">
                            <p class="tototo">to</p>
                            <p class="tmtmtm">2h 25m</p>
                            <p class="clclcl">Economy</p>
                        </td>

                        <td class="flight-blabla-td fl-left">
                            <h2 class="flight-blabla-main">20:50 BKK</h2>
                            <span class="date-flight-blabla">Tue, Sep 18 2018</span>
                            <p class="flight-airport-name">Bangkok - Suvarnabhumi Airport</p>
                        </td>
                    </tr>
                </table>
            
        <table class="travelly" cellpadding="0" cellspacing="0">   
            <tr class="heading">
                <td>
                    Travellers
                </td>
                
                <td>
                   PNR
                </td>

                <td>
                    Ticket No.
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Anupam Dixit
                </td>
                
                <td>
                    BDKINHG
                </td>

                <td>
                   KJDGVASHGFDS
                </td>

            </tr>
            
            
            
            
            
        </table>
        <hr>
        <p class="tip"><strong>Tip:</strong>Eat a balanced diet and get good sleep before your journey.</p>
        <hr>
        <p class="important"><strong>Important:</strong>Eat a balanced diet and get good sleep before your journey.</p>
        <hr>
        <h2 class="important-instructions">Important Instructions</h2>
        <p class="imp-p">Use Your PNR for all communications related to your flight.</p>
        <p class="imp-p">Use Your PNR for all communications related to your flight.</p>
        <p class="imp-p">Use Your PNR for all communications related to your flight.</p>
        <p class="imp-p">Use Your PNR for all communications related to your flight.</p>
        <p class="imp-p">Use Your PNR for all communications related to your flight.</p>
        <p class="imp-p">Use Your PNR for all communications related to your flight.</p>

        <hr>
        <table class="footer-support" cellpadding="0" cellspacing="0">   
            <tr class="footery">
                <td>
                    Thailandwale Support
                </td>
                
                <td>
                   Spicejet Helpline
                </td>

                <td>
                    Need a hotel?
                </td>
            </tr>
            
            <tr class="item">
                <td>
                   +91-9999999999
                </td>
                
                <td>
                    +91-9999999999
                </td>

                <td>
                    +91-9999999999
                </td>

            </tr>
            
            
            
            
            
        </table>
        <hr>
    </div>
</body>
</html>
