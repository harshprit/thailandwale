@include('frontend.includes.header')
@include('frontend.includes.customize-header')
@include('frontend.includes.nav1')

@if(Session::has('error'))
<div class=' alert alert-danger'>{{Session::get('error')}}</div>
@endif

<!-- Popup :  edit passenger details-->
@php

$activity_sum=0;
if(!isset($package_cost))
{
    $package_cost=0;
}

@endphp
     <!-- #Popup :  edit passenger details-->
    
      <div class="theme-page-section theme-page-section-lg">
         <div class="container">
            <div class="row row-col-static row-col-mob-gap" id="sticky-parent" data-gutter="60">
               <div class="col-md-8 ">
                  <div class="theme-payment-page-sections">
                     <div class="fullimage">
                                             @if(isset($package_detail))
                        <img src="{{url('storage/app/public/img/package').'/'.$package_detail->featured_image}}" class="img-responsive">
                     @else
                        <img src="{{asset('assets/home/customize/images/pack.jpg')}}" class="img-responsive">
                     @endif
                                          </div>
      <h3 class="itenary-headi">{{$package_detail->title}}</h3>
<ul class="nav nav-tabs nav-fill sing-nav-tab" id="myTab" role="tablist">
  <li class="nav-item active">
    <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" aria-expanded="false">Itinerary</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" aria-expanded="true">Inclusions</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false" aria-expanded="false">Hotels</a>
  </li><li class="nav-item">
    <a class="nav-link" id="flight-tab" data-toggle="tab" href="#flight" role="tab" aria-controls="flight" aria-selected="false" aria-expanded="false">Flights</a>
  </li><li class="nav-item">
    <a class="nav-link" id="tandc-tab" data-toggle="tab" href="#tandc" role="tab" aria-controls="tandc" aria-selected="false" aria-expanded="false">T &amp; Cs</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="fi-tab" data-toggle="tab" href="#fi" role="tab" aria-controls="fi" aria-selected="false" aria-expanded="false">Full Itinerary</a>
  </li>
</ul>

<div class="tab-content sing-tab-con" id="myTabContent">
  <div class="tab-pane fade active in" id="home" role="tabpanel" aria-labelledby="home-tab">
                        @php

                            if(!empty($package_detail->day_title))
                            {

                            $explode_data = explode(',',$package_detail->day_title);
                            $explode_data1 = explode(',',$package_detail->day_destination);
                            $explode_data2 = explode('---',$package_detail->day_description);
                            for($i=0;$i < count($explode_data);$i++)
                            {
                            echo " <button type='button' class='accordion sing-accord'>".$explode_data[$i]."</button><div class='panel sing-pan'>" ;


                            echo" <div class='form-group'>

                            <div class='col-lg-10'>
                            ".$explode_data1[$i]."
                            </div>
                            </div>";

                            echo" <div class='form-group'>

                            <div class='col-lg-10 mce-box'>

                            ".$explode_data2[$i]."

                            </div>
                            </div>";

                            echo "</div>";
                            }
                            }

                        @endphp
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

      @php

      $incs=explode('-',$package_detail->inclusion);
      $details=json_decode($package_detail->inclusion_details);
      
       echo $package_detail->content; 
      
      @endphp
   
     </div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
      @isset($details->hotels)
      @php echo $details->hotels; @endphp
      @endisset
  </div><div class="tab-pane fade" id="flight" role="tabpanel" aria-labelledby="flight-tab">
      @isset($details->flights)
      @php echo $details->flights; @endphp
      @endisset
      </div>
      <div class="tab-pane fade" id="tandc" role="tabpanel" aria-labelledby="tandc-tab">@php echo $package_detail->terms_and_conditions @endphp</div>
      <div class="tab-pane fade active" id="fi" role="tabpanel" aria-labelledby="fi-tab">
      
          @php

                            if(!empty($package_detail->day_title))
                            {

                            $explode_data = explode(',',$package_detail->day_title);
                            $explode_data1 = explode(',',$package_detail->day_destination);
                            $explode_data2 = explode('---',$package_detail->day_description);
                            for($i=0;$i < count($explode_data);$i++)
                            {
                            echo "<div class='sp-description'> <h3 class='day-head'>".$explode_data[$i]."</h3><span class='sp-descrp'>" ;


                            echo" <div class='sp-content'><h5 class='day-dest'>Location:

                            ".$explode_data1[$i]."</h5>
                            
                            </div>";

                            echo" <div class='sp-content'>

                            ".$explode_data2[$i]."

                            </div>";

                            echo "</span></div>";
                            }
                            }

                        @endphp
      </div>
</div>


                                            </div>
                                            
<!--New Design for Single Package-->

<div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-plus"></span> What is HTML?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>HTML stands for HyperText Markup Language. HTML is the standard markup language for describing the structure of web pages. <a href="https://www.tutorialrepublic.com/html-tutorial/" target="_blank">Learn more.</a></p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-plus"></span> What is Bootstrap?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Bootstrap is a sleek, intuitive, and powerful front-end framework for faster and easier web development. It is a collection of CSS and HTML conventions. <a href="https://www.tutorialrepublic.com/twitter-bootstrap-tutorial/" target="_blank">Learn more.</a></p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-plus"></span> What is CSS?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>CSS stands for Cascading Style Sheet. CSS allows you to specify various style properties for a given HTML element such as colors, backgrounds, fonts etc. <a href="https://www.tutorialrepublic.com/css-tutorial/" target="_blank">Learn more.</a></p>
                </div>
            </div>
        </div>
    </div>
<!--New Design for Single Package Ends-->
                                        </div>

               <div class="col-md-4 ">
                  <div class="sticky-col">
                     <div class="theme-sidebar-section _mb-10">
                     @php $count_destination=count($destination);
                            $count_iter=0;
                     @endphp
                        <h5 class="theme-sidebar-section-title">Itinerary - {{($count_destination-1)}} City</h5>
                        <div class="theme-sidebar-section-charges">
                           <div class="itenary-cities">
                            @foreach($destination as $key=>$dest)
                                @if($count_iter>0)
                                    @php $explode_dest=explode(',',$dest); @endphp
                                    @if($count_iter==1)
                                    {{$explode_dest[0].' '.$no_of_nights[$key-1].'N'}}
                                    @else
                                    {{', '.$explode_dest[0].' '.$no_of_nights[$key-1].'N'}}
                                    @endif
                                @endif
                                @php $count_iter++; @endphp
                            @endforeach
                           <!-- Bangkok 3N, Chiang Mai 4N -->
                           </div>
                        
                        <form id="package_form" method="post" action="{{route('frontend.package.single.pax.details')}}">
                            @csrf
                            <input type="hidden" name="package_id" value="{{$package_detail->slug}}">
                           <div class="theme-payment-page-sections-item">
                        <div class="theme-payment-page-booking">
                           <div class="theme-payment-page-booking-header">
                              <p class="theme-payment-page-booking-subtitle">By clicking book now button you agree with terms and conditions.</p>
                              <p class="theme-payment-page-booking-price">
                                <div class="destination-info" id="destination-price">
                                    @if(isset($package_cost_off) && $package_cost_off > 0)
                                    
                                    <del id="package_cost_without_off">{{$package_cost}}</del>

                                    <span class="holC-discount-tag inlineB ng-binding">{{$package_cost_off."%"}} off</span>
                                    @endif
                                    <div class="destination-title">
                                        <a class="pack-cost" href="#">
                                        <i class="fa fa-lg fa-inr" aria-hidden="true"></i>

                                        <?php
                                        $offer_val=0;
                                        if(isset($package_cost_off))
                                        {
                                            $offer_val=$package_cost_off;
                                            $cost_val=$package_cost;
                                            // $offer_val = ($package_cost_off/100)*$package_cost;
                                             $cost_val = $package_cost-($package_cost*$offer_val)/100;
                                        }
                                        else
                                        {
                                            $cost_val = $package_cost;
                                        }

                                        ?>
                                        <span id="package_total_cost">
                                           {{$cost_val}}
                                        </span>
                                        <input type="hidden" name="package_basefare" value="{{$package_cost}}">
                                        <input type="hidden" name="package_basefare_off" value="{{$package_cost_off}}">
                                        </a>
                                        
                                        
                                        <div class="row">    
<label>Adults</label>
<select name="adults">
<option value="1" selected>1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">7</option>
</select>
<label>Children</label>
<select name="children">
<option value="0" selected>0</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
</select>    
</div>
                                        
                                        
                                        
                                        
                                        <p id="pax_details">1 Adult from {{$destination[0]}}</p>

                                    </div>
                                            <!-- end destination-title -->
                                </div>
                                <!-- <span class="theme-payment-page-booking-title">Total Price</span>1278.00 -->
                              </p>
                           </div>
                           <button id="customize_button" type="button" class="btn" data-toggle="modal" data-target="#package-edit"> Customize</button>
                           <a class="btn _tt-uc btn-primary-inverse btn-lg btn-block red_button" id="book_now" href="#">Book Now</a>
                        </div>
                     </div>
                     </form>
                     
                        </div>
                     </div>


                     <div class="theme-sidebar-section _mb-10">
                        <ul class="theme-sidebar-section-features-list">
                           <li>
                              <h5 class="theme-sidebar-section-features-list-title">Manage your bookings!</h5>
                              <p class="theme-sidebar-section-features-list-body">You're in control of your booking - no registration required.</p>
                           </li>
                           <li>
                              <h5 class="theme-sidebar-section-features-list-title">Customer support available 24/7 worldwide!</h5>
                              <p class="theme-sidebar-section-features-list-body">For Hassle Free Bookings</p>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     
	  

      @include('frontend.includes.footer')
      @include('frontend.includes.customize-footer')
      <script>
      
      $('select[name="adults"]').on('change',function(){
          
          let adults = parseInt($(this).val());
          let children = parseInt($('select[name="children"]').val());
          let cost_without_discount = {{$package_cost}};
          let cost = {{$cost_val}};
          let new_cost = (adults+children)*cost;
          let new_cost_without_discount = (adults+children)*cost_without_discount;
          $('#package_total_cost').html(new_cost);
          $('input[name="package_basefare"]').val(new_cost)
          $('#package_cost_without_off').html(new_cost_without_discount);
          $('input[name="package_basefare_off"]').val(new_cost_without_discount)
          
        if(children > 0)  {
            
            $('#pax_details').html(adults +" Adult(s), "+ children + " Children from "+ "{{$destination[0]}}");
        }
        else{
        $('#pax_details').html($(this).val() +" Adult(s)" + "{{$destination[0]}}");    
        }
        
        });
        
        $('select[name="children"]').on('change',function(){
          
          let adults = parseInt($('select[name="adults"]').val());
          let children = parseInt($(this).val());
          let cost_without_discount = {{$package_cost}};
          let cost = {{$cost_val}};
          let new_cost = (adults+children)*cost;
          let new_cost_without_discount = (adults+children)*cost_without_discount;
          $('#package_total_cost').html(new_cost);
          $('#package_cost_without_off').html(new_cost_without_discount);
          
        if(children > 0)  {
            
            $('#pax_details').html(adults +" Adult(s), "+ children + " Children from "+ "{{$destination[0]}}");
        }
        else{
        $('#pax_details').html($(this).val() +" Adult(s) from "+ "{{$destination[0]}}");    
        }
        
        });
      
      </script>
      <script>
          function calculatePackageFare()
          {
              var flight_price="";
              var hotel_price="";
              var activity_price="";
              var base_price="";
          }
      </script>
       <script>
           $(document).ready(function(){
               calculatePackageFare();
               function calculatePackageFare()
               {

                   var packageBasePrice = $("input[name='package_basefare']").val();
                   var packageOff = $("input[name='package_basefare_off']").val();
                   var packageOffValue= (parseInt(packageOff)/100)*parseInt(packageBasePrice);
                   packageTotalPrice = parseInt(packageBasePrice));
                   alert(packageTotalPrice);
                   packageTotalPriceWithOffer = parseInt(packageBasePrice)-parseInt(packageOffValue);
                   //alert(packageTotalPrice);
                   $("#package_total_cost").text(packageTotalPriceWithOffer);
                   $("#package_cost_without_off").text(packageTotalPrice);
               }



           });
     </script>
     	  <script>
	  $(document).on('click','#book_now',function(){
		 $('#package_form')[0].submit(); 
	  });
	  </script>
     <script>
     $("#included_flight").on("click",".flight_details",function(e){
    e.preventDefault();
    $(this).next(".flight_tipTip_cust_card").show(function(){$(this).focus();});
});
$(document).on('blur','.flight_tipTip_cust_card',function(){
    $(this).hide();
});

     </script>


    <script>
        var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}
    </script>

    <script>
    $(window).click(function(event) {
  if (!$(event.target).closest(".modal,.js-open-modal").length) {
    $("body").find(".modal").removeClass("visible");
  }
});

    </script>

   </body>
