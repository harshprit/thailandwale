@include('frontend.includes.header2')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')
@if(Session::has('msg'))
<div class=' alert alert-danger'>{{Session::get('msg')}}</div>
@endif

@if(!empty($errors->first()))
<div class=' alert alert-danger'>{{$errors->first()}}</div>
@endif

<style>
#airline_name{
     padding-left: 20px;
    font-weight: 600;
    font-size: smaller;   
 }
#airline_code{
  padding-left: 20px;
    font-size: smaller;   
 }
table tr>td{
     vertical-align:middle!important;
     text-align:center;
 }

input{
     color: indianred;
 }
/*.error_msg{*/
/*    font-size:12px;*/
/*    color:#e53935;*/
/*    display:none*/
/*}*/
/*.error{*/
/*    color:#ff0d08;*/
/*}*/
</style>
<!--@if($errors->any())-->
<!--<h4>{{$errors->first()}}</h4>-->
<!--@endif-->
<div class="pasflightdet">
<form id="PassengerDetail" name="PassengerDetail" action="{{route('frontend.package.single.booking')}}" method="post" autocomplete="off" novalidate="novalidate" >

    {{ csrf_field() }}
<input type="hidden" name="_token" value="{{ csrf_token() }}">
@php 
$inclusion = json_decode($package->inclusion_details);


@endphp
<div class="row flight-pasdetails">
      <div id="flight_details">
                
        
    </div>
	<div class="rework-az col-md-9" id="mobile_pax_detail">
	                <div class="htd_heading" style="margin-bottom: 0px; margin-top: 20px;">
            <h3 class="fpf-headings">Your Included Hotels</h3>
            </div>    
                        @if(isset($inclusion->hotels))
                        <div class="hotel-details-package">
<span class="hotel-name-p"><?php echo $inclusion->hotels;?></span>

                        </div>
                        @endif
                        <!-- mobile pax top container start-->

                 
                

            <div class="htd_heading" style="margin-bottom: 0px;  margin-top: 20px;">
            <h3 class="fpf-headings">Your Included Flights</h3>
            </div>
            @if(isset($inclusion->flights))
            <div class="flight-details-p">
    <?php echo $inclusion->flights; ?>
</div>
@endif

        <?php
    
        
        
        $infant_count =0; 
        $pax_type=1;
        
        $passenger_count=$adult_count+$child_count+$infant_count;
         
        
        
        ?>
        <input type="hidden" value="{{$adult_count}}" id="adult_count">
		<?php $tab=0; ?>
	    @for($i=1;$i<=$passenger_count;$i++)
	        
			 <div class="htd_heading"><h3 class="fpf-headings">Passenger {{$i}} - <span>
			     @if($i<=$adult_count)
			    (Adult {{$i}})
				
			    @elseif($i<=($adult_count+$child_count))
			   <?php $c=1; ?>
			    (Child {{$c}})
				
			  <?php  $c++;;; ?>
			  @elseif($i<=($adult_count+$child_count+$infant_count))
                <?php $inf = 1; ?>			  
			    (Infant {{$inf}})
				
			    <?php $inf++; ?>
			    @endif
			 </span></h3>
			 </div>




			<?php $e = $i-1;  ?>
                   <div class="row">
                    <div class="col-md-6 ">
                        <label>First Name: <sup class="red font11">*</sup></label>                        
                        <code class="fly_br_rt i_pad68">
                        <select id="Title.{{$e}}" name="Title[]" class="title width60 fly_select mr5 error {{$errors->has('Title.'.$e)?'input-error':''}}" tabindex="{{$tab++}}" aria-required="true" aria-describedby="Title_0-error" required="required">
                            <option value="">Title</option>
                                <option value="Mr" <?php echo old('Title.'.$e)=="Mr"?"selected":""; ?>>Mr</option>
                            @if($pax_type==1)
                                <option value="Mrs" <?php echo old('Title.'.$e)=="Mrs"?"selected":""; ?>>Mrs</option>
                            @endif   
                                <option value="Ms" <?php echo old('Title.'.$e)=="Ms"?"selected":""; ?>>Ms</option>
                                <option value="Miss" <?php echo old('Title.'.$e)=="Miss"?"selected":""; ?>>Miss</option>   
                                <option value="Mstr" <?php echo old('Title.'.$e)=="Mstr"?"selected":""; ?>>Mstr</option>  
                        </select>
                        
                       
                       
                        <!--<span id="Title_0-error" class="error">Please select a valid title.</span>-->
                        <input class="fname  i_pad_59 error {{$errors->has('FirstName.'.$e)?'input-error':''}}" id="FirstName.{{$e}}"  name="FirstName[]"  type="text" tabindex="{{$tab++}}" value="{{old('FirstName.'.$e)}}" aria-required="true" aria-describedby="FirstName_0-error" required="required"><br/>
                         @if ($errors->has('Title.'.$e))
                            <!--<span id="mobileNo_0-error" class="error">{{'*'. $errors->first('Title.'.$e) }}</span>-->
                	        <!--<span class="text-danger">{{ $errors->first('Title.'.$e) }}</span>-->
                        @endif
                        @if ($errors->has('FirstName.'.$e))
                            <!--<span id="mobileNo_0-error" class="error">{{'*'. $errors->first('FirstName.'.$e) }}</span>-->
                	        <!--<span class="text-danger">{{ $errors->first('FirstName.'.$e) }}</span>-->
                        @endif
                        
                        <span class="fleft width_100" style="display:none">
                            <!-- <span style="display:none;">Please select a valid title.</span>-->
                            <!--<span id="erroFirstName_0" style="display:none;">Please select a valid First Name.</span>-->
                        </span>
                        </code>  
                    </div>
                    <div class="col-md-6 ">
                       <label>Last Name :<sup class="red font11">*</sup> </label>
                       <code class="fly_br_rt i_pad68">                             
                         <input id="LastName.{{$e}}" name="LastName[]" class="lname pdfinput error {{$errors->has('LastName.'.$e)?'input-error':''}}" type="text" tabindex="{{$tab++}}" value="{{old('LastName.'.$e)}}" aria-required="true" aria-describedby="LastName_0-error">
                        
                         <br/>
                         <!--<span id="erroLastName_0" style="display:none;">Please select a valid Last Name.</span>                              -->
                       </code>
                       @if ($errors->has('LastName.'.$e))
                             <!--<span id="mobileNo_0-error" class="error">{{'*'. $errors->first('LastName.'.$e) }}</span>-->
        	                <!--<span class="text-danger">{{ $errors->first('LastName.'.$e) }}</span>-->
                        @endif
                </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ">
                            <label>Gender: <sup class="red">*</sup></label>
                            <code class="code-ki-class">
                            <select id="Gender.{{$e}}" name="Gender[]" class="gender fly_select m_ valid {{$errors->has('Gender.'.$e)?'input-error':''}}" tabindex="{{$tab++}}" aria-required="true">
                                <option>Choose</option>
                                 <option value="1" <?php echo old('Gender.'.$e)==1?"selected":""; ?>>Male</option>
                                <option value="2" <?php echo old('Gender.'.$e)==2?"selected":""; ?>>Female</option>
                            </select><br/>

                             @if ($errors->has('Gender.'.$e))
                                 <!--<span id="mobileNo_0-error" class="error">{{ $errors->first('Gender.'.$e) }}</span>-->
        	                    <!--<span class="text-danger">{{ $errors->first('Gender.'.$e) }}</span>-->
                            @endif
                             <!--<span class="red font10 ml5" id="genErr_0" style="display:none;">Gender is mandatory.</span>-->
                            </code>
                        </div>
                        </div>
                        
                <div class="row">
                        @if($i==1)
                        <h3 class="hopdf only-twice">Contact Details</h3>
                         <div class="col-md-6 ">
                            <label>Mobile: <sup class="red font11">*</sup> </label>
                            <code class="code-ki-class">
                            <input id="mobileNo" name="mobileNo" type="text" class="error  mobile_98 mobileNOClass {{$errors->has('mobileNo')?'input-error':''}}" tabindex="{{$tab++}}" value="{{old('mobileNo')}}" aria-required="true" aria-describedby="mobileNo_0-error"><br/>
                            @if ($errors->has('mobileNo'))
                                <!--<span id="mobileNo_0-error" class="error">{{$errors->first('mobileNo')}}</span>-->
        	                   <!--<span class="text-danger">{{$errors->first('mobileNo')}}</span>-->
        	                @endif
                            <!--<span id="mobileNo_0-error" class="error">Please enter Mobile number</span>  -->
                            </code>
                            
                        </div>
                         <div class="col-md-6 ">
                         
                         <label>Email :<sup class="red font11">*</sup></label>
                                                     
                            <code class="code-ki-class">
                               <input id="email" name="email" type="text" class="emailClass pdfinput error {{$errors->has('email')?'input-error':''}}" tabindex="{{$tab++}}" value="{{old('email')}}" aria-required="true" aria-describedby="email-error"></br>
                               @if ($errors->has('email'))
                                    <!--<span id="mobileNo_0-error" class="error">{{$errors->first('email')}}</span>-->
        	                   <!--<span class="text-danger">{{$errors->first('email')}}</span>-->
        	                   @endif
                               <!--<span id="email-error" class="error">This field is required.</span>-->
                                <!--<span class="red font11" id="emailerg" style="display:none;">Please Enter valid email.</span>-->
                            </code>
                             
                         </div>
                                             
                        @endif
                    </div>
                    
                   <div class="row">
                       @if($i==1)
                        <h3 class="hopdf only-twice">Personal Details</h3>
                        @endif
                        <div class="col-md-6 ">
                        <label>D.O.B :<sup class="red font11">*</sup></label> 
                            <code class="code-ki-class">
                            <select id="DobDay.{{$e}}" name="DobDay[]" data-pax-type="{{$pax_type}}" class="date_width fly_select {{$errors->has('DobDay.'.$e)?'input-error':''}}" tabindex="{{$tab++}}" required="required"> 
                                 <option value="">Day</option>
									@for($day=1;$day<32;$day++)
                                    <option value="{{$day}}" <?php echo old('DobDay.'.$e)==$day?"selected":""; ?>>{{$day}}</option>
									@endfor
                                    
                            </select>
                            
                            <select id="DobMonth.{{$e}}" name="DobMonth[]" data-pax-type="{{$pax_type}}" class="date_width fly_select {{$errors->has('DobMonth.'.$e)?'input-error':''}}" tabindex="{{$tab++}}" required="required">
                                   <option value="">Month</option>
                                
                                   @for($month=1;$month<=12;$month++)

                                    <option value="{{$month}}" <?php echo old('DobMonth.'.$e)==$month?"selected":""; ?>>{{date('F', mktime(0,0,0,$month, 1, date('Y')))}}</option>
                                  @endfor                                                        
                            </select>
                           
                                 <select id="DobYear.{{$e}}" name="DobYear[]" data-pax-type="{{$pax_type}}" class="adultdob date_width fly_select error {{$errors->has('DobYear.'.$e)?'input-error':''}}" tabindex="{{$tab++}}" required="required" aria-required="true" aria-describedby="adultDobYear_0-error">
                             
                                   <option value="">Year</option>
                                   <?php 
                                   if($i<=$adult_count){
                                      
                                    $min_age=idate('Y', strtotime('-12 years'));
                                    $max_age = 1875;
                                  }
                                  elseif($i<=($adult_count+$child_count)){
                                   $current_date = date("Y-m-d");
                                    $min_age=idate('Y', strtotime('-2 years'));
                                    $max_age=$min_age-12;
                                  }
                                   elseif($i<=($adult_count+$child_count+$infant_count)){
                                   
                                    $min_age=idate("Y");
                                    $max_age=$min_age-2;
                                  }
                                   ?>
                                  @for($y=$min_age;$y>$max_age;$y--)
                                    <option value="{{$y}}" <?php echo old('DobYear.'.$e)==$y?"selected":""; ?>>{{$y}}</option>
                                @endfor
                                    
                                
                            </select>
                        
                        <br/>
                            @if ($errors->has('DobDay.'.$e)||$errors->has('DobMonth.'.$e)||$errors->has('DobYear.'.$e))
                                @if ($errors->has('DobMonth.'.$e)||$errors->has('DobYear.'.$e))
                                    @if ($errors->has('DobYear.'.$e))
                                        <!--<span id="mobileNo_0-error" class="error">{{"Enter valid Date of birth"}}</span>-->
        	                           <!--<span class="text-danger">{{"Enter valid Date of birth"}}</span>-->
        	                        @else
        	                           <!--<span id="mobileNo_0-error" class="error">{{"Enter valid Date of birth"}}</span>-->
                                    @endif
                                @else 
        	                        <!--<span id="mobileNo_0-error" class="error">{{"Enter valid Date of birth"}}</span>-->
                                @endif
        	                @endif
        	           
                             
                            <!--<span id="adultDobYear_0-error" class="error">Date of birth is mandatory.</span>-->
                            <!--<span class="red font11" id="DobError_0" style="display:none;">Date of birth is mandatory.</span>-->
                            <!-- <span class="red font11" id="pDOBA_0" style="display:none;">Date of birth is mandatory with passport number.</span>-->
                            </code>
                        </div>
                         
                     @if($i==1)
                       <div class="col-md-6 ">
                       
                         <label>Address Line 1:<sup class="red font11">*</sup></label>
                       
                          
                            <code class="code-ki-class">
                                <input id="addressLine1" name="addressLine1" type="text" class="pdfinput valid {{$errors->has('addressLine1')?'input-error':''}}" tabindex="{{$tab++}}" value="{{old('addressLine1')}}" aria-required="true"><br/>
                                    @if ($errors->has('addressLine1'))
                                    <!--<span id="mobileNo_0-error" class="error">{{$errors->first('addressLine1')}}</span>-->
            	                    <!--<span class="text-danger">{{$errors->first('addressLine1')}}</span>-->
            	                    @endif
                            </code>
                           
                            <!--<div class="red font11" id="erradd1" style="display:none">Special Character (\) not allowed in Address</div>-->
                        </div>
                        <div class="col-md-6 ">
                          <label>Address Line 2:</label>
                            <code class="code-ki-class">
                              <input id="addressLine2" name="addressLine2" type="text" class="address_98 {{$errors->has('addressLine2')?'input-error':''}}" tabindex="{{$tab++}}" value="{{old('addressLine2')}}"><br/>
                              @if ($errors->has('addressLine2'))
                                <!--<span id="mobileNo_0-error" class="error">{{$errors->first('addressLine2')}}</span>-->
        	                   <!--<span class="text-danger">{{$errors->first('addressLine2')}}</span>-->
        	                  @endif
                            </code>
                           
                          <!--<div class="red font11" id="erradd2" style="display:none">Special Character (\) not allowed in Address</div>-->
                        </div>
                        <div class="col-md-6 ">
                           <label id="lblCity">City :</label>
                          <code class="code-ki-class"><input id="City" name="City" type="text" class="pdfinput  valid {{$errors->has('city')?'input-error':''}}" tabindex="{{$tab++}}" value="{{old('City')}}"></code><br/>
                           @if ($errors->has('City'))
                                <!--<span id="mobileNo_0-error" class="error">{{$errors->first('City')}}</span>-->
        	                   <!--<span class="text-danger">{{$errors->first('City')}}</span>-->
        	               @endif
                        </div>
                       <div class="col-md-6 ">                  
                            <label>Country :</label>
                            <code class="code-ki-class">
                            <select id="Country.{{$e}}" name="Country[]" class="country  pdfselect fly_select valid {{$errors->has('Country')?'input-error':''}}" tabindex="{{$tab++}}" aria-required="true">
                                <option value="">Choose</option>
                                    @if(isset($countries))                                
                                        @foreach($countries as $ctr)
                                         <option value="{{$ctr->sortname}}" <?php echo old('Country.'.$e)==$ctr->sortname? "selected":""; ?>>{{$ctr->name}}</option>
                                        @endforeach
                                    @endif
                                         
                            </select><br/>
                            @if ($errors->has('Country.'.$e))
                                <!--<span id="mobileNo_0-error" class="error">{{$errors->first('Country.'.$e)}}</span>-->
        	                @endif
                            <!--<span style="display:none;">Please Select Country</span>-->
                            </code>
                         </div>
                       
                        
                         
                       @endif       
                    
                   </div>
                    
                   
                   
                     <div class="row">
                         <h3 class="hopdf">Passport Details</h3> 
                         @if($i==1)
                         <div class="col-md-6 ">
                            <label>Nationality: <sup class="red font11">*</sup></label>
                            <code class="code-ki-class">
                            <select id="Nationality.{{$e}}" name="Nationality[]" class="nationality  pdfselect fly_select " tabindex="{{$tab++}}">
                                        <option value="">Choose</option>
										
                                        <option value="AF"> Afghan</option>
                                    
                                        <option value="AL"> Albanian</option>
                                    
                                        <option value="DZ"> Algerian</option>
                                    
                                        <option value="AS"> American Samoan</option>
                                    
                                        <option value="US"> American; US citizen</option>
                                    
                                        <option value="AD"> Andorran</option>
                                    
                                        <option value="AO"> Angolan</option>
                                    
                                        <option value="AI"> Anguillan</option>
                                    
                                        <option value="AQ"> Antarctica</option>
                                    
                                        <option value="AG"> Antiguan; Barbudian</option>
                                    
                                        <option value="AN"> Antillean</option>
                                    
                                        <option value="AR"> Argentinian</option>
                                    
                                        <option value="AM"> Armenian</option>
                                    
                                        <option value="AW"> Aruban</option>
                                    
                                        <option value="AU"> Australian</option>
                                    
                                        <option value="AT"> Austrian</option>
                                    
                                        <option value="AZ"> Azerbaijani</option>
                                    
                                        <option value="BS"> Bahamian</option>
                                    
                                        <option value="BH"> Bahraini</option>
                                    
                                        <option value="BD"> Bangladeshi</option>
                                    
                                        <option value="BB"> Barbadian</option>
                                    
                                        <option value="LS"> Basotho</option>
                                    
                                        <option value="BY"> Belarusian</option>
                                    
                                        <option value="BE"> Belgian</option>
                                    
                                        <option value="BZ"> Belizean</option>
                                    
                                        <option value="BJ"> Beninese</option>
                                    
                                        <option value="BM"> Bermudian</option>
                                    
                                        <option value="BT"> Bhutanese</option>
                                    
                                        <option value="BO"> Bolivian</option>
                                    
                                        <option value="BW"> Botswanan</option>
                                    
                                        <option value="BV"> Bouvet Islands</option>
                                    
                                        <option value="BR"> Brazilian</option>
                                    
                                        <option value="IO"> British Indian Ocean Territory</option>
                                    
                                        <option value="VG"> British Virgin Islander; BV Islander</option>
                                    
                                        <option value="BN"> Bruneian</option>
                                    
                                        <option value="BG"> Bulgarian</option>
                                    
                                        <option value="BF"> Burkinabe</option>
                                    
                                        <option value="MM"> Burmese</option>
                                    
                                        <option value="BI"> Burundian</option>
                                    
                                        <option value="KH"> Cambodian</option>
                                    
                                        <option value="CM"> Cameroonian</option>
                                    
                                        <option value="CB"> Canada Buffer</option>
                                    
                                        <option value="CA"> Canadian</option>
                                    
                                        <option value="CV"> Cape Verdean</option>
                                    
                                        <option value="KY"> Caymanian</option>
                                    
                                        <option value="CF"> Central African</option>
                                    
                                        <option value="TD"> Chadian</option>
                                    
                                        <option value="CL"> Chilean</option>
                                    
                                        <option value="CN"> Chinese</option>
                                    
                                        <option value="CX"> Christmas Islander</option>
                                    
                                        <option value="CC"> Cocos Islander</option>
                                    
                                        <option value="CO"> Colombian</option>
                                    
                                        <option value="KM"> Comorian</option>
                                    
                                        <option value="CD"> congo</option>
                                    
                                        <option value="CG"> Congolese</option>
                                    
                                        <option value="CK"> Cook Islander</option>
                                    
                                        <option value="CR"> Costa Rican</option>
                                    
                                        <option value="HR"> Croat</option>
                                    
                                        <option value="CU"> Cuban</option>
                                    
                                        <option value="CY"> Cypriot</option>
                                    
                                        <option value="CZ"> Czech</option>
                                    
                                        <option value="DK"> Dane</option>
                                    
                                        <option value="DJ"> Djiboutian</option>
                                    
                                        <option value="DO"> Dominican</option>
                                    
                                        <option value="DM"> Dominicana</option>
                                    
                                        <option value="NL"> Dutchman; Dutchwoman; Netherlander</option>
                                    
                                        <option value="TP"> East Timor</option>
                                    
                                        <option value="EC"> Ecuadorian</option>
                                    
                                        <option value="EG"> Egyptian</option>
                                    
                                        <option value="AE"> Emirian</option>
                                    
                                        <option value="GQ"> Equatorial Guinean</option>
                                    
                                        <option value="ER"> Eritrean</option>
                                    
                                        <option value="EE"> Estonian</option>
                                    
                                        <option value="ET"> Ethiopian</option>
                                    
                                        <option value="EU"> European Monetary Union</option>
                                    
                                        <option value="FO"> Faeroese</option>
                                    
                                        <option value="FK"> Falkland Islander</option>
                                    
                                        <option value="FJ"> Fiji Islander</option>
                                    
                                        <option value="PH"> Filipino</option>
                                    
                                        <option value="FI"> Finn</option>
                                    
                                        <option value="TF"> French Southern Territories</option>
                                    
                                        <option value="FR"> Frenchman; Frenchwoman</option>
                                    
                                        <option value="GA"> Gabonese</option>
                                    
                                        <option value="GM"> Gambian</option>
                                    
                                        <option value="GE"> Georgian</option>
                                    
                                        <option value="DE"> German</option>
                                    
                                        <option value="GH"> Ghanaian</option>
                                    
                                        <option value="GI"> Gibraltarian</option>
                                    
                                        <option value="GR"> Greece</option>
                                    
                                        <option value="GL"> Greenlander</option>
                                    
                                        <option value="GD"> Grenadian</option>
                                    
                                        <option value="GP"> Guadeloupean</option>
                                    
                                        <option value="GU"> Guamanian</option>
                                    
                                        <option value="GT"> Guatemalan</option>
                                    
                                        <option value="GF"> Guianese</option>
                                    
                                        <option value="GW"> Guinea-Bissau national</option>
                                    
                                        <option value="GN"> Guinean</option>
                                    
                                        <option value="GY"> Guyanese</option>
                                    
                                        <option value="HT"> Haitian</option>
                                    
                                        <option value="HM"> Heard &amp; Mcdonald Islands</option>
                                    
                                        <option value="HN"> Honduran</option>
                                    
                                        <option value="HK"> Hong Kong Chinese</option>
                                    
                                        <option value="HU"> Hungarian</option>
                                    
                                        <option value="IS"> Icelander</option>
                                    
                                        <option selected="selected" value="IN"> Indian</option>
                                    
                                        <option value="ID"> Indonesian</option>
                                    
                                        <option value="IR"> Iranian</option>
                                    
                                        <option value="IQ"> Iraqi</option>
                                    
                                        <option value="IE"> Irishman; Irishwoman</option>
                                    
                                        <option value="IL"> Israeli</option>
                                    
                                        <option value="IT"> Italian</option>
                                    
                                        <option value="CI"> Ivorian</option>
                                    
                                        <option value="JM"> Jamaican</option>
                                    
                                        <option value="JP"> Japanese</option>
                                    
                                        <option value="JO"> Jordanian</option>
                                    
                                        <option value="KZ"> Kazakh</option>
                                    
                                        <option value="KE"> Kenyan</option>
                                    
                                        <option value="KI"> Kiribatian</option>
                                    
                                        <option value="KN"> Kittsian; Nevisian</option>
                                    
                                        <option value="KW"> Kuwaiti</option>
                                    
                                        <option value="KG"> Kyrgyz</option>
                                    
                                        <option value="LA"> Lao</option>
                                    
                                        <option value="LV"> Latvian</option>
                                    
                                        <option value="LB"> Lebanese</option>
                                    
                                        <option value="LR"> Liberian</option>
                                    
                                        <option value="LY"> Libyan</option>
                                    
                                        <option value="LI"> Liechtensteiner</option>
                                    
                                        <option value="QL"> Lithuania (Dummy Code)</option>
                                    
                                        <option value="LT"> Lithuanian</option>
                                    
                                        <option value="LU"> Luxembourger</option>
                                    
                                        <option value="MO"> Macanese</option>
                                    
                                        <option value="MK"> Macedonia</option>
                                    
                                        <option value="YT"> Mahorais</option>
                                    
                                        <option value="MG"> Malagasy</option>
                                    
                                        <option value="MW"> Malawian</option>
                                    
                                        <option value="MY"> Malaysian</option>
                                    
                                        <option value="MV"> Maldivian</option>
                                    
                                        <option value="ML"> Malian</option>
                                    
                                        <option value="MT"> Maltese</option>
                                    
                                        <option value="MH"> Marshallese</option>
                                    
                                        <option value="MQ"> Martinican</option>
                                    
                                        <option value="MR"> Mauritanian</option>
                                    
                                        <option value="MU"> Mauritian</option>
                                    
                                        <option value="MX"> Mexican</option>
                                    
                                        <option value="MB"> Mexico Buffer</option>
                                    
                                        <option value="FM"> Micronesian</option>
                                    
                                        <option value="MD"> Moldovan</option>
                                    
                                        <option value="MC"> Monegasque</option>
                                    
                                        <option value="MN"> Mongolian</option>
                                    
                                        <option value="MS"> Montserratian</option>
                                    
                                        <option value="MA"> Moroccan</option>
                                    
                                        <option value="MZ"> Mozambican</option>
                                    
                                        <option value="NA"> Namibian</option>
                                    
                                        <option value="NR"> Nauruan</option>
                                    
                                        <option value="NP"> Nepalese</option>
                                    
                                        <option value="NC"> New Caledonian</option>
                                    
                                        <option value="NZ"> New Zealander</option>
                                    
                                        <option value="NI"> Nicaraguan</option>
                                    
                                        <option value="NG"> Nigerian</option>
                                    
                                        <option value="NE"> Nigerien</option>
                                    
                                        <option value="NU"> Niuean</option>
                                    
                                        <option value="NF"> Norfolk Islander</option>
                                    
                                        <option value="KP"> North Korean</option>
                                    
                                        <option value="MP"> Northern Mariana Islander</option>
                                    
                                        <option value="NO"> Norwegian</option>
                                    
                                        <option value="BA"> of Bosnia and Herzegovina</option>
                                    
                                        <option value="VA"> of the Holy See (of the Vatican) </option>
                                    
                                        <option value="OM"> Omani</option>
                                    
                                        <option value="PK"> Pakistani</option>
                                    
                                        <option value="PW"> Palauan</option>
                                    
                                        <option value="PS"> Palestinian Occ. Territories</option>
                                    
                                        <option value="PA"> Panamanian</option>
                                    
                                        <option value="PG"> Papua New Guinean</option>
                                    
                                        <option value="PY"> Paraguayan</option>
                                    
                                        <option value="PE"> Peruvian</option>
                                    
                                        <option value="PL"> Pole</option>
                                    
                                        <option value="PF"> Polynesian</option>
                                    
                                        <option value="PT"> Portuguese</option>
                                    
                                        <option value="PR"> Puerto Rican</option>
                                    
                                        <option value="QA"> Qatari</option>
                                    
                                        <option value="RE"> Reunionese</option>
                                    
                                        <option value="RO"> Romanian</option>
                                    
                                        <option value="RU"> Russian</option>
                                    
                                        <option value="RW"> Rwandan; Rwandese</option>
                                    
                                        <option value="EH"> Sahrawi</option>
                                    
                                        <option value="SH"> Saint Helenian</option>
                                    
                                        <option value="LC"> Saint Lucian</option>
                                    
                                        <option value="SV"> Salvadorian; Salvadoran</option>
                                    
                                        <option value="WS"> Samoan</option>
                                    
                                        <option value="SM"> San Marinese</option>
                                    
                                        <option value="ST"> São Toméan</option>
                                    
                                        <option value="SA"> Saudi Arabian</option>
                                    
                                        <option value="SN"> Senegalese</option>
                                    
                                        <option value="SC"> Seychellois</option>
                                    
                                        <option value="SL"> Sierra Leonean</option>
                                    
                                        <option value="SG"> Singaporean</option>
                                    
                                        <option value="SK"> Slovak</option>
                                    
                                        <option value="SI"> Slovene</option>
                                    
                                        <option value="SB"> Solomon Islander</option>
                                    
                                        <option value="SO"> Somali</option>
                                    
                                        <option value="ZA"> South African</option>
                                    
                                        <option value="GS"> South Georgia &amp; South Sandwich</option>
                                    
                                        <option value="KR"> South Korean</option>
                                    
                                        <option value="SU"> Soviet Union</option>
                                    
                                        <option value="ES"> Spaniard</option>
                                    
                                        <option value="LK"> Sri Lankan</option>
                                    
                                        <option value="PM"> St-Pierrais; Miquelonnais</option>
                                    
                                        <option value="SD"> Sudanese</option>
                                    
                                        <option value="SR"> Surinamer</option>
                                    
                                        <option value="SJ"> Svalbard &amp; Jan Mayen Islands</option>
                                    
                                        <option value="SZ"> Swazi</option>
                                    
                                        <option value="SE"> Swede</option>
                                    
                                        <option value="CH"> Swiss</option>
                                    
                                        <option value="SY"> Syrian</option>
                                    
                                        <option value="TW"> Taiwanese</option>
                                    
                                        <option value="TJ"> Tajik</option>
                                    
                                        <option value="TZ"> Tanzanian</option>
                                    
                                        <option value="TH"> Thai</option>
                                    
                                        <option value="TG"> Togolese</option>
                                    
                                        <option value="TK"> Tokelauan</option>
                                    
                                        <option value="TO"> Tongan</option>
                                    
                                        <option value="TT"> Trinidadian; Tobagonian</option>
                                    
                                        <option value="TN"> Tunisian</option>
                                    
                                        <option value="TR"> Turk</option>
                                    
                                        <option value="TM"> Turkmen</option>
                                    
                                        <option value="TC"> Turks and Caicos Islander</option>
                                    
                                        <option value="TV"> Tuvaluan</option>
                                    
                                        <option value="UM"> U.S. Minor Outlaying Islands</option>
                                    
                                        <option value="UG"> Ugandan</option>
                                    
                                        <option value="UA"> Ukrainian</option>
                                    
                                        <option value="GB"> United Kingdom</option>
                                    
                                        <option value="UY"> Uruguayan</option>
                                    
                                        <option value="VI"> US Virgin Islander</option>
                                    
                                        <option value="UZ"> Uzbek</option>
                                    
                                        <option value="VU"> Vanuatuan</option>
                                    
                                        <option value="VE"> Venezuelan</option>
                                    
                                        <option value="VN"> Vietnamese</option>
                                    
                                        <option value="VC"> Vincentian</option>
                                    
                                        <option value="WF"> Wallisian; Futunan; Wallis and Futuna Islander</option>
                                    
                                        <option value="YE"> Yemeni</option>
                                    
                                        <option value="YU"> Yugoslavia</option>
                                    
                                        <option value="ZM"> Zambian</option>
                                    
                                        <option value="ZW"> Zimbabwean</option>
                                    
                            
                            </select>
                             
                          <!--<span id="bhuerr_0" class="red font11" style="display:none;">Please Select Nationality</span>-->
                            </code>
                        </div>
                        @endif    
                      <div class="col-md-6 ">
                      
                         
                          <label>Passport No. :</label>
                         
                           
                            <code class="code-ki-class">
                            <input id="PassportNo.{{$e}}" name="PassportNo[]" type="text" maxlength="16" class="passport   valid " tabindex="{{$tab++}}" value="{{old('PassportNo.'.$e)}}">
                            <br><span class="red font11" id="passNo_0" style="display:none;">Please Enter Passport No.</span>
                            </code>

                   </div>
                      <div class="col-md-6 ">

                          <label>Passport Exp :</label>
                         
                            <code class="code-ki-class">
                            <select id="PassExpDay.{{$e}}" name="PassExpDay[]" class="date_width fly_select " tabindex="{{$tab++}}">
                                <option  value="">Day</option>
                                    @for($d=1;$d<32;$d++)
                                    <option value="{{$d}}" <?php echo old('PassExpDay.'.$e)==$d? "selected":""; ?>>{{$d}}</option>
                                    @endfor
                                    
                            </select>
                            <select id="PassExpMonth.{{$e}}" name="PassExpMonth[]" class="date_width fly_select " tabindex="{{$tab++}}">
                                <option value="">Month</option>
                                
                                  @for($month=1;$month<=12;$month++)
                                    <option value="{{$month}}" <?php echo old('PassExpMonth.'.$e)==$month? "selected":""; ?>>{{date('F', mktime(0,0,0,$month, 1, date('Y')))}}</option>
                                  @endfor                                
                                    
                                
                            </select>
                            <select id="PassExpYear.{{$e}}" name="PassExpYear[]" class="passExp date_width fly_select " tabindex="{{$tab++}}">
                                <option value="" selected="selected">Year</option>
                                    @for($year=2018;$year<2100;$year++)
                                    <option value="{{$year}}" <?php echo old('PassExpYear.'.$e)==$year? "selected":""; ?>>{{$year}}</option>
                                    @endfor
                                
                            </select>
                             <br><span class="red font11" id="pED_0" style="display:none;">Please Enter Valid Date.</span>
                            </code>                            
                          
                      </div>
                     </div>
                   <div class="row">
                   
                    <div class="col-md-6 ">
                   
                       
                    </div>
                    <div class="col-md-6 ">
                           
                           
                       
                    </div>
                </div>
                @if($i<=($adult_count+$child_count))
					<?php $ssr_passenger = $adult_count+$child_count; ?>
				<div class="row">
												 @if(isset($ssr->Response->Baggage))
												 
												 @foreach($ssr->Response->Baggage as $key=>$baggage)
												 <?php $baggage_result = count($baggage); ?>
															<div class="col-md-6 ">
																<code class="code-ki-class"> {{$baggage[0]->Origin}} - {{$baggage[$baggage_result-1]->Destination}} </code>
															
																<label class="air_bagg">Select Excess Baggage<br>(Extra charges will be applicable): </label>                                            
																<code class="code-ki-class">                                                                
															         @if($key==0)
																    <select id="baggage_{{$i}}" onchange="return selectedBaggage({{$ssr_passenger}});" name="baggage[]" class="baggage pdfselect fly_select width_96 " tabindex="{{$tab++}}">
																    @elseif($key==1)
																    <select id="baggage_ret_{{$i}}" onchange="return selectedReturnBaggage({{$ssr_passenger}});" name="baggage_ret[]" class="baggage pdfselect fly_select width_96 " tabindex="{{$tab++}}">
																    @endif
															     
																        @foreach($baggage as $bg)
																			<option value="{{$bg->Code}}">
																			   @if($bg->Weight==0||$bg->Weight==NULL)
																			   {{$bg->Code}}
																			   @else
																			  {{$bg->Weight."Kg - ₹".$bg->Price}}
																			   @endif
																			</option>                                                                
																		   
																		@endforeach
																		  
																</select>
															
															   </code>
							                            </div>
							                     @endforeach
							                    @endif
							
							
													@if(isset($ssr->Response->MealDynamic))
													   @foreach($ssr->Response->MealDynamic as $key=>$mealdynamic)
													    <?php $meal_result = count($mealdynamic); ?>
															<div class="col-md-6 ">
															  <label class="air_bagg">Meal Preferences: </label>
															  <code class="code-ki-class">
															  {{$mealdynamic[0]->Origin}} - {{$mealdynamic[$meal_result-1]->Destination}} 
															  
															    @if($key==0)
																    <select id="meal_{{$i}}" onchange="return selectedMeal({{$ssr_passenger}})" name="meal[]" class="mealExtra pdfselect fly_select width_96" tabindex="{{$tab++}}">
															    @elseif($key==1)
																    <select id="meal_ret_{{$i}}" onchange="return selectedReturnMeal({{$ssr_passenger}})" name="meal_ret[]" class="mealExtra pdfselect fly_select width_96" tabindex="{{$tab++}}">
																@endif
															  
															   @foreach($mealdynamic as $meal)
																  <option value="{{$meal->Code}}">
																      @if($meal->Price==0||$meal->Price==null)
																      {{$meal->Code}}
																      @else
																      {{$meal->AirlineDescription." - Rs.".$meal->Price}}
																      @endif
																    </option>
															@endforeach
															  </select>
														
															</code>
														 </div>
													    @endforeach	
													@endif
													
	<!-----------------------------------------------SSR return Baggage and Meal Details-------------------------------------------------->
	                                                
														 @if(isset($ssr_return->Response->Baggage[0]))
															<div class="col-md-6 ">
																<label class="air_bagg"> &nbsp;</label>
																<code class="code-ki-class"> {{$ssr_return->Response->Baggage[0][0]->Origin}} - {{$ssr_return->Response->Baggage[0][0]->Destination}} </code>
															
																<label class="air_bagg">Select Excess Baggage<br>(Extra charges will be applicable): </label>                                            
																<code class="code-ki-class">                                                                
															   
																<select id="baggage_ret_{{$i}}" onchange="return selectedReturnBaggage({{$ssr_passenger}});" name="baggage_ret[]" class="baggage pdfselect fly_select width_96" tabindex="{{$tab++}}">
																						
																			@foreach($ssr_return->Response->Baggage[0] as $bgr)
																			<option value="{{$bgr->Code}}">
																			   @if($bgr->Weight==0||$bgr->Weight==NULL)
																			   {{$bgr->Code}}
																			   @else
																			  {{$bgr->Weight."Kg - ₹".$bgr->Price}}
																			   @endif
																			</option>                                                                
																		   
																			@endforeach
																		  
																</select>
															
															   </code>
							</div>
							@endif
							
							
													@if(isset($ssr_return->Response->MealDynamic[0]))
													   
															<div class="col-md-6 ">
															  <label class="air_bagg">Meal Preferences: </label>
															  <code class="code-ki-class">
															  {{$ssr_return->Response->MealDynamic[0][0]->Origin}} - {{$ssr_return->Response->MealDynamic[0][0]->Destination}} 
															  <select id="meal_ret_{{$i}}" onchange="return selectedReturnMeal({{$ssr_passenger}})" name="meal_ret[]" class="mealExtra pdfselect fly_select width_96" tabindex="{{$tab++}}">
															   @foreach($ssr_return->Response->MealDynamic[0] as $rmeal)
																  <option value="{{$rmeal->Code}}">
																      @if($rmeal->Price==0||$rmeal->Price==null)
																      {{$rmeal->Code}}
																      @else
																      {{$rmeal->AirlineDescription." - Rs.".$rmeal->Price}}
																      @endif
																    </option>
															@endforeach
															  </select>
														
															</code>
														 </div>
														
													@endif
													
																	   <div class="col-md-6 ">
																		  
																			<code class="code-ki-class">  
																			  
																				<input type="hidden" id="Seat_0_indi_1_seg_0" name="Seat_0_indi_1_seg_0"> 
																				 </code>
																		   </div>
																	   
																	   <div class="col-md-6 ">
																		  
																			<code class="code-ki-class">  
																			  
																				<input type="hidden" id="Seat_0_indi_1_seg_1" name="Seat_0_indi_1_seg_1"> 
																				 </code>
																		   </div>

																		
										</div>                              
									@endif	  
								<input type="hidden" id="userAction_0" name="userAction_0" value="">
								<input type="hidden" id="CustomerID_0" name="CustomerID_0" value="">
                @if($i==1)
			        
					
					@endif
	@endfor
				
						<div class="htd_head"><h1>Terms & Conditions</h1></div>
						<div class="htd_databox">
				   <div class="htd_databoxin">
						
						<?php echo $package->terms_and_conditions;?>
							
						
				   </div>
				</div>
				<div class="row">
				  <div class="col-md-12">
					
				  </div>
				  </div>
	</div>
	<div class="col-md-3 mobfilter_display_none" id="mobile_queue_filter">
                <h3 class="hopdf">Promo Code </h3>                  
                <div class="deal_cont" id="lblPromoCode">
                         
                <div class="drdr_in">                        
                    <label class="promo_error" id="errorPromoCode"></label>
                        
                        <input id="PromoCode" name="PromoCode" type="text" class="promoinput fleft valid" value="" placeholder="Promo Code" maxlength="10">
                         
                    <div class="fright">
						@if(access()->user())
                        <a id="PromoCodeApply_login" href="#" class="btn_main fright mr5" >Apply</a>
						@else
						<a id="PromoCodeApply" href="#" class="btn_main fright mr5" >Apply</a>
						@endif
						
                </div>
                </div>       
                       
                           
                          
                           
                </div>
                <br><br>
                <div class="row">
		
				<div class="col-md-12">
					
					<div class="table-responsive">
						<table class="table">
						    <thead>
						        	<tr>
						  		<th>Package Price</th>
						  		<th>Pax</th>
						  		<th class="text-right">(Pax X Price)</th>
						  	</tr>
						    </thead>
						  <tbody>
						    	<tr>
						    	    @php
						    	    $total = $passenger_count * $price;
						    	    $discount_amount = ($total * $discount)/100;
						    	    $payable = $total - $discount_amount;
						    	    @endphp
						    	    <input type="hidden" name="package_id" value="{{$package->id}}">
						    	    <input type="hidden" name="payable_amount" value="{{$payable}}">
						    	    <input type="hidden" name="discount" value="{{$discount_amount}}">
						    	    <input type="hidden" name="discount_rate" value="{{$discount}}">
						    	    
						  		<td>&#8377;{{$price}}</td>
						  		<td>{{$passenger_count}}</td>
						  		<td class="text-right">&#8377;{{$total}}</td>
						  	</tr>
						 
						  	<tr class="bg-grey bg-lighten-12">
						  		<td class="text-bold-800">Package Discount@ {{$discount}}%</td>
						  		<td class="text-bold-800 text-right" colspan="2">&#8377;{{$discount_amount}}</td>
						  	</tr>
						 
						  	<tr class="bg-grey bg-lighten-12">
						  		<td class="text-bold-800">Total Payable</td>
						  		<td class="text-bold-800 text-right" colspan="2">&#8377;{{$payable}}</td>
						  	</tr>
						  </tbody>
						</table>
					</div>
					
				</div>
			</div>
            
            
            <input style="color: #fff;" type="submit" id="continue" href="#!" class="btn_main center mr5" value='Proceed to Payment'>
                  <!--<div class="deal_head cursor">-->
                      
                  <!--    <a href="#" id="divSalesSummaryHead">+ Show Details</a>-->
                      
                  <!--</div>-->
        
            </div>
</div>

<div class="modal fade" id="select_seats" role="dialog">
    <div class="plane">
  <div class="cockpit">
    <h1>Please select a seat</h1>
  </div>
  <div class="exit exit--front fuselage">
    
  </div>
  <ol class="cabin fuselage">
    <li class="row row--1">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="1A">
          <label for="1A">1A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="1B">
          <label for="1B">1B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="1C">
          <label for="1C">1C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="1D">
          <label for="1D">1D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="1E">
          <label for="1E">1E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="1F">
          <label for="1F">1F</label>
        </li>
      </ol>
    </li>
    <li class="row row--2">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="2A">
          <label for="2A">2A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="2B">
          <label for="2B">2B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="2C">
          <label for="2C">2C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="2D">
          <label for="2D">2D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="2E">
          <label for="2E">2E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="2F">
          <label for="2F">2F</label>
        </li>
      </ol>
    </li>
    <li class="row row--3">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="3A">
          <label for="3A">3A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="3B">
          <label for="3B">3B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="3C">
          <label for="3C">3C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="3D">
          <label for="3D">3D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="3E">
          <label for="3E">3E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="3F">
          <label for="3F">3F</label>
        </li>
      </ol>
    </li>
    <li class="row row--4">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="4A">
          <label for="4A">4A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="4B">
          <label for="4B">4B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="4C">
          <label for="4C">4C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="4D">
          <label for="4D">4D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="4E">
          <label for="4E">4E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="4F">
          <label for="4F">4F</label>
        </li>
      </ol>
    </li>
    <li class="row row--5">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="5A">
          <label for="5A">5A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="5B">
          <label for="5B">5B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="5C">
          <label for="5C">5C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="5D">
          <label for="5D">5D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="5E">
          <label for="5E">5E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="5F">
          <label for="5F">5F</label>
        </li>
      </ol>
    </li>
    <li class="row row--6">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="6A">
          <label for="6A">6A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="6B">
          <label for="6B">6B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="6C">
          <label for="6C">6C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="6D">
          <label for="6D">6D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="6E">
          <label for="6E">6E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="6F">
          <label for="6F">6F</label>
        </li>
      </ol>
    </li>
    <li class="row row--7">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="7A">
          <label for="7A">7A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="7B">
          <label for="7B">7B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="7C">
          <label for="7C">7C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="7D">
          <label for="7D">7D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="7E">
          <label for="7E">7E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="7F">
          <label for="7F">7F</label>
        </li>
      </ol>
    </li>
    <li class="row row--8">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="8A">
          <label for="8A">8A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="8B">
          <label for="8B">8B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="8C">
          <label for="8C">8C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="8D">
          <label for="8D">8D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="8E">
          <label for="8E">8E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="8F">
          <label for="8F">8F</label>
        </li>
      </ol>
    </li>
    <li class="row row--9">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="9A">
          <label for="9A">9A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="9B">
          <label for="9B">9B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="9C">
          <label for="9C">9C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="9D">
          <label for="9D">9D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="9E">
          <label for="9E">9E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="9F">
          <label for="9F">9F</label>
        </li>
      </ol>
    </li>
    <li class="row row--10">
      <ol class="seats" type="A">
        <li class="seat">
          <input type="checkbox" id="10A">
          <label for="10A">10A</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="10B">
          <label for="10B">10B</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="10C">
          <label for="10C">10C</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="10D">
          <label for="10D">10D</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="10E">
          <label for="10E">10E</label>
        </li>
        <li class="seat">
          <input type="checkbox" id="10F">
          <label for="10F">10F</label>
        </li>
      </ol>
    </li>
  </ol>
  <div class="exit exit--back fuselage">
    
  </div>
</div>
    </div>
</form>
 

</div>
@include('frontend.includes.footer2')
<script>
    $(document).ready(function(){
       $("#chkBoxGST").on("click",function(){
           if($("#chkBoxGST").is(':checked'))
         $("#GSTDataBlock").show();  
         else
         $("#GSTDataBlock").hide();  
       });
       $("#chkBoxRoamer").on("click",function(){
          if($("#chkBoxRoamer").is(":checked"))
          $("#RoamerDataBlock").show();
          else
          $("#RoamerDataBlock").hide();
       });
    });
</script>
<script>
function selectedBaggage(bag_psg){
      var total_baggage = 0;
      var total_baggage_price = 0;
    for(var b=1;b<=bag_psg;b++){
        var baggage_details =$("#baggage_"+b+" option:selected").text().replace(/[^a-zA-Z 0-9]+/g, ""); 
        var baggage = baggage_details.split(" ");
            //alert(baggage);
        var stringArray = new Array();
        var w="";
        var price=0;
    
        for(var i =0; i < baggage.length; i++){
            
                //alert(isNaN(baggage[i]));
                var x=baggage[i].charCodeAt(0);
                if(!isNaN(x))
                {
                    stringArray.push(baggage[i]);
                }
            
            
        
        }
        if(stringArray[0]!="No"){
        baggage_weight= stringArray[0].split("Kg");
         total_baggage = parseInt(total_baggage)+parseInt(baggage_weight[0]);
        }
        else{
           baggage_weight[0]=0;
         total_baggage = parseInt(total_baggage)+parseInt(baggage_weight[0]);  
        }
        
        if(!isNaN(stringArray[1])){
            baggage_price = stringArray[1];
            total_baggage_price = parseFloat(total_baggage_price)+parseFloat(baggage_price);
            
         }
       
      // alert(total_baggage_price);
    
    }
    var bag_cost = total_baggage_price.toFixed(2);
    var ret_total_baggage = $("#ibbagSelected").val();
    var ret_bag_charges = $("#ibbagCharges").val();
    var ib_ob_total_baggage = parseInt(ret_total_baggage)+parseInt(total_baggage);
    var ib_ob_bag_charges = parseFloat(ret_bag_charges)+parseFloat(bag_cost);
    $("#obbagSelected").val(total_baggage);
    $("#obbagCharges").val(bag_cost);
    
    $("#bagSelected").html(ib_ob_total_baggage+"Kg");
    $("#bagCharges").html(ib_ob_bag_charges);
    calculateTotalFare();
}

function selectedReturnBaggage(bag_psg){
      var total_baggage_ret = 0;
      var total_baggage_ret_price = 0;
    for(var b=1;b<=bag_psg;b++){
        var baggage_ret_details =$("#baggage_ret_"+b+" option:selected").text().replace(/[^a-zA-Z 0-9]+/g, ""); 
        var baggage_ret = baggage_ret_details.split(" ");
            //alert(baggage);
        var stringArray = new Array();
        var w="";
        var price=0;
    
        for(var i =0; i < baggage_ret.length; i++){
            
                //alert(isNaN(baggage[i]));
                var x=baggage_ret[i].charCodeAt(0);
                if(!isNaN(x))
                {
                    stringArray.push(baggage_ret[i]);
                }
            
            
        
        }
        if(stringArray[0]!="No"){
        baggage_ret_weight= stringArray[0].split("Kg");
         total_baggage_ret = parseInt(total_baggage_ret)+parseInt(baggage_ret_weight[0]);
        }
        else{
           baggage_ret_weight[0]=0;
         total_baggage_ret = parseInt(total_baggage_ret)+parseInt(baggage_ret_weight[0]);  
        }
        
        if(!isNaN(stringArray[1])){
            baggage_ret_price = stringArray[1];
            total_baggage_ret_price = parseFloat(total_baggage_ret_price)+parseFloat(baggage_ret_price);
            
         }
       
      // alert(total_baggage_price);
    
    }
    var bag_cost = total_baggage_ret_price.toFixed(2);
    var ob_total_baggage = $("#obbagSelected").val();
    var ob_bag_charges = $("#obbagCharges").val();
    var ib_ob_total_baggage = parseInt(ob_total_baggage)+parseInt(total_baggage_ret);
    var ib_ob_bag_charges = parseFloat(ob_bag_charges)+parseFloat(bag_cost);
    $("#ibbagSelected").val(total_baggage_ret);
    $("#ibbagCharges").val(bag_cost);
    
    $("#bagSelected").html(ib_ob_total_baggage+"Kg");
    $("#bagCharges").html(ib_ob_bag_charges);
    calculateTotalFare();
}
    
</script>
<script>
function selectedMeal(meal_psg){
      var total_meal = 0;
      var total_meal_price = 0;
    for(var b=1;b<=meal_psg;b++){
        var meal_details =$("#meal_"+b+" option:selected").text().replace(/[^a-zA-Z 0-9]+/g, ""); 
        var meal = meal_details.split("Rs");
            //alert(baggage);
        var stringArray = new Array();
        var w="";
        var price=0;
    
        for(var i =0; i < meal.length; i++){
            
                //alert(isNaN(baggage[i]));
                var x=meal[i].charCodeAt(0);
                if(!isNaN(x))
                {
                    stringArray.push(meal[i]);
                }
            
             
        
        }
       
        if(stringArray[0].trim()!="No Meal"){
           total_meal++;
        }
       
     
        if(!isNaN(stringArray[1])){
            meal_price = stringArray[1];
            total_meal_price = parseFloat(total_meal_price)+parseFloat(meal_price);
          }
     }
    var meal_cost = total_meal_price.toFixed(2);
   
    var ret_meal_platter = $("#ibmealPlatter").val();
    var ret_meal_charge = $("#ibmealCharge").val();
    var ib_ob_meal_platter = parseInt(ret_meal_platter)+parseInt(total_meal);
    var ib_ob_meal_charge = parseFloat(ret_meal_charge)+parseFloat(meal_cost);
    $("#obmealPlatter").val(total_meal);
    $("#obmealCharge").val(meal_cost);
    
    $("#mealPlatter").html(ib_ob_meal_platter);
    $("#mealCharge").html(ib_ob_meal_charge);
    calculateTotalFare();
}

function selectedReturnMeal(meal_psg){
      var total_meal = 0;
      var total_meal_price = 0;
    for(var b=1;b<=meal_psg;b++){
        var meal_details =$("#meal_ret_"+b+" option:selected").text().replace(/[^a-zA-Z 0-9]+/g, ""); 
        var meal = meal_details.split("Rs");
            //alert(baggage);
        var stringArray = new Array();
        var w="";
        var price=0;
    
        for(var i =0; i < meal.length; i++){
            
                //alert(isNaN(baggage[i]));
                var x=meal[i].charCodeAt(0);
                if(!isNaN(x))
                {
                    stringArray.push(meal[i]);
                }
            
             
        
        }
       
        if(stringArray[0].trim()!="No Meal"){
           total_meal++;
        }
       
     
        if(!isNaN(stringArray[1])){
            meal_price = stringArray[1];
            total_meal_price = parseFloat(total_meal_price)+parseFloat(meal_price);
          }
     }
    var meal_cost = total_meal_price.toFixed(2);
   
    var meal_platter = $("#obmealPlatter").val();
    var ob_meal_charge = $("#obmealCharge").val();
    var ib_ob_meal_platter = parseInt(meal_platter)+parseInt(total_meal);
    var ib_ob_meal_charge = parseFloat(ob_meal_charge)+parseFloat(meal_cost);
    $("#ibmealPlatter").val(total_meal);
    $("#ibmealCharge").val(meal_cost);
    
    $("#mealPlatter").html(ib_ob_meal_platter);
    $("#mealCharge").html(ib_ob_meal_charge);
    calculateTotalFare();
}
    
</script>
<script>
    function calculateTotalFare(){
        var adult_count = $("#adultcount").html();
        var child_count = $("#childcount").html();
        var tws = 150*(parseInt(adult_count)+parseInt(child_count));
        var adult = $("#total_adult_cost").html(); 
        var child=$("#total_child_cost").html();
        var infant=$("#total_infant_cost").html();
        var baggage=$("#bagCharges").html();
        var meal=$("#mealCharge").html();
        var special_service=$("#specialServiceCharge").html();
        var gst=$("#total_gst_cost").html();
        var gsttws=(tws*18)/100;
         var total_fare = parseFloat(adult)+parseFloat(child)+parseFloat(infant)+parseFloat(baggage)+parseFloat(meal)+parseFloat(special_service)+parseFloat(gst)+parseFloat(tws)+parseFloat(gsttws);
         var tf = total_fare.toFixed(2);
         //alert(total_fare);
        // alert("adult:"+adult+"child:"+child+"infant:"+infant+"baggage:"+baggage+"meal:"+meal+"special_service:"+special_service+"gst:"+gst);
         $("#tws").html(tws);
         $("#gst_tws").html(gsttws.toFixed(2));
       $("#total_fare_cost").html(tf);
       $("#total_booking_cost").val(tf);
    }
    calculateTotalFare();
    
</script>
<!------------------------------------------------------------Passenger form validation----------------------------------------------------->
<script>
$(document).ready(function(){
   $("input[name='FirstName[]']").on("blur",function(){
       var fname = $(this).val();
       if(fname==null||fname==""){
        $(this).focus();   
        // $(this).next().show();
       }
       else{
        $(this).next().hide(); 
       }
   }) ;
   $("input[name='LastName[]']").on("blur",function(){
       var lname = $(this).val();
       if(lname==null||lname==""){
        $(this).focus();   
        // $(this).next().show();
       }
       else{
        $(this).next().hide(); 
       }
   }) ;
//   $("input[name='Gender[]']").on("blur",function(){
//       var gender = $(this).val();
//       if(gender==null||gender==""){
//         $(this).focus();   
//         $(this).next().show();
//       }
//       else{
//         $(this).next().hide(); 
//       }
//   }) ;
$("form").on("submit",function(e){
    if(!domestic())
    {
        swal({
  type: 'error',
  title: 'Oops...',
  text: 'Date of Birth is mandatory!',
  footer: '<a href>Why do I have this issue?</a>'
})
        e.preventDefault();
    }
    //return false;
});
function domestic(){
    let flag=1;
       
       var airline = $.trim($("#airline_name").text());
      
        var myControls = document.getElementsByName('DobDay[]');
       
        for (var i = 0; i < myControls.length; i++) {
            var aDay = myControls[i].value;
            var aMonth = document.getElementsByName('DobMonth[]')[i].value;
            var aYear =  document.getElementsByName('DobYear[]')[i].value;
            var pax_type = myControls[i].getAttribute("data-pax-type");
           
               // alert(pax_type);
            if(pax_type==1 && (airline.toLowerCase().indexOf('air india')>=0))
            {   
                if((aDay=="" || aDay==null)||(aMonth=="" || aMonth==null)||(aYear=="" || aYear==null))
                {
                    var p = document.createElement("span");
                    var error = document.createTextNode("Date of birth is mandatory");
                    p.appendChild(error);
                    
                     //document.getElementsByName('DobYear[]')[i].parentNode.removeChild(p);
                    document.getElementsByName('DobYear[]')[i].parentNode.insertBefore(p, document.getElementsByName('DobYear[]')[i].nextSibling);
                    flag=0;
                   // myControls[i].appendChild(p);
                    //return false;
                }
                else
                {
                    flag=1;
                }
                
            }
        }
        
        if(flag!=0)
        {
            
            return true;
        }
      
      else
      {
        
      return false;
      }
    }
});


</script>




