@include('frontend.includes.header')
@include('frontend.includes.nav1')

<div class="modal fade" id="confirmation" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">EMAIL INVITATION</h4>
        </div>
        <div class="modal-body">
           <p class="append_bottom10"><strong>Recipients</strong><span class="light-grey font12">(Invites will not be sent to those you have already invited.)</span></p><input type="hidden" id="medium" value="email"><p class="col-sm-12 email-invites append_bottom20">hello@atoz.digital</p><p class="append_bottom18 clearfix"><span class="checkbox-group"><input type="checkbox" id="send-download-link" checked="checked"><label for="send-download-link"><span class="box"><span class="check"></span></span><span class="labeltext">Send me the app download link</span></label></span></p>
            <button type="button" onclick="sendInvites();" class="email-invite">Send Invites</button> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<section class="refer-invite-section">
    <div class="container text-center">
        <h2>Invite a friend to sign up, earn <span>₹</span> 500!</h2>
        <p class="font16 append_bottom20">Your friend gets <span>₹</span> 1000 on Signingup in www.thailandwale.com.</p>
        <div class="clearfix referral-flow">
            <p class="referral-flow-connect"></p>
            <div class="pull-left stg1">
                <img src="{{asset('assets/home/images/email.png')}}" alt="referral">
                <p>Invite a friend to Signup on www.thailandwale.com</p>
            </div>
            <div class="pull-left stg2">
                    <img src="{{asset('assets/home/images/sign-in.png')}}" alt="mobile">
                    <p>Friend signs up on the , you earn <strong class="dark-grey">₹ 500</strong></p>
            </div>
            <div class="pull-left stg3">
                <img src="{{asset('assets/home/images/earnings.png')}}" alt="piggy-bank">
                <p>When a friend books with us, you earn up to <strong class="dark-grey">₹ 200</strong></p>
                </div>
                <div class="refer-earned-points">
					<span class="amount-earned hide"> YOU HAVE EARNED <span class="amount referralEarned">₹</span><span class="amount referralEarned referralEarnedText">0</span></span>
						<span class="light-grey">Refer more &amp; earn upto</span> ₹ 5000
				</div>
		</div>
	</div>
</section>
<section class="refer-share-section text-center">
    <div class="container">
        <h2>SHARE YOUR REFFERAL LINK</h2>
        <div class="col-sm-12 share-options text-left">
            <div class="col-sm-6 col-md-3 share-social">
                <h3>Social media</h3>
                <p class="font14 append_bottom12">Share on Facebook and whatsapp</p><p class="clearfix">
                    <a class="fb-share-btn col-sm-10 invite-btn"><span class="fb-icon"></span> Share on Facebook</a></p>
                    <p class="clearfix"><a href="https://api.whatsapp.com/send?text=Home%20http%3A%2F%2Fwww.thailandwale.com" class="whatsapp-share-btn col-sm-10 invite-btn"><span class="whatsapp-icon"></span> Share on WhatsApp</a></p>
                    <p  class="whatsapp-share-text"><a >Click to copy and paste link on WhatsApp Web.</a></p>
            </div>
            <div class="col-sm-9 col-md-9 share-email">
                <h3>Email</h3>
                <p class="font14 append_bottom12">Invite upto 5 friends</p>
                <p class="clearfix">
                    <input placeholder="abc@email.com, dce@email.com " class="col-sm-12 email-text" name="emails" id="emails"></p>
                <p class="clearfix">
                @if(!access()->user())
                <a class="btn-primary-red email-btn invite-btn col-sm-6" data-toggle="modal" data-target="#signin">Invite</a>
                @else
                <a class="btn-primary-red email-btn invite-btn col-sm-6" data-toggle="modal" data-target="#confirmation">Invite</a>
                @endif
                </p>
            </div>
            <!-- <div class="col-sm-6 col-md-3 share-sms">
                <h3>Send Sms</h3>
                <p class="font14 append_bottom12">Invite upto 5 friends</p>
                <p class="clearfix">
                    <input placeholder="9891XXXXXX,9899XXXXXX" class="col-sm-12 sms-text"></p>
                    <p class="clearfix"><a class="btn-primary-red sms-btn invite-btn col-sm-6">Invite</a></p>
            </div> -->
            <!-- <div class="col-sm-6 col-md-3 share-url">
                <h3>Copy Url</h3>
                <p class="font14 append_bottom12 copy-url-text">Copy your unique url</p><p class="clearfix">
                    <input placeholder="https://www.thailandwale.com/0982hxg" class="col-sm-12" id="linkInput" value="" readonly="readonly"></p>
                    <p class="clearfix"><a id="referralLink" class="btn-primary-red copy-url-btn col-sm-6 invite-btn">Copy</a></p>
                    <p class="copy-confirm-text hide">Successfully copied! Now paste to share the link.</p>
            </div> -->
        </div>
    </div>
</section>
<section class="refer-condition-section text-center">
    <div class="container">
        <h2>Click here for <a href="http://www.makemytrip.com/promotion/includes/referandearn-promo-27082015.php" target="_blank">Terms &amp; Conditions</a></h2>
    </div>
</section>
<script>
$('.email-btn').on('click',function(e){
        let emails=$('#emails').val();
        let email=emails.split(',');
        for(let i=0;i<email.length;i++)
        {
         if(!ValidateEmail(email[i]))
         {
            swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Please Enter valid emails!',
                    footer: '<a href>Why do I have this issue?</a>'
                    })
                    e.stopPropagation();
         }   
        }        
    
$('.email-invites').html(emails);
});
</script>
<script>
function sendInvites()
{
    let emails=$('#emails').val();
    alert(emails);
    let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    
    $.ajax({
                  
                    url: "{{route('frontend.user.send_refer_email')}}",
                    type: "POST",
                    data: {_token: "{{ csrf_token() }}", email:emails},
                    success: function (data) { 
                        swal(
                                 'Good job!',
                                  data,
                                'success'
)
                    },
                    error:function(){
                        alert("Error");
                    }
                });
}
function ValidateEmail(inputText)
{
var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
if(inputText.match(mailformat))
{

return true;
}
else
{
return false;
}
}

</script>

@include('frontend.includes.footer')