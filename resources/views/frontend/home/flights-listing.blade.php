
@include('frontend.includes.header2')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')

@if(!empty($errors->first()))
<div class=' alert alert-danger'>{{$errors->first()}}</div>
@endif



<div id="flightlist-head" class="container-fluid">
    <div class="row">
      <div class="col-md-12 flights-form">
			@if($journey_type==1)
				@php
					$jtype="ONE-WAY";
					$org=$search_info['Segments'][0]['Origin'];
					$dest=$search_info['Segments'][0]['Destination'];
					$dep=$org." to ".$dest;
					$depDate=$search_info['Segments'][0]['PreferredDepartureTime'];
					$arrDate="--";
				@endphp
			@elseif($journey_type==2)
				@php
					$jtype="ROUND-TRIP";
					$org=$search_info['Segments'][0]['Origin'];
					$dest=$search_info['Segments'][0]['Destination'];
					$dep=$org." to ".$dest;
					$depDate=$search_info['Segments'][0]['PreferredDepartureTime'];
					$arrDate=$search_info['Segments'][1]['PreferredDepartureTime'];
					
				@endphp
			@else
				@php
					$jtype="MULTI-TRIP";
					$org=$search_info['Segments'][0]['Origin'];
					$dest=$search_info['Segments'][count($search_info['Segments'])-1]['Destination'];
					$dep=$org." to ".$dest;
					$depDate=$search_info['Segments'][0]['PreferredDepartureTime'];
					$arrDate=$search_info['Segments'][1]['PreferredDepartureTime'];
					
				@endphp
			@endif
          <div class="col-md-3 col-xs-4 right-border">
            <span><a href="#" data-toggle="modal" data-target="#modal_show">{{$jtype}}
			</a></span>
            <P> <a href="#" data-toggle="modal" data-target="#modal_show">{{$dep}}</a></P>
        </div>
        <div class="col-md-2 col-xs-4 right-border">
          <span> <a href="#" data-toggle="modal" data-target="#modal_show"> DEPARTURE </a> </span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> <i class="fas fa-calendar-alt"> </i> <span>{{date('d',strtotime($depDate))}}</span> 
            <span> <span>{{date('M y',strtotime($depDate))}}</span> <span>{{date('D',strtotime($depDate))}}</span></span>
          </a> </p>

        </div> 
        @if(!$jtype==3)
        <div class="col-md-2  col-xs-4">
          <span> <a href="#" data-toggle="modal" data-target="#modal_show"> Return </a> </span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> <i class="fas fa-calendar-alt"> </i> 
			@if($arrDate!=='--')
			<span>{{date('d',strtotime($arrDate))}}</span> 
            <span> <span>{{date('M y',strtotime($arrDate))}}</span> <span>{{date('D',strtotime($arrDate))}}</span></span>
			@else
				<span>{{$arrDate}}</span> 
			@endif
          </a> </p>

        </div> 
        @endif
		
        <div class="col-md-1 col-xs-4 right-border">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Adult</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
		@if($search_info['AdultCount']>0)
            <span>{{$search_info['AdultCount']}}</span>
		@else
			<span>--</span>
		@endif
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4 right-border">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Child</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
		  @if($search_info['ChildCount']>0)
            <span>{{$search_info['ChildCount']}}</span>
		@else
			<span>--</span>
		@endif
            
          </a> </p>
        </div>
       <div class="col-md-1 col-xs-4">
         <span> <a href="#" data-toggle="modal" data-target="#modal_show">Infant</a></span>
          <p><a href="#" data-toggle="modal" data-target="#modal_show"> 
            @if($search_info['InfantCount']>0)
            <span>{{$search_info['InfantCount']}}</span>
		@else
			<span>--</span>
		@endif
          </a> </p>
        </div>
        <div class="col-md-2 col-xs-12">
          <button type="button" class="btn btn-success" style=" margin: 0 auto; display: block; float: none; ">Search</button>
        </div>
   
    
        <div class="modal fade" tabindex="-1" role="dialog" id="modal_show">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header modal-flights">
    <h3>Search for Flights</h3>
          </div>
          <div class="modal-body">
              <form method="post"  action="{{route('frontend.search_flight')}}" novalidate>
<section class=" form-flights">
         <div class="tabbable-panel">
     <div class="tabbable-line">
     <div class="col-md-12 col-xs-12">
   
		 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="travel_class" id="travel_class">
        <div id="tab" class="btn-group btn-group-justified" data-toggle="buttons">
        <a href="#one_way" class="btn btn-default active" data-toggle="tab">
          <input type="radio" name="journey_type" value="1" checked="checked"/>ONE WAY
        </a>
        <a href="#round_trip" class="btn btn-default" data-toggle="tab">
          <input type="radio" name="journey_type" value="2" />ROUND TRIP
        </a>
        <a href="#multi_trip" class="btn btn-default" data-toggle="tab">
          <input type="radio" name="journey_type" value="3"/>MULTI-TRIP
        </a>
    
      </div>
     </div>

   

      <div class="tab-content">
        <div class="tab-pane active container-fluid" id="">	
        
                          <div class="row">
                        <div class="col-xs-12 col-md-6">
                              <h3>From</h3>
                        <select class="select2" id='input2' name="from[]" required="required">
                              <option value="">Source</option>  
                                <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?> 
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php } }?>

                        </select>
                            </div>
                        <div class="col-xs-12 col-md-6">
                              <h3>To</h3>
                        <select class="select2" id='input3' name="to[]" onchange="setOrigin(this);"  required="required">
                            <option value="">Destination</option>
                              <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?>
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php }
                                 }?>
                        </select>
                        
                            </div>
                            


                        <div class="col-xs-12 col-md-6">
                            <div class="date">
                                <div class="depart">
                                  <h3>Depart</h3>
                                  <input id="datepicker" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" autocomplete="off"  required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>
                        <div class="col-xs-12 col-md-6" id="multi-trip-hide">
                              <div class="return">
                            <h3>Return</h3>
                          <input  id="datepicker1" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" onfocus="this.value = '';" autocomplete="off" required>
                          </div>
                            </div>

					  <div class="col-xs-12 col-md-6">
                        <div class="popover-markup"> 
                            <div class="trigger form-group form-group-lg form-group-icon-left"><i class="fa fa-users input-icon input-icon-highlight"></i>
                                  <label>Passenger</label>
                                  <input type="hidden" name="passenger_count" id="passenger_count" value="1">
                                  <input type="text" name="passengers_one" id="passengers_one" value="1|Economy" class="form-control" autocomplete="off">
                            </div>
                            <div id="contactForm" style="display:none">
                                  <div class="triangle"></div>
                            <div class="content">
                                <!-- adult row -->
							<div class="row">
                                  <div class="form-group">
                                      <div class="col-md-3">
                                <label class="control-label"><strong>Adults</strong><br>
                                      <i> (+12 yrs)</i></label></div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn btn-danger" data-dir="dwn" id="adult_dn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="adult_one" id="adult_one" class="form-control text-center" value="1">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="adult_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="2"> Economy </button></div>
                              </div>
							  </div>
							  <!-- adult row end -->
							  <!-- child row -->
							  	<div class="row">
                                  <div class="form-group">
                                      <div class="col-md-3">
                                <label class="control-label"><strong>Children</strong><br>
                                      <i> (+12 yrs)</i></label>
                                      </div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner"> <span class="input-group-btn"> <a class="btn btn-danger" data-dir="dwn" id="child_dn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="child_one" id="child_one" class="form-control text-center" value="0">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="child_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="3">Premium Economy </button></div>
                              </div>
							  </div>
							 <!-- child row end -->
							 <!-- infant row -->
							  <div class="row">
                                <div class="form-group">
                                    <div class="col-md-3">
                                <label class="control-label"><strong>Infants</strong><br>
                                      <i> (0-2 yrs)</i></label>
                                      </div>
                                      <div class="col-md-5">
                                <div class="input-group number-spinner1"> <span class="input-group-btn"> <a class="btn btn-danger" id="inf_dn" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></a> </span>
                                      <input type="text"  name="infants_one" id="infants_one" class="form-control text-center" value="0">
                                      <span class="input-group-btn"> <a class="btn btn-info" id="inf_up"><span class="glyphicon glyphicon-plus"></span></a> </span> </div>
                                      </div>
									  <div class="col-md-4"><button class="flight-class" type="button" value="4"> Business </button></div>
                              </div>
							  </div>
							  <!-- infant row end -->
							  <div class="row">
							      <div class="form-group">
                                  <button class="btn btn-default btn-block demise" type="button" id="done">Done</button>
                                  </div>
                              </div>
								  </div>
								  </div>
                          </div>
                            </div>                    

                             <!--- Multi City Starts from Here --->
          <div class="row multi-city-extra" id="multi_city" style="display:none">
                        <div class="row">
                        <div class="col-xs-12 col-md-4">
                              <h3>From</h3>
                        <select class="select2 input-select"  name="from[]" required="required">
                              <option value="">Source</option>  
                                <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?> 
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php } }?>

                        </select>
                            </div>
                        <div class="col-xs-12 col-md-4">
                              <h3>To</h3>
                        <select class="select2 input-select"  name="to[]" required="required" onchange="setOrigin(this.value);">
                            <option value="">Destination</option>
                              <?php
                                    if(isset($airport))
                                    { 
                                    foreach($airport as $city) {?>
                                    <option value="<?php echo $city['CityCode'].','.$city['CountryCode'];?>"><?php echo $city['CityName'].','. $city['CityCode'];?></option>
            
                            <?php }
                                 }?>
                        </select>
                            </div>
                            


                        <div class="col-xs-12 col-md-4">
                            <div class="date">
                                <div class="depart">
                                  <h3>Depart</h3>
                                  <input  name="journey_date[]" type="text"  placeholder="mm/dd/yyyy" class="datepicker"  required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>		
						</div>
                        
                        <button id="addCityMulti" class="btn btn-warning form-search-btn" type="button">Add City</button>
                        
                      </div>
                      <!---- Multi City Ends Here--->							

							
						
					
                            <input type="submit" class="btn btn-warning form-search-btn" id="test1212" value="Search">
                   
                      </div>
					  </div>
        
      </div>
    </div>
  </div>


            
        </div>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
</div>

</div>
</form>
</div>
<!--======= flights listing form ================-->
<!--================= container ================-->
<div class="container-fluid flightlisting-below">
  <div class="row side-bar">
      <div class="col-md-2 margin-top-headline side-flightfilter filter-block desktop-dhappa">



<div class="mtf-nav">
    <ul>
        @for($i=0;$i<count($filters);$i++)
        <li><a id="fly_{{($i+1)}}" href="#" segment="{{$i}}" class="mtta @if($i==0) active-mttab @endif">{{($i+1)}}</a></li>
        @endfor
    </ul>
</div>


@for($i=0;$i<count($filters);$i++)

<div id="flytab_{{($i+1)}}" class="mttabs @if($i!=0) hide-mttabs @endif">
    <div class="side-stops">
        <h3 class="fsidebar-head">Stops {{($i+1)}}</h3>
        <ul class="flight-stop-list">
        
        @foreach($filters[$i]['stops'] as $key=>$stops)    
        <li class="custom-check">
           <input type="checkbox" id="stops{{$i.$key}}" name="stops" value="{{$key}}">
           <label for="stops{{$i.$key}}"><span><i class="fa fa-check"></i></span>{{$key}} Stops</label>
           <span class="fside-counts">({{$stops}})</span>
        </li>
        @endforeach
        
        </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Departure Time</h3>
    <ul class="flight-flytime-list">
                <li class="fft-item">
                    <input type="checkbox" id="dep0" value="12:00 AM - 8:00 AM" name="departure"><label>12:00 AM - 8:00 AM</label><span class="box"><span class="check"></span></span>
    <!--<span class="fside-counts count-fftime">(10)</span>-->
                </li>
                
                <li class="fsl-item">
                    <input type="checkbox" id="dep1" value="8:00 AM - 12:00 PM" name="departure"><label>8:00 AM - 12:00 PM</label><span class="box"><span class="check"></span></span>
    <!--<span class="fside-counts count-fftime">(10)</span>-->
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="dep2" value="12:00 PM - 4:00 PM" name="departure"><label>12:00 PM - 4:00 PM</label><span class="box"><span class="check"></span></span>
    <!--<span class="fside-counts count-fftime">(10)</span>-->
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="dep3" value="4:00 PM - 8:00 PM" name="departure"><label>4:00 PM - 8:00 PM</label><span class="box"><span class="check"></span></span>
    <!--<span class="fside-counts count-fftime">(10)</span>-->
                </li>
    <li class="fsl-item">
                    <input type="checkbox" id="dep4" value="8:00 PM - 12:00 AM" name="departure"><label>8:00 PM - 12:00 AM</label><span class="box"><span class="check"></span></span>
    <!--<span class="fside-counts count-fftime">(10)</span>-->
                </li>
            </ul>
    </div>
        <div class="side-flytime">
            <h3 class="fsidebar-head">Arrival Time</h3>
    <ul class="flight-flytime-list">
        <li class="custom-check"><input type="checkbox" id="return1" name="arrival" value="12:00 AM - 8:00 AM">
           <label for="return1"><span><i class="fa fa-check"></i></span>12:00 AM - 8:00 AM</label>
           <!--<span class="fside-counts">(10)</span>-->
        </li>
        <li class="custom-check"><input type="checkbox" id="return2" name="arrival" value="8:00 AM - 12:00 PM">
           <label for="return2"><span><i class="fa fa-check"></i></span>8:00 AM - 12:00 PM</label>
           <!--<span class="fside-counts">(10)</span>-->
        </li>
        <li class="custom-check"><input type="checkbox" id="return3" name="arrival" value="12:00 PM - 4:00 PM">
           <label for="return3"><span><i class="fa fa-check"></i></span>12:00 PM - 4:00 PM</label>
           <!--<span class="fside-counts">(10)</span>-->
        </li>
        <li class="custom-check"><input type="checkbox" id="return4" name="arrival" value="4:00 PM - 8:00 PM">
           <label for="return4"><span><i class="fa fa-check"></i></span>4:00 PM - 8:00 PM</label>
           <!--<span class="fside-counts">(10)</span>-->
        </li>
        <li class="custom-check"><input type="checkbox" id="return5" name="arrival" value="8:00 PM - 12:00 AM">
           <label for="return5"><span><i class="fa fa-check"></i></span>8:00 PM - 12:00 AM</label>
           <!--<span class="fside-counts">(10)</span>-->
        </li>
            </ul>
        </div>
    <div class="side-airlines">
            <h3 class="fsidebar-head">Airlines</h3>
    <ul class="flight-airlines-list">
     
     
     @foreach($filters[$i]['airlines'] as $key=>$airline)    
        <li class="custom-check"><input type="checkbox" id="airlines{{$key}}" name="airlines" value="{{$key}}">
       <label for="airlines{{$key}}"><span><i class="fa fa-check"></i></span>{{$key}}</label><span class="fside-counts">({{$airline}})</span>
    </li>
        @endforeach   
            </ul>
        </div>
    <div class="side-airfare">
            <h3 class="fsidebar-head">Fare Range</h3>
    <ul class="flight-fare-list">
                <li class="custom-check"><input type="radio" id="price1" name="price" value="0-2000">
       <label for="price1"><span><i class="fa fa-check"></i></span>Under &#8377;2000</label><span class="fside-counts">(10)</span>
    </li>
                
                <li class="custom-check"><input type="radio" id="price2" name="price" value="2001-4000">
       <label for="price2"><span><i class="fa fa-check"></i></span>&#8377;2001-&#8377;4000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="radio" id="price3" name="price" value="4001-6000">
       <label for="price3"><span><i class="fa fa-check"></i></span>&#8377;4001-&#8377;6000</label><span class="fside-counts">(10)</span>
    </li>
    <li class="custom-check"><input type="radio" id="price4" name="price" value="6000">
       <label for="price4"><span><i class="fa fa-check"></i></span>&#8377;6000 and above</label><span class="fside-counts">(10)</span>
    </li>
    
    </ul>
        
        </div>
    </div>
    @endfor
    

  </div>

 <!--   </div>-->
 @php
 $flight_result=$flights->Response->Results[0];
 @endphp
 <div id="flight-listing" class="col-xs-12 col-md-8">
 @if($journey_type==3)
	{!! view('frontend.home.partials.flights.multi-trip',compact('flight_result','flights','cabinClass','search_info')) !!}
@elseif($journey_type==1)
	{!! view('frontend.home.partials.flights.one-way',compact('flight_result','flights','cabinClass','search_info')) !!}
@elseif($is_domestic && $journey_type==2)
	{!! view('frontend.home.partials.flights.round-dom',compact('flight_result','flights','cabinClass','search_info')) !!}
@else
	{!! view('frontend.home.partials.flights.round-intl',compact('flight_result','flights','cabinClass','search_info')) !!}
@endif
</div>
<div class="col-md-2 discount-sidebar">
    <h2>Irresistible Discounts</h2><div class="discount-block">
    <span class="discount-details"><p><i class="fa fa-rupee"></i>Get 10% discount on your first booking to Thailand. Book now!</p></span>
<span class="code-details"><strong>Code:</strong>FIRSTOFF10</span>
</div>
<div class="discount-block">
    <span class="discount-details"><p><i class="fa fa-rupee"></i>Get 10% discount on your first booking to Thailand. Book now!</p></span>
<span class="code-details"><strong>Code:</strong>FIRSTOFF10</span>
</div>
<div class="discount-block">
    <span class="discount-details"><p><i class="fa fa-rupee"></i>Get 10% discount on your first booking to Thailand. Book now!</p></span>
<span class="code-details"><strong>Code:</strong>FIRSTOFF10</span>
</div>
<div class="discount-block">
<img src="https://gifimage.net/wp-content/uploads/2018/10/best-deal-gif-6.gif">
    <span class="discount-details"><p>Try your best but finding Thailand's deals better than ours would just be a waste of time!</p></span>
</div></div>
        </div>

      </div>
    </div>
  </div>
  </div>
@if($is_domestic && $journey_type==2)
@include('frontend.includes.sticky-container') 
@endif

@include('frontend.includes.footer2')
   <script>
   var segment=0;
   </script>
   <script>
        $(document).ready(function(){
            $('#multi_city').find(':input').prop('disabled', true);
        });
    </script>
  <script>
      $(document).ready(function() {
$("#fly_1").click(function () {
    $(".mtta").removeClass("active-mttab");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active-mttab");
    $("[id^=flytab]").addClass("hide-mttabs");
    $('#flytab_1').removeClass("hide-mttabs");
   
});
});
$(document).ready(function() {
$("#fly_2").click(function () {
    $(".mtta").removeClass("active-mttab");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active-mttab");
    $("[id^=flytab]").addClass("hide-mttabs");
    $('#flytab_2').removeClass("hide-mttabs");
   
});
});
$(document).ready(function() {
$("#fly_3").click(function () {
    $(".mtta").removeClass("active-mttab");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active-mttab");
    $("[id^=flytab]").addClass("hide-mttabs");
    $('#flytab_3').removeClass("hide-mttabs");
   
});
});
$(document).ready(function() {
$("#fly_4").click(function () {
    $(".mtta").removeClass("active-mttab");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active-mttab");
    $("[id^=flytab]").addClass("hide-mttabs");
    $('#flytab_4').removeClass("hide-mttabs");
   
});
});
$(document).ready(function() {
$("#fly_5").click(function () {
    $(".mtta").removeClass("active-mttab");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active-mttab");
    $("[id^=flytab]").addClass("hide-mttabs");
    $('#flytab_5').removeClass("hide-mttabs");
   
});
});
  </script>
  <script>

$(document).ready(function(){
     
      $("#input3").on("change",function(){
         // alert("hello");
            var source = $("#input2").val();
            var destination = $("#input3").val();
           // alert(source+" "+destination);
           if(source=="" && destination==""){
                $("#input2").focus();
            }
             else if(source==destination)
            {
                  alert("Source and Destination can not be same.");
                    $('#input3 option[value=""]').prop('selected', true);
                    $("#select2-input3-container").text("Destination");
                  $("#input3").focus();
            }
           
            
      });
});
// $(function() {
//       $( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
//       });
$(document).ready(function() {
$(".flight-class").click(function () {
    
    $(".flight-class").removeClass("active");
    // $(".tab").addClass("active"); // instead of this do the below
    $('#travel_class').val($(this).val());
    $(this).addClass("active");
    // alert($(this).val());
    var passengers_count = $("#passenger_count").val();
     //alert(passengers_count);
    var t_class = travelClassInfo();
    
    $("#passengers_one").val(passengers_count+"|"+t_class);
    
    
});
});
</script> 
<script type="text/javascript">
  
    var dates = $("#datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {
    $("#datepicker1").datepicker('option', 'minDate', date);
  }
});
$("#datepicker1").datepicker({ numberOfMonths: 2,changeMonth: true,});
</script>
<script>
    var dates = $(".datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {
        $(".datepicker").closest('div.body').next().find('div.datepicker').datepicker('option', 'minDate', date);
  }
});
$(".datepicker").closest('div.body').next().find('div.datepicker').datepicker({ numberOfMonths: 2,changeMonth: true,});
</script>

<!--<script type="text/javascript">-->
  
<!--    var dates = $("#datepicker").datepicker({-->
    <!--// defaultDate: "",-->
<!--     changeMonth: true,-->
<!--    numberOfMonths: 2,-->
<!--    minDate: 0,-->
<!--    onSelect: function(date) {-->
<!--    $("#datepicker1").datepicker('option', 'minDate', date);-->
<!--  }-->
<!--});-->
<!--$("#datepicker1").datepicker({ numberOfMonths: 2,changeMonth: true,});-->
<!--</script>-->
<script>
 function travelClassInfo(){
         var travel_class= $("#travel_class").val();
        if(travel_class==""||travel_class==null)
        { travel_class="Economy"; }
        else if(travel_class==2){
            travel_class="Economy";
        }
        else if(travel_class==3){
            travel_class="Premium Economy";
        }
        else if(travel_class==4){
            travel_class="Business";
        }
        else{
            travel_class = "Economy";
        }
        return travel_class;
    }

$(document).ready(function(){
   
//increment the value of passenger
    $("#adult_up").on("click",function(){
        var ad_count = $("#adult_one").val();
        ad_count++;
        $("#adult_one").val(ad_count);
       
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
     $("#child_up").on("click",function(){
        
        var ch_count = $("#child_one").val();
        ch_count++;
        $("#child_one").val(ch_count);
        
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
    $("#inf_up").on("click",function(){
        var inf_count = $("#infants_one").val();
        inf_count++;
        $("#infants_one").val(inf_count);
        
        var t_class = travelClassInfo();
        
        var passengers_count = $("#passenger_count").val();
        passengers_count++;
        $("#passenger_count").val(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
//decrement the value of passenger
     $("#adult_dn").on("click",function(){
        var ad_count = $("#adult_one").val();
        ad_count--;
        
        if(ad_count>=1){
        $("#adult_one").val(ad_count);
         var passengers_count = $("#passenger_count").val();
        passengers_count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
       // alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
     $("#child_dn").on("click",function(){
        var ch_count = $("#child_one").val();
        ch_count--;
        
       if(ch_count>=0){
        $("#child_one").val(ch_count);
         var passengers_count = $("#passenger_count").val();
        passengers_count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
        //alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
    $("#inf_dn").on("click",function(){
        var inf_count = $("#infants_one").val();
        inf_count--;
        
        if(inf_count>=0){
        $("#infants_one").val(inf_count);
         var passengers_count = $("#passenger_count").val();
        passengers_count--;
        }
        else{
             var passengers_count = $("#passenger_count").val();
        }
        
        var t_class = travelClassInfo();
        
        $("#passenger_count").val(passengers_count);
        //alert(passengers_count);
        $("#passengers_one").val(passengers_count+"|"+t_class);
    });
});
</script>
<script type="text/javascript">
    $('#passengers_one').on('click',function(){
       $('#contactForm').show(); 
    });
</script>
<script type="text/javascript">
    $('#done').on('click',function(){
       $('#contactForm').hide(); 
    });
</script>
<!---->
<script>
  $("#datepicker1").prop('disabled',true);
  $("#multi-trip-hide").show();
   $('.multi-trip-show').hide(); 
    $("a[href='#one_way']").on("click",function(){
         $("#multi-trip-hide").show();
          $('.multi-trip-show').hide(); 
          $('.multi-city-extra').hide();
      $("#datepicker1").prop('disabled',true);   
    });
</script>
<script>
    $("a[href='#round_trip']").on("click",function(){
         $("#multi-trip-hide").show();
          $('.multi-trip-show').hide(); 
          $('.multi-city-extra').hide();
      $("#datepicker1").prop('disabled',false);   
    });

</script>
<script>
if (window.matchMedia('(max-width: 768px)').matches)
{
        $("a[href='#one_way']").on("click",function(){
         $("#multi-trip-hide").addClass("desktop-dhappa");
         $("#multi-trip-hide").removeClass("mobile-dhappa");
    });
        $("a[href='#round_trip']").on("click",function(){
        $("#multi-trip-hide").removeClass("desktop-dhappa");
        $("#multi-trip-hide").addClass("mobile-dhappa");
        });
} 
</script>
<script type="text/javascript">
    $("a[href='#multi_trip']").on('click',function(){
        $("#multi-trip-hide").hide();
       $('.multi-trip-show').show(); 
       $('.multi-city-extra').show();
       $('#multi_city').find(':input').prop('disabled', false);
    });
</script>
<!---->


<!--<script type="text/javascript">-->
<!--    $("a[href='#multi_trip']").on('click',function(){-->
<!--        $("#multi-trip-hide").hide();-->
<!--       $('.multi-trip-show').show(); -->
<!--    });-->
<!--</script>-->
<script>
    $('.select2').select2()
.on("select2:open", function () {
    $('.select2-results__options').niceScroll({
       cursorcolor: "#5fdfe8",
        cursorwidth: "8px",
        autohidemode: false,
        cursorborder: "1px solid #5fdfe8",
        horizrailenabled: false,
    });
});
</script>
<script>
var count=0
var row='<div class="row addition-multi-city"><div class="col-xs-12 col-md-2"><h3>From</h3><select class="select2 input-select"  name="from[]" required="required"><option value="">Source</option> <?php if(isset($airport)) { foreach($airport as $city) {?> <option value="<?php echo $city['CityCode'].",".$city['CountryCode'];?>"><?php echo $city['CityName'].",". $city['CityCode'];?></option><?php } }?></select></div><div class="col-xs-12 col-md-2"><h3>To</h3><select class="select2 input-select"  name="to[]" required="required" onchange="setOrigin(this.text);"><option value="">Destination</option><?php if(isset($airport)){ foreach($airport as $city) {?><option value="<?php echo $city['CityCode'].",".$city['CountryCode'];?>"><?php echo $city['CityName'].",". $city['CityCode'];?></option><?php }}?></select></div><div class="col-xs-12 col-md-2"><div class="date"><div class="depart"><h3>Depart</h3><input class="datepicker" name="journey_date[]" type="text"  placeholder="mm/dd/yyyy"   required></div><div class="clear"></div></div></div></div>';                                       
$('#addCityMulti').on('click',function() {
    if(count<3)
    {    
    $('#addCityMulti').before(row);
    $('.select2').select2()
.on("select2:open", function () {
    $('.select2-results__options').niceScroll({
       cursorcolor: "#5fdfe8",
        cursorwidth: "8px",
        autohidemode: false,
        cursorborder: "2px solid #5fdfe8",
        horizrailenabled: false,
    });
});

    var dates = $(".datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {

    $(".datepicker").closest('div.body').next().find('div.datepicker').datepicker('option', 'minDate', date);
  }
});
$(".datepicker").closest('div.body').next().find('div.datepicker').datepicker({ numberOfMonths: 2,changeMonth: true,});
    //$('.multi-city-extra').append(row);

    count++;
    }
    else
    {
        $('#addCityMulti').hide();
        swal({
  type: 'error',
  title: 'Oops...',
  text: 'You can add maximum cities!',
  footer: '<a href>Why do I have this issue?</a>'
})
    }
    //$('.multi-trip-show:last').before('<div class="row" class="multi-trip-show" style="display:none"><div class="col-xs-12 col-md-2"><h3>From</h3><input id="input2" type="text" name="from" id "single" class="form-control"></div><div class="col-xs-12 col-md-2"><h3>To</h3><input id="input3" type="text" name="to"  class="form-control"></div><div class="col-xs-12 col-md-2"><div class="date"><div class="depart"><h3>Depart</h3><input  id="datepicker" name="depart_date" type="text" value="mm/dd/yyyy" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = "mm/dd/yyyy";}" required></div></div></div><span class="remove">Remove Option</span></div>');
});
// $('.multi-trip-show').on('click','.remove',function() {
//  	$(this).parent().remove();
// });
</script>
<script>

$(document).click(function(e){

    // Check if click was triggered on or within #menu_content
    if( $(e.target).closest("#passengers_one").length > 0 || $(e.target).closest("#contactForm").length>0 ) {
        return false;
    }
    else
    {
        $('#contactForm').css('display','none'); 
    }
    })

</script>

<script>
$(document).ready(function(){
    
    $('.mtta').on('click',function(){
       segment=$(this).attr('segment');
   });
    
    $('input[type="checkbox"]').click(function(){
        let stops=[];
        let airlines=[];
        let depTime=[];
        let arrTime=[];
        let prices=$('input[name="price"]:checked').val();
        $('input[name="stops"]:checked').each(function() {
            stops.push(this.value);
            });
            $('input[name="airlines"]:checked').each(function() {
            airlines.push(this.value);
            });
            $('input[name="departure"]:checked').each(function() {
            depTime.push(this.value);
            });
            $('input[name="arrival"]:checked').each(function() {
            arrTime.push(this.value);
            });
          
    $.ajax({
        method:"POST",
        url:"{{route('frontend.fliter.flights')}}",
        data:{"_token": "{{ csrf_token() }}",'stops':stops,'prices':prices,'airlines':airlines,'depTime':depTime,'arrTime':arrTime,'segment':segment},
        success:function(data){
            //alert(data);
            $('#flight-listing').html(data);
        },
        error:function(data){
            alert(data.responseText);
        }

    });
   
    
});

});

   $('input[name="price"]').on('click',function(){
       let stops=[];
        let airlines=[];
        let depTime=[];
        let arrTime=[];
        let prices=$(this).val();
        $('input[name="stops"]:checked').each(function() {
            stops.push(this.value);
            });
            $('input[name="airlines"]:checked').each(function() {
            airlines.push(this.value);
            });
            $('input[name="departure"]:checked').each(function() {
            depTime.push(this.value);
            });
            $('input[name="arrival"]:checked').each(function() {
            arrTime.push(this.value);
            });

          $.ajax({
        method:"POST",
        url:"{{route('frontend.fliter.flights')}}",
        data:{"_token": "{{ csrf_token() }}",'stops':stops,'prices':prices,'airlines':airlines,'depTime':depTime,'arrTime':arrTime,'segment':segment},
        success:function(data){
            $('#flight-listing').html(data);
        },
        error:function(data){
            console.log(data.responseText);
        }

    });
       
   });
   
   
</script>

