@include('frontend.includes.header')


<section class="visa_section">
    <div class="container">


    <div class="row">
    
        <div class="col-md-8 col-xs-12" > 
        <br>
        <h2 class="beliya"><u>Visa Information</u></h2>
        
        <p align="justify" class="visa_text">Jaunt Makers Pvt Ltd service fees may apply bookings made online or via the Jaunt Makers Pvt Ltd Travel Centre. Your Thailandwale consultant can provide you full details on request. The final judgment bestows with the mission for the visa however, we maintain transparency in helping gather all the information which the mission requires to process visa. We are highly proficient in providing information to the Embassies in a timely manner and customize reports according to the Embassies requirements. In order to mitigate the risks of wrongful use of information technology, misinterpretation of facts, falsification of documents, and of unlawful persons entering partner countries, it utilizes country- wide network of experienced and erudite personnel drawn from the Indian Police Services, Parliamentary Services and premier investigative wings of the Government of India.</p>

<h4 class="visa_services_head">Services include verification of the following:</h4>

<ul class="serv_list">
<li class="serv_list_item">Birth certificate</li>
<li class="serv_list_item">Marriage certificate</li>
<li class="serv_list_item">Work experience certificate</li>
<li class="serv_list_item">Education certificate/ affidavit</li>
<li class="serv_list_item">Divorce certificate</li>
<li class="serv_list_item">Adoption</li>
<li class="serv_list_item">Asylum</li>
<li class="serv_list_item">Passport/ driving license and Identity Cards</li>
<li class="serv_list_item">Bank statements</li>
<li class="serv_list_item">Medical reports</li>
<li class="serv_list_item">Company / Trust verification</li>
<li class="serv_list_item">Criminal Background check</li>
</ul>
        
        </div>
         
         
        
     
            <div class="col-md-4 col-xs-12">
        

            <div class="widget-sidebar">
                
            <div class="boxes">
              <h3><span class="fa fa-envelope"></span> Need Our Help ?</h3>
              <h5>WE WOULD BE HAPPY TO HELP YOU!</h5>
              <h5><i class="fa fa-phone"></i> <a href="tel:+8755169330">(+91) 74043 40404</a></h5>
              <h5>info@thailandwale.com</h5>
            </div>
            
            
            <br>
          
           <div class="boxes">
              
         <form method="post">
           <br>
            
                <div class='form-row'>
                <div class='form-group required'>
                    
                <input class='form-control' placeholder="Your Name" name="name" size='20' type='text'>
                </div>
                </div>
            
               <div class='form-row'>
               <div class='form-group card required'>
               <input autocomplete='off' required="true" placeholder="Your Email" name="email" class='form-control card-number' size='20' type='email'>
               </div>
               </div>
            
              <div class='form-row'>
              <div class='form-group card required'>
             
              <input autocomplete='off' required="true" placeholder="Your Number" name="number" class='form-control card-number' size='20' type='text'>
              </div>
              </div>
            
              <div class='form-row'>
              <div class='form-group cvc required'>
             
              <input autocomplete='off' class='form-control card-cvc' name="message" placeholder="Description" size='20' type='text'>
              </div>
              </div>
    
           
           
              <div class='form-row'>
              <div class='form-group'>
              <label class='control-label'></label>
              <button class='form-control btn btn-primary visa-button' type='submit'> Send Query →</button>
              </div>
              </div>

              </form>  
              
              
          </div>
          <br>
            
            </div>



</div>
</div>
   </div>
   </section>
@include('frontend.includes.footer')