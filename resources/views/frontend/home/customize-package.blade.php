<!DOCTYPE HTML>
<html lang="en">
   <head>
      <title>Thailandwale | Customize Package</title>
      <meta charset="UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/font-awesome.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/lineicons.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/weather-icons.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/bootstrap.css')}}"/>
      <link rel="stylesheet" href="{{asset('assets/home/customize/css/styles.css')}}"/>
   </head>
 <body>
       <!--============= TOP-BAR ===========-->
        <div id="top-bar" class="tb-text-white">
            <div class="container">
                <div class="row">          
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <nav class="navbar navbar-default main-navbar navbar-custom navbar-white" id="mynavbar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" id="menu-button">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>                        
                    </button>
                  
                    <a href="{{route('frontend.index')}}" class="navbar-brand"><img src="{{asset('assets/home/images/logo-2.png')}}"></a>
                </div><!-- end navbar-header -->
                
                <div class="collapse navbar-collapse" id="myNavbar1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown active" menu-name="flights">{{ link_to_route('frontend.index', trans('navs.frontend.flights')) }}
                          			
                        </li>
                        
                        <li class="dropdown" menu-name="hotels">{{ link_to_route('frontend.hotels', trans('navs.frontend.hotels')) }}
                         			
                        </li>
                        <li class="dropdown" menu-name="holidays">{{ link_to_route('frontend.holidays', trans('navs.frontend.holidays')) }}
                           		
                        </li>
                        <li class="dropdown" menu-name="tohfa"><a href="{{route('frontend.giftavacation')}}">Tohfa</a> 
                           <!--<span class="le-bc-badge"><img src="{{asset('assets/home/images/new-png-logo-1.png')}}"></span>		-->
                        </li>
                        <li class="dropdown" menu-name="elite"><a href="#">Elite Travellers</a> 
                           <!--<span class="le-bc-elite-badge"><img src="{{asset('assets/home/images/new-png-logo-1.png')}}"></span>		-->
                        </li>
                        <li class="dropdown" menu-name="more">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        More
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu more-links">
                        
                        <li>
                            <a class="btn btn-block" href="{{route('frontend.visa')}}">{{trans('navs.frontend.visa') }}</a>
                                            </li>
                        <li>
                            <a class="btn btn-block" href="{{route('frontend.activity','Transfers')}}">{{trans('navs.frontend.cabs') }}</a>
                        </li>                        <li>
                            <a class="btn btn-block" href="{{route('frontend.activity','Entertainment')}}">{{trans('navs.frontend.entertainment') }}</a>
                        </li>                </ul>
                </li>
                           			
                        
                     
                       
                    </ul>
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </nav><!-- end navbar -->
                        </div>
                    
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        
                        <div id="links">
                            <ul class="list-unstyled list-inline">
						 <li class="dropdown">
						     <a href="{{route('frontend.refer_and_earn')}}"><span class="fa fa-rupee"></span>{{trans('navs.frontend.refer_and_earn') }}</a>
                           		
                        </li>
                        <li class="dropdown">
                            <a href="{{route('frontend.support')}}"><span class="fa fa-headphones"></span>{{trans('navs.frontend.support') }}</a>
                           		
                        </li>
                                
								@if(!access()->user())
                                <li><a href="#" data-toggle="modal" data-target="#signin"><span><i class="fa fa-lock"></i></span>Login</a></li>
                                <li><a href="#" data-toggle="modal" data-target="#signup"><span><i class="fa fa-user-plus"></i></span>Sign Up</a></li>
                                @else
                                
                                
                                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="fa fa-user"></span>
                        <strong>Hey, {{access()->user()->first_name}}</strong>
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </a>
                    <ul class="dropdown-menu menu-ka-drop">
                        
                        <li>
                                            <a href="{!! route('frontend.user.account') !!}" class="btn btn-block mcount-bt">My Account</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">My Trips</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">Cancellation</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">My Bookings</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">Wallet</a>
                        </li>
                        <li>
                                            <a href="#" class="btn btn-block mcount-bt">Refer & Earn</a>
                        </li>
                        <li>
                                            <a href="{!! route('frontend.auth.logout') !!}" class="btn btn-block mcount-bt">Logout</a>
                        </li>
                    </ul>
                </li>
                
                                
                                @endif
            
                                
                                
                            </ul>
                            
                        </div><!-- end links -->
                    </div><!-- end columns -->				
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end top-bar -->
      <div class="_pos-r">
         <div class="theme-hero-area _h-desk-100vh">
            <div class="theme-hero-area-bg-wrap">
               <div class="theme-hero-area-bg" style="background-image:url({{asset('assets/home/customizeimages/banner.jpg')}});"></div>
               <div class="theme-hero-area-mask theme-hero-area-mask-half"></div>
               <div class="theme-hero-area-inner-shadow"></div>
            </div>
             <form method="post" action="{{route('frontend.package_details')}}" id="package_form">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="no_of_rooms" id="no_of_rooms" value="1">
            <div class="theme-hero-area-body">
               <div class="container">
                  <div class="row _pt-150 _pv-mob-50" data-gutter="60">
                     <div class="col-md-7">
                        <div class="theme-search-area-tabs">
                           <div class="tabbable">
                              <div class="tab-content _p-25 _bg-b-04 _br-4">
                                 <div class="tab-pane active" id="SearchAreaTabs-1" role="tab-panel">
                                    <div class="theme-search-area theme-search-area-vert theme-search-area-white">
                                       <div class="theme-search-area-form">
                                          <div class="row" data-gutter="20">
                                        
                                             <div class="col-md-6">
                                                <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-border-light">
                                                   <div class="theme-search-area-section-inner">
                                                      <i class="theme-search-area-section-icon lin lin-location-pin"></i>
                                                      <input class="theme-search-area-section-input typeahead destination" type="text" id="origin" name="destination[]" placeholder="Serch your departure city" data-provide="typeahead"/>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-md-6 ">
                                                <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light">
                                                   <div class="theme-search-area-section-inner">
                                                      <i class="theme-search-area-section-icon lin lin-calendar"></i>
                                                      <input class="theme-search-area-section-input datePickerEnd _mob-h"  placeholder="Select Date"value="" id="date"  type="text" placeholder="Check-out"/>
                                                      <input class="theme-search-area-section-input _desk-h mobile-picker" placeholder="Select Date" value="" type="date"/>

                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-md-12">
                                                <div class="theme-search-area-section first theme-search-area-section-curved theme-search-area-section-border-light">
                                                   <div class="theme-search-area-section-inner">
                                                      <i class="theme-search-area-section-icon lin lin-location-pin"></i>
                                                      <input class="theme-search-area-section-input typeahead" type="text" id="destination"  placeholder="Add city you wish to travel" data-provide="typeahead"/>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <input type="hidden" name="depart_date[]" id="depart_date" class="depart_date">
                                          <div class="row" data-gutter="20" id="test">
                                             <div class="col-md-12">
                                                <div class="fullbacken">
                                                   <h5>Mention Number of Rooms &amp; Travellers</h5>
                                                   <div class="row kingpas" id="kingpas1">
                                                      <div class="col-md-2 col-sm-2">
                                                         <span class="fonw">Room 1:</span>
                                                      </div>
                                                      <div class="col-md-3 col-sm-3">
                                                         <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light quantity-selector" data-increment="Rooms">
                                                            <label class="theme-search-area-section-label">Adult: (12+ Yrs)</label>
                                                            <div class="theme-search-area-section-inner">
                                                               <i class="theme-search-area-section-icon lin lin-tag"></i>
                                                               <input class="theme-search-area-section-input" value="1" type="text" name="adults[]">
                                                               <div class="quantity-selector-box" id="HotelSearchRooms">
                                                                  <div class="quantity-selector-inner">
                                                                     <p class="quantity-selector-title">Adults</p>
                                                                     <ul class="quantity-selector-controls">
                                                                        <li class="quantity-selector-decrement">
                                                                           <a href="#">-</a>
                                                                        </li>
                                                                        <li class="quantity-selector-current">1</li>
                                                                        <li class="quantity-selector-increment">
                                                                           <a href="#">+</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-md-3  col-sm-3">
                                                         <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light quantity-selector" data-increment="Rooms">
                                                            <label class="theme-search-area-section-label">Child: (2-11 Yrs)</label>
                                                            <div class="theme-search-area-section-inner">
                                                               <i class="theme-search-area-section-icon lin lin-tag"></i>
                                                               <input class="theme-search-area-section-input" value="0" type="text" name="children[]">
                                                               <div class="quantity-selector-box" id="HotelSearchRooms">
                                                                  <div class="quantity-selector-inner">
                                                                     <p class="quantity-selector-title">Children</p>
                                                                     <ul class="quantity-selector-controls">
                                                                        <li class="quantity-selector-decrement">
                                                                           <a href="#">-</a>
                                                                        </li>
                                                                        <li class="quantity-selector-current">0</li>
                                                                        <li class="quantity-selector-increment">
                                                                           <a href="#" class="child_age">+</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-md-3  col-sm-3">
                                                         <div class="theme-search-area-section theme-search-area-section-curved theme-search-area-section-border-light quantity-selector" data-increment="Rooms">
                                                            <label class="theme-search-area-section-label">Infant: (0-2 Yrs)</label>
                                                            <div class="theme-search-area-section-inner">
                                                               <i class="theme-search-area-section-icon lin lin-tag"></i>
                                                               <input class="theme-search-area-section-input" value="0" type="text" name="infants[]">
                                                               <div class="quantity-selector-box" id="HotelSearchRooms">
                                                                  <div class="quantity-selector-inner">
                                                                     <p class="quantity-selector-title">Rooms</p>
                                                                     <ul class="quantity-selector-controls">
                                                                        <li class="quantity-selector-decrement">
                                                                           <a href="#">-</a>
                                                                        </li>
                                                                        <li class="quantity-selector-current">0</li>
                                                                        <li class="quantity-selector-increment">
                                                                           <a href="#">+</a>
                                                                        </li>
                                                                     </ul>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>

                                                      


                                                   </div>
                                                   <div class="col-md-12 text-right">
                                                      <a href="#" class="addnew">Add New Room</a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row" data-gutter="20">
                                             <div class="col-md-5">
                                                <button type="button" class="theme-search-area-submit _mt-10 theme-search-area-submit-curved" data-toggle="modal" data-target="#myModal">Find Details</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5">
                        <div class="owl-carousel mt100" data-items="1" data-loop="true" data-nav="false" data-autoplay="3000">
                           <div class="theme-testimonial _c-w">
                              <div class="theme-hero-text theme-hero-text-white">
                                 <div class="theme-hero-text-header">
                                    <h2 class="theme-hero-text-title">Customize Your Trip to Thailand
                                    </h2>
                                    <p class="theme-hero-text-subtitle">Our package customizer is for those Thailand lovers who know how they can enjoy their trip to Thailand to the fullest. Choose the hotels, flights and destinations of your choice. Amazing, isn't it?</p>
                                 </div>
                                 <a class="btn _tt-uc _mt-20 btn-white btn-ghost btn-lg" href="{{route('frontend.holidays')}}">View Our Packages</a>
                              </div>
                           </div>
                           <div class="theme-testimonial _c-w">
                              <div class="theme-hero-text theme-hero-text-white">
                                 <div class="theme-hero-text-header">
                                    <h2 class="theme-hero-text-title">Get access
                                       <br>to our secret deals
                                    </h2>
                                    <p class="theme-hero-text-subtitle">Subscribe now and unlock our secret deals. Save up to 70% by getting access to our special offers for hotels, flights, cars, vacation rentals and travel experiences.</p>
                                 </div>
                                 <a class="btn _tt-uc _mt-20 btn-white btn-ghost btn-lg" href="#">View Our Packages</a>
                              </div>
                           </div>
                           <div class="theme-testimonial _c-w">
                              <div class="theme-hero-text theme-hero-text-white">
                                 <div class="theme-hero-text-header">
                                    <h2 class="theme-hero-text-title">Get access
                                       <br>to our secret deals
                                    </h2>
                                    <p class="theme-hero-text-subtitle">Subscribe now and unlock our secret deals. Save up to 70% by getting access to our special offers for hotels, flights, cars, vacation rentals and travel experiences.</p>
                                 </div>
                                 <a class="btn _tt-uc _mt-20 btn-white btn-ghost btn-lg" href="#">View Our Packages</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Your Search Result</h4>
               </div>
               <div class="modal-body">
                  <div class="theme-search-results">
                     <div class="theme-search-results-item _mb-10 _b-n theme-search-results-item-rounded theme-search-results-item-">

                     </div>

                  </div>
               </div>
               <div class="modal-footer">
                  <a href="#" class="rullybutton">Get More Details</a>
               </div>
            </div>
         </div>
      </div>
      </form>

      <script src="{{asset('assets/home/customize/js/jquery.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/moment.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/bootstrap.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/owl-carousel.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/blur-area.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/icheck.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/magnific-popup.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/ion-range-slider.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/sticky-kit.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/smooth-scroll.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/fotorama.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/bs-datepicker.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/typeahead.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/quantity-selector.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/countdown.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/window-scroll-action.js')}}"></script>
      <script src="{{asset('assets/home/customize/js/custom.js')}}"></script>
   </body>
</html>

