<head>
<script>
	window.onload = function() {
		var d = new Date().getTime();
		document.getElementById("tid").value = d;
	};
</script>
</head>
@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')

@if(!empty($errors->first()))
<div class=' alert alert-danger'>{{$errors->first()}}</div>
@endif

<section class="insurance_field">
<div id="HotelBooking" class="row">
<div id="PassengerDetail" class="pasflightdet">                
                <form id="signupForm" method="post" action="{{route('frontend.book_hotel')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @if(isset($block_room_info))
                    @foreach($block_room_info as $room)
                    <input type="hidden" id="categoryIndex" name="categoryIndex" value="0">
                    <div class="col-md-8" id="mobile_pax_detail">
                        <div class="ht_detlcontainer hotel-detail-card">
                                <div class="col-md-8"><a class="visited" href="#"><h1 class="htd_head">{{$room->HotelName}}</h1> <span class="city city_wdth"> ({{$room->AddressLine1}},{{$room->AddressLine2}})

                                <input type="hidden" name='HotelCode' value="{{$hotel_code}}">
                                
                                <input type="hidden" name='HotelName' value="{{$room->HotelName}}">    
                                <input type="hidden" name="result_index" value={{$result_index}}>
                                <input type="hidden" name="trace_id" value={{$trace_id}}> 
                                <input type="hidden" name="tid" id="tid" readonly />                                                                                                                                                             </span></a>
                                    <!--<a href="#" id="chooseAnother" class="htd_anhotel">Choose Another Hotel</a>-->
                                   
                                </div>
                                
                                <?php
                                    $d=explode('/',$search_info['CheckInDate']);
                                    $date=$d[2].'-'.$d[1].'-'.$d[0];
                                    $day=$search_info['NoOfNights'];
                                    
                                    $date=new DateTime($date);
                                    $check_out= $date->add(new DateInterval('P'.$day.'D'))->format('d F, Y');
                                    $d=explode('/',$search_info['CheckInDate']);
                                    $date=$d[2].'-'.$d[1].'-'.$d[0];
                                    $check_in=(new DateTime($date))->format('d F, Y');
                                    
                                    
                
                                    ?>
                                
                                <div class="col-md-4">
                                    <p> Check In : <span>{{$check_in}}</span></p>
                                    
                                    <p> Check Out : <span>{{$check_out}}</span></p>
                                    <p> Nights : <span>{{$search_info['NoOfNights']}} </span></p>
                                </div>
                        </div>  
                        <!-- mobile pax top container start-->

                 
                        <!-- mobile pax top container end-->
                            <div class="col-md-12" id="paxroom_detail">
                                <table cellpadding="0" cellspacing="0" width="100%" class="htd_table">
                                    <tbody><tr>
                                        <th width="90" align="left">Rooms</th>
                                        <th align="left">Room Type</th>
                                        <th width="180" align="left">No. of Guests</th>
                                    </tr>
                                        <?php $i=1;
                                        ?>
                                        @foreach($room->HotelRoomsDetails as $rm)

                                                    <!--////////////////////////////////////////////////////////-->
                                                    <input type="hidden" name="RoomIndex[]" value="{{$rm->RoomIndex}}" >
                                                    <input type="hidden" name="RatePlanCode[]" value="{{$rm->RatePlanCode}}" >
                                                    <input type="hidden" name="RoomTypeCode[]" value="{{$rm->RoomTypeCode}}" >
                                                    <input type="hidden" name="RoomTypeName[]" value="{{$rm->RoomTypeName}}" >
                                                    <input type="hidden" name="SmokingPreference[]" value="{{$rm->SmokingPreference}}" >
                                                    <!-- price fhskhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhk-->
                                                    <!--{{$rm->Price->CurrencyCode}}-->
                                                    <input type="hidden" name="CurrencyCode[]" value="{{$rm->Price->CurrencyCode}}" >
                                                    <input type="hidden" name="RoomPrice[]" value="{{$rm->Price->RoomPrice}}" >
                                                    <input type="hidden" name="Tax[]" value="{{$rm->Price->Tax}}" >
                                                    <input type="hidden" name="ExtraGuestCharge[]" value="{{$rm->Price->ExtraGuestCharge}}" >
                                                    <input type="hidden" name="ChildCharge[]" value="{{$rm->Price->ChildCharge}}" >
                                                    <input type="hidden" name="OtherCharges[]" value="{{$rm->Price->OtherCharges}}" >
                                                    <input type="hidden" name="Discount[]" value="{{$rm->Price->Discount}}" >
                                                    <input type="hidden" name="PublishedPrice[]" value="{{$rm->Price->PublishedPrice}}" >
                                                    <input type="hidden" name="PublishedPriceRoundedOff[]" value="{{$rm->Price->PublishedPriceRoundedOff}}" >
                                                    <input type="hidden" name="OfferedPrice[]" value="{{$rm->Price->OfferedPrice}}" >
                                                    <input type="hidden" name="OfferedPriceRoundedOff[]" value="{{$rm->Price->OfferedPriceRoundedOff}}" >
                                                    <input type="hidden" name="AgentCommission[]" value="{{$rm->Price->AgentCommission}}" >
                                                    <input type="hidden" name="AgentMarkUp[]" value="{{$rm->Price->AgentMarkUp}}" >
                                                    <input type="hidden" name="ServiceTax[]" value="{{$rm->Price->ServiceTax}}" >
                                                    <input type="hidden" name="TDS[]" value="{{$rm->Price->TDS}}" >
                                                    <!--////////////////////////////////////////////////////////////////////////////////////-->



                                        <tr>
                                            <td valign="top">Room {{$i}}</td>
                                            <td><b>{{$rm->RoomTypeName}}</b><br> 
                                                <span>
                                                    <br>
                                                   
                                                </span> </td>
                                            <td valign="top"><b>{{$search_info['RoomGuests'][$i-1]['NoOfAdults']}}</b> <span>Adult(s)</span> <b>, {{$search_info['RoomGuests'][$i-1]['NoOfChild']}}</b> <span>Child(s)</span></td>
                                        </tr>
                                        <?php $i++;?>
                                    @endforeach                                  
                                </tbody></table>
                            </div>
                        <div class="htd_head">
                            <h1>Enter Passenger Details</h1>
                        </div>
                            <div class="htd_databoxin" id="mobile_ad_child_detail">
								<?php $i=1;?>
								@foreach($search_info['RoomGuests'] as $guests)
								@for($j=1;$j<=$guests['NoOfAdults'];$j++)
                                <div class="col-md-12 htd_heading">Room {{$i}} - Guest {{$j}} <span>(Adult)</span> - <dfn>@if($j==1) Lead Passenger @endif</dfn></div>
                                    <div class="col-md-6">
                                        <select id="titleAdult" name="titleAdult[]" class="width60">
                                            <option>Mr.</option>
                                            <option>Mrs.</option>
                                            <option>Miss.</option>
                                        </select>
                                        <input class="fname error" id="firstNameAdult" name="firstNameAdult[]" placeholder="First Name" type="text" maxlength="20" required >
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <input class="name1 error" id="lastNameAdult" name="lastNameAdult[]" placeholder="Last Name" type="text" maxlength="20" required>
                                        
                                    </div>
                                    <div class="clr"></div>

                                     @endfor
										
										@if($guests['NoOfChild']>0)
										@for($j=1;$j<=$guests['NoOfChild'];$j++)
                                        <div class="htd_heading mt">Room {{$i}} - Child {{$j}} </div>
                                                <div class="col-md-6">
                                                    <select id="titleChild" name="titleChild[]" class="width60">
                                                        <option>Mr.</option>
                                                        <option>Mrs.</option>
                                                        <option>Miss.</option>
                                                    </select>
                                                    <input type="text" class="fname error" id="firstNameChild" name="firstNameChild[]" placeholder="First Name" maxlength="20" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="name1 error" id="lastNameChild" name="lastNameChild[]" placeholder="Last Name" maxlength="20" required>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Age : {{$guests['ChildAge'][$j-1]}}  <input type="hidden" class="name1 error" id="ageChild" name="ageChild[]" placeholder="Child Age" value="{{$guests['ChildAge'][$j-1]}}"></label>
                                                </div>
									@endfor
                                    @endif
                                    <?php $i++;?>
									@endforeach
									                          
                            </div>
                                
                            
                            <div class="col-md-12">
                                <span class="red mt font11">Note: <em><?php echo $block_room_info->BlockRoomResult->HotelNorms; ?></em></span>
                            </div>
                      
                     
                     
                                <div class="htd_head">
                                    <h1>Cancellation and Charges:</h1>
                                </div>
                                <div class="col-md-12">                                
                                                @php
                                                $i=1
                                                @endphp
                                                @foreach($block_room_info->BlockRoomResult->HotelRoomsDetails as $room_info)
                                                    <tt class="bold">Room {{$i++}} : {{$room_info->RoomTypeName}}</tt>
                                                                                        
                                            <table cellpadding="0" cellspacing="0" width="100%" class="htd_table" style="margin-left:0.0%;">
                                                <tbody>
                                                @foreach($room_info->CancellationPolicies as $policies)
                                                    <tr>
                                                        <th align="left">Cancelled on or After</th>
                                                        <th align="left">Cancelled on or Before</th>
                                                        <th align="left">Cancellation Charges</th>
                                                    </tr>

                                                    
                                                    
                                                            <tr>
                                                                 
                                                                <td align="left">{{date('d F, Y',strtotime($policies->FromDate))}}</td>
                                                                <td align="left">{{date('d F, Y',strtotime($policies->ToDate))}}</td>
                                                                <td align="left">
                                                                      {{$policies->Charge}}% 
                                                                                                                        
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                            
                                                </tbody>
                                            </table><br>
                                            
                                            @endforeach    

                                                                                
                                    <div class="htd_frmrow">
                                        <span class="red mt font11">Note: <em>Early check out will attract full cancellation charges unless otherwise specified.</em></span>
                                    </div>
                                </div>
                        
                            <div class="htd_head">
                                <h1>Hotel Norms</h1>
                            </div>    
                            <div class="htd_databox">
                                <div class="htd_databoxin">
                                    <ol>
                                        
                                                <li><?php echo $room->HotelPolicyDetail; ?></li>
                                        
                                                
                                        
                                   
                                    </ol>
                                </div>
                            </div> 
                            @endforeach
                            @endif
                            
                        <div class="col-md-6">
                            <label for="guest">Continue as a guest</label>
                            <input type="text" id="guest" name="guest_email" placeholder="Enter your email id..." class="form-control">
                        </div>
                              <div class="col-md-6">
                                  @if(!access()->user())
                                <label for="login">Not Logged in?</label>
                                <input type="button" data-toggle="modal" data-target="#signin" id="login" value="LOG IN" name="login" class="btn" style="color:#fff;background-color:#3f51b5">
                                  @else
                                <label for="login">Proceed Payment with</label>
                                <input type="text"  id="login" value="{{access()->user()->email}}" name="user_email" class="form-control" readonly>
                                @endif
                        </div>
                        
                        <div class="col-md-12">
                            <input type="hidden" value="0" id="total_booking_cost" name="bookingCharge">
                            
                            <input id="continue" class="btn_main" style="color: #fff;" value="PAY" type="submit">
                            <!--<a href="#" id="chooseAnother2" class="fright mr mt5 htd_anhotel2">Choose Another Hotel </a>-->
                        </div>
                      
       
                    </div>
                </form>
                <script>
                    $(document).ready(function(){
                       var login_name = $("#login").attr('name'); 
                       if(login_name=="login")
                       {
                           $("#guest").prop("required",true);
                       }
                    });
                </script>
                 <!-- sign in modal -->
                        	<div class="modal fade" id="signin" role="dialog">
		
		    <ul class="signup-bhi login-ki-class dropdown-menu"  style="padding: 15px;min-width: 250px;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			
                      <li>
					  
                         <div class="row">
                            <div class="col-md-12">
                               <form method="POST" class="register-form" id="login-form" action="{{route('frontend.auth.login')}}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <div class="form-group">
                                     <label class="sr-only" for="emailInput">Email address</label>
                                     <input type="email" name="email" class="form-control" id="emailInput" placeholder="Email address" required="">
                                  </div>
                                  <div class="form-group">
                                     <label class="sr-only" for="passwordInput">Password</label>
                                     <input type="password" name="password" class="form-control" id="passwordInput" placeholder="Password" required="">
                                  </div>
                                  <div class="checkbox">
                                     <label>
                                     <input type="checkbox" name="remember"> Remember me
                                     </label>
                                  </div>
                                  <div class="form-group">
                                     <button type="submit" class="btn btn-success btn-block">Sign in</button>
                                  </div>
                               </form>
                            </div>
                         </div>
                      </li>
                      <li class="divider"></li>
                      <li>
                         <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','google')}}" id="sign-in-google">Sign Up with Google</a>
                         <a class="btn btn-primary btn-block" href="{{route('frontend.auth.social.login','facebook')}}" id="sign-in-facebook" >Sign Up with Facebook</a>
                      </li>
                   </ul>
				   </div>
                       <!-- end sign in modal -->
                <div class="col-md-3" id="mobile_queue_filter">
                    <h3 class="hopdf">Promo Code </h3>
                    <div class="deal_cont" id="lblPromoCode">
                         
                <div class="drdr_in">                        
                    <label class="promo_error" id="errorPromoCode"></label>
                        
                        <input id="PromoCode" name="PromoCode" type="text" class="promoinput fleft valid" value="" placeholder="Promo Code" maxlength="10">
                         
                    <div class="fright">
                        @if(access()->user())
                        <a id="PromoCodeApply_login" href="#" class="btn_main fright mr5">Apply</a>
                        @else
                        <a id="PromoCodeApply" href="#" class="btn_main fright mr5">Apply</a>
                        @endif

                        
                </div>
                </div>       
                       
                           
                          
                           
                </div>
                    @php
                    $total=0;
                    @endphp
						
                        <div class="deal_cont">
                            <div class="deal_head">
                                <h3 class="hopdf"> Booking Summary</h3>
                            </div>
                            <div class="deal_price">
                                <div class="drdr">
                                
                                    <div class="raterow">
                                        <label>Base Price <tt class="small_font">({{count($room->HotelRoomsDetails)}} Rooms x {{$search_info['NoOfNights']}} Nights)</tt></label>
                                        <code>Rs. <code id="basefare"> 
                                            @foreach($room->HotelRoomsDetails as $rm)
                                                  @php
                                                  $total+=$rm->Price->OfferedPriceRoundedOff;
                                                  @endphp
                                            @endforeach
                                            {{$total}}
                                        </code>  </code>   
                                    </div>
                                    
                                    <!--<div class="raterow">-->
                                    <!--    <label>Tax -->
                                    <!--    <tt class="small_font">(Offered)</tt>-->
                                    <!--    </label>-->
                                    <!--    <code><em>Rs. -->
                                    <!--              {{$rm->Price->OfferedPriceRoundedOff}}-->
                                    <!--          </em><br>-->
                                                
                                    <!--        <a id="rateblockHead-0" href="#">Rate Breakup</a>-->
                                    <!--    </code>-->
                                    <!--</div>-->
                                        <!-- Rate Break up -->
                                        <div id="rateblock-0" class="hotelsearch_popup m_ipad_ratebr iehack" style="background: #fff; border: 1px solid #D7DFF4; display: none; margin-left: -34%; margin-left: -34%; margin-top: 3%; position: absolute; width: 635px;">
                                            <div class="modalMessage">
                                                <div class="rate-breakup-days">
                                                    <ul>
                                                        <li class="one-li">&nbsp;</li>
                                                        <li>Sun</li>
                                                        <li>Mon</li>
                                                        <li>Tue</li>
                                                        <li>Wed</li>
                                                        <li>Thu</li>
                                                        <li>Fri</li>
                                                        <li>Sat</li>
                                                    </ul>
                                                </div>
                                                
                                                    <div class="rate-breakup-days-data">
                                                        <ul>
                                                            <li>Week
                                                                1
                                                            </li>
                                                            
                                                                    <li><div style="text-align: center;">-</div></li>
                                                                
                                                                    <li><div style="text-align: center;">-</div></li>
                                                                
                                                                    <li><div style="text-align: center;">-</div></li>
                                                                
                                                                    <li><span class="fleft">
                                                                            1,190.83</span>
                                                                    </li>
                                                       
                                                                
                                                                    <li><span class="fleft">
                                                                            1,190.83</span>
                                                                    </li>
                                                       
                                                                
                                                                    <li><span class="fleft">
                                                                            1,191.29</span>
                                                                    </li>
                                                       
                                                                
                                                        
                                                                    <li><div style="text-align: center;">-</div></li>

                                                            
                                                        </ul>
                                                    </div>
                                                
                                                
                                                <div class="rate-breakup-inner-head">
                                                    <p>
                                                        Rate Summary</p>
                                                    <ul>
                                                        <li class="left-section">Currency</li>
                                                        <li><b>INR </b></li>
                                                    </ul>
                                                    <ul>
                                                        <li class="left-section">Total</li>
                                                        <li><b>
                                                                {{$rm->Price->OfferedPriceRoundedOff}}</b></li>
                                                    </ul>
                                                    <ul>
                                                        <li class="left-section">Tax (+)</li>
                                                        <li><b>
                                                                0.00
                                                            </b></li>
                                                    </ul>
                                                    <ul>
                                                        <li class="left-section">Discount(-)</li>
                                                        <li><b>
                                                                0.00
                                                            </b></li>
                                                    </ul>
                                                    <ul>
                                                        <li class="left-section">Extra Guest Charge(+)</li>
                                                        <li><b>
                                                                0.00
                                                            </b></li>
                                                    </ul>
                                                    <ul>
                                                        <li class="left-section">Service Charges(+) </li>
                                                        <li><b>
                                                                0.00
                                                            </b></li>
                                                    </ul>
                                                    <ul>
                                                        <li class="left-section">No. of Rooms</li>
                                                        <li><b>
                                                                1</b></li>
                                                    </ul>
                                                    <ul>
                                                        <li class="left-section">Total Price</li>
                                                        <li><b>
                                                                    
                                                                {{$rm->Price->OfferedPriceRoundedOff}}
                                                            </b></li>
                                                    </ul>
                                                    
                                                </div>
                                                <div class="fleft width_100 align_center pt">
                                                    <a id="rateClose-0" class="btn_continue_s" href="#">Close</a>
                                                </div>
                                            </div>
                                            <div class="clr"></div>
                                        </div>
                                        <!-- Rate Break up -->

                                    
                                    <!--<div class="raterow">-->
                                    <!--    <label>No. of Rooms</label>-->
                                    <!--    <code>1</code>-->
                                    <!--</div>-->
                                </div>
                                <div class="drdr_in total">
                                    <div class="raterow" style="display:none">
                                        <label>Total <br>
                                            <dfn>({{$rm->Price->OfferedPriceRoundedOff}} X 1)</dfn></label>
                                        <code><b>Rs. 
                                                  {{$rm->Price->OfferedPriceRoundedOff}}
                                              </b></code>
                                              <br>
                                               
                                    </div>
                                    
                                    <div class="raterow" style="display:none">
                                        <label>Comm. Earned</label>
                                        <code>Rs. {{$rm->Price->AgentCommission}}</code><br>
                                           
                                    </div>
                                    
                                    <div class="raterow" >
                                        <label>TWS</label>
                                          <code>Rs. <code id="tws"> 
                                            <?php 
                                                $tws=(count($room->HotelRoomsDetails))*($search_info['NoOfNights'])*350;
                                            ?>
                                            {{$tws}}
                                        </code></code>
                                          
                                    </div>
                                    <div class="raterow" >
                                        <label>GST @tws</label>
                                      <code>Rs. <code id="gst_tws"> 
                                            <?php 
                                                $gst_tws=($tws*18)/100;
                                            ?>
                                            {{$gst_tws}}
                                        </code></code>
                                    </div>
                                </div>
                            </div>
                        </div>

                    
                     <div class="grandtotal" id="promo_div">
                        <label>Promo Discount</label>
                        <code><b id="promo_discount_amount">0</b></code>
                        <br>
                         <br>
                    </div>
                     
                    <div class="grandtotal">
                        <label>Grand Total</label>
                        <code><b id="total_fare_cost"></b></code>
                        
                          

                    </div>

                    
                    <div class="grandtotal" id="payable_div" style="display:none">
                        <label>Payable Amount</label>
                        <code><b id="payable">0</b></code>
                        
                          

                    </div>

                    
                    
                    <div id="divAgentMarkup" class="deal_price" style="display: none">
                        <div class="drdr">
                            <div id="markUp" class="raterow">
                                <label>Agent Mark Up</label>
                                <code>Rs. 0.00<br>
                                </code>
                                  
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!--===== This Sales summary is for Mobile ======-->
                <div id="mobile_rate_breack" style="display: none;">
                    <div class="mobile_sale_sum">
                        <h2>Sale Summary</h2>
                        <div class="m_sale_room_info">
								@foreach($room->HotelRoomsDetails as $rm)
                                <div class="m_room_detailss">
                                    <h3>{{$rm->RoomTypeName}}</h3>
                                    <div class="m_room_inner_c">
                                        <p>
                                        
                                    <span class="mleft"><b>Rate:</b><tt class="small_font">(Published)</tt></span>
                                       <span class="mright"> Rs.
									   {{$rm->Price->PublishedPriceRoundedOff}} </span>
                                                  
                                             <br>
                                    
                                    
                                            <span class="mleft"><b>Rate:</b>
                                            <tt class="small_font">(Offered)</tt>
                                        
                                            </span>
                                            <span class="mright"> Rs.
											{{$rm->Price->OfferedPriceRoundedOff}} </span>
                                                 
                    
                                        </p>
                                        <p>
                                            <span class="mleft">&nbsp;</span>
                                            <span class="mright"><a id="mob_ratebrakup-0" href="#">Rate breakup</a> </span>
                    
                                        </p>
                                        <p>
                                            <span class="mleft"><b>No. of Rooms:</b></span>
                                            <span class="mright">1</span>
                    
                                        </p>
                                        <div class="total_c">
                                            <p>
                                                <span class="mleft"><b>Total:</b></span>
                                                <span class="mright"> Rs.
                                                    {{$rm->Price->OfferedPriceRoundedOff}} </span>
                                                     
                                            </p>
                                            <p>
                                                <span class="mleft font11">({{$rm->Price->OfferedPriceRoundedOff}}
                                                    X 1)</span>
                                                <span class="mright">&nbsp; </span>
                    
                                            </p>

                                            
                                    <p>
                                                <span class="mleft"><b>Comm. Earned:</b></span>
                                                <span class="mright">  Rs.
                                                    {{$rm->Price->AgentCommission}} </span>
                                                     
                                            </p>                                          
                                    
                                    <p>
                                                <span class="mleft"><b>TDS:</b></span>
                                                <span class="mright">  Rs.
                                                    {{$rm->Price->TDS}} </span>
                                                       
                                            </p>
                                    
                                            
                                        </div>
                
                                    </div>
            
                                </div> 
								@endforeach
                            
                            <p class="m_grand_total">
                                <span><b>Total GST</b></span>
                                <dfn>Rs.
                                    0.00<!--</dfn-->
                                    <br>
                                             
                            </dfn></p>
                     
                            <p class="m_grand_total">
                                <span><b>Grand Total</b></span>
                                <dfn>Rs.
                                    7,384.10<!--</dfn-->
                                    <br>
                                             

                            </dfn></p>
                             
                            <p class="m_hide_details">
                                <a href="#"> <span id="divAgentMarkupHeadMobile"> +Show Details </span> </a>
                            </p>
                            <div id="divAgentMarkupMobile" class="hide_show_c" style="display: none">
                                <p><span class="mleft"><b>Agent Mark Up</b></span><span class="mright"> Rs. 0.00</span><br>
                                                
                                         </p>
                            </div>
                            
                            <p class="sale_summery_btn"><a id="mobileColse" href="#">Close</a></p>
                        </div>
    
                    </div>
                    
                        <div class="m_rate_breakup_pop" id="m_ratebreakup-0" style="display: none;">
                          
                                                 
                            <div class="modalMessage">
                                <div class="rate-breakup-days">
                                    <ul>
                                        
                                                <li style="width: 20%;">  </li>
                                            
                                            <li style="width: 20%;">Week
                                                1</li>
                                                
                                                        
                                    </ul>
                                </div>

                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Sun</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           
                                                                               -
                                                                            
                                                </span></li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Mon</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           
                                                                               -
                                                                            
                                                </span></li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Tue</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           
                                                                               -
                                                                            
                                                </span></li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Wed</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           1,190.83</span>
                                                     
                                                </li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Thu</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           1,190.83</span>
                                                     
                                                </li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Fri</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           1,191.29</span>
                                                     
                                                </li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Sat</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           
                                                        -
                                                     
                                                </span></li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                <div class="fleft width_100 align_center pt">
                                    <a id="m_rateBrakupClose-0" class="btn_continue_s" href="#">Close</a>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                                 
                    
                        <div class="m_rate_breakup_pop" id="m_ratebreakup-1" style="display: none;">
                          
                                                 
                            <div class="modalMessage">
                                <div class="rate-breakup-days">
                                    <ul>
                                        
                                                <li style="width: 20%;">  </li>
                                            
                                            <li style="width: 20%;">Week
                                                1</li>
                                                
                                                        
                                    </ul>
                                </div>

                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Sun</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           
                                                                               -
                                                                            
                                                </span></li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Mon</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           
                                                                               -
                                                                            
                                                </span></li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Tue</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           
                                                                               -
                                                                            
                                                </span></li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Wed</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           1,190.83</span>
                                                     
                                                </li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Thu</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           1,190.83</span>
                                                     
                                                </li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Fri</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           1,191.29</span>
                                                     
                                                </li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                    <div class="rate-breakup-days-data">
                                        <ul>
                                            <li style="width: 20%;"> Sat</li>
                                            
                                                <li style="width: 20%"><span class="fleft">
                                                                           
                                                        -
                                                     
                                                </span></li>
                                                       
                                            
                                                                 
                                        </ul>
                                    </div>
                                
                                <div class="fleft width_100 align_center pt">
                                    <a id="m_rateBrakupClose-1" class="btn_continue_s" href="#">Close</a>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </div>
                                 
                    
                    <div class="clr"></div>
                </div>
                   
                <div id="BlockDivOpacity" class="bg_block" style="display: none;"></div>
                <div id="BlockDivForSmallScreen" class="bg_block" style="display: none;"></div>
              
            
                <script type="text/javascript">
                    
                        var flag = 0;
                        $('.name1').bind("keyup", function () {
                            //Replace a space that happens after zero or one character
                            this.value = this.value.replace(/^([a-zA-Z]{0,1})(\s)([a-zA-Z]+)/, '$1$3');
                            //Replaces any spaces that occur after the first space
                            this.value = this.value.replace(/^([a-zA-Z]{2,})(\s+)([a-zA-Z]+)(\s+)/, '$1$2$3');
                        });

                        //Added to change Hotel-tab color  on Menu bar 
                        $("#flightId").attr('class', '');
                        $("#hotelid").attr('class', 'active');

                        //                    $.validator.setDefaults({
                        //                        submitHandler: function() { alert("submitted!"); }
                        //                    });
                        $("#signupForm").validate({
                            rules: {
                                phone: {
                                    required: true,
                                    number: true,
                                    minlength: 10,
                                    maxlength: 13
                                },
                                email: {
                                    required: true,
                                    email: true
                                }
                            },
                            errorElement: "em",
                            messages: {
                                email: "Please enter a valid email address",
                                phone: "Please enter a valid phone no."
                            }, success: function () {
                                if ($('.error').text() == "" && flag == 1) {                                    
                                        setTimeout('window.scrollTo(0, 0);', 200);
                                    }
                                
                                flag = 0;
                            }

                        });


                        $('.name1').each(function () {


                            $(this).rules('add', {
                                required: true,
                                lettersonly: true,
                                minlength: 2,
                                maxlength: 20,
                                messages: {
                                    required: "Please enter name.",
                                    lettersonly: "Please enter alphabets only.",
                                    minlength: "Please enter atleast two letter.",
                                    maxlength: "Name cannot exceed 20 characters"
                                }

                            });

                        });


                        jQuery.validator.addMethod("lettersonly", function (value, element) {
                            return this.optional(element) || /^[a-z\s]*$/i.test(value);
                        }, "Please enter alphabets and space only.");

                        $("#btnValidate").live('click', function (e) {
                            e.preventDefault();
                            var isGSTValidDetails = true ;
                            if($("#GST_Number") && $("#GST_Number").val() != null && $.trim($("#GST_Number").val()) != "" )
                            {
                                $('[id^="errorGST"]').hide();
                                $('[id^="GST_"]').each(function () {
                                    
                                    if ($.trim($('#' + this.id).val()) == "" || alphaNumericRegx.test($('#' + this.id).val()))
                                    {
                                        isGSTValidDetails = false;
                                        $('#error' + this.id).show();
                                    }
                                });                                
                            }
                            flag = 1;
                            
                            if (isGSTValidDetails) {
                                $('#signupForm').submit();
                            }
                        });

                        if ($("#chkBoxGST")) {
                            if ($('input:checkbox[id^="chkBoxGST"]').attr("checked") == "checked") {
                                $('#GSTDataBlock').show();
                            }
                            else {
                                $('#GSTDataBlock').hide();
                                $('[id^="GST_"]').val("");
                            }
                        }
                        $("#chkBoxGST").live('click', function (e) {
                            
                            if ($('input:checkbox[id^="chkBoxGST"]').attr("checked") == "checked") {
                                $('#GSTDataBlock').show();
                            }
                            else {
                                $('#GSTDataBlock').hide();
                                $('[id^="GST_"]').val("");
                            }
                        });
                        


                        // amenities start
                        $("[id^=showMore_]").live('click', function (e) {
                            var id = this.id.split('_')[1];
                            $("#showContent_" + id).hide();
                            $("#fullContent_" + id).show();
                        });
                        $("[id^=showLess_]").live('click', function (e) {
                            var id = this.id.split('_')[1];
                            $("#showContent_" + id).show();
                            $("#fullContent_" + id).hide();
                        });
                        //amenities end

                        //close block of ratebreak
                        $('[id^="rateClose"]').live('click', function (e) {
                            e.preventDefault();
                            var id = this.id.split('-');
                            $('#rateblock-' + id[1]).hide();
                        });

                        //rateblockHead-
                        $('[id^="rateblockHead-"]').click(function (e) {
                            e.preventDefault();
                            var id = $(this).attr('id');
                            var divObj = $('#' + id.replace('Head', ''));
                            divObj.slideToggle("slow");
                        });


                        $(function () {
                            $("#divAgentMarkupHead").toggle(function () {
                                $("#divAgentMarkup").slideToggle();
                                $("#divAgentMarkupHead").text("- Hide Details")
                                .stop();
                            }, function () {
                                $("#divAgentMarkup").slideToggle();
                                $("#divAgentMarkupHead").text("+ Show Details")
                                .stop();
                            });
                        });

                    
                </script>
              
                
                <script type="text/javascript">
                    var intRegex = /^\d{10}$/;
                    var emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
                    var alphaNumericRegx = /[~<>]/;

                                $('#btnYes').click(function (e) {
                                $('#confPopUp').hide();
                                $('#modalBG').hide();
                                });

                                $('#hotelNameChangeOK').click(function (e) {
                                    $('#modalHotelNameChange').hide();   
                                    $('#hotelNameChangePopUp').hide();  
                                    $('#modalBG').hide();
                                   
                                });

                                
                            $('#btnYes1').click(function (e) {
                                $('#confPopUp1').hide();
                                $('#modalBG1').hide();
                            });

                            $('#btnNo').click(function (e) {                                
                                $('#confPopUp').hide();
                                $('#modalBG').hide();
                                $('#redirectToSearchResponseFromPaxPage').attr("action", "HotelSearchResult.aspx");
                                $('#redirectToSearchResponseFromPaxPage').submit();
                            });

                            $('#btnNo1').click(function (e) {
                                //window.history.back();
                                $('#confPopUp1').hide();
                                $('#modalBG1').hide();
                                $('#redirectToSearchResponseFromPaxPage').attr("action", "HotelSearchResult.aspx");
                                $('#redirectToSearchResponseFromPaxPage').submit();
                            });
                            
                            
                            $('#chooseAnother, #chooseAnother1, #chooseAnother2').click(function (e) {                                
                                e.preventDefault();
                                $('#confPopUp').hide();
                                $('#modalBG').hide();

                                $('#redirectToSearchResponseFromPaxPage').attr("action", "HotelSearchResult.aspx");
                                $('#redirectToSearchResponseFromPaxPage').submit();
                            });
                                                        
                       
                          </script>
        <form id="redirectToSearchResponseFromPaxPage" method="post" action="">
            <input type="hidden" id="returnToSearch" name="returnToSearch" value="true">
             <input type="hidden" id="isOnBehalfBooking" name="isOnBehalfBooking" value="False">
             <input type="hidden" id="agencyID" name="agencyID" value="">
             <input type="hidden" id="agencyName" name="agencyName" value="">
             <input type="hidden" id="selectedSourcesOnHotelPaxPage" name="selectedSourcesOnHotelPaxPage" value="">
        </form>
        </div>
        </div>
        </section>
        @include('frontend.includes.footer')
        <script>
            function calculateTotalFare(){
                var basefare=$("#basefare").text();
                var tws_charge=$("#tws").text();
                var gst_tws=$("#gst_tws").text();
                var promocode_disc=$("#promo_discount_amount").text();
                
                var totalfare = parseInt(basefare)+parseInt(tws_charge)+parseInt(gst_tws)-parseInt(promocode_disc);
                $("#total_booking_cost").val(totalfare);
                $("#total_fare_cost").text(totalfare);
                
            }
            calculateTotalFare();
        </script>
        