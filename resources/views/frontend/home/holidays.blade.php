@include('frontend.includes.header')
<style>
.image {
    position:relative;
    width:100%;
    height:260px;
}
.image span img {
    width:100%;
    vertical-align:top;
}
.image:after, .image:before {
    position:absolute;
    opacity:0;
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
}
.image:after {
    content:'\A';
    width:100%; height:100%;
    top:0; left:0;
    background:rgba(0,0,0,0.6);
}
.image:before {
    content: attr(data-content);
    width:100%;
    color:#fff;
    z-index:1;
    bottom:0;
    padding:4px 10px;
    text-align:center;
    background:red;
    box-sizing:border-box;
    -moz-box-sizing:border-box;
}
.image:hover:after, .image:hover:before {
    opacity:1;
}
.image-dest {
    position:relative;
    width:100%;
    height:260px;
}
.image-dest span img {
    width:100%;
    vertical-align:top;
}
.image-dest:after, .image-dest:before {
    position:absolute;
    opacity:0;
    transition: all 0.5s;
    -webkit-transition: all 0.5s;
}
.image-dest:after {
    content:'\A';
    width:100%; height:100%;
    top:0; left:0;
    background:rgba(0,0,0,0.6);
}
.image-dest:before {
    content: attr(data-content);
    width:100%;
    height:100%;
    color:#fff;
    z-index:1;
    bottom:0;
    padding:40% 10px;
    font-family:'Merriweather', serif;
    text-align:center;
    font-size:20px;
    /* background:yellow; */
    box-sizing:border-box;
    -moz-box-sizing:border-box;
}
.image-dest:hover:after, .image-dest:hover:before {
    opacity:1;
}

.blog-tile img{
    height:200px;
    width:100%;
}

.carousel-col-theme {
    position: relative;
    min-height: 1px;
    padding: 5px;
    float: left;
}

@media (max-width: 767px)
.carousel-col-theme {
    width: 100%;
}

@media (min-width: 1200px)
.carousel-col-theme {
    width: 20%;
}
</style>

<div class="package-search-div">
<div class="container">
    <div class="flights-headline">
            <h3>Explore Thailand Like Never Before</h3>
    </div>
<div class="package-dhundo">
<form class="ye-raha-form" method="get" action="{{route('frontend.all_package')}}">
			<!--<input type="hidden" name="_token" value="NBbwckyve4wFEYXrMdmIPBD0SGC7MB1YomhZnCtC">-->
                     <div class="row">
					<!--<input type="hidden" name="_token" value="NBbwckyve4wFEYXrMdmIPBD0SGC7MB1YomhZnCtC">-->
                        
                        <div class="col-xs-12 col-md-2">
                              <h3>Choose Destination</h3>
                                <select class="select2" id='input2' name="source" required="required">
                                    <option value="">Source</option>  
                                     <?php
                                        if(isset($destination))
                                        { 
                                        foreach($destination as $city) {?> 
                                        <option value="{{'destination-'.$city->id}}">{{$city->title}}</option>
                                     <?php } } ?>

                                </select>
                              <!--<input id="input_hotel" type="text" name="dest" class="form-control immybox immybox_witharrow" placeholder="Enter City" required="" autocomplete="off">-->
                              <!--<input type="hidden" id="destination" name="destination">-->
                            </div>
                            <!--<div class="col-xs-12 col-md-2">-->
                            <!--  <h3>To</h3>-->
                            <!--  <input id="input_hotel" type="text" name="dest" class="form-control immybox immybox_witharrow" placeholder="Enter City" required="" autocomplete="off">-->
                            <!--  <input type="hidden" id="destination" name="destination">-->
                            <!--</div>-->
                        
                        
                        
                        <div class="col-xs-12 col-md-1">
                              <button type="submit" class="btn btn-warning form-search-btn">Search</button>
                            </div><div class="col-md-1"><p class="oror">Or</p></div>
<div class="col-xs-12 col-md-2" style="">
                              <a class="btn btn-warning form-search-btn" href="{{route('frontend.customize_package')}}">Customise Your Trip</a>
                            </div>

					
                      </div></form>
</div>
</div>
</div>

<section class="hol-thailand-destination">
    
    <div class="container">
          <div class="thai-destination">
<h2 style="text-align: center;" class="thai-dhead">The Irresitible Destinations</h2>
<div class="row pack-destination">
<!-------------------DESTINATION SLIDER--------------------->

		<div class="col-xs-12 col-md-12 col-centered">

			<div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="4000">
				<div class="carousel-inner">
                    @foreach($destination as $dest)
					<div class="item {{ $loop->first ? 'active' : '' }}">
						<div class="carousel-col">
							<a href="{{route('frontend.all_package',['destination', to_slug($dest->title)])}}" target="_blank">
                                <div data-content="{{$dest->title}}" class="image-dest img-responsive">    
                                    <span>
                                        <img src="{{  asset('storage/app/public/img/destination').'/'.$dest->image}}" data-src="{{'/img/destination/'.$dest->image}}" class=" img-responsive  lazyImg" alt="{{'file failed to load '.$dest->title}}" title="{{$dest->title}}"style="height: 260px;width: 100%;">
                                    </span>
                                </div>
                            </a>      
						</div>
					</div>
					@endforeach
				</div>

				<!-- Controls -->
				<div class="left carousel-control">
					<a href="#carousel" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
				</div>
				<div class="right carousel-control">
					<a href="#carousel" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>

		</div>
	
<!-------------------DESTINATION SLIDER END--------------------->

    <!-- @foreach($destination as $dest) -->
   <!-- <div class="col-sm-4">
      <div class="margin-bottom-20">
         <div class="">
            <a href="/holidays/international-tour-packages/thailand-tour-packages?src=hol-bn1" target="_blank">
            <span>
            <img src="{{  asset('storage/app/public/img/destination').'/'.$dest->image}}" data-src="{{'/img/destination/'.$dest->image}}" class=" img-responsive  lazyImg" alt="{{'file failed to load '.$dest->title}}" title="{{$dest->title}}">
            </span>
            </a>
         </div>
      </div>
   </div> -->
   <!-- @endforeach -->
  
<!-- <div  id="imageCarousel" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
   @foreach( $destination as $dest )
      <li data-target="#imageCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
   @endforeach
  </ol>
  <div class="carousel-inner" role="listbox">
    @foreach($destination as $dest)
        <div class="item {{ $loop->first ? 'active' : '' }}">
        <img class="img-responsive" src="{{  asset('storage/app/public/img/destination').'/'.$dest->image}}" alt="{{'file failed to load '.$dest->title}}" title="{{$dest->title}}">
             
       </div>
    @endforeach
  </div>
  <a class="left carousel-control" href="#imageCarousel"  role="button" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left gly-co-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#imageCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right gly-co-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div> -->
 
  
        </div>
    </div>
</div>
</section>

<section class="thai-tour-types">
    <div class="container">
      <div class="thai-tourtypes">
          <div class="reccomnede-title-second-head">
          <h2>Amazing Tours for Everyone</h2>
          </div>
          
             
          <!-------------------PACKAGE THEME SLIDER--------------------->

		<div class="col-xs-12 col-md-12 col-centered">

    <div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="4000">
    <div class="carousel-inner">
        @foreach($packageTheme as $pack)
        <div class="item {{ $loop->first ? 'active' : '' }}">
						<div class="carousel-col-theme">
                            <a href="{{route('frontend.all_package',['theme', to_slug($pack->name)])}}" target="_blank">
							    <div  data-content="{{$pack->name}}" class="image img-responsive">                                
                                    <span>
                                        <img src="{{  asset('storage/app/public/img/packagetheme').'/'.$pack->image}}" data-src="{{'/img/packagetheme/'.$pack->image}}" class=" img-responsive  lazyImg" alt="{{'file failed to load '.$pack->title}}" title="{{$pack->title}}"style="height: 260px;width: 100%;">
                                    </span>
                                </div>
                            </a>
						</div>
					</div>
        <!-- <div class="item {{ $loop->first ? 'active' : '' }}" style="cursor:pointer;" onclick='location.href="{{route('frontend.all_package','theme-'.$pack->id)}}"' >
            <div class="carousel-col">
            <a href="" target="_blank" style="display:block;">
                <div class="img-responsive">
                    
                        <span>
                            <img src="{{  asset('storage/app/public/img/packagetheme').'/'.$pack->image}}" data-src="{{'/img/packagetheme/'.$pack->image}}" class=" img-responsive  lazyImg" alt="{{'file failed to load '.$pack->name}}" title="{{$pack->name}}"style="height: 260px;width: 100%;">
                        </span>
                        <div class="reccomnede-title-second-box-hv">
                            <a href="#" class="reccomnede-title-second-anc">1</a>
                        <div class="grop-tour-rec-sec">
                        <h3>{{$pack->name}}</h3>
                        <a href="mice/">view more</a>
                        </div>
                        </div>
                    
                </div>
            </a>
            </div>
        </div> -->
        @endforeach
    </div>

    <!-- Controls -->
    <div class="left carousel-control">
        <a href="#carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
    </div>
    <div class="right carousel-control">
        <a href="#carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

</div>

<!-------------------PACKAGE THEME SLIDER END--------------------->
      </div>
      </div>
</section>

<section class="thai-travel-stories">
    <div class="container">
    <div class="thai-recblogs">
    <h2 style="text-align: center;">Thailand Stories</h2>
    <div class="row">
    @foreach($blog as $blg)
	<div class="col-md-3">
        <div class="blog-tile">
        <img src="{{  asset('storage/app/public/img/blog').'/'.$blg->featured_image}}">
            <div class="grid-header-box">
			<h2 class="grid-title entry-title"><a href="http://soledad.pencidesign.com/soledad-dark-version/find-your-way-home/">{{strlen($blg->name)>20 ? substr($blg->name,0,30).'...':$blg->name}}</a></h2>
                <div class="grid-post-box-meta">
					<span><time class="entry-date published" datetime="2017-10-16T09:42:31+00:00"> {{date('d-M-Y',strtotime($blg->publish_datetime))}}</time></span>
				</div>
							
			</div>
        </div>
   </div>
  @endforeach



</div>
    </div>
</div>
</section>

@include('frontend.includes.footer')
<script>
    $('.select2').select2()
.on("select2:open", function () {
    $('.select2-results__options').niceScroll({
       cursorcolor: "#5fdfe8",
        cursorwidth: "8px",
        autohidemode: false,
        cursorborder: "2px solid #5fdfe8",
        horizrailenabled: false,
    });
});
</script>
<script>
  $('.carousel[data-type="multi"] .item').each(function() {
	var next = $(this).next();
	if (!next.length) {
		next = $(this).siblings(':first');
	}
	next.children(':first-child').clone().appendTo($(this));

	for (var i = 0; i < 3; i++) {
		next = next.next();
		if (!next.length) {
			next = $(this).siblings(':first');
		}

		next.children(':first-child').clone().appendTo($(this));
	}
});
    </script>