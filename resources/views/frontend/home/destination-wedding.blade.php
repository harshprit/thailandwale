@include('frontend.includes.header')
<section class="visa_section">
    <div class="container">


    <div class="row">
    
        <div class="col-md-8 col-xs-12" > 
        <br>
        <h2 class="beliya"><u>Destination Wedding</u></h2>
        
        <p align="justify" class="visa_text">As a parent, you know that your child deserves the best. When your little one is all set to get married, why not give them a wedding worth a thousand lifetimes? At Thailand Wale, we help you plan out destination weddings that will live on in your memory for years to come. From handling venue arrangements, caterers, décor, hotels, and flights, to making sure that everything runs smoothly once you arrive in Thailand, we do it all. 
</br></br>
A destination wedding is one of the best ways to celebrate such an important union. It gives the couple a chance to see a brand new place, while the families also bond during their travels. If you are considering having a destination wedding, get in touch with any of our expert holiday and wedding planners! While you are at it, do have a look at all the packages we offer!  
</p>
        
        </div>
         
         
        
     
            <div class="col-md-4 col-xs-12">
        

            <div class="widget-sidebar">
                
            <div class="boxes">
              <h3><span class="fa fa-envelope"></span> Need Our Help ?</h3>
              <h5>WE WOULD BE HAPPY TO HELP YOU!</h5>
              <h5><i class="fa fa-phone"></i> <a href="tel:+8755169330">(+91) 74043 40404</a></h5>
              <h5>info@thailandwale.com</h5>
            </div>
            
            
            <br>
          
           <div class="boxes">
              
         <form method="post">
           <br>
            
                <div class='form-row'>
                <div class='form-group required'>
                    
                <input class='form-control' placeholder="Your Name" name="name" size='20' type='text'>
                </div>
                </div>
            
               <div class='form-row'>
               <div class='form-group card required'>
               <input autocomplete='off' required="true" placeholder="Your Email" name="email" class='form-control card-number' size='20' type='email'>
               </div>
               </div>
            
              <div class='form-row'>
              <div class='form-group card required'>
             
              <input autocomplete='off' required="true" placeholder="Your Number" name="number" class='form-control card-number' size='20' type='text'>
              </div>
              </div>
            
              <div class='form-row'>
              <div class='form-group cvc required'>
             
              <input autocomplete='off' class='form-control card-cvc' name="message" placeholder="Description" size='20' type='text'>
              </div>
              </div>
    
           
           
              <div class='form-row'>
              <div class='form-group'>
              <label class='control-label'></label>
              <button class='form-control btn btn-primary visa-button' type='submit'> Send Query →</button>
              </div>
              </div>

              </form>  
              
              
          </div>
          <br>
            
            </div>



</div>
</div>
   </div>
   </section>
@include('frontend.includes.footer')
@include('frontend.includes.footer')