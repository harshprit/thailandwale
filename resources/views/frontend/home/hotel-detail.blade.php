@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')
@if(session()->get('flash_error'))
<div class=' alert alert-danger'>{{session()->get('flash_error')}}</div>
@endif

@if(!empty($errors->first()))
<div class=' alert alert-danger'>{{$errors->first()}}</div>
@endif
<!-- end page-cover -->
<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
  <div id="hotel-details" class="innerpage-section-padding" style="    margin-top: 55px;">
      <div class="container">
          <div class="hotel-ki-detail">
               @if(isset($hotel_info))
                    @foreach($hotel_info as $hotel)
                    <div class="col-md-8">
                    <div class="col-md-6">
                        <h2 class="hotel-ka-naam">{{$hotel->HotelDetails->HotelName}}</h2>
                    </div>
                <div class="col-md-4">
                <div class="hotel-star-rat">
                    <span class="standard">
                @for($i=0; $i < 5; $i++)
                    @if($i < $hotel->HotelDetails->StarRating)
                        <i class="fa fa-star"></i>
                    @else
                        <i class="fa fa-star-o"></i>
                    @endif
                  
                  
                @endfor
                </span>
                <!--<span class="delux" style="display:none">-->
                <!--  <i class="fa fa-star"></i>-->
                <!--  <i class="fa fa-star"></i>-->
                <!--  <i class="fa fa-star"></i>-->
                <!--  <i class="fa fa-star"></i>-->
                <!--  <i class="fa fa-star-o"></i>-->
                <!--</span>-->
                <!--<span class="premium" style="display:none">-->
                <!--  <i class="fa fa-star"></i>-->
                <!--  <i class="fa fa-star"></i>-->
                <!--  <i class="fa fa-star"></i>-->
                <!--  <i class="fa fa-star"></i>-->
                <!--  <i class="fa fa-star"></i>-->
                <!--</span>-->
                
              </div>
</div>
    <div class="hotel-ka-pata">
        <span>{{$hotel->HotelDetails->Address}}</span>
    </div>
    </div>
    <div class="col-md-4 hkrsws">
        <div class="col-md-7 col-xs-6 hotelprice-kidetail">
        <span class="hotel-discount-price">
        
        </span>
        <span class="hotel-asli-price">
            <span class="room-ka-type"></span> - <span class="offered_price"> </span>
            </span><br>
            Per Room/Night
            </div>
        <div class="col-md-5">
           <a href="#available-rooms"><button id="hotelBookkaro" href="#">Select Room</button></a> 
        </div>
        
    </div>
    @endforeach
    @endif
    </div>
         <div class="row">
            <!-- end columns -->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 content-side">
                    @if(isset($hotel_info))
                    @foreach($hotel_info as $hotel)
                    <form method="post" id="hotel_block" action="{{route('frontend.block_hotel')}}">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                                   <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner">
                                         <?php $i=1?>
                                         @foreach($hotel->HotelDetails->Images as $img)
                                         @if($i==1)
                                         <div class="item active">
                                            <img src="{{$img}}">
                                         </div>
                                         <!-- End Item -->
                                         @else
                                         <div class="item">
                                            <img src="{{$img}}">
                                         </div>
                                         <!-- End Item -->
                                         @endif
                                         <?php $i++;?>
                                         @endforeach   
                                         <!-- End Item -->
                                      </div>
                                      <!-- End Carousel Inner -->
                                      <ul class="nav nav-pills nav-justified">
                                         <?php $i=1?>
                                         @foreach($hotel->HotelDetails->Images as $img)
                                         @if($i==1)
                                         <li data-target="#myCarousel" data-slide-to="0" class="active"><a href="#">  <img src="{{$img}}" style="width: 140px;"></a></li>
                                         @else
                                         <li data-target="#myCarousel" data-slide-to="{{$i}}"><a href="#">  <img src="{{$img}}" style="width: 140px;"></a></li>
                                         @endif
                                         <?php $i++;?>
                                         @endforeach   
                                      </ul>
                                   </div>
                                   <!-- End Carousel -->
                                   <div class="detail-tabs over-ka-flow">
                                      <ul class="nav nav-tabs nav-justified">
                                         <li class="active"><a href="#hotel-overview" data-toggle="tab">Hotel Overview</a></li>
                                         <li><a href="#restaurant" data-toggle="tab">Attractions</a></li>
                                         <li><a href="#pick-up" data-toggle="tab">Hotel Policy</a></li>
                                         <li><a href="#luxury-gym" data-toggle="tab">Hotel Facilities</a></li>
                                         <li><a href="#sports-club" data-toggle="tab">Special Instructions</a></li>
                                      </ul>
                                      <div class="tab-content illegal-weapon">
                                         <div id="hotel-overview" class="tab-pane in active">
                                            <div class="row">
                                               <div class="col-sm-12 col-md-12 tab-text">
                                                  <h3>Hotel Overview</h3>
                                                  <p><?php echo $hotel->HotelDetails->Description; ?></p>
                                               </div>
                                               <!-- end columns -->
                                            </div>
                                            <!-- end row -->
                                         </div>
                                         <!-- end hotel-overview -->
                                         <div id="restaurant" class="tab-pane">
                                            <div class="row">
                                               <div class="col-sm-12 col-md-12 tab-text">
                                                  <h3>Attractions</h3>
                                                  @if(isset($hotel->HotelDetails->Attractions))
                                                  @foreach($hotel->HotelDetails->Attractions as $atr)
                                                  @foreach($atr as $at)
                                                  <li><?php echo $at; ?></li>
                                                  @endforeach
                                                  @endforeach
                                                  @endif
                                               </div>
                                               <!-- end columns -->
                                            </div>
                                            <!-- end row -->
                                         </div>
                                         <!-- end restaurant -->
                                         <div id="pick-up" class="tab-pane">
                                            <div class="row">
                                               <div class="col-sm-12 col-md-12 tab-text">
                                                  <h3>Hotel Policy</h3>
                                                  @if(isset($hotel->HotelDetails->HotelPolicy))
                                                  <p><?php echo $hotel->HotelDetails->HotelPolicy; ?></p>
                                                  @endif
                                               </div>
                                               <!-- end columns -->
                                            </div>
                                            <!-- end row -->
                                         </div>
                                         <!-- end pick-up -->
                                         <div id="luxury-gym" class="tab-pane">
                                            <div class="row">
                                               <div class="col-sm-12 col-md-12 tab-text">
                                                  <h3>Hotel Facilities</h3>
                                                  @if(isset($hotel->HotelDetails->HotelFacilities))
                                                  @foreach($hotel->HotelDetails->HotelFacilities as $hf)
                                                  <li><?php echo $hf; ?></li>
                                                  @endforeach
                                                  @endif
                                               </div>
                                               <!-- end columns -->
                                            </div>
                                            <!-- end row -->
                                         </div>
                                         <!-- end luxury-gym -->
                                         <div id="sports-club" class="tab-pane">
                                            <div class="row">
                                               <div class="col-sm-12 col-md-12 tab-text">
                                                  <h3>Special Instructions</h3>
                                                  @if($hotel->HotelDetails->SpecialInstructions)
                                                  <p><?php echo $hotel->HotelDetails->SpecialInstructions; ?></p>
                                                  @endif
                                               </div>
                                               <!-- end columns -->
                                            </div>
                                            <!-- end row -->
                                         </div>
                                         <!-- end sports-club -->
                                      </div>
                                      <!-- end tab-content -->
                                   </div>
                                   <!-- end detail-tabs -->
                                   <div class="available-blocks" id="available-rooms">
                                      <h2>Available Rooms</h2>
                                      @if(isset($room_info))
                                      <?php $i=1; ?>
                                      @foreach($comb as $cmb)
                                      
                                      <div class="list-block main-block room-block">
                                         
                                         <?php $room_offered_price=0; ?>
                                         @foreach($room_info->GetHotelRoomResult->HotelRoomsDetails as $rm)
                                         @if(in_array($rm->RoomIndex, $cmb))
                                         <div class="list-content">
                                            <div class="main-img list-img room-img">
                                               <a href="#">
                                               <img src="{{asset('assets/home/images/h-feature-1.jpg')}}" class="img-responsive" alt="room-img">
                                               </a>
                                               <div class="main-mask">
                                                  <ul class="list-unstyled list-inline offer-price-1">
                                                     <li id="selectroomdet" class="price"><span id="published_price" style="display:none">{{$rm->Price->PublishedPriceRoundedOff}}</span><span id="room_offered_price">{{$rm->Price->CurrencyCode}}&nbsp;{{$rm->Price->OfferedPriceRoundedOff}}</span><span class="divider">|</span><span class="pkg">per room/night</span></li>
                                                     <?php $room_offered_price=$room_offered_price+$rm->Price->OfferedPriceRoundedOff; 
                                                        $room_price_currency=$rm->Price->CurrencyCode;
                                                        ?>
                                                     <!-- price fhskhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhk-->
                                                     {{$rm->Price->CurrencyCode}}
                                                     <!-- price fhskhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhk-->
                                                  </ul>
                                               </div>
                                               <!-- end main-mask -->
                                            </div>
                                            <!-- end room-img -->
                                            <div class="list-info room-info">
                                               <h3 class="block-title"><a href="#">{{$rm->RoomTypeName}}</a></h3>
                                               <!--<p class="block-minor">Max Guests:02</p> 
                                                  <p>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum at, pro an eros perpetua ullamcorper.</p>-->
                                               <h4>Room Bed Type</h4>
                                               <ul>
                                                  @if(isset($rm->BedTypes))
                                                  @foreach($rm->BedTypes as $bed)
                                                  <li>{{$bed->BedTypeDescription}}</li>
                                                  @endforeach
                                                  @endif
                                               </ul>
                                               <ul>
                                               </ul>
                                               <h4>Cancellation Policy</h4>
                                               <p>{{$rm->CancellationPolicy}}</p>
                                               <h6>Amenity</h6>
                                               <ol type="1">
                                                  @if(isset($rm->Amenity))
                                                  @foreach($rm->Amenity as $am)
                                                  <li>{{$am}}</li>
                                                  @endforeach
                                                  @endif
                                               </ol>
                                            </div>
                                            <!-- end room-info -->
                                           
                                         </div>
                                          @endif
                                            @endforeach
                                            <script>
                                            $('#setRoomPrice'+{{$i}}).html({{$room_offered_price}});
                                            </script>
                                         <input type="hidden" value="{{$room_offered_price}}" data-currency="{{$room_price_currency}}" id="roomCombPrice{{$i++}}">
                                        <div class="choose-hotel">                    
                                            <?php $combination=implode('-',$cmb)?>
                                            <button class="button greenOutlineBtn" type="button" id="select_room{{$i}}" name="select_room" value="{{$combination}}" onclick="roomComb(this.value,{{$i}});" >Book Now</button>
                                         </div>
                                         
                                      </div>
                                      
                                      
                                      @endforeach
                                      @endif
                        </div>
                    
                    @endforeach
                    @endif
    <!-- end pages -->
    </div><!-- end columns -->

                <div class="col-xs-12 col-sm-12 col-md-4 side-bar right-side-bar">
                   <!--<div class="side-bar-block booking-form-block">-->
                   <!--   <h2 class="selected-price"><span>{{$hotel->HotelDetails->HotelName}}</span></h2>-->
                   <!--   <span id="room-price" class="hotel-detail-price">-->
                         <!--15,000 INR-->
                   <!--   </span>-->
                      <input type="hidden" name='HotelCode' value="{{$hotel->HotelDetails->HotelCode}}">
                      <input type="hidden" name='HotelName' value="{{$hotel->HotelDetails->HotelName}}">
                      <input type="hidden" name="result_index" value={{$result_index}}>
                      <input type="hidden" name="trace_id" value={{$trace_id}}>
                      <input type="hidden" name="combination" id="comb">
                   <!--   <input type="hidden" name="InfoSource" value="{{$room_info->GetHotelRoomResult->RoomCombinations->InfoSource}}">-->
                   <!--   <div class="address-hotel-side">-->
                   <!--      <p class="hotel-text">{{$hotel->HotelDetails->Address}}</p>-->
                   <!--   </div>-->
                   <!--</div>-->
                   <!-- end side-bar-block -->
                   <!--<a href="#available-rooms" class="you-just-wanna">Select Rooms</a>-->
                   <!--<br>-->
                   <!--<button class="button orange xlarge width100 mkitop" id="book_now">Book Now</button>-->
                   <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-12">
                         <div class="side-bar-block support-block">
                            <h3>Need Help</h3>
                            <p>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum.</p>
                            <div class="support-contact">
                               <span><i class="fa fa-phone"></i></span>
                               <p>+1 123 1234567</p>
                            </div>
                            <!-- end support-contact -->
                         </div>
                         <!-- end side-bar-block -->
                      </div>
                      <!-- end columns -->
                   </div>
                   <!-- end row -->
                </div>
           </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end hotel-details -->
</section><!-- end innerpage-wrapper -->
</form>
@include('frontend.includes.footer')
<script>
$(document).ready(function(){
    var room_name=$(".block-title:first").text();
    var offered_price=$("#room_offered_price:first").text();
    var published_price=$("#published_price:first").text();
    
    $(".room-ka-type").text(room_name);
    if(offered_price < published_price)
    {
        $(".offered_price").text(offered_price);
        $(".hotel-discount-price").text(published_price);
    }
    else
    {
        $(".offered_price").text(published_price);
        $(".hotel-discount-price").text('');
    }
    
});
</script>
<script>
   function roomComb(val,i)
   {
   
   //alert(val);
   $('#comb').val(val);
   
     
   
     var total_room_price=$("#roomCombPrice"+i).val();
     var currency = $("#roomCombPrice"+i).attr("data-currency");
     
       $("#room-price").html(currency+"&nbsp;"+total_room_price);
       $('#hotel_block')[0].submit();
   }
</script>
<script>
$('#book_now').on('click',function(){
    $('#hotel_block')[0].submit();
}); 
</script>