@include('frontend.includes.header')
@include('frontend.includes.nav1')
@include('frontend.includes.sidenav')

@if(!empty($errors->first()))
<div class=' alert alert-danger'>{{$errors->first()}}</div>
@endif

<div class=" form-flights">
<div class="container">
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <div class="flights-headline">
            <h3>Book Hotels for Thailand</h3>
          </div>
        </div>
        
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="tab-content">
          <div class="tab-pane active" id="tab_default_1">
		  <form method="post"  action="{{route('frontend.search_hotel')}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="row" style="float: left;">
					 <form method="post"  action="{{route('frontend.search_flight')}}" id="hotel-search-form" >
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                        <div class="col-xs-12 col-md-3">
                              <h3>Find Hotel</h3>
                              <input id='input_hotel' type='text'name="dest" class='form-control' placeholder="Enter City" required>
                              <input type="hidden" id ="destination" name="destination">
                            </div>
                        <div class="col-xs-12 col-md-2">
                              <div class="date">
                            <div class="depart">
                                  <h3> Check-In</h3>
                                  <input  id="datepicker" name="check_in" type="text" autocomplete="off" placeholder="mm/dd/yyyy" onfocus="this.value = '';"  required>
                                </div>
                            <div class="clear"></div>
                          </div>
                            </div>
                        <div class="col-xs-12 col-md-2">
                              <div class="return">
                            <h3>Check-Out</h3>
                            <input  id="datepicker1" name="check_out" type="text" autocomplete="off" placeholder="mm/dd/yyyy"  required>
                          </div>
                            </div>
                        <div class="col-xs-12 col-md-2">
                              <div class="">
                            <div class="trigger form-group form-group-lg form-group-icon-left">
                                  <label style="text-align: left;">Passenger</label>
                                  <input style="height: 35px;" type="text" name="passengers" id="passengers" class="form-control flip"   onclick="myFunction()" autocomplete="off" placeholder="Rooms|Passengers" required>
                                </div>
                            <div id="panel" >
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <div class="row" style="width: 434px;">
                              <div class="col-md-2 col-xs-2">
                                <label class="control-label"><strong>Room(s)</strong>
                                     </label>
                                     <select name="rooms" id="rooms" onchange="showGuest(this.value);">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                              </select>
                              </div>

                              
                                 <div class="col-md-2 col-xs-2">
                                  <div id="adults">
                                    
                                <label class="control-label"><strong>Adult(s)</strong><br>
                                      </label>
                                         <select onchange="count_adults();" name="adults[]" id="adults1">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                              </select>
                              </div>
                              </div>
                                 <div class="col-md-8 col-xs-8" >
                                <div class="form-group" id="children">
                               <div class="row kids-row">
                                <div class="col-md-6 col-xs-6">     
                                <label class="control-label">
                                <strong>Child</strong></label>
                                <select name="children[]" id="children1" onchange="showAges(this.value,1);">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                </select>
                                </div>
                              <div class="col-md-8 col-xs-8">
                              <div class="row kids-selection" id="child1"></div> 
                              </div>
                              </div>
                              </div>
                              </div>
                              </div>
                            
                               <div class="col-md-12">
                                  <a class="btn btn-primary btn-block demise close" data-dismiss="modal">Done</a> </div>
                                </div>
                          </div>
                            </div>
                        <div class="col-xs-12 col-md-2">
                              <button type="sumbmit" class="btn btn-warning form-search-btn">Search</button>
                            </div>
					</form>
                      </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>

	@include('frontend.includes.flightoffers')
		
	@include('frontend.includes.footer')
<script>
  $('.carousel[data-type="multi"] .item').each(function() {
	var next = $(this).next();
	if (!next.length) {
		next = $(this).siblings(':first');
	}
	next.children(':first-child').clone().appendTo($(this));

	for (var i = 0; i < 1; i++) {
		next = next.next();
		if (!next.length) {
			next = $(this).siblings(':first');
		}

		next.children(':first-child').clone().appendTo($(this));
	}
});
    </script>	
<script type="text/javascript">
  
    var dates = $("#datepicker").datepicker({
    // defaultDate: "",
     changeMonth: true,
    numberOfMonths: 2,
    minDate: 0,
    onSelect: function(date) {
    $("#datepicker1").datepicker('option', 'minDate', date);
  }
});
$("#datepicker1").datepicker({ numberOfMonths: 2,changeMonth: true,});
</script>

<script>
    $(document).ready(function(){
        $("#datepicker").change(function(){
            var  checkin =  $(this).val();
          var checkout = $("#datepicker1").val();
            if(checkin=="")
            {
                $(this).val("");
                $("#datepicker").focus();
            }
            if(checkin==checkout)
            {
                 $(this).val("");
            alert("CheckIn, CheckOut Date can't be same!");
            $("#datepicker").focus();
            }
        });
          $("#datepicker1").change(function(){
          var  checkin =  $("#datepicker").val();
          var checkout = $(this).val();
          if(checkin==checkout)
          {
              $(this).val("");
            alert("CheckIn, CheckOut Date can't be same!");
            $("#datepicker1").focus();
          }
         
      });  
    });
</script>

<script type="text/javascript">$(function () { 
    var $popover = $('.popover-markup>.trigger').popover({
      html: true,
      placement: 'bottom',
      
      content: function () {
          return $(this).parent().find('.content').html();
      }
    });

    // open popover & inital value in form

 
// store form value when popover closed
 $popover.on('hide.bs.popover', function () {
    $(".popover-content input").each(function(i) {
        passengers[i] = $(this).val();
    });
  });
 
  $(".popover-markup>.trigger").popover('show');
});
</script>
<script type="text/javascript">
function showGuest(val)
{
 $no_of_rooms=$("#rooms").val();
 $("#passengers").val($no_of_rooms+" Rooms/");
  var adults="";
  var child="";
  var $no_of_adults=0;
  
  
  for(i=1;i<=val;i++)
  {
    adults+='<label class="control-label"><strong>Adult(s)</strong><br></label><select onchange="count_adults();" name="adults[]" id="adults'+i+'"><option value="1">1</option>   <option value="2">2</option>   <option value="3">3</option>   <option value="4">4</option>   <option value="5">5</option>   <option value="6">6</option>   <option value="7">7</option>  <option value="8">8</option></select>';
    child+=' <div class="row kids-selection"><div class="col-md-4 col-xs-4 in-kid-sel"><label class="control-label"><strong>Child</strong><br></label><select  name="children[]" id="children'+i+'" onchange="showAges(this.value,'+i+');"><option value="0">0</option><option value="1">1</option><option value="2">2</option></select></div><div class="col-md-6 col-xs-6 in-kid-sel"><div class="form-group" id="child'+i+'"></div> </div></div>';
  }
  $('#adults').html(adults);
  $('#children').html(child);
  for($i=1;$i<=$no_of_rooms;$i++){
    $no_of_adults=parseInt($no_of_adults)+parseInt($("#adults"+$i).val())+parseInt($("#children"+$i).val());
    }
  $("#passengers").val($no_of_rooms+" Rooms/"+$no_of_adults+" Guest");
  
}
function count_adults(){
 $no_of_adults=0;
    $no_of_rooms=$("#rooms").val();
    for($i=1;$i<=$no_of_rooms;$i++){
    $no_of_adults=parseInt($no_of_adults)+parseInt($("#adults"+$i).val())+parseInt($("#children"+$i).val());
    }
  $("#passengers").val($no_of_rooms+" Rooms/"+$no_of_adults+" Guest");
}
function count_child(){
  
 $no_of_guest=0;
    $no_of_rooms=$("#rooms").val();
    for($i=1;$i<=$no_of_rooms;$i++){
    $no_of_guest=parseInt($no_of_guest)+parseInt($("#adults"+$i).val())+parseInt($("#children"+$i).val());
    }
  $("#passengers").val($no_of_rooms+" Rooms/"+$no_of_guest+" Guest");
}
function showAges(val,m)
{
  $no_of_guest=0;
    $no_of_rooms=$("#rooms").val();
    for($i=1;$i<=$no_of_rooms;$i++){
    $no_of_guest=parseInt($no_of_guest)+parseInt($("#adults"+$i).val())+parseInt($("#children"+$i).val());
    }
  $("#passengers").val($no_of_rooms+" Rooms/"+$no_of_guest+" Guest");

  var ages="";
  for(i=1;i<=val;i++)
  {
  ages+='<div class="col-md-6 col-xs-6"><label class="control-label"><strong>Age</strong><br></label><select name="ages[]" id="ages' +i+ '"> <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select></div>';
  }
  $('#child'+m).html(ages);

}
  </script>
<script>
function myFunction() {
    document.getElementById("panel").style.display = "block";
    showGuest(1);
}
</script>

<script type="text/javascript">
  $('.close').on('click',function(){
    $('#panel').hide();
  });
</script> 
<script type="text/javascript">
  $('#fl').on('click',function(){
    $('#panel').show();
  });
</script>

<script type="text/javascript">
      $('#input2').immybox({
          choices: [
              <?php if(isset($cities)) {foreach($cities as $city) {?>
            {text: '<?php echo $city['CityName'].','.$city['CityId'];?>', value: '<?php echo $city['CityId'];?>'},
              <?php }} else if(isset($airport)) { foreach($airport as $air) {?>
				{text: "<?php echo $air['CityName'].','.$air['AirportCode'];?>", value: '<?php echo $air['AirportCode'];?>'},
				<?php }}?>
          ],
          defaultSelectedValue: 'LA'
        });

	

	  });
</script>
