<style>
#cost{
   border-radius: 0px 3px 3px 0px ;
   border:1px solid #C0C0C0;
   padding:2.5px;
   padding-left:38px;
}
.static-value{
  position:absolute;
  font-weight:bold;
  font-size:0.8em;
  color:#fff;
  background-color: #343a40;
  padding:6px;
}
</style>
@php

if(isset($package))
{
$inclusionDetails=json_decode($package->inclusion_details);




}
@endphp
<div class="box-body">
    <div class="form-group">
        {{ Form::label('title', trans('validation.attributes.backend.packages.title'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.packages.title'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
   </div><!--form control-->

   <div class="form-group">
        {{ Form::label('destination', trans('validation.attributes.backend.packages.destination'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($selectedDestination))
                {{ Form::select('destination[]', $packageDestination, $selectedDestination, ['class' => 'form-control tags box-size','id'=>'destination_nights', 'required' => 'required','multiple' => 'multiple']) }}
            @else
                {{ Form::select('destination[]', $packageDestination, null, ['class' => 'form-control tags box-size','data-reorder'=>'1','id'=>'destination_nights', 'required' => 'required','multiple' => 'multiple']) }}
            @endif

@if(!empty($selectedDestinationNights))
    @foreach($selectedDestinationNights as $key=>$nights)
        @foreach($packageDestination as $pkey=>$packdestination)
           
                @if($pkey==$selectedDestination[$key])  
                <div id='{{str_replace(" ","",$packdestination)}}' class='form-group' style='margin-top:10px'>
                    <label for='destination_night' class='col-lg-2 control-label'>
                        <strong>{{$packdestination}}</strong>
                    </label>
                    <div class='col-lg-8'>
                        <input type='number' min='0' name='destinationNights[]' required='true' id='destination_night' placeholder='Enter Number of nights' class='form-control' value="{{$nights}}" />
                    </div>
                </div>
                @endif
           
        @endforeach
    @endforeach
@endif
        </div><!--col-lg-10-->
    </div><!--form control-->
    <input type="hidden" name="prev_destination" id="prev_destination">


    <div class="form-group">
        {{ Form::label('package_theme', trans('validation.attributes.backend.packages.package_theme'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($selectedTheme))
                {{ Form::select('package_theme', $packageTheme, $selectedTheme, ['class' => 'form-control box-size', 'required' => 'required']) }}
            @else
                {{ Form::select('package_theme', $packageTheme, null, ['class' => 'form-control box-size', 'required' => 'required']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('package_type', trans('validation.attributes.backend.packages.package_type'), ['class' => 'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            @if(!empty($selectedType))
                @foreach($packageType as $id => $display_name)
                    <input type="checkbox" class="pack_type" name="package_type[]" data-name="{{strtolower($display_name)}}" value="{{$id}}" <?php foreach($selectedType as $type_id){ if($type_id==$id)echo"checked"; } ?> >{{$display_name}}
                @endforeach
            @else
                @foreach($packageType as $id => $display_name)
                <input type="checkbox" class="pack_type" name="package_type[]" data-name="{{strtolower($display_name)}}" value="{{$id}}">{{$display_name}}
                @endforeach
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->
     <!-- standard cost -->
     <?php //if(isset($package->standard_price)){if(!empty($package->standard_price)||($package->standard_price>0)) echo 'display:block';else echo 'display:none';}else echo 'display:none'; ?>
     <div class="form-group " style="display:none" id="standard_package"> 
        {{ Form::label('standard_price', 'Standard price', ['class'=>'col-lg-2 control-label required']) }}
        <div class="col-lg-10"> 
            {{ Form::number('standard_price', null, ['class'=>'form-control box-size', 'placeholder'=>'Standard price','id'=>'standard_price']) }}
        </div>
    </div>
    <!-- standard cost end -->
    <!-- standard Offer section -->
    <div class="form-group" style="display:none" id="standard_offer"> 
    {{ Form::label('standard_off','Standard Offer',['class'=>'col-lg-2 control-label']) }}
    <div class="col-lg-10">
          {{ Form::number('standard_off',null,['class'=>'form-control box-size','placeholder'=>'enter offer in standard price','id'=>'standard_off','style'=>'padding-right: 4%;']) }}
        <label for="standard_off" style="z-index: 9;float: right;margin-top: -3%;margin-right: 15%;">%</label>
    </div>
    </div>
<!-- standard Offer section end -->
 <!-- delux cost -->
 <div class="form-group" style="display:none" id="delux_package"> 
        {{ Form::label('delux_price', 'Delux price', ['class'=>'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::number('delux_price', null, ['class'=>'form-control box-size', 'placeholder'=>'Delux Price','id'=>'delux_price']) }}
        </div>
    </div>
    <!-- delux cost end -->
    <!-- delux Offer section -->
    <div class="form-group" style="display:none" id="delux_offer"> 
    {{ Form::label('delux_off','Delux Offer',['class'=>'col-lg-2 control-label']) }}
    <div class="col-lg-10">
          {{ Form::number('delux_off',null,['class'=>'form-control box-size','placeholder'=>'enter offer in delux price','id'=>'delux_off','style'=>'padding-right: 4%;']) }}
          <label for="delux_off" style="z-index: 9;float: right;margin-top: -3%;margin-right: 15%;">%</label>
    </div>
    </div>
<!-- premium Offer section end -->
 <!-- premium cost -->
 <div class="form-group" style="display:none" id="premium_package"> 
        {{ Form::label('premium_price', 'Premium price', ['class'=>'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::number('premium_price', null, ['class'=>'form-control box-size', 'placeholder'=>'Premium Price','id'=>'premium_price']) }}
        </div>
    </div>
    <!-- premium cost end -->
    <!-- premium Offer section -->
    <div class="form-group" style="display:none" id="premium_offer"> 
    {{ Form::label('premium_off','Premium Offer',['class'=>'col-lg-2 control-label']) }}
    <div class="col-lg-10">
          {{ Form::number('premium_off',null,['class'=>'form-control box-size','placeholder'=>'enter offer in premium price','id'=>'premium_off','style'=>'padding-right: 4%;']) }}
          <label for="premium_off" style="z-index: 9;float: right;margin-top: -3%;margin-right: 15%;">%</label>
    </div>
    </div>
<!-- premium Offer section end -->
    <div class="form-group">
        {{ Form::label('publish_datetime', trans('validation.attributes.backend.packages.publish'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($package->publish_datetime))
                {{ Form::text('publish_datetime', \Carbon\Carbon::parse($package->publish_datetime)->format('m/d/Y h:i a'), ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.packages.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
            @else
                {{ Form::text('publish_datetime', null, ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.packages.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('featured_image', trans('validation.attributes.backend.packages.image'), ['class' => 'col-lg-2 control-label required']) }}
        @if(!empty($package->featured_image))
            <div class="col-lg-1">
            <img src="{{ url('storage/app/public/img/package').'/'.$package->featured_image }}" height="80" width="80">
                <!-- <img src="{{ asset('assets/admin/img/package').'/'.$package->featured_image }}" height="80" width="80"> -->
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="featured_image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="featured_image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->   

    <div class="form-group">
        {{ Form::label('content', trans('validation.attributes.backend.packages.content'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('content', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.packages.content')]) }}
        </div>
    </div>
    
        <div class="form-group">
        {{ Form::label('content', trans('validation.attributes.backend.packages.terms_conditions'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('terms_and_conditions', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.packages.terms_conditions')]) }}
        </div>
    </div>

   

<div class="form-group">
        {{ Form::label('inclusion', trans('validation.attributes.backend.packages.inclusion'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($selectedInclusions))
                {{ Form::select('inclusion[]', $inclusions, $selectedInclusions, ['class' => 'form-control tags box-size', 'id'=>'inclusions', 'required' => 'required','multiple' => 'multiple']) }}
            @else
                {{ Form::select('inclusion[]', $inclusions, null, ['class' => 'form-control tags box-size', 'id'=>'inclusions', 'required' => 'required','multiple' => 'multiple']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->

<div class="form-group" id="activities" style="display:@if(!empty($selectedActivities)) block @else none @endif">
        {{ Form::label('activity', trans('validation.attributes.backend.packages.activities'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($selectedActivities))
                {{ Form::select('activity[]', $activities, $selectedActivities, ['class' => 'form-control tags box-size', 'id'=>'activities_input', 'required' => 'required','multiple' => 'multiple']) }}
            @else
                {{ Form::select('activity[]', $activities, null, ['class' => 'form-control tags box-size', 'required' => 'required', 'id'=>'activities_input','multiple' => 'multiple']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->
    
        @isset($inclusionDetails->flights)
       <div class="form-group">
        {{ Form::label('flight-details', 'Flight Details', ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('flight_details', $inclusionDetails->flights, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.packages.content')]) }}
        </div>
    </div>
    @endisset
    
    @isset($inclusionDetails->hotels)
    
       <div class="form-group">
        {{ Form::label('content', 'Hotel Details', ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('hotel_details', $inclusionDetails->hotels, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.packages.content')]) }}
        </div>
    </div>
    @endisset
    
    
    
    
    
    
    
    
    
    
    


    <div class="form-group">
        {{ Form::label('slug', trans('validation.attributes.backend.packages.slug'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            {{ Form::text('slug', null, ['class' => 'form-control box-size','placeholder' => trans('validation.attributes.backend.packages.slug'), 'readonly' => 'readonly']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->
  
<!-- No of Days -->
@if(!empty($package->no_days))
<div class="form-group"> 
    {{ Form::label('no_days','Days',['class'=>'col-lg-2 control-label']) }}
    <div class="col-lg-10">
          {{ Form::number('no_days',null,['class'=>'form-control box-size','min'=>'1','max'=>'15','placeholder'=>'Enter no. of days..','id'=>'no_days','readonly'=>'true']) }}
    </div>
</div>
@else
<div class="form-group"> 
    {{ Form::label('no_days','Days',['class'=>'col-lg-2 control-label']) }}
    <div class="col-lg-10">
          {{ Form::number('no_days',1,['class'=>'form-control box-size','min'=>'1','max'=>'15','placeholder'=>'Enter no. of days..','id'=>'no_days','readonly'=>'true']) }}
    </div>
</div>
@endif

   <!-- Day Description details -->
   @if(!empty($package->day_title))
   <?php

$explode_data = explode(',',$package->day_title);
$explode_data1 = explode(',',$package->day_destination);
$explode_data2 = explode('---',$package->day_description);
//$explode_data3 = explode(',',$package->day_image);


for($i=0;$i<count($explode_data);$i++)
{
    
   echo"<div class='day-card'><div class='form-group'> 
<label for='p_date' class='col-lg-2 control-label'>Day Title</label>
<div class='col-lg-10'>
<input type='text' name='dayTitle[]' id='p_date' value='".$explode_data[$i]."' class='form-control box-size'/>
</div>
</div>";

 echo" <div class='form-group'> 
<label for='p_date' class='col-lg-2 control-label'>Day Destination</label>
<div class='col-lg-10'>
<input type='text' name='dayDestination[]' id='p_date' value='".$explode_data1[$i]."' class='form-control box-size'/>
</div>
</div>";

 echo" <div class='form-group'> 
<label for='p_date' class='col-lg-2 control-label'>Day Description</label>
<div class='col-lg-10 mce-box'>
<textarea name='dayDescription[]'> ".$explode_data2[$i]."</textarea>
</div>
</div><a class='remove_day_card' href='#'>Remove</a></div>";
}

?>

 <?php $num = 1; ?>
<div class="input_fields_wrap">
    
  <button class="add_field_button btn btn-primary btn-md" style="margin-bottom:5px;">Add More Days</button>
    <span id="warning_days"></span>
    <input type="hidden" id="count_dayBox" value="{{count($explode_data)}}">
  <div>
    
   
  </div>
</div>

   @else
   <?php $num = 1; ?>
<div class="input_fields_wrap">
    <button class="add_field_button btn btn-primary btn-md" style="margin-bottom:5px;">Add More Days</button>
    <span id="warning_days"></span>
    <input type="hidden" id="count_dayBox" value="1">
    <div>
        <div class="card-header">Day <?php echo $num; ?> </div>
        <div class="card-body">
        <div class="form-group"> 
            <label for="title" class="col-lg-2 control-label"><strong>Title</strong></label>
            <div class="col-lg-8">
            <input type="text" name="dayTitle[]" id="title" class="form-control" required="true" value="Day <?php echo $num; ?>"/>
        </div>
        </div> 
        
        <div class="form-group"> 
            <label for="destination" class="col-lg-2 control-label"><strong>Destination</strong></label>
            <div class="col-lg-8">
            <input type="text" name="dayDestination[]" required="true" id="destination" placeholder="Example: Delhi-Haryana / Delhi-Harayana-Punjab" class="form-control"/>
        </div>
        </div>
        
        <div class="form-group"> 
            <label for="description" class="col-lg-2 control-label"><strong>Description</strong></label>
            <div class="col-lg-10 mce-box">
            <textarea name="dayDescription[]" class="form-control" placeholder="Description"></textarea>
            </div>
        </div>

        <!-- <div class="form-group row"> 
            <label for="image" class="col-lg-2 control-label"><strong>Image </strong></label>
            <div class="col-lg-8">
            <input type="file" name="dayImage[]" class="form-control" multiple/>
            </div>
        </div> -->

        </div>
    </div>
    </div>
    @endif
   <!-- Day Description details end -->
  
<div class="form-group">
        {{ Form::label('status', trans('validation.attributes.backend.packages.status'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
           {{ Form::select('status', $status, null, ['class' => 'form-control select2 status box-size', 'placeholder' => trans('validation.attributes.backend.packages.status'), 'required' => 'required']) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
</div>

@section("after-scripts")
<script type="text/javascript">
    $(".day-card").on("click",".remove_day_card",function(e){
        e.preventDefault(); $(this).parent('div').remove();
        let dbox = $("#count_dayBox").val();
        let udbox = dbox-1;
        $("#count_dayBox").val(udbox);
        $("#warning_days").css({"color":"#d9534f"}).html("");
        
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#title").change(function(){
            $val = $(this).val();
            $val= $val.toLowerCase();
            $slug_val = $val.replace(/[^a-zA-Z0-9]/g," ");
            $slug = $slug_val.replace(/\s+/g,"-");
            
            $('#slug').val($slug);            
        });
       
    });
    Backend.Package.selectors.GenerateSlugUrl = "{{route('admin.generate.slug')}}";
    Backend.Package.selectors.SlugUrl = "{{url('/')}}";
    Backend.Package.init();
    
</script>
<script type="text/javascript">
    
    $(document).ready(function() {
        var default_nights_edited = calculateNights();
        $("#no_days").val(default_nights_edited);
        //$("#count_dayBox").val(1);
       
        $(document).on("submit","form",function(){
            //alert("hello");
            let tn=calculateNights();
            let dbox = $("#count_dayBox").val();
            if(tn>dbox){
                $(".add_field_button").focus();
                $("#warning_days").css({"color":"#d9534f"}).html("*Add "+(tn-dbox)+" days more");
            return false;
            }
            if(tn<dbox)
            {
                $(".add_field_button").focus();
                $("#warning_days").css({"color":"#d9534f"}).html("*Remove "+(dbox-tn)+" days");
            return false;
            }
            alert("All Cool!");
            return true;
        });
        
        $(document).on("change","input[name='destinationNights[]']",function(){
            var total_nights = calculateNights();
            $("#no_days").val(total_nights);
            var dbox =  $("#count_dayBox").val();
            if(dbox<1)
            $("#count_dayBox").val(1);

        });
        //in case of edit package set count day box
        var count_dbox = $("#count_dayBox").val();
        function calculateNights(){
            var total_nights = $("input[name='destinationNights[]']");
            var sum=0;
            for(let k=0;k<total_nights.length;k++)
            {
                val_n = $(total_nights[k]).val();
                sum = parseInt(sum)+parseInt(val_n);
            
            }
            //alert(sum);
            return sum;
        }
       // $("#no_days").val(sum);
        if(count_dbox==null||count_dbox==0)
            {
                    count_dbox=$("#no_days").val();
                $("#count_dayBox").val(count_dbox);
            }
        //show warning while entering the no. of days
        $("#no_days").on("change",function(){
            var count_dbox = $("#count_dayBox").val();
            var days_val=$(this).val();
            if(days_val<count_dbox){
            $("#no_days").val(count_dbox);
            }
        })
        $("#no_days").on("keyup",function(e){
           var days = $(this).val();
           var count_dbox = $("#count_dayBox").val(); 
            if(days>1){
                
                $("#warning_days").css({"color":"#d9534f"}).html("*Add "+(days-count_dbox)+" days more");

            }
            else
            {
               $("#warning_days").html("");  
            }
        })
        // Warning script end
       
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID
        var max_fields = $("#no_days").val();
        var x = 1; //initlal text box count
        $(add_button).on('click',function(e){ //on add input button click
            
            e.preventDefault();
            var days = $("#no_days").val();
                x= $("#count_dayBox").val();
                //alert(days+"-"+x);
             max_fields = days; //maximum input boxes allowed
            
            if(parseInt(x)<max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><div class="card-header">Day'+x+'</div><div class="card-body"><div class="form-group row"><label for="title" class="col-md-2 col-form-label"><strong>Title</strong></label><div class="col-md-6"><input type="text" class="form-control" name="dayTitle[]" required="true" value="Day '+x+'"/></div></div><div class="form-group row"><label for="destination" class="col-md-2 col-form-label"><strong>Destination</strong></label><div class="col-md-6"><input type="text" name="dayDestination[]" id="destination" required="true" placeholder="Example: Delhi-Haryana / Delhi-Harayana-Punjab" class="form-control"/></div></div><div class="form-group row"> <label for="description" class="col-md-2 col-form-label"><strong>Description</strong></label><div class="col-lg-10 mce-box"><textarea name="dayDescription[]" class="form-control" placeholder="Description"/></textarea></div></div></div><a href="#" class="remove_field">Remove</a></div>'); //add input box
                tinymce.init({selector:'textarea'});
                if((max_fields-x)>0)
                $("#warning_days").css({"color":"#d9534f"}).html("*Add "+(max_fields-x)+" days more");
                else
                $("#warning_days").css({"color":"#38e200"}).html("All days added successfully");
                $("#count_dayBox").val(x);
                $("#warning_days").focus();

            }
        });
    
        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        
            if((max_fields-x)>0)
                $("#warning_days").css({"color":"#d9534f"}).html("*Add "+(max_fields-x)+" days more");
            else
                $("#warning_days").css({"color":"#38e200"}).html("All days added successfully");
            $("#count_dayBox").val(x);
            $("#warning_days").focus();
        })
        
    });
</script>
<script>
$(document).ready(function(){

    let chk=$('input[name="package_type[]"]');
    for(let i=0;i<chk.length;i++)
    {
        if($(chk[i]).is(':checked')){
            var packtype=$(chk[i]).data("name");
             //alert(packtype);
             $("#"+packtype+"_price").attr("required","required");
             $("#"+packtype+"_package").show();           
            
             $("#"+packtype+"_offer").show();
        }
    }
   
    $(".pack_type").on('click',function(){
        if(this.checked==true)
        {
            var ptype=$(this).data("name");
            $("#"+ptype+"_price").attr("required","required");
            $("#"+ptype+"_package").show();           
            //alert($("#"+ptype+"_price").attr("required"));
            $("#"+ptype+"_offer").show();
        }
        else{
            var ptype=$(this).data("name");
                $("#"+ptype+"_price").removeAttr("required");
               // $("#"+ptype+"_offer").hide();
                $("#"+ptype+"_package").hide();
                $("#"+ptype+"_offer").hide();
                // $("#"+ptype+"_price").prop("disabled","disabled");
                // $("#"+ptype+"_offer").prop("disabled","disabled");

        }
        
    });
   
});
</script>
<script>
$(document).ready(function(){
    let dest_ar = $("#destination_nights option:selected").text();
    $("#prev_destination").val(dest_ar);
     
   //alert(abc);
    $("#destination_nights").on("change",function(){
        let prev_dest = $("#prev_destination").val(); 
        //let len_prev_dest = prev_dest.length;
        prev_dest = prev_dest.replace(/([a-z])([A-Z])/g, '$1,$2');
        //alert(prev_dest);
        let destination_ar = $("#destination_nights option:selected").text();
        destination_ar = destination_ar.replace(/([a-z])([A-Z])/g, '$1,$2');
        let ar_prev_dest = prev_dest.split(',');
        let ar_destination = destination_ar.split(',');
        if(prev_dest.length<destination_ar.length)
        {
            //let current_destination;
            for(let i=0;i<ar_destination.length;i++){
                for(let j=0;j<ar_prev_dest.length;j++){
                    //alert(ar_destination[i]+' - '+ar_prev_dest[j]);
                    if(ar_destination[i]==ar_prev_dest[j])
                    {
                        ar_destination[i]="";
                    // alert(ar_destination[i]); 
                    }
                    
                }
            //alert(ar_destination[i]); 
            }
            let current_dest =ar_destination.join('');
            //alert(current_dest);
            if(current_dest!="")
            {
                $(this).parent().append(" <div id='"+current_dest.replace(/\s+/g, '')+"' class='form-group' style='margin-top:10px'><label for='destination_night' class='col-lg-2 control-label'><strong>"+current_dest+"</strong></label><div class='col-lg-8'><input type='number'min='0' name='destinationNights[]' required='true' id='destination_night' placeholder='Enter Number of nights' class='form-control'/></div></div>")
                $("#prev_destination").val(destination_ar);
            }
        }
        if(prev_dest.length>destination_ar.length)
        {
            //alert("more prev");
            //let current_destination;
            for(let i=0;i<ar_prev_dest.length;i++){
                for(let j=0;j<ar_destination.length;j++){
                    //alert(ar_destination[i]+' - '+ar_prev_dest[j]);
                    if(ar_destination[j]==ar_prev_dest[i])
                    {
                        ar_prev_dest[i]="";
                    // alert(ar_destination[i]); 
                    }
                    
                }
            //alert(ar_destination[i]); 
            }
            let delete_dest = ar_prev_dest.join('');
           // alert(delete_dest.replace(/\s+/g, ''));
            let delete_ele=$(this).parent().children().nextAll("#"+delete_dest.replace(/\s+/g, '')).remove();
            //alert(delete_ele);
            $("#prev_destination").val(destination_ar);
            //alert(delete_dest);
        }
                //alert(destination_ar);
            // let current_dest = destination_ar.replace(prev_dest,' ');
            
           
        
        //alert(destinations.join(","))
     });
});
</script>





<script>
$(document).ready(function(){
//     $("select").select2({
// tags: true
// });
$("select[name='destination[]']").val("");
<?php 
if(isset($selectedDestination)){
foreach($selectedDestination as $sel_dest){ ?>
   var dest_id = "<?php echo $sel_dest; ?>";
  var evt2= $("select[name='destination[]'] option[value='"+dest_id+"'] ").prop("selected",true);
 
    var $element = $(evt2);
   // alert($("select[name='destination[]']").find(":selected").val());
 
      if ($("select[name='destination[]']").find(":selected").length > 1) {
        var $second = $("select[name='destination[]']").find(":selected").eq(-2);
        $element.detach();
        $second.after($element);
       } 
      else {
        $element.detach();
        $("select[name='destination[]']").prepend($element);
      }

     // $("select[name='destination[]']").trigger("change");
  
<?php }} ?>

// On select, place the selected item in order
$("select[name='destination[]']").on("select2:select", function (evt) {
   
    var element = evt.params.data.element;
    var $element = $(element);
    //alert(element);
    window.setTimeout(function () {  
      if ($("select[name='destination[]']").find(":selected").length > 1) {
        var $second = $("select[name='destination[]']").find(":selected").eq(-2);
        $element.detach();
        $second.after($element);
      } 
      else {
        $element.detach();
        $("select[name='destination[]']").prepend($element);
      }

      $("select[name='destination[]']").trigger("change");
    }, 1);

});

// on unselect, put the selected item last
$("select[name='destination[]']").on("select2:unselect", function (evt) {
    var element = evt.params.data.element;
    $("select[name='destination[]']").append(element);
    
     var total_no_of_nights = calculateNights();
        
         $("#no_days").val(total_no_of_nights);
});
function calculateNights(){
            var total_nights = $("input[name='destinationNights[]']");
            var sum=0;
            for(let k=0;k<total_nights.length;k++)
            {
                val_n = $(total_nights[k]).val();
                sum = parseInt(sum)+parseInt(val_n);
            
            }
            //alert(sum);
            return sum;
        }

});
</script>
<script>
$(document).ready(function(){
    
    $('.select2-search__field').css('width','54.75em');
    
    var inclusion_default = $("select[name='inclusion[]']").val();
      var inclusion_def_ar = inclusion_default.toString().split(",");
      let hotelIncluded=0;
      let flightIncluded=0;
    $("select[name='inclusion[]']").on("change",function(){
      var inclusion_val = $(this).val();
      var inclusion_ar = inclusion_val.toString().split(",");
      var count=0;
      let allInclusions=0;
      
      
      
      
      if($.inArray('Flights', inclusion_ar)!=-1 && flightIncluded==0)
      {
          
          $('#inclusions').parent().parent().after('<div class="form-group flight-details"> <label for="flight-details" class="col-lg-2 control-label required"><strong>Flight Details</strong></label><div class="col-lg-10 mce-box"><textarea name="flight_details" class="form-control" placeholder="Flight Details"/></textarea></div></div>');
          tinymce.init({selector:'textarea'});
          flightIncluded=1;
      }
      else if($.inArray('Flights', inclusion_ar)==-1)
      {
          
          allInclusions=$('#inclusions').parent().parent().nextAll();
          for(let i=0;i<allInclusions.length;i++)
          {
              if($(allInclusions[i]).hasClass('flight-details'))
              {
                  $(allInclusions[i]).remove();
                  flightIncluded=0;
              }
          }
          
      }
      
      if($.inArray('Hotels', inclusion_ar)!=-1 && hotelIncluded==0)
      {
          
          $('#inclusions').parent().parent().after('<div class="form-group hotel-details"> <label for="hotel-details" class="col-lg-2 control-label required"><strong>Hotel Details</strong></label><div class="col-lg-10 mce-box"><textarea name="hotel_details" class="form-control" placeholder="Hotel Details"/></textarea></div></div>');
          tinymce.init({selector:'textarea'});
          hotelIncluded=1;
          
      }
      else if($.inArray('Hotels', inclusion_ar)==-1)
      {
          
          allInclusions=$('#inclusions').parent().parent().nextAll();
          for(let i=0;i<allInclusions.length;i++)
          {
              if($(allInclusions[i]).hasClass('hotel-details'))
              {
                  $(allInclusions[i]).remove();
                  hotelIncluded=0;
              }
          }
          
      }
      
        for(var i=0;i<inclusion_ar.length;i++)
      {          
        if(inclusion_ar[i]=="Activities")
        {
            count=1;
        }
        else
        {              
            $("#activities").hide();
            $("select[name='activity[]']").attr("disabled",true);
        }
      }
      if(count==1)
      {
        $("#activities").show(); 
        $("select[name='activity[]']").attr("disabled",false);  
      }
      
      
    });
});
</script>
<script>
$("select[name='activity[]']").on("change",function(){
$('.select2-selection--multiple').css('width','54.75em');
$('.select2-selection--multiple').css('height','auto');
});
</script>
@endsection