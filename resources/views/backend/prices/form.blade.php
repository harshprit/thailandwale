<div class="box-body">


<div class="form-group">
        {{ Form::label('services', trans('validation.attributes.backend.prices.service'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
        @if(!empty($prices->service))
            {{ Form::select('service', $services, $prices->service, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        @else
            {{ Form::select('service', $services, null, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        @endif
        </div><!--col-lg-10-->
    </div><!--form control-->
    

    <div class="form-group">
        {{ Form::label('type', trans('validation.attributes.backend.prices.type'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">

        @if(!empty($prices->type))
            {{ Form::select('type', $type, $prices->type, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        @else
        {{ Form::select('type', $type, null, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        @endif
                
        </div><!--col-lg-10-->
    </div><!--form control-->


    <div class="form-group">
        {{ Form::label('amount_type', trans('validation.attributes.backend.prices.amount_type'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">

        @if(!empty($coupon->amount_type))
            {{ Form::select('amount_type', $amountType, $prices->amount_type, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        @else
        {{ Form::select('amount_type', $amountType, null, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        @endif
                
        </div><!--col-lg-10-->
    </div><!--form control-->

    


    <div class="form-group">
        {{ Form::label('amount', trans('validation.attributes.backend.prices.amount'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::number('amount', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.prices.amount'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    

   

    <div class="form-group">
        {{ Form::label('status', trans('validation.attributes.backend.prices.status'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
           {{ Form::select('status', $status, null, ['class' => 'form-control select2 status box-size', 'placeholder' => trans('validation.attributes.backend.prices.status'), 'required' => 'required']) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
</div>

@section("after-scripts")
    <script type="text/javascript">

        
        Backend.Coupon.init();
        
    </script>
@endsection