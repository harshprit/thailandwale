<div class="box-body">
    <div class="form-group">
        {{ Form::label('title', trans('validation.attributes.backend.activities.title'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.activities.title'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
   </div><!--form control-->
    <div class="form-group">
        {{ Form::label('destination', trans('validation.attributes.backend.packages.destination'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($selectedDestination))
                {{ Form::select('destination', $destination_list, $selectedDestination, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
            @else
                {{ Form::select('destination', $destination_list, null, ['class' => 'form-control tags box-size','data-reorder'=>'1', 'required' => 'required']) }}
            @endif
        </div>
    </div>
   <div class="form-group">
        {{ Form::label('category', trans('validation.attributes.backend.activities.category'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($selectedCategory))
                {{ Form::select('category', $activityCategory, $selectedCategory, ['class' => 'form-control box-size', 'required' => 'required']) }}
            @else
                {{ Form::select('category', $activityCategory, null, ['class' => 'form-control box-size', 'required' => 'required']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->
   <div class="form-group">
        {{ Form::label('description', trans('validation.attributes.backend.activities.description'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('description', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.activities.description')]) }}
        </div><!--col-lg-10-->
    </div>
  
 <!-- Activity cost -->
    <div class="form-group"> 
        {{ Form::label('price', 'Activity price', ['class'=>'col-lg-2 control-label required']) }}
        <div class="col-lg-10">
            {{ Form::number('price', null, ['class'=>'form-control box-size', 'placeholder'=>'Activity Price','id'=>'price']) }}
        </div>
    </div>
    <!-- Activity cost end -->
    
<div class="form-group">
        {{ Form::label('inclusions', trans('validation.attributes.backend.activities.inclusions'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
           
            {{ Form::textarea('inclusions', null, ['class' => 'form-control box-size']) }}
          
        </div><!--col-lg-10-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('exclusions', trans('validation.attributes.backend.activities.exclusions'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
           
            {{ Form::textarea('exclusions', null, ['class' => 'form-control box-size']) }}
          
        </div><!--col-lg-10-->
    </div><!--form control-->
<div class="form-group">
        {{ Form::label('suitable_for', trans('validation.attributes.backend.activities.suitable_for'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
           {{ Form::text('suitable_for', null, ['class' => 'form-control  box-size', 'placeholder' => trans('validation.attributes.backend.activities.suitable_for'), 'required' => 'required']) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
</div>

@section("after-scripts")
<script type="text/javascript">
    
    Backend.Activity.selectors.GenerateSlugUrl = "{{route('admin.generate.slug')}}";
    Backend.Activity.selectors.SlugUrl = "{{url('/')}}";
    Backend.Activity.init();
    
</script>
@endsection
