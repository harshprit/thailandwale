<div class="box-body">
    <div class="form-group">
        {{ Form::label('title', trans('validation.attributes.backend.destinations.title'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($selectedDestination))
                {{ Form::select('title', $destination_city, $selectedDestination, ['class' => 'form-control box-size', 'required' => 'required']) }}
            @else
                {{ Form::select('title', $destination_city, null, ['class' => 'form-control box-size', 'required' => 'required']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->
    <input type="hidden" name="flight_code">
    <!--form control-->
   
    <div class="form-group">
        {{ Form::label('image', trans('validation.attributes.backend.destinations.image'), ['class' => 'col-lg-2 control-label required']) }}
        @if(!empty($destination->image))
            <div class="col-lg-1">
                <img src="{{ url('storage/app/public/img/destination/' . $destination->image) }}" height="80" width="80">
            </div>
            <div class="col-lg-5">
                <div class="custom-file-input">
                    <input type="file" name="image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                    <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @else
            <div class="col-lg-5">
                <div class="custom-file-input">
                        <input type="file" name="image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                        <label for="file-1"><i class="fa fa-upload"></i><span>Choose a file</span></label>
                </div>
            </div>
        @endif
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('description', trans('validation.attributes.backend.destinations.description'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.destinations.description')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->
    <div class="form-group">
        {{ Form::label('status', trans('validation.attributes.backend.destinations.is_active'), ['class' => 'col-lg-2 control-label']) }}

        <div class="col-lg-10">
            <div class="control-group">
                <label class="control control--checkbox">
                        @if(isset($destination->status) && !empty ($destination->status))
                            {{ Form::checkbox('status', 1, true) }}
                        @else
                            {{ Form::checkbox('status', 1, false) }}
                        @endif
                    <div class="control__indicator"></div>
                </label>
            </div>
        </div><!--col-lg-10-->
    </div><!--form-control-->
   

    
</div><!--box-body-->

@section("after-scripts")
    <script type="text/javascript">
        Backend.Destination.selectors.GenerateSlugUrl = "{{route('admin.generate.slug')}}";
        Backend.Destination.selectors.SlugUrl = "{{url('/')}}";
        Backend.Destination.init();
        //Put your javascript needs in here.
        //Don't forget to put `@`parent exactly after `@`section("after-scripts"),
        //if your create or edit blade contains javascript of its own
        $(document).ready( function() {
            //Everything in here would execute after the DOM is ready to manipulated.
        });
    </script>
@endsection
