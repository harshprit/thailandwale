@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.destinations.management') . ' | ' . trans('labels.backend.destinations.edit'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.destinations.management') }}
        <small>{{ trans('labels.backend.destinations.edit') }}</small>
    </h1>
@endsection

@section('content')
    {{ Form::model($destination, ['route' => ['admin.destinations.update', $destination], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role','files' => true]) }}

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('labels.backend.destinations.edit') }}</h3>

                <div class="box-tools pull-right">
                    @include('backend.destinations.partials.destinations-header-buttons')
                </div><!--box-tools pull-right-->
            </div><!--box-header with-border-->

            <div class="box-body">
                <div class="form-group">
                    {{-- Including Form blade file --}}
                    @include("backend.destinations.form")
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.destinations.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div><!--edit-form-btn-->
                </div><!--form-group-->
            </div><!--box-body-->
        </div><!--box box-success -->
    {{ Form::close() }}
@endsection
