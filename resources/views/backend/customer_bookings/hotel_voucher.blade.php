@extends('backend.layouts.app')
@section('content')
<section class="page-cover" id="cover-thank-you">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    	<h1 class="page-title">Thank You</h1>
                        <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Thank You</li>
                        </ul>
                    </div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end page-cover -->
<style>
 </style>
<body style="background: #E7E7FF; color: #3D3D3D; font: 12px/18px Arial,Verdana,Tahoma;
    margin: 0; padding: 0;"><form id="HotelVoucherForm" action="HotelVoucherEmail.aspx">

    <div style="background: none repeat scroll 0 0 #FFFFFF; margin: 0 auto; width: 800px;">
        <div class="backbutton ui-draggable" id="backbutton"></div>
        <table style="border: 0 solid #9CBAD6; border-collapse: collapse; margin-bottom: -1px;
            width: 100%;">
            <tbody>
                <tr>
                    <td bgcolor="#004684" style="padding: 1px 2px;">
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td colspan="2" align="left" style="font-size: 18px; font-weight: bold; padding: 5px 5px;
                                        color: #fff;">
                                        Hotel Voucher
                                    </td>
                                    <td valign="top" style="padding: 5px 5px;">
                                        <ul style="list-style: none; display: inline; margin: 0px; padding: 0px; float: right;">
                                            <li id="emailVoucherBlock" style="display: none; list-style: none; display: inline;
                                                margin: 0px 2px; padding: 0px; color: #fff;"><a id="emailVoucher1" style="color: #fff;
                                                    padding: 5px; text-decoration: underline;" href="#">Email Voucher</a> |
                                            </li>
                                            <li style="list-style: none; display: inline; margin: 0px 2px; padding: 0px; color: #fff;">
                                                <a id="printVoucher1" style="color: #fff; padding: 5px; text-decoration: underline;" href="#">Print Voucher</a> | </li>
                                            <li style="list-style: none; display: inline; margin: 0px 2px; padding: 0px; color: #fff;">
                                                <a id="createPdf" style="color: #fff; padding: 5px; text-decoration: underline;" href="#">Generate PDF <img src="./Images/new_blink_small.gif" alt="New image"></a></li>
                                            
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="emailSent" style="display: block" class="email-message-parent">
            <span id="emailMessage" style="left: 350px; position: relative; font-weight: bold;" class="email-message-child"></span>
        </div>
        @if(isset($bookingInfo))
        <table style="border: 0 solid #9CBAD6; border-collapse: collapse; margin-bottom: -1px;
            width: 100%;">
            <tbody>
                <tr>
                    <td style="padding: 1px 2px;">
                        <table style="width: 100%; border-collapse: collapse;">
                            <tbody>
                                <tr>
                                    <th align="left" width="40%" style="padding: 4px 5px; background: none repeat scroll 0 0 #EAE9FF;
                                        border: 1px solid #9CBAD6; color: #004684; font-weight: bold;">
                                        Reference No
                                    </th>
                                    
                                    <th align="left" style="padding: 4px 5px; background: none repeat scroll 0 0 #EAE9FF;
                                        border: 1px solid #9CBAD6; color: #004684; font-weight: bold;">
                                        Confirmation No
                                    </th>
                                    
                                </tr>
                                <tr id="rowHotelConfNo">
                                    
                                    <td align="left" style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        <!--B627UDCDET92-->
                                        {{$bookingInfo->GetBookingDetailResult->BookingRefNo}}
                                    </td>
                                    
                                    <td align="left" style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        <!--QC9FBO(313089021|1234,1235)-->
                                        {{$bookingInfo->GetBookingDetailResult->ConfirmationNo}}
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 1px 2px;">
                        <table style="width: 100%; border-collapse: collapse;">
                            <tbody>
                                <tr>
                                    <td valign="top" style="border: 1px solid #9CBAD6; padding: 4px 5px; background: #f7f7ff;">
                                        <div>
                                            <b>Hotel Address Details</b>
                                        </div>
                                        <div>
                                            <!--Asia Resort Kaset Nawamin-->
                                            {{$bookingInfo->GetBookingDetailResult->HotelName}} 
                                            <br>
                                        </div>
                                        <div>
                                            <!--24/46 Kaset-Nawamin Road Soi Jamjan, Klongkum, Bangkok, 10230 ZipCode: 10230-->
                                             {{$bookingInfo->GetBookingDetailResult->AddressLine1}} 
                                            <br>
                                            
                                            <a href="#!" id="ViewMap-0">View Map</a>
                                            
                                            
                                        </div>
                                    </td>
                                    <td valign="top" align="left" style="border: 1px solid #9CBAD6; padding: 4px 5px;
                                        width: 60%;">
                                        
                                        <div style="float: left;">
                                        
                                            <b>Agency Address Details</b><br>
                                            Thailandwale<br>
                                            Delhi<br>
                                            
                                            City :
                                            Delhi
                                             <br>
                                            
                                            Phone :
                                            9780814728
                                            <br>
                                            Email :
                                            us@thailandwale.com
                                        </div>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 1px 2px;">
                        <table style="border: 1px solid #9CBAD6; width: 100%; border-collapse: collapse;">
                            <tbody>
                                <tr>
                                    <td colspan="3" style="border: 0 solid #9CBAD6; padding: 5px 4px;">
                                        <span style="color: #004684;"><b>Lead Passenger Name:</b>
                                            <!--Mr. Zubair Abbas-->
                                            {{$bookingInfo->GetBookingDetailResult->HotelRoomsDetails[0]->HotelPassenger[0]->Title." ".$bookingInfo->GetBookingDetailResult->HotelRoomsDetails[0]->HotelPassenger[0]->FirstName." ".$bookingInfo->GetBookingDetailResult->HotelRoomsDetails[0]->HotelPassenger[0]->LastName}}     
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border: 0 solid #9CBAD6; padding: 5px 4px;">
                                        <span style="color: #004684;"><b>Check In Date:</b>
                                        <?php $checkindate = date("Y-m-d", strtotime($bookingInfo->GetBookingDetailResult->CheckInDate)); ?>
                                           {{$checkindate}}
                                        </span>
                                    </td>
                                    <td style="border: 0 solid #9CBAD6; padding: 5px 4px;">
                                        <span style="color: #004684;"><b>Check Out Date:</b>
                                           <?php $checkoutdate = date("Y-m-d",strtotime($bookingInfo->GetBookingDetailResult->CheckOutDate)); ?>
                                           {{$checkoutdate}}
                                        </span>
                                    </td>
                                    <td style="border: 0 solid #9CBAD6; padding: 5px 4px;">
                                        <span style="color: #004684;"><b>No of Nights:</b>
                                        
                                           <?php
                                           $datetime1=date_create($bookingInfo->GetBookingDetailResult->CheckInDate);
                                    		$datetime2=date_create($bookingInfo->GetBookingDetailResult->CheckOutDate);
                                    		$interval = date_diff($datetime1,$datetime2);
                                    $days = $interval->format('%a');
                                            ?>
                                            {{$days}}
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 1px 2px;">
                        <table style="width: 100%; border-collapse: collapse;">
                            <tbody>
                                <tr>
                                    <th align="left" style="padding: 4px 5px; background: none repeat scroll 0 0 #EAE9FF;
                                        border: 1px solid #9CBAD6; color: #004684; font-weight: bold;">
                                        S.No
                                    </th>
                                    <th align="left" style="padding: 4px 5px; background: none repeat scroll 0 0 #EAE9FF;
                                        border: 1px solid #9CBAD6; color: #004684; font-weight: bold;">
                                        Room Type
                                    </th>
                                    <th align="left" style="padding: 4px 5px; background: none repeat scroll 0 0 #EAE9FF;
                                        border: 1px solid #9CBAD6; color: #004684; font-weight: bold;">
                                        No of Rooms
                                    </th>
                                    <th align="left" style="padding: 4px 5px; background: none repeat scroll 0 0 #EAE9FF;
                                        border: 1px solid #9CBAD6; color: #004684; font-weight: bold;">
                                        No of Guests
                                    </th>
                                    <th align="left" style="padding: 4px 5px; background: none repeat scroll 0 0 #EAE9FF;
                                        border: 1px solid #9CBAD6; color: #004684; font-weight: bold;">
                                        Guests Type
                                    </th>
                                </tr>
                                <?php $i=1; ?>
                                @foreach($bookingInfo->GetBookingDetailResult->HotelRoomsDetails as $rd)
                                <tr>
                                    <td align="center" style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                       {{$i}}     
                                    </td>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        <b>
                                            <!--Standard Double Room-->
                                            {{$rd->RoomTypeName}}
                                        </b>
                                        <br>
                                        
                                    </td>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                      1
                                    </td>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        <?php 
                                        $guest = $rd->AdultCount + $rd->ChildCount; ?>
                                        {{$guest}}
                                    </td>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                       {{$rd->AdultCount}} Adult(s){{$rd->ChildCount}} Child(ren)
                                        
                                        <br>
                                        <?php $m=1;$n=$rd->AdultCount;$comma=""; ?>
                                       <span>Adults:</span>
                                        @foreach($rd->HotelPassenger as $hp)
                                          @if($m<=$n)
                                           {{ $comma."".$hp->FirstName." ".$hp->LastName }}
                                           <?php $comma=","; ?>
                                          @endif
                                          <?php $m++; ?>
                                        @endforeach
                                        <!--Adults:Zubair Abbas-->
                                        
                                        <br>
                                        <!--Child(ren):Pooja Singh-->
                                         <?php $r=1;$s=$rd->AdultCount;$comma=""; ?>
                                        <span>Child(ren):</span>
                                        @foreach($rd->HotelPassenger as $hp)
                                          @if($r>$s)
                                           {{ $comma."".$hp->FirstName." ".$hp->LastName }}
                                           <?php $comma=","; ?>
                                          @endif
                                          <?php $r++; ?>
                                        @endforeach
                                    </td>
                                    <?php $i++; ?>
                                </tr>
                                @endforeach
                                
                                
                              <!--  <tr>
                                    <td align="center" style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        1
                                    </td>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        <b>
                                            Standard Double Room</b>
                                        <br>
                                        
                                    </td>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        1
                                    </td>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        2
                                    </td>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        1
                                        Adult(s)1
                                        Child(ren)
                                        
                                        <br>
                                        
                                        Adults:Kumar Sharma
                                        
                                        <br>
                                        Child(ren):Evank Dixit
                                        
                                    </td>
                                </tr>-->
                                
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 1px 2px;">
                        <table style="width: 100%; border-collapse: collapse;">
                            <tbody>
                                <tr>
                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                        <h3 style="color: #004684; padding: 2px 0; margin: 2px 0;">
                                            Remarks</h3>
                                        <div>
                                            Please note that while your booking had been confirmed and is guaranteed, the rooming
                                            list with your name may not be adjusted in the hotel's reservation system until
                                            closer to arrival.<br/>
                                            *  {{$bookingInfo->GetBookingDetailResult->HotelPolicyDetail}}
                                            
                                        </div>
                                    </td>
                                </tr>
                                <!-- if there is cancellation policy -->
                                @if($bookingInfo->GetBookingDetailResult->HotelRoomsDetails[0]->CancellationPolicy)
                                <tr>
                                    <td style="padding: 1px 2px;">
                                        <table style="width: 100%; border-collapse: collapse;">
                                            <tbody>
                                                <tr>
                                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                                        <h3 style="color: #004684; padding: 2px 0; margin: 2px 0;">
                                                            Remarks</h3>
                                                        <div>
                                                           {{$bookingInfo->GetBookingDetailResult->HotelRoomsDetails[0]->CancellationPolicy}}
                                                            
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endif
                                                
                                                <tr>
                                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                                        <h3 style="color: #004684; padding: 2px 0; margin: 2px 0;">
                                                            Booking Terms &amp; Conditions
                                                        </h3>
                                                        <div>
                                                            <ul>
                                                                <li style="list-style: disc outside none; margin-left: 20px;">You must present a photo
                                                                    ID at the time of check in. Hotel may ask for credit card or cash deposit for the
                                                                    extra services at the time of check in.</li>
                                                                <li style="list-style: disc outside none; margin-left: 20px;">All extra charges should
                                                                    be collected directly from clients prior to departure such as parking, phone calls,
                                                                    room service, city tax, etc.</li>
                                                                <li style="list-style: disc outside none; margin-left: 20px;">We don't accept any responsibility
                                                                    for additional expenses due to the changes or delays in air, road, rail, sea or
                                                                    indeed of any other causes, all such expenses will have to be borne by passengers.</li>
                                                                <li style="list-style: disc outside none; margin-left: 20px;">In case of wrong residency
                                                                    &amp; nationality selected by user at the time of booking; the supplement charges
                                                                    may be applicable and need to be paid to the hotel by guest on check in/ check out.</li>
                                                                <li style="list-style: disc outside none; margin-left: 20px;">Any special request for
                                                                    bed type, early check in, late check out, smoking rooms, etc are not guaranteed
                                                                    as subject to availability at the time of check in.</li>
                                                                <li style="list-style: disc outside none; margin-left: 20px;">Early check out will attract
                                                                    full cancellation charges unless otherwise specified.</li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                             
                                                
                                                <tr>
                                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                                        <h3 style="color: #004684; padding: 2px 0; margin: 2px 0;">
                                                            Hotel Policies
                                                        </h3>
                                                        <div>
                                                            <ul>
                                                                
                                                                <li style="list-style: disc outside none; margin-left: 20px;">
                                                                    Early check out will attract full cancellation charge unless otherwise specified.</li>
                                                                
                                                                <li style="list-style: disc outside none; margin-left: 20px;">
                                                                    . -This booking is non-amendable, If you need to make an amendment, please cancel and re-book for the new dates or room type.. <p><b>Know Before You Go</b> </p><ul>  <li>No pets and no service animals are allowed at this property. </li> </ul><p></p><p><b>Fees</b> </p><p>The following fees and deposits are charged by the property at time of service, check-in, or check-out. </p> <ul> <li>Breakfast fee: between THB 50 and THB 200 per person (approximately)</li>                               </ul> <p>The above list may not be comprehensive. Fees and deposits may not include tax and are subject to change. </p><p></p>. 1 double bed. Free Parking. Free Wireless Internet. Standard Double Room</li>
                                                                
                                                                <li style="list-style: disc outside none; margin-left: 20px;">
                                                                    City tax and Resort fee are to be paid directly at hotel if applicable.</li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td style="border: 1px solid #9CBAD6; padding: 4px 5px;">
                                                        <h3 style="color: #004684; padding: 2px 0; margin: 2px 0;">
                                                            Contact Details:
                                                        </h3>
                                                        
                                                        Phone :
                                                        9780814728
                                                        <br>
                                                        Email :
                                                        us@thailandwale.com
                                                        <br>
                                                    </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                        <div style="margin: 5px;">
                                            
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
					</td>
                </tr>
            </tbody>
        </table>
        @endif
                        <input type="hidden" name="sAddress" id="sAddress-0" value="24/46 Kaset-Nawamin Road Soi Jamjan, Klongkum, Bangkok, 10230 ZipCode: 10230">
                        <input type="hidden" name="sMap" id="sMap-0" value="13.81631|100.63947">
                        <input type="hidden" name="sHotelName" id="sHotelName-0" value="Asia Resort Kaset Nawamin">
                        <input type="hidden" name="sHotelRating" id="sRating-0" value="ThreeStar">
                        <div id="contentString" style="display:none;">
                            <div id="content">
                                <div id="siteNotice">
                                    <h2 id="firstHeading" class="firstHeading" style=" float:left; width:65%;">Asia Resort Kaset Nawamin</h2>
                                    <span style=" float:left; width:130px; margin:5px 0 0 0;">ThreeStar</span><br>
                                    <div id="bodyContent" style="float:left; width:65%;"><br>
                                        <p>24/46 Kaset-Nawamin Road Soi Jamjan, Klongkum, Bangkok, 10230 ZipCode: 10230</p><br>                                  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="confirmationNo" name="confirmationNo" value="QC9FBO(313089021|1234,1235)">
                        <input type="hidden" id="BookingNo" name="BookingNo" value="1377885">
                        <div id="emailBlock" style="display: none;">
                            <div style="border: 1px solid #D7DFF4; background: #fff; border-radius: 8px; -webkit-border-radius: 8px;
                                -ms-border-radius: 8px; width: 240px; padding-bottom: 1%; box-shadow: 2px 2px 3px #D5DCEF;
                                z-index: 100; position: absolute; left: 530px; top: 135px;">
                                <div style="float: right; margin: -6px -6px 0 0;">
                                    <a style="background: url(../images/Air/fl_close.png) no-repeat; float: right; width: 20px;
                                        height: 20px; cursor: pointer" id="emailClose"></a>
                                </div>
                                <div style="float: left; width: 90%; padding: 5%;">
                                    <span id="emailMsg" style="font-size: 10px; height: 20px;"></span>
                                    <div style="float: left; font-weight: bold; padding-bottom: 15px; width: 100%; margin-left: 10px;">
                                        <span style="width: 150px; float: left; font-size: 12px; padding-bottom: 10px;">Enter
                                            email address :</span>
                                        <input id="addressBox" name="" type="text" style="width: 180px; padding: 3px;">
                                    </div>
                                    <div style="float: left; width: 80%; text-align: left; margin-left: 10px;">
                                        <input id="sendEmail" type="button" value="Send mail" style="margin-right: 10px;">
                                        <input id="emailCancel" type="button" value="Cancel">
                                    </div>
                                </div>
                            </div>
                        </div>
                    
    
    <div id="modalBGR" class="modalBGR" style="display: none;">
    </div>
    <div class="hotel_view_map_popup" style="display: none; background: none repeat scroll 0 0 #ffffff;border: 1px solid #d7dff4;border-radius: 8px;box-shadow: 2px 2px 3px #d5dcef;left: 20%;margin-top: 3%;padding-bottom: 0; position: fixed;top: 0;width: 800px !important;z-index: 100;" id="mapDiv">
        <div class="dtl_left">
            <div class="hotel_view_map_head" style="background: none repeat scroll 0 0 #dee4f6;  border-radius: 8px 8px 0 0; color: #004684; float: left; font-size: 12px; font-weight: bold; width: 100%;">
                <dfn><a class="cursor" href="#null" id="closeMapDiv" style="float:right; background:url('./Images/Air/fl_close_hover.png'); height:20px; margin-right:-10px; margin-top:-11px; width: 20px;"></a></dfn> <span style=" float:left; padding:2%;">Google Map</span>
            </div>
        </div>
        <div class="map_mainbox" id="googleMap" style="border: 2px solid #edeff9;float: left;margin: 1%; min-height: 400px;width: 97.5%;">
        </div>
    </div>
    
    <input type="hidden" name="DestinationPDF" id="DestinationPDF" value="No guide found for this city">
    <input type="hidden" name="cityID" id="cityID" value="0">
    <input type="hidden" name="cityName" id="cityName" value="Bangkok">
    </form>

</body>
@endsection
    <script type="text/javascript" language="javascript">
        function TAPDF() {
            $('#frmTA').submit();
        }
        $("#backbutton").draggable();
        $('#backbutton').click(function () {
            history.go(-1);
            window.close();
            return false;
        });

    </script>

        
