@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.customerBooking.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.customerBooking.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.customerBooking.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.customer_bookings.partials.customerBooking-header-buttons')
            </div>
        </div><!-- /.box-header -->


        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="customer-booking-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.customerBooking.table.booking_id') }}</th>
                            <!-- <th>Transaction Id</th> -->
                            <th>{{ trans('labels.backend.customerBooking.table.booking_type') }}</th>
                            <th>{{ trans('labels.backend.customerBooking.table.checkin') }}</th>

                            <th>{{ trans('labels.general.actions') }}</th>

                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            <th>
                                {!! Form::text('booking_id', null, ["class" => "search-input-text form-control", "data-column" => 0, "placeholder" => trans('labels.backend.customerBooking.table.booking_id')]) !!}
                            </th>
                            <th>
                            </th>
                            <th>
                            </th>
                            <!-- <th>
                                {!! Form::text('lead_passenger', null, ["class" => "search-input-text form-control", "data-column" => 0, "placeholder" => trans('labels.backend.customerBooking.table.lead_passenger')]) !!}
                            </th>
                            <th>
                                {!! Form::text('booking_type', null, ["class" => "search-input-text form-control", "data-column" => 0, "placeholder" => trans('labels.backend.customerBooking.table.booking_type')]) !!}
                            </th>
                            <th>
                                {!! Form::text('checkin', null, ["class" => "search-input-text form-control", "data-column" => 0, "placeholder" => trans('labels.backend.customerBooking.table.checkin')]) !!}
                            </th>
                            <th>
                                {!! Form::text('checkout', null, ["class" => "search-input-text form-control", "data-column" => 0, "placeholder" => trans('labels.backend.customerBooking.table.checkout')]) !!}
                            </th> -->
                        </tr>
                    </thead>

                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->


@endsection

@section('after-scripts')
{{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            var dataTable = $('#customer-booking-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.customerbookings.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'booking_id', name: '{{config('module.customerbookings.table')}}.booking_id',
                    "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = '<a href="booking_info/' + data + '">' + data + '</a>';
                            }

                            return data;
                        }

                    },
                    // {data: 'transaction_id', name: 'transaction.txn_id'},

                    {data: 'booking_type', name: '{{config('module.customerbookings.table')}}.booking_type'},
                    {data: 'checkin', name: '{{config('module.customerbookings.table')}}.checkin'},

                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1 ]  }}
                    ]
                }
            });

         FinBuilders.DataTableSearch.init(dataTable);
        });
    </script>
    @endsection
