<div class="box-body">
    <div class="form-group">
        {{ Form::label('title', trans('validation.attributes.backend.coupon.title'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.coupon.title'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

      <div class="form-group">
        {{ Form::label('description', trans('validation.attributes.backend.coupon.description'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10 mce-box">
            {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.coupon.description')]) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('code', trans('validation.attributes.backend.coupon.code'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::text('code', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.coupon.code'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('type', trans('validation.attributes.backend.coupon.type'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">

        @if(!empty($coupon->type))
            {{ Form::select('type', $couponCat, $coupon->type, ['class' => 'form-control tags box-size', 'required' => 'required', 'multiple' => 'multiple']) }}
        @else
        {{ Form::select('type', $couponCat, null, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
        @endif
        
            
        
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('start_date', trans('validation.attributes.backend.coupon.start'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($coupon->start_date))
                {{ Form::text('start_date', \Carbon\Carbon::parse($coupon->start)->format('m/d/Y h:i a'), ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.blogs.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
            @else
                {{ Form::text('start_date', null, ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.coupon.start'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('expiry_date', trans('validation.attributes.backend.coupon.end'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            @if(!empty($coupon->expiry_date))
                {{ Form::text('expiry_date', \Carbon\Carbon::parse($coupon->end)->format('m/d/Y h:i a'), ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.blogs.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
            @else
                {{ Form::text('expiry_date', null, ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.coupon.end'),  'required' => 'required', 'id' => 'datetimepicker2']) }}
            @endif
        </div><!--col-lg-10-->
    </div><!--form control-->

<div class="form-group">
        {{ Form::label('services', trans('validation.attributes.backend.coupon.category'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
        @if(!empty($selectedServices))
            {{ Form::select('services[]', $services, $selectedServices, ['class' => 'form-control tags box-size', 'required' => 'required', 'multiple' => 'multiple']) }}
        @else
            {{ Form::select('services[]', $services, null, ['class' => 'form-control tags box-size', 'required' => 'required', 'multiple' => 'multiple']) }}
        @endif
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('amount', trans('validation.attributes.backend.coupon.amount'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::number('amount', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.coupon.amount'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('max_discount', trans('validation.attributes.backend.coupon.max_discount'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::number('max_discount', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.coupon.max_discount'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('minimum_amount', trans('validation.attributes.backend.coupon.minimum_amount'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::number('minimum_amount', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.coupon.minimum_amount'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('single_user_restriction', trans('validation.attributes.backend.coupon.single_user'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::number('single_user_restriction', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.coupon.single_user'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

    <div class="form-group">
        {{ Form::label('all_user_restriction', trans('validation.attributes.backend.coupon.all_user'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
            {{ Form::number('all_user_restriction', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.coupon.all_user'), 'required' => 'required']) }}
        </div><!--col-lg-10-->
    </div><!--form control-->

   

    <div class="form-group">
        {{ Form::label('status', trans('validation.attributes.backend.blogs.status'), ['class' => 'col-lg-2 control-label required']) }}

        <div class="col-lg-10">
           {{ Form::select('status', $status, null, ['class' => 'form-control select2 status box-size', 'placeholder' => trans('validation.attributes.backend.blogs.status'), 'required' => 'required']) }}
        </div><!--col-lg-3-->
    </div><!--form control-->
</div>

@section("after-scripts")
    <script type="text/javascript">

        
        Backend.Coupon.init();
        
    </script>
@endsection