@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.coupons.management'))

@section('page-header')
    <h1>{{ trans('labels.backend.coupons.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.coupons.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.coupons.partials.coupons-header-buttons')
            </div>
        </div><!--box-header with-border-->

        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="coupons-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.coupons.table.id') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.title') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.code') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.end_date') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.type') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.services') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.amount') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.single_user') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.all_user') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.max') }}</th>
                            <th>{{ trans('labels.backend.coupons.table.createdat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg">
                        <tr>
                            
                        </tr>
                    </thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection

@section('after-scripts')
    {{-- For DataTables --}}
    {{ Html::script(mix('js/dataTable.js')) }}

    <script>
        //Below written line is short form of writing $(document).ready(function() { })
        $(function() {
            var dataTable = $('#coupons-table').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.coupons.get") }}',
                    type: 'post'
                },
                columns: [
                    {data: 'id', name: '{{config('module.coupons.table')}}.id'},
                    {data: 'title', name: '{{config('module.coupons.table')}}.title'},
                    {data: 'code', name: '{{config('module.coupons.table')}}.code'},
                    {data: 'expiry_date', name: '{{config('module.coupons.table')}}.expiry_date'},
                    {data: 'type', name: '{{config('module.coupons.table')}}.type'},
                    {data: 'services', name: '{{config('module.coupons.table')}}.expiry_date'},
                    {data: 'amount', name: '{{config('module.coupons.table')}}.amount'},
                    {data: 'single_user_restriction', name: '{{config('module.coupons.table')}}.single_user_restriction'},
                    {data: 'all_user_restriction', name: '{{config('module.coupons.table')}}.all_user_restriction'},
                    {data: 'max_discount', name: '{{config('module.coupons.table')}}.max_discount'},
                    
                    {data: 'created_at', name: '{{config('module.coupons.table')}}.created_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                dom: 'lBfrtip',
                buttons: {
                    buttons: [
                        { extend: 'copy', className: 'copyButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'csv', className: 'csvButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'excel', className: 'excelButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'pdf', className: 'pdfButton',  exportOptions: {columns: [ 0, 1 ]  }},
                        { extend: 'print', className: 'printButton',  exportOptions: {columns: [ 0, 1 ]  }}
                    ]
                }
            });

            FinBuilders.DataTableSearch.init(dataTable);
        });
    </script>
@endsection
