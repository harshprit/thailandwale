<!--<!doctype html>-->
<!--<html>-->
<!--<head>-->
<!--    <meta charset="utf-8">-->
<!--    <title>{{ trans('http.404.title') }}</title>-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->

<!--    <style>-->
<!--        * {-->
<!--            line-height: 1.2;-->
<!--            margin: 0;-->
<!--        }-->

<!--        html {-->
<!--            color: #888;-->
<!--            display: table;-->
<!--            font-family: sans-serif;-->
<!--            height: 100%;-->
<!--            text-align: center;-->
<!--            width: 100%;-->
<!--        }-->

<!--        body {-->
<!--            display: table-cell;-->
<!--            vertical-align: middle;-->
<!--            margin: 2em auto;-->
<!--        }-->

<!--        h1 {-->
<!--            color: #555;-->
<!--            font-size: 2em;-->
<!--            font-weight: 400;-->
<!--        }-->

<!--        p {-->
<!--            margin: 0 auto;-->
<!--            width: 280px;-->
<!--        }-->

<!--        @media only screen and (max-width: 280px) {-->

<!--            body, p {-->
<!--                width: 95%;-->
<!--            }-->

<!--            h1 {-->
<!--                font-size: 1.5em;-->
<!--                margin: 0 0 0.3em;-->
<!--            }-->

<!--        }-->
<!--    </style>-->
<!--</head>-->
<!--<body>-->
<!--<h1>{{ trans('http.404.title') }}</h1>-->
<!--<p>{{ trans('http.404.description') }}</p>-->
<!--</body>-->
<!--</html>-->
<!--New 404 error-->
<!DOCTYPE html>
<html lang="en">

 <head> 

<!--<link rel="stylesheet" href="style.css">-->
<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> -->

<style>

.m-t-80 {
    margin-top: 80px !important;
}

.p-b-150 {
    padding-bottom: 150px !important;
}

.text-medium {
    font-size: 50px !important;
    font-weight: 800;
    line-height: 1.1;
    margin-bottom: 20px;
}

.text-left {
    text-align: left !important;
}

.lead {
    font-size: 1.35714286em;
    line-height: 1.68421053em;
}

.page-error-404 {
    color: #eee;
    display: block;
    font-size: 300px;
    font-weight: 800;
    line-height: 0.7;
}


</style>
 </head>

<body>


    <!-- Wrapper -->
    <div id="wrapper">

        <!-- 404 PAGE -->
        <section class="m-t-80 p-b-150">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="page-error-404">404</div>
                    </div>
                    <div class="col-md-6">
                        <div class="text-left">
                            <h1 class="text-medium">Ooops, This Page Could Not Be Found!</h1   
                            <p class="lead">The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>
                            <div class="seperator m-t-20 m-b-20"></div>

                            <div class="search-form">
                                <p>Please try searching again</p>

                                <form action="search-results-page.html" method="get" class="form-inline">
                                    <div class="input-group input-group-lg">
                                        
                                        <span class="input-group-btn">
		                            		<button class="btn color btn-primary" type="submit">Visit Home Page</button>									
                        		</span>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end:  404 PAGE -->

        
    </div>
    <!-- end: Wrapper -->



</body>

</html>

<!--end new 404 error-->
<!-- IE needs 512+ bytes: http://blogs.msdn.com/b/ieinternals/archive/2010/08/19/http-error-pages-in-internet-explorer.aspx -->