<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'thailand_wp821');

/** MySQL database username */
define('DB_USER', 'thailand_wp821');

/** MySQL database password */
define('DB_PASSWORD', '8@0pSx[6VZ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'et115qfpmjzph6ssy1jomewct4nnrtlrq40qyark9ceafgy9zlktvj6uzeipwock');
define('SECURE_AUTH_KEY',  'htlaxq8okueo3kfmqvtaz7q0xlbffysdxt4w7bnkxqzl1c1nf6hkyjwv4rfd8rtd');
define('LOGGED_IN_KEY',    'wlaeyba1u2n3saduqfqhcvzqkqwakulik7inlfh05syxdysyrzlie99iadjaksbo');
define('NONCE_KEY',        'npdbyaghkcesl3d5qi8dgl7xhuuxlo2r3urltrwdlins6i9hq7jqpugzocykoxme');
define('AUTH_SALT',        '68nqxewbylmosz2fdfag7mn8ovy4vqixgx6nzocphc7jedct22egxygxfqueagqg');
define('SECURE_AUTH_SALT', 'gmlvrhr9aeu6nz8wo2gsgkyzccg3a9uz9j7v0v6prxvdeie17mwaibtnknwe51yd');
define('LOGGED_IN_SALT',   'skrnpkfncoxy0wis4nyytx18gnn9ij6lv8yjm6dusgiar2edm9ewledh4hjwrb7o');
define('NONCE_SALT',       'cixc6grbwo3zobbhgmgpyeqzalq6txpimrajnqnibdrp2wliklfx8reumfdm8sep');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp4p_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
