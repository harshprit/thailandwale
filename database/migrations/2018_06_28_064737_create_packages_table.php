<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 191);
            $table->string('featured_image', 191);
            $table->integer('destination_id');
            $table->string('day_title', 191);
            $table->string('day_destination', 191);
            $table->text('day_description');
            $table->string('day_image', 191);
            $table->string('offer', 191);
            $table->string('cost', 191);
            $table->text('content');
            $table->text('inclusion_exclusion');
            $table->string('slug', 191)->nullable();
           
            $table->enum('status', ['Published', 'Draft', 'InActive', 'Scheduled']);
            $table->dateTime('publish_datetime');
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
