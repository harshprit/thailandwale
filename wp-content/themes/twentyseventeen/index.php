<!DOCTYPE html>
<html lang="en">
<head>
<title>Thailandwale: Thailand As Never Before</title>

<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<!--// Meta tag Keywords -->

<!-- css files -->
<link rel="stylesheet" href="http://thailandwale.com/wp-content/themes/twentyseventeen/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
<link rel="stylesheet" href="http://thailandwale.com/wp-content/themes/twentyseventeen/style.css" type="text/css" media="all" /> <!-- Style-CSS --> 
<link rel="stylesheet" href="http://thailandwale.com/wp-content/themes/twentyseventeen/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
<!-- //css files -->



<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
<!-- //js -->
</head>
<body>
<!-- header -->
<div class="header">
	<div class="agile-top-header">
	<div class="banner-agile-top">

				
				<div class="clearfix"></div>
	</div>
		<div class="logo">
      <a href="http://thailandwale.com/"><img src="images/logo-2.png" class="img-responsive"></a>
		</div>
		<div class="features"><div class="col-sm-8"><div class="sub-title streched white strong mt20 hidden-xs hidden-sm" style="font-size: 20px"><ul class="simple-list"><li class="mt10"><span class="fa fa-check fs15 mr5"></span>Dedicated only to Thailand Destination'</li><li class="mt10"><span class="fa fa-check fs15 mr5"></span>No hidden charges, Transparency on the top.</li><li class="mt10"><span class="fa fa-check fs15 mr5"></span>Flexibility to Clients.</li><li class="mt10"><span class="fa fa-check fs15 mr5"></span>Indian Guide/ Driver for hassle free communication.</li>
<li class="mt10"><span class="fa fa-check fs15 mr5"></span>Security of customers by facilitating Travel Insuarance, free sim card to each customer.</li><li class="mt10"><span class="fa fa-check fs15 mr5"></span>Home to Home service.</li><li style="margin-top: 10px;" class="mt10">EXPERIENCE THAILAND WITH THAILANDWALE</li></ul></div></div></div>
		<!-- navigation -->
	
			<div class="clearfix"> </div>	
			<!-- //navigation -->
		</div>
	</div>
	<!--Slider-->
		<div class="slider">
			<div class="callbacks_container">
				<ul class="rslides" id="slider">
					<li>
						<div class="slider-info">
							<h3></h3>
						   
							
						</div>
					</li>
					<li>
						
					</li>
					<li>
						
					</li>
				</ul>
	
			</div>
			<div class="clearfix"></div>
</div>		
		<!--//Slider-->       

		<div class="main" id="main">
 <h3 class="callu-us-head">Call Us: +917404340404</h3>
		<div class="w3layouts_main_grid">
    
		<div class="booking-form-head-agile">
		</div>
		
		
			<?php echo do_shortcode('[contact-form-7 id="8" title="Contact form 1"]'); ?>
		</div>

</div>
<!-- //header -->

<!-- services--->
<div class="about">
<div class="container">
    <h2 style="text-align: center;">What Do We Take Care of</h2></br>
<div class="row">
<div class="col-md-4 col-xs-12">
		<img src="images/flights.png">
		<h1>Flights</h1>
</div>
<div class="col-md-4 col-xs-12">
<img src="images/hotel.png">
		<h1>Hotels</h1>
</div>
<div class="col-md-4 col-xs-12">
<img src="images/sightseeing-1.png">
		<h1>sightseeing</h1>
</div>
<div class="col-md-4 col-xs-12">
<img src="images/906211.png">
		<h1>Restaurants</h1>
</div>
<div class="col-md-4 col-xs-12">
<img src="images/Transfer-Arrows.png">
		<h1>Transfers</h1>
</div>
<div class="col-md-4 col-xs-12">
<img src="images/620765.png">
		<h1>VISA</h1>
</div>
</div>
</div>
</div>
<!--- services--->



	<!-- contact -->
<div class="contact" id="contact">
		<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14013.91795387332!2d77.312808!3d28.585389!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa33e1f3c2b9dba04!2sHacker+Space+Co-working!5e0!3m2!1sen!2sin!4v1536144866648" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		
			<div class="container">
				<div class="agileinfo_map_color">
					<div class="agileinfo_map_color_grid">
						<div class="col-md-6 contact-up">
							<div class="agileinfo_map_color_grid_left">
								<h4>Contact us</h4>
							<p>A-73, Sector-2 <span>Noida </span></p>
							<p>+917404340404
   
								</p>
						
								
							</div>
						</div>
						<div class="col-md-6 contact-up1">
							<div class="agileinfo_map_color_grid_left">
								<h4>Mail us</h4>
								<p><a href="#"> us@thailandwale.com</a></p>
								<p><a href="#"> sk@thailandwale.com</a></p>
								<ul class="map-top-icon">
								<li><i class="fa fa-facebook" aria-hidden="true"></i></li>
								<li><i class="fa fa-twitter" aria-hidden="true"></i></li>
								<li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
								</ul>
							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
		</div>
		
</div>
<!-- //contact -->
<!--footer-->
<div class="footer-w3">
    <div class="top-icons">
				<ul>
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				</ul>
			</div>
	<p>&copy; 2018 thailandwale. All rights reserved </p>
</div>

<!-- js files -->
	
	<!-- //For-Banner -->
		<!-- contact form -->


		<!-- //for-Testimonials -->
<!-- Calendar -->
			<link rel="stylesheet" href="css/jquery-ui.css" />
			<script src="js/jquery-ui.js"></script>
			  <script>
					  $(function() {
						$( "#datepicker,#datepicker1" ).datepicker();
					  });
			  </script>
		<!-- //Calendar -->
		
		<!-- smooth scrolling-bottom-to-top -->
				
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
				<script src="js/SmoothScroll.min.js"></script>



</body>
</html>

